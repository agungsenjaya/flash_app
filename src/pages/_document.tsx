/* eslint-disable jsx-a11y/iframe-has-title */
import Document, { Html, Head, Main, NextScript, DocumentContext } from 'next/document'
import { ServerStyleSheet } from 'styled-components'
import React from 'react'
import { nodes } from 'utils/getRpcUrl'

class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const sheet = new ServerStyleSheet()
    const originalRenderPage = ctx.renderPage

    try {
      // eslint-disable-next-line no-param-reassign
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: (App) => (props) => sheet.collectStyles(<App {...props} />),
        })

      const initialProps = await Document.getInitialProps(ctx)
      return {
        ...initialProps,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        ),
      }
    } finally {
      sheet.seal()
    }
  }

  render() {
    return (
      <Html>
        <Head>
          {nodes.map((node) => (
            <link key={node} rel="preconnect" href={node} />
          ))}
          {process.env.NEXT_PUBLIC_NODE_PRODUCTION && (
            <link rel="preconnect" href={process.env.NEXT_PUBLIC_NODE_PRODUCTION} />
          )}
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link href="https://fonts.googleapis.com/css2?family=Saira:wght@300;400;500;600;700&display=swap" rel="stylesheet" />
          <link rel="shortcut icon" href="https://lh3.googleusercontent.com/j27DpW5xn_sQAzELtwakAvFTXZAXdfnQn8nsnAuQRB0RbEgfEwQgnjMbf4AK3SvqwCPz_B0QYvF_ylMGPpIAqdO-I2wRlirCHaTiuGUF3aK4nZ8zTstc4V10YQJgKumbmE7gZnbm8stRxF7mcsnBdCyFl8_an19jVvQuoCcUh46qJZWoddYNTwmYnIhzzlatG3dci73ihimT5ziL2auXFc-VEg2ezVKEWuWw66UIyorLI33MG1ZU_G8QmArgNHc7qRnb6L2rICbm3RH5iA7aj9v2PQ7uFZmmmM_lT3C5mYgadND_7kznjeXkCIvYHp5KqPhTRnaP1294cmPQYEqVM6P0o39F4fpirAy7Oi8_F5gOmhNsHCQqjBT-zmPAcS_KK3QNGHHIpC6Sd2CSAD0p3zw467mr0iFzDOqWWdXXYBXK1hnhVz7sT_Pljo-OMfiUnQGjNTvmq-gypwfYHt9XiKytkWxhbiRZri6Ea_Ov9LFKfKoPCdR8_m8Phw0kDOV3EOSvnTFqBhV-EHkAHocICuL-36duZ2CY-513-i9-mJkcr_re347ZoK_Hp08-0fUhSLFlKAQUIwRXkTtOIo8CfZ8DitVw41Z1t44SWJPOypi4T_6J7J2NxCbLsTCthf9D8bSwT3e67LtDOMpnt5BympFdScKWiRtA_dzOR8z5lM0wrIGBmQxJAi2rXliLPQ3EF_yol2Iql1hLrZ12vNeCCrR8=w1632-h1446" />
          <link rel="apple-touch-icon" href="https://lh3.googleusercontent.com/j27DpW5xn_sQAzELtwakAvFTXZAXdfnQn8nsnAuQRB0RbEgfEwQgnjMbf4AK3SvqwCPz_B0QYvF_ylMGPpIAqdO-I2wRlirCHaTiuGUF3aK4nZ8zTstc4V10YQJgKumbmE7gZnbm8stRxF7mcsnBdCyFl8_an19jVvQuoCcUh46qJZWoddYNTwmYnIhzzlatG3dci73ihimT5ziL2auXFc-VEg2ezVKEWuWw66UIyorLI33MG1ZU_G8QmArgNHc7qRnb6L2rICbm3RH5iA7aj9v2PQ7uFZmmmM_lT3C5mYgadND_7kznjeXkCIvYHp5KqPhTRnaP1294cmPQYEqVM6P0o39F4fpirAy7Oi8_F5gOmhNsHCQqjBT-zmPAcS_KK3QNGHHIpC6Sd2CSAD0p3zw467mr0iFzDOqWWdXXYBXK1hnhVz7sT_Pljo-OMfiUnQGjNTvmq-gypwfYHt9XiKytkWxhbiRZri6Ea_Ov9LFKfKoPCdR8_m8Phw0kDOV3EOSvnTFqBhV-EHkAHocICuL-36duZ2CY-513-i9-mJkcr_re347ZoK_Hp08-0fUhSLFlKAQUIwRXkTtOIo8CfZ8DitVw41Z1t44SWJPOypi4T_6J7J2NxCbLsTCthf9D8bSwT3e67LtDOMpnt5BympFdScKWiRtA_dzOR8z5lM0wrIGBmQxJAi2rXliLPQ3EF_yol2Iql1hLrZ12vNeCCrR8=w1632-h1446" />
          <link rel="manifest" href="/manifest.json" />
        </Head>
        <body>
          <noscript>
            <iframe
              src={`https://www.googletagmanager.com/ns.html?id=${process.env.NEXT_PUBLIC_GTAG}`}
              height="0"
              width="0"
              style={{ display: 'none', visibility: 'hidden' }}
            />
          </noscript>
          <Main />
          <NextScript />
          <div id="portal-root" />
        </body>
      </Html>
    )
  }
}

export default MyDocument
