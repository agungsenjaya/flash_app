import { ResetCSS } from '@pancakeswap/uikit'
import Script from 'next/script'
import BigNumber from 'bignumber.js'
import EasterEgg from 'components/EasterEgg'
import GlobalCheckClaimStatus from 'components/GlobalCheckClaimStatus'
import SubgraphHealthIndicator from 'components/SubgraphHealthIndicator'
import { ToastListener } from 'contexts/ToastsContext'
import useEagerConnect from 'hooks/useEagerConnect'
import { useInactiveListener } from 'hooks/useInactiveListener'
import useSentryUser from 'hooks/useSentryUser'
import useUserAgent from 'hooks/useUserAgent'
import type { AppProps } from 'next/app'
import Head from 'next/head'
import React, { Fragment } from 'react'
import { PersistGate } from 'redux-persist/integration/react'
import { useStore, persistor } from 'state'
import { usePollBlockNumber } from 'state/block/hooks'
import { usePollCoreFarmData } from 'state/farms/hooks'
import { NextPage } from 'next'
import { useFetchProfile } from 'state/profile/hooks'
import { Blocklist, Updaters } from '..'
import ErrorBoundary from '../components/ErrorBoundary'
import Menu from '../components/Menu'
import Providers from '../Providers'
import GlobalStyle from '../style/Global'

// This config is required for number formatting
BigNumber.config({
  EXPONENTIAL_AT: 1000,
  DECIMAL_PLACES: 80,
})

function GlobalHooks() {
  usePollBlockNumber()
  useEagerConnect()
  useFetchProfile()
  usePollCoreFarmData()
  useUserAgent()
  useInactiveListener()
  useSentryUser()
  return null
}

function MyApp(props: AppProps) {
  const { pageProps } = props
  const store = useStore(pageProps.initialReduxState)

  return (
    <>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=5, minimum-scale=1, viewport-fit=cover"
        />
        <meta
          name="description"
          content="Flash Hero is the first Metahero reflection token running on Binance Smart Chain."
        />
        <meta name="theme-color" content="#203C2F" />
        <meta name="twitter:image" content="https://lh3.googleusercontent.com/Sgr9vr3SXIgkUgALlzy_0-C0uYc-7K8DZvvsSGbQOQZtbLqa_DPq84xmO0zzC-9Pg5dABNRd6PvIFscEoPVMZZ7Km4W5O1ndFe_yEsDNgilBXAOW4ggSdGlgKwChwiG7sOoKu2JwIs77F0bzo3jbYg0YmbvbbnI4_Gbc0MnpgcOGYBqfQ6XkxL96_skZobb-mTzZz8rIpF2s2bp9BKOTsCAIcV6VdjAxla3JTwjQNAsJf0sLYLRUjs80CRVke7JRTZqUjokW1mTvCFpzsaAs4nBTAH4MBAa5A02aUbM-KyoqIeN5xVUDjQ-jxXoC74LFDhFK-NPGnf7Q8PQBBzS-yAWjShg34iF6wIk0dBYgbo7Q29dk5g3qv2oCOYjSyyMP21GSpRiM1hBpFif2CshOSFAWCRnZjhI95YSQQytFk_dTb6iW8P_332A_Qtuf98DsbijVemoM5EsmuRryc30OPucMt_llDLGw2Lb0puXPa9143aA_S_GT5oSMCk1k5yaiXJql2-mlB52t6YTcQ8iyU46CUR5Vavf9QZcU3CbXi96u1t6JkX6nIT0YOdwI7YbE4bEgxeiLQNcFJgdOzRPPLTHNZze8IkFsaq0adbWCqxdxntSEheQAWiSXmTGpSTnQ-2gu5IvpOABsq9yhOBAHGU8089iGAJRrRC16CdYKhW1o-wkdzelGWBSgMaFrk85WcnS4S0tYP9otrF5GatV0TzgD=w370-h208" />
        <meta
          name="twitter:description"
          content="Flash Hero is the first Metahero reflection token running on Binance Smart Chain. The longer you hold, The more you earn. It is really that simple. "
        />
        <meta name="twitter:card" content="summary_large_image" />
        <meta
          name="twitter:title"
          content="Flash Hero BSC - Exchange Token With Flash Hero BSC Token"
        />
        <title>Flash Hero Bsc | Exchange</title>
      </Head>
      <Providers store={store}>
        <Blocklist>
          <GlobalHooks />
          <Updaters />
          <ResetCSS />
          <GlobalStyle />
          <GlobalCheckClaimStatus excludeLocations={[]} />
          <PersistGate loading={null} persistor={persistor}>
            <App {...props} />
          </PersistGate>
        </Blocklist>
      </Providers>
      <Script
        strategy="afterInteractive"
        id="google-tag"
        dangerouslySetInnerHTML={{
          __html: `
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer', '${process.env.NEXT_PUBLIC_GTAG}');
          `,
        }}
      />
    </>
  )
}

type NextPageWithLayout = NextPage & {
  Layout?: React.FC
}

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout
}

const App = ({ Component, pageProps }: AppPropsWithLayout) => {
  // Use the layout defined at the page level, if available
  const Layout = Component.Layout || Fragment
  return (
    <ErrorBoundary>
      <Menu>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </Menu>
      <EasterEgg iterations={2} />
      <ToastListener />
      <SubgraphHealthIndicator />
    </ErrorBoundary>
  )
}

export default MyApp
