"use strict";
(() => {
var exports = {};
exports.id = 660;
exports.ids = [660];
exports.modules = {

/***/ 5641:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_document__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6859);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var utils_getRpcUrl__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(7308);

/* eslint-disable jsx-a11y/iframe-has-title */ 



class MyDocument extends next_document__WEBPACK_IMPORTED_MODULE_1__["default"] {
    static async getInitialProps(ctx) {
        const sheet = new styled_components__WEBPACK_IMPORTED_MODULE_2__.ServerStyleSheet();
        const originalRenderPage = ctx.renderPage;
        try {
            // eslint-disable-next-line no-param-reassign
            ctx.renderPage = ()=>originalRenderPage({
                    enhanceApp: (App)=>(props)=>sheet.collectStyles(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(App, {
                                ...props
                            }))
                })
            ;
            const initialProps = await next_document__WEBPACK_IMPORTED_MODULE_1__["default"].getInitialProps(ctx);
            return {
                ...initialProps,
                styles: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                    children: [
                        initialProps.styles,
                        sheet.getStyleElement()
                    ]
                })
            };
        } finally{
            sheet.seal();
        }
    }
    render() {
        return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(next_document__WEBPACK_IMPORTED_MODULE_1__.Html, {
            children: [
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(next_document__WEBPACK_IMPORTED_MODULE_1__.Head, {
                    children: [
                        utils_getRpcUrl__WEBPACK_IMPORTED_MODULE_4__/* .nodes.map */ .t.map((node)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("link", {
                                rel: "preconnect",
                                href: node
                            }, node)
                        ),
                         true && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("link", {
                            rel: "preconnect",
                            href: "https://nodes.pancakeswap.com"
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("link", {
                            rel: "preconnect",
                            href: "https://fonts.gstatic.com"
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("link", {
                            href: "https://fonts.googleapis.com/css2?family=Saira:wght@300;400;500;600;700&display=swap",
                            rel: "stylesheet"
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("link", {
                            rel: "shortcut icon",
                            href: "https://lh3.googleusercontent.com/j27DpW5xn_sQAzELtwakAvFTXZAXdfnQn8nsnAuQRB0RbEgfEwQgnjMbf4AK3SvqwCPz_B0QYvF_ylMGPpIAqdO-I2wRlirCHaTiuGUF3aK4nZ8zTstc4V10YQJgKumbmE7gZnbm8stRxF7mcsnBdCyFl8_an19jVvQuoCcUh46qJZWoddYNTwmYnIhzzlatG3dci73ihimT5ziL2auXFc-VEg2ezVKEWuWw66UIyorLI33MG1ZU_G8QmArgNHc7qRnb6L2rICbm3RH5iA7aj9v2PQ7uFZmmmM_lT3C5mYgadND_7kznjeXkCIvYHp5KqPhTRnaP1294cmPQYEqVM6P0o39F4fpirAy7Oi8_F5gOmhNsHCQqjBT-zmPAcS_KK3QNGHHIpC6Sd2CSAD0p3zw467mr0iFzDOqWWdXXYBXK1hnhVz7sT_Pljo-OMfiUnQGjNTvmq-gypwfYHt9XiKytkWxhbiRZri6Ea_Ov9LFKfKoPCdR8_m8Phw0kDOV3EOSvnTFqBhV-EHkAHocICuL-36duZ2CY-513-i9-mJkcr_re347ZoK_Hp08-0fUhSLFlKAQUIwRXkTtOIo8CfZ8DitVw41Z1t44SWJPOypi4T_6J7J2NxCbLsTCthf9D8bSwT3e67LtDOMpnt5BympFdScKWiRtA_dzOR8z5lM0wrIGBmQxJAi2rXliLPQ3EF_yol2Iql1hLrZ12vNeCCrR8=w1632-h1446"
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("link", {
                            rel: "apple-touch-icon",
                            href: "https://lh3.googleusercontent.com/j27DpW5xn_sQAzELtwakAvFTXZAXdfnQn8nsnAuQRB0RbEgfEwQgnjMbf4AK3SvqwCPz_B0QYvF_ylMGPpIAqdO-I2wRlirCHaTiuGUF3aK4nZ8zTstc4V10YQJgKumbmE7gZnbm8stRxF7mcsnBdCyFl8_an19jVvQuoCcUh46qJZWoddYNTwmYnIhzzlatG3dci73ihimT5ziL2auXFc-VEg2ezVKEWuWw66UIyorLI33MG1ZU_G8QmArgNHc7qRnb6L2rICbm3RH5iA7aj9v2PQ7uFZmmmM_lT3C5mYgadND_7kznjeXkCIvYHp5KqPhTRnaP1294cmPQYEqVM6P0o39F4fpirAy7Oi8_F5gOmhNsHCQqjBT-zmPAcS_KK3QNGHHIpC6Sd2CSAD0p3zw467mr0iFzDOqWWdXXYBXK1hnhVz7sT_Pljo-OMfiUnQGjNTvmq-gypwfYHt9XiKytkWxhbiRZri6Ea_Ov9LFKfKoPCdR8_m8Phw0kDOV3EOSvnTFqBhV-EHkAHocICuL-36duZ2CY-513-i9-mJkcr_re347ZoK_Hp08-0fUhSLFlKAQUIwRXkTtOIo8CfZ8DitVw41Z1t44SWJPOypi4T_6J7J2NxCbLsTCthf9D8bSwT3e67LtDOMpnt5BympFdScKWiRtA_dzOR8z5lM0wrIGBmQxJAi2rXliLPQ3EF_yol2Iql1hLrZ12vNeCCrR8=w1632-h1446"
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("link", {
                            rel: "manifest",
                            href: "/manifest.json"
                        })
                    ]
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("body", {
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("noscript", {
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("iframe", {
                                src: `https://www.googletagmanager.com/ns.html?id=${"GTM-TL4JTFW"}`,
                                height: "0",
                                width: "0",
                                style: {
                                    display: 'none',
                                    visibility: 'hidden'
                                }
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_document__WEBPACK_IMPORTED_MODULE_1__.Main, {}),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_document__WEBPACK_IMPORTED_MODULE_1__.NextScript, {}),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            id: "portal-root"
                        })
                    ]
                })
            ]
        }));
    }
}
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MyDocument);


/***/ }),

/***/ 7657:
/***/ ((module) => {

module.exports = require("lodash/sample");

/***/ }),

/***/ 4140:
/***/ ((module) => {

module.exports = require("next/dist/server/get-page-files.js");

/***/ }),

/***/ 9716:
/***/ ((module) => {

module.exports = require("next/dist/server/htmlescape.js");

/***/ }),

/***/ 6368:
/***/ ((module) => {

module.exports = require("next/dist/server/utils.js");

/***/ }),

/***/ 6724:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/constants.js");

/***/ }),

/***/ 2796:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/head-manager-context.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 6689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 997:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 7518:
/***/ ((module) => {

module.exports = require("styled-components");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [730,859,308], () => (__webpack_exec__(5641)));
module.exports = __webpack_exports__;

})();
//# sourceMappingURL=_document.js.map