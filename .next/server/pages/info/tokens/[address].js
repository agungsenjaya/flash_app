"use strict";
(() => {
var exports = {};
exports.id = 861;
exports.ids = [861];
exports.modules = {

/***/ 7899:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ _address_),
  "getStaticPaths": () => (/* binding */ getStaticPaths),
  "getStaticProps": () => (/* binding */ getStaticProps)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./src/components/NextLink.tsx
var NextLink = __webpack_require__(3629);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: external "@pancakeswap/uikit"
var uikit_ = __webpack_require__(2829);
// EXTERNAL MODULE: ./src/components/Layout/Page.tsx + 3 modules
var Page = __webpack_require__(5601);
// EXTERNAL MODULE: ./src/utils/index.ts + 1 modules
var utils = __webpack_require__(8328);
// EXTERNAL MODULE: ./src/utils/truncateHash.ts
var truncateHash = __webpack_require__(3467);
;// CONCATENATED MODULE: ./src/views/Info/hooks/useCMCLink.ts

// endpoint to check asset exists and get url to CMC page
// returns 400 status code if token is not on CMC
const CMC_ENDPOINT = 'https://3rdparty-apis.coinmarketcap.com/v1/cryptocurrency/contract?address=';
/**
 * Check if asset exists on CMC, if exists
 * return  url, if not return undefined
 * @param address token address (all lowercase, checksummed are not supported by CMC)
 */ const useCMCLink = (address)=>{
    const { 0: cmcPageUrl , 1: setCMCPageUrl  } = (0,external_react_.useState)(undefined);
    (0,external_react_.useEffect)(()=>{
        const fetchLink = async ()=>{
            const result = await fetch(`${CMC_ENDPOINT}${address}`);
            // if link exists, format the url
            if (result.status === 200) {
                result.json().then(({ data  })=>{
                    setCMCPageUrl(data.url);
                });
            }
        };
        if (address) {
            fetchLink();
        }
    }, [
        address
    ]);
    return cmcPageUrl;
};
/* harmony default export */ const hooks_useCMCLink = (useCMCLink);

// EXTERNAL MODULE: ./src/views/Info/components/CurrencyLogo/index.tsx + 1 modules
var CurrencyLogo = __webpack_require__(764);
// EXTERNAL MODULE: ./src/views/Info/utils/formatInfoNumbers.ts
var formatInfoNumbers = __webpack_require__(3589);
// EXTERNAL MODULE: ./src/views/Info/components/Percent/index.tsx
var Percent = __webpack_require__(7795);
// EXTERNAL MODULE: ./src/views/Info/components/SaveIcon/index.tsx
var SaveIcon = __webpack_require__(3614);
// EXTERNAL MODULE: ./src/state/info/hooks.ts + 6 modules
var hooks = __webpack_require__(3985);
// EXTERNAL MODULE: ./src/views/Info/components/InfoTables/PoolsTable.tsx
var PoolsTable = __webpack_require__(994);
// EXTERNAL MODULE: ./src/views/Info/components/InfoTables/TransactionsTable.tsx
var TransactionsTable = __webpack_require__(8543);
// EXTERNAL MODULE: ./src/state/user/hooks/index.tsx
var user_hooks = __webpack_require__(8605);
// EXTERNAL MODULE: ./src/config/constants/info.ts
var info = __webpack_require__(4003);
// EXTERNAL MODULE: ./src/contexts/Localization/index.tsx + 3 modules
var Localization = __webpack_require__(9150);
// EXTERNAL MODULE: ./src/views/Info/components/InfoCharts/ChartCard/index.tsx + 1 modules
var ChartCard = __webpack_require__(5275);
;// CONCATENATED MODULE: ./src/views/Info/Tokens/TokenPage.tsx

/* eslint-disable no-nested-ternary */ 


















const ContentLayout = external_styled_components_default().div.withConfig({
    componentId: "sc-4f3c89b-0"
})`
  margin-top: 16px;
  display: grid;
  grid-template-columns: 260px 1fr;
  grid-gap: 1em;
  @media screen and (max-width: 800px) {
    grid-template-columns: 1fr;
    grid-template-rows: 1fr 1fr;
  }
`;
const StyledCMCLink = external_styled_components_default()(uikit_.Link).withConfig({
    componentId: "sc-4f3c89b-1"
})`
  width: 24px;
  height: 24px;
  margin-right: 8px;

  & :hover {
    opacity: 0.8;
  }
`;
const DEFAULT_TIME_WINDOW = {
    weeks: 1
};
const TokenPage = ({ routeAddress  })=>{
    const { isXs , isSm  } = (0,uikit_.useMatchBreakpoints)();
    const { t  } = (0,Localization/* useTranslation */.$G)();
    // In case somebody pastes checksummed address into url (since GraphQL expects lowercase address)
    const address = routeAddress.toLowerCase();
    const cmcLink = hooks_useCMCLink(address);
    const tokenData = (0,hooks/* useTokenData */.Ws)(address);
    const poolsForToken = (0,hooks/* usePoolsForToken */.CN)(address);
    const poolDatas = (0,hooks/* usePoolDatas */.zV)(poolsForToken ?? []);
    const transactions = (0,hooks/* useTokenTransactions */.Vq)(address);
    const chartData = (0,hooks/* useTokenChartData */.ku)(address);
    // pricing data
    const priceData = (0,hooks/* useTokenPriceData */.Q4)(address, info/* ONE_HOUR_SECONDS */.Tb, DEFAULT_TIME_WINDOW);
    const adjustedPriceData = (0,external_react_.useMemo)(()=>{
        // Include latest available price
        if (priceData && tokenData && priceData.length > 0) {
            return [
                ...priceData,
                {
                    time: new Date().getTime() / 1000,
                    open: priceData[priceData.length - 1].close,
                    close: tokenData?.priceUSD,
                    high: tokenData?.priceUSD,
                    low: priceData[priceData.length - 1].close
                }, 
            ];
        }
        return undefined;
    }, [
        priceData,
        tokenData
    ]);
    const [watchlistTokens, addWatchlistToken] = (0,user_hooks/* useWatchlistTokens */.z6)();
    return(/*#__PURE__*/ jsx_runtime_.jsx(Page/* default */.Z, {
        symbol: tokenData?.symbol,
        children: tokenData ? !tokenData.exists ? /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Card, {
            children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Box, {
                p: "16px",
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Text, {
                    children: [
                        t('No pool has been created with this token yet. Create one'),
                        /*#__PURE__*/ jsx_runtime_.jsx(NextLink/* NextLinkFromReactRouter */.a, {
                            style: {
                                display: 'inline',
                                marginLeft: '6px'
                            },
                            to: `/add/${address}`,
                            children: t('here.')
                        })
                    ]
                })
            })
        }) : /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
            children: [
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                    justifyContent: "space-between",
                    mb: "24px",
                    flexDirection: [
                        'column',
                        'column',
                        'row'
                    ],
                    children: [
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Breadcrumbs, {
                            mb: "32px",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx(NextLink/* NextLinkFromReactRouter */.a, {
                                    to: "/info",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                        color: "primary",
                                        children: t('Info')
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx(NextLink/* NextLinkFromReactRouter */.a, {
                                    to: "/info/tokens",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                        color: "primary",
                                        children: t('Tokens')
                                    })
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                            mr: "8px",
                                            children: tokenData.symbol
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                            children: `(${(0,truncateHash/* default */.Z)(address)})`
                                        })
                                    ]
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                            justifyContent: [
                                null,
                                null,
                                'flex-end'
                            ],
                            mt: [
                                '8px',
                                '8px',
                                0
                            ],
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx(uikit_.LinkExternal, {
                                    mr: "8px",
                                    color: "primary",
                                    href: (0,utils/* getBscScanLink */.s6)(address, 'address'),
                                    children: t('View on BscScan')
                                }),
                                cmcLink && /*#__PURE__*/ jsx_runtime_.jsx(StyledCMCLink, {
                                    href: cmcLink,
                                    rel: "noopener noreferrer nofollow",
                                    target: "_blank",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Image, {
                                        src: "/images/CMC-logo.svg",
                                        height: 22,
                                        width: 22,
                                        alt: t('View token on CoinMarketCap')
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx(SaveIcon/* default */.Z, {
                                    fill: watchlistTokens.includes(address),
                                    onClick: ()=>addWatchlistToken(address)
                                })
                            ]
                        })
                    ]
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                    justifyContent: "space-between",
                    flexDirection: [
                        'column',
                        'column',
                        'column',
                        'row'
                    ],
                    children: [
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                            flexDirection: "column",
                            mb: [
                                '8px',
                                null
                            ],
                            children: [
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                                    alignItems: "center",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx(CurrencyLogo/* CurrencyLogo */.X, {
                                            size: "32px",
                                            address: address
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                            ml: "12px",
                                            bold: true,
                                            lineHeight: "0.7",
                                            fontSize: isXs || isSm ? '24px' : '40px',
                                            id: "info-token-name-title",
                                            children: tokenData.name
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Text, {
                                            ml: "12px",
                                            lineHeight: "1",
                                            color: "textSubtle",
                                            fontSize: isXs || isSm ? '14px' : '20px',
                                            children: [
                                                "(",
                                                tokenData.symbol,
                                                ")"
                                            ]
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                                    mt: "8px",
                                    ml: "46px",
                                    alignItems: "center",
                                    children: [
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Text, {
                                            mr: "16px",
                                            bold: true,
                                            fontSize: "24px",
                                            children: [
                                                "$",
                                                (0,formatInfoNumbers/* formatAmount */.d)(tokenData.priceUSD, {
                                                    notation: 'standard'
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx(Percent/* default */.Z, {
                                            value: tokenData.priceUSDChange,
                                            fontWeight: 600
                                        })
                                    ]
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx(NextLink/* NextLinkFromReactRouter */.a, {
                                    to: `/add/${address}`,
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Button, {
                                        mr: "8px",
                                        variant: "secondary",
                                        children: t('Add Liquidity')
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx(NextLink/* NextLinkFromReactRouter */.a, {
                                    to: `/swap?inputCurrency=${address}`,
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Button, {
                                        children: t('Trade')
                                    })
                                })
                            ]
                        })
                    ]
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)(ContentLayout, {
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Card, {
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Box, {
                                p: "24px",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                        bold: true,
                                        small: true,
                                        color: "secondary",
                                        fontSize: "12px",
                                        textTransform: "uppercase",
                                        children: t('Liquidity')
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Text, {
                                        bold: true,
                                        fontSize: "24px",
                                        children: [
                                            "$",
                                            (0,formatInfoNumbers/* formatAmount */.d)(tokenData.liquidityUSD)
                                        ]
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx(Percent/* default */.Z, {
                                        value: tokenData.liquidityUSDChange
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                        mt: "24px",
                                        bold: true,
                                        color: "secondary",
                                        fontSize: "12px",
                                        textTransform: "uppercase",
                                        children: t('Volume 24H')
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Text, {
                                        bold: true,
                                        fontSize: "24px",
                                        textTransform: "uppercase",
                                        children: [
                                            "$",
                                            (0,formatInfoNumbers/* formatAmount */.d)(tokenData.volumeUSD)
                                        ]
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx(Percent/* default */.Z, {
                                        value: tokenData.volumeUSDChange
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                        mt: "24px",
                                        bold: true,
                                        color: "secondary",
                                        fontSize: "12px",
                                        textTransform: "uppercase",
                                        children: t('Volume 7D')
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Text, {
                                        bold: true,
                                        fontSize: "24px",
                                        children: [
                                            "$",
                                            (0,formatInfoNumbers/* formatAmount */.d)(tokenData.volumeUSDWeek)
                                        ]
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                        mt: "24px",
                                        bold: true,
                                        color: "secondary",
                                        fontSize: "12px",
                                        textTransform: "uppercase",
                                        children: t('Transactions 24H')
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                        bold: true,
                                        fontSize: "24px",
                                        children: (0,formatInfoNumbers/* formatAmount */.d)(tokenData.txCount, {
                                            isInteger: true
                                        })
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(ChartCard/* default */.Z, {
                            variant: "token",
                            chartData: chartData,
                            tokenData: tokenData,
                            tokenPriceData: adjustedPriceData
                        })
                    ]
                }),
                /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Heading, {
                    scale: "lg",
                    mb: "16px",
                    mt: "40px",
                    children: t('Pools')
                }),
                /*#__PURE__*/ jsx_runtime_.jsx(PoolsTable/* default */.Z, {
                    poolDatas: poolDatas
                }),
                /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Heading, {
                    scale: "lg",
                    mb: "16px",
                    mt: "40px",
                    children: t('Transactions')
                }),
                /*#__PURE__*/ jsx_runtime_.jsx(TransactionsTable/* default */.Z, {
                    transactions: transactions
                })
            ]
        }) : /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Flex, {
            mt: "80px",
            justifyContent: "center",
            children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Spinner, {})
        })
    }));
};
/* harmony default export */ const Tokens_TokenPage = (TokenPage);

// EXTERNAL MODULE: ./src/views/Info/index.tsx + 13 modules
var Info = __webpack_require__(7661);
;// CONCATENATED MODULE: ./src/pages/info/tokens/[address].tsx





const _address_TokenPage = ({ address  })=>{
    if (!address) {
        return null;
    }
    return(/*#__PURE__*/ jsx_runtime_.jsx(Tokens_TokenPage, {
        routeAddress: address
    }));
};
_address_TokenPage.Layout = Info/* InfoPageLayout */.O;
/* harmony default export */ const _address_ = (_address_TokenPage);
const getStaticPaths = ()=>{
    return {
        paths: [],
        fallback: true
    };
};
const getStaticProps = async ({ params  })=>{
    const address = params?.address;
    // In case somebody pastes checksummed address into url (since GraphQL expects lowercase address)
    if (!address || !(0,utils/* isAddress */.UJ)(String(address).toLowerCase())) {
        return {
            redirect: {
                destination: '/',
                permanent: false
            }
        };
    }
    return {
        props: {
            address
        }
    };
};


/***/ }),

/***/ 6187:
/***/ ((module) => {

module.exports = require("@ethersproject/abi");

/***/ }),

/***/ 1541:
/***/ ((module) => {

module.exports = require("@ethersproject/address");

/***/ }),

/***/ 5757:
/***/ ((module) => {

module.exports = require("@ethersproject/bignumber");

/***/ }),

/***/ 9935:
/***/ ((module) => {

module.exports = require("@ethersproject/bytes");

/***/ }),

/***/ 6644:
/***/ ((module) => {

module.exports = require("@ethersproject/constants");

/***/ }),

/***/ 2792:
/***/ ((module) => {

module.exports = require("@ethersproject/contracts");

/***/ }),

/***/ 399:
/***/ ((module) => {

module.exports = require("@ethersproject/providers");

/***/ }),

/***/ 9213:
/***/ ((module) => {

module.exports = require("@ethersproject/strings");

/***/ }),

/***/ 3138:
/***/ ((module) => {

module.exports = require("@ethersproject/units");

/***/ }),

/***/ 543:
/***/ ((module) => {

module.exports = require("@pancakeswap/sdk");

/***/ }),

/***/ 2829:
/***/ ((module) => {

module.exports = require("@pancakeswap/uikit");

/***/ }),

/***/ 5184:
/***/ ((module) => {

module.exports = require("@reduxjs/toolkit");

/***/ }),

/***/ 7248:
/***/ ((module) => {

module.exports = require("@snapshot-labs/snapshot.js");

/***/ }),

/***/ 1554:
/***/ ((module) => {

module.exports = require("@uniswap/token-lists");

/***/ }),

/***/ 8054:
/***/ ((module) => {

module.exports = require("@web3-react/core");

/***/ }),

/***/ 4215:
/***/ ((module) => {

module.exports = require("bignumber.js");

/***/ }),

/***/ 899:
/***/ ((module) => {

module.exports = require("bignumber.js/bignumber");

/***/ }),

/***/ 4146:
/***/ ((module) => {

module.exports = require("date-fns");

/***/ }),

/***/ 4175:
/***/ ((module) => {

module.exports = require("fast-json-stable-stringify");

/***/ }),

/***/ 5805:
/***/ ((module) => {

module.exports = require("graphql-request");

/***/ }),

/***/ 221:
/***/ ((module) => {

module.exports = require("lodash/chunk");

/***/ }),

/***/ 8190:
/***/ ((module) => {

module.exports = require("lodash/flatMap");

/***/ }),

/***/ 8579:
/***/ ((module) => {

module.exports = require("lodash/flatten");

/***/ }),

/***/ 9699:
/***/ ((module) => {

module.exports = require("lodash/isEmpty");

/***/ }),

/***/ 3385:
/***/ ((module) => {

module.exports = require("lodash/keyBy");

/***/ }),

/***/ 1341:
/***/ ((module) => {

module.exports = require("lodash/maxBy");

/***/ }),

/***/ 1831:
/***/ ((module) => {

module.exports = require("lodash/merge");

/***/ }),

/***/ 4042:
/***/ ((module) => {

module.exports = require("lodash/range");

/***/ }),

/***/ 7657:
/***/ ((module) => {

module.exports = require("lodash/sample");

/***/ }),

/***/ 8459:
/***/ ((module) => {

module.exports = require("lodash/uniq");

/***/ }),

/***/ 562:
/***/ ((module) => {

module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 5832:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/loadable.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ 8032:
/***/ ((module) => {

module.exports = require("numeral");

/***/ }),

/***/ 9819:
/***/ ((module) => {

module.exports = require("querystring");

/***/ }),

/***/ 6689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 6022:
/***/ ((module) => {

module.exports = require("react-redux");

/***/ }),

/***/ 997:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 3655:
/***/ ((module) => {

module.exports = require("recharts");

/***/ }),

/***/ 4161:
/***/ ((module) => {

module.exports = require("redux-persist");

/***/ }),

/***/ 8936:
/***/ ((module) => {

module.exports = require("redux-persist/lib/storage");

/***/ }),

/***/ 7518:
/***/ ((module) => {

module.exports = require("styled-components");

/***/ }),

/***/ 549:
/***/ ((module) => {

module.exports = require("swr");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [730,664,152,308,624,601,54,589,24,337,838,994,275], () => (__webpack_exec__(7899)));
module.exports = __webpack_exports__;

})();
//# sourceMappingURL=[address].js.map