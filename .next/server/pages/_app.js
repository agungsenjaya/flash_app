(() => {
var exports = {};
exports.id = 888;
exports.ids = [888];
exports.modules = {

/***/ 9484:
/***/ (() => {

const _global = (typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {}); _global.SENTRY_RELEASE={id:"BAcPYM_URjtJo4msILWkT"};

/***/ }),

/***/ 5561:
/***/ ((__unused_webpack_module, __unused_webpack___webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony import */ var _sentry_nextjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8097);
/* harmony import */ var _sentry_nextjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_sentry_nextjs__WEBPACK_IMPORTED_MODULE_0__);
// This file configures the initialization of Sentry on the server.
// The config you add here will be used whenever the server handles a request.
// https://docs.sentry.io/platforms/javascript/guides/nextjs/

const SENTRY_DSN = process.env.SENTRY_DSN || process.env.NEXT_PUBLIC_SENTRY_DSN;
_sentry_nextjs__WEBPACK_IMPORTED_MODULE_0__.init({
    dsn: SENTRY_DSN || 'https://ed98e16b9d704c22bef92d24bdd5f3b7@o1092725.ingest.sentry.io/6111410',
    // Adjust this value in production, or use tracesSampler for greater control
    tracesSampleRate: 0.1
});


/***/ }),

/***/ 3629:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "a": () => (/* binding */ NextLinkFromReactRouter)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1664);




const A = styled_components__WEBPACK_IMPORTED_MODULE_2___default().a.withConfig({
    componentId: "sc-c6707b7e-0"
})``;
/**
 * temporary solution for migrating React Router to Next.js Link
 */ const NextLinkFromReactRouter = /*#__PURE__*/ (0,react__WEBPACK_IMPORTED_MODULE_1__.forwardRef)(({ to , replace , children , prefetch , ...props }, ref)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_3__["default"], {
        href: to,
        replace: replace,
        passHref: true,
        prefetch: prefetch,
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(A, {
            ref: ref,
            ...props,
            children: children
        })
    })
);


/***/ }),

/***/ 2420:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ _app)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "@pancakeswap/uikit"
var uikit_ = __webpack_require__(2829);
;// CONCATENATED MODULE: external "next/script"
const script_namespaceObject = require("next/script");
var script_default = /*#__PURE__*/__webpack_require__.n(script_namespaceObject);
// EXTERNAL MODULE: external "bignumber.js"
var external_bignumber_js_ = __webpack_require__(4215);
var external_bignumber_js_default = /*#__PURE__*/__webpack_require__.n(external_bignumber_js_);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);
;// CONCATENATED MODULE: ./src/components/EasterEgg/EasterEgg.tsx



const EasterEgg = (props)=>{
    const { 0: show , 1: setShow  } = (0,external_react_.useState)(false);
    const startFalling = (0,external_react_.useCallback)(()=>setShow(true)
    , [
        setShow
    ]);
    (0,uikit_.useKonamiCheatCode)(startFalling);
    if (show) {
        return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
            onAnimationEnd: ()=>setShow(false)
            ,
            children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.FallingBunnies, {
                ...props
            })
        }));
    }
    return null;
};
/* harmony default export */ const EasterEgg_EasterEgg = (/*#__PURE__*/external_react_default().memo(EasterEgg));

;// CONCATENATED MODULE: ./src/components/EasterEgg/index.ts


// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
// EXTERNAL MODULE: external "@web3-react/core"
var core_ = __webpack_require__(8054);
// EXTERNAL MODULE: ./node_modules/next/dynamic.js
var dynamic = __webpack_require__(5152);
// EXTERNAL MODULE: ./src/utils/contractHelpers.ts + 22 modules
var contractHelpers = __webpack_require__(1553);
;// CONCATENATED MODULE: ./src/components/GlobalCheckClaimStatus/index.tsx







const AnniversaryAchievementModal = (0,dynamic["default"])(null, {
    loadableGenerated: {
        modules: [
            "../components/GlobalCheckClaimStatus/index.tsx -> " + "./AnniversaryAchievementModal"
        ]
    },
    ssr: false
});
// change it to true if we have events to check claim status
const enable = false;
const GlobalCheckClaimStatus = (props)=>{
    if (!enable) {
        return null;
    }
    return(/*#__PURE__*/ jsx_runtime_.jsx(GlobalCheckClaim, {
        ...props
    }));
};
/**
 * This is represented as a component rather than a hook because we need to keep it
 * inside the Router.
 *
 * TODO: Put global checks in redux or make a generic area to house global checks
 */ const GlobalCheckClaim = ({ excludeLocations  })=>{
    const hasDisplayedModal = (0,external_react_.useRef)(false);
    const { 0: canClaimAnniversaryPoints , 1: setCanClaimAnniversaryPoints  } = (0,external_react_.useState)(false);
    const { account  } = (0,core_.useWeb3React)();
    const { pathname  } = (0,router_.useRouter)();
    const [onPresentAnniversaryModal] = (0,uikit_.useModal)(/*#__PURE__*/ jsx_runtime_.jsx(AnniversaryAchievementModal, {}));
    // Check claim status
    (0,external_react_.useEffect)(()=>{
        const fetchClaimAnniversaryStatus = async ()=>{
            const { canClaim  } = (0,contractHelpers/* getAnniversaryAchievementContract */.f$)();
            const canClaimAnniversary = await canClaim(account);
            setCanClaimAnniversaryPoints(canClaimAnniversary);
        };
        if (account) {
            fetchClaimAnniversaryStatus();
        }
    }, [
        account
    ]);
    // Check if we need to display the modal
    (0,external_react_.useEffect)(()=>{
        const matchesSomeLocations = excludeLocations.some((location)=>pathname.includes(location)
        );
        if (canClaimAnniversaryPoints && !matchesSomeLocations && !hasDisplayedModal.current) {
            onPresentAnniversaryModal();
            hasDisplayedModal.current = true;
        }
    }, [
        pathname,
        excludeLocations,
        hasDisplayedModal,
        onPresentAnniversaryModal,
        canClaimAnniversaryPoints
    ]);
    // Reset the check flag when account changes
    (0,external_react_.useEffect)(()=>{
        hasDisplayedModal.current = false;
    }, [
        account,
        hasDisplayedModal
    ]);
    return null;
};
/* harmony default export */ const components_GlobalCheckClaimStatus = (GlobalCheckClaimStatus);

// EXTERNAL MODULE: ./src/config/index.ts
var config = __webpack_require__(3206);
// EXTERNAL MODULE: ./src/contexts/Localization/index.tsx + 3 modules
var Localization = __webpack_require__(9150);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: ./src/state/user/hooks/index.tsx
var hooks = __webpack_require__(8605);
// EXTERNAL MODULE: external "graphql-request"
var external_graphql_request_ = __webpack_require__(5805);
// EXTERNAL MODULE: ./src/config/constants/endpoints.ts
var endpoints = __webpack_require__(5906);
// EXTERNAL MODULE: ./src/utils/providers.ts
var providers = __webpack_require__(5922);
// EXTERNAL MODULE: ./src/config/constants/index.ts
var constants = __webpack_require__(3862);
// EXTERNAL MODULE: external "swr"
var external_swr_ = __webpack_require__(549);
var external_swr_default = /*#__PURE__*/__webpack_require__.n(external_swr_);
;// CONCATENATED MODULE: ./src/hooks/useRefreshEffect.ts



function useFastRefreshEffect(effect, deps) {
    const { data =0  } = external_swr_default()([
        constants/* FAST_INTERVAL */.sR,
        'blockNumber'
    ]);
    const depsMemo = (0,external_react_.useMemo)(()=>[
            data,
            ...deps || []
        ]
    , [
        data,
        deps
    ]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    (0,external_react_.useEffect)(effect, depsMemo);
}
function useRefreshEffect_useSlowRefreshEffect(effect, deps) {
    const { data =0  } = external_swr_default()([
        constants/* SLOW_INTERVAL */.KI,
        'blockNumber'
    ]);
    const depsMemo = (0,external_react_.useMemo)(()=>[
            data,
            ...deps || []
        ]
    , [
        data,
        deps
    ]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    (0,external_react_.useEffect)(effect, depsMemo);
}

;// CONCATENATED MODULE: ./src/hooks/useSubgraphHealth.ts





var SubgraphStatus;
(function(SubgraphStatus) {
    SubgraphStatus[SubgraphStatus["OK"] = 0] = "OK";
    SubgraphStatus[SubgraphStatus["WARNING"] = 1] = "WARNING";
    SubgraphStatus[SubgraphStatus["NOT_OK"] = 2] = "NOT_OK";
    SubgraphStatus[SubgraphStatus["UNKNOWN"] = 3] = "UNKNOWN";
})(SubgraphStatus || (SubgraphStatus = {}));
const NOT_OK_BLOCK_DIFFERENCE = 200 // ~15 minutes delay
;
const WARNING_BLOCK_DIFFERENCE = 50 // ~2.5 minute delay
;
const useSubgraphHealth = ()=>{
    const { 0: sgHealth , 1: setSgHealth  } = (0,external_react_.useState)({
        status: SubgraphStatus.UNKNOWN,
        currentBlock: 0,
        chainHeadBlock: 0,
        latestBlock: 0,
        blockDifference: 0
    });
    useRefreshEffect_useSlowRefreshEffect(()=>{
        const getSubgraphHealth = async ()=>{
            try {
                const { indexingStatusForCurrentVersion  } = await (0,external_graphql_request_.request)(endpoints/* GRAPH_HEALTH */.AM, external_graphql_request_.gql`
            query getNftMarketSubgraphHealth {
              indexingStatusForCurrentVersion(subgraphName: "pancakeswap/nft-market") {
                synced
                health
                chains {
                  chainHeadBlock {
                    number
                  }
                  latestBlock {
                    number
                  }
                }
              }
            }
          `);
                const currentBlock = await providers/* simpleRpcProvider.getBlockNumber */.J.getBlockNumber();
                const isHealthy = indexingStatusForCurrentVersion.health === 'healthy';
                const chainHeadBlock = parseInt(indexingStatusForCurrentVersion.chains[0].chainHeadBlock.number);
                const latestBlock = parseInt(indexingStatusForCurrentVersion.chains[0].latestBlock.number);
                const blockDifference = currentBlock - latestBlock;
                // Sometimes subgraph might report old block as chainHeadBlock, so its important to compare
                // it with block retrieved from simpleRpcProvider.getBlockNumber()
                const chainHeadBlockDifference = currentBlock - chainHeadBlock;
                if (!isHealthy || blockDifference > NOT_OK_BLOCK_DIFFERENCE || chainHeadBlockDifference > NOT_OK_BLOCK_DIFFERENCE) {
                    setSgHealth({
                        status: SubgraphStatus.NOT_OK,
                        currentBlock,
                        chainHeadBlock,
                        latestBlock,
                        blockDifference
                    });
                } else if (blockDifference > WARNING_BLOCK_DIFFERENCE || chainHeadBlockDifference > WARNING_BLOCK_DIFFERENCE) {
                    setSgHealth({
                        status: SubgraphStatus.WARNING,
                        currentBlock,
                        chainHeadBlock,
                        latestBlock,
                        blockDifference
                    });
                } else {
                    setSgHealth({
                        status: SubgraphStatus.OK,
                        currentBlock,
                        chainHeadBlock,
                        latestBlock,
                        blockDifference
                    });
                }
            } catch (error) {
                console.error('Failed to perform health check for NFT Market subgraph', error);
            }
        };
        getSubgraphHealth();
    }, []);
    return sgHealth;
};
/* harmony default export */ const hooks_useSubgraphHealth = (useSubgraphHealth);

;// CONCATENATED MODULE: ./src/components/SubgraphHealthIndicator/index.tsx









const StyledCard = external_styled_components_default()(uikit_.Card).withConfig({
    componentId: "sc-36ae2c02-0"
})`
  border-radius: 8px;
  > div {
    border-radius: 8px;
  }
`;
const IndicatorWrapper = external_styled_components_default()(uikit_.Box).withConfig({
    componentId: "sc-36ae2c02-1"
})`
  display: flex;
  align-items: center;
  gap: 7px;
`;
const Dot = external_styled_components_default()(uikit_.Box).withConfig({
    componentId: "sc-36ae2c02-2"
})`
  width: 12px;
  height: 12px;
  border-radius: 50%;
  background: ${({ $color , theme  })=>theme.colors[$color]
};
`;
const indicator = (t)=>({
        delayed: {
            label: t('Delayed'),
            color: 'failure',
            description: t('Subgraph is currently experiencing delays due to BSC issues. Performance may suffer until subgraph is restored.')
        },
        slow: {
            label: t('Slight delay'),
            color: 'warning',
            description: t('Subgraph is currently experiencing delays due to BSC issues. Performance may suffer until subgraph is restored.')
        },
        healthy: {
            label: t('Fast'),
            color: 'success',
            description: t('No issues with the subgraph.')
        }
    })
;
const getIndicator = (sgStatus)=>{
    if (sgStatus === SubgraphStatus.NOT_OK) {
        return 'delayed';
    }
    if (sgStatus === SubgraphStatus.WARNING) {
        return 'slow';
    }
    return 'healthy';
};
const SubgraphHealthIndicator = ()=>{
    const { pathname  } = (0,router_.useRouter)();
    const isOnNftPages = pathname.includes('nfts');
    return isOnNftPages ? /*#__PURE__*/ jsx_runtime_.jsx(SubgraphHealth, {}) : null;
};
const SubgraphHealth = ()=>{
    const { t  } = (0,Localization/* useTranslation */.$G)();
    const { status , currentBlock , blockDifference , latestBlock  } = hooks_useSubgraphHealth();
    const [alwaysShowIndicator] = (0,hooks/* useSubgraphHealthIndicatorManager */.YF)();
    const forceIndicatorDisplay = status === SubgraphStatus.WARNING || status === SubgraphStatus.NOT_OK;
    const showIndicator = alwaysShowIndicator || forceIndicatorDisplay;
    const indicatorProps = indicator(t);
    const secondRemainingBlockSync = blockDifference * config/* BSC_BLOCK_TIME */.hJ;
    const indicatorValue = getIndicator(status);
    const current = indicatorProps[indicatorValue];
    const { targetRef , tooltipVisible , tooltip  } = (0,uikit_.useTooltip)(/*#__PURE__*/ jsx_runtime_.jsx(TooltipContent, {
        currentBlock: currentBlock,
        secondRemainingBlockSync: secondRemainingBlockSync,
        blockNumberFromSubgraph: latestBlock,
        ...current
    }), {
        placement: 'top'
    });
    if (!latestBlock || !currentBlock || !showIndicator) {
        return null;
    }
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Box, {
        position: "fixed",
        bottom: "55px",
        right: "5%",
        ref: targetRef,
        "data-test": "subgraph-health-indicator",
        children: [
            tooltipVisible && tooltip,
            /*#__PURE__*/ jsx_runtime_.jsx(StyledCard, {
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(IndicatorWrapper, {
                    p: "10px",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx(Dot, {
                            $color: current.color
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                            children: current.label
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.InfoIcon, {})
                    ]
                })
            })
        ]
    }));
};
const TooltipContent = ({ color , label , description , currentBlock , secondRemainingBlockSync , blockNumberFromSubgraph ,  })=>{
    const { t  } = (0,Localization/* useTranslation */.$G)();
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Box, {
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(IndicatorWrapper, {
                pb: "10px",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(Dot, {
                        $color: color
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                        children: label
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                pb: "24px",
                children: description
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Text, {
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("strong", {
                        children: [
                            t('Chain Head Block'),
                            ":"
                        ]
                    }),
                    " ",
                    currentBlock
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Text, {
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("strong", {
                        children: [
                            t('Latest Subgraph Block'),
                            ":"
                        ]
                    }),
                    " ",
                    blockNumberFromSubgraph
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Text, {
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("strong", {
                        children: [
                            t('Delay'),
                            ":"
                        ]
                    }),
                    " ",
                    currentBlock - blockNumberFromSubgraph,
                    " (",
                    secondRemainingBlockSync,
                    "s)"
                ]
            })
        ]
    }));
};
/* harmony default export */ const components_SubgraphHealthIndicator = (SubgraphHealthIndicator);

// EXTERNAL MODULE: ./src/contexts/ToastsContext/index.tsx + 2 modules
var ToastsContext = __webpack_require__(5083);
// EXTERNAL MODULE: ./src/hooks/useAuth.ts
var useAuth = __webpack_require__(8470);
;// CONCATENATED MODULE: ./src/hooks/useEagerConnect.ts



const _binanceChainListener = async ()=>new Promise((resolve)=>Object.defineProperty(window, 'BinanceChain', {
            get () {
                return this.bsc;
            },
            set (bsc) {
                this.bsc = bsc;
                resolve();
            }
        })
    )
;
const useEagerConnect = ()=>{
    const { login  } = (0,useAuth/* default */.Z)();
    (0,external_react_.useEffect)(()=>{
        const connectorId = window.localStorage.getItem(uikit_.connectorLocalStorageKey);
        if (connectorId) {
            const isConnectorBinanceChain = connectorId === uikit_.ConnectorNames.BSC;
            const isBinanceChainDefined = Reflect.has(window, 'BinanceChain');
            // Currently BSC extension doesn't always inject in time.
            // We must check to see if it exists, and if not, wait for it before proceeding.
            if (isConnectorBinanceChain && !isBinanceChainDefined) {
                _binanceChainListener().then(()=>login(connectorId)
                );
                return;
            }
            login(connectorId);
        }
    }, [
        login
    ]);
};
/* harmony default export */ const hooks_useEagerConnect = (useEagerConnect);

// EXTERNAL MODULE: ./src/state/index.ts + 41 modules
var state = __webpack_require__(4211);
// EXTERNAL MODULE: ./src/utils/clearUserStates.ts
var clearUserStates = __webpack_require__(8382);
;// CONCATENATED MODULE: ./src/hooks/useInactiveListener.ts




const useInactiveListener = ()=>{
    const { account , chainId , connector  } = (0,core_.useWeb3React)();
    const dispatch = (0,state/* useAppDispatch */.TL)();
    (0,external_react_.useEffect)(()=>{
        if (account && connector) {
            const handleDeactivate = ()=>{
                (0,clearUserStates/* clearUserStates */.J)(dispatch, chainId);
            };
            connector.addListener('Web3ReactDeactivate', handleDeactivate);
            return ()=>{
                connector.removeListener('Web3ReactDeactivate', handleDeactivate);
            };
        }
        return undefined;
    }, [
        account,
        chainId,
        dispatch,
        connector
    ]);
};

// EXTERNAL MODULE: external "@sentry/react"
var react_ = __webpack_require__(5427);
// EXTERNAL MODULE: ./src/hooks/useActiveWeb3React.ts
var useActiveWeb3React = __webpack_require__(4011);
;// CONCATENATED MODULE: ./src/hooks/useSentryUser.ts



function useSentryUser() {
    const { account  } = (0,useActiveWeb3React/* default */.Z)();
    (0,external_react_.useEffect)(()=>{
        if (account) {
            react_.setUser({
                account
            });
        }
    }, [
        account
    ]);
}
/* harmony default export */ const hooks_useSentryUser = (useSentryUser);

;// CONCATENATED MODULE: ./src/hooks/useUserAgent.ts

const useUserAgent = ()=>{
    (0,external_react_.useEffect)(()=>{
        document.documentElement.setAttribute('data-useragent', navigator.userAgent);
    }, []);
};
/* harmony default export */ const hooks_useUserAgent = (useUserAgent);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(968);
var head_default = /*#__PURE__*/__webpack_require__.n(head_);
;// CONCATENATED MODULE: external "redux-persist/integration/react"
const integration_react_namespaceObject = require("redux-persist/integration/react");
// EXTERNAL MODULE: ./src/state/block/hooks.ts
var block_hooks = __webpack_require__(7063);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(6022);
// EXTERNAL MODULE: ./src/state/user/hooks/helpers.ts
var helpers = __webpack_require__(787);
// EXTERNAL MODULE: ./src/utils/bigNumber.ts
var bigNumber = __webpack_require__(5128);
// EXTERNAL MODULE: ./src/utils/formatBalance.ts
var formatBalance = __webpack_require__(5044);
// EXTERNAL MODULE: ./src/state/farms/index.ts + 8 modules
var farms = __webpack_require__(7802);
;// CONCATENATED MODULE: ./src/state/farms/hooks.ts











const deserializeFarmUserData = (farm)=>{
    return {
        allowance: farm.userData ? new (external_bignumber_js_default())(farm.userData.allowance) : bigNumber/* BIG_ZERO */.HW,
        tokenBalance: farm.userData ? new (external_bignumber_js_default())(farm.userData.tokenBalance) : bigNumber/* BIG_ZERO */.HW,
        stakedBalance: farm.userData ? new (external_bignumber_js_default())(farm.userData.stakedBalance) : bigNumber/* BIG_ZERO */.HW,
        earnings: farm.userData ? new (external_bignumber_js_default())(farm.userData.earnings) : bigNumber/* BIG_ZERO */.HW
    };
};
const deserializeFarm = (farm)=>{
    const { lpAddresses , lpSymbol , pid , dual , multiplier , isCommunity , quoteTokenPriceBusd , tokenPriceBusd  } = farm;
    return {
        lpAddresses,
        lpSymbol,
        pid,
        dual,
        multiplier,
        isCommunity,
        quoteTokenPriceBusd,
        tokenPriceBusd,
        token: (0,helpers/* deserializeToken */.iG)(farm.token),
        quoteToken: (0,helpers/* deserializeToken */.iG)(farm.quoteToken),
        userData: deserializeFarmUserData(farm),
        tokenAmountTotal: farm.tokenAmountTotal ? new (external_bignumber_js_default())(farm.tokenAmountTotal) : bigNumber/* BIG_ZERO */.HW,
        lpTotalInQuoteToken: farm.lpTotalInQuoteToken ? new (external_bignumber_js_default())(farm.lpTotalInQuoteToken) : bigNumber/* BIG_ZERO */.HW,
        lpTotalSupply: farm.lpTotalSupply ? new (external_bignumber_js_default())(farm.lpTotalSupply) : bigNumber/* BIG_ZERO */.HW,
        tokenPriceVsQuote: farm.tokenPriceVsQuote ? new (external_bignumber_js_default())(farm.tokenPriceVsQuote) : bigNumber/* BIG_ZERO */.HW,
        poolWeight: farm.poolWeight ? new (external_bignumber_js_default())(farm.poolWeight) : bigNumber/* BIG_ZERO */.HW
    };
};
const usePollFarmsPublicData = (includeArchive = false)=>{
    const dispatch = useAppDispatch();
    useSlowRefreshEffect(()=>{
        const farmsToFetch = includeArchive ? farmsConfig : nonArchivedFarms;
        const pids = farmsToFetch.map((farmToFetch)=>farmToFetch.pid
        );
        dispatch(fetchFarmsPublicDataAsync(pids));
    }, [
        includeArchive,
        dispatch
    ]);
};
const usePollFarmsWithUserData = (includeArchive = false)=>{
    const dispatch = useAppDispatch();
    const { account  } = useWeb3React();
    useSlowRefreshEffect(()=>{
        const farmsToFetch = includeArchive ? farmsConfig : nonArchivedFarms;
        const pids = farmsToFetch.map((farmToFetch)=>farmToFetch.pid
        );
        dispatch(fetchFarmsPublicDataAsync(pids));
        if (account) {
            dispatch(fetchFarmUserDataAsync({
                account,
                pids
            }));
        }
    }, [
        includeArchive,
        dispatch,
        account
    ]);
};
/**
 * Fetches the "core" farm data used globally
 * 251 = CAKE-BNB LP
 * 252 = BUSD-BNB LP
 */ const usePollCoreFarmData = ()=>{
    const dispatch = (0,state/* useAppDispatch */.TL)();
    useFastRefreshEffect(()=>{
        dispatch((0,farms/* fetchFarmsPublicDataAsync */.eG)([
            251,
            252
        ]));
    }, [
        dispatch
    ]);
};
const useFarms = ()=>{
    const farms = useSelector((state)=>state.farms
    );
    const deserializedFarmsData = farms.data.map(deserializeFarm);
    const { loadArchivedFarmsData , userDataLoaded  } = farms;
    return {
        loadArchivedFarmsData,
        userDataLoaded,
        data: deserializedFarmsData
    };
};
const useFarmFromPid = (pid)=>{
    const farm = (0,external_react_redux_.useSelector)((state)=>state.farms.data.find((f)=>f.pid === pid
        )
    );
    return deserializeFarm(farm);
};
const useFarmFromLpSymbol = (lpSymbol)=>{
    const farm = useSelector((state)=>state.farms.data.find((f)=>f.lpSymbol === lpSymbol
        )
    );
    return deserializeFarm(farm);
};
const useFarmUser = (pid)=>{
    const { userData  } = useFarmFromPid(pid);
    const { allowance , tokenBalance , stakedBalance , earnings  } = userData;
    return {
        allowance,
        tokenBalance,
        stakedBalance,
        earnings
    };
};
// Return the base token price for a farm, from a given pid
const useBusdPriceFromPid = (pid)=>{
    const farm = useFarmFromPid(pid);
    return farm && new BigNumber(farm.tokenPriceBusd);
};
const useLpTokenPrice = (symbol)=>{
    const farm = useFarmFromLpSymbol(symbol);
    const farmTokenPriceInUsd = useBusdPriceFromPid(farm.pid);
    let lpTokenPrice = BIG_ZERO;
    if (farm.lpTotalSupply.gt(0) && farm.lpTotalInQuoteToken.gt(0)) {
        // Total value of base token in LP
        const valueOfBaseTokenInFarm = farmTokenPriceInUsd.times(farm.tokenAmountTotal);
        // Double it to get overall value in LP
        const overallValueOfAllTokensInFarm = valueOfBaseTokenInFarm.times(2);
        // Divide total value of all tokens, by the number of LP tokens
        const totalLpTokens = getBalanceAmount(farm.lpTotalSupply);
        lpTokenPrice = overallValueOfAllTokensInFarm.div(totalLpTokens);
    }
    return lpTokenPrice;
};
/**
 * @@deprecated use the BUSD hook in /hooks
 */ const usePriceCakeBusd = ()=>{
    const cakeBnbFarm = useFarmFromPid(251);
    const cakePriceBusdAsString = cakeBnbFarm.tokenPriceBusd;
    const cakePriceBusd = (0,external_react_.useMemo)(()=>{
        return new (external_bignumber_js_default())(cakePriceBusdAsString);
    }, [
        cakePriceBusdAsString
    ]);
    return cakePriceBusd;
};

// EXTERNAL MODULE: ./src/utils/index.ts + 1 modules
var utils = __webpack_require__(8328);
;// CONCATENATED MODULE: ./src/config/constants/campaigns.ts
/**
 * id: The campaign id (required)
 * type: The type of the achievement
 * title: A string or an object to be translated.
 * Note: If the value is a string it is likely used as data in a translation object
 *
 * badge: Achievement avatar
 */ const campaigns = [
    {
        id: '511110000',
        type: 'ifo',
        title: 'Kalmar',
        badge: 'ifo-kalm.svg'
    },
    {
        id: '511100000',
        type: 'ifo',
        title: 'Hotcross',
        badge: 'ifo-hotcross.svg'
    },
    {
        id: '511090000',
        type: 'ifo',
        title: 'Horizon Protocol',
        badge: 'ifo-hzn.svg'
    },
    {
        id: '511080000',
        type: 'ifo',
        title: 'Belt',
        badge: 'ifo-belt.svg'
    },
    {
        id: '511070000',
        type: 'ifo',
        title: 'Yieldwatch',
        badge: 'ifo-watch.svg'
    },
    {
        id: '511060000',
        type: 'ifo',
        title: 'Berry',
        badge: 'ifo-bry.svg'
    },
    {
        id: '511050000',
        type: 'ifo',
        title: 'Soteria',
        badge: 'ifo-wsote.svg'
    },
    {
        id: '511040000',
        type: 'ifo',
        title: 'Helmet',
        badge: 'ifo-helmet.svg'
    },
    {
        id: '511030000',
        type: 'ifo',
        title: 'Tenet',
        badge: 'ifo-ten.svg'
    },
    {
        id: '511020000',
        type: 'ifo',
        title: 'Ditto',
        badge: 'ifo-ditto.svg'
    },
    {
        id: '511010000',
        type: 'ifo',
        title: 'Blink',
        badge: 'ifo-blink.svg'
    },
    {
        id: '512010001',
        type: 'teambattle',
        title: 'Easter Champion: Gold',
        badge: 'easter-champion-gold.svg'
    },
    {
        id: '512010002',
        type: 'teambattle',
        title: 'Easter Top 500: Gold',
        badge: 'easter-top-500-gold.svg'
    },
    {
        id: '512010003',
        type: 'teambattle',
        title: 'Easter Top 500: Gold',
        badge: 'easter-top-500-gold.svg'
    },
    {
        id: '512010004',
        type: 'teambattle',
        title: 'Easter Top 500: Gold',
        badge: 'easter-top-500-gold.svg'
    },
    {
        id: '512010005',
        type: 'teambattle',
        title: 'Easter Participant: Gold',
        badge: 'easter-participant-gold.svg'
    },
    {
        id: '512010006',
        type: 'teambattle',
        title: 'Easter Champion: Silver',
        badge: 'easter-champion-silver.svg'
    },
    {
        id: '512010007',
        type: 'teambattle',
        title: 'Easter Top 500: Silver',
        badge: 'easter-top-500-silver.svg'
    },
    {
        id: '512010008',
        type: 'teambattle',
        title: 'Easter Top 500: Silver',
        badge: 'easter-top-500-silver.svg'
    },
    {
        id: '512010009',
        type: 'teambattle',
        title: 'Easter Top 500: Silver',
        badge: 'easter-top-500-silver.svg'
    },
    {
        id: '512010010',
        type: 'teambattle',
        title: 'Easter Participant: Silver',
        badge: 'easter-participant-silver.svg'
    },
    {
        id: '512010011',
        type: 'teambattle',
        title: 'Easter Champion: Bronze',
        badge: 'easter-champion-bronze.svg'
    },
    {
        id: '512010012',
        type: 'teambattle',
        title: 'Easter Top 500: Bronze',
        badge: 'easter-top-500-bronze.svg'
    },
    {
        id: '512010013',
        type: 'teambattle',
        title: 'Easter Top 500: Bronze',
        badge: 'easter-top-500-bronze.svg'
    },
    {
        id: '512010014',
        type: 'teambattle',
        title: 'Easter Top 500: Bronze',
        badge: 'easter-top-500-bronze.svg'
    },
    {
        id: '512010015',
        type: 'teambattle',
        title: 'Easter Participant: Bronze',
        badge: 'easter-participant-bronze.svg'
    },
    {
        id: '513010001',
        type: 'participation',
        title: 'Syrup Soaker',
        description: 'Took a dip in the early days of the Auto CAKE Pool',
        badge: 'syrup-soaker.svg'
    },
    {
        id: '514010001',
        type: 'participation',
        title: 'Clairvoyant',
        description: 'Played a round of Prediction before round 12,120',
        badge: 'clairvoyant.svg'
    },
    {
        id: '515010001',
        type: 'participation',
        title: 'Lottie',
        description: 'Joined a round in the early days of Lottery V2',
        badge: 'lottie.svg'
    },
    {
        id: '515020001',
        type: 'participation',
        title: 'Lucky',
        description: 'Won a round in the early days of Lottery V2',
        badge: 'lucky.svg'
    },
    {
        id: '515030001',
        type: 'participation',
        title: 'Baller',
        description: 'Top 100 ticket buyer in the early days of Lottery V2',
        badge: 'baller.svg'
    },
    {
        id: '516010001',
        type: 'participation',
        title: '1 Year',
        description: 'Joined PancakeSwap during the first year of our journey!',
        badge: '1-year.svg'
    },
    {
        id: '511120000',
        type: 'ifo',
        title: 'Duelist King',
        badge: 'ifo-dkt.svg'
    },
    {
        id: '511130000',
        type: 'ifo',
        title: 'Mines of Dalarnia',
        badge: 'ifo-dar.svg'
    },
    {
        id: '511140000',
        type: 'ifo',
        title: 'FC Porto Fan Token',
        badge: 'ifo-porto.svg'
    },
    {
        id: '511150000',
        type: 'ifo',
        title: 'FC Santos Fan Token',
        badge: 'ifo-santos.svg'
    },
    {
        id: '512020001',
        type: 'teambattle',
        title: 'Fan Token Champion: Gold',
        badge: 'fan-token-champion-gold.svg'
    },
    {
        id: '512020002',
        type: 'teambattle',
        title: 'Fan Token Top 10: Gold',
        badge: 'fan-token-top-10-gold.svg'
    },
    {
        id: '512020003',
        type: 'teambattle',
        title: 'Fan Token Top 100: Gold',
        badge: 'fan-token-top-100-gold.svg'
    },
    {
        id: '512020004',
        type: 'teambattle',
        title: 'Fan Token Top 500: Gold',
        badge: 'fan-token-top-500-gold.svg'
    },
    {
        id: '512020005',
        type: 'teambattle',
        title: 'Fan Token Participant: Gold',
        badge: 'fan-token-participant-gold.svg'
    },
    {
        id: '512020006',
        type: 'teambattle',
        title: 'Fan Token Champion: Silver',
        badge: 'fan-token-champion-silver.svg'
    },
    {
        id: '512020007',
        type: 'teambattle',
        title: 'Fan Token Top 10: Silver',
        badge: 'fan-token-top-10-silver.svg'
    },
    {
        id: '512020008',
        type: 'teambattle',
        title: 'Fan Token Top 100: Silver',
        badge: 'fan-token-top-100-silver.svg'
    },
    {
        id: '512020009',
        type: 'teambattle',
        title: 'Fan Token Top 500: Silver',
        badge: 'fan-token-top-500-silver.svg'
    },
    {
        id: '512020010',
        type: 'teambattle',
        title: 'Fan Token Participant: Silver',
        badge: 'fan-token-participant-silver.svg'
    },
    {
        id: '512020011',
        type: 'teambattle',
        title: 'Fan Token Champion: Bronze',
        badge: 'fan-token-champion-bronze.svg'
    },
    {
        id: '512020012',
        type: 'teambattle',
        title: 'Fan Token Top 10: Bronze',
        badge: 'fan-token-top-10-bronze.svg'
    },
    {
        id: '512020013',
        type: 'teambattle',
        title: 'Fan Token Top 100: Bronze',
        badge: 'fan-token-top-100-bronze.svg'
    },
    {
        id: '512020014',
        type: 'teambattle',
        title: 'Fan Token Top 500: Bronze',
        badge: 'fan-token-top-500-bronze.svg'
    },
    {
        id: '512020015',
        type: 'teambattle',
        title: 'Fan Token Participant: Bronze',
        badge: 'fan-token-participant-bronze.svg'
    },
    {
        id: '511160000',
        type: 'ifo',
        title: 'Diviner Protocol',
        badge: 'ifo-dpt.svg'
    },
    {
        id: '511170000',
        type: 'ifo',
        title: 'Froyo Games',
        badge: 'ifo-froyo.svg'
    }, 
];
/**
 * Transform the campaign config into a map. Keeps the config the same
 * as the others and allows easy access to a campaign by id
 */ const campaigns_campaignMap = new Map();
campaigns.forEach((campaign)=>{
    campaigns_campaignMap.set(campaign.id, campaign);
});
/* harmony default export */ const constants_campaigns = ((/* unused pure expression or super */ null && (campaigns)));

// EXTERNAL MODULE: ./src/config/constants/ifo.ts
var ifo = __webpack_require__(7829);
// EXTERNAL MODULE: ./src/utils/multicall.ts
var multicall = __webpack_require__(1144);
// EXTERNAL MODULE: ./src/utils/addressHelpers.ts + 1 modules
var addressHelpers = __webpack_require__(5878);
// EXTERNAL MODULE: ./src/config/abi/pointCenterIfo.json
var pointCenterIfo = __webpack_require__(8750);
;// CONCATENATED MODULE: ./src/utils/achievements.ts





const achievements_getAchievementTitle = (campaign)=>{
    switch(campaign.type){
        case 'ifo':
            return {
                key: 'IFO Shopper: %title%',
                data: {
                    title: campaign.title
                }
            };
        default:
            return campaign.title;
    }
};
const achievements_getAchievementDescription = (campaign)=>{
    switch(campaign.type){
        case 'ifo':
            return {
                key: 'Committed more than $5 worth of LP in the %title% IFO',
                data: {
                    title: campaign.title
                }
            };
        default:
            return campaign.description;
    }
};
/**
 * Checks if a wallet is eligible to claim points from valid IFO's
 */ const getClaimableIfoData = async (account)=>{
    const ifoCampaigns = ifosList.filter((ifoItem)=>ifoItem.campaignId !== undefined
    );
    // Returns the claim status of every IFO with a campaign ID
    const claimStatusCalls = ifoCampaigns.map(({ address  })=>{
        return {
            address: getPointCenterIfoAddress(),
            name: 'checkClaimStatus',
            params: [
                account,
                address
            ]
        };
    });
    const claimStatuses = await multicallv2(pointCenterIfoABI, claimStatusCalls, {
        requireSuccess: false
    });
    // Get IFO data for all IFO's that are eligible to claim
    const claimableIfoData = await multicallv2(pointCenterIfoABI, claimStatuses.reduce((accum, claimStatusArr, index)=>{
        if (claimStatusArr === null) {
            return accum;
        }
        const [claimStatus] = claimStatusArr;
        if (claimStatus === true) {
            return [
                ...accum,
                {
                    address: getPointCenterIfoAddress(),
                    name: 'ifos',
                    params: [
                        ifoCampaigns[index].address
                    ]
                }
            ];
        }
        return accum;
    }, []));
    // Transform response to an Achievement
    return claimableIfoData.reduce((accum, claimableIfoDataItem)=>{
        const claimableCampaignId = claimableIfoDataItem.campaignId.toString();
        if (!campaignMap.has(claimableCampaignId)) {
            return accum;
        }
        const campaignMeta = campaignMap.get(claimableCampaignId);
        const { address  } = ifoCampaigns.find((ifoCampaign)=>ifoCampaign.campaignId === claimableCampaignId
        );
        return [
            ...accum,
            {
                address,
                id: claimableCampaignId,
                type: 'ifo',
                title: achievements_getAchievementTitle(campaignMeta),
                description: achievements_getAchievementDescription(campaignMeta),
                badge: campaignMeta.badge,
                points: claimableIfoDataItem.numberPoints.toNumber()
            }, 
        ];
    }, []);
};

;// CONCATENATED MODULE: ./src/state/achievements/helpers.ts




/**
 * Gets all user point increase events on the profile filtered by wallet address
 */ const getUserPointIncreaseEvents = async (account)=>{
    try {
        const { user  } = await request(GRAPH_API_PROFILE, gql`
        query getUserPointIncreaseEvents($account: ID!) {
          user(id: $account) {
            points {
              id
              campaignId
              points
            }
          }
        }
      `, {
            account: account.toLowerCase()
        });
        return user.points;
    } catch (error) {
        return null;
    }
};
/**
 * Gets all user point increase events and adds achievement meta
 */ const helpers_getAchievements = async (account)=>{
    const pointIncreaseEvents = await getUserPointIncreaseEvents(account);
    if (!pointIncreaseEvents) {
        return [];
    }
    return pointIncreaseEvents.reduce((accum, userPoint)=>{
        if (!campaignMap.has(userPoint.campaignId)) {
            return accum;
        }
        const campaignMeta = campaignMap.get(userPoint.campaignId);
        return [
            ...accum,
            {
                id: userPoint.campaignId,
                type: campaignMeta.type,
                address: userPoint.id,
                title: getAchievementTitle(campaignMeta),
                description: getAchievementDescription(campaignMeta),
                badge: campaignMeta.badge,
                points: Number(userPoint.points)
            }, 
        ];
    }, []);
};

// EXTERNAL MODULE: ./src/config/constants/types.ts
var types = __webpack_require__(7971);
;// CONCATENATED MODULE: external "swr/immutable"
const immutable_namespaceObject = require("swr/immutable");
// EXTERNAL MODULE: ./src/state/profile/index.tsx
var profile = __webpack_require__(5952);
// EXTERNAL MODULE: ./src/state/profile/helpers.ts
var profile_helpers = __webpack_require__(5712);
;// CONCATENATED MODULE: ./src/state/profile/hooks.ts










const useFetchProfile = ()=>{
    const { account  } = (0,core_.useWeb3React)();
    const dispatch = (0,state/* useAppDispatch */.TL)();
    (0,external_react_.useEffect)(()=>{
        if (account) {
            dispatch((0,profile/* fetchProfile */.In)(account));
        }
    }, [
        account,
        dispatch
    ]);
};
const useProfileForAddress = (address)=>{
    const { data , status , mutate  } = useSWRImmutable(address ? [
        address,
        'profile'
    ] : null, ()=>getProfile(address)
    );
    return {
        profile: data,
        isFetching: status === FetchStatus.Fetching,
        refresh: mutate
    };
};
const useAchievementsForAddress = (address)=>{
    const { data , status , mutate  } = useSWRImmutable(address ? [
        address,
        'achievements'
    ] : null, ()=>getAchievements(address)
    );
    return {
        achievements: data || [],
        isFetching: status === FetchStatus.Fetching,
        refresh: mutate
    };
};
const useProfile = ()=>{
    const { isInitialized , isLoading , data , hasRegistered  } = (0,external_react_redux_.useSelector)((state)=>state.profile
    );
    return {
        profile: data,
        hasProfile: isInitialized && hasRegistered,
        isInitialized,
        isLoading
    };
};
const useGetProfileAvatar = (account)=>{
    const profileAvatar = useSelector((state)=>state.profile.profileAvatars[account]
    );
    const { username , nft , hasRegistered , usernameFetchStatus , avatarFetchStatus  } = profileAvatar || {};
    const dispatch = useAppDispatch();
    useEffect(()=>{
        const address = isAddress(account);
        if (!nft && avatarFetchStatus !== FetchStatus.Fetched && address) {
            dispatch(fetchProfileAvatar(account));
        }
        if (!username && avatarFetchStatus === FetchStatus.Fetched && usernameFetchStatus !== FetchStatus.Fetched && address) {
            dispatch(fetchProfileUsername({
                account,
                hasRegistered
            }));
        }
    }, [
        account,
        nft,
        username,
        hasRegistered,
        avatarFetchStatus,
        usernameFetchStatus,
        dispatch
    ]);
    return {
        username,
        nft,
        usernameFetchStatus,
        avatarFetchStatus
    };
};

// EXTERNAL MODULE: ./src/state/lists/hooks.ts + 2 modules
var lists_hooks = __webpack_require__(7952);
// EXTERNAL MODULE: external "@uniswap/token-lists"
var token_lists_ = __webpack_require__(1554);
// EXTERNAL MODULE: ./src/hooks/Tokens.ts
var Tokens = __webpack_require__(6435);
// EXTERNAL MODULE: ./src/config/constants/lists.ts
var constants_lists = __webpack_require__(428);
// EXTERNAL MODULE: ./src/hooks/useFetchListCallback.ts + 1 modules
var useFetchListCallback = __webpack_require__(8339);
;// CONCATENATED MODULE: ./src/hooks/useInterval.ts

function useInterval(callback, delay, leading = true) {
    const savedCallback = (0,external_react_.useRef)();
    // Remember the latest callback.
    (0,external_react_.useEffect)(()=>{
        savedCallback.current = callback;
    }, [
        callback
    ]);
    // Set up the interval.
    (0,external_react_.useEffect)(()=>{
        function tick() {
            const { current  } = savedCallback;
            if (current) {
                current();
            }
        }
        if (delay !== null) {
            if (leading) tick();
            const id = setInterval(tick, delay);
            return ()=>clearInterval(id)
            ;
        }
        return undefined;
    }, [
        delay,
        leading
    ]);
};

;// CONCATENATED MODULE: ./src/hooks/useIsWindowVisible.ts

function isWindowVisible() {
    if (!(typeof document !== 'undefined' && 'visibilityState' in document)) {
        return true;
    }
    return document.visibilityState === 'visible';
}
/**
 * Returns whether the window is currently visible to the user.
 */ function useIsWindowVisible() {
    const { 0: isVisible , 1: setIsVisible  } = (0,external_react_.useState)(isWindowVisible());
    (0,external_react_.useEffect)(()=>{
        if (!('visibilityState' in document)) return undefined;
        const handleVisibilityChange = ()=>{
            setIsVisible(isWindowVisible());
        };
        document.addEventListener('visibilitychange', handleVisibilityChange);
        return ()=>{
            document.removeEventListener('visibilitychange', handleVisibilityChange);
        };
    }, [
        setIsVisible
    ]);
    return isVisible;
};

// EXTERNAL MODULE: ./src/state/lists/actions.ts
var actions = __webpack_require__(8412);
;// CONCATENATED MODULE: ./src/state/lists/updater.ts












function Updater() {
    const { library  } = (0,useActiveWeb3React/* default */.Z)();
    const dispatch = (0,external_react_redux_.useDispatch)();
    const isWindowVisible = useIsWindowVisible();
    // get all loaded lists, and the active urls
    const lists = (0,lists_hooks/* useAllLists */.R0)();
    const activeListUrls = (0,lists_hooks/* useActiveListUrls */.v0)();
    // initiate loading
    (0,Tokens/* useAllInactiveTokens */.EK)();
    const fetchList = (0,useFetchListCallback/* default */.Z)();
    const fetchAllListsCallback = (0,external_react_.useCallback)(()=>{
        if (!isWindowVisible) return;
        Object.keys(lists).forEach((url)=>fetchList(url).catch((error)=>console.debug('interval list fetching error', error)
            )
        );
    }, [
        fetchList,
        isWindowVisible,
        lists
    ]);
    // fetch all lists every 10 minutes, but only after we initialize library
    useInterval(fetchAllListsCallback, library ? 1000 * 60 * 10 : null);
    // whenever a list is not loaded and not loading, try again to load it
    (0,external_react_.useEffect)(()=>{
        Object.keys(lists).forEach((listUrl)=>{
            const list = lists[listUrl];
            if (!list.current && !list.loadingRequestId && !list.error) {
                fetchList(listUrl).catch((error)=>console.debug('list added fetching error', error)
                );
            }
        });
    }, [
        dispatch,
        fetchList,
        library,
        lists
    ]);
    // if any lists from unsupported lists are loaded, check them too (in case new updates since last visit)
    (0,external_react_.useEffect)(()=>{
        Object.keys(constants_lists/* UNSUPPORTED_LIST_URLS */.US).forEach((listUrl)=>{
            const list = lists[listUrl];
            if (!list || !list.current && !list.loadingRequestId && !list.error) {
                fetchList(listUrl).catch((error)=>console.debug('list added fetching error', error)
                );
            }
        });
    }, [
        dispatch,
        fetchList,
        library,
        lists
    ]);
    // automatically update lists if versions are minor/patch
    (0,external_react_.useEffect)(()=>{
        Object.keys(lists).forEach((listUrl)=>{
            const list = lists[listUrl];
            if (list.current && list.pendingUpdate) {
                const bump = (0,token_lists_.getVersionUpgrade)(list.current.version, list.pendingUpdate.version);
                // eslint-disable-next-line default-case
                switch(bump){
                    case token_lists_.VersionUpgrade.NONE:
                        throw new Error('unexpected no version bump');
                    // update any active or inactive lists
                    case token_lists_.VersionUpgrade.PATCH:
                    case token_lists_.VersionUpgrade.MINOR:
                    case token_lists_.VersionUpgrade.MAJOR:
                        dispatch((0,actions/* acceptListUpdate */.xJ)(listUrl));
                }
            }
        });
    }, [
        dispatch,
        lists,
        activeListUrls
    ]);
    return null;
};

// EXTERNAL MODULE: ./src/hooks/useContract.ts + 2 modules
var useContract = __webpack_require__(6063);
// EXTERNAL MODULE: ./src/hooks/useDebounce.ts
var useDebounce = __webpack_require__(5999);
;// CONCATENATED MODULE: ./src/state/multicall/retry.ts
/* eslint-disable */ function wait(ms) {
    return new Promise((resolve)=>setTimeout(resolve, ms)
    );
}
function waitRandom(min, max) {
    return wait(min + Math.round(Math.random() * Math.max(0, max - min)));
}
/**
 * This error is thrown if the function is cancelled before completing
 */ class CancelledError extends Error {
    constructor(){
        super('Cancelled');
    }
}
/**
 * Throw this error if the function should retry
 */ class RetryableError extends Error {
}
/**
 * Retries the function that returns the promise until the promise successfully resolves up to n retries
 * @param fn function to retry
 * @param n how many times to retry
 * @param minWait min wait between retries in ms
 * @param maxWait max wait between retries in ms
 */ function retry(fn, { n , minWait , maxWait  }) {
    let completed = false;
    let rejectCancelled;
    const promise = new Promise(async (resolve, reject)=>{
        rejectCancelled = reject;
        while(true){
            let result;
            try {
                result = await fn();
                if (!completed) {
                    resolve(result);
                    completed = true;
                }
                break;
            } catch (error) {
                console.error(error);
                if (completed) {
                    break;
                }
                if (n <= 0 || !(error instanceof RetryableError)) {
                    reject(error);
                    completed = true;
                    break;
                }
                n--;
            }
            await waitRandom(minWait, maxWait);
        }
    });
    return {
        promise,
        cancel: ()=>{
            if (completed) return;
            completed = true;
            rejectCancelled(new CancelledError());
        }
    };
} /* eslint-enable */ 

// EXTERNAL MODULE: ./src/state/multicall/actions.ts
var multicall_actions = __webpack_require__(3884);
;// CONCATENATED MODULE: ./src/state/multicall/chunkArray.ts
// chunks array into chunks of maximum size
// evenly distributes items among the chunks
function chunkArray(items, maxChunkSize) {
    if (maxChunkSize < 1) throw new Error('maxChunkSize must be gte 1');
    if (items.length <= maxChunkSize) return [
        items
    ];
    const numChunks = Math.ceil(items.length / maxChunkSize);
    const chunkSize = Math.ceil(items.length / numChunks);
    return [
        ...Array(numChunks).keys()
    ].map((ix)=>items.slice(ix * chunkSize, ix * chunkSize + chunkSize)
    );
};

;// CONCATENATED MODULE: ./src/state/multicall/updater.tsx









// chunk calls so we do not exceed the gas limit
const CALL_CHUNK_SIZE = 500;
/**
 * Fetches a chunk of calls, enforcing a minimum block number constraint
 * @param multicallContract multicall contract to fetch against
 * @param chunk chunk of calls to make
 * @param minBlockNumber minimum block number of the result set
 */ async function fetchChunk(multicallContract, chunk, minBlockNumber) {
    console.debug('Fetching chunk', multicallContract, chunk, minBlockNumber);
    let resultsBlockNumber;
    let returnData;
    try {
        // prettier-ignore
        [resultsBlockNumber, returnData] = await multicallContract.aggregate(chunk.map((obj)=>[
                obj.address,
                obj.callData
            ]
        ), {
            blockTag: minBlockNumber
        });
    } catch (err) {
        const error = err;
        if (error.code === -32000 || error?.data?.message && error?.data?.message?.indexOf('header not found') !== -1 || error.message?.indexOf('header not found') !== -1) {
            throw new RetryableError(`header not found for block number ${minBlockNumber}`);
        } else if (error.code === -32603 || error.message?.indexOf('execution ran out of gas') !== -1) {
            if (chunk.length > 1) {
                if (false) {}
                const half = Math.floor(chunk.length / 2);
                const [c0, c1] = await Promise.all([
                    fetchChunk(multicallContract, chunk.slice(0, half), minBlockNumber),
                    fetchChunk(multicallContract, chunk.slice(half, chunk.length), minBlockNumber), 
                ]);
                return {
                    results: c0.results.concat(c1.results),
                    blockNumber: c1.blockNumber
                };
            }
        }
        console.debug('Failed to fetch chunk inside retry', error);
        throw error;
    }
    if (resultsBlockNumber.toNumber() < minBlockNumber) {
        console.debug(`Fetched results for old block number: ${resultsBlockNumber.toString()} vs. ${minBlockNumber}`);
    }
    return {
        results: returnData,
        blockNumber: resultsBlockNumber.toNumber()
    };
}
/**
 * From the current all listeners state, return each call key mapped to the
 * minimum number of blocks per fetch. This is how often each key must be fetched.
 * @param allListeners the all listeners state
 * @param chainId the current chain id
 */ function activeListeningKeys(allListeners, chainId) {
    if (!allListeners || !chainId) return {};
    const listeners = allListeners[chainId];
    if (!listeners) return {};
    return Object.keys(listeners).reduce((memo, callKey)=>{
        const keyListeners = listeners[callKey];
        memo[callKey] = Object.keys(keyListeners).filter((key)=>{
            const blocksPerFetch = parseInt(key);
            if (blocksPerFetch <= 0) return false;
            return keyListeners[blocksPerFetch] > 0;
        }).reduce((previousMin, current)=>{
            return Math.min(previousMin, parseInt(current));
        }, Infinity);
        return memo;
    }, {});
}
/**
 * Return the keys that need to be refetched
 * @param callResults current call result state
 * @param listeningKeys each call key mapped to how old the data can be in blocks
 * @param chainId the current chain id
 * @param currentBlock the latest block number
 */ function outdatedListeningKeys(callResults, listeningKeys, chainId, currentBlock) {
    if (!chainId || !currentBlock) return [];
    const results = callResults[chainId];
    // no results at all, load everything
    if (!results) return Object.keys(listeningKeys);
    return Object.keys(listeningKeys).filter((callKey)=>{
        const blocksPerFetch = listeningKeys[callKey];
        const data = callResults[chainId][callKey];
        // no data, must fetch
        if (!data) return true;
        const minDataBlockNumber = currentBlock - (blocksPerFetch - 1);
        // already fetching it for a recent enough block, don't refetch it
        if (data.fetchingBlockNumber && data.fetchingBlockNumber >= minDataBlockNumber) return false;
        // if data is older than minDataBlockNumber, fetch it
        return !data.blockNumber || data.blockNumber < minDataBlockNumber;
    });
}
function updater_Updater() {
    const dispatch = (0,external_react_redux_.useDispatch)();
    const state = (0,external_react_redux_.useSelector)((s)=>s.multicall
    );
    // wait for listeners to settle before triggering updates
    const debouncedListeners = (0,useDebounce/* default */.Z)(state.callListeners, 100);
    const currentBlock = (0,block_hooks/* useCurrentBlock */.je)();
    const { chainId  } = (0,useActiveWeb3React/* default */.Z)();
    const multicallContract = (0,useContract/* useMulticallContract */.gq)();
    const cancellations = (0,external_react_.useRef)();
    const listeningKeys = (0,external_react_.useMemo)(()=>{
        return activeListeningKeys(debouncedListeners, chainId);
    }, [
        debouncedListeners,
        chainId
    ]);
    const unserializedOutdatedCallKeys = (0,external_react_.useMemo)(()=>{
        return outdatedListeningKeys(state.callResults, listeningKeys, chainId, currentBlock);
    }, [
        chainId,
        state.callResults,
        listeningKeys,
        currentBlock
    ]);
    const serializedOutdatedCallKeys = (0,external_react_.useMemo)(()=>JSON.stringify(unserializedOutdatedCallKeys.sort())
    , [
        unserializedOutdatedCallKeys
    ]);
    (0,external_react_.useEffect)(()=>{
        if (!currentBlock || !chainId || !multicallContract) return;
        const outdatedCallKeys = JSON.parse(serializedOutdatedCallKeys);
        if (outdatedCallKeys.length === 0) return;
        const calls = outdatedCallKeys.map((key)=>(0,multicall_actions/* parseCallKey */.gl)(key)
        );
        const chunkedCalls = chunkArray(calls, CALL_CHUNK_SIZE);
        if (cancellations.current?.blockNumber !== currentBlock) {
            cancellations.current?.cancellations?.forEach((c)=>c()
            );
        }
        dispatch((0,multicall_actions/* fetchingMulticallResults */.nu)({
            calls,
            chainId,
            fetchingBlockNumber: currentBlock
        }));
        cancellations.current = {
            blockNumber: currentBlock,
            cancellations: chunkedCalls.map((chunk, index)=>{
                const { cancel , promise  } = retry(()=>fetchChunk(multicallContract, chunk, currentBlock)
                , {
                    n: Infinity,
                    minWait: 2500,
                    maxWait: 3500
                });
                promise.then(({ results: returnData , blockNumber: fetchBlockNumber  })=>{
                    cancellations.current = {
                        cancellations: [],
                        blockNumber: currentBlock
                    };
                    // accumulates the length of all previous indices
                    const firstCallKeyIndex = chunkedCalls.slice(0, index).reduce((memo, curr)=>memo + curr.length
                    , 0);
                    const lastCallKeyIndex = firstCallKeyIndex + returnData.length;
                    dispatch((0,multicall_actions/* updateMulticallResults */.zT)({
                        chainId,
                        results: outdatedCallKeys.slice(firstCallKeyIndex, lastCallKeyIndex).reduce((memo, callKey, i)=>{
                            memo[callKey] = returnData[i] ?? null;
                            return memo;
                        }, {}),
                        blockNumber: fetchBlockNumber
                    }));
                }).catch((error)=>{
                    if (error instanceof CancelledError) {
                        console.debug('Cancelled fetch for blockNumber', currentBlock);
                        return;
                    }
                    console.error('Failed to fetch multicall chunk', chunk, chainId, error);
                    dispatch((0,multicall_actions/* errorFetchingMulticallResults */.wC)({
                        calls: chunk,
                        chainId,
                        fetchingBlockNumber: currentBlock
                    }));
                });
                return cancel;
            })
        };
    }, [
        chainId,
        multicallContract,
        dispatch,
        serializedOutdatedCallKeys,
        currentBlock
    ]);
    return null;
};

// EXTERNAL MODULE: ./src/components/Toast/index.tsx + 4 modules
var Toast = __webpack_require__(3937);
// EXTERNAL MODULE: ./src/hooks/useToast.ts
var useToast = __webpack_require__(789);
// EXTERNAL MODULE: ./src/state/transactions/actions.ts
var transactions_actions = __webpack_require__(1564);
;// CONCATENATED MODULE: ./src/state/transactions/updater.tsx









function shouldCheck(currentBlock, tx) {
    if (tx.receipt) return false;
    if (!tx.lastCheckedBlockNumber) return true;
    const blocksSinceCheck = currentBlock - tx.lastCheckedBlockNumber;
    if (blocksSinceCheck < 1) return false;
    const minutesPending = (new Date().getTime() - tx.addedTime) / 1000 / 60;
    if (minutesPending > 60) {
        // every 10 blocks if pending for longer than an hour
        return blocksSinceCheck > 9;
    }
    if (minutesPending > 5) {
        // every 3 blocks if pending more than 5 minutes
        return blocksSinceCheck > 2;
    }
    // otherwise every block
    return true;
}
function transactions_updater_Updater() {
    const { library , chainId  } = (0,useActiveWeb3React/* default */.Z)();
    const { t  } = (0,Localization/* useTranslation */.$G)();
    const currentBlock = (0,block_hooks/* useCurrentBlock */.je)();
    const dispatch = (0,external_react_redux_.useDispatch)();
    const state = (0,external_react_redux_.useSelector)((s)=>s.transactions
    );
    const transactions = (0,external_react_.useMemo)(()=>chainId ? state[chainId] ?? {} : {}
    , [
        chainId,
        state
    ]);
    const { toastError , toastSuccess  } = (0,useToast/* default */.Z)();
    (0,external_react_.useEffect)(()=>{
        if (!chainId || !library || !currentBlock) return;
        Object.keys(transactions).filter((hash)=>shouldCheck(currentBlock, transactions[hash])
        ).forEach((hash)=>{
            library.getTransactionReceipt(hash).then((receipt)=>{
                if (receipt) {
                    dispatch((0,transactions_actions/* finalizeTransaction */.Aw)({
                        chainId,
                        hash,
                        receipt: {
                            blockHash: receipt.blockHash,
                            blockNumber: receipt.blockNumber,
                            contractAddress: receipt.contractAddress,
                            from: receipt.from,
                            status: receipt.status,
                            to: receipt.to,
                            transactionHash: receipt.transactionHash,
                            transactionIndex: receipt.transactionIndex
                        }
                    }));
                    const toast = receipt.status === 1 ? toastSuccess : toastError;
                    toast(t('Transaction receipt'), /*#__PURE__*/ jsx_runtime_.jsx(Toast/* ToastDescriptionWithTx */.YO, {
                        txHash: receipt.transactionHash
                    }));
                } else {
                    dispatch((0,transactions_actions/* checkedTransaction */.LN)({
                        chainId,
                        hash,
                        blockNumber: currentBlock
                    }));
                }
            }).catch((error)=>{
                console.error(`failed to check transaction hash: ${hash}`, error);
            });
        });
    }, [
        chainId,
        library,
        transactions,
        currentBlock,
        dispatch,
        toastSuccess,
        toastError,
        t
    ]);
    return null;
};

;// CONCATENATED MODULE: ./src/index.tsx







function Updaters() {
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(Updater, {}),
            /*#__PURE__*/ jsx_runtime_.jsx(transactions_updater_Updater, {}),
            /*#__PURE__*/ jsx_runtime_.jsx(updater_Updater, {})
        ]
    }));
}
function Blocklist({ children  }) {
    const { account  } = (0,useActiveWeb3React/* default */.Z)();
    const blocked = (0,external_react_.useMemo)(()=>Boolean(account && constants/* BLOCKED_ADDRESSES.indexOf */.mj.indexOf(account) !== -1)
    , [
        account
    ]);
    if (blocked) {
        return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
            children: "Blocked address"
        }));
    }
    return(/*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
        children: children
    }));
}

// EXTERNAL MODULE: ./src/components/Layout/Page.tsx + 3 modules
var Page = __webpack_require__(5601);
;// CONCATENATED MODULE: ./src/components/ErrorBoundary/ErrorBoundary.tsx






function ErrorBoundary({ children  }) {
    const { t  } = (0,Localization/* useTranslation */.$G)();
    return(/*#__PURE__*/ jsx_runtime_.jsx(react_.ErrorBoundary, {
        fallback: ()=>{
            return(/*#__PURE__*/ jsx_runtime_.jsx(Page/* default */.Z, {
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.LogoIcon, {
                            width: "64px",
                            mb: "8px"
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                            mb: "16px",
                            children: t('Oops, something wrong.')
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Button, {
                            onClick: ()=>window.location.reload()
                            ,
                            children: t('Click here to reset!')
                        })
                    ]
                })
            }));
        },
        children: children
    }));
};

;// CONCATENATED MODULE: ./src/components/ErrorBoundary/index.ts


// EXTERNAL MODULE: ./src/components/NextLink.tsx
var NextLink = __webpack_require__(3629);
// EXTERNAL MODULE: ./src/config/localization/languages.ts
var languages = __webpack_require__(9821);
// EXTERNAL MODULE: ./src/hooks/useTheme.ts
var useTheme = __webpack_require__(3917);
// EXTERNAL MODULE: ./src/views/Nft/market/constants.ts + 2 modules
var market_constants = __webpack_require__(1940);
;// CONCATENATED MODULE: ./src/components/Menu/config/config.ts


const config_config = (t)=>[
        {
            label: t('Trade'),
            icon: 'Swap',
            href: '/swap',
            showItemsOnMobile: false,
            items: [
                {
                    label: t('Exchange'),
                    href: '/swap'
                },
                {
                    label: t('Liquidity'),
                    href: '/liquidity'
                }, 
            ]
        },
        {
            label: t('Earn'),
            href: '/farms',
            icon: 'Earn',
            items: [
                {
                    label: t('Farms'),
                    href: '/farms'
                },
                {
                    label: t('Pools'),
                    href: '/pools'
                }, 
            ]
        },
        {
            label: t('Win'),
            href: '/prediction',
            icon: 'Trophy',
            items: [
                {
                    label: t('Trading Competition'),
                    href: '/competition'
                },
                {
                    label: t('Prediction (BETA)'),
                    href: '/prediction'
                },
                {
                    label: t('Lottery'),
                    href: '/lottery'
                }, 
            ]
        },
        {
            label: t('NFT'),
            href: `${market_constants/* nftsBaseUrl */.Vf}`,
            icon: 'Nft',
            items: [
                {
                    label: t('Overview'),
                    href: `${market_constants/* nftsBaseUrl */.Vf}`
                },
                {
                    label: t('Collections'),
                    href: `${market_constants/* nftsBaseUrl */.Vf}/collections`
                },
                {
                    label: t('Activity'),
                    href: `${market_constants/* nftsBaseUrl */.Vf}/activity`
                }, 
            ]
        },
        {
            label: '',
            href: '/info',
            icon: 'More',
            hideSubNav: true,
            items: [
                {
                    label: t('Info'),
                    href: '/info'
                },
                {
                    label: t('IFO'),
                    href: '/ifo'
                },
                {
                    label: t('Voting'),
                    href: '/voting'
                },
                {
                    type: uikit_.DropdownMenuItemType.DIVIDER
                },
                {
                    label: t('Leaderboard'),
                    href: '/teams'
                },
                {
                    type: uikit_.DropdownMenuItemType.DIVIDER
                },
                {
                    label: t('Blog'),
                    href: 'https://medium.com/pancakeswap',
                    type: uikit_.DropdownMenuItemType.EXTERNAL_LINK
                },
                {
                    label: t('Docs'),
                    href: 'https://docs.pancakeswap.finance',
                    type: uikit_.DropdownMenuItemType.EXTERNAL_LINK
                }, 
            ]
        }, 
    ]
;
/* harmony default export */ const Menu_config_config = (config_config);

// EXTERNAL MODULE: ./src/components/ConnectWalletButton.tsx
var ConnectWalletButton = __webpack_require__(621);
// EXTERNAL MODULE: ./src/config/constants/tokens.ts
var constants_tokens = __webpack_require__(9748);
// EXTERNAL MODULE: external "@ethersproject/bignumber"
var bignumber_ = __webpack_require__(5757);
// EXTERNAL MODULE: external "@ethersproject/constants"
var constants_ = __webpack_require__(6644);
// EXTERNAL MODULE: external "@ethersproject/abi"
var abi_ = __webpack_require__(6187);
;// CONCATENATED MODULE: ./src/hooks/useSWRContract.ts
/* eslint-disable no-param-reassign */ 




const fetchStatusMiddleware = (useSWRNext)=>{
    return (key, fetcher, config)=>{
        const swr = useSWRNext(key, fetcher, config);
        let status = types/* FetchStatus.Idle */.iF.Idle;
        if (!swr.isValidating && !swr.error && !swr.data) {
            status = types/* FetchStatus.Idle */.iF.Idle;
        } else if (swr.isValidating && !swr.error && !swr.data) {
            status = types/* FetchStatus.Fetching */.iF.Fetching;
        } else if (swr.data) {
            status = types/* FetchStatus.Fetched */.iF.Fetched;
        } else if (swr.error && !swr.data) {
            status = types/* FetchStatus.Failed */.iF.Failed;
        }
        return {
            status,
            ...swr
        };
    };
};
const getContractKey = (key)=>{
    if (Array.isArray(key)) {
        const [contract, methodName, params] = key || [];
        return {
            contract,
            methodName,
            params
        };
    }
    return key;
};
const serializesContractKey = (key)=>{
    const { contract , methodName , params  } = getContractKey(key) || {};
    const serializedKeys = key && contract && methodName ? {
        address: contract.address,
        interfaceFormat: contract.interface.format(abi_.FormatTypes.full),
        methodName,
        callData: contract.interface.encodeFunctionData(methodName, params)
    } : null;
    return serializedKeys;
};
/**
 * @example
 * const key = [contract, 'methodName', [params]]
 * const key = { contract, methodName, params }
 * const { data, error, mutate } = useSWRContract(key)
 */ function useSWRContract_useSWRContract(key, config = {}) {
    const { contract , methodName , params  } = getContractKey(key) || {};
    const serializedKeys = (0,external_react_.useMemo)(()=>serializesContractKey(key)
    , [
        key
    ]);
    return external_swr_default()(serializedKeys, async ()=>{
        if (!contract || !methodName) return null;
        if (!params) return contract[methodName]();
        return contract[methodName](...params);
    }, config);
}
const immutableMiddleware = (useSWRNext)=>(key, fetcher, config)=>{
        config.revalidateOnFocus = false;
        config.revalidateIfStale = false;
        config.revalidateOnReconnect = false;
        return useSWRNext(key, fetcher, config);
    }
;
function useSWRMulticall(abi, calls, options = {
    requireSuccess: true
}) {
    return useSWR(calls, ()=>multicallv2(abi, calls, options)
    , {
        revalidateIfStale: false,
        revalidateOnFocus: false
    });
}
// dev only
const loggerMiddleware = (useSWRNext)=>{
    return (key, fetcher, config)=>{
        // Add logger to the original fetcher.
        const extendedFetcher = fetcher ? (...args)=>{
            console.debug('SWR Request:', key);
            return fetcher(...args);
        } : null;
        // Execute the hook with the new fetcher.
        return useSWRNext(key, extendedFetcher, config);
    };
};

;// CONCATENATED MODULE: ./src/hooks/useTokenBalance.ts











const useTokenBalance = (tokenAddress)=>{
    const { account  } = (0,core_.useWeb3React)();
    const contract = (0,useContract/* useTokenContract */.Ib)(tokenAddress, false);
    const { data , status , ...rest } = useSWRContract_useSWRContract(account ? {
        contract,
        methodName: 'balanceOf',
        params: [
            account
        ]
    } : null, {
        refreshInterval: constants/* FAST_INTERVAL */.sR
    });
    return {
        ...rest,
        fetchStatus: status,
        balance: data ? new (external_bignumber_js_default())(data.toString()) : bigNumber/* BIG_ZERO */.HW
    };
};
const useTotalSupply = ()=>{
    const cakeContract = useCake();
    const { data  } = useSWRContract([
        cakeContract,
        'totalSupply'
    ], {
        refreshInterval: SLOW_INTERVAL
    });
    return data ? new BigNumber(data.toString()) : null;
};
const useBurnedBalance = (tokenAddress)=>{
    const contract = useTokenContract(tokenAddress, false);
    const { data  } = useSWRContract([
        contract,
        'balanceOf',
        [
            '0x000000000000000000000000000000000000dEaD'
        ]
    ], {
        refreshInterval: SLOW_INTERVAL
    });
    return data ? new BigNumber(data.toString()) : BIG_ZERO;
};
const useGetBnbBalance = ()=>{
    const { account  } = (0,core_.useWeb3React)();
    const { status , data , mutate  } = external_swr_default()([
        account,
        'bnbBalance'
    ], async ()=>{
        return providers/* simpleRpcProvider.getBalance */.J.getBalance(account);
    });
    return {
        balance: data || constants_.Zero,
        fetchStatus: status,
        refresh: mutate
    };
};
const useGetCakeBalance = ()=>{
    const { balance , fetchStatus  } = useTokenBalance(tokens.cake.address);
    // TODO: Remove ethers conversion once useTokenBalance is converted to ethers.BigNumber
    return {
        balance: EthersBigNumber.from(balance.toString()),
        fetchStatus
    };
};
/* harmony default export */ const hooks_useTokenBalance = (useTokenBalance);

// EXTERNAL MODULE: external "@ethersproject/units"
var units_ = __webpack_require__(3138);
;// CONCATENATED MODULE: ./src/components/Menu/UserMenu/CopyAddress.tsx





const Wrapper = external_styled_components_default()(uikit_.Flex).withConfig({
    componentId: "sc-f35b35c3-0"
})`
  align-items: center;
  background-color: ${({ theme  })=>theme.colors.dropdown
};
  border-radius: 16px;
  position: relative;
`;
const Address = external_styled_components_default().div.withConfig({
    componentId: "sc-f35b35c3-1"
})`
  flex: 1;
  position: relative;
  padding-left: 16px;

  & > input {
    background: transparent;
    border: 0;
    color: ${({ theme  })=>theme.colors.text
};
    display: block;
    font-weight: 600;
    font-size: 16px;
    padding: 0;
    width: 100%;

    &:focus {
      outline: 0;
    }
  }

  &:after {
    background: linear-gradient(
      to right,
      ${({ theme  })=>theme.colors.background
}00,
      ${({ theme  })=>theme.colors.background
}E6
    );
    content: '';
    height: 100%;
    pointer-events: none;
    position: absolute;
    right: 0;
    top: 0;
    width: 40px;
  }
`;
const Tooltip = external_styled_components_default().div.withConfig({
    componentId: "sc-f35b35c3-2"
})`
  display: ${({ isTooltipDisplayed  })=>isTooltipDisplayed ? 'inline-block' : 'none'
};
  position: absolute;
  padding: 8px;
  top: -38px;
  right: 0;
  text-align: center;
  background-color: ${({ theme  })=>theme.colors.contrast
};
  color: ${({ theme  })=>theme.colors.invertedContrast
};
  border-radius: 16px;
  opacity: 0.7;
  width: 100px;
`;
const CopyAddress = ({ account , ...props })=>{
    const { 0: isTooltipDisplayed , 1: setIsTooltipDisplayed  } = (0,external_react_.useState)(false);
    const { t  } = (0,Localization/* useTranslation */.$G)();
    const copyAddress = ()=>{
        if (navigator.clipboard && navigator.permissions) {
            navigator.clipboard.writeText(account).then(()=>displayTooltip()
            );
        } else if (document.queryCommandSupported('copy')) {
            const ele = document.createElement('textarea');
            ele.value = account;
            document.body.appendChild(ele);
            ele.select();
            document.execCommand('copy');
            document.body.removeChild(ele);
            displayTooltip();
        }
    };
    function displayTooltip() {
        setIsTooltipDisplayed(true);
        setTimeout(()=>{
            setIsTooltipDisplayed(false);
        }, 1000);
    }
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Box, {
        position: "relative",
        ...props,
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(Wrapper, {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(Address, {
                        title: account,
                        children: /*#__PURE__*/ jsx_runtime_.jsx("input", {
                            type: "text",
                            readOnly: true,
                            value: account
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.IconButton, {
                        variant: "text",
                        onClick: copyAddress,
                        children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.CopyIcon, {
                            color: "primary",
                            width: "24px"
                        })
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(Tooltip, {
                isTooltipDisplayed: isTooltipDisplayed,
                children: t('Copied')
            })
        ]
    }));
};
/* harmony default export */ const UserMenu_CopyAddress = (CopyAddress);

;// CONCATENATED MODULE: ./src/components/Menu/UserMenu/WalletInfo.tsx












const WalletInfo = ({ hasLowBnbBalance , onDismiss  })=>{
    const { t  } = (0,Localization/* useTranslation */.$G)();
    const { account  } = (0,core_.useWeb3React)();
    const { balance , fetchStatus  } = useGetBnbBalance();
    const { balance: cakeBalance , fetchStatus: cakeFetchStatus  } = hooks_useTokenBalance(constants_tokens/* default.cake.address */.ZP.cake.address);
    const { logout  } = (0,useAuth/* default */.Z)();
    const handleLogout = ()=>{
        onDismiss();
        logout();
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                color: "secondary",
                fontSize: "12px",
                textTransform: "uppercase",
                fontWeight: "bold",
                mb: "8px",
                children: t('Your Address')
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(UserMenu_CopyAddress, {
                account: account,
                mb: "24px"
            }),
            hasLowBnbBalance && /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Message, {
                variant: "warning",
                mb: "24px",
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Box, {
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                            fontWeight: "bold",
                            children: t('BNB Balance Low')
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                            as: "p",
                            children: t('You need BNB for transaction fees.')
                        })
                    ]
                })
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                alignItems: "center",
                justifyContent: "space-between",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                        color: "textSubtle",
                        children: t('BNB Balance')
                    }),
                    fetchStatus !== types/* FetchStatus.Fetched */.iF.Fetched ? /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Skeleton, {
                        height: "22px",
                        width: "60px"
                    }) : /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                        children: (0,formatBalance/* formatBigNumber */.dp)(balance, 6)
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                alignItems: "center",
                justifyContent: "space-between",
                mb: "24px",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                        color: "textSubtle",
                        children: t('CAKE Balance')
                    }),
                    cakeFetchStatus !== types/* FetchStatus.Fetched */.iF.Fetched ? /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Skeleton, {
                        height: "22px",
                        width: "60px"
                    }) : /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                        children: (0,formatBalance/* getFullDisplayBalance */.NJ)(cakeBalance, 18, 3)
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Flex, {
                alignItems: "center",
                justifyContent: "end",
                mb: "24px",
                children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.LinkExternal, {
                    href: (0,utils/* getBscScanLink */.s6)(account, 'address'),
                    children: t('View on BscScan')
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Button, {
                variant: "secondary",
                width: "100%",
                onClick: handleLogout,
                children: t('Disconnect Wallet')
            })
        ]
    }));
};
/* harmony default export */ const UserMenu_WalletInfo = (WalletInfo);

// EXTERNAL MODULE: ./src/state/transactions/hooks.tsx
var transactions_hooks = __webpack_require__(5892);
// EXTERNAL MODULE: external "lodash/orderBy"
var orderBy_ = __webpack_require__(9949);
var orderBy_default = /*#__PURE__*/__webpack_require__.n(orderBy_);
;// CONCATENATED MODULE: ./src/components/Menu/UserMenu/TransactionRow.tsx






const TxnIcon = external_styled_components_default()(uikit_.Flex).withConfig({
    componentId: "sc-d1836170-0"
})`
  align-items: center;
  flex: none;
  width: 24px;
`;
const Summary = external_styled_components_default().div.withConfig({
    componentId: "sc-d1836170-1"
})`
  flex: 1;
  padding: 0 8px;
`;
const TxnLink = external_styled_components_default()(uikit_.Link).withConfig({
    componentId: "sc-d1836170-2"
})`
  align-items: center;
  color: ${({ theme  })=>theme.colors.text
};
  display: flex;
  margin-bottom: 16px;
  width: 100%;

  &:hover {
    text-decoration: none;
  }
`;
const renderIcon = (txn)=>{
    if (!txn.receipt) {
        return(/*#__PURE__*/ jsx_runtime_.jsx(uikit_.RefreshIcon, {
            spin: true,
            width: "24px"
        }));
    }
    return txn.receipt?.status === 1 || typeof txn.receipt?.status === 'undefined' ? /*#__PURE__*/ jsx_runtime_.jsx(uikit_.CheckmarkCircleIcon, {
        color: "success",
        width: "24px"
    }) : /*#__PURE__*/ jsx_runtime_.jsx(uikit_.BlockIcon, {
        color: "failure",
        width: "24px"
    });
};
const TransactionRow = ({ txn  })=>{
    const { chainId  } = (0,useActiveWeb3React/* default */.Z)();
    if (!txn) {
        return null;
    }
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(TxnLink, {
        href: (0,utils/* getBscScanLink */.s6)(txn.hash, 'transaction', chainId),
        external: true,
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(TxnIcon, {
                children: renderIcon(txn)
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(Summary, {
                children: txn.summary ?? txn.hash
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(TxnIcon, {
                children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.OpenNewIcon, {
                    width: "24px",
                    color: "primary"
                })
            })
        ]
    }));
};
/* harmony default export */ const UserMenu_TransactionRow = (TransactionRow);

;// CONCATENATED MODULE: ./src/components/Menu/UserMenu/WalletTransactions.tsx










const WalletTransactions = ()=>{
    const { chainId  } = (0,useActiveWeb3React/* default */.Z)();
    const dispatch = (0,external_react_redux_.useDispatch)();
    const { t  } = (0,Localization/* useTranslation */.$G)();
    const allTransactions = (0,transactions_hooks/* useAllTransactions */.kf)();
    const sortedTransactions = orderBy_default()(Object.values(allTransactions).filter(transactions_hooks/* isTransactionRecent */.mH), 'addedTime', 'desc');
    const handleClearAll = ()=>{
        if (chainId) {
            dispatch((0,transactions_actions/* clearAllTransactions */.fY)({
                chainId
            }));
        }
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Box, {
        minHeight: "120px",
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                alignItems: "center",
                justifyContent: "space-between",
                mb: "24px",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                        color: "secondary",
                        fontSize: "12px",
                        textTransform: "uppercase",
                        fontWeight: "bold",
                        children: t('Recent Transactions')
                    }),
                    sortedTransactions.length > 0 && /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Button, {
                        scale: "sm",
                        onClick: handleClearAll,
                        variant: "text",
                        px: "0",
                        children: t('Clear all')
                    })
                ]
            }),
            sortedTransactions.length > 0 ? sortedTransactions.map((txn)=>/*#__PURE__*/ jsx_runtime_.jsx(UserMenu_TransactionRow, {
                    txn: txn
                }, txn.hash)
            ) : /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                textAlign: "center",
                children: t('No recent transactions')
            })
        ]
    }));
};
/* harmony default export */ const UserMenu_WalletTransactions = (WalletTransactions);

;// CONCATENATED MODULE: ./src/components/Menu/UserMenu/WalletModal.tsx










var WalletView;
(function(WalletView) {
    WalletView[WalletView["WALLET_INFO"] = 0] = "WALLET_INFO";
    WalletView[WalletView["TRANSACTIONS"] = 1] = "TRANSACTIONS";
})(WalletView || (WalletView = {}));
const LOW_BNB_BALANCE = (0,units_.parseUnits)('2', 'gwei');
const ModalHeader = external_styled_components_default()(uikit_.ModalHeader).withConfig({
    componentId: "sc-603407d1-0"
})`
  background: ${({ theme  })=>theme.colors.gradients.bubblegum
};
`;
const Tabs = external_styled_components_default().div.withConfig({
    componentId: "sc-603407d1-1"
})`
  background-color: ${({ theme  })=>theme.colors.dropdown
};
  border-bottom: 1px solid ${({ theme  })=>theme.colors.cardBorder
};
  padding: 16px 24px;
`;
const WalletModal = ({ initialView =WalletView.WALLET_INFO , onDismiss  })=>{
    const { 0: view , 1: setView  } = (0,external_react_.useState)(initialView);
    const { t  } = (0,Localization/* useTranslation */.$G)();
    const { balance , fetchStatus  } = useGetBnbBalance();
    const hasLowBnbBalance = fetchStatus === types/* FetchStatus.Fetched */.iF.Fetched && balance.lte(LOW_BNB_BALANCE);
    const handleClick = (newIndex)=>{
        setView(newIndex);
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.ModalContainer, {
        title: t('Welcome!'),
        minWidth: "320px",
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(ModalHeader, {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.ModalTitle, {
                        children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Heading, {
                            children: t('Your Wallet')
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.IconButton, {
                        variant: "text",
                        onClick: onDismiss,
                        children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.CloseIcon, {
                            width: "24px",
                            color: "text"
                        })
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(Tabs, {
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.ButtonMenu, {
                    scale: "sm",
                    variant: "subtle",
                    onItemClick: handleClick,
                    activeIndex: view,
                    fullWidth: true,
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.ButtonMenuItem, {
                            children: t('Wallet')
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.ButtonMenuItem, {
                            children: t('Transactions')
                        })
                    ]
                })
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.ModalBody, {
                p: "24px",
                maxWidth: "400px",
                width: "100%",
                children: [
                    view === WalletView.WALLET_INFO && /*#__PURE__*/ jsx_runtime_.jsx(UserMenu_WalletInfo, {
                        hasLowBnbBalance: hasLowBnbBalance,
                        onDismiss: onDismiss
                    }),
                    view === WalletView.TRANSACTIONS && /*#__PURE__*/ jsx_runtime_.jsx(UserMenu_WalletTransactions, {})
                ]
            })
        ]
    }));
};
/* harmony default export */ const UserMenu_WalletModal = (WalletModal);

;// CONCATENATED MODULE: ./src/components/Menu/UserMenu/WalletUserMenuItem.tsx




const WalletUserMenuItem = ({ hasLowBnbBalance , onPresentWalletModal  })=>{
    const { t  } = (0,Localization/* useTranslation */.$G)();
    return(/*#__PURE__*/ jsx_runtime_.jsx(uikit_.UserMenuItem, {
        as: "button",
        onClick: onPresentWalletModal,
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
            alignItems: "center",
            justifyContent: "space-between",
            width: "100%",
            children: [
                t('Wallet'),
                hasLowBnbBalance && /*#__PURE__*/ jsx_runtime_.jsx(uikit_.WarningIcon, {
                    color: "warning",
                    width: "24px"
                })
            ]
        })
    }));
};
/* harmony default export */ const UserMenu_WalletUserMenuItem = (WalletUserMenuItem);

;// CONCATENATED MODULE: ./src/components/Menu/UserMenu/index.tsx













const UserMenu = ()=>{
    const router = (0,router_.useRouter)();
    const { t  } = (0,Localization/* useTranslation */.$G)();
    const { account  } = (0,core_.useWeb3React)();
    const { logout  } = (0,useAuth/* default */.Z)();
    const { balance , fetchStatus  } = useGetBnbBalance();
    const { isInitialized , isLoading , profile  } = useProfile();
    const [onPresentWalletModal] = (0,uikit_.useModal)(/*#__PURE__*/ jsx_runtime_.jsx(UserMenu_WalletModal, {
        initialView: WalletView.WALLET_INFO
    }));
    const [onPresentTransactionModal] = (0,uikit_.useModal)(/*#__PURE__*/ jsx_runtime_.jsx(UserMenu_WalletModal, {
        initialView: WalletView.TRANSACTIONS
    }));
    const hasProfile = isInitialized && !!profile;
    const avatarSrc = profile?.nft?.image?.thumbnail;
    const hasLowBnbBalance = fetchStatus === types/* FetchStatus.Fetched */.iF.Fetched && balance.lte(LOW_BNB_BALANCE);
    if (!account) {
        return(/*#__PURE__*/ jsx_runtime_.jsx(ConnectWalletButton/* default */.Z, {
            scale: "sm"
        }));
    }
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.UserMenu, {
        account: account,
        avatarSrc: avatarSrc,
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(UserMenu_WalletUserMenuItem, {
                hasLowBnbBalance: hasLowBnbBalance,
                onPresentWalletModal: onPresentWalletModal
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(uikit_.UserMenuItem, {
                as: "button",
                onClick: onPresentTransactionModal,
                children: t('Transactions')
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(uikit_.UserMenuDivider, {}),
            /*#__PURE__*/ jsx_runtime_.jsx(uikit_.UserMenuItem, {
                as: "button",
                onClick: logout,
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                    alignItems: "center",
                    justifyContent: "space-between",
                    width: "100%",
                    children: [
                        t('Disconnect'),
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.LogoutIcon, {})
                    ]
                })
            })
        ]
    }));
};
/* harmony default export */ const Menu_UserMenu = (UserMenu);

// EXTERNAL MODULE: ./src/components/Menu/GlobalSettings/index.tsx + 4 modules
var GlobalSettings = __webpack_require__(8742);
;// CONCATENATED MODULE: ./src/components/Menu/utils.ts
const getActiveMenuItem = ({ pathname , menuConfig  })=>menuConfig.find((menuItem)=>pathname.startsWith(menuItem.href) || getActiveSubMenuItem({
            menuItem,
            pathname
        })
    )
;
const getActiveSubMenuItem = ({ pathname , menuItem  })=>{
    const activeSubMenuItems = menuItem?.items.filter((subMenuItem)=>pathname.startsWith(subMenuItem.href)
    ) ?? [];
    // Pathname doesn't include any submenu item href - return undefined
    if (!activeSubMenuItems || activeSubMenuItems.length === 0) {
        return undefined;
    }
    // Pathname includes one sub menu item href - return it
    if (activeSubMenuItems.length === 1) {
        return activeSubMenuItems[0];
    }
    // Pathname includes multiple sub menu item hrefs - find the most specific match
    const mostSpecificMatch = activeSubMenuItems.sort((subMenuItem1, subMenuItem2)=>subMenuItem2.href.length - subMenuItem1.href.length
    )[0];
    return mostSpecificMatch;
};

;// CONCATENATED MODULE: ./src/components/Menu/config/footerConfig.ts
const footerLinks = (t)=>[
        {
            label: t('About'),
            items: [
                {
                    label: t('Contact'),
                    href: 'https://docs.pancakeswap.finance/contact-us'
                },
                {
                    label: t('Brand'),
                    href: 'https://docs.pancakeswap.finance/brand'
                },
                {
                    label: t('Blog'),
                    href: 'https://medium.com/pancakeswap'
                },
                {
                    label: t('Community'),
                    href: 'https://docs.pancakeswap.finance/contact-us/telegram'
                },
                {
                    label: t('CAKE token'),
                    href: 'https://docs.pancakeswap.finance/tokenomics/cake'
                },
                {
                    label: '—'
                },
                {
                    label: t('Online Store'),
                    href: 'https://pancakeswap.creator-spring.com/',
                    isHighlighted: true
                }, 
            ]
        },
        {
            label: t('Help'),
            items: [
                {
                    label: t('Customer Support'),
                    href: 'https://docs.pancakeswap.finance/contact-us/customer-support'
                },
                {
                    label: t('Troubleshooting'),
                    href: 'https://docs.pancakeswap.finance/help/troubleshooting'
                },
                {
                    label: t('Guides'),
                    href: 'https://docs.pancakeswap.finance/get-started'
                }, 
            ]
        },
        {
            label: t('Developers'),
            items: [
                {
                    label: 'Github',
                    href: 'https://github.com/pancakeswap'
                },
                {
                    label: t('Documentation'),
                    href: 'https://docs.pancakeswap.finance'
                },
                {
                    label: t('Bug Bounty'),
                    href: 'https://docs.pancakeswap.finance/code/bug-bounty'
                },
                {
                    label: t('Audits'),
                    href: 'https://docs.pancakeswap.finance/help/faq#is-pancakeswap-safe-has-pancakeswap-been-audited'
                },
                {
                    label: t('Careers'),
                    href: 'https://docs.pancakeswap.finance/hiring/become-a-chef'
                }, 
            ]
        }, 
    ]
;

;// CONCATENATED MODULE: ./src/components/Menu/index.tsx















const Menu = (props)=>{
    const { isDark , toggleTheme  } = (0,useTheme/* default */.Z)();
    const cakePriceUsd = usePriceCakeBusd();
    const { currentLanguage , setLanguage , t  } = (0,Localization/* useTranslation */.$G)();
    const { pathname  } = (0,router_.useRouter)();
    const [showPhishingWarningBanner] = (0,hooks/* usePhishingBannerManager */.FT)();
    const activeMenuItem = getActiveMenuItem({
        menuConfig: Menu_config_config(t),
        pathname
    });
    const activeSubMenuItem = getActiveSubMenuItem({
        menuItem: activeMenuItem,
        pathname
    });
    return(/*#__PURE__*/ jsx_runtime_.jsx(uikit_.Menu, {
        linkComponent: (linkProps)=>{
            return(/*#__PURE__*/ jsx_runtime_.jsx(NextLink/* NextLinkFromReactRouter */.a, {
                to: linkProps.href,
                ...linkProps,
                prefetch: false
            }));
        },
        userMenu: /*#__PURE__*/ jsx_runtime_.jsx(Menu_UserMenu, {}),
        globalMenu: /*#__PURE__*/ jsx_runtime_.jsx(GlobalSettings/* default */.Z, {}),
        isDark: isDark,
        toggleTheme: toggleTheme,
        currentLang: currentLanguage.code,
        langs: languages/* languageList */.s0,
        setLang: setLanguage,
        cakePriceUsd: cakePriceUsd.toNumber(),
        links: Menu_config_config(t),
        subLinks: activeMenuItem?.hideSubNav ? [] : activeMenuItem?.items,
        footerLinks: footerLinks(t),
        activeItem: activeMenuItem?.href,
        activeSubItem: activeSubMenuItem?.href,
        buyCakeLabel: t('Buy CAKE'),
        ...props
    }));
};
/* harmony default export */ const components_Menu = (Menu);

// EXTERNAL MODULE: ./src/utils/web3React.ts
var web3React = __webpack_require__(2338);
;// CONCATENATED MODULE: ./src/Providers.tsx












const ThemeProviderWrapper = (props)=>{
    const [isDark] = (0,hooks/* useThemeManager */.HY)();
    return(/*#__PURE__*/ jsx_runtime_.jsx(external_styled_components_.ThemeProvider, {
        theme: isDark ? uikit_.dark : uikit_.light,
        ...props
    }));
};
const Providers = ({ children , store  })=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx(core_.Web3ReactProvider, {
        getLibrary: web3React/* getLibrary */.av,
        children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_redux_.Provider, {
            store: store,
            children: /*#__PURE__*/ jsx_runtime_.jsx(ToastsContext/* ToastsProvider */.d0, {
                children: /*#__PURE__*/ jsx_runtime_.jsx(ThemeProviderWrapper, {
                    children: /*#__PURE__*/ jsx_runtime_.jsx(Localization/* LanguageProvider */.iL, {
                        children: /*#__PURE__*/ jsx_runtime_.jsx(external_swr_.SWRConfig, {
                            value: {
                                use: [
                                    fetchStatusMiddleware
                                ]
                            },
                            children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.ModalProvider, {
                                children: children
                            })
                        })
                    })
                })
            })
        })
    }));
};
/* harmony default export */ const src_Providers = (Providers);

;// CONCATENATED MODULE: ./src/style/Global.tsx

const GlobalStyle = external_styled_components_.createGlobalStyle`
  * {
    font-family: 'Saira', sans-serif;
  }
  body {
    background-color: ${({ theme  })=>theme.colors.background
};

    img {
      height: auto;
      max-width: 100%;
    }
  }
`;
/* harmony default export */ const Global = (GlobalStyle);

;// CONCATENATED MODULE: ./src/pages/_app.tsx
























// This config is required for number formatting
external_bignumber_js_default().config({
    EXPONENTIAL_AT: 1000,
    DECIMAL_PLACES: 80
});
function GlobalHooks() {
    (0,block_hooks/* usePollBlockNumber */.hd)();
    hooks_useEagerConnect();
    useFetchProfile();
    usePollCoreFarmData();
    hooks_useUserAgent();
    useInactiveListener();
    hooks_useSentryUser();
    return null;
}
function MyApp(props) {
    const { pageProps  } = props;
    const store = (0,state/* useStore */.oR)(pageProps.initialReduxState);
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)((head_default()), {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "viewport",
                        content: "width=device-width, initial-scale=1, maximum-scale=5, minimum-scale=1, viewport-fit=cover"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "description",
                        content: "Flash Hero is the first Metahero reflection token running on Binance Smart Chain."
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "theme-color",
                        content: "#203C2F"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "twitter:image",
                        content: "https://lh3.googleusercontent.com/Sgr9vr3SXIgkUgALlzy_0-C0uYc-7K8DZvvsSGbQOQZtbLqa_DPq84xmO0zzC-9Pg5dABNRd6PvIFscEoPVMZZ7Km4W5O1ndFe_yEsDNgilBXAOW4ggSdGlgKwChwiG7sOoKu2JwIs77F0bzo3jbYg0YmbvbbnI4_Gbc0MnpgcOGYBqfQ6XkxL96_skZobb-mTzZz8rIpF2s2bp9BKOTsCAIcV6VdjAxla3JTwjQNAsJf0sLYLRUjs80CRVke7JRTZqUjokW1mTvCFpzsaAs4nBTAH4MBAa5A02aUbM-KyoqIeN5xVUDjQ-jxXoC74LFDhFK-NPGnf7Q8PQBBzS-yAWjShg34iF6wIk0dBYgbo7Q29dk5g3qv2oCOYjSyyMP21GSpRiM1hBpFif2CshOSFAWCRnZjhI95YSQQytFk_dTb6iW8P_332A_Qtuf98DsbijVemoM5EsmuRryc30OPucMt_llDLGw2Lb0puXPa9143aA_S_GT5oSMCk1k5yaiXJql2-mlB52t6YTcQ8iyU46CUR5Vavf9QZcU3CbXi96u1t6JkX6nIT0YOdwI7YbE4bEgxeiLQNcFJgdOzRPPLTHNZze8IkFsaq0adbWCqxdxntSEheQAWiSXmTGpSTnQ-2gu5IvpOABsq9yhOBAHGU8089iGAJRrRC16CdYKhW1o-wkdzelGWBSgMaFrk85WcnS4S0tYP9otrF5GatV0TzgD=w370-h208"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "twitter:description",
                        content: "Flash Hero is the first Metahero reflection token running on Binance Smart Chain. The longer you hold, The more you earn. It is really that simple. "
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "twitter:card",
                        content: "summary_large_image"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "twitter:title",
                        content: "Flash Hero BSC - Exchange Token With Flash Hero BSC Token"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("title", {
                        children: "Flash Hero Bsc | Exchange"
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(src_Providers, {
                store: store,
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(Blocklist, {
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx(GlobalHooks, {}),
                        /*#__PURE__*/ jsx_runtime_.jsx(Updaters, {}),
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.ResetCSS, {}),
                        /*#__PURE__*/ jsx_runtime_.jsx(Global, {}),
                        /*#__PURE__*/ jsx_runtime_.jsx(components_GlobalCheckClaimStatus, {
                            excludeLocations: []
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(integration_react_namespaceObject.PersistGate, {
                            loading: null,
                            persistor: state/* persistor */.Dj,
                            children: /*#__PURE__*/ jsx_runtime_.jsx(App, {
                                ...props
                            })
                        })
                    ]
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx((script_default()), {
                strategy: "afterInteractive",
                id: "google-tag",
                dangerouslySetInnerHTML: {
                    __html: `
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer', '${"GTM-TL4JTFW"}');
          `
                }
            })
        ]
    }));
}
const App = ({ Component , pageProps  })=>{
    // Use the layout defined at the page level, if available
    const Layout = Component.Layout || external_react_.Fragment;
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(ErrorBoundary, {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(components_Menu, {
                children: /*#__PURE__*/ jsx_runtime_.jsx(Layout, {
                    children: /*#__PURE__*/ jsx_runtime_.jsx(Component, {
                        ...pageProps
                    })
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(EasterEgg_EasterEgg, {
                iterations: 2
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(ToastsContext/* ToastListener */.IG, {}),
            /*#__PURE__*/ jsx_runtime_.jsx(components_SubgraphHealthIndicator, {})
        ]
    }));
};
/* harmony default export */ const _app = (MyApp);


/***/ }),

/***/ 3467:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/**
 * Truncate a transaction or address hash
 */ const truncateHash = (address, startLength = 4, endLength = 4)=>{
    return `${address.substring(0, startLength)}...${address.substring(address.length - endLength)}`;
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (truncateHash);


/***/ }),

/***/ 1593:
/***/ (() => {

global.__rewriteFramesDistDir__ = '.next';


/***/ }),

/***/ 8454:
/***/ ((module) => {

"use strict";
module.exports = require("@binance-chain/bsc-connector");

/***/ }),

/***/ 6187:
/***/ ((module) => {

"use strict";
module.exports = require("@ethersproject/abi");

/***/ }),

/***/ 1541:
/***/ ((module) => {

"use strict";
module.exports = require("@ethersproject/address");

/***/ }),

/***/ 5757:
/***/ ((module) => {

"use strict";
module.exports = require("@ethersproject/bignumber");

/***/ }),

/***/ 9935:
/***/ ((module) => {

"use strict";
module.exports = require("@ethersproject/bytes");

/***/ }),

/***/ 6644:
/***/ ((module) => {

"use strict";
module.exports = require("@ethersproject/constants");

/***/ }),

/***/ 2792:
/***/ ((module) => {

"use strict";
module.exports = require("@ethersproject/contracts");

/***/ }),

/***/ 750:
/***/ ((module) => {

"use strict";
module.exports = require("@ethersproject/hash");

/***/ }),

/***/ 399:
/***/ ((module) => {

"use strict";
module.exports = require("@ethersproject/providers");

/***/ }),

/***/ 9213:
/***/ ((module) => {

"use strict";
module.exports = require("@ethersproject/strings");

/***/ }),

/***/ 3138:
/***/ ((module) => {

"use strict";
module.exports = require("@ethersproject/units");

/***/ }),

/***/ 543:
/***/ ((module) => {

"use strict";
module.exports = require("@pancakeswap/sdk");

/***/ }),

/***/ 2829:
/***/ ((module) => {

"use strict";
module.exports = require("@pancakeswap/uikit");

/***/ }),

/***/ 5184:
/***/ ((module) => {

"use strict";
module.exports = require("@reduxjs/toolkit");

/***/ }),

/***/ 8097:
/***/ ((module) => {

"use strict";
module.exports = require("@sentry/nextjs");

/***/ }),

/***/ 5427:
/***/ ((module) => {

"use strict";
module.exports = require("@sentry/react");

/***/ }),

/***/ 7248:
/***/ ((module) => {

"use strict";
module.exports = require("@snapshot-labs/snapshot.js");

/***/ }),

/***/ 1554:
/***/ ((module) => {

"use strict";
module.exports = require("@uniswap/token-lists");

/***/ }),

/***/ 8054:
/***/ ((module) => {

"use strict";
module.exports = require("@web3-react/core");

/***/ }),

/***/ 6590:
/***/ ((module) => {

"use strict";
module.exports = require("@web3-react/injected-connector");

/***/ }),

/***/ 9795:
/***/ ((module) => {

"use strict";
module.exports = require("@web3-react/walletconnect-connector");

/***/ }),

/***/ 5888:
/***/ ((module) => {

"use strict";
module.exports = require("ajv");

/***/ }),

/***/ 4215:
/***/ ((module) => {

"use strict";
module.exports = require("bignumber.js");

/***/ }),

/***/ 899:
/***/ ((module) => {

"use strict";
module.exports = require("bignumber.js/bignumber");

/***/ }),

/***/ 8729:
/***/ ((module) => {

"use strict";
module.exports = require("cids");

/***/ }),

/***/ 4146:
/***/ ((module) => {

"use strict";
module.exports = require("date-fns");

/***/ }),

/***/ 4175:
/***/ ((module) => {

"use strict";
module.exports = require("fast-json-stable-stringify");

/***/ }),

/***/ 5805:
/***/ ((module) => {

"use strict";
module.exports = require("graphql-request");

/***/ }),

/***/ 221:
/***/ ((module) => {

"use strict";
module.exports = require("lodash/chunk");

/***/ }),

/***/ 8190:
/***/ ((module) => {

"use strict";
module.exports = require("lodash/flatMap");

/***/ }),

/***/ 8579:
/***/ ((module) => {

"use strict";
module.exports = require("lodash/flatten");

/***/ }),

/***/ 1712:
/***/ ((module) => {

"use strict";
module.exports = require("lodash/get");

/***/ }),

/***/ 9699:
/***/ ((module) => {

"use strict";
module.exports = require("lodash/isEmpty");

/***/ }),

/***/ 1546:
/***/ ((module) => {

"use strict";
module.exports = require("lodash/kebabCase");

/***/ }),

/***/ 3385:
/***/ ((module) => {

"use strict";
module.exports = require("lodash/keyBy");

/***/ }),

/***/ 1341:
/***/ ((module) => {

"use strict";
module.exports = require("lodash/maxBy");

/***/ }),

/***/ 1831:
/***/ ((module) => {

"use strict";
module.exports = require("lodash/merge");

/***/ }),

/***/ 9949:
/***/ ((module) => {

"use strict";
module.exports = require("lodash/orderBy");

/***/ }),

/***/ 4042:
/***/ ((module) => {

"use strict";
module.exports = require("lodash/range");

/***/ }),

/***/ 7657:
/***/ ((module) => {

"use strict";
module.exports = require("lodash/sample");

/***/ }),

/***/ 4354:
/***/ ((module) => {

"use strict";
module.exports = require("lodash/times");

/***/ }),

/***/ 8459:
/***/ ((module) => {

"use strict";
module.exports = require("lodash/uniq");

/***/ }),

/***/ 6677:
/***/ ((module) => {

"use strict";
module.exports = require("multicodec");

/***/ }),

/***/ 3735:
/***/ ((module) => {

"use strict";
module.exports = require("multihashes");

/***/ }),

/***/ 562:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 5832:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/loadable.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ 9819:
/***/ ((module) => {

"use strict";
module.exports = require("querystring");

/***/ }),

/***/ 6689:
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ 6022:
/***/ ((module) => {

"use strict";
module.exports = require("react-redux");

/***/ }),

/***/ 4466:
/***/ ((module) => {

"use strict";
module.exports = require("react-transition-group");

/***/ }),

/***/ 997:
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 4161:
/***/ ((module) => {

"use strict";
module.exports = require("redux-persist");

/***/ }),

/***/ 8936:
/***/ ((module) => {

"use strict";
module.exports = require("redux-persist/lib/storage");

/***/ }),

/***/ 7518:
/***/ ((module) => {

"use strict";
module.exports = require("styled-components");

/***/ }),

/***/ 549:
/***/ ((module) => {

"use strict";
module.exports = require("swr");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [730,664,152,308,624,601,54,633], () => (__webpack_exec__(9484), __webpack_exec__(1593), __webpack_exec__(5561), __webpack_exec__(2420)));
module.exports = __webpack_exports__;

})();
//# sourceMappingURL=_app.js.map