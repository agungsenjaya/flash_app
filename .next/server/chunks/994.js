"use strict";
exports.id = 994;
exports.ids = [994];
exports.modules = {

/***/ 994:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var components_NextLink__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3629);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2829);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var views_Info_utils_formatInfoNumbers__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3589);
/* harmony import */ var config_constants_info__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(4003);
/* harmony import */ var views_Info_components_CurrencyLogo__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(764);
/* harmony import */ var contexts_Localization__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(9150);
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(8910);










/**
 *  Columns on different layouts
 *  5 = | # | Pool | TVL | Volume 24H | Volume 7D |
 *  4 = | # | Pool |     | Volume 24H | Volume 7D |
 *  3 = | # | Pool |     | Volume 24H |           |
 *  2 = |   | Pool |     | Volume 24H |           |
 */ const ResponsiveGrid = styled_components__WEBPACK_IMPORTED_MODULE_2___default().div.withConfig({
    componentId: "sc-6f2aa06c-0"
})`
  display: grid;
  grid-gap: 1em;
  align-items: center;
  grid-template-columns: 20px 3.5fr repeat(5, 1fr);

  padding: 0 24px;
  @media screen and (max-width: 900px) {
    grid-template-columns: 20px 1.5fr repeat(3, 1fr);
    & :nth-child(4),
    & :nth-child(5) {
      display: none;
    }
  }
  @media screen and (max-width: 500px) {
    grid-template-columns: 20px 1.5fr repeat(1, 1fr);
    & :nth-child(4),
    & :nth-child(5),
    & :nth-child(6),
    & :nth-child(7) {
      display: none;
    }
  }
  @media screen and (max-width: 480px) {
    grid-template-columns: 2.5fr repeat(1, 1fr);
    > *:nth-child(1) {
      display: none;
    }
  }
`;
const LinkWrapper = styled_components__WEBPACK_IMPORTED_MODULE_2___default()(components_NextLink__WEBPACK_IMPORTED_MODULE_3__/* .NextLinkFromReactRouter */ .a).withConfig({
    componentId: "sc-6f2aa06c-1"
})`
  text-decoration: none;
  :hover {
    cursor: pointer;
    opacity: 0.7;
  }
`;
const SORT_FIELD = {
    volumeUSD: 'volumeUSD',
    liquidityUSD: 'liquidityUSD',
    volumeUSDWeek: 'volumeUSDWeek',
    lpFees24h: 'lpFees24h',
    lpApr7d: 'lpApr7d'
};
const LoadingRow = ()=>/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(ResponsiveGrid, {
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Skeleton, {}),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Skeleton, {}),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Skeleton, {}),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Skeleton, {}),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Skeleton, {}),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Skeleton, {}),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Skeleton, {})
        ]
    })
;
const TableLoader = ()=>/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(LoadingRow, {}),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(LoadingRow, {}),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(LoadingRow, {})
        ]
    })
;
const DataRow = ({ poolData , index  })=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(LinkWrapper, {
        to: `/info/pool/${poolData.address}`,
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(ResponsiveGrid, {
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                    children: index + 1
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Flex, {
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(views_Info_components_CurrencyLogo__WEBPACK_IMPORTED_MODULE_6__/* .DoubleCurrencyLogo */ .g, {
                            address0: poolData.token0.address,
                            address1: poolData.token1.address
                        }),
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                            ml: "8px",
                            children: [
                                poolData.token0.symbol,
                                "/",
                                poolData.token1.symbol
                            ]
                        })
                    ]
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                    children: [
                        "$",
                        (0,views_Info_utils_formatInfoNumbers__WEBPACK_IMPORTED_MODULE_5__/* .formatAmount */ .d)(poolData.volumeUSD)
                    ]
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                    children: [
                        "$",
                        (0,views_Info_utils_formatInfoNumbers__WEBPACK_IMPORTED_MODULE_5__/* .formatAmount */ .d)(poolData.volumeUSDWeek)
                    ]
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                    children: [
                        "$",
                        (0,views_Info_utils_formatInfoNumbers__WEBPACK_IMPORTED_MODULE_5__/* .formatAmount */ .d)(poolData.lpFees24h)
                    ]
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                    children: [
                        (0,views_Info_utils_formatInfoNumbers__WEBPACK_IMPORTED_MODULE_5__/* .formatAmount */ .d)(poolData.lpApr7d),
                        "%"
                    ]
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                    children: [
                        "$",
                        (0,views_Info_utils_formatInfoNumbers__WEBPACK_IMPORTED_MODULE_5__/* .formatAmount */ .d)(poolData.liquidityUSD)
                    ]
                })
            ]
        })
    }));
};
const PoolTable = ({ poolDatas , loading  })=>{
    // for sorting
    const { 0: sortField , 1: setSortField  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(SORT_FIELD.volumeUSD);
    const { 0: sortDirection , 1: setSortDirection  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(true);
    const { t  } = (0,contexts_Localization__WEBPACK_IMPORTED_MODULE_7__/* .useTranslation */ .$G)();
    // pagination
    const { 0: page , 1: setPage  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(1);
    const { 0: maxPage , 1: setMaxPage  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(1);
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        let extraPages = 1;
        if (poolDatas.length % config_constants_info__WEBPACK_IMPORTED_MODULE_9__/* .ITEMS_PER_INFO_TABLE_PAGE */ .si === 0) {
            extraPages = 0;
        }
        setMaxPage(Math.floor(poolDatas.length / config_constants_info__WEBPACK_IMPORTED_MODULE_9__/* .ITEMS_PER_INFO_TABLE_PAGE */ .si) + extraPages);
    }, [
        poolDatas
    ]);
    const sortedPools = (0,react__WEBPACK_IMPORTED_MODULE_1__.useMemo)(()=>{
        return poolDatas ? poolDatas.sort((a, b)=>{
            if (a && b) {
                return a[sortField] > b[sortField] ? (sortDirection ? -1 : 1) * 1 : (sortDirection ? -1 : 1) * -1;
            }
            return -1;
        }).slice(config_constants_info__WEBPACK_IMPORTED_MODULE_9__/* .ITEMS_PER_INFO_TABLE_PAGE */ .si * (page - 1), page * config_constants_info__WEBPACK_IMPORTED_MODULE_9__/* .ITEMS_PER_INFO_TABLE_PAGE */ .si) : [];
    }, [
        page,
        poolDatas,
        sortDirection,
        sortField
    ]);
    const handleSort = (0,react__WEBPACK_IMPORTED_MODULE_1__.useCallback)((newField)=>{
        setSortField(newField);
        setSortDirection(sortField !== newField ? true : !sortDirection);
    }, [
        sortDirection,
        sortField
    ]);
    const arrow = (0,react__WEBPACK_IMPORTED_MODULE_1__.useCallback)((field)=>{
        const directionArrow = !sortDirection ? '↑' : '↓';
        return sortField === field ? directionArrow : '';
    }, [
        sortDirection,
        sortField
    ]);
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_shared__WEBPACK_IMPORTED_MODULE_8__/* .TableWrapper */ .y6, {
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(ResponsiveGrid, {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                        color: "secondary",
                        fontSize: "12px",
                        bold: true,
                        children: "#"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                        color: "secondary",
                        fontSize: "12px",
                        bold: true,
                        textTransform: "uppercase",
                        children: t('Pool')
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_shared__WEBPACK_IMPORTED_MODULE_8__/* .ClickableColumnHeader */ ._J, {
                        color: "secondary",
                        fontSize: "12px",
                        bold: true,
                        onClick: ()=>handleSort(SORT_FIELD.volumeUSD)
                        ,
                        textTransform: "uppercase",
                        children: [
                            t('Volume 24H'),
                            " ",
                            arrow(SORT_FIELD.volumeUSD)
                        ]
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_shared__WEBPACK_IMPORTED_MODULE_8__/* .ClickableColumnHeader */ ._J, {
                        color: "secondary",
                        fontSize: "12px",
                        bold: true,
                        onClick: ()=>handleSort(SORT_FIELD.volumeUSDWeek)
                        ,
                        textTransform: "uppercase",
                        children: [
                            t('Volume 7D'),
                            " ",
                            arrow(SORT_FIELD.volumeUSDWeek)
                        ]
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_shared__WEBPACK_IMPORTED_MODULE_8__/* .ClickableColumnHeader */ ._J, {
                        color: "secondary",
                        fontSize: "12px",
                        bold: true,
                        onClick: ()=>handleSort(SORT_FIELD.lpFees24h)
                        ,
                        textTransform: "uppercase",
                        children: [
                            t('LP reward fees 24H'),
                            " ",
                            arrow(SORT_FIELD.lpFees24h)
                        ]
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_shared__WEBPACK_IMPORTED_MODULE_8__/* .ClickableColumnHeader */ ._J, {
                        color: "secondary",
                        fontSize: "12px",
                        bold: true,
                        onClick: ()=>handleSort(SORT_FIELD.lpApr7d)
                        ,
                        textTransform: "uppercase",
                        children: [
                            t('LP reward APR'),
                            " ",
                            arrow(SORT_FIELD.lpApr7d)
                        ]
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_shared__WEBPACK_IMPORTED_MODULE_8__/* .ClickableColumnHeader */ ._J, {
                        color: "secondary",
                        fontSize: "12px",
                        bold: true,
                        onClick: ()=>handleSort(SORT_FIELD.liquidityUSD)
                        ,
                        textTransform: "uppercase",
                        children: [
                            t('Liquidity'),
                            " ",
                            arrow(SORT_FIELD.liquidityUSD)
                        ]
                    })
                ]
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_shared__WEBPACK_IMPORTED_MODULE_8__/* .Break */ .SS, {}),
            sortedPools.length > 0 ? /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                children: [
                    sortedPools.map((poolData, i)=>{
                        if (poolData) {
                            return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((react__WEBPACK_IMPORTED_MODULE_1___default().Fragment), {
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(DataRow, {
                                        index: (page - 1) * config_constants_info__WEBPACK_IMPORTED_MODULE_9__/* .ITEMS_PER_INFO_TABLE_PAGE */ .si + i,
                                        poolData: poolData
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_shared__WEBPACK_IMPORTED_MODULE_8__/* .Break */ .SS, {})
                                ]
                            }, poolData.address));
                        }
                        return null;
                    }),
                    loading && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(LoadingRow, {}),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_shared__WEBPACK_IMPORTED_MODULE_8__/* .PageButtons */ .Ob, {
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_shared__WEBPACK_IMPORTED_MODULE_8__/* .Arrow */ .Eh, {
                                onClick: ()=>{
                                    setPage(page === 1 ? page : page - 1);
                                },
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.ArrowBackIcon, {
                                    color: page === 1 ? 'textDisabled' : 'primary'
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                                children: t('Page %page% of %maxPage%', {
                                    page,
                                    maxPage
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_shared__WEBPACK_IMPORTED_MODULE_8__/* .Arrow */ .Eh, {
                                onClick: ()=>{
                                    setPage(page === maxPage ? page : page + 1);
                                },
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.ArrowForwardIcon, {
                                    color: page === maxPage ? 'textDisabled' : 'primary'
                                })
                            })
                        ]
                    })
                ]
            }) : /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(TableLoader, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Box, {})
                ]
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PoolTable);


/***/ })

};
;
//# sourceMappingURL=994.js.map