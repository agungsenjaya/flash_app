"use strict";
exports.id = 633;
exports.ids = [633];
exports.modules = {

/***/ 621:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2829);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var hooks_useAuth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8470);
/* harmony import */ var contexts_Localization__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(9150);





const ConnectWalletButton = (props)=>{
    const { t  } = (0,contexts_Localization__WEBPACK_IMPORTED_MODULE_4__/* .useTranslation */ .$G)();
    const { login , logout  } = (0,hooks_useAuth__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z)();
    const { onPresentConnectModal  } = (0,_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_2__.useWalletModal)(login, logout, t);
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_2__.Button, {
        onClick: onPresentConnectModal,
        ...props,
        children: t('Connect Wallet')
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ConnectWalletButton);


/***/ }),

/***/ 8742:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ Menu_GlobalSettings)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "@pancakeswap/uikit"
var uikit_ = __webpack_require__(2829);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: ./src/state/user/hooks/index.tsx
var hooks = __webpack_require__(8605);
// EXTERNAL MODULE: ./src/contexts/Localization/index.tsx + 3 modules
var Localization = __webpack_require__(9150);
// EXTERNAL MODULE: ./src/state/swap/hooks.ts + 17 modules
var swap_hooks = __webpack_require__(1990);
// EXTERNAL MODULE: ./src/hooks/useTheme.ts
var useTheme = __webpack_require__(3917);
// EXTERNAL MODULE: ./src/components/QuestionHelper/index.tsx
var QuestionHelper = __webpack_require__(5061);
// EXTERNAL MODULE: ./src/utils/index.ts + 1 modules
var utils = __webpack_require__(8328);
;// CONCATENATED MODULE: ./src/components/Menu/GlobalSettings/TransactionSettings.tsx







var SlippageError;
(function(SlippageError) {
    SlippageError["InvalidInput"] = "InvalidInput";
    SlippageError["RiskyLow"] = "RiskyLow";
    SlippageError["RiskyHigh"] = "RiskyHigh";
})(SlippageError || (SlippageError = {}));
var DeadlineError;
(function(DeadlineError) {
    DeadlineError["InvalidInput"] = "InvalidInput";
})(DeadlineError || (DeadlineError = {}));
const inputRegex = RegExp(`^\\d*(?:\\\\[.])?\\d*$`) // match escaped "." characters via in a non-capturing group
;
const SlippageTabs = ()=>{
    const [userSlippageTolerance, setUserSlippageTolerance] = (0,hooks/* useUserSlippageTolerance */.$2)();
    const [ttl, setTtl] = (0,hooks/* useUserTransactionTTL */.A6)();
    const { 0: slippageInput , 1: setSlippageInput  } = (0,external_react_.useState)('');
    const { 0: deadlineInput , 1: setDeadlineInput  } = (0,external_react_.useState)('');
    const { t  } = (0,Localization/* useTranslation */.$G)();
    const slippageInputIsValid = slippageInput === '' || (userSlippageTolerance / 100).toFixed(2) === Number.parseFloat(slippageInput).toFixed(2);
    const deadlineInputIsValid = deadlineInput === '' || (ttl / 60).toString() === deadlineInput;
    let slippageError;
    if (slippageInput !== '' && !slippageInputIsValid) {
        slippageError = SlippageError.InvalidInput;
    } else if (slippageInputIsValid && userSlippageTolerance < 50) {
        slippageError = SlippageError.RiskyLow;
    } else if (slippageInputIsValid && userSlippageTolerance > 500) {
        slippageError = SlippageError.RiskyHigh;
    } else {
        slippageError = undefined;
    }
    let deadlineError;
    if (deadlineInput !== '' && !deadlineInputIsValid) {
        deadlineError = DeadlineError.InvalidInput;
    } else {
        deadlineError = undefined;
    }
    const parseCustomSlippage = (value)=>{
        if (value === '' || inputRegex.test((0,utils/* escapeRegExp */.hr)(value))) {
            setSlippageInput(value);
            try {
                const valueAsIntFromRoundedFloat = Number.parseInt((Number.parseFloat(value) * 100).toString());
                if (!Number.isNaN(valueAsIntFromRoundedFloat) && valueAsIntFromRoundedFloat < 5000) {
                    setUserSlippageTolerance(valueAsIntFromRoundedFloat);
                }
            } catch (error) {
                console.error(error);
            }
        }
    };
    const parseCustomDeadline = (value)=>{
        setDeadlineInput(value);
        try {
            const valueAsInt = Number.parseInt(value) * 60;
            if (!Number.isNaN(valueAsInt) && valueAsInt > 0) {
                setTtl(valueAsInt);
            }
        } catch (error) {
            console.error(error);
        }
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
        flexDirection: "column",
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                flexDirection: "column",
                mb: "24px",
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                        mb: "12px",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                children: t('Slippage Tolerance')
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(QuestionHelper/* default */.Z, {
                                text: t('Setting a high slippage tolerance can help transactions succeed, but you may not get such a good price. Use with caution.'),
                                placement: "top-start",
                                ml: "4px"
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                        flexWrap: "wrap",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Button, {
                                mt: "4px",
                                mr: "4px",
                                scale: "sm",
                                onClick: ()=>{
                                    setSlippageInput('');
                                    setUserSlippageTolerance(10);
                                },
                                variant: userSlippageTolerance === 10 ? 'primary' : 'tertiary',
                                children: "0.1%"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Button, {
                                mt: "4px",
                                mr: "4px",
                                scale: "sm",
                                onClick: ()=>{
                                    setSlippageInput('');
                                    setUserSlippageTolerance(50);
                                },
                                variant: userSlippageTolerance === 50 ? 'primary' : 'tertiary',
                                children: "0.5%"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Button, {
                                mr: "4px",
                                mt: "4px",
                                scale: "sm",
                                onClick: ()=>{
                                    setSlippageInput('');
                                    setUserSlippageTolerance(100);
                                },
                                variant: userSlippageTolerance === 100 ? 'primary' : 'tertiary',
                                children: "1.0%"
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                                alignItems: "center",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Box, {
                                        width: "76px",
                                        mt: "4px",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Input, {
                                            scale: "sm",
                                            inputMode: "decimal",
                                            pattern: "^[0-9]*[.,]?[0-9]{0,2}$",
                                            placeholder: (userSlippageTolerance / 100).toFixed(2),
                                            value: slippageInput,
                                            onBlur: ()=>{
                                                parseCustomSlippage((userSlippageTolerance / 100).toFixed(2));
                                            },
                                            onChange: (event)=>{
                                                if (event.currentTarget.validity.valid) {
                                                    parseCustomSlippage(event.target.value.replace(/,/g, '.'));
                                                }
                                            },
                                            isWarning: !slippageInputIsValid,
                                            isSuccess: ![
                                                10,
                                                50,
                                                100
                                            ].includes(userSlippageTolerance)
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                        color: "primary",
                                        bold: true,
                                        ml: "2px",
                                        children: "%"
                                    })
                                ]
                            })
                        ]
                    }),
                    !!slippageError && /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                        fontSize: "14px",
                        color: slippageError === SlippageError.InvalidInput ? 'red' : '#F3841E',
                        mt: "8px",
                        children: slippageError === SlippageError.InvalidInput ? t('Enter a valid slippage percentage') : slippageError === SlippageError.RiskyLow ? t('Your transaction may fail') : t('Your transaction may be frontrun')
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                justifyContent: "space-between",
                alignItems: "center",
                mb: "24px",
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                        alignItems: "center",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                children: t('Tx deadline (mins)')
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(QuestionHelper/* default */.Z, {
                                text: t('Your transaction will revert if it is left confirming for longer than this time.'),
                                placement: "top-start",
                                ml: "4px"
                            })
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Flex, {
                        children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Box, {
                            width: "52px",
                            mt: "4px",
                            children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Input, {
                                scale: "sm",
                                inputMode: "numeric",
                                pattern: "^[0-9]+$",
                                color: deadlineError ? 'red' : undefined,
                                onBlur: ()=>{
                                    parseCustomDeadline((ttl / 60).toString());
                                },
                                placeholder: (ttl / 60).toString(),
                                value: deadlineInput,
                                onChange: (event)=>{
                                    if (event.currentTarget.validity.valid) {
                                        parseCustomDeadline(event.target.value);
                                    }
                                }
                            })
                        })
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const TransactionSettings = (SlippageTabs);

;// CONCATENATED MODULE: ./src/components/Menu/GlobalSettings/ExpertModal.tsx





const ExpertModal = ({ setShowConfirmExpertModal , setShowExpertModeAcknowledgement  })=>{
    const [, toggleExpertMode] = (0,hooks/* useExpertModeManager */.DG)();
    const { 0: isRememberChecked , 1: setIsRememberChecked  } = (0,external_react_.useState)(false);
    const { t  } = (0,Localization/* useTranslation */.$G)();
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Modal, {
        title: t('Expert Mode'),
        onBack: ()=>setShowConfirmExpertModal(false)
        ,
        onDismiss: ()=>setShowConfirmExpertModal(false)
        ,
        headerBackground: "gradients.cardHeader",
        style: {
            maxWidth: '360px'
        },
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Message, {
                variant: "warning",
                mb: "24px",
                children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                    children: t("Expert mode turns off the 'Confirm' transaction prompt, and allows high slippage trades that often result in bad rates and lost funds.")
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                mb: "24px",
                children: t('Only use this mode if you know what you’re doing.')
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                alignItems: "center",
                mb: "24px",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Checkbox, {
                        name: "confirmed",
                        type: "checkbox",
                        checked: isRememberChecked,
                        onChange: ()=>setIsRememberChecked(!isRememberChecked)
                        ,
                        scale: "sm"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                        ml: "10px",
                        color: "textSubtle",
                        style: {
                            userSelect: 'none'
                        },
                        children: t('Don’t show this again')
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Button, {
                mb: "8px",
                id: "confirm-expert-mode",
                onClick: ()=>{
                    // eslint-disable-next-line no-alert
                    if (window.prompt(`Please type the word "confirm" to enable expert mode.`) === 'confirm') {
                        toggleExpertMode();
                        setShowConfirmExpertModal(false);
                        if (isRememberChecked) {
                            setShowExpertModeAcknowledgement(false);
                        }
                    }
                },
                children: t('Turn On Expert Mode')
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Button, {
                variant: "secondary",
                onClick: ()=>{
                    setShowConfirmExpertModal(false);
                },
                children: t('Cancel')
            })
        ]
    }));
};
/* harmony default export */ const GlobalSettings_ExpertModal = (ExpertModal);

// EXTERNAL MODULE: ./src/state/user/hooks/helpers.ts
var helpers = __webpack_require__(787);
;// CONCATENATED MODULE: ./src/components/Menu/GlobalSettings/GasSettings.tsx







const GasSettings = ()=>{
    const { t  } = (0,Localization/* useTranslation */.$G)();
    const [gasPrice, setGasPrice] = (0,hooks/* useGasPriceManager */.nF)();
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
        flexDirection: "column",
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                mb: "12px",
                alignItems: "center",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                        children: t('Default Transaction Speed (GWEI)')
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(QuestionHelper/* default */.Z, {
                        text: t('Adjusts the gas price (transaction fee) for your transaction. Higher GWEI = higher speed = higher fees'),
                        placement: "top-start",
                        ml: "4px"
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                flexWrap: "wrap",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Button, {
                        mt: "4px",
                        mr: "4px",
                        scale: "sm",
                        onClick: ()=>{
                            setGasPrice(helpers/* GAS_PRICE_GWEI.default */.j4["default"]);
                        },
                        variant: gasPrice === helpers/* GAS_PRICE_GWEI.default */.j4["default"] ? 'primary' : 'tertiary',
                        children: t('Standard (%gasPrice%)', {
                            gasPrice: helpers/* GAS_PRICE.default */.DB["default"]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Button, {
                        mt: "4px",
                        mr: "4px",
                        scale: "sm",
                        onClick: ()=>{
                            setGasPrice(helpers/* GAS_PRICE_GWEI.fast */.j4.fast);
                        },
                        variant: gasPrice === helpers/* GAS_PRICE_GWEI.fast */.j4.fast ? 'primary' : 'tertiary',
                        children: t('Fast (%gasPrice%)', {
                            gasPrice: helpers/* GAS_PRICE.fast */.DB.fast
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Button, {
                        mr: "4px",
                        mt: "4px",
                        scale: "sm",
                        onClick: ()=>{
                            setGasPrice(helpers/* GAS_PRICE_GWEI.instant */.j4.instant);
                        },
                        variant: gasPrice === helpers/* GAS_PRICE_GWEI.instant */.j4.instant ? 'primary' : 'tertiary',
                        children: t('Instant (%gasPrice%)', {
                            gasPrice: helpers/* GAS_PRICE.instant */.DB.instant
                        })
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const GlobalSettings_GasSettings = (GasSettings);

;// CONCATENATED MODULE: ./src/components/Menu/GlobalSettings/SettingsModal.tsx












const ScrollableContainer = external_styled_components_default()(uikit_.Flex).withConfig({
    componentId: "sc-be1584cb-0"
})`
  flex-direction: column;
  max-height: 400px;
  ${({ theme  })=>theme.mediaQueries.sm
} {
    max-height: none;
  }
`;
const SettingsModal = ({ onDismiss  })=>{
    const { 0: showConfirmExpertModal , 1: setShowConfirmExpertModal  } = (0,external_react_.useState)(false);
    const [showExpertModeAcknowledgement, setShowExpertModeAcknowledgement] = (0,hooks/* useUserExpertModeAcknowledgementShow */.wX)();
    const [expertMode, toggleExpertMode] = (0,hooks/* useExpertModeManager */.DG)();
    const [singleHopOnly, setSingleHopOnly] = (0,hooks/* useUserSingleHopOnly */.RO)();
    const [audioPlay, toggleSetAudioMode] = (0,hooks/* useAudioModeManager */.TO)();
    const [subgraphHealth, setSubgraphHealh] = (0,hooks/* useSubgraphHealthIndicatorManager */.YF)();
    const { onChangeRecipient  } = (0,swap_hooks/* useSwapActionHandlers */._r)();
    const { t  } = (0,Localization/* useTranslation */.$G)();
    const { theme , isDark , toggleTheme  } = (0,useTheme/* default */.Z)();
    if (showConfirmExpertModal) {
        return(/*#__PURE__*/ jsx_runtime_.jsx(GlobalSettings_ExpertModal, {
            setShowConfirmExpertModal: setShowConfirmExpertModal,
            onDismiss: onDismiss,
            setShowExpertModeAcknowledgement: setShowExpertModeAcknowledgement
        }));
    }
    const handleExpertModeToggle = ()=>{
        if (expertMode) {
            onChangeRecipient(null);
            toggleExpertMode();
        } else if (!showExpertModeAcknowledgement) {
            onChangeRecipient(null);
            toggleExpertMode();
        } else {
            setShowConfirmExpertModal(true);
        }
    };
    return(/*#__PURE__*/ jsx_runtime_.jsx(uikit_.Modal, {
        title: t('Settings'),
        headerBackground: "gradients.cardHeader",
        onDismiss: onDismiss,
        style: {
            maxWidth: '420px'
        },
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(ScrollableContainer, {
            children: [
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                    pb: "24px",
                    flexDirection: "column",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                            bold: true,
                            textTransform: "uppercase",
                            fontSize: "12px",
                            color: "secondary",
                            mb: "24px",
                            children: t('Global')
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                            justifyContent: "space-between",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                    mb: "24px",
                                    children: t('Dark mode')
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx(uikit_.ThemeSwitcher, {
                                    isDark: isDark,
                                    toggleTheme: toggleTheme
                                })
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(GlobalSettings_GasSettings, {})
                    ]
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                    pt: "24px",
                    flexDirection: "column",
                    borderTop: `1px ${theme.colors.cardBorder} solid`,
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                            bold: true,
                            textTransform: "uppercase",
                            fontSize: "12px",
                            color: "secondary",
                            mb: "24px",
                            children: t('Swaps & Liquidity')
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(TransactionSettings, {})
                    ]
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                    justifyContent: "space-between",
                    alignItems: "center",
                    mb: "24px",
                    children: [
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                            alignItems: "center",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                    children: t('Expert Mode')
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx(QuestionHelper/* default */.Z, {
                                    text: t('Bypasses confirmation modals and allows high slippage trades. Use at your own risk.'),
                                    placement: "top-start",
                                    ml: "4px"
                                })
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Toggle, {
                            id: "toggle-expert-mode-button",
                            scale: "md",
                            checked: expertMode,
                            onChange: handleExpertModeToggle
                        })
                    ]
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                    justifyContent: "space-between",
                    alignItems: "center",
                    mb: "24px",
                    children: [
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                            alignItems: "center",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                    children: t('Disable Multihops')
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx(QuestionHelper/* default */.Z, {
                                    text: t('Restricts swaps to direct pairs only.'),
                                    placement: "top-start",
                                    ml: "4px"
                                })
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Toggle, {
                            id: "toggle-disable-multihop-button",
                            checked: singleHopOnly,
                            scale: "md",
                            onChange: ()=>{
                                setSingleHopOnly(!singleHopOnly);
                            }
                        })
                    ]
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                    justifyContent: "space-between",
                    alignItems: "center",
                    mb: "24px",
                    children: [
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                            alignItems: "center",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                    children: t('Subgraph Health Indicator')
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx(QuestionHelper/* default */.Z, {
                                    text: t('Turn on NFT market subgraph health indicator all the time. Default is to show the indicator only when the network is delayed'),
                                    placement: "top-start",
                                    ml: "4px"
                                })
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Toggle, {
                            id: "toggle-subgraph-health-button",
                            checked: subgraphHealth,
                            scale: "md",
                            onChange: ()=>{
                                setSubgraphHealh(!subgraphHealth);
                            }
                        })
                    ]
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                    justifyContent: "space-between",
                    alignItems: "center",
                    children: [
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                            alignItems: "center",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                    children: t('Flippy sounds')
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx(QuestionHelper/* default */.Z, {
                                    text: t('Fun sounds to make a truly immersive pancake-flipping trading experience'),
                                    placement: "top-start",
                                    ml: "4px"
                                })
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.PancakeToggle, {
                            checked: audioPlay,
                            onChange: toggleSetAudioMode,
                            scale: "md"
                        })
                    ]
                })
            ]
        })
    }));
};
/* harmony default export */ const GlobalSettings_SettingsModal = (SettingsModal);

;// CONCATENATED MODULE: ./src/components/Menu/GlobalSettings/index.tsx




const GlobalSettings = ({ color , mr ='8px'  })=>{
    const [onPresentSettingsModal] = (0,uikit_.useModal)(/*#__PURE__*/ jsx_runtime_.jsx(GlobalSettings_SettingsModal, {}));
    return(/*#__PURE__*/ jsx_runtime_.jsx(uikit_.Flex, {
        children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.IconButton, {
            onClick: onPresentSettingsModal,
            variant: "text",
            scale: "sm",
            mr: mr,
            id: "open-settings-dialog-button",
            children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.CogIcon, {
                height: 24,
                width: 24,
                color: color || 'textSubtle'
            })
        })
    }));
};
/* harmony default export */ const Menu_GlobalSettings = (GlobalSettings);


/***/ }),

/***/ 5061:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2829);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_3__);




const QuestionWrapper = styled_components__WEBPACK_IMPORTED_MODULE_3___default().div.withConfig({
    componentId: "sc-3ffb2e33-0"
})`
  :hover,
  :focus {
    opacity: 0.7;
  }
`;
const QuestionHelper = ({ text , placement ='right-end' , size ='16px' , ...props })=>{
    const { targetRef , tooltip , tooltipVisible  } = (0,_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_2__.useTooltip)(text, {
        placement,
        trigger: 'hover'
    });
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_2__.Box, {
        ...props,
        children: [
            tooltipVisible && tooltip,
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(QuestionWrapper, {
                ref: targetRef,
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_2__.HelpIcon, {
                    color: "textSubtle",
                    width: size
                })
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (QuestionHelper);


/***/ }),

/***/ 3937:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Ix": () => (/* reexport */ Toast_ToastContainer),
  "YO": () => (/* reexport */ Toast_DescriptionWithTx),
  "m$": () => (/* reexport */ types)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "react-transition-group"
var external_react_transition_group_ = __webpack_require__(4466);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: external "@pancakeswap/uikit"
var uikit_ = __webpack_require__(2829);
;// CONCATENATED MODULE: ./src/components/Toast/types.ts
const types = {
    SUCCESS: 'success',
    DANGER: 'danger',
    WARNING: 'warning',
    INFO: 'info'
};

;// CONCATENATED MODULE: ./src/components/Toast/Toast.tsx






const alertTypeMap = {
    [types.INFO]: uikit_.alertVariants.INFO,
    [types.SUCCESS]: uikit_.alertVariants.SUCCESS,
    [types.DANGER]: uikit_.alertVariants.DANGER,
    [types.WARNING]: uikit_.alertVariants.WARNING
};
const StyledToast = external_styled_components_default().div.withConfig({
    componentId: "sc-4cea16a-0"
})`
  right: 16px;
  position: fixed;
  max-width: calc(100% - 32px);
  transition: all 250ms ease-in;
  width: 100%;

  ${({ theme  })=>theme.mediaQueries.sm
} {
    max-width: 400px;
  }
`;
const Toast = ({ toast , onRemove , style , ttl , ...props })=>{
    const timer = (0,external_react_.useRef)();
    const ref = (0,external_react_.useRef)(null);
    const removeHandler = (0,external_react_.useRef)(onRemove);
    const { id , title , description , type  } = toast;
    const handleRemove = (0,external_react_.useCallback)(()=>removeHandler.current(id)
    , [
        id,
        removeHandler
    ]);
    const handleMouseEnter = ()=>{
        clearTimeout(timer.current);
    };
    const handleMouseLeave = ()=>{
        if (timer.current) {
            clearTimeout(timer.current);
        }
        timer.current = window.setTimeout(()=>{
            handleRemove();
        }, ttl);
    };
    (0,external_react_.useEffect)(()=>{
        if (timer.current) {
            clearTimeout(timer.current);
        }
        timer.current = window.setTimeout(()=>{
            handleRemove();
        }, ttl);
        return ()=>{
            clearTimeout(timer.current);
        };
    }, [
        timer,
        ttl,
        handleRemove
    ]);
    return(/*#__PURE__*/ jsx_runtime_.jsx(external_react_transition_group_.CSSTransition, {
        nodeRef: ref,
        timeout: 250,
        style: style,
        ...props,
        children: /*#__PURE__*/ jsx_runtime_.jsx(StyledToast, {
            ref: ref,
            onMouseEnter: handleMouseEnter,
            onMouseLeave: handleMouseLeave,
            children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Alert, {
                title: title,
                variant: alertTypeMap[type],
                onClick: handleRemove,
                children: description
            })
        })
    }));
};
/* harmony default export */ const Toast_Toast = (Toast);

;// CONCATENATED MODULE: ./src/components/Toast/ToastContainer.tsx





const ZINDEX = 1000;
const TOP_POSITION = 80 // Initial position from the top
;
const StyledToastContainer = external_styled_components_default().div.withConfig({
    componentId: "sc-d2f82d86-0"
})`
  .enter,
  .appear {
    opacity: 0.01;
  }

  .enter.enter-active,
  .appear.appear-active {
    opacity: 1;
    transition: opacity 250ms ease-in;
  }

  .exit {
    opacity: 1;
  }

  .exit.exit-active {
    opacity: 0.01;
    transition: opacity 250ms ease-out;
  }
`;
const ToastContainer = ({ toasts , onRemove , ttl =6000 , stackSpacing =24  })=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx(StyledToastContainer, {
        children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_transition_group_.TransitionGroup, {
            children: toasts.map((toast, index)=>{
                const zIndex = (ZINDEX - index).toString();
                const top = TOP_POSITION + index * stackSpacing;
                return(/*#__PURE__*/ jsx_runtime_.jsx(Toast_Toast, {
                    toast: toast,
                    onRemove: onRemove,
                    ttl: ttl,
                    style: {
                        top: `${top}px`,
                        zIndex
                    }
                }, toast.id));
            })
        })
    }));
};
/* harmony default export */ const Toast_ToastContainer = (ToastContainer);

// EXTERNAL MODULE: ./src/utils/index.ts + 1 modules
var utils = __webpack_require__(8328);
// EXTERNAL MODULE: ./src/hooks/useActiveWeb3React.ts
var useActiveWeb3React = __webpack_require__(4011);
// EXTERNAL MODULE: ./src/contexts/Localization/index.tsx + 3 modules
var Localization = __webpack_require__(9150);
// EXTERNAL MODULE: ./src/utils/truncateHash.ts
var truncateHash = __webpack_require__(3467);
;// CONCATENATED MODULE: ./src/components/Toast/DescriptionWithTx.tsx







const DescriptionWithTx = ({ txHash , children  })=>{
    const { chainId  } = (0,useActiveWeb3React/* default */.Z)();
    const { t  } = (0,Localization/* useTranslation */.$G)();
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            typeof children === 'string' ? /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                as: "p",
                children: children
            }) : children,
            txHash && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Link, {
                external: true,
                href: (0,utils/* getBscScanLink */.s6)(txHash, 'transaction', chainId),
                children: [
                    t('View on BscScan'),
                    ": ",
                    (0,truncateHash/* default */.Z)(txHash, 8, 0)
                ]
            })
        ]
    }));
};
/* harmony default export */ const Toast_DescriptionWithTx = (DescriptionWithTx);

;// CONCATENATED MODULE: ./src/components/Toast/index.tsx





/***/ }),

/***/ 5083:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "IG": () => (/* reexport */ Listener),
  "iv": () => (/* reexport */ ToastsContext),
  "d0": () => (/* reexport */ ToastsProvider)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "lodash/kebabCase"
var kebabCase_ = __webpack_require__(1546);
var kebabCase_default = /*#__PURE__*/__webpack_require__.n(kebabCase_);
// EXTERNAL MODULE: ./src/components/Toast/index.tsx + 4 modules
var Toast = __webpack_require__(3937);
;// CONCATENATED MODULE: ./src/contexts/ToastsContext/Provider.tsx




const ToastsContext = /*#__PURE__*/ (0,external_react_.createContext)(undefined);
const ToastsProvider = ({ children  })=>{
    const { 0: toasts , 1: setToasts  } = (0,external_react_.useState)([]);
    const toast = (0,external_react_.useCallback)(({ title , description , type  })=>{
        setToasts((prevToasts)=>{
            const id = kebabCase_default()(title);
            // Remove any existing toasts with the same id
            const currentToasts = prevToasts.filter((prevToast)=>prevToast.id !== id
            );
            return [
                {
                    id,
                    title,
                    description,
                    type
                },
                ...currentToasts, 
            ];
        });
    }, [
        setToasts
    ]);
    const toastError = (title, description)=>{
        return toast({
            title,
            description,
            type: Toast/* toastTypes.DANGER */.m$.DANGER
        });
    };
    const toastInfo = (title, description)=>{
        return toast({
            title,
            description,
            type: Toast/* toastTypes.INFO */.m$.INFO
        });
    };
    const toastSuccess = (title, description)=>{
        return toast({
            title,
            description,
            type: Toast/* toastTypes.SUCCESS */.m$.SUCCESS
        });
    };
    const toastWarning = (title, description)=>{
        return toast({
            title,
            description,
            type: Toast/* toastTypes.WARNING */.m$.WARNING
        });
    };
    const clear = ()=>setToasts([])
    ;
    const remove = (id)=>{
        setToasts((prevToasts)=>prevToasts.filter((prevToast)=>prevToast.id !== id
            )
        );
    };
    return(/*#__PURE__*/ jsx_runtime_.jsx(ToastsContext.Provider, {
        value: {
            toasts,
            clear,
            remove,
            toastError,
            toastInfo,
            toastSuccess,
            toastWarning
        },
        children: children
    }));
};

// EXTERNAL MODULE: ./src/hooks/useToast.ts
var useToast = __webpack_require__(789);
;// CONCATENATED MODULE: ./src/contexts/ToastsContext/Listener.tsx




const ToastListener = ()=>{
    const { toasts , remove  } = (0,useToast/* default */.Z)();
    const handleRemove = (id)=>remove(id)
    ;
    return(/*#__PURE__*/ jsx_runtime_.jsx(Toast/* ToastContainer */.Ix, {
        toasts: toasts,
        onRemove: handleRemove
    }));
};
/* harmony default export */ const Listener = (ToastListener);

;// CONCATENATED MODULE: ./src/contexts/ToastsContext/index.tsx




/***/ }),

/***/ 5611:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ useENS)
});

// EXTERNAL MODULE: ./src/utils/index.ts + 1 modules
var utils = __webpack_require__(8328);
// EXTERNAL MODULE: external "@ethersproject/hash"
var hash_ = __webpack_require__(750);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./src/state/multicall/hooks.ts
var hooks = __webpack_require__(1001);
// EXTERNAL MODULE: ./src/utils/isZero.ts
var isZero = __webpack_require__(3010);
// EXTERNAL MODULE: ./src/hooks/useContract.ts + 2 modules
var useContract = __webpack_require__(6063);
// EXTERNAL MODULE: ./src/hooks/useDebounce.ts
var useDebounce = __webpack_require__(5999);
;// CONCATENATED MODULE: ./src/hooks/ENS/useENSAddress.ts






/**
 * Does a lookup for an ENS name to find its address.
 */ function useENSAddress(ensName) {
    const debouncedName = (0,useDebounce/* default */.Z)(ensName, 200);
    const ensNodeArgument = (0,external_react_.useMemo)(()=>{
        if (!debouncedName) return [
            undefined
        ];
        try {
            return debouncedName ? [
                (0,hash_.namehash)(debouncedName)
            ] : [
                undefined
            ];
        } catch (error) {
            return [
                undefined
            ];
        }
    }, [
        debouncedName
    ]);
    const registrarContract = (0,useContract/* useENSRegistrarContract */.zb)(false);
    const resolverAddress = (0,hooks/* useSingleCallResult */.Wk)(registrarContract, 'resolver', ensNodeArgument);
    const resolverAddressResult = resolverAddress.result?.[0];
    const resolverContract = (0,useContract/* useENSResolverContract */.uU)(resolverAddressResult && !(0,isZero/* default */.Z)(resolverAddressResult) ? resolverAddressResult : undefined, false);
    const addr = (0,hooks/* useSingleCallResult */.Wk)(resolverContract, 'addr', ensNodeArgument);
    const changed = debouncedName !== ensName;
    return {
        address: changed ? null : addr.result?.[0] ?? null,
        loading: changed || resolverAddress.loading || addr.loading
    };
};

;// CONCATENATED MODULE: ./src/hooks/ENS/useENSName.ts







/**
 * Does a reverse lookup for an address to find its ENS name.
 * Note this is not the same as looking up an ENS name to find an address.
 */ function useENSName(address) {
    const debouncedAddress = (0,useDebounce/* default */.Z)(address, 200);
    const ensNodeArgument = (0,external_react_.useMemo)(()=>{
        if (!debouncedAddress || !(0,utils/* isAddress */.UJ)(debouncedAddress)) return [
            undefined
        ];
        try {
            return debouncedAddress ? [
                (0,hash_.namehash)(`${debouncedAddress.toLowerCase().substr(2)}.addr.reverse`)
            ] : [
                undefined
            ];
        } catch (error) {
            return [
                undefined
            ];
        }
    }, [
        debouncedAddress
    ]);
    const registrarContract = (0,useContract/* useENSRegistrarContract */.zb)(false);
    const resolverAddress = (0,hooks/* useSingleCallResult */.Wk)(registrarContract, 'resolver', ensNodeArgument);
    const resolverAddressResult = resolverAddress.result?.[0];
    const resolverContract = (0,useContract/* useENSResolverContract */.uU)(resolverAddressResult && !(0,isZero/* default */.Z)(resolverAddressResult) ? resolverAddressResult : undefined, false);
    const name = (0,hooks/* useSingleCallResult */.Wk)(resolverContract, 'name', ensNodeArgument);
    const changed = debouncedAddress !== address;
    return {
        ENSName: changed ? null : name.result?.[0] ?? null,
        loading: changed || resolverAddress.loading || name.loading
    };
};

;// CONCATENATED MODULE: ./src/hooks/ENS/useENS.ts



/**
 * Given a name or address, does a lookup to resolve to an address and name
 * @param nameOrAddress ENS name or address
 */ function useENS(nameOrAddress) {
    const validated = (0,utils/* isAddress */.UJ)(nameOrAddress);
    const reverseLookup = useENSName(validated || undefined);
    const lookup = useENSAddress(nameOrAddress);
    return {
        loading: reverseLookup.loading || lookup.loading,
        address: validated || lookup.address,
        name: reverseLookup.ENSName ? reverseLookup.ENSName : !validated && lookup.address ? nameOrAddress || null : null
    };
};


/***/ }),

/***/ 3715:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "mP": () => (/* binding */ useIsTransactionUnsupported),
  "AU": () => (/* binding */ useTradeExactIn),
  "in": () => (/* binding */ useTradeExactOut)
});

// EXTERNAL MODULE: external "@pancakeswap/sdk"
var sdk_ = __webpack_require__(543);
// EXTERNAL MODULE: ./src/config/constants/index.ts
var constants = __webpack_require__(3862);
;// CONCATENATED MODULE: ./src/utils/trades.ts


// returns whether tradeB is better than tradeA by at least a threshold percentage amount
function isTradeBetter(tradeA, tradeB, minimumDelta = constants/* ZERO_PERCENT */.fI) {
    if (tradeA && !tradeB) return false;
    if (tradeB && !tradeA) return true;
    if (!tradeA || !tradeB) return undefined;
    if (tradeA.tradeType !== tradeB.tradeType || !(0,sdk_.currencyEquals)(tradeA.inputAmount.currency, tradeB.inputAmount.currency) || !(0,sdk_.currencyEquals)(tradeB.outputAmount.currency, tradeB.outputAmount.currency)) {
        throw new Error('Trades are not comparable');
    }
    if (minimumDelta.equalTo(constants/* ZERO_PERCENT */.fI)) {
        return tradeA.executionPrice.lessThan(tradeB.executionPrice);
    }
    return tradeA.executionPrice.raw.multiply(minimumDelta.add(constants/* ONE_HUNDRED_PERCENT */.yC)).lessThan(tradeB.executionPrice);
}
/* harmony default export */ const trades = ((/* unused pure expression or super */ null && (isTradeBetter)));

// EXTERNAL MODULE: external "lodash/flatMap"
var flatMap_ = __webpack_require__(8190);
var flatMap_default = /*#__PURE__*/__webpack_require__.n(flatMap_);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./src/hooks/useActiveWeb3React.ts
var useActiveWeb3React = __webpack_require__(4011);
// EXTERNAL MODULE: ./src/state/user/hooks/index.tsx
var hooks = __webpack_require__(8605);
// EXTERNAL MODULE: ./src/hooks/usePairs.ts
var usePairs = __webpack_require__(4915);
// EXTERNAL MODULE: ./src/utils/wrappedCurrency.ts
var wrappedCurrency = __webpack_require__(3854);
// EXTERNAL MODULE: ./src/hooks/Tokens.ts
var Tokens = __webpack_require__(6435);
;// CONCATENATED MODULE: ./src/hooks/Trades.ts
/* eslint-disable no-param-reassign */ 









function useAllCommonPairs(currencyA, currencyB) {
    const { chainId  } = (0,useActiveWeb3React/* default */.Z)();
    const [tokenA, tokenB] = chainId ? [
        (0,wrappedCurrency/* wrappedCurrency */.pu)(currencyA, chainId),
        (0,wrappedCurrency/* wrappedCurrency */.pu)(currencyB, chainId)
    ] : [
        undefined,
        undefined
    ];
    const bases = (0,external_react_.useMemo)(()=>{
        if (!chainId) return [];
        const common = constants/* BASES_TO_CHECK_TRADES_AGAINST */.lM[chainId] ?? [];
        const additionalA = tokenA ? constants/* ADDITIONAL_BASES */.ck[chainId]?.[tokenA.address] ?? [] : [];
        const additionalB = tokenB ? constants/* ADDITIONAL_BASES */.ck[chainId]?.[tokenB.address] ?? [] : [];
        return [
            ...common,
            ...additionalA,
            ...additionalB
        ];
    }, [
        chainId,
        tokenA,
        tokenB
    ]);
    const basePairs = (0,external_react_.useMemo)(()=>flatMap_default()(bases, (base)=>bases.map((otherBase)=>[
                    base,
                    otherBase
                ]
            )
        )
    , [
        bases
    ]);
    const allPairCombinations = (0,external_react_.useMemo)(()=>tokenA && tokenB ? [
            // the direct pair
            [
                tokenA,
                tokenB
            ],
            // token A against all bases
            ...bases.map((base)=>[
                    tokenA,
                    base
                ]
            ),
            // token B against all bases
            ...bases.map((base)=>[
                    tokenB,
                    base
                ]
            ),
            // each base against all bases
            ...basePairs, 
        ].filter((tokens)=>Boolean(tokens[0] && tokens[1])
        ).filter(([t0, t1])=>t0.address !== t1.address
        ).filter(([tokenA_, tokenB_])=>{
            if (!chainId) return true;
            const customBases = constants/* CUSTOM_BASES */.IP[chainId];
            const customBasesA = customBases?.[tokenA_.address];
            const customBasesB = customBases?.[tokenB_.address];
            if (!customBasesA && !customBasesB) return true;
            if (customBasesA && !customBasesA.find((base)=>tokenB_.equals(base)
            )) return false;
            if (customBasesB && !customBasesB.find((base)=>tokenA_.equals(base)
            )) return false;
            return true;
        }) : []
    , [
        tokenA,
        tokenB,
        bases,
        basePairs,
        chainId
    ]);
    const allPairs = (0,usePairs/* usePairs */.z$)(allPairCombinations);
    // only pass along valid pairs, non-duplicated pairs
    return (0,external_react_.useMemo)(()=>Object.values(allPairs// filter out invalid pairs
        .filter((result)=>Boolean(result[0] === usePairs/* PairState.EXISTS */._G.EXISTS && result[1])
        )// filter out duplicated pairs
        .reduce((memo, [, curr])=>{
            memo[curr.liquidityToken.address] = memo[curr.liquidityToken.address] ?? curr;
            return memo;
        }, {}))
    , [
        allPairs
    ]);
}
const MAX_HOPS = 3;
/**
 * Returns the best trade for the exact amount of tokens in to the given token out
 */ function useTradeExactIn(currencyAmountIn, currencyOut) {
    const allowedPairs = useAllCommonPairs(currencyAmountIn?.currency, currencyOut);
    const [singleHopOnly] = (0,hooks/* useUserSingleHopOnly */.RO)();
    return (0,external_react_.useMemo)(()=>{
        if (currencyAmountIn && currencyOut && allowedPairs.length > 0) {
            if (singleHopOnly) {
                return sdk_.Trade.bestTradeExactIn(allowedPairs, currencyAmountIn, currencyOut, {
                    maxHops: 1,
                    maxNumResults: 1
                })[0] ?? null;
            }
            // search through trades with varying hops, find best trade out of them
            let bestTradeSoFar = null;
            for(let i = 1; i <= MAX_HOPS; i++){
                const currentTrade = sdk_.Trade.bestTradeExactIn(allowedPairs, currencyAmountIn, currencyOut, {
                    maxHops: i,
                    maxNumResults: 1
                })[0] ?? null;
                // if current trade is best yet, save it
                if (isTradeBetter(bestTradeSoFar, currentTrade, constants/* BETTER_TRADE_LESS_HOPS_THRESHOLD */.Ru)) {
                    bestTradeSoFar = currentTrade;
                }
            }
            return bestTradeSoFar;
        }
        return null;
    }, [
        allowedPairs,
        currencyAmountIn,
        currencyOut,
        singleHopOnly
    ]);
}
/**
 * Returns the best trade for the token in to the exact amount of token out
 */ function useTradeExactOut(currencyIn, currencyAmountOut) {
    const allowedPairs = useAllCommonPairs(currencyIn, currencyAmountOut?.currency);
    const [singleHopOnly] = (0,hooks/* useUserSingleHopOnly */.RO)();
    return (0,external_react_.useMemo)(()=>{
        if (currencyIn && currencyAmountOut && allowedPairs.length > 0) {
            if (singleHopOnly) {
                return sdk_.Trade.bestTradeExactOut(allowedPairs, currencyIn, currencyAmountOut, {
                    maxHops: 1,
                    maxNumResults: 1
                })[0] ?? null;
            }
            // search through trades with varying hops, find best trade out of them
            let bestTradeSoFar = null;
            for(let i = 1; i <= MAX_HOPS; i++){
                const currentTrade = sdk_.Trade.bestTradeExactOut(allowedPairs, currencyIn, currencyAmountOut, {
                    maxHops: i,
                    maxNumResults: 1
                })[0] ?? null;
                if (isTradeBetter(bestTradeSoFar, currentTrade, constants/* BETTER_TRADE_LESS_HOPS_THRESHOLD */.Ru)) {
                    bestTradeSoFar = currentTrade;
                }
            }
            return bestTradeSoFar;
        }
        return null;
    }, [
        currencyIn,
        currencyAmountOut,
        allowedPairs,
        singleHopOnly
    ]);
}
function useIsTransactionUnsupported(currencyIn, currencyOut) {
    const unsupportedTokens = (0,Tokens/* useUnsupportedTokens */.l6)();
    const { chainId  } = (0,useActiveWeb3React/* default */.Z)();
    const tokenIn = (0,wrappedCurrency/* wrappedCurrency */.pu)(currencyIn, chainId);
    const tokenOut = (0,wrappedCurrency/* wrappedCurrency */.pu)(currencyOut, chainId);
    // if unsupported list loaded & either token on list, mark as unsupported
    if (unsupportedTokens) {
        if (tokenIn && Object.keys(unsupportedTokens).includes(tokenIn.address)) {
            return true;
        }
        if (tokenOut && Object.keys(unsupportedTokens).includes(tokenOut.address)) {
            return true;
        }
    }
    return false;
}


/***/ }),

/***/ 8470:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _web3_react_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(8054);
/* harmony import */ var _web3_react_core__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_web3_react_core__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _binance_chain_bsc_connector__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8454);
/* harmony import */ var _binance_chain_bsc_connector__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_binance_chain_bsc_connector__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _web3_react_injected_connector__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6590);
/* harmony import */ var _web3_react_injected_connector__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_web3_react_injected_connector__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _web3_react_walletconnect_connector__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(9795);
/* harmony import */ var _web3_react_walletconnect_connector__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_web3_react_walletconnect_connector__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2829);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var utils_web3React__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(2338);
/* harmony import */ var utils_wallet__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(9058);
/* harmony import */ var hooks_useToast__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(789);
/* harmony import */ var state__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(4211);
/* harmony import */ var contexts_Localization__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(9150);
/* harmony import */ var _utils_clearUserStates__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(8382);












const useAuth = ()=>{
    const { t  } = (0,contexts_Localization__WEBPACK_IMPORTED_MODULE_10__/* .useTranslation */ .$G)();
    const dispatch = (0,state__WEBPACK_IMPORTED_MODULE_9__/* .useAppDispatch */ .TL)();
    const { chainId , activate , deactivate  } = (0,_web3_react_core__WEBPACK_IMPORTED_MODULE_1__.useWeb3React)();
    const { toastError  } = (0,hooks_useToast__WEBPACK_IMPORTED_MODULE_8__/* ["default"] */ .Z)();
    const login = (0,react__WEBPACK_IMPORTED_MODULE_0__.useCallback)((connectorID)=>{
        const connector = utils_web3React__WEBPACK_IMPORTED_MODULE_6__/* .connectorsByName */ .BA[connectorID];
        if (connector) {
            activate(connector, async (error)=>{
                if (error instanceof _web3_react_core__WEBPACK_IMPORTED_MODULE_1__.UnsupportedChainIdError) {
                    const hasSetup = await (0,utils_wallet__WEBPACK_IMPORTED_MODULE_7__/* .setupNetwork */ .Y)();
                    if (hasSetup) {
                        activate(connector);
                    }
                } else {
                    window.localStorage.removeItem(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_5__.connectorLocalStorageKey);
                    if (error instanceof _web3_react_injected_connector__WEBPACK_IMPORTED_MODULE_3__.NoEthereumProviderError || error instanceof _binance_chain_bsc_connector__WEBPACK_IMPORTED_MODULE_2__.NoBscProviderError) {
                        toastError(t('Provider Error'), t('No provider was found'));
                    } else if (error instanceof _web3_react_injected_connector__WEBPACK_IMPORTED_MODULE_3__.UserRejectedRequestError || error instanceof _web3_react_walletconnect_connector__WEBPACK_IMPORTED_MODULE_4__.UserRejectedRequestError) {
                        if (connector instanceof _web3_react_walletconnect_connector__WEBPACK_IMPORTED_MODULE_4__.WalletConnectConnector) {
                            const walletConnector = connector;
                            walletConnector.walletConnectProvider = null;
                        }
                        toastError(t('Authorization Error'), t('Please authorize to access your account'));
                    } else {
                        toastError(error.name, error.message);
                    }
                }
            });
        } else {
            toastError(t('Unable to find connector'), t('The connector config is wrong'));
        }
    }, [
        t,
        activate,
        toastError
    ]);
    const logout = (0,react__WEBPACK_IMPORTED_MODULE_0__.useCallback)(()=>{
        deactivate();
        (0,_utils_clearUserStates__WEBPACK_IMPORTED_MODULE_11__/* .clearUserStates */ .J)(dispatch, chainId);
    }, [
        deactivate,
        dispatch,
        chainId
    ]);
    return {
        login,
        logout
    };
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (useAuth);


/***/ }),

/***/ 8339:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ hooks_useFetchListCallback)
});

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: external "@pancakeswap/sdk"
var sdk_ = __webpack_require__(543);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(6022);
// EXTERNAL MODULE: ./src/hooks/useActiveWeb3React.ts
var useActiveWeb3React = __webpack_require__(4011);
// EXTERNAL MODULE: ./src/state/lists/actions.ts
var actions = __webpack_require__(8412);
// EXTERNAL MODULE: external "@ethersproject/contracts"
var contracts_ = __webpack_require__(2792);
// EXTERNAL MODULE: external "@ethersproject/hash"
var hash_ = __webpack_require__(750);
;// CONCATENATED MODULE: ./src/utils/ENS/resolveENSContentHash.ts


const REGISTRAR_ABI = [
    {
        constant: true,
        inputs: [
            {
                name: 'node',
                type: 'bytes32'
            }, 
        ],
        name: 'resolver',
        outputs: [
            {
                name: 'resolverAddress',
                type: 'address'
            }, 
        ],
        payable: false,
        stateMutability: 'view',
        type: 'function'
    }, 
];
const REGISTRAR_ADDRESS = '0x00000000000C2E074eC69A0dFb2997BA6C7d2e1e';
const RESOLVER_ABI = [
    {
        constant: true,
        inputs: [
            {
                internalType: 'bytes32',
                name: 'node',
                type: 'bytes32'
            }, 
        ],
        name: 'contenthash',
        outputs: [
            {
                internalType: 'bytes',
                name: '',
                type: 'bytes'
            }, 
        ],
        payable: false,
        stateMutability: 'view',
        type: 'function'
    }, 
];
// cache the resolver contracts since most of them are the public resolver
function resolverContract(resolverAddress, provider) {
    return new contracts_.Contract(resolverAddress, RESOLVER_ABI, provider);
}
/**
 * Fetches and decodes the result of an ENS contenthash lookup on mainnet to a URI
 * @param ensName to resolve
 * @param provider provider to use to fetch the data
 */ async function resolveENSContentHash(ensName, provider) {
    const ensRegistrarContract = new contracts_.Contract(REGISTRAR_ADDRESS, REGISTRAR_ABI, provider);
    const hash = (0,hash_.namehash)(ensName);
    const resolverAddress = await ensRegistrarContract.resolver(hash);
    return resolverContract(resolverAddress, provider).contenthash(hash);
};

;// CONCATENATED MODULE: ./src/hooks/useFetchListCallback.ts








function useFetchListCallback() {
    const { library  } = (0,useActiveWeb3React/* default */.Z)();
    const { chainId  } = (0,useActiveWeb3React/* default */.Z)();
    const dispatch = (0,external_react_redux_.useDispatch)();
    const ensResolver = (0,external_react_.useCallback)((ensName)=>{
        if (chainId !== sdk_.ChainId.MAINNET) {
            throw new Error('Could not construct mainnet ENS resolver');
        }
        return resolveENSContentHash(ensName, library);
    }, [
        chainId,
        library
    ]);
    // note: prevent dispatch if using for list search or unsupported list
    return (0,external_react_.useCallback)(async (listUrl, sendDispatch = true)=>{
        const requestId = (0,toolkit_.nanoid)();
        if (sendDispatch) {
            dispatch(actions/* fetchTokenList.pending */.Dn.pending({
                requestId,
                url: listUrl
            }));
        }
        // lazy load avj and token list schema
        const getTokenList = (await Promise.all(/* import() */[__webpack_require__.e(475), __webpack_require__.e(402), __webpack_require__.e(555)]).then(__webpack_require__.bind(__webpack_require__, 3555))).default;
        return getTokenList(listUrl, ensResolver).then((tokenList)=>{
            if (sendDispatch) {
                dispatch(actions/* fetchTokenList.fulfilled */.Dn.fulfilled({
                    url: listUrl,
                    tokenList,
                    requestId
                }));
            }
            return tokenList;
        }).catch((error)=>{
            console.error(`Failed to get list at url ${listUrl}`, error);
            if (sendDispatch) {
                dispatch(actions/* fetchTokenList.rejected */.Dn.rejected({
                    url: listUrl,
                    requestId,
                    errorMessage: error.message
                }));
            }
            throw error;
        });
    }, [
        dispatch,
        ensResolver
    ]);
}
/* harmony default export */ const hooks_useFetchListCallback = (useFetchListCallback);


/***/ }),

/***/ 789:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var contexts_ToastsContext__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5083);


const useToast = ()=>{
    const toastContext = (0,react__WEBPACK_IMPORTED_MODULE_0__.useContext)(contexts_ToastsContext__WEBPACK_IMPORTED_MODULE_1__/* .ToastsContext */ .iv);
    if (toastContext === undefined) {
        throw new Error('Toasts context undefined');
    }
    return toastContext;
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (useToast);


/***/ }),

/***/ 1990:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "eo": () => (/* binding */ tryParseAmount),
  "jj": () => (/* binding */ useDefaultsFromURLSearch),
  "SM": () => (/* binding */ useDerivedSwapInfo),
  "rH": () => (/* binding */ useFetchPairPrices),
  "yF": () => (/* binding */ useSingleTokenSwapInfo),
  "_r": () => (/* binding */ useSwapActionHandlers),
  "dU": () => (/* binding */ useSwapState)
});

// UNUSED EXPORTS: queryParametersToSwapState

// EXTERNAL MODULE: external "@ethersproject/units"
var units_ = __webpack_require__(3138);
// EXTERNAL MODULE: external "@pancakeswap/sdk"
var sdk_ = __webpack_require__(543);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(6022);
// EXTERNAL MODULE: ./src/hooks/ENS/useENS.ts + 2 modules
var useENS = __webpack_require__(5611);
// EXTERNAL MODULE: ./src/hooks/useActiveWeb3React.ts
var useActiveWeb3React = __webpack_require__(4011);
// EXTERNAL MODULE: ./src/hooks/Tokens.ts
var Tokens = __webpack_require__(6435);
// EXTERNAL MODULE: ./src/hooks/Trades.ts + 1 modules
var Trades = __webpack_require__(3715);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
;// CONCATENATED MODULE: ./src/hooks/useParsedQueryString.ts

function useParsedQueryString() {
    const { query  } = (0,router_.useRouter)();
    return query;
};

// EXTERNAL MODULE: ./src/contexts/Localization/index.tsx + 3 modules
var Localization = __webpack_require__(9150);
// EXTERNAL MODULE: ./src/utils/index.ts + 1 modules
var utils = __webpack_require__(8328);
// EXTERNAL MODULE: ./src/utils/prices.ts
var prices = __webpack_require__(7879);
;// CONCATENATED MODULE: ./src/utils/getLpAddress.ts


const getLpAddress = (token1, token2)=>{
    let token1AsTokenInstance = token1;
    let token2AsTokenInstance = token2;
    if (!token1 || !token2) {
        return null;
    }
    if (typeof token1 === 'string' || token1 instanceof String) {
        const checksummedToken1Address = (0,utils/* isAddress */.UJ)(token1);
        if (!checksummedToken1Address) {
            return null;
        }
        token1AsTokenInstance = new sdk_.Token(sdk_.ChainId.MAINNET, checksummedToken1Address, 18);
    }
    if (typeof token2 === 'string' || token2 instanceof String) {
        const checksummedToken2Address = (0,utils/* isAddress */.UJ)(token2);
        if (!checksummedToken2Address) {
            return null;
        }
        token2AsTokenInstance = new sdk_.Token(sdk_.ChainId.MAINNET, checksummedToken2Address, 18);
    }
    return sdk_.Pair.getAddress(token1AsTokenInstance, token2AsTokenInstance);
};
/* harmony default export */ const utils_getLpAddress = (getLpAddress);

// EXTERNAL MODULE: ./src/views/Swap/components/Chart/utils.ts
var Chart_utils = __webpack_require__(9197);
// EXTERNAL MODULE: ./src/state/wallet/hooks.ts
var hooks = __webpack_require__(1900);
// EXTERNAL MODULE: ./src/state/swap/actions.ts
var actions = __webpack_require__(193);
// EXTERNAL MODULE: ./src/state/user/hooks/index.tsx
var user_hooks = __webpack_require__(8605);
// EXTERNAL MODULE: ./src/utils/requestWithTimeout.ts
var requestWithTimeout = __webpack_require__(261);
// EXTERNAL MODULE: ./src/utils/graphql.ts
var graphql = __webpack_require__(5509);
// EXTERNAL MODULE: external "graphql-request"
var external_graphql_request_ = __webpack_require__(5805);
;// CONCATENATED MODULE: ./src/state/swap/queries/lastPairDayId.ts

const lastPairDayId = external_graphql_request_.gql`
  query lastPairDayId($pairId: String) {
    pairDayDatas(first: 1, where: { pairAddress: $pairId }, orderBy: date, orderDirection: desc) {
      id
    }
  }
`;
/* harmony default export */ const queries_lastPairDayId = (lastPairDayId);

;// CONCATENATED MODULE: ./src/state/swap/queries/pairHourDatas.ts

const pairHourDatas = external_graphql_request_.gql`
  query pairHourDatas($pairId: String, $first: Int) {
    pairHourDatas(first: $first, where: { pair: $pairId }, orderBy: hourStartUnix, orderDirection: desc) {
      id
      hourStartUnix
      reserve0
      reserve1
      reserveUSD
      pair {
        token0 {
          id
        }
        token1 {
          id
        }
      }
    }
  }
`;
/* harmony default export */ const queries_pairHourDatas = (pairHourDatas);

;// CONCATENATED MODULE: ./src/state/swap/queries/pairDayDatasByIdsQuery.ts

const pairDayDatasByIdsQuery = external_graphql_request_.gql`
  query pairDayDatasByIdsQuery($pairIds: [String]) {
    pairDayDatas(where: { id_in: $pairIds }, orderBy: date, orderDirection: desc) {
      id
      date
      reserve0
      reserve1
      reserveUSD
      pairAddress {
        token0 {
          id
        }
        token1 {
          id
        }
      }
    }
  }
`;
/* harmony default export */ const queries_pairDayDatasByIdsQuery = (pairDayDatasByIdsQuery);

;// CONCATENATED MODULE: ./src/state/swap/types.ts
var PairDataTimeWindowEnum;
(function(PairDataTimeWindowEnum) {
    PairDataTimeWindowEnum[PairDataTimeWindowEnum["DAY"] = 0] = "DAY";
    PairDataTimeWindowEnum[PairDataTimeWindowEnum["WEEK"] = 1] = "WEEK";
    PairDataTimeWindowEnum[PairDataTimeWindowEnum["MONTH"] = 2] = "MONTH";
    PairDataTimeWindowEnum[PairDataTimeWindowEnum["YEAR"] = 3] = "YEAR";
})(PairDataTimeWindowEnum || (PairDataTimeWindowEnum = {}));

;// CONCATENATED MODULE: ./src/state/swap/fetch/constants.ts

// Specifies the amount of data points to query for specific time window
const timeWindowIdsCountMapping = {
    [PairDataTimeWindowEnum.DAY]: 24,
    [PairDataTimeWindowEnum.WEEK]: 28,
    [PairDataTimeWindowEnum.MONTH]: 30,
    [PairDataTimeWindowEnum.YEAR]: 24
};
// How many StreamingFast ids to skip when querying the data
const timeWindowGapMapping = {
    [PairDataTimeWindowEnum.DAY]: null,
    [PairDataTimeWindowEnum.WEEK]: 6,
    [PairDataTimeWindowEnum.MONTH]: 1,
    [PairDataTimeWindowEnum.YEAR]: 15
};

// EXTERNAL MODULE: external "lodash/times"
var times_ = __webpack_require__(4354);
var times_default = /*#__PURE__*/__webpack_require__.n(times_);
;// CONCATENATED MODULE: ./src/state/swap/fetch/utils.ts



const getPairSequentialId = ({ id , pairId  })=>id.replace(`${pairId}-`, '')
;
const getIdsByTimeWindow = ({ pairAddress , pairLastId , timeWindow , idsCount  })=>{
    const pairLastIdAsNumber = Number(pairLastId);
    if (timeWindow === PairDataTimeWindowEnum.DAY) {
        return [];
    }
    return times_default()(idsCount, (value)=>`${pairAddress}-${pairLastIdAsNumber - value * timeWindowGapMapping[timeWindow]}`
    );
};
const pairHasEnoughLiquidity = (data, timeWindow)=>{
    const liquidityThreshold = 10000;
    switch(timeWindow){
        case PairDataTimeWindowEnum.DAY:
        case PairDataTimeWindowEnum.WEEK:
            {
                const amountOfDataPoints = data?.pairHourDatas?.length ?? 1;
                const totalUSD = data?.pairHourDatas?.reduce((totalLiquidity, fetchPairEntry)=>{
                    return totalLiquidity + parseFloat(fetchPairEntry.reserveUSD);
                }, 0);
                return totalUSD / amountOfDataPoints > liquidityThreshold;
            }
        case PairDataTimeWindowEnum.MONTH:
        case PairDataTimeWindowEnum.YEAR:
            {
                const amountOfDataPoints = data?.pairDayDatas?.length ?? 1;
                const totalUSD = data?.pairDayDatas?.reduce((totalLiquidity, fetchPairEntry)=>{
                    return totalLiquidity + parseFloat(fetchPairEntry.reserveUSD);
                }, 0);
                return totalUSD / amountOfDataPoints > liquidityThreshold;
            }
        default:
            return null;
    }
};

;// CONCATENATED MODULE: ./src/state/swap/queries/pairDayDatas.ts

const pairDayDatas = external_graphql_request_.gql`
  query pairDayDatas($pairId: String, $first: Int) {
    pairDayDatas(first: $first, where: { pairAddress: $pairId }, orderBy: date, orderDirection: desc) {
      id
      date
      reserve0
      reserve1
      reserveUSD
      pairAddress {
        token0 {
          id
        }
        token1 {
          id
        }
      }
    }
  }
`;
/* harmony default export */ const queries_pairDayDatas = (pairDayDatas);

;// CONCATENATED MODULE: ./src/state/swap/queries/pairHourDatasByIds.ts

const pairHourDatasByIds = external_graphql_request_.gql`
  query pairHourDatasByIds($pairIds: [String]) {
    pairHourDatas(where: { id_in: $pairIds }, orderBy: hourStartUnix, orderDirection: desc) {
      id
      hourStartUnix
      reserve0
      reserve1
      reserveUSD
      pair {
        token0 {
          id
        }
        token1 {
          id
        }
      }
    }
  }
`;
/* harmony default export */ const queries_pairHourDatasByIds = (pairHourDatasByIds);

;// CONCATENATED MODULE: ./src/state/swap/queries/lastPairHourId.ts

const lastPairHourId = external_graphql_request_.gql`
  query lastPairHourId($pairId: String) {
    pairHourDatas(first: 1, where: { pair: $pairId }, orderBy: hourStartUnix, orderDirection: desc) {
      id
    }
  }
`;
/* harmony default export */ const queries_lastPairHourId = (lastPairHourId);

;// CONCATENATED MODULE: ./src/state/swap/fetch/fetchPairPriceData.ts











const fetchPairPriceData = async ({ pairId , timeWindow  })=>{
    const client = graphql/* infoClient */.d;
    try {
        switch(timeWindow){
            case PairDataTimeWindowEnum.DAY:
                {
                    const data = await (0,requestWithTimeout/* default */.Z)(client, queries_pairHourDatas, {
                        pairId,
                        first: timeWindowIdsCountMapping[timeWindow]
                    });
                    return {
                        data,
                        error: false
                    };
                }
            case PairDataTimeWindowEnum.WEEK:
                {
                    const lastPairHourIdData = await (0,requestWithTimeout/* default */.Z)(client, queries_lastPairHourId, {
                        pairId
                    });
                    const lastId = lastPairHourIdData?.pairHourDatas ? lastPairHourIdData.pairHourDatas[0]?.id : null;
                    if (!lastId) {
                        return {
                            data: {
                                pairHourDatas: []
                            },
                            error: false
                        };
                    }
                    const pairHourId = getPairSequentialId({
                        id: lastId,
                        pairId
                    });
                    const pairHourIds = getIdsByTimeWindow({
                        pairAddress: pairId,
                        pairLastId: pairHourId,
                        timeWindow,
                        idsCount: timeWindowIdsCountMapping[timeWindow]
                    });
                    const pairHoursData = await (0,requestWithTimeout/* default */.Z)(client, queries_pairHourDatasByIds, {
                        pairIds: pairHourIds
                    });
                    return {
                        data: pairHoursData,
                        error: false
                    };
                }
            case PairDataTimeWindowEnum.MONTH:
                {
                    const data = await (0,requestWithTimeout/* default */.Z)(client, queries_pairDayDatas, {
                        pairId,
                        first: timeWindowIdsCountMapping[timeWindow]
                    });
                    return {
                        data,
                        error: false
                    };
                }
            case PairDataTimeWindowEnum.YEAR:
                {
                    const lastPairDayIdData = await (0,requestWithTimeout/* default */.Z)(client, queries_lastPairDayId, {
                        pairId
                    });
                    const lastId = lastPairDayIdData?.pairDayDatas ? lastPairDayIdData.pairDayDatas[0]?.id : null;
                    if (!lastId) {
                        return {
                            data: {
                                pairDayDatas: []
                            },
                            error: false
                        };
                    }
                    const pairLastId = getPairSequentialId({
                        id: lastId,
                        pairId
                    });
                    const pairDayIds = getIdsByTimeWindow({
                        pairAddress: pairId,
                        pairLastId,
                        timeWindow,
                        idsCount: timeWindowIdsCountMapping[timeWindow]
                    });
                    const pairDayData = await (0,requestWithTimeout/* default */.Z)(client, queries_pairDayDatasByIdsQuery, {
                        pairIds: pairDayIds
                    });
                    return {
                        data: pairDayData,
                        error: false
                    };
                }
            default:
                return {
                    data: null,
                    error: false
                };
        }
    } catch (error) {
        console.error('Failed to fetch price chart data', error);
        return {
            error: true
        };
    }
};
/* harmony default export */ const fetch_fetchPairPriceData = (fetchPairPriceData);

// EXTERNAL MODULE: external "date-fns"
var external_date_fns_ = __webpack_require__(4146);
;// CONCATENATED MODULE: ./src/state/swap/normalizers.ts


const normalizeChartData = (data, timeWindow)=>{
    switch(timeWindow){
        case PairDataTimeWindowEnum.DAY:
        case PairDataTimeWindowEnum.WEEK:
            return data?.pairHourDatas?.map((fetchPairEntry)=>({
                    time: fetchPairEntry.hourStartUnix,
                    token0Id: fetchPairEntry.pair.token0.id,
                    token1Id: fetchPairEntry.pair.token1.id,
                    reserve0: parseFloat(fetchPairEntry.reserve0),
                    reserve1: parseFloat(fetchPairEntry.reserve1)
                })
            );
        case PairDataTimeWindowEnum.MONTH:
        case PairDataTimeWindowEnum.YEAR:
            return data?.pairDayDatas?.map((fetchPairEntry)=>({
                    time: fetchPairEntry.date,
                    token0Id: fetchPairEntry.pairAddress.token0.id,
                    token1Id: fetchPairEntry.pairAddress.token1.id,
                    reserve0: parseFloat(fetchPairEntry.reserve0),
                    reserve1: parseFloat(fetchPairEntry.reserve1)
                })
            );
        default:
            return null;
    }
};
const normalizeDerivedChartData = (data)=>{
    if (!data?.token0DerivedBnb || data?.token0DerivedBnb.length === 0) {
        return [];
    }
    return data?.token0DerivedBnb.reduce((acc, token0DerivedBnbEntry)=>{
        const token1DerivedBnbEntry = data?.token1DerivedBnb?.find((entry)=>entry.timestamp === token0DerivedBnbEntry.timestamp
        );
        if (!token1DerivedBnbEntry) {
            return acc;
        }
        return [
            ...acc,
            {
                time: parseInt(token0DerivedBnbEntry.timestamp, 10),
                token0Id: token0DerivedBnbEntry.tokenAddress,
                token1Id: token1DerivedBnbEntry.tokenAddress,
                token0DerivedBNB: token0DerivedBnbEntry.derivedBNB,
                token1DerivedBNB: token1DerivedBnbEntry.derivedBNB
            }, 
        ];
    }, []);
};
const normalizePairDataByActiveToken = ({ pairData , activeToken  })=>pairData?.map((pairPrice)=>({
            time: (0,external_date_fns_.fromUnixTime)(pairPrice.time),
            value: activeToken === pairPrice?.token0Id ? pairPrice.reserve1 / pairPrice.reserve0 : pairPrice.reserve0 / pairPrice.reserve1
        })
    ).reverse()
;
const normalizeDerivedPairDataByActiveToken = ({ pairData , activeToken  })=>pairData?.map((pairPrice)=>({
            time: (0,external_date_fns_.fromUnixTime)(pairPrice.time),
            value: activeToken === pairPrice?.token0Id ? pairPrice.token0DerivedBNB / pairPrice.token1DerivedBNB : pairPrice.token1DerivedBNB / pairPrice.token0DerivedBNB
        })
    )
;

// EXTERNAL MODULE: external "lodash/get"
var get_ = __webpack_require__(1712);
var get_default = /*#__PURE__*/__webpack_require__.n(get_);
;// CONCATENATED MODULE: ./src/state/swap/selectors.ts

const pairByDataIdSelector = ({ pairId , timeWindow  })=>(state)=>get_default()(state, [
            'swap',
            'pairDataById',
            pairId,
            timeWindow
        ])
;
const derivedPairByDataIdSelector = ({ pairId , timeWindow  })=>(state)=>get_default()(state, [
            'swap',
            'derivedPairDataById',
            pairId,
            timeWindow
        ])
;

;// CONCATENATED MODULE: ./src/state/swap/constants.ts
// BNB
const DEFAULT_INPUT_CURRENCY = 'BNB';
// BUSD
const DEFAULT_OUTPUT_CURRENCY = '';

// EXTERNAL MODULE: ./src/config/constants/endpoints.ts
var endpoints = __webpack_require__(5906);
// EXTERNAL MODULE: ./src/config/constants/info.ts
var info = __webpack_require__(4003);
// EXTERNAL MODULE: ./src/views/Info/hooks/useBlocksFromTimestamps.ts
var useBlocksFromTimestamps = __webpack_require__(9566);
// EXTERNAL MODULE: ./src/views/Info/utils/infoQueryHelpers.ts
var infoQueryHelpers = __webpack_require__(5126);
;// CONCATENATED MODULE: ./src/state/swap/queries/getDerivedPrices.ts

const getDerivedPrices = (tokenAddress, blocks)=>blocks.map((block)=>`
    t${block.timestamp}:token(id:"${tokenAddress}", block: { number: ${block.number} }) { 
        derivedBNB
      }
    `
    )
;
const getDerivedPricesQueryConstructor = (subqueries)=>{
    return external_graphql_request_.gql`
      query derivedTokenPriceData {
        ${subqueries}
      }
    `;
};

;// CONCATENATED MODULE: ./src/state/swap/fetch/fetchDerivedPriceData.ts







const getTokenDerivedBnbPrices = async (tokenAddress, blocks)=>{
    const prices = await (0,infoQueryHelpers/* multiQuery */.L)(getDerivedPricesQueryConstructor, getDerivedPrices(tokenAddress, blocks), endpoints/* INFO_CLIENT */.JY, 200);
    if (!prices) {
        console.error('Price data failed to load');
        return null;
    }
    // format token BNB price results
    const tokenPrices = [];
    // Get Token prices in BNB
    Object.keys(prices).forEach((priceKey)=>{
        const timestamp = priceKey.split('t')[1];
        if (timestamp) {
            tokenPrices.push({
                tokenAddress,
                timestamp,
                derivedBNB: prices[priceKey]?.derivedBNB ? parseFloat(prices[priceKey].derivedBNB) : 0
            });
        }
    });
    tokenPrices.sort((a, b)=>parseInt(a.timestamp, 10) - parseInt(b.timestamp, 10)
    );
    return tokenPrices;
};
const getInterval = (timeWindow)=>{
    switch(timeWindow){
        case PairDataTimeWindowEnum.DAY:
            return info/* ONE_HOUR_SECONDS */.Tb;
        case PairDataTimeWindowEnum.WEEK:
            return info/* ONE_HOUR_SECONDS */.Tb * 4;
        case PairDataTimeWindowEnum.MONTH:
            return info/* ONE_DAY_UNIX */.Bq;
        case PairDataTimeWindowEnum.YEAR:
            return info/* ONE_DAY_UNIX */.Bq * 15;
        default:
            return info/* ONE_HOUR_SECONDS */.Tb * 4;
    }
};
const getSkipDaysToStart = (timeWindow)=>{
    switch(timeWindow){
        case PairDataTimeWindowEnum.DAY:
            return 1;
        case PairDataTimeWindowEnum.WEEK:
            return 7;
        case PairDataTimeWindowEnum.MONTH:
            return 30;
        case PairDataTimeWindowEnum.YEAR:
            return 365;
        default:
            return 7;
    }
};
// Fetches derivedBnb values for tokens to calculate derived price
// Used when no direct pool is available
const fetchDerivedPriceData = async (token0Address, token1Address, timeWindow)=>{
    const interval = getInterval(timeWindow);
    const endTimestamp = (0,external_date_fns_.getUnixTime)(new Date());
    const startTimestamp = (0,external_date_fns_.getUnixTime)((0,external_date_fns_.startOfHour)((0,external_date_fns_.sub)(endTimestamp * 1000, {
        days: getSkipDaysToStart(timeWindow)
    })));
    const timestamps = [];
    let time = startTimestamp;
    while(time <= endTimestamp){
        timestamps.push(time);
        time += interval;
    }
    try {
        const blocks = await (0,useBlocksFromTimestamps/* getBlocksFromTimestamps */.z)(timestamps, 'asc', 500);
        if (!blocks || blocks.length === 0) {
            console.error('Error fetching blocks for timestamps', timestamps);
            return null;
        }
        const token0DerivedBnb = await getTokenDerivedBnbPrices(token0Address, blocks);
        const token1DerivedBnb = await getTokenDerivedBnbPrices(token1Address, blocks);
        return {
            token0DerivedBnb,
            token1DerivedBnb
        };
    } catch (error) {
        console.error('Failed to fetched derived price data for chart', error);
        return null;
    }
};
/* harmony default export */ const fetch_fetchDerivedPriceData = (fetchDerivedPriceData);

;// CONCATENATED MODULE: ./src/state/swap/hooks.ts























function useSwapState() {
    return (0,external_react_redux_.useSelector)((state)=>state.swap
    );
}
function useSwapActionHandlers() {
    const dispatch = (0,external_react_redux_.useDispatch)();
    const onCurrencySelection = (0,external_react_.useCallback)((field, currency)=>{
        dispatch((0,actions/* selectCurrency */.j)({
            field,
            currencyId: currency instanceof sdk_.Token ? currency.address : currency === sdk_.ETHER ? 'BNB' : ''
        }));
    }, [
        dispatch
    ]);
    const onSwitchTokens = (0,external_react_.useCallback)(()=>{
        dispatch((0,actions/* switchCurrencies */.KS)());
    }, [
        dispatch
    ]);
    const onUserInput = (0,external_react_.useCallback)((field, typedValue)=>{
        dispatch((0,actions/* typeInput */.LC)({
            field,
            typedValue
        }));
    }, [
        dispatch
    ]);
    const onChangeRecipient = (0,external_react_.useCallback)((recipient)=>{
        dispatch((0,actions/* setRecipient */.He)({
            recipient
        }));
    }, [
        dispatch
    ]);
    return {
        onSwitchTokens,
        onCurrencySelection,
        onUserInput,
        onChangeRecipient
    };
}
// try to parse a user entered amount for a given token
function tryParseAmount(value, currency) {
    if (!value || !currency) {
        return undefined;
    }
    try {
        const typedValueParsed = (0,units_.parseUnits)(value, currency.decimals).toString();
        if (typedValueParsed !== '0') {
            return currency instanceof sdk_.Token ? new sdk_.TokenAmount(currency, sdk_.JSBI.BigInt(typedValueParsed)) : sdk_.CurrencyAmount.ether(sdk_.JSBI.BigInt(typedValueParsed));
        }
    } catch (error) {
        // should fail if the user specifies too many decimal places of precision (or maybe exceed max uint?)
        console.debug(`Failed to parse input amount: "${value}"`, error);
    }
    // necessary for all paths to return a value
    return undefined;
}
const BAD_RECIPIENT_ADDRESSES = [
    '0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f',
    '0xf164fC0Ec4E93095b804a4795bBe1e041497b92a',
    '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D'
];
/**
 * Returns true if any of the pairs or tokens in a trade have the given checksummed address
 * @param trade to check for the given address
 * @param checksummedAddress address to check in the pairs and tokens
 */ function involvesAddress(trade, checksummedAddress) {
    return trade.route.path.some((token)=>token.address === checksummedAddress
    ) || trade.route.pairs.some((pair)=>pair.liquidityToken.address === checksummedAddress
    );
}
// Get swap price for single token disregarding slippage and price impact
function useSingleTokenSwapInfo() {
    const { [actions/* Field.INPUT */.gN.INPUT]: { currencyId: inputCurrencyId  } , [actions/* Field.OUTPUT */.gN.OUTPUT]: { currencyId: outputCurrencyId  } ,  } = useSwapState();
    const inputCurrency = (0,Tokens/* useCurrency */.U8)(inputCurrencyId);
    const outputCurrency = (0,Tokens/* useCurrency */.U8)(outputCurrencyId);
    const token0Address = (0,Chart_utils/* getTokenAddress */.D)(inputCurrencyId);
    const token1Address = (0,Chart_utils/* getTokenAddress */.D)(outputCurrencyId);
    const parsedAmount = tryParseAmount('1', inputCurrency ?? undefined);
    const bestTradeExactIn = (0,Trades/* useTradeExactIn */.AU)(parsedAmount, outputCurrency ?? undefined);
    if (!inputCurrency || !outputCurrency || !bestTradeExactIn) {
        return null;
    }
    const inputTokenPrice = parseFloat(bestTradeExactIn?.executionPrice?.toSignificant(6));
    const outputTokenPrice = 1 / inputTokenPrice;
    return {
        [token0Address]: inputTokenPrice,
        [token1Address]: outputTokenPrice
    };
}
// from the current swap inputs, compute the best trade and return it.
function useDerivedSwapInfo() {
    const { account  } = (0,useActiveWeb3React/* default */.Z)();
    const { t  } = (0,Localization/* useTranslation */.$G)();
    const { independentField , typedValue , [actions/* Field.INPUT */.gN.INPUT]: { currencyId: inputCurrencyId  } , [actions/* Field.OUTPUT */.gN.OUTPUT]: { currencyId: outputCurrencyId  } , recipient ,  } = useSwapState();
    const inputCurrency = (0,Tokens/* useCurrency */.U8)(inputCurrencyId);
    const outputCurrency = (0,Tokens/* useCurrency */.U8)(outputCurrencyId);
    const recipientLookup = (0,useENS/* default */.Z)(recipient ?? undefined);
    const to = (recipient === null ? account : recipientLookup.address) ?? null;
    const relevantTokenBalances = (0,hooks/* useCurrencyBalances */.K5)(account ?? undefined, [
        inputCurrency ?? undefined,
        outputCurrency ?? undefined, 
    ]);
    const isExactIn = independentField === actions/* Field.INPUT */.gN.INPUT;
    const parsedAmount = tryParseAmount(typedValue, (isExactIn ? inputCurrency : outputCurrency) ?? undefined);
    const bestTradeExactIn = (0,Trades/* useTradeExactIn */.AU)(isExactIn ? parsedAmount : undefined, outputCurrency ?? undefined);
    const bestTradeExactOut = (0,Trades/* useTradeExactOut */["in"])(inputCurrency ?? undefined, !isExactIn ? parsedAmount : undefined);
    const v2Trade = isExactIn ? bestTradeExactIn : bestTradeExactOut;
    const currencyBalances = {
        [actions/* Field.INPUT */.gN.INPUT]: relevantTokenBalances[0],
        [actions/* Field.OUTPUT */.gN.OUTPUT]: relevantTokenBalances[1]
    };
    const currencies = {
        [actions/* Field.INPUT */.gN.INPUT]: inputCurrency ?? undefined,
        [actions/* Field.OUTPUT */.gN.OUTPUT]: outputCurrency ?? undefined
    };
    let inputError;
    if (!account) {
        inputError = t('Connect Wallet');
    }
    if (!parsedAmount) {
        inputError = inputError ?? t('Enter an amount');
    }
    if (!currencies[actions/* Field.INPUT */.gN.INPUT] || !currencies[actions/* Field.OUTPUT */.gN.OUTPUT]) {
        inputError = inputError ?? t('Select a token');
    }
    const formattedTo = (0,utils/* isAddress */.UJ)(to);
    if (!to || !formattedTo) {
        inputError = inputError ?? t('Enter a recipient');
    } else if (BAD_RECIPIENT_ADDRESSES.indexOf(formattedTo) !== -1 || bestTradeExactIn && involvesAddress(bestTradeExactIn, formattedTo) || bestTradeExactOut && involvesAddress(bestTradeExactOut, formattedTo)) {
        inputError = inputError ?? t('Invalid recipient');
    }
    const [allowedSlippage] = (0,user_hooks/* useUserSlippageTolerance */.$2)();
    const slippageAdjustedAmounts = v2Trade && allowedSlippage && (0,prices/* computeSlippageAdjustedAmounts */.b5)(v2Trade, allowedSlippage);
    // compare input balance to max input based on version
    const [balanceIn, amountIn] = [
        currencyBalances[actions/* Field.INPUT */.gN.INPUT],
        slippageAdjustedAmounts ? slippageAdjustedAmounts[actions/* Field.INPUT */.gN.INPUT] : null, 
    ];
    if (balanceIn && amountIn && balanceIn.lessThan(amountIn)) {
        inputError = t('Insufficient %symbol% balance', {
            symbol: amountIn.currency.symbol
        });
    }
    return {
        currencies,
        currencyBalances,
        parsedAmount,
        v2Trade: v2Trade ?? undefined,
        inputError
    };
}
function parseCurrencyFromURLParameter(urlParam) {
    if (typeof urlParam === 'string') {
        const valid = (0,utils/* isAddress */.UJ)(urlParam);
        if (valid) return valid;
        if (urlParam.toUpperCase() === 'BNB') return 'BNB';
        if (valid === false) return 'BNB';
    }
    return '';
}
function parseTokenAmountURLParameter(urlParam) {
    // eslint-disable-next-line no-restricted-globals
    return typeof urlParam === 'string' && !isNaN(parseFloat(urlParam)) ? urlParam : '';
}
function parseIndependentFieldURLParameter(urlParam) {
    return typeof urlParam === 'string' && urlParam.toLowerCase() === 'output' ? actions/* Field.OUTPUT */.gN.OUTPUT : actions/* Field.INPUT */.gN.INPUT;
}
const ENS_NAME_REGEX = /^[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&/=]*)?$/;
const ADDRESS_REGEX = /^0x[a-fA-F0-9]{40}$/;
function validatedRecipient(recipient) {
    if (typeof recipient !== 'string') return null;
    const address = (0,utils/* isAddress */.UJ)(recipient);
    if (address) return address;
    if (ENS_NAME_REGEX.test(recipient)) return recipient;
    if (ADDRESS_REGEX.test(recipient)) return recipient;
    return null;
}
function queryParametersToSwapState(parsedQs) {
    let inputCurrency = parseCurrencyFromURLParameter(parsedQs.inputCurrency) || DEFAULT_INPUT_CURRENCY;
    let outputCurrency = parseCurrencyFromURLParameter(parsedQs.outputCurrency) || DEFAULT_OUTPUT_CURRENCY;
    if (inputCurrency === outputCurrency) {
        if (typeof parsedQs.outputCurrency === 'string') {
            inputCurrency = '';
        } else {
            outputCurrency = '';
        }
    }
    const recipient = validatedRecipient(parsedQs.recipient);
    return {
        [actions/* Field.INPUT */.gN.INPUT]: {
            currencyId: inputCurrency
        },
        [actions/* Field.OUTPUT */.gN.OUTPUT]: {
            currencyId: outputCurrency
        },
        typedValue: parseTokenAmountURLParameter(parsedQs.exactAmount),
        independentField: parseIndependentFieldURLParameter(parsedQs.exactField),
        recipient,
        pairDataById: {},
        derivedPairDataById: {}
    };
}
// updates the swap state to use the defaults for a given network
function useDefaultsFromURLSearch() {
    const { chainId  } = (0,useActiveWeb3React/* default */.Z)();
    const dispatch = (0,external_react_redux_.useDispatch)();
    const parsedQs = useParsedQueryString();
    const { 0: result , 1: setResult  } = (0,external_react_.useState)();
    (0,external_react_.useEffect)(()=>{
        if (!chainId) return;
        const parsed = queryParametersToSwapState(parsedQs);
        dispatch((0,actions/* replaceSwapState */.mV)({
            typedValue: parsed.typedValue,
            field: parsed.independentField,
            inputCurrencyId: parsed[actions/* Field.INPUT */.gN.INPUT].currencyId,
            outputCurrencyId: parsed[actions/* Field.OUTPUT */.gN.OUTPUT].currencyId,
            recipient: null
        }));
        setResult({
            inputCurrencyId: parsed[actions/* Field.INPUT */.gN.INPUT].currencyId,
            outputCurrencyId: parsed[actions/* Field.OUTPUT */.gN.OUTPUT].currencyId
        });
    }, [
        dispatch,
        chainId,
        parsedQs
    ]);
    return result;
}
const useFetchPairPrices = ({ token0Address , token1Address , timeWindow , currentSwapPrice  })=>{
    const { 0: pairId , 1: setPairId  } = (0,external_react_.useState)(null);
    const { 0: isLoading , 1: setIsLoading  } = (0,external_react_.useState)(false);
    const pairData = (0,external_react_redux_.useSelector)(pairByDataIdSelector({
        pairId,
        timeWindow
    }));
    const derivedPairData = (0,external_react_redux_.useSelector)(derivedPairByDataIdSelector({
        pairId,
        timeWindow
    }));
    const dispatch = (0,external_react_redux_.useDispatch)();
    (0,external_react_.useEffect)(()=>{
        const fetchDerivedData = async ()=>{
            console.info('[Price Chart]: Not possible to retrieve price data from single pool, trying to fetch derived prices');
            try {
                // Try to get at least derived data for chart
                // This is used when there is no direct data for pool
                // i.e. when multihops are necessary
                const derivedData = await fetch_fetchDerivedPriceData(token0Address, token1Address, timeWindow);
                if (derivedData) {
                    const normalizedDerivedData = normalizeDerivedChartData(derivedData);
                    dispatch((0,actions/* updateDerivedPairData */._U)({
                        pairData: normalizedDerivedData,
                        pairId,
                        timeWindow
                    }));
                } else {
                    dispatch((0,actions/* updateDerivedPairData */._U)({
                        pairData: [],
                        pairId,
                        timeWindow
                    }));
                }
            } catch (error) {
                console.error('Failed to fetch derived prices for chart', error);
                dispatch((0,actions/* updateDerivedPairData */._U)({
                    pairData: [],
                    pairId,
                    timeWindow
                }));
            } finally{
                setIsLoading(false);
            }
        };
        const fetchAndUpdatePairPrice = async ()=>{
            setIsLoading(true);
            const { data  } = await fetch_fetchPairPriceData({
                pairId,
                timeWindow
            });
            if (data) {
                // Find out if Liquidity Pool has enough liquidity
                // low liquidity pool might mean that the price is incorrect
                // in that case try to get derived price
                const hasEnoughLiquidity = pairHasEnoughLiquidity(data, timeWindow);
                const newPairData = normalizeChartData(data, timeWindow) || [];
                if (newPairData.length > 0 && hasEnoughLiquidity) {
                    dispatch((0,actions/* updatePairData */.Wk)({
                        pairData: newPairData,
                        pairId,
                        timeWindow
                    }));
                    setIsLoading(false);
                } else {
                    console.info(`[Price Chart]: Liquidity too low for ${pairId}`);
                    dispatch((0,actions/* updatePairData */.Wk)({
                        pairData: [],
                        pairId,
                        timeWindow
                    }));
                    fetchDerivedData();
                }
            } else {
                dispatch((0,actions/* updatePairData */.Wk)({
                    pairData: [],
                    pairId,
                    timeWindow
                }));
                fetchDerivedData();
            }
        };
        if (!pairData && !derivedPairData && pairId && !isLoading) {
            fetchAndUpdatePairPrice();
        }
    }, [
        pairId,
        timeWindow,
        pairData,
        currentSwapPrice,
        token0Address,
        token1Address,
        derivedPairData,
        dispatch,
        isLoading, 
    ]);
    (0,external_react_.useEffect)(()=>{
        const updatePairId = ()=>{
            try {
                const pairAddress = utils_getLpAddress(token0Address, token1Address)?.toLowerCase();
                if (pairAddress !== pairId) {
                    setPairId(pairAddress);
                }
            } catch (error) {
                setPairId(null);
            }
        };
        updatePairId();
    }, [
        token0Address,
        token1Address,
        pairId
    ]);
    const normalizedPairData = (0,external_react_.useMemo)(()=>normalizePairDataByActiveToken({
            activeToken: token0Address,
            pairData
        })
    , [
        token0Address,
        pairData
    ]);
    const normalizedDerivedPairData = (0,external_react_.useMemo)(()=>normalizeDerivedPairDataByActiveToken({
            activeToken: token0Address,
            pairData: derivedPairData
        })
    , [
        token0Address,
        derivedPairData
    ]);
    const hasSwapPrice = currentSwapPrice && currentSwapPrice[token0Address] > 0;
    const normalizedPairDataWithCurrentSwapPrice = normalizedPairData?.length > 0 && hasSwapPrice ? [
        ...normalizedPairData,
        {
            time: new Date(),
            value: currentSwapPrice[token0Address]
        }
    ] : normalizedPairData;
    const normalizedDerivedPairDataWithCurrentSwapPrice = normalizedDerivedPairData?.length > 0 && hasSwapPrice ? [
        ...normalizedDerivedPairData,
        {
            time: new Date(),
            value: currentSwapPrice[token0Address]
        }
    ] : normalizedDerivedPairData;
    const hasNoDirectData = normalizedPairDataWithCurrentSwapPrice && normalizedPairDataWithCurrentSwapPrice?.length === 0;
    const hasNoDerivedData = normalizedDerivedPairDataWithCurrentSwapPrice && normalizedDerivedPairDataWithCurrentSwapPrice?.length === 0;
    // undefined is used for loading
    let pairPrices = hasNoDirectData && hasNoDerivedData ? [] : undefined;
    if (normalizedPairDataWithCurrentSwapPrice && normalizedPairDataWithCurrentSwapPrice?.length > 0) {
        pairPrices = normalizedPairDataWithCurrentSwapPrice;
    } else if (normalizedDerivedPairDataWithCurrentSwapPrice && normalizedDerivedPairDataWithCurrentSwapPrice?.length > 0) {
        pairPrices = normalizedDerivedPairDataWithCurrentSwapPrice;
    }
    return {
        pairPrices,
        pairId
    };
};


/***/ }),

/***/ 5892:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "h7": () => (/* binding */ useTransactionAdder),
/* harmony export */   "kf": () => (/* binding */ useAllTransactions),
/* harmony export */   "mH": () => (/* binding */ isTransactionRecent),
/* harmony export */   "wB": () => (/* binding */ useHasPendingApproval)
/* harmony export */ });
/* unused harmony export useIsTransactionPending */
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6022);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var hooks_useActiveWeb3React__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4011);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1564);




// helper that can take a ethers library transaction response and add it to the list of transactions
function useTransactionAdder() {
    const { chainId , account  } = (0,hooks_useActiveWeb3React__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z)();
    const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_1__.useDispatch)();
    return (0,react__WEBPACK_IMPORTED_MODULE_0__.useCallback)((response, { summary , approval , claim  } = {})=>{
        if (!account) return;
        if (!chainId) return;
        const { hash  } = response;
        if (!hash) {
            throw Error('No transaction hash found.');
        }
        dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_3__/* .addTransaction */ .dT)({
            hash,
            from: account,
            chainId,
            approval,
            summary,
            claim
        }));
    }, [
        dispatch,
        chainId,
        account
    ]);
}
// returns all the transactions for the current chain
function useAllTransactions() {
    const { chainId  } = (0,hooks_useActiveWeb3React__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z)();
    const state = (0,react_redux__WEBPACK_IMPORTED_MODULE_1__.useSelector)((s)=>s.transactions
    );
    return chainId ? state[chainId] ?? {} : {};
}
function useIsTransactionPending(transactionHash) {
    const transactions = useAllTransactions();
    if (!transactionHash || !transactions[transactionHash]) return false;
    return !transactions[transactionHash].receipt;
}
/**
 * Returns whether a transaction happened in the last day (86400 seconds * 1000 milliseconds / second)
 * @param tx to check for recency
 */ function isTransactionRecent(tx) {
    return new Date().getTime() - tx.addedTime < 86400000;
}
// returns whether a token has a pending approval transaction
function useHasPendingApproval(tokenAddress, spender) {
    const allTransactions = useAllTransactions();
    return (0,react__WEBPACK_IMPORTED_MODULE_0__.useMemo)(()=>typeof tokenAddress === 'string' && typeof spender === 'string' && Object.keys(allTransactions).some((hash)=>{
            const tx = allTransactions[hash];
            if (!tx) return false;
            if (tx.receipt) {
                return false;
            }
            const { approval  } = tx;
            if (!approval) return false;
            return approval.spender === spender && approval.tokenAddress === tokenAddress && isTransactionRecent(tx);
        })
    , [
        allTransactions,
        spender,
        tokenAddress
    ]);
}


/***/ }),

/***/ 1900:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "K5": () => (/* binding */ useCurrencyBalances),
/* harmony export */   "_h": () => (/* binding */ useCurrencyBalance),
/* harmony export */   "uD": () => (/* binding */ useAllTokenBalances)
/* harmony export */ });
/* unused harmony exports useBNBBalances, useTokenBalancesWithLoadingIndicator, useTokenBalances, useTokenBalance */
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(543);
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _web3_react_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8054);
/* harmony import */ var _web3_react_core__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_web3_react_core__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var config_abi_erc20__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5169);
/* harmony import */ var hooks_Tokens__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(6435);
/* harmony import */ var hooks_useContract__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(6063);
/* harmony import */ var utils__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(8328);
/* harmony import */ var _multicall_hooks__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1001);








/**
 * Returns a map of the given addresses to their eventually consistent BNB balances.
 */ function useBNBBalances(uncheckedAddresses) {
    const multicallContract = (0,hooks_useContract__WEBPACK_IMPORTED_MODULE_5__/* .useMulticallContract */ .gq)();
    const addresses = (0,react__WEBPACK_IMPORTED_MODULE_1__.useMemo)(()=>uncheckedAddresses ? uncheckedAddresses.map(utils__WEBPACK_IMPORTED_MODULE_6__/* .isAddress */ .UJ).filter((a)=>a !== false
        ).sort() : []
    , [
        uncheckedAddresses
    ]);
    const results = (0,_multicall_hooks__WEBPACK_IMPORTED_MODULE_7__/* .useSingleContractMultipleData */ .es)(multicallContract, 'getEthBalance', addresses.map((address)=>[
            address
        ]
    ));
    return (0,react__WEBPACK_IMPORTED_MODULE_1__.useMemo)(()=>addresses.reduce((memo, address, i)=>{
            const value = results?.[i]?.result?.[0];
            if (value) memo[address] = _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.CurrencyAmount.ether(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.BigInt(value.toString()));
            return memo;
        }, {})
    , [
        addresses,
        results
    ]);
}
/**
 * Returns a map of token addresses to their eventually consistent token balances for a single account.
 */ function useTokenBalancesWithLoadingIndicator(address, tokens) {
    const validatedTokens = (0,react__WEBPACK_IMPORTED_MODULE_1__.useMemo)(()=>tokens?.filter((t)=>(0,utils__WEBPACK_IMPORTED_MODULE_6__/* .isAddress */ .UJ)(t?.address) !== false
        ) ?? []
    , [
        tokens
    ]);
    const validatedTokenAddresses = (0,react__WEBPACK_IMPORTED_MODULE_1__.useMemo)(()=>validatedTokens.map((vt)=>vt.address
        )
    , [
        validatedTokens
    ]);
    const balances = (0,_multicall_hooks__WEBPACK_IMPORTED_MODULE_7__/* .useMultipleContractSingleData */ ._Y)(validatedTokenAddresses, config_abi_erc20__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .ZP, 'balanceOf', [
        address
    ]);
    const anyLoading = (0,react__WEBPACK_IMPORTED_MODULE_1__.useMemo)(()=>balances.some((callState)=>callState.loading
        )
    , [
        balances
    ]);
    return [
        (0,react__WEBPACK_IMPORTED_MODULE_1__.useMemo)(()=>address && validatedTokens.length > 0 ? validatedTokens.reduce((memo, token, i)=>{
                const value = balances?.[i]?.result?.[0];
                const amount = value ? _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.BigInt(value.toString()) : undefined;
                if (amount) {
                    memo[token.address] = new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.TokenAmount(token, amount);
                }
                return memo;
            }, {}) : {}
        , [
            address,
            validatedTokens,
            balances
        ]),
        anyLoading, 
    ];
}
function useTokenBalances(address, tokens) {
    return useTokenBalancesWithLoadingIndicator(address, tokens)[0];
}
// get the balance for a single token/account combo
function useTokenBalance(account, token) {
    const tokenBalances = useTokenBalances(account, [
        token
    ]);
    if (!token) return undefined;
    return tokenBalances[token.address];
}
function useCurrencyBalances(account, currencies) {
    const tokens = (0,react__WEBPACK_IMPORTED_MODULE_1__.useMemo)(()=>currencies?.filter((currency)=>currency instanceof _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token
        ) ?? []
    , [
        currencies
    ]);
    const tokenBalances = useTokenBalances(account, tokens);
    const containsBNB = (0,react__WEBPACK_IMPORTED_MODULE_1__.useMemo)(()=>currencies?.some((currency)=>currency === _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ETHER
        ) ?? false
    , [
        currencies
    ]);
    const ethBalance = useBNBBalances(containsBNB ? [
        account
    ] : []);
    return (0,react__WEBPACK_IMPORTED_MODULE_1__.useMemo)(()=>currencies?.map((currency)=>{
            if (!account || !currency) return undefined;
            if (currency instanceof _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token) return tokenBalances[currency.address];
            if (currency === _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ETHER) return ethBalance[account];
            return undefined;
        }) ?? []
    , [
        account,
        currencies,
        ethBalance,
        tokenBalances
    ]);
}
function useCurrencyBalance(account, currency) {
    return useCurrencyBalances(account, [
        currency
    ])[0];
}
// mimics useAllBalances
function useAllTokenBalances() {
    const { account  } = (0,_web3_react_core__WEBPACK_IMPORTED_MODULE_2__.useWeb3React)();
    const allTokens = (0,hooks_Tokens__WEBPACK_IMPORTED_MODULE_4__/* .useAllTokens */ .e_)();
    const allTokensArray = (0,react__WEBPACK_IMPORTED_MODULE_1__.useMemo)(()=>Object.values(allTokens ?? {})
    , [
        allTokens
    ]);
    const balances = useTokenBalances(account ?? undefined, allTokensArray);
    return balances ?? {};
}


/***/ }),

/***/ 8382:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "J": () => (/* binding */ clearUserStates)
/* harmony export */ });
/* harmony import */ var _sentry_react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5427);
/* harmony import */ var _sentry_react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_sentry_react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2829);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _state_profile__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5952);
/* harmony import */ var _web3React__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2338);
/* harmony import */ var _state_transactions_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1564);





const clearUserStates = (dispatch, chainId)=>{
    dispatch((0,_state_profile__WEBPACK_IMPORTED_MODULE_2__/* .profileClear */ .Gb)());
    _sentry_react__WEBPACK_IMPORTED_MODULE_0__.configureScope((scope)=>scope.setUser(null)
    );
    // This localStorage key is set by @web3-react/walletconnect-connector
    if (window.localStorage.getItem('walletconnect')) {
        _web3React__WEBPACK_IMPORTED_MODULE_3__/* .connectorsByName.walletconnect.close */ .BA.walletconnect.close();
        _web3React__WEBPACK_IMPORTED_MODULE_3__/* .connectorsByName.walletconnect.walletConnectProvider */ .BA.walletconnect.walletConnectProvider = null;
    }
    window.localStorage.removeItem(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_1__.connectorLocalStorageKey);
    if (chainId) {
        dispatch((0,_state_transactions_actions__WEBPACK_IMPORTED_MODULE_4__/* .clearAllTransactions */ .fY)({
            chainId
        }));
    }
};


/***/ }),

/***/ 3010:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ isZero)
/* harmony export */ });
/**
 * Returns true if the string value is zero in hex
 * @param hexNumberString
 */ function isZero(hexNumberString) {
    return /^0x0*$/.test(hexNumberString);
};


/***/ }),

/***/ 9058:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Y": () => (/* binding */ setupNetwork),
/* harmony export */   "$": () => (/* binding */ registerToken)
/* harmony export */ });
/* harmony import */ var config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3206);
/* harmony import */ var _getRpcUrl__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7308);
// Set of helper functions to facilitate wallet setup


/**
 * Prompt the user to add BSC as a network on Metamask, or switch to BSC if the wallet is on a different network
 * @returns {boolean} true if the setup succeeded, false otherwise
 */ const setupNetwork = async ()=>{
    const provider = window.ethereum;
    if (provider) {
        const chainId = parseInt("56", 10);
        try {
            await provider.request({
                method: 'wallet_addEthereumChain',
                params: [
                    {
                        chainId: `0x${chainId.toString(16)}`,
                        chainName: 'Binance Smart Chain Mainnet',
                        nativeCurrency: {
                            name: 'BNB',
                            symbol: 'bnb',
                            decimals: 18
                        },
                        rpcUrls: _getRpcUrl__WEBPACK_IMPORTED_MODULE_1__/* .nodes */ .t,
                        blockExplorerUrls: [
                            `${config__WEBPACK_IMPORTED_MODULE_0__/* .BASE_BSC_SCAN_URL */ .OS}/`
                        ]
                    }, 
                ]
            });
            return true;
        } catch (error) {
            console.error('Failed to setup the network in Metamask:', error);
            return false;
        }
    } else {
        console.error("Can't setup the BSC network on metamask because window.ethereum is undefined");
        return false;
    }
};
/**
 * Prompt the user to add a custom token to metamask
 * @param tokenAddress
 * @param tokenSymbol
 * @param tokenDecimals
 * @returns {boolean} true if the token has been added, false otherwise
 */ const registerToken = async (tokenAddress, tokenSymbol, tokenDecimals)=>{
    const tokenAdded = await window.ethereum.request({
        method: 'wallet_watchAsset',
        params: {
            type: 'ERC20',
            options: {
                address: tokenAddress,
                symbol: tokenSymbol,
                decimals: tokenDecimals,
                image: `${config__WEBPACK_IMPORTED_MODULE_0__/* .BASE_URL */ ._n}/images/tokens/${tokenAddress}.png`
            }
        }
    });
    return tokenAdded;
};


/***/ }),

/***/ 2338:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BA": () => (/* binding */ connectorsByName),
/* harmony export */   "av": () => (/* binding */ getLibrary)
/* harmony export */ });
/* unused harmony export signMessage */
/* harmony import */ var _web3_react_injected_connector__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6590);
/* harmony import */ var _web3_react_injected_connector__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_web3_react_injected_connector__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _web3_react_walletconnect_connector__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9795);
/* harmony import */ var _web3_react_walletconnect_connector__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_web3_react_walletconnect_connector__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _binance_chain_bsc_connector__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8454);
/* harmony import */ var _binance_chain_bsc_connector__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_binance_chain_bsc_connector__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2829);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ethersproject_bytes__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(9935);
/* harmony import */ var _ethersproject_bytes__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_ethersproject_bytes__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _ethersproject_strings__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(9213);
/* harmony import */ var _ethersproject_strings__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_ethersproject_strings__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _ethersproject_providers__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(399);
/* harmony import */ var _ethersproject_providers__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_ethersproject_providers__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _getRpcUrl__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(7308);








const POLLING_INTERVAL = 12000;
const rpcUrl = (0,_getRpcUrl__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z)();
const chainId = parseInt("56", 10);
const injected = new _web3_react_injected_connector__WEBPACK_IMPORTED_MODULE_0__.InjectedConnector({
    supportedChainIds: [
        chainId
    ]
});
const walletconnect = new _web3_react_walletconnect_connector__WEBPACK_IMPORTED_MODULE_1__.WalletConnectConnector({
    rpc: {
        [chainId]: rpcUrl
    },
    qrcode: true,
    pollingInterval: POLLING_INTERVAL
});
const bscConnector = new _binance_chain_bsc_connector__WEBPACK_IMPORTED_MODULE_2__.BscConnector({
    supportedChainIds: [
        chainId
    ]
});
const connectorsByName = {
    [_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_3__.ConnectorNames.Injected]: injected,
    [_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_3__.ConnectorNames.WalletConnect]: walletconnect,
    [_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_3__.ConnectorNames.BSC]: bscConnector
};
const getLibrary = (provider)=>{
    const library = new _ethersproject_providers__WEBPACK_IMPORTED_MODULE_6__.Web3Provider(provider);
    library.pollingInterval = POLLING_INTERVAL;
    return library;
};
/**
 * BSC Wallet requires a different sign method
 * @see https://docs.binance.org/smart-chain/wallet/wallet_api.html#binancechainbnbsignaddress-string-message-string-promisepublickey-string-signature-string
 */ const signMessage = async (connector, provider, account, message)=>{
    if (window.BinanceChain && connector instanceof BscConnector) {
        const { signature  } = await window.BinanceChain.bnbSign(account, message);
        return signature;
    }
    /**
   * Wallet Connect does not sign the message correctly unless you use their method
   * @see https://github.com/WalletConnect/walletconnect-monorepo/issues/462
   */ if (provider.provider?.wc) {
        const wcMessage = hexlify(toUtf8Bytes(message));
        const signature = await provider.provider?.wc.signPersonalMessage([
            wcMessage,
            account
        ]);
        return signature;
    }
    return provider.getSigner(account).signMessage(message);
};


/***/ }),

/***/ 9298:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "I": () => (/* binding */ BNB_ADDRESS)
/* harmony export */ });
// BNB Address
const BNB_ADDRESS = '0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c';



/***/ }),

/***/ 9197:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "E": () => (/* binding */ getTimeWindowChange),
/* harmony export */   "D": () => (/* binding */ getTokenAddress)
/* harmony export */ });
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9298);

const MIN_VALUE_DISPLAYED = 0.001;
const getTimeWindowChange = (lineChartData)=>{
    if (lineChartData.length > 0) {
        const firstValue = lineChartData.find(({ value  })=>!!value && value > 0
        )?.value ?? 0;
        const lastValue = lineChartData[lineChartData.length - 1].value;
        const changeValue = lastValue - firstValue;
        return {
            changeValue: changeValue > 0 ? Math.max(changeValue, MIN_VALUE_DISPLAYED) : Math.min(changeValue, MIN_VALUE_DISPLAYED * -1),
            changePercentage: (changeValue / firstValue * 100).toFixed(2)
        };
    }
    return {
        changeValue: 0,
        changePercentage: 0
    };
};
const getTokenAddress = (tokenAddress)=>{
    if (!tokenAddress) {
        return '';
    }
    const lowerCaseAddress = tokenAddress.toLowerCase();
    if (lowerCaseAddress === 'bnb') {
        return _constants__WEBPACK_IMPORTED_MODULE_0__/* .BNB_ADDRESS */ .I;
    }
    return lowerCaseAddress;
};


/***/ })

};
;
//# sourceMappingURL=633.js.map