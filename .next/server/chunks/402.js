"use strict";
exports.id = 402;
exports.ids = [402];
exports.modules = {

/***/ 2188:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "y": () => (/* binding */ parseENSAddress)
/* harmony export */ });
const ENS_NAME_REGEX = /^(([a-zA-Z0-9]+(-[a-zA-Z0-9]+)*\.)+)eth(\/.*)?$/;
function parseENSAddress(ensAddress) {
    const match = ensAddress.match(ENS_NAME_REGEX);
    if (!match) return undefined;
    return {
        ensName: `${match[1].toLowerCase()}eth`,
        ensPath: match[4]
    };
}
/* unused harmony default export */ var __WEBPACK_DEFAULT_EXPORT__ = ((/* unused pure expression or super */ null && (parseENSAddress)));


/***/ }),

/***/ 864:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ contenthashToUri)
/* harmony export */ });
/* harmony import */ var cids__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8729);
/* harmony import */ var cids__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(cids__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var multicodec__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6677);
/* harmony import */ var multicodec__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(multicodec__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var multihashes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3735);
/* harmony import */ var multihashes__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(multihashes__WEBPACK_IMPORTED_MODULE_2__);



function hexToUint8Array(hex) {
    // eslint-disable-next-line no-param-reassign
    hex = hex.startsWith('0x') ? hex.substr(2) : hex;
    if (hex.length % 2 !== 0) throw new Error('hex must have length that is multiple of 2');
    const arr = new Uint8Array(hex.length / 2);
    for(let i = 0; i < arr.length; i++){
        arr[i] = parseInt(hex.substr(i * 2, 2), 16);
    }
    return arr;
}
const UTF_8_DECODER = new TextDecoder();
/**
 * Returns the URI representation of the content hash for supported codecs
 * @param contenthash to decode
 */ function contenthashToUri(contenthash) {
    const buff = hexToUint8Array(contenthash);
    const codec = (0,multicodec__WEBPACK_IMPORTED_MODULE_1__.getCodec)(buff) // the typing is wrong for @types/multicodec
    ;
    switch(codec){
        case 'ipfs-ns':
            {
                const data = (0,multicodec__WEBPACK_IMPORTED_MODULE_1__.rmPrefix)(buff);
                const cid = new (cids__WEBPACK_IMPORTED_MODULE_0___default())(data);
                return `ipfs://${(0,multihashes__WEBPACK_IMPORTED_MODULE_2__.toB58String)(cid.multihash)}`;
            }
        case 'ipns-ns':
            {
                const data = (0,multicodec__WEBPACK_IMPORTED_MODULE_1__.rmPrefix)(buff);
                const cid = new (cids__WEBPACK_IMPORTED_MODULE_0___default())(data);
                const multihash = (0,multihashes__WEBPACK_IMPORTED_MODULE_2__.decode)(cid.multihash);
                if (multihash.name === 'identity') {
                    return `ipns://${UTF_8_DECODER.decode(multihash.digest).trim()}`;
                }
                return `ipns://${(0,multihashes__WEBPACK_IMPORTED_MODULE_2__.toB58String)(cid.multihash)}`;
            }
        default:
            throw new Error(`Unrecognized codec: ${codec}`);
    }
};


/***/ }),

/***/ 570:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ uriToHttp)
/* harmony export */ });
/* eslint-disable no-case-declarations */ /**
 * Given a URI that may be ipfs, ipns, http, or https protocol, return the fetch-able http(s) URLs for the same content
 * @param uri to convert to fetch-able http url
 */ function uriToHttp(uri) {
    const protocol = uri.split(':')[0].toLowerCase();
    switch(protocol){
        case 'https':
            return [
                uri
            ];
        case 'http':
            return [
                `https${uri.substr(4)}`,
                uri
            ];
        case 'ipfs':
            const hash = uri.match(/^ipfs:(\/\/)?(.*)$/i)?.[2];
            return [
                `https://cloudflare-ipfs.com/ipfs/${hash}/`,
                `https://ipfs.io/ipfs/${hash}/`
            ];
        case 'ipns':
            const name = uri.match(/^ipns:(\/\/)?(.*)$/i)?.[2];
            return [
                `https://cloudflare-ipfs.com/ipns/${name}/`,
                `https://ipfs.io/ipns/${name}/`
            ];
        default:
            return [];
    }
};


/***/ })

};
;
//# sourceMappingURL=402.js.map