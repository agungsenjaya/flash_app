"use strict";
exports.id = 555;
exports.ids = [555];
exports.modules = {

/***/ 3555:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ getTokenList)
/* harmony export */ });
/* harmony import */ var _uniswap_token_lists_src_tokenlist_schema_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(475);
/* harmony import */ var ajv__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5888);
/* harmony import */ var ajv__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(ajv__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _contenthashToUri__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(864);
/* harmony import */ var _ENS_parseENSAddress__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2188);
/* harmony import */ var _uriToHttp__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(570);





const tokenListValidator = new (ajv__WEBPACK_IMPORTED_MODULE_1___default())({
    allErrors: true
}).compile(_uniswap_token_lists_src_tokenlist_schema_json__WEBPACK_IMPORTED_MODULE_0__);
/**
 * Contains the logic for resolving a list URL to a validated token list
 * @param listUrl list url
 * @param resolveENSContentHash resolves an ens name to a contenthash
 */ async function getTokenList(listUrl, resolveENSContentHash) {
    const parsedENS = (0,_ENS_parseENSAddress__WEBPACK_IMPORTED_MODULE_3__/* .parseENSAddress */ .y)(listUrl);
    let urls;
    if (parsedENS) {
        let contentHashUri;
        try {
            contentHashUri = await resolveENSContentHash(parsedENS.ensName);
        } catch (error) {
            console.error(`Failed to resolve ENS name: ${parsedENS.ensName}`, error);
            throw new Error(`Failed to resolve ENS name: ${parsedENS.ensName}`);
        }
        let translatedUri;
        try {
            translatedUri = (0,_contenthashToUri__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z)(contentHashUri);
        } catch (error1) {
            console.error('Failed to translate contenthash to URI', contentHashUri);
            throw new Error(`Failed to translate contenthash to URI: ${contentHashUri}`);
        }
        urls = (0,_uriToHttp__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z)(`${translatedUri}${parsedENS.ensPath ?? ''}`);
    } else {
        urls = (0,_uriToHttp__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z)(listUrl);
    }
    for(let i = 0; i < urls.length; i++){
        const url = urls[i];
        const isLast = i === urls.length - 1;
        let response;
        try {
            response = await fetch(url);
        } catch (error2) {
            console.error('Failed to fetch list', listUrl, error2);
            if (isLast) throw new Error(`Failed to download list ${listUrl}`);
            continue;
        }
        if (!response.ok) {
            if (isLast) throw new Error(`Failed to download list ${listUrl}`);
            continue;
        }
        const json = await response.json();
        if (!tokenListValidator(json)) {
            const validationErrors = tokenListValidator.errors?.reduce((memo, error)=>{
                const add = `${error.dataPath} ${error.message ?? ''}`;
                return memo.length > 0 ? `${memo}; ${add}` : `${add}`;
            }, '') ?? 'unknown error';
            throw new Error(`Token list failed validation: ${validationErrors}`);
        }
        return json;
    }
    throw new Error('Unrecognized list URL protocol.');
};


/***/ })

};
;
//# sourceMappingURL=555.js.map