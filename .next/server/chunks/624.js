"use strict";
exports.id = 624;
exports.ids = [624];
exports.modules = {

/***/ 5906:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "v5": () => (/* binding */ GRAPH_API_PREDICTION),
/* harmony export */   "Xr": () => (/* binding */ GRAPH_API_LOTTERY),
/* harmony export */   "C1": () => (/* binding */ API_NFT),
/* harmony export */   "TY": () => (/* binding */ SNAPSHOT_API),
/* harmony export */   "JY": () => (/* binding */ INFO_CLIENT),
/* harmony export */   "I0": () => (/* binding */ BLOCKS_CLIENT),
/* harmony export */   "Bd": () => (/* binding */ GRAPH_API_NFTMARKET),
/* harmony export */   "AM": () => (/* binding */ GRAPH_HEALTH)
/* harmony export */ });
/* unused harmony exports GRAPH_API_PROFILE, SNAPSHOT_VOTING_API, SNAPSHOT_BASE_URL, API_PROFILE, SNAPSHOT_HUB_API, GRAPH_API_PREDICTION_V1 */
const GRAPH_API_PROFILE = (/* unused pure expression or super */ null && ("https://api.thegraph.com/subgraphs/name/pancakeswap/profile"));
const GRAPH_API_PREDICTION = "https://api.thegraph.com/subgraphs/name/pancakeswap/prediction-v2";
const GRAPH_API_LOTTERY = "https://api.thegraph.com/subgraphs/name/pancakeswap/lottery";
const SNAPSHOT_VOTING_API = (/* unused pure expression or super */ null && ("https://voting-api.pancakeswap.info/api"));
const SNAPSHOT_BASE_URL = "https://hub.snapshot.org";
const API_PROFILE = (/* unused pure expression or super */ null && ("https://profile.pancakeswap.com"));
const API_NFT = "https://nft.pancakeswap.com/api/v1";
const SNAPSHOT_API = `${SNAPSHOT_BASE_URL}/graphql`;
const SNAPSHOT_HUB_API = `${SNAPSHOT_BASE_URL}/api/message`;
/**
 * V1 will be deprecated but is still used to claim old rounds
 */ const GRAPH_API_PREDICTION_V1 = 'https://api.thegraph.com/subgraphs/name/pancakeswap/prediction';
const INFO_CLIENT = 'https://bsc.streamingfast.io/subgraphs/name/pancakeswap/exchange-v2';
const BLOCKS_CLIENT = 'https://api.thegraph.com/subgraphs/name/pancakeswap/blocks';
const GRAPH_API_NFTMARKET = "https://api.thegraph.com/subgraphs/name/pancakeswap/nft-market";
const GRAPH_HEALTH = 'https://api.thegraph.com/index-node/graphql';


/***/ }),

/***/ 1115:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _tokens__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9748);

const serializedTokens = (0,_tokens__WEBPACK_IMPORTED_MODULE_0__/* .serializeTokens */ .uO)();
const farms = [
    /**
   * These 3 farms (PID 0, 251, 252) should always be at the top of the file.
   */ {
        pid: 0,
        lpSymbol: 'CAKE',
        lpAddresses: {
            97: '0x9C21123D94b93361a29B2C2EFB3d5CD8B17e0A9e',
            56: '0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82'
        },
        token: serializedTokens.syrup,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 251,
        lpSymbol: 'CAKE-BNB LP',
        lpAddresses: {
            97: '0x3ed8936cAFDF85cfDBa29Fbe5940A5b0524824F4',
            56: '0x0eD7e52944161450477ee417DE9Cd3a859b14fD0'
        },
        token: serializedTokens.cake,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 252,
        lpSymbol: 'BUSD-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x58F876857a02D6762E0101bb5C46A8c1ED44Dc16'
        },
        token: serializedTokens.busd,
        quoteToken: serializedTokens.wbnb
    },
    /**
   * V3 by order of release (some may be out of PID order due to multiplier boost)
   */ {
        pid: 505,
        lpSymbol: 'FUSE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x6483F166b9E4310A165a55FEa04F867499aded06'
        },
        token: serializedTokens.fuse,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 386,
        lpSymbol: 'HOTCROSS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xf23bad605e94de0e3b60c9718a43a94a5af43915'
        },
        token: serializedTokens.hotcross,
        quoteToken: serializedTokens.wbnb,
        isCommunity: true
    },
    {
        pid: 504,
        lpSymbol: 'PRL-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xb5FEAE037c2330a8F298F39bcE96dd6E69f4Fa0E'
        },
        token: serializedTokens.prl,
        quoteToken: serializedTokens.busd,
        isCommunity: true
    },
    {
        pid: 498,
        lpSymbol: '8PAY-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x92c3E2cddDb0CE886bCA864151BD4d611A86E563'
        },
        token: serializedTokens['8pay'],
        quoteToken: serializedTokens.busd,
        isCommunity: true
    },
    {
        pid: 503,
        lpSymbol: 'FROYO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x1Ce76390Dd210B9C9ae28373FDf79714206ECb73'
        },
        token: serializedTokens.froyo,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 497,
        lpSymbol: 'AOG-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x88c9bf5E334e2591C6A866D5E20683E31226Be3d'
        },
        token: serializedTokens.aog,
        quoteToken: serializedTokens.busd,
        isCommunity: false
    },
    {
        pid: 502,
        lpSymbol: 'APX-BUSD',
        lpAddresses: {
            97: '',
            56: '0xa0ee789a8f581cb92dd9742ed0b5d54a0916976c'
        },
        token: serializedTokens.apx,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 501,
        lpSymbol: 'BCOIN-BNB',
        lpAddresses: {
            97: '',
            56: '0x2Eebe0C34da9ba65521E98CBaA7D97496d05f489'
        },
        token: serializedTokens.bcoin,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 500,
        lpSymbol: 'INSUR-BNB',
        lpAddresses: {
            97: '',
            56: '0xD01bf29EdCA0285A004a25e325A449ba56e5926E'
        },
        token: serializedTokens.insur,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 499,
        lpSymbol: 'BATH-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xEE90C67C9dD5dE862F4eabFDd53007a2D95Df5c6'
        },
        token: serializedTokens.bath,
        quoteToken: serializedTokens.busd,
        isCommunity: true
    },
    {
        pid: 496,
        lpSymbol: 'GM-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x1C640a98a0c62120B0AD23C15FfF8dC1a2Fb9C4D'
        },
        token: serializedTokens.gm,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 495,
        lpSymbol: 'WOOP-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x2AE94A6C768D59f5DDc25bd7f12C7cBE1D51dc04'
        },
        token: serializedTokens.woop,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 492,
        lpSymbol: 'SDAO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x43b95976cF0929478bC13332C9cd2D63Bf060976'
        },
        token: serializedTokens.sdao,
        quoteToken: serializedTokens.wbnb,
        isCommunity: true
    },
    {
        pid: 493,
        lpSymbol: 'ANTEX-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x4DcB7b3b0E8914DC0e6D366521604cD23E7991E1'
        },
        token: serializedTokens.antex,
        quoteToken: serializedTokens.busd,
        isCommunity: true
    },
    {
        pid: 494,
        lpSymbol: 'BBT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x3D5A3E3824da092851026fCda3D8a0B7438c4573'
        },
        token: serializedTokens.bbt,
        quoteToken: serializedTokens.wbnb,
        isCommunity: true
    },
    {
        pid: 491,
        lpSymbol: 'HIGH-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xe98ac95A1dB2fCaaa9c7D4ba7ecfCE4877ca2bEa'
        },
        token: serializedTokens.high,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 490,
        lpSymbol: 'CCAR-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x845d301C864d48027DB73ec4394e6DDBE52Cbc39'
        },
        token: serializedTokens.ccar,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 489,
        lpSymbol: 'DPT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x141e9558f66Cc21c93628400cCa7d830c15c2c24'
        },
        token: serializedTokens.dpt,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 488,
        lpSymbol: 'THG-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x486697ae24469cB1122F537924Aa46E705B142Aa'
        },
        token: serializedTokens.thg,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 485,
        lpSymbol: 'TT-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x6DA32849Fc5E1c23894d9E08166912F15bDb2E95'
        },
        token: serializedTokens.tt,
        quoteToken: serializedTokens.busd,
        isCommunity: true
    },
    {
        pid: 486,
        lpSymbol: 'GMEE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x6a24a877bb7D07fba59397DecBBAED5F92890AeA'
        },
        token: serializedTokens.gmee,
        quoteToken: serializedTokens.wbnb,
        isCommunity: true
    },
    {
        pid: 487,
        lpSymbol: 'HTD-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x0c3b12fCA25bfa840E0553DA97C532e9Abd3913d'
        },
        token: serializedTokens.htd,
        quoteToken: serializedTokens.busd,
        isCommunity: true
    },
    {
        pid: 484,
        lpSymbol: 'IDIA-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x71E6de81381eFE0Aa98f56b3B43eB3727D640715'
        },
        token: serializedTokens.idia,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 483,
        lpSymbol: 'XCV-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xD39F05AB936Aa201235005c47B83268f2d9833f8'
        },
        token: serializedTokens.xcv,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 482,
        lpSymbol: 'NABOX-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x29b4abb0f8734EA672a0e82FA47998F710B6A07a'
        },
        token: serializedTokens.nabox,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 481,
        lpSymbol: 'SANTOS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x06043B346450BbCfdE066ebc39fdc264FdFFeD74'
        },
        token: serializedTokens.santos,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 480,
        lpSymbol: 'QUIDD-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xD6d206F59cC5a3BfA4Cc10bc8Ba140ac37Ad1C89'
        },
        token: serializedTokens.quidd,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 479,
        lpSymbol: 'ZOO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x85e5889Fc3Ed01B4e8B56bbc717D7643294d2c31'
        },
        token: serializedTokens.zoo,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 450,
        lpSymbol: 'SFUND-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x74fA517715C4ec65EF01d55ad5335f90dce7CC87'
        },
        token: serializedTokens.sfund,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 447,
        lpSymbol: 'GNT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x3747e3e107223539FD09bb730b055A1f11F78Adf'
        },
        token: serializedTokens.gnt,
        quoteToken: serializedTokens.wbnb,
        isCommunity: true
    },
    {
        pid: 477,
        lpSymbol: 'SHEESHA-BNB',
        lpAddresses: {
            97: '',
            56: '0xB31Ecb43645EB273210838e710f2692CC6b30a11'
        },
        token: serializedTokens.sheesha,
        quoteToken: serializedTokens.wbnb,
        isCommunity: true
    },
    {
        pid: 478,
        lpSymbol: 'BCOIN-BUSD',
        lpAddresses: {
            97: '',
            56: '0xd76026a78a2A9aF2f9F57fe6337eED26Bfc26AED'
        },
        token: serializedTokens.bcoin,
        quoteToken: serializedTokens.busd,
        isCommunity: true
    },
    {
        pid: 476,
        lpSymbol: 'QI-BNB',
        lpAddresses: {
            97: '',
            56: '0xf924E642f05ACC57fc3b14990c2B1a137683b201'
        },
        token: serializedTokens.qi,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 475,
        lpSymbol: 'KART-BNB',
        lpAddresses: {
            97: '',
            56: '0x0927C49A18eAc4512112e7a226275e2c36f2C3Db'
        },
        token: serializedTokens.kart,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 474,
        lpSymbol: 'PORTO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x0A292e96ABb35297786a38fDD67Dc4f82689E670'
        },
        token: serializedTokens.porto,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 425,
        lpSymbol: 'DVI-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x89ebf9cd99864f6e51bd7a578965922029cab977'
        },
        token: serializedTokens.dvi,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 351,
        lpSymbol: 'JGN-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x7275278C94b5e20708380561C4Af98F38dDC6374'
        },
        token: serializedTokens.jgn,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 446,
        lpSymbol: 'BMON-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x00e53C169dA54a7E11172aEEDf8Eb87F060F479e'
        },
        token: serializedTokens.bmon,
        quoteToken: serializedTokens.busd,
        isCommunity: true
    },
    {
        pid: 438,
        lpSymbol: 'WSG-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x37Ff7D4459ad96E0B01275E5efffe091f33c2CAD'
        },
        token: serializedTokens.wsg,
        quoteToken: serializedTokens.wbnb,
        isCommunity: true
    },
    {
        pid: 469,
        lpSymbol: 'ZOO-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xd5D00B0aD40FF6d8C1a6F7e72F185B6fB3c3fd1F'
        },
        token: serializedTokens.zoo,
        quoteToken: serializedTokens.busd,
        isCommunity: true
    },
    {
        pid: 473,
        lpSymbol: 'ETERNAL-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xbd26e08411483C4BEBba80939FA5a775beE22338'
        },
        token: serializedTokens.eternal,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 472,
        lpSymbol: 'XWG-USDC LP',
        lpAddresses: {
            97: '',
            56: '0x936928146a21AfCcd30DfA84824A780572B1630B'
        },
        token: serializedTokens.xwg,
        quoteToken: serializedTokens.usdc
    },
    {
        pid: 471,
        lpSymbol: 'DAR-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x062f88E2B4896e823ac78Ac314468c29eEC4186d'
        },
        token: serializedTokens.dar,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 470,
        lpSymbol: 'FINA-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x6dB23b5360c9D2859fDcbf41c56494e7b8573649'
        },
        token: serializedTokens.fina,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 467,
        lpSymbol: 'MONI-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xbcfd0d4a37fEb4dceAAeFa9da28CD833E5f04e9f'
        },
        token: serializedTokens.moni,
        quoteToken: serializedTokens.wbnb,
        isCommunity: true
    },
    {
        pid: 468,
        lpSymbol: 'XMS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xBC7e925F9Dec60FE4F50C0457609f3cA5c3f5906'
        },
        token: serializedTokens.xms,
        quoteToken: serializedTokens.wbnb,
        isCommunity: true
    },
    {
        pid: 437,
        lpSymbol: 'BMON-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x3C2b7B578Dd2175A1c3524Aa0D515106282Bf108'
        },
        token: serializedTokens.bmon,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 466,
        lpSymbol: 'DKT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x365c3F921b2915a480308D0b1C04aEF7B99c2876'
        },
        token: serializedTokens.dkt,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 448,
        lpSymbol: 'RUSD-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x59FaC9e98479fc9979aE2a0C7422Af50bCBB9B26'
        },
        token: serializedTokens.rusd,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 464,
        lpSymbol: 'LAZIO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x11c0b2bb4fbb430825d07507a9e24e4c32f7704d'
        },
        token: serializedTokens.lazio,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 432,
        lpSymbol: 'SPS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xfdfde3af740a22648b9dd66d05698e5095940850'
        },
        token: serializedTokens.sps,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 459,
        lpSymbol: 'CART-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x47F0987f276b06E7Ce8D3F09E4E7fEc41a5dB808'
        },
        token: serializedTokens.cart,
        quoteToken: serializedTokens.wbnb,
        isCommunity: true
    },
    {
        pid: 465,
        lpSymbol: 'ARV-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xA63E32FeEFC6590bBf869070Fd2e706Eb7881bd2'
        },
        token: serializedTokens.arv,
        quoteToken: serializedTokens.wbnb,
        isCommunity: true
    },
    {
        pid: 460,
        lpSymbol: 'LIGHT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xD469F2E7d1329836733aDBAc6B53E09b775a6e03'
        },
        token: serializedTokens.light,
        quoteToken: serializedTokens.wbnb,
        isCommunity: true
    },
    {
        pid: 463,
        lpSymbol: 'MCB-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xE4077A90f8600d9599440cC6d9057785f43a6Ac9'
        },
        token: serializedTokens.mcb,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 462,
        lpSymbol: 'RPG-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x55cdb14855220b239Cf857A03849D96736b9103f'
        },
        token: serializedTokens.rpg,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 461,
        lpSymbol: 'BETA-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x88a02d94f437799f06f8c256ff07aa397e6d0016'
        },
        token: serializedTokens.beta,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 458,
        lpSymbol: 'PROS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x109cBFfE72C02F26536Ff8b92278Dfd3610dE656'
        },
        token: serializedTokens.pros,
        quoteToken: serializedTokens.wbnb,
        isCommunity: true
    },
    {
        pid: 457,
        lpSymbol: 'NFT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x0ecc84c9629317a494f12bc56aa2522475bf32e8'
        },
        token: serializedTokens.nft,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 429,
        lpSymbol: 'CHESS-USDC LP',
        lpAddresses: {
            97: '',
            56: '0x1472976e0b97f5b2fc93f1fff14e2b5c4447b64f'
        },
        token: serializedTokens.chess,
        quoteToken: serializedTokens.usdc
    },
    {
        pid: 439,
        lpSymbol: 'MCRN-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xe8D5d81dac092Ae61d097f84EFE230759BF2e522'
        },
        token: serializedTokens.mcrn,
        quoteToken: serializedTokens.wbnb,
        isCommunity: true
    },
    {
        pid: 456,
        lpSymbol: 'TLOS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x3eDb06e2d182d133864fe7C0f9B4C204bBf61D4E'
        },
        token: serializedTokens.tlos,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 455,
        lpSymbol: 'HERO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x5d937c3966002cbD9d32c890a59439b4b300a14d'
        },
        token: serializedTokens.stephero,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 449,
        lpSymbol: 'BP-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x2bF2dEB40639201C9A94c9e33b4852D9AEa5fd2D'
        },
        token: serializedTokens.bp,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 454,
        lpSymbol: 'BSCDEFI-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x5B0A3b98C2f01741A11E57A9d0595B254E62F9F2'
        },
        token: serializedTokens.bscdefi,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 453,
        lpSymbol: 'QBT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x67EFeF66A55c4562144B9AcfCFbc62F9E4269b3e'
        },
        token: serializedTokens.qbt,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 452,
        lpSymbol: 'NAOS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xcaa662ad41a662b81be2aea5d59ec0697628665f'
        },
        token: serializedTokens.naos,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 451,
        lpSymbol: 'PHA-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x4ddd56e2f34338839BB5953515833950eA680aFb'
        },
        token: serializedTokens.pha,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 314,
        lpSymbol: 'BEL-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x69DEE989c30b5fFe40867f5FC14F00E4bCE7B681'
        },
        token: serializedTokens.bel,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 317,
        lpSymbol: 'RAMP-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xE834bf723f5bDff34a5D1129F3c31Ea4787Bc76a'
        },
        token: serializedTokens.ramp,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 445,
        lpSymbol: 'POTS-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xF90BAA331Cfd40F094476E752Bf272892170d399'
        },
        token: serializedTokens.pots,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 397,
        lpSymbol: 'TUSD-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x2e28b9b74d6d99d4697e913b82b41ef1cac51c6c'
        },
        token: serializedTokens.tusd,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 443,
        lpSymbol: 'BTT-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xdcfbb12ded3fea12d2a078bc6324131cd14bf835'
        },
        token: serializedTokens.btt,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 442,
        lpSymbol: 'TRX-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xb5d108578be3750209d1b3a8f45ffee8c5a75146'
        },
        token: serializedTokens.trx,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 441,
        lpSymbol: 'WIN-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x6a445ceb72c8b1751755386c3990055ff92e14a0'
        },
        token: serializedTokens.win,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 436,
        lpSymbol: 'BABYCAKE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xb5e33fE13a821e55ED33C884589a804B1b4F6fD8'
        },
        token: serializedTokens.babycake,
        quoteToken: serializedTokens.wbnb,
        isCommunity: true
    },
    {
        pid: 440,
        lpSymbol: 'HERO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xe267018C943E77992e7e515724B07b9CE7938124'
        },
        token: serializedTokens.hero,
        quoteToken: serializedTokens.wbnb,
        isCommunity: true
    },
    {
        pid: 435,
        lpSymbol: 'REVV-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x1cc18962b919ef90085a8b21f8ddc95824fbad9e'
        },
        token: serializedTokens.revv,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 367,
        lpSymbol: 'BTT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x946696344e7d4346b223e1cf77035a76690d6a73'
        },
        token: serializedTokens.btt,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 434,
        lpSymbol: 'SKILL-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xc19dfd34d3ba5816df9cbdaa02d32a9f8dc6f6fc'
        },
        token: serializedTokens.skill,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 369,
        lpSymbol: 'WIN-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x894bd57afd8efc93d9171cb585d11d0977557425'
        },
        token: serializedTokens.win,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 433,
        lpSymbol: 'IF-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x7b4682D2B3f8670b125aF6AEA8d7eD2Daa43Bdc1'
        },
        token: serializedTokens.if,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 431,
        lpSymbol: 'C98-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x92247860A03F48d5c6425c7CA35CDcFCB1013AA1'
        },
        token: serializedTokens.c98,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 430,
        lpSymbol: 'AXS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xC2d00De94795e60FB76Bc37d899170996cBdA436'
        },
        token: serializedTokens.axs,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 379,
        lpSymbol: 'PMON-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xcdb0016d97fd0e7ec2c3b78aa4786cbd8e19c14c'
        },
        token: serializedTokens.pmon,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 368,
        lpSymbol: 'TRX-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x3cd338c3bb249b6b3c55799f85a589febbbff9dd'
        },
        token: serializedTokens.trx,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 428,
        lpSymbol: 'TITAN-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x9392a1f471d9aa14c0b8eb28bd7a3f4a814727be'
        },
        token: serializedTokens.titan,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 427,
        lpSymbol: 'ONE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x9d2296e2fe3cdbf2eb3e3e2ca8811bafa42eedff'
        },
        token: serializedTokens.harmony,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 426,
        lpSymbol: 'MASK-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x46c6bA71af7648cD7f67D0AD4d16f75bE251ed12'
        },
        token: serializedTokens.mask,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 339,
        lpSymbol: 'GUM-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x28Ea5894D4DBbE90bB58eE3BAB2869387d711c87'
        },
        token: serializedTokens.gum,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 424,
        lpSymbol: 'ADX-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x0648ff5de80adf54aac07ece2490f50a418dde23'
        },
        token: serializedTokens.adx,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 423,
        lpSymbol: 'USDC-USDT LP',
        lpAddresses: {
            97: '',
            56: '0xec6557348085aa57c72514d67070dc863c0a5a8c'
        },
        token: serializedTokens.usdc,
        quoteToken: serializedTokens.usdt
    },
    {
        pid: 422,
        lpSymbol: 'CAKE-USDT LP',
        lpAddresses: {
            97: '',
            56: '0xA39Af17CE4a8eb807E076805Da1e2B8EA7D0755b'
        },
        token: serializedTokens.cake,
        quoteToken: serializedTokens.usdt
    },
    {
        pid: 357,
        lpSymbol: 'SUTER-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x2d5DB889392Bc3c8B023A8631ca230A033eEA1B8'
        },
        token: serializedTokens.suter,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 421,
        lpSymbol: 'BSCPAD-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xba01662e978de7d67f8ffc937726215eb8995d17'
        },
        token: serializedTokens.bscpad,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 420,
        lpSymbol: 'RABBIT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x04b56A5B3f45CFeaFbfDCFc999c14be5434f2146'
        },
        token: serializedTokens.rabbit,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 419,
        lpSymbol: 'WAULTx-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x3e4370204f598205998143F07ebCC486E441b456'
        },
        token: serializedTokens.waultx,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 418,
        lpSymbol: 'WEX-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x547A355E70cd1F8CAF531B950905aF751dBEF5E6'
        },
        token: serializedTokens.wex,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 417,
        lpSymbol: 'FORM-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x3E19C18Fe3458A6065D8F0844cB7Eae52C9DAE07'
        },
        token: serializedTokens.form,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 416,
        lpSymbol: 'ORBS-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xB87b857670A44356f2b70337E0F218713D2378e8'
        },
        token: serializedTokens.orbs,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 415,
        lpSymbol: 'DG-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x8b2824d57eebf07f5aff5c91fa67ed7c501a9f43'
        },
        token: serializedTokens.$dg,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 414,
        lpSymbol: 'WOO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x89eE0491CE55d2f7472A97602a95426216167189'
        },
        token: serializedTokens.woo,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 413,
        lpSymbol: 'HTB-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x2a995d355d5df641e878c0f366685741fd18d004'
        },
        token: serializedTokens.htb,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 337,
        lpSymbol: 'DFT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x24d3B0eD4C444A4f6882d527cBF67aDc8c026582'
        },
        token: serializedTokens.dft,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 412,
        lpSymbol: 'HAI-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x289841bFb694767bCb56fBc7B741aB4B4D97D490'
        },
        token: serializedTokens.hai,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 411,
        lpSymbol: 'O3-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x7759283571Da8c0928786A96AE601944E10461Ff'
        },
        token: serializedTokens.o3,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 410,
        lpSymbol: 'AMPL-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x6e98beb694ff1cdb1ee130edd2b21b0298683d58'
        },
        token: serializedTokens.ampl,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 343,
        lpSymbol: 'ODDZ-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x3c2c77353E2F6AC1578807b6b2336Bf3a3CbB014'
        },
        token: serializedTokens.oddz,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 409,
        lpSymbol: 'ETH-USDC LP',
        lpAddresses: {
            97: '',
            56: '0xEa26B78255Df2bBC31C1eBf60010D78670185bD0'
        },
        token: serializedTokens.eth,
        quoteToken: serializedTokens.usdc
    },
    {
        pid: 408,
        lpSymbol: 'BTCB-ETH LP',
        lpAddresses: {
            97: '',
            56: '0xD171B26E4484402de70e3Ea256bE5A2630d7e88D'
        },
        token: serializedTokens.btcb,
        quoteToken: serializedTokens.eth
    },
    {
        pid: 347,
        lpSymbol: 'BONDLY-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xb8b4383B49d451BBeA63BC4421466E1086da6f18'
        },
        token: serializedTokens.bondly,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 407,
        lpSymbol: 'MARSH-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x222f93187f15f354d41ff6a7703ef7e18cdd5103'
        },
        token: serializedTokens.marsh,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 406,
        lpSymbol: 'BORING-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xDfA808Da5CFB9ABA5Fb3748FF85888F79174F378'
        },
        token: serializedTokens.boring,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 405,
        lpSymbol: 'MBOX-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x8FA59693458289914dB0097F5F366d771B7a7C3F'
        },
        token: serializedTokens.mbox,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 404,
        lpSymbol: 'ATA-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xef7767677867552cfa699148b96a03358a9be779'
        },
        token: serializedTokens.ata,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 403,
        lpSymbol: 'MX-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x41f049d990d38305504631c9835f6f856bf1ba67'
        },
        token: serializedTokens.mx,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 402,
        lpSymbol: 'bCFX-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xA0387eBeA6be90849c2261b911fBBD52B4C9eAC4'
        },
        token: serializedTokens.bcfx,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 401,
        lpSymbol: 'QKC-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x8853e3309a31583ea438f7704681f46f0d4d909b'
        },
        token: serializedTokens.qkc,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 400,
        lpSymbol: 'KTN-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x48028de4a9b0d3d91180333d796021ec7757ba1b'
        },
        token: serializedTokens.ktn,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 399,
        lpSymbol: 'MTRG-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x4dcA4D427511bC327639b222DA18FA5e334F686F'
        },
        token: serializedTokens.mtrg,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 398,
        lpSymbol: 'SWG-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x226af4e918fcf3e62e5eeec867a3e78aaa7bb01d'
        },
        token: serializedTokens.swg,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 396,
        lpSymbol: 'VRT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xef5212ada83ec2cc105c409df10b8806d20e3b35'
        },
        token: serializedTokens.vrt,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 395,
        lpSymbol: 'EZ-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x51bf99bbae59b67e5ce2fa9c17b683384773f8b3'
        },
        token: serializedTokens.ez,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 394,
        lpSymbol: 'KALM-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xc74f7243766269dec5b85b0ef4af186e909c1b06'
        },
        token: serializedTokens.kalm,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 393,
        lpSymbol: 'pOPEN-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x1090c996fd1490d15dd7906322ee676a5cc3cf82'
        },
        token: serializedTokens.popen,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 335,
        lpSymbol: 'LIEN-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xa4963B38b271c0D714593063497Fc786Fa4029Ce'
        },
        token: serializedTokens.lien,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 392,
        lpSymbol: 'WELL-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x1d94cb25895abd6ccfef863c53372bb462aa6b86'
        },
        token: serializedTokens.well,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 391,
        lpSymbol: 'DERI-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xdc7188ac11e124b1fa650b73ba88bf615ef15256'
        },
        token: serializedTokens.deri,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 390,
        lpSymbol: 'CHR-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x6045931e511ef7e53a4a817f971e0ca28c758809'
        },
        token: serializedTokens.chr,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 389,
        lpSymbol: 'CAKE-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x804678fa97d91B974ec2af3c843270886528a9E6'
        },
        token: serializedTokens.cake,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 388,
        lpSymbol: 'CYC-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xecf30fbecfa642012f54212a3be92eef1e48edac'
        },
        token: serializedTokens.cyc,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 387,
        lpSymbol: 'XEND-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xcecfc2789af72ed151589a96a59f3a1abc65c3b5'
        },
        token: serializedTokens.xend,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 296,
        lpSymbol: 'HGET-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xF74ee1e10e097dc326a2ad004F9Cc95CB71088d3'
        },
        token: serializedTokens.hget,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 385,
        lpSymbol: 'RFOX-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x8e04b3972b5c25766c681dfd30a8a1cbf6dcc8c1'
        },
        token: serializedTokens.rfox,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 384,
        lpSymbol: 'WMASS-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xec95ff6281c3ad8e27372fa6675eb337640b8e5e'
        },
        token: serializedTokens.wmass,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 383,
        lpSymbol: 'UBXT-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x8d3ff27d2ad6a9556b7c4f82f4d602d20114bc90'
        },
        token: serializedTokens.ubxt,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 381,
        lpSymbol: 'BTR-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xbc7ac609fa730239190a70952e64ee1dfc2530ac'
        },
        token: serializedTokens.btr,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 380,
        lpSymbol: 'τDOGE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x2030845Ce7d4224523fd2F03Ca20Afe4aAD1D890'
        },
        token: serializedTokens.τdoge,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 378,
        lpSymbol: 'ONE-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x4d057f769d930eafd597b49d6fb2e1009a73a702'
        },
        token: serializedTokens.one,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 377,
        lpSymbol: 'FINE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xc309a6d2f1537922e06f15aa2eb21caa1b2eedb6'
        },
        token: serializedTokens.fine,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 376,
        lpSymbol: 'DOGE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xac109c8025f272414fd9e2faa805a583708a017f'
        },
        token: serializedTokens.doge,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 310,
        lpSymbol: 'bMXX-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xc20A92a1424b29b78DFaF92FD35D4cf8A06419B4'
        },
        token: serializedTokens.bmxx,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 375,
        lpSymbol: 'OIN-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x6a00e41561ac36a78dba1d09091b0f00c4e53724'
        },
        token: serializedTokens.oin,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 374,
        lpSymbol: 'HYFI-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x0716725d78081a9e0e1ff81516f5415b399e274d'
        },
        token: serializedTokens.hyfi,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 373,
        lpSymbol: 'KUN-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xea61020e5a128d2bec67d48f7cfbe3408db7e391'
        },
        token: serializedTokens.kun,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 372,
        lpSymbol: 'KUN-QSD LP',
        lpAddresses: {
            97: '',
            56: '0x4eafbf68a2d50291ffd163d4e00ad0f040aae707'
        },
        token: serializedTokens.kun,
        quoteToken: serializedTokens.qsd
    },
    {
        pid: 371,
        lpSymbol: 'MATH-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xb7cada0f120ca46745a024e6b9fe907b2fe10cf3'
        },
        token: serializedTokens.math,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 370,
        lpSymbol: 'mCOIN-UST LP',
        lpAddresses: {
            97: '',
            56: '0xbcf01a42f6bc42f3cfe81b05519565044d65d22a'
        },
        token: serializedTokens.mcoin,
        quoteToken: serializedTokens.ust
    },
    {
        pid: 366,
        lpSymbol: 'PNT-pBTC LP',
        lpAddresses: {
            97: '',
            56: '0xdaa89d335926628367b47852989bb22ee62ca5de'
        },
        token: serializedTokens.pnt,
        quoteToken: serializedTokens.pbtc
    },
    {
        pid: 311,
        lpSymbol: 'xMARK-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xAa40f1AC20AAFcFEE8595Da606D78C503C7e70A3'
        },
        token: serializedTokens.xmark,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 365,
        lpSymbol: 'BTCB-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xf45cd219aef8618a92baa7ad848364a158a24f33'
        },
        token: serializedTokens.btcb,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 364,
        lpSymbol: 'LMT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x8271d7eafeeb8f24d7c9fe1acce2ae20611972e5'
        },
        token: serializedTokens.lmt,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 363,
        lpSymbol: 'DFD-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x029d66f9c0469450b7b4834b8ddc6a1118cec3e1'
        },
        token: serializedTokens.dfd,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 362,
        lpSymbol: 'ALPACA-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x7752e1fa9f3a2e860856458517008558deb989e3'
        },
        token: serializedTokens.alpaca,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 361,
        lpSymbol: 'τBTC-BTCB LP',
        lpAddresses: {
            97: '',
            56: '0x8046fa66753928F35f7Db23ae0188ee6743C2FBA'
        },
        token: serializedTokens.τbtc,
        quoteToken: serializedTokens.btcb
    },
    {
        pid: 304,
        lpSymbol: 'SWINGBY-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x4Fd6D315bEf387fAD2322fbc64368fC443F0886D'
        },
        token: serializedTokens.swingby,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 360,
        lpSymbol: 'XED-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xa7A0b605343dF36B748FF4B5f7578b3F2D0651CE'
        },
        token: serializedTokens.xed,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 359,
        lpSymbol: 'HAKKA-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x2C624C9Ecf16Cb81aB85cC2C0B0c5e12A09AFDa6'
        },
        token: serializedTokens.hakka,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 358,
        lpSymbol: 'CGG-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x0604471c532F9fEBAD3E37190B667f44BD0894b3'
        },
        token: serializedTokens.cgg,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 356,
        lpSymbol: 'bROOBEE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x950FD020F8E4B8C57285EC7020b7a204348dadFa'
        },
        token: serializedTokens.broobee,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 355,
        lpSymbol: 'HZN-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xDc9a574b9B341D4a98cE29005b614e1E27430E74'
        },
        token: serializedTokens.hzn,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 354,
        lpSymbol: 'ALPA-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x4cC442220BE1cE560C1f2573f8CA8f460B3E4172'
        },
        token: serializedTokens.alpa,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 353,
        lpSymbol: 'PERL-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x61010e6CbA3b56ba47E9dFd56Da682daCFe76131'
        },
        token: serializedTokens.perl,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 352,
        lpSymbol: 'TLM-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xE6b421a4408c82381b226Ab5B6F8C4b639044359'
        },
        token: serializedTokens.tlm,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 350,
        lpSymbol: 'EPS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xddE420cbB3794ebD8FFC3Ac69F9c78e5d1411870'
        },
        token: serializedTokens.eps,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 349,
        lpSymbol: 'ARPA-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x9730c791743300E9f984C9264395ce705A55Da7c'
        },
        token: serializedTokens.arpa,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 348,
        lpSymbol: 'ITAM-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xd02DA76c813b9cd4516eD50442923E625f90228f'
        },
        token: serializedTokens.itam,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 346,
        lpSymbol: 'TKO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xFFd4B200d3C77A0B691B5562D804b3bd54294e6e'
        },
        token: serializedTokens.tko,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 345,
        lpSymbol: 'APYS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x510b29a93ebf098f3fC24A16541aAA0114D07056'
        },
        token: serializedTokens.apys,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 344,
        lpSymbol: 'HOO-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x3e42C1f7239231E3752B507764445dd8e6A570d5'
        },
        token: serializedTokens.hoo,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 342,
        lpSymbol: 'EASY-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x20c6De8983Fb2D641c55004646aEF40b4EA66E18'
        },
        token: serializedTokens.easy,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 341,
        lpSymbol: 'NRV-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xE482249Cd295C0d1e9D2baAEE71e66de21024C68'
        },
        token: serializedTokens.nrv,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 340,
        lpSymbol: 'DEGO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xF1Ec67fA1881796BFf63Db3E1A301cE9cb787Fad'
        },
        token: serializedTokens.dego,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 338,
        lpSymbol: 'pBTC-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x0362ba706DFE8ED12Ec1470aB171d8Dcb1C72B8D'
        },
        token: serializedTokens.pbtc,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 336,
        lpSymbol: 'SWTH-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x123D475E13aA54A43a7421d94CAa4459dA021c77'
        },
        token: serializedTokens.swth,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 334,
        lpSymbol: 'ZIL-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x6A97867a4b7Eb7646ffB1F359ad582e9903aa1C2'
        },
        token: serializedTokens.zil,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 333,
        lpSymbol: 'pCWS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x6615CE60D71513aA4849269dD63821D324A23F8C'
        },
        token: serializedTokens.pcws,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 332,
        lpSymbol: 'bBADGER-BTCB LP',
        lpAddresses: {
            97: '',
            56: '0x5A58609dA96469E9dEf3fE344bC39B00d18eb9A5'
        },
        token: serializedTokens.bbadger,
        quoteToken: serializedTokens.btcb
    },
    {
        pid: 331,
        lpSymbol: 'bDIGG-BTCB LP',
        lpAddresses: {
            97: '',
            56: '0x81d776C90c89B8d51E9497D58338933127e2fA80'
        },
        token: serializedTokens.bdigg,
        quoteToken: serializedTokens.btcb
    },
    {
        pid: 330,
        lpSymbol: 'LTO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xa5Bb44c6F5fD9B836E5a654c8AbbCCc96A15deE5'
        },
        token: serializedTokens.lto,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 329,
        lpSymbol: 'MIR-UST LP',
        lpAddresses: {
            97: '',
            56: '0x89666d026696660e93Bf6edf57B71A68615768B7'
        },
        token: serializedTokens.mir,
        quoteToken: serializedTokens.ust
    },
    {
        pid: 328,
        lpSymbol: 'TRADE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x8F6baf368E7A4f6e2C9c995f22702d5e654A0237'
        },
        token: serializedTokens.trade,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 327,
        lpSymbol: 'DUSK-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x678EDb8B268e73dB57b7694c163e1dc296b6e219'
        },
        token: serializedTokens.dusk,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 326,
        lpSymbol: 'BIFI-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x3f1A9f3D9aaD8bD339eD4853F345d2eF89fbfE0c'
        },
        token: serializedTokens.bifi,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 325,
        lpSymbol: 'TXL-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x1434BB50196A0C7eA825940b1DFd8aAd25d79817'
        },
        token: serializedTokens.txl,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 324,
        lpSymbol: 'COS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xe98585bBb2dc81854fF100A3d9D7B0F53E0dafEd'
        },
        token: serializedTokens.cos,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 323,
        lpSymbol: 'BUNNY-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x5aFEf8567414F29f0f927A0F2787b188624c10E2'
        },
        token: serializedTokens.bunny,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 322,
        lpSymbol: 'ALICE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xcAD7019D6d84a3294b0494aEF02e73BD0f2572Eb'
        },
        token: serializedTokens.alice,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 321,
        lpSymbol: 'FOR-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xE60B4e87645093A42fa9dcC5d0C8Df6E67f1f9d2'
        },
        token: serializedTokens.for,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 320,
        lpSymbol: 'BUX-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x222C3CbB89647bF77822435Bd4c234A04272A77A'
        },
        token: serializedTokens.bux,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 319,
        lpSymbol: 'NULS-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x853784B7BDe87d858555715c0123374242db7943'
        },
        token: serializedTokens.nuls,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 318,
        lpSymbol: 'BELT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xF3Bc6FC080ffCC30d93dF48BFA2aA14b869554bb'
        },
        token: serializedTokens.belt,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 316,
        lpSymbol: 'BFI-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x153Ad7d25B0b810497483d0cEE8AF42Fc533FeC8'
        },
        token: serializedTokens.bfi,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 315,
        lpSymbol: 'DEXE-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x3578B1f9BCE98D2F4D293b422d8850fdf48B1f21'
        },
        token: serializedTokens.dexe,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 313,
        lpSymbol: 'TPT-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x6D0c831254221ba121fB53fb44Df289A6558867d'
        },
        token: serializedTokens.tpt,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 312,
        lpSymbol: 'WATCH-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x13321AcfF4A27f3d2bcA64b8bEaC6e5FdAAAf12C'
        },
        token: serializedTokens.watch,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 309,
        lpSymbol: 'IOTX-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xc13aA76AAc067c86aE38028019F414D731b3D86A'
        },
        token: serializedTokens.iotx,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 308,
        lpSymbol: 'BOR-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xe094c686aD6cDda57b9564457F541FBF099B948A'
        },
        token: serializedTokens.bor,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 307,
        lpSymbol: 'bOPEN-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xc7A9c2af263ebB86139Cca9349e49b17129Ba033'
        },
        token: serializedTokens.bopen,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 306,
        lpSymbol: 'SUSHI-ETH LP',
        lpAddresses: {
            97: '',
            56: '0x16aFc4F2Ad82986bbE2a4525601F8199AB9c832D'
        },
        token: serializedTokens.sushi,
        quoteToken: serializedTokens.eth
    },
    {
        pid: 305,
        lpSymbol: 'DODO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xA9986Fcbdb23c2E8B11AB40102990a08f8E58f06'
        },
        token: serializedTokens.dodo,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 303,
        lpSymbol: 'BRY-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x21dD71aB78EDE3033c976948f769D506E4F489eE'
        },
        token: serializedTokens.bry,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 302,
        lpSymbol: 'ZEE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x8e799cB0737525CeB8A6C6Ad07f748535fF6377B'
        },
        token: serializedTokens.zee,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 301,
        lpSymbol: 'SWGb-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x856f9AD94cA8680B899214Bb1EB3d235a3C33Afe'
        },
        token: serializedTokens.swgb,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 300,
        lpSymbol: 'COMP-ETH LP',
        lpAddresses: {
            97: '',
            56: '0x37908620dEf1491Dd591b5a2d16022A33cDDA415'
        },
        token: serializedTokens.comp,
        quoteToken: serializedTokens.eth
    },
    {
        pid: 299,
        lpSymbol: 'SFP-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x942b294e59a8c47a0F7F20DF105B082710F7C305'
        },
        token: serializedTokens.sfp,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 298,
        lpSymbol: 'LINA-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xC5768c5371568Cf1114cddD52CAeD163A42626Ed'
        },
        token: serializedTokens.lina,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 297,
        lpSymbol: 'LIT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x1F37d4226d23d09044B8005c127C0517BD7e94fD'
        },
        token: serializedTokens.lit,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 295,
        lpSymbol: 'BDO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x4288706624e3dD839b069216eB03B8B9819C10d2'
        },
        token: serializedTokens.bdo,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 294,
        lpSymbol: 'EGLD-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xcD68856b6E72E99b5eEaAE7d41Bb4A3b484c700D'
        },
        token: serializedTokens.egld,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 293,
        lpSymbol: 'UST-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x05faf555522Fa3F93959F86B41A3808666093210'
        },
        token: serializedTokens.ust,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 292,
        lpSymbol: 'mAMZN-UST LP',
        lpAddresses: {
            97: '',
            56: '0xC05654C66756eBB82c518598c5f1ea1a0199a563'
        },
        token: serializedTokens.mamzn,
        quoteToken: serializedTokens.ust
    },
    {
        pid: 291,
        lpSymbol: 'mGOOGL-UST LP',
        lpAddresses: {
            97: '',
            56: '0xA3BfBbAd526C6B856B1Fdf73F99BCD894761fbf3'
        },
        token: serializedTokens.mgoogl,
        quoteToken: serializedTokens.ust
    },
    {
        pid: 290,
        lpSymbol: 'mNFLX-UST LP',
        lpAddresses: {
            97: '',
            56: '0x91417426C3FEaA3Ca795921eB9FdD9715ad92537'
        },
        token: serializedTokens.mnflx,
        quoteToken: serializedTokens.ust
    },
    {
        pid: 289,
        lpSymbol: 'mTSLA-UST LP',
        lpAddresses: {
            97: '',
            56: '0xEc6b56a736859AE8ea4bEdA16279Ecd8c60dA7EA'
        },
        token: serializedTokens.mtsla,
        quoteToken: serializedTokens.ust
    },
    {
        pid: 288,
        lpSymbol: 'wSOTE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x7653D2c31440f04d2c6520D482dC5DbD7650f70a'
        },
        token: serializedTokens.wsote,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 287,
        lpSymbol: 'FRONT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xC6b668548aA4A56792e8002A920d3159728121D5'
        },
        token: serializedTokens.front,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 286,
        lpSymbol: 'Helmet-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xC869A9943b702B03770B6A92d2b2d25cf3a3f571'
        },
        token: serializedTokens.helmet,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 285,
        lpSymbol: 'BTCST-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xB2678C414ebC63c9CC6d1a0fC45f43E249B50fdE'
        },
        token: serializedTokens.btcst,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 284,
        lpSymbol: 'LTC-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x71b01eBdDD797c8E9E0b003ea2f4FD207fBF46cC'
        },
        token: serializedTokens.ltc,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 283,
        lpSymbol: 'USDC-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x2354ef4DF11afacb85a5C7f98B624072ECcddbB1'
        },
        token: serializedTokens.usdc,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 282,
        lpSymbol: 'DAI-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x66FDB2eCCfB58cF098eaa419e5EfDe841368e489'
        },
        token: serializedTokens.dai,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 281,
        lpSymbol: 'BSCX-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x47C42b0A056A9C6e9C65b9Ef79020Af518e767A5'
        },
        token: serializedTokens.bscx,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 280,
        lpSymbol: 'TEN-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x1B415C3ec8095AfBF9d78882b3a6263c4ad141B5'
        },
        token: serializedTokens.ten,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 279,
        lpSymbol: 'bALBT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x24EB18bA412701f278B172ef96697c4622b19da6'
        },
        token: serializedTokens.balbt,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 278,
        lpSymbol: 'REEF-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xd63b5CecB1f40d626307B92706Df357709D05827'
        },
        token: serializedTokens.reef,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 277,
        lpSymbol: 'Ditto-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x8645148dE4E339964bA480AE3478653b5bc6E211'
        },
        token: serializedTokens.ditto,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 276,
        lpSymbol: 'VAI-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x133ee93FE93320e1182923E1a640912eDE17C90C'
        },
        token: serializedTokens.vai,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 275,
        lpSymbol: 'BLK-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x356Dd24BfF8e23BdE0430f00ad0C290E33438bD7'
        },
        token: serializedTokens.blink,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 274,
        lpSymbol: 'UNFI-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x44EA47F2765fd5D26b7eF0222736AD6FD6f61950'
        },
        token: serializedTokens.unfi,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 273,
        lpSymbol: 'HARD-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x73566ca86248bD12F0979793e4671e99a40299A7'
        },
        token: serializedTokens.hard,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 272,
        lpSymbol: 'CTK-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x460b4193Ec4C1a17372Aa5FDcd44c520ba658646'
        },
        token: serializedTokens.ctk,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 271,
        lpSymbol: 'SXP-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xD8E2F8b6Db204c405543953Ef6359912FE3A88d6'
        },
        token: serializedTokens.sxp,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 270,
        lpSymbol: 'INJ-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x1BdCebcA3b93af70b58C41272AEa2231754B23ca'
        },
        token: serializedTokens.inj,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 269,
        lpSymbol: 'FIL-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xD9bCcbbbDFd9d67BEb5d2273102CE0762421D1e3'
        },
        token: serializedTokens.fil,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 268,
        lpSymbol: 'UNI-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x014608E87AF97a054C9a49f81E1473076D51d9a3'
        },
        token: serializedTokens.uni,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 267,
        lpSymbol: 'YFI-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xCE383277847f8217392eeA98C5a8B4a7D27811b0'
        },
        token: serializedTokens.yfi,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 266,
        lpSymbol: 'ATOM-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x468b2DC8DC75990eE3E9dc0648965Ad6294E7914'
        },
        token: serializedTokens.atom,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 265,
        lpSymbol: 'XRP-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x03F18135c44C64ebFdCBad8297fe5bDafdBbdd86'
        },
        token: serializedTokens.xrp,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 264,
        lpSymbol: 'USDT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x16b9a82891338f9bA80E2D6970FddA79D1eb0daE'
        },
        token: serializedTokens.usdt,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 263,
        lpSymbol: 'ALPHA-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xACF47CBEaab5c8A6Ee99263cfE43995f89fB3206'
        },
        token: serializedTokens.alpha,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 262,
        lpSymbol: 'BTCB-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x61EB789d75A95CAa3fF50ed7E47b96c132fEc082'
        },
        token: serializedTokens.btcb,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 261,
        lpSymbol: 'ETH-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x74E4716E431f45807DCF19f284c7aA99F18a4fbc'
        },
        token: serializedTokens.eth,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 260,
        lpSymbol: 'XVS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x7EB5D86FD78f3852a3e0e064f2842d45a3dB6EA2'
        },
        token: serializedTokens.xvs,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 259,
        lpSymbol: 'TWT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x3DcB1787a95D2ea0Eb7d00887704EeBF0D79bb13'
        },
        token: serializedTokens.twt,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 258,
        lpSymbol: 'USDT-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x7EFaEf62fDdCCa950418312c6C91Aef321375A00'
        },
        token: serializedTokens.usdt,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 257,
        lpSymbol: 'LINK-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x824eb9faDFb377394430d2744fa7C42916DE3eCe'
        },
        token: serializedTokens.link,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 256,
        lpSymbol: 'EOS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xB6e34b5C65Eda51bb1BD4ea5F79d385Fb94b9504'
        },
        token: serializedTokens.eos,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 255,
        lpSymbol: 'DOT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xDd5bAd8f8b360d76d12FdA230F8BAF42fe0022CF'
        },
        token: serializedTokens.dot,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 254,
        lpSymbol: 'BAND-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x168B273278F3A8d302De5E879aA30690B7E6c28f'
        },
        token: serializedTokens.band,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 253,
        lpSymbol: 'ADA-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x28415ff2C35b65B9E5c7de82126b4015ab9d031F'
        },
        token: serializedTokens.ada,
        quoteToken: serializedTokens.wbnb
    },
    /**
   * V2 farms, set to be removed once unstaked
   */ {
        pid: 139,
        lpSymbol: 'CAKE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xFB7E9FE9D13561AdA7131Fa746942a14F7dd4Cf6'
        },
        token: serializedTokens.cake,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 250,
        lpSymbol: 'τBTC-BTCB LP',
        lpAddresses: {
            97: '',
            56: '0xFD09CDbd6A7dCAd8AC47df4F139443a729264763'
        },
        token: serializedTokens.τbtc,
        quoteToken: serializedTokens.btcb
    },
    {
        pid: 193,
        lpSymbol: 'SWINGBY-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xA0e3F72BAFcc5d52F0052a39165FD40D3d4d34Fc'
        },
        token: serializedTokens.swingby,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 249,
        lpSymbol: 'XED-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xBbE20dA99db94Fa1077F1C9A5d256761dAf89C60'
        },
        token: serializedTokens.xed,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 248,
        lpSymbol: 'HAKKA-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x9ed1ca73AA8F1ccdc3c3a174E77014f8900411CE'
        },
        token: serializedTokens.hakka,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 247,
        lpSymbol: 'CGG-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xB9aA8B0d67DE546aaa82091065a64B7F1C4B1a1F'
        },
        token: serializedTokens.cgg,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 246,
        lpSymbol: 'SUTER-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x6f41c9226fa89a552009c3AC087BA74b83772C52'
        },
        token: serializedTokens.suter,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 245,
        lpSymbol: 'bROOBEE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x5Ac5184eA06dE24ce8ED2133f58b4Aa2CEd2dC3b'
        },
        token: serializedTokens.broobee,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 244,
        lpSymbol: 'HZN-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xF7fcD7e7B3853bf59bCA9183476F218ED07eD3B0'
        },
        token: serializedTokens.hzn,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 243,
        lpSymbol: 'ALPA-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xBB41898A3051A6b6D4A36a1c43e906b05799B744'
        },
        token: serializedTokens.alpa,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 242,
        lpSymbol: 'PERL-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xB1C2e08A992a619DA570425E78828A8508654f4F'
        },
        token: serializedTokens.perl,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 241,
        lpSymbol: 'TLM-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x25f15Cb3D3B3753702E1d5c4E5f6F0720b197843'
        },
        token: serializedTokens.tlm,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 240,
        lpSymbol: 'JGN-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x8fD5ca41B2B44e4713590584f97c85f9FF59F00D'
        },
        token: serializedTokens.jgn,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 239,
        lpSymbol: 'EPS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x43bc6C256240e657Ad84aFb86825E21B48FEDe78'
        },
        token: serializedTokens.eps,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 238,
        lpSymbol: 'ARPA-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xD55e5A7b886aE9657b95641c6A7dc5A662EcAbF3'
        },
        token: serializedTokens.arpa,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 237,
        lpSymbol: 'ITAM-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x3e78b0eD211a49e263fF9b3F0B410932a021E368'
        },
        token: serializedTokens.itam,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 236,
        lpSymbol: 'BONDLY-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x2205a6424ec4D74a7588450fB71ffd0C4A3Ead65'
        },
        token: serializedTokens.bondly,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 235,
        lpSymbol: 'TKO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xc43EdF4a7e89160135C2553E9868446fef9C18DD'
        },
        token: serializedTokens.tko,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 234,
        lpSymbol: 'APYS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x7A5523f50a80790cAD011167E20bD21056A2f04A'
        },
        token: serializedTokens.apys,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 233,
        lpSymbol: 'HOO-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xc12dAD966505443b5aad7b0C55716c13d285B520'
        },
        token: serializedTokens.hoo,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 232,
        lpSymbol: 'ODDZ-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x3B0a7d1030bcDFf45ABB7B03C04110FcCc8095BC'
        },
        token: serializedTokens.oddz,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 231,
        lpSymbol: 'EASY-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x4b0ec41404a7FF59BaE33C8Dc420804c58B7bF24'
        },
        token: serializedTokens.easy,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 230,
        lpSymbol: 'NRV-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x25dea33a42c7775F6945fae22A8fFBfAC9fB22CD'
        },
        token: serializedTokens.nrv,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 229,
        lpSymbol: 'DEGO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x6108aBd546AF17D8f7aFAe59EBfb4A01132A11Bb'
        },
        token: serializedTokens.dego,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 228,
        lpSymbol: 'GUM-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xA99d1926a3c15DC4Fb83aB3Fafd63B6C3E87CF22'
        },
        token: serializedTokens.gum,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 227,
        lpSymbol: 'pBTC-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xaccd6673FFc24cD56B080D71384327f78fD92496'
        },
        token: serializedTokens.pbtc,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 226,
        lpSymbol: 'DFT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xe86d075051f20eb8c741007Cb8e262f4519944ee'
        },
        token: serializedTokens.dft,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 225,
        lpSymbol: 'SWTH-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x4f6dfFc9795d35dc1D92c2a7B23Cb7d6EF190B33'
        },
        token: serializedTokens.swth,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 224,
        lpSymbol: 'LIEN-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xbe7BDE4aD1c136038Dc9f57ef94d1d16e6F9CbF7'
        },
        token: serializedTokens.lien,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 223,
        lpSymbol: 'ZIL-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xcBDf499db66Df19A66aB48F16C790FF9eE872add'
        },
        token: serializedTokens.zil,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 222,
        lpSymbol: 'pCWS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xe3D941e74141311436F82523817EBaa26462967d'
        },
        token: serializedTokens.pcws,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 221,
        lpSymbol: 'bBADGER-BTCB LP',
        lpAddresses: {
            97: '',
            56: '0x87Ae7b5c43D4e160cDB9427a78BA87B9503ee37b'
        },
        token: serializedTokens.bbadger,
        quoteToken: serializedTokens.btcb
    },
    {
        pid: 220,
        lpSymbol: 'bDIGG-BTCB LP',
        lpAddresses: {
            97: '',
            56: '0xfbfa92e037e37F946c0105902640914E3aCe6752'
        },
        token: serializedTokens.bdigg,
        quoteToken: serializedTokens.btcb
    },
    {
        pid: 219,
        lpSymbol: 'LTO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xF62e92292772F24EAa6B6B8a105c9FC7B8F31EC5'
        },
        token: serializedTokens.lto,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 218,
        lpSymbol: 'MIR-UST LP',
        lpAddresses: {
            97: '',
            56: '0x905186a70ba3Eb50090d1d0f6914F5460B4DdB40'
        },
        token: serializedTokens.mir,
        quoteToken: serializedTokens.ust
    },
    {
        pid: 217,
        lpSymbol: 'TRADE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x52fCfB6d91Bcf1F1f6d375D0f6c303688b0E8550'
        },
        token: serializedTokens.trade,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 216,
        lpSymbol: 'DUSK-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x69773f622cE228Ca7dEd42D8C34Eba8582e85dcA'
        },
        token: serializedTokens.dusk,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 215,
        lpSymbol: 'BIFI-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x01956F08A55e4FF9775bc01aF6ACb09144564837'
        },
        token: serializedTokens.bifi,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 214,
        lpSymbol: 'TXL-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x8Ba7eB4056338fd7271E1b7431C8ca3827eF907c'
        },
        token: serializedTokens.txl,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 213,
        lpSymbol: 'COS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xAfB2e729A24629aBdE8E55CEB0e1f899bEe0f70f'
        },
        token: serializedTokens.cos,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 212,
        lpSymbol: 'BUNNY-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x283FA8d459Da6e3165B2faF7FA0DD0137503DECf'
        },
        token: serializedTokens.bunny,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 211,
        lpSymbol: 'ALICE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x9e1BB5033d47BF8F16FC017CEC0959De7FF00833'
        },
        token: serializedTokens.alice,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 210,
        lpSymbol: 'FOR-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xaBB817B07663521Cf64B006EC9D0FF185b65cfE5'
        },
        token: serializedTokens.for,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 209,
        lpSymbol: 'BUX-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x7aA4eb5c3bF33e3AD41A47e26b3Bd9b902984610'
        },
        token: serializedTokens.bux,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 208,
        lpSymbol: 'NULS-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xaB46737CAAFbB99999f8b91E4D3C6D4D28E10e05'
        },
        token: serializedTokens.nuls,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 207,
        lpSymbol: 'BELT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x446ff2C0F5350bF2dadD0e0F1AaAA573b362CA6B'
        },
        token: serializedTokens.belt,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 206,
        lpSymbol: 'RAMP-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x6ED589e69D1927AC45054cBb6E57877879384d6F'
        },
        token: serializedTokens.ramp,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 205,
        lpSymbol: 'BFI-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xFFA2357f1E6f48d74b1c18c363c3Fe58A032405a'
        },
        token: serializedTokens.bfi,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 204,
        lpSymbol: 'DEXE-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x534b0b0700c0cfF9785852707f07f60E7C0bc07E'
        },
        token: serializedTokens.dexe,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 203,
        lpSymbol: 'BEL-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x2013265224E3cB6A53C67130F9Fe53Ae36CFcfdd'
        },
        token: serializedTokens.bel,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 202,
        lpSymbol: 'TPT-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xC14c2dd21d6aEA3C2068A1F8e58d41D3c28F9288'
        },
        token: serializedTokens.tpt,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 201,
        lpSymbol: 'WATCH-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xD5fBfFf5faB9d29f614d9bd50AF9b1356C53049C'
        },
        token: serializedTokens.watch,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 200,
        lpSymbol: 'xMARK-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x601aE41C5a65b2089a6af2CcfaF984896a1f52AD'
        },
        token: serializedTokens.xmark,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 199,
        lpSymbol: 'bMXX-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x037d38c7DfF5732DAA5f8C05478Eb75cdf24f42B'
        },
        token: serializedTokens.bmxx,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 198,
        lpSymbol: 'IOTX-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x8503462D0d4D3ce73e857bCC7D0Ef1125B0d66fF'
        },
        token: serializedTokens.iotx,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 197,
        lpSymbol: 'BOR-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xE0243Ce3b50bd551168cE6964F178507d0a1acD5'
        },
        token: serializedTokens.bor,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 196,
        lpSymbol: 'bOPEN-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xD2FcF98EaeD2c08e9BcA854802C07b93D27913aC'
        },
        token: serializedTokens.bopen,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 195,
        lpSymbol: 'SUSHI-ETH LP',
        lpAddresses: {
            97: '',
            56: '0x3BECbb09F622187B544C0892EeDeB58C004117e1'
        },
        token: serializedTokens.sushi,
        quoteToken: serializedTokens.eth
    },
    {
        pid: 194,
        lpSymbol: 'DODO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x220e34306a93002fB7947C9Fc633d6f538bd5032'
        },
        token: serializedTokens.dodo,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 192,
        lpSymbol: 'BRY-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xC3E303647cbD43EC22989275e7ecFA8952A6BA02'
        },
        token: serializedTokens.bry,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 191,
        lpSymbol: 'ZEE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x6d1299B158bd13F4B50e951aaBf2Aa501FD87E52'
        },
        token: serializedTokens.zee,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 190,
        lpSymbol: 'SWGb-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xd2A5008d555371e97F30B6dD71597b4F1eDB0f20'
        },
        token: serializedTokens.swgb,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 189,
        lpSymbol: 'COMP-ETH LP',
        lpAddresses: {
            97: '',
            56: '0x6A55a9176f11c1118f01CBaf6c4033a5c1B22a81'
        },
        token: serializedTokens.comp,
        quoteToken: serializedTokens.eth
    },
    {
        pid: 188,
        lpSymbol: 'SFP-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x01744B868fe98dB669EBf4e9CA557462BAA6097c'
        },
        token: serializedTokens.sfp,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 187,
        lpSymbol: 'LINA-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xb923A2Beeb0834528D20b8973A2c69088571aA9E'
        },
        token: serializedTokens.lina,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 186,
        lpSymbol: 'LIT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x41D69Beda3AFF2FFE48E715e2f4248Cb272cFf30'
        },
        token: serializedTokens.lit,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 185,
        lpSymbol: 'HGET-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x239aD1874114B2235485e34b14c48dB73CCA3ffb'
        },
        token: serializedTokens.hget,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 184,
        lpSymbol: 'BDO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xF7045D7dE334a3F6c1254f98167b2af130eEA8E6'
        },
        token: serializedTokens.bdo,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 183,
        lpSymbol: 'EGLD-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xB4670bBEce2D02c4D30786D173985A984686042C'
        },
        token: serializedTokens.egld,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 182,
        lpSymbol: 'UST-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x1719606031F1E0B3CCaCa11A2CF550Ef8feBEB0F'
        },
        token: serializedTokens.ust,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 181,
        lpSymbol: 'mAMZN-UST LP',
        lpAddresses: {
            97: '',
            56: '0x2c065E42B464ef38480778B0624A207A09042481'
        },
        token: serializedTokens.mamzn,
        quoteToken: serializedTokens.ust
    },
    {
        pid: 180,
        lpSymbol: 'mGOOGL-UST LP',
        lpAddresses: {
            97: '',
            56: '0x74d8Dbac5053d31E904a821A3B4C411Bd4dd2307'
        },
        token: serializedTokens.mgoogl,
        quoteToken: serializedTokens.ust
    },
    {
        pid: 179,
        lpSymbol: 'mNFLX-UST LP',
        lpAddresses: {
            97: '',
            56: '0xe1d76359FE4Eb7f0dAd1D719256c22890864718E'
        },
        token: serializedTokens.mnflx,
        quoteToken: serializedTokens.ust
    },
    {
        pid: 178,
        lpSymbol: 'mTSLA-UST LP',
        lpAddresses: {
            97: '',
            56: '0x36285DDD149949f366b5aFb3f41Cea71d35B8c9e'
        },
        token: serializedTokens.mtsla,
        quoteToken: serializedTokens.ust
    },
    {
        pid: 177,
        lpSymbol: 'wSOTE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xe5909de3822d589c220Fb4FA1660A0Fd251Fa87d'
        },
        token: serializedTokens.wsote,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 176,
        lpSymbol: 'FRONT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x84Df48B3e900C79539F6c523D6F528802BeAa713'
        },
        token: serializedTokens.front,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 175,
        lpSymbol: 'Helmet-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xD09648792d7e77523ae311Fa5A8F38E4684A5f15'
        },
        token: serializedTokens.helmet,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 174,
        lpSymbol: 'BTCST-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xf967845A6D30C44b555C49C50530076dF5D7fd75'
        },
        token: serializedTokens.btcst,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 173,
        lpSymbol: 'LTC-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x286E8d71722c585c9A82876B1B2FB4dEe9fc536E'
        },
        token: serializedTokens.ltc,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 172,
        lpSymbol: 'USDC-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x05FC2ac8a4FA697501087C916c87b8a5dc4f7b46'
        },
        token: serializedTokens.usdc,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 171,
        lpSymbol: 'DAI-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xdaFE10aA3AB6758596aDAC70f6873C49F5a9bf86'
        },
        token: serializedTokens.dai,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 170,
        lpSymbol: 'BSCX-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x5fE5394BBc394345737b8e6e48be2804E89eC0eB'
        },
        token: serializedTokens.bscx,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 169,
        lpSymbol: 'TEN-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x003C4d60de42eAD30739dD204BD153fE69E20Fb2'
        },
        token: serializedTokens.ten,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 168,
        lpSymbol: 'bALBT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x1B8ab50d894CfE793B44057F681A950E87Bd0331'
        },
        token: serializedTokens.balbt,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 167,
        lpSymbol: 'REEF-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x074ed2De503580887073A0F788E035C0fbe13F48'
        },
        token: serializedTokens.reef,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 166,
        lpSymbol: 'Ditto-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xb33D432eACe45DF62F0145228B550b214DCaA6D4'
        },
        token: serializedTokens.ditto,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 165,
        lpSymbol: 'VAI-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x9d95063661fa34B67E0Be0cc71Cf92fc6126aF37'
        },
        token: serializedTokens.vai,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 164,
        lpSymbol: 'BLK-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xdA5a79fFe24739876a52AEF0d419aBB3b2517922'
        },
        token: serializedTokens.blink,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 163,
        lpSymbol: 'UNFI-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x029f944CD3afa7c229122b19c706d8B7c01e062a'
        },
        token: serializedTokens.unfi,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 162,
        lpSymbol: 'HARD-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x158e337e7Dcfcd8FC512840208BB522d122bB19d'
        },
        token: serializedTokens.hard,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 161,
        lpSymbol: 'CTK-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xcbb3fCE7134aF9ef2f3DCe0EAE96db68961b1337'
        },
        token: serializedTokens.ctk,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 160,
        lpSymbol: 'SXP-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x6294D8518b7321dc22E32AA907A89B1DAfc1aDbB'
        },
        token: serializedTokens.sxp,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 159,
        lpSymbol: 'INJ-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x0444712EE8DFF8913B2b44CB1D2a0273b4CDaBe9'
        },
        token: serializedTokens.inj,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 158,
        lpSymbol: 'FIL-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xD027c0B352578b1Cf57f472107591CaE5fa27Eb1'
        },
        token: serializedTokens.fil,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 157,
        lpSymbol: 'UNI-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x2937202a53C82E36bC8beCFBe79795bedF284804'
        },
        token: serializedTokens.uni,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 156,
        lpSymbol: 'YFI-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xfffad7374c894E65b498BDBD489a9a5324A59F60'
        },
        token: serializedTokens.yfi,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 155,
        lpSymbol: 'YFII-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x237E7016Ff50D3B704A7e07571aE08628909A116'
        },
        token: serializedTokens.yfii,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 154,
        lpSymbol: 'ATOM-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x7DD05eF533b1eBCE7815c90678D4B7344E32b8c9'
        },
        token: serializedTokens.atom,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 153,
        lpSymbol: 'XRP-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x0F640E3ec77415Fd810D18B3ac000cD8a172E22f'
        },
        token: serializedTokens.xrp,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 152,
        lpSymbol: 'USDT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x4160910ca32eAD83B6d4b32107974397D2579c2d'
        },
        token: serializedTokens.usdt,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 151,
        lpSymbol: 'ALPHA-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x0edAA38Bd263E83fAECbC8476822800F30eE6028'
        },
        token: serializedTokens.alpha,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 150,
        lpSymbol: 'BTCB-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x356b7d0d3c54F22C82B7a670C6Ba9E2381b0624c'
        },
        token: serializedTokens.btcb,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 149,
        lpSymbol: 'ETH-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x4D7078a6B348766E7a16cD6e6fCb3064721bc6a6'
        },
        token: serializedTokens.eth,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 148,
        lpSymbol: 'XVS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x77B5dB64fD4Cf5B699855420fF2608B2EA6708B3'
        },
        token: serializedTokens.xvs,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 147,
        lpSymbol: 'TWT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x34910518Faf5bfd3a4D15ccFE104B63f06ee3d85'
        },
        token: serializedTokens.twt,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 146,
        lpSymbol: 'USDT-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x28b81C6b155fd9152AE4A09c4eeB7E7F1C114FaC'
        },
        token: serializedTokens.usdt,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 145,
        lpSymbol: 'LINK-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x380941fFd7b7cbf4AEbBfa8A26aa80c2f6570909'
        },
        token: serializedTokens.link,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 144,
        lpSymbol: 'EOS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x082A80b3a55BD3B320a16678784186a979882b21'
        },
        token: serializedTokens.eos,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 143,
        lpSymbol: 'DOT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x3aFfc4dd05286ed6B7206Fc85219d222130e35a9'
        },
        token: serializedTokens.dot,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 142,
        lpSymbol: 'BAND-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x473390697036E7611a670575eA9141583471fF47'
        },
        token: serializedTokens.band,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 141,
        lpSymbol: 'ADA-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xec0C5719cC100DfB6c6F371bb48d3D079ab6A6D2'
        },
        token: serializedTokens.ada,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 140,
        lpSymbol: 'BUSD-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x9bdEdb0c876fC0Da79D945DF28942b898Af89Fc7'
        },
        token: serializedTokens.busd,
        quoteToken: serializedTokens.wbnb
    },
    /**
   * All farms below here are from v1 and are to be set to 0x
   */ {
        pid: 1,
        lpSymbol: 'CAKE-BNB LP',
        lpAddresses: {
            97: '0x3ed8936cAFDF85cfDBa29Fbe5940A5b0524824F4',
            56: '0xA527a61703D82139F8a06Bc30097cC9CAA2df5A6'
        },
        token: serializedTokens.cake,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 136,
        lpSymbol: 'τBTC-BTCB LP',
        lpAddresses: {
            97: '',
            56: '0x2d4e52c48fd18ee06d3959e82ab0f773c41b9277'
        },
        token: serializedTokens.τbtc,
        quoteToken: serializedTokens.btcb
    },
    {
        pid: 76,
        lpSymbol: 'SWINGBY-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x4576C456AF93a37a096235e5d83f812AC9aeD027'
        },
        token: serializedTokens.swingby,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 135,
        lpSymbol: 'XED-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x718d3baa161e1a38758bb0f1fe751e401f431ac4'
        },
        token: serializedTokens.xed,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 134,
        lpSymbol: 'HAKKA-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x56bc8de6e90a8454cb2614b33e750d960aecdf12'
        },
        token: serializedTokens.hakka,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 133,
        lpSymbol: 'CGG-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x484c60f6202c8043DCA0236bB3101ada7ec50AD4'
        },
        token: serializedTokens.cgg,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 132,
        lpSymbol: 'SUTER-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x6Ff75C20656A0E4745E7c114972D361F483AFa5f'
        },
        token: serializedTokens.suter,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 131,
        lpSymbol: 'bROOBEE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x9e6f9f3382f9edc683203b528222c554c92382c2'
        },
        token: serializedTokens.broobee,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 130,
        lpSymbol: 'HZN-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xee4ca18e91012bf87fefde3dd6723a8834347a4d'
        },
        token: serializedTokens.hzn,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 129,
        lpSymbol: 'ALPA-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x837cd42282801e340f1f17aadf3166fee99fb07c'
        },
        token: serializedTokens.alpa,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 128,
        lpSymbol: 'PERL-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x044e9985c14a547d478b1e3d4a4e562e69c8f936'
        },
        token: serializedTokens.perl,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 127,
        lpSymbol: 'TLM-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x34e821e785A93261B697eBD2797988B3AA78ca33'
        },
        token: serializedTokens.tlm,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 125,
        lpSymbol: 'JGN-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x890479844484D67e34B99e1BBc1468231b254c08'
        },
        token: serializedTokens.jgn,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 124,
        lpSymbol: 'EPS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xf9045866e7b372def1eff3712ce55fac1a98daf0'
        },
        token: serializedTokens.eps,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 123,
        lpSymbol: 'ARPA-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xfb29fca952b478ddcb8a43f57176161e498225b1'
        },
        token: serializedTokens.arpa,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 122,
        lpSymbol: 'ITAM-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xCdC53345192D0e31eEAD03D7E9e008Ee659FAEbE'
        },
        token: serializedTokens.itam,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 121,
        lpSymbol: 'BONDLY-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x67581bfb4fc13bb73c71489b504e9b5354769063'
        },
        token: serializedTokens.bondly,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 120,
        lpSymbol: 'TKO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x496a8b716A3A3410B16e71E3c906968CE4488e52'
        },
        token: serializedTokens.tko,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 119,
        lpSymbol: 'APYS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xE5783Cc9dFb3E7e474B81B07369a008e80F1cEdb'
        },
        token: serializedTokens.apys,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 118,
        lpSymbol: 'HOO-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x3c9e55edbd937ae0ad8c084a1a8302110612a371'
        },
        token: serializedTokens.hoo,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 117,
        lpSymbol: 'ODDZ-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x61376b56ff33c618b115131712a4138f98810a6a'
        },
        token: serializedTokens.oddz,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 116,
        lpSymbol: 'EASY-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xbd1ec00b0d1cca9d5b28fbe0bb7d664238af2ffa'
        },
        token: serializedTokens.easy,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 115,
        lpSymbol: 'NRV-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x5a805994a2e30ac710e7376ccc0211285bd4dd92'
        },
        token: serializedTokens.nrv,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 114,
        lpSymbol: 'DEGO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x17F0b998B81cE75074a7CDAdAe6D63Da3cb23572'
        },
        token: serializedTokens.dego,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 113,
        lpSymbol: 'GUM-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x155645cDF8e4B28d5B7790b65d9f79efc222740C'
        },
        token: serializedTokens.gum,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 112,
        lpSymbol: 'pBTC-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xb5f6f7dad23132d40d778085d795bd0fd4b859cd'
        },
        token: serializedTokens.pbtc,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 111,
        lpSymbol: 'DFT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x8FbCbD7e30b1733965a8980bf7Ae2ca1c0C456cc'
        },
        token: serializedTokens.dft,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 110,
        lpSymbol: 'SWTH-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x8c5cFfad6cddb96Ee33DA685D0d50a37e030E115'
        },
        token: serializedTokens.swth,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 109,
        lpSymbol: 'LIEN-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xcd14855150335AAE984aa6D281E090c27035C692'
        },
        token: serializedTokens.lien,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 108,
        lpSymbol: 'ZIL-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xc746337b5f800a0e19ed4eb3bda03ff1401b8167'
        },
        token: serializedTokens.zil,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 107,
        lpSymbol: 'pCWS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x889e81d25bffba437b2a5d3e0e4fc58a0e2749c5'
        },
        token: serializedTokens.pcws,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 106,
        lpSymbol: 'bBADGER-BTCB LP',
        lpAddresses: {
            97: '',
            56: '0x10F461CEAC7A17F59e249954Db0784d42EfF5DB5'
        },
        token: serializedTokens.bbadger,
        quoteToken: serializedTokens.btcb
    },
    {
        pid: 104,
        lpSymbol: 'bDIGG-BTCB LP',
        lpAddresses: {
            97: '',
            56: '0xE1E33459505bB3763843a426F7Fd9933418184ae'
        },
        token: serializedTokens.bdigg,
        quoteToken: serializedTokens.btcb
    },
    {
        pid: 103,
        lpSymbol: 'LTO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x85644fcd00c401e1a0a0a10d2ae6bbe04a73e4ab'
        },
        token: serializedTokens.lto,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 102,
        lpSymbol: 'MIR-UST LP',
        lpAddresses: {
            97: '',
            56: '0xf64a269F0A06dA07D23F43c1Deb217101ee6Bee7'
        },
        token: serializedTokens.mir,
        quoteToken: serializedTokens.ust
    },
    {
        pid: 101,
        lpSymbol: 'TRADE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x2562f94E90dE6D9eb4fB6B3b8Eab56b15Aa4FC72'
        },
        token: serializedTokens.trade,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 100,
        lpSymbol: 'DUSK-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xB7918560273FD56e50E9c215CC0DFE8D764C36C5'
        },
        token: serializedTokens.dusk,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 99,
        lpSymbol: 'BIFI-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xd132D2C24F29EE8ABb64a66559d1b7aa627Bd7fD'
        },
        token: serializedTokens.bifi,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 98,
        lpSymbol: 'TXL-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xD20E0BcCa8B29409bf5726CB24DD779Fe337020e'
        },
        token: serializedTokens.txl,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 97,
        lpSymbol: 'COS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x7b1e440240B220244761C9D9A3B07fbA1995BD84'
        },
        token: serializedTokens.cos,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 96,
        lpSymbol: 'BUNNY-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x7Bb89460599Dbf32ee3Aa50798BBcEae2A5F7f6a'
        },
        token: serializedTokens.bunny,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 95,
        lpSymbol: 'ALICE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xe022baa3E5E87658f789c9132B10d7425Fd3a389'
        },
        token: serializedTokens.alice,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 94,
        lpSymbol: 'FOR-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xfEc200A5E3adDD4a7915a556DDe3F5850e644020'
        },
        token: serializedTokens.for,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 93,
        lpSymbol: 'BUX-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x0F556f4E47513d1a19Be456a9aF778d7e1A226B9'
        },
        token: serializedTokens.bux,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 92,
        lpSymbol: 'NULS-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xCA01F5D89d5b1d24ca5D6Ecc856D21e8a61DAFCc'
        },
        token: serializedTokens.nuls,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 91,
        lpSymbol: 'NULS-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xad7e515409e5a7d516411a85acc88c5e993f570a'
        },
        token: serializedTokens.nuls,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 90,
        lpSymbol: 'BELT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x83B92D283cd279fF2e057BD86a95BdEfffED6faa'
        },
        token: serializedTokens.belt,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 89,
        lpSymbol: 'RAMP-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xbF36959939982D0D34B37Fb3f3425da9676C13a3'
        },
        token: serializedTokens.ramp,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 88,
        lpSymbol: 'BFI-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x45a9e8d48bc560416008d122c9437927fed50e7d'
        },
        token: serializedTokens.bfi,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 87,
        lpSymbol: 'DEXE-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x69ab367bc1bea1d2c0fb4dbaec6b7197951da56c'
        },
        token: serializedTokens.dexe,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 86,
        lpSymbol: 'BEL-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xAB97952a2806D5c92b7046c7aB13a72A87e0097b'
        },
        token: serializedTokens.bel,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 85,
        lpSymbol: 'TPT-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x4db28767d1527ba545ca5bbda1c96a94ed6ff242'
        },
        token: serializedTokens.tpt,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 84,
        lpSymbol: 'WATCH-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xdc6c130299e53acd2cc2d291fa10552ca2198a6b'
        },
        token: serializedTokens.watch,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 83,
        lpSymbol: 'xMARK-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x292ca56ed5b3330a19037f835af4a9c0098ea6fa'
        },
        token: serializedTokens.xmark,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 82,
        lpSymbol: 'bMXX-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x4D5aA94Ce6BbB1BC4eb73207a5a5d4D052cFcD67'
        },
        token: serializedTokens.bmxx,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 81,
        lpSymbol: 'IOTX-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x36b8b28d37f93372188f2aa2507b68a5cd8b2664'
        },
        token: serializedTokens.iotx,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 80,
        lpSymbol: 'BOR-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x86e650350c40a5178a5d014ba37fe8556232b898'
        },
        token: serializedTokens.bor,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 79,
        lpSymbol: 'bOPEN-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x9d8b7e4a9d53654d82f12c83448d8f92732bc761'
        },
        token: serializedTokens.bopen,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 78,
        lpSymbol: 'SUSHI-ETH LP',
        lpAddresses: {
            97: '',
            56: '0x17580340f3daedae871a8c21d15911742ec79e0f'
        },
        token: serializedTokens.sushi,
        quoteToken: serializedTokens.eth
    },
    {
        pid: 77,
        lpSymbol: 'DODO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x9e642d174b14faea31d842dc83037c42b53236e6'
        },
        token: serializedTokens.dodo,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 75,
        lpSymbol: 'BRY-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x5E3CD27F36932Bc0314aC4e2510585798C34a2fC'
        },
        token: serializedTokens.bry,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 74,
        lpSymbol: 'ZEE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xb5ab3996808c7e489dcdc0f1af2ab212ae0059af'
        },
        token: serializedTokens.zee,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 73,
        lpSymbol: 'SWGb-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xc1800c29cf91954357cd0bf3f0accaada3d0109c'
        },
        token: serializedTokens.swgb,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 72,
        lpSymbol: 'COMP-ETH LP',
        lpAddresses: {
            97: '',
            56: '0x0392957571f28037607c14832d16f8b653edd472'
        },
        token: serializedTokens.comp,
        quoteToken: serializedTokens.eth
    },
    {
        pid: 71,
        lpSymbol: 'SFP-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xcbe2cf3bd012e9c1ade2ee4d41db3dac763e78f3'
        },
        token: serializedTokens.sfp,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 70,
        lpSymbol: 'BETH-ETH LP',
        lpAddresses: {
            97: '',
            56: '0x99d865ed50d2c32c1493896810fa386c1ce81d91'
        },
        token: serializedTokens.beth,
        quoteToken: serializedTokens.eth
    },
    {
        pid: 69,
        lpSymbol: 'LINA-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xeb325a8ea1c5abf40c7ceaf645596c1f943d0948'
        },
        token: serializedTokens.lina,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 68,
        lpSymbol: 'LIT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x60bB03D1010b99CEAdD0dd209b64bC8bd83da161'
        },
        token: serializedTokens.lit,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 67,
        lpSymbol: 'HGET-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x66b9e1eac8a81f3752f7f3a5e95de460688a17ee'
        },
        token: serializedTokens.hget,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 66,
        lpSymbol: 'BDO-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x74690f829fec83ea424ee1f1654041b2491a7be9'
        },
        token: serializedTokens.bdo,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 65,
        lpSymbol: 'EGLD-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x3ef4952c7a9afbe374ea02d1bf5ed5a0015b7716'
        },
        token: serializedTokens.egld,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 63,
        lpSymbol: 'UST-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xD1F12370b2ba1C79838337648F820a87eDF5e1e6'
        },
        token: serializedTokens.ust,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 62,
        lpSymbol: 'mAMZN-UST LP',
        lpAddresses: {
            97: '',
            56: '0xc92Dc34665c8a21f98E1E38474580b61b4f3e1b9'
        },
        token: serializedTokens.mamzn,
        quoteToken: serializedTokens.ust
    },
    {
        pid: 61,
        lpSymbol: 'mGOOGL-UST LP',
        lpAddresses: {
            97: '',
            56: '0x852A68181f789AE6d1Da3dF101740a59A071004f'
        },
        token: serializedTokens.mgoogl,
        quoteToken: serializedTokens.ust
    },
    {
        pid: 60,
        lpSymbol: 'mNFLX-UST LP',
        lpAddresses: {
            97: '',
            56: '0xF609ade3846981825776068a8eD7746470029D1f'
        },
        token: serializedTokens.mnflx,
        quoteToken: serializedTokens.ust
    },
    {
        pid: 59,
        lpSymbol: 'mTSLA-UST LP',
        lpAddresses: {
            97: '',
            56: '0xD5664D2d15cdffD597515f1c0D945c6c1D3Bf85B'
        },
        token: serializedTokens.mtsla,
        quoteToken: serializedTokens.ust
    },
    {
        pid: 58,
        lpSymbol: 'wSOTE-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xffb9e2d5ce4378f1a89b29bf53f80804cc078102'
        },
        token: serializedTokens.wsote,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 57,
        lpSymbol: 'FRONT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x36b7d2e5c7877392fb17f9219efad56f3d794700'
        },
        token: serializedTokens.front,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 56,
        lpSymbol: 'Helmet-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x6411310c07d8c48730172146fd6f31fa84034a8b'
        },
        token: serializedTokens.helmet,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 55,
        lpSymbol: 'BTCST-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x91589786D36fEe5B27A5539CfE638a5fc9834665'
        },
        token: serializedTokens.btcst,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 54,
        lpSymbol: 'LTC-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xbc765fd113c5bdb2ebc25f711191b56bb8690aec'
        },
        token: serializedTokens.ltc,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 53,
        lpSymbol: 'USDC-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x680dd100e4b394bda26a59dd5c119a391e747d18'
        },
        token: serializedTokens.usdc,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 52,
        lpSymbol: 'DAI-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0x3aB77e40340AB084c3e23Be8e5A6f7afed9D41DC'
        },
        token: serializedTokens.dai,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 51,
        lpSymbol: 'BSCX-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x20781bc3701c5309ac75291f5d09bdc23d7b7fa8'
        },
        token: serializedTokens.bscx,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 50,
        lpSymbol: 'TEN-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x01ecc44ddd2d104f44d2aa1a2bd9dfbc91ae8275'
        },
        token: serializedTokens.ten,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 49,
        lpSymbol: 'bALBT-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xbe14f3a89a4f7f279af9d99554cf12e8c29db921'
        },
        token: serializedTokens.balbt,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 46,
        lpSymbol: 'OG-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x64373608f2e93ea97ad4d8ca2cce6b2575db2f55'
        },
        token: serializedTokens.og,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 47,
        lpSymbol: 'ASR-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xd6b900d5308356317299dafe303e661271aa12f1'
        },
        token: serializedTokens.asr,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 48,
        lpSymbol: 'ATM-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xd5b3ebf1a85d32c73a82b40f75e1cd929caf4029'
        },
        token: serializedTokens.atm,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 45,
        lpSymbol: 'REEF-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x58B58cab6C5cF158f63A2390b817710826d116D0'
        },
        token: serializedTokens.reef,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 44,
        lpSymbol: 'Ditto-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x470bc451810b312bbb1256f96b0895d95ea659b1'
        },
        token: serializedTokens.ditto,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 43,
        lpSymbol: 'JUV-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x51a2ffa5b7de506f9a22549e48b33f6cf0d9030e'
        },
        token: serializedTokens.juv,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 42,
        lpSymbol: 'PSG-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x9c4f6a5050cf863e67a402e8b377973b4e3372c1'
        },
        token: serializedTokens.psg,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 39,
        lpSymbol: 'UNFI-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xbEA35584b9a88107102ABEf0BDeE2c4FaE5D8c31'
        },
        token: serializedTokens.unfi,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 41,
        lpSymbol: 'VAI-BUSD LP',
        lpAddresses: {
            97: '',
            56: '0xff17ff314925dff772b71abdff2782bc913b3575'
        },
        token: serializedTokens.vai,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 40,
        lpSymbol: 'BLK-BNB LP',
        lpAddresses: {
            97: '',
            56: '0xC743Dc05F03D25E1aF8eC5F8228f4BD25513c8d0'
        },
        token: serializedTokens.blink,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 37,
        lpSymbol: 'HARD-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x9f40e8a2fcaa267a0c374b6c661e0b372264cc3d'
        },
        token: serializedTokens.hard,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 2,
        lpSymbol: 'BUSD-BNB LP',
        lpAddresses: {
            97: '0x2f7682b64b88149ba3250aee32db712964de5fa9',
            56: '0x1b96b92314c44b159149f7e0303511fb2fc4774f'
        },
        token: serializedTokens.busd,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 3,
        lpSymbol: 'ADA-BNB LP',
        lpAddresses: {
            97: '0xcbe3282a562e23b8c61ed04bb72ffdbb9233b1ce',
            56: '0xba51d1ab95756ca4eab8737ecd450cd8f05384cf'
        },
        token: serializedTokens.ada,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 4,
        lpSymbol: 'BAND-BNB LP',
        lpAddresses: {
            97: '0xcbe3282a562e23b8c61ed04bb72ffdbb9233b1ce',
            56: '0xc639187ef82271d8f517de6feae4faf5b517533c'
        },
        token: serializedTokens.band,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 5,
        lpSymbol: 'DOT-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0xbcd62661a6b1ded703585d3af7d7649ef4dcdb5c'
        },
        token: serializedTokens.dot,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 6,
        lpSymbol: 'EOS-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x981d2ba1b298888408d342c39c2ab92e8991691e'
        },
        token: serializedTokens.eos,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 7,
        lpSymbol: 'LINK-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0xaebe45e3a03b734c68e5557ae04bfc76917b4686'
        },
        token: serializedTokens.link,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 11,
        lpSymbol: 'USDT-BUSD LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0xc15fa3E22c912A276550F3E5FE3b0Deb87B55aCd'
        },
        token: serializedTokens.usdt,
        quoteToken: serializedTokens.busd
    },
    {
        pid: 12,
        lpSymbol: 'TWT-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x610e7a287c27dfFcaC0F0a94f547Cc1B770cF483'
        },
        token: serializedTokens.twt,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 13,
        lpSymbol: 'XVS-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x41182c32F854dd97bA0e0B1816022e0aCB2fc0bb'
        },
        token: serializedTokens.xvs,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 14,
        lpSymbol: 'ETH-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x70D8929d04b60Af4fb9B58713eBcf18765aDE422'
        },
        token: serializedTokens.eth,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 15,
        lpSymbol: 'BTCB-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x7561EEe90e24F3b348E1087A005F78B4c8453524'
        },
        token: serializedTokens.btcb,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 16,
        lpSymbol: 'ALPHA-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x4e0f3385d932F7179DeE045369286FFa6B03d887'
        },
        token: serializedTokens.alpha,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 17,
        lpSymbol: 'USDT-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x20bcc3b8a0091ddac2d0bc30f68e6cbb97de59cd'
        },
        token: serializedTokens.usdt,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 18,
        lpSymbol: 'XRP-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0xc7b4b32a3be2cb6572a1c9959401f832ce47a6d2'
        },
        token: serializedTokens.xrp,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 19,
        lpSymbol: 'ATOM-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x2333c77fc0b2875c11409cdcd3c75d42d402e834'
        },
        token: serializedTokens.atom,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 20,
        lpSymbol: 'YFII-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x574a978c2d0d36d707a05e459466c7a1054f1210'
        },
        token: serializedTokens.yfii,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 21,
        lpSymbol: 'DAI-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x56c77d59e82f33c712f919d09fceddf49660a829'
        },
        token: serializedTokens.dai,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 22,
        lpSymbol: 'XTZ-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x5acac332f0f49c8badc7afd0134ad19d3db972e6'
        },
        token: serializedTokens.xtz,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 23,
        lpSymbol: 'BCH-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x54edd846db17f43b6e43296134ecd96284671e81'
        },
        token: serializedTokens.bch,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 24,
        lpSymbol: 'YFI-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x68Ff2ca47D27db5Ac0b5c46587645835dD51D3C1'
        },
        token: serializedTokens.yfi,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 25,
        lpSymbol: 'UNI-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x4269e7F43A63CEA1aD7707Be565a94a9189967E9'
        },
        token: serializedTokens.uni,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 26,
        lpSymbol: 'FIL-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x35fe9787f0ebf2a200bac413d3030cf62d312774'
        },
        token: serializedTokens.fil,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 27,
        lpSymbol: 'INJ-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x7a34bd64d18e44CfdE3ef4B81b87BAf3EB3315B6'
        },
        token: serializedTokens.inj,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 29,
        lpSymbol: 'USDC-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x30479874f9320a62bce3bc0e315c920e1d73e278'
        },
        token: serializedTokens.usdc,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 30,
        lpSymbol: 'SXP-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x752E713fB70E3FA1Ac08bCF34485F14A986956c4'
        },
        token: serializedTokens.sxp,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 32,
        lpSymbol: 'CTK-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x7793870484647a7278907498ec504879d6971EAb'
        },
        token: serializedTokens.ctk,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 34,
        lpSymbol: 'STAX-CAKE LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x7cd05f8b960ba071fdf69c750c0e5a57c8366500'
        },
        token: serializedTokens.stax,
        quoteToken: serializedTokens.cake,
        isCommunity: true
    },
    {
        pid: 35,
        lpSymbol: 'NAR-CAKE LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x745c4fd226e169d6da959283275a8e0ecdd7f312'
        },
        token: serializedTokens.nar,
        quoteToken: serializedTokens.cake,
        isCommunity: true
    },
    {
        pid: 36,
        lpSymbol: 'NYA-CAKE LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x2730bf486d658838464a4ef077880998d944252d'
        },
        token: serializedTokens.nya,
        quoteToken: serializedTokens.cake,
        isCommunity: true
    },
    {
        pid: 38,
        lpSymbol: 'bROOBEE-CAKE LP',
        lpAddresses: {
            97: '',
            56: '0x970858016C963b780E06f7DCfdEf8e809919BcE8'
        },
        token: serializedTokens.broobee,
        quoteToken: serializedTokens.cake,
        isCommunity: true
    },
    {
        pid: 8,
        lpSymbol: 'BAKE-BNB Bakery LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0xc2eed0f5a0dc28cfa895084bc0a9b8b8279ae492'
        },
        token: serializedTokens.bake,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 9,
        lpSymbol: 'BURGER-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0xd937FB9E6e47F3805981453BFB277a49FFfE04D7'
        },
        token: serializedTokens.burger,
        quoteToken: serializedTokens.wbnb
    },
    {
        pid: 10,
        lpSymbol: 'BAKE-BNB LP',
        lpAddresses: {
            97: '0xE66790075ad839978fEBa15D4d8bB2b415556a1D',
            56: '0x3Da30727ed0626b78C212e81B37B97A8eF8A25bB'
        },
        token: serializedTokens.bake,
        quoteToken: serializedTokens.wbnb
    }, 
];
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (farms);


/***/ }),

/***/ 7829:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* unused harmony export cakeBnbLpToken */
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(543);
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tokens__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9748);
/* harmony import */ var _farms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1115);



const cakeBnbLpToken = new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ChainId.MAINNET, _farms__WEBPACK_IMPORTED_MODULE_2__/* ["default"][1].lpAddresses */ .Z[1].lpAddresses[_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ChainId.MAINNET], 18, _farms__WEBPACK_IMPORTED_MODULE_2__/* ["default"][1].lpSymbol */ .Z[1].lpSymbol);
const ifos = [
    {
        id: 'froyo',
        address: '0xE0d6c91860a332068bdB59275b0AAC8769e26Ac4',
        isActive: true,
        name: 'Froyo Games (FROYO)',
        poolBasic: {
            saleAmount: '20,000,000 FROYO',
            raiseAmount: '$1,200,000',
            cakeToBurn: '$0',
            distributionRatio: 0.3
        },
        poolUnlimited: {
            saleAmount: '46,666,668 FROYO',
            raiseAmount: '$2,800,000',
            cakeToBurn: '$0',
            distributionRatio: 0.7
        },
        currency: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].cake */ .ZP.cake,
        token: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].froyo */ .ZP.froyo,
        releaseBlockNumber: 14297000,
        campaignId: '511170000',
        articleUrl: 'https://pancakeswap.finance/voting/proposal/QmRhc4oC73jk4zxU4YkP1kudKHeq6qamgYA1sWoh6XJnks',
        tokenOfferingPrice: 0.06,
        version: 3,
        telegramUrl: 'https://t.me/froyogames',
        twitterUrl: 'https://twitter.com/realfroyogames',
        description: `Froyo Games is a game publisher and decentralized GameFi platform, with a NFT Marketplace that integrates NFTs with their games.\n \n FROYO tokens can be used to buy NFTs and participate in Froyo games`
    },
    {
        id: 'dpt',
        address: '0x63914805A0864e9557eA3A5cC86cc1BA054ec64b',
        isActive: false,
        name: 'Diviner Protocol (DPT)',
        poolBasic: {
            saleAmount: '7,200,000 DPT',
            raiseAmount: '$180,000',
            cakeToBurn: '$0',
            distributionRatio: 0.3
        },
        poolUnlimited: {
            saleAmount: '16,800,000 DPT',
            raiseAmount: '$420,000',
            cakeToBurn: '$0',
            distributionRatio: 0.7
        },
        currency: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].cake */ .ZP.cake,
        token: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].dpt */ .ZP.dpt,
        releaseBlockNumber: 13491500,
        campaignId: '511160000',
        articleUrl: 'https://pancakeswap.finance/voting/proposal/QmQqpknHvuQwshVP259qFxfQsxiWbQ9SLMebohDeRwRzKg',
        tokenOfferingPrice: 0.025,
        version: 3
    },
    {
        id: 'santos',
        address: '0x69B5D2Ab0cf532a0E22Fc0dEB0c5135639892468',
        isActive: false,
        name: 'FC Santos Fan Token (SANTOS)',
        poolBasic: {
            saleAmount: '120,000 SANTOS',
            raiseAmount: '$300,000',
            cakeToBurn: '$0',
            distributionRatio: 0.3
        },
        poolUnlimited: {
            saleAmount: '280,000 SANTOS',
            raiseAmount: '$700,000',
            cakeToBurn: '$0',
            distributionRatio: 0.7
        },
        currency: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].cake */ .ZP.cake,
        token: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].santos */ .ZP.santos,
        releaseBlockNumber: 13097777,
        campaignId: '511150000',
        articleUrl: 'https://pancakeswap.finance/voting/proposal/QmUqRxjwZCWeZWEdgV2vHJ6hex7jMW7i247NKFas73xc8j',
        tokenOfferingPrice: 2.5,
        version: 2
    },
    {
        id: 'porto',
        address: '0xFDFf29dD0b4DD49Bf5E991A30b8593eaA34B4580',
        isActive: false,
        name: 'FC Porto Fan Token (PORTO)',
        poolBasic: {
            saleAmount: '250,000 PORTO',
            raiseAmount: '$500,000',
            cakeToBurn: '$0',
            distributionRatio: 0.5
        },
        poolUnlimited: {
            saleAmount: '250,000 PORTO',
            raiseAmount: '$500,000',
            cakeToBurn: '$0',
            distributionRatio: 0.5
        },
        currency: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].cake */ .ZP.cake,
        token: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].porto */ .ZP.porto,
        releaseBlockNumber: 12687500,
        campaignId: '511140000',
        articleUrl: 'https://pancakeswap.finance/voting/proposal/QmaakXYpydAwCgTuSPe3R2ZNraRtkCbK6iBRqBCCdzqKEG',
        tokenOfferingPrice: 2,
        version: 2
    },
    {
        id: 'dar',
        address: '0xb6eF1f36220397c434A6B15dc5EA616CFFDF58CE',
        isActive: false,
        name: 'Mines of Dalarnia (DAR)',
        poolBasic: {
            saleAmount: '6,000,000 DAR',
            raiseAmount: '$450,000',
            cakeToBurn: '$0',
            distributionRatio: 0.5
        },
        poolUnlimited: {
            saleAmount: '6,000,000 DAR',
            raiseAmount: '$450,000',
            cakeToBurn: '$0',
            distributionRatio: 0.5
        },
        currency: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].cake */ .ZP.cake,
        token: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].dar */ .ZP.dar,
        releaseBlockNumber: 12335455,
        campaignId: '511130000',
        articleUrl: 'https://pancakeswap.finance/voting/proposal/QmeJenHcbY45eQxLRebzvpNk5qSCrj2wM1t4EAMHotFoJL',
        tokenOfferingPrice: 0.075,
        version: 2
    },
    {
        id: 'dkt',
        address: '0x89ab9852155A794e371095d863aEAa2DF198067C',
        isActive: false,
        name: 'Duelist King (DKT)',
        poolBasic: {
            saleAmount: '75,000 DKT',
            raiseAmount: '$131,250',
            cakeToBurn: '$65,625',
            distributionRatio: 0.3
        },
        poolUnlimited: {
            saleAmount: '175,000 DKT',
            raiseAmount: '$306,250',
            cakeToBurn: '$153,125',
            distributionRatio: 0.7
        },
        currency: cakeBnbLpToken,
        token: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].dkt */ .ZP.dkt,
        releaseBlockNumber: 12130750,
        campaignId: '511120000',
        articleUrl: 'https://pancakeswap.finance/voting/proposal/QmTRWdW9a65fAkyJy1wrAJRU548fNMAZhRUDrSxzMDLmwk',
        tokenOfferingPrice: 1.75,
        version: 2
    },
    {
        id: 'kalmar',
        address: '0x1aFB32b76696CdF05593Ca3f3957AEFB23a220FB',
        isActive: false,
        name: 'Kalmar (KALM)',
        poolBasic: {
            saleAmount: '375,000 KALM',
            raiseAmount: '$750,000',
            cakeToBurn: '$375,000',
            distributionRatio: 0.3
        },
        poolUnlimited: {
            saleAmount: '875,000 KALM',
            raiseAmount: '$2,500,000',
            cakeToBurn: '$1,250,000',
            distributionRatio: 0.7
        },
        currency: cakeBnbLpToken,
        token: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].kalm */ .ZP.kalm,
        releaseBlockNumber: 7707736,
        campaignId: '511110000',
        articleUrl: 'https://pancakeswap.medium.com/kalmar-kalm-ifo-to-be-hosted-on-pancakeswap-4540059753e4',
        tokenOfferingPrice: 2,
        version: 2
    },
    {
        id: 'hotcross',
        address: '0xb664cdbe385656F8c54031c0CB12Cea55b584b63',
        isActive: false,
        name: 'Hot Cross (HOTCROSS)',
        poolBasic: {
            saleAmount: '15,000,000 HOTCROSS',
            raiseAmount: '$750,000',
            cakeToBurn: '$375,000',
            distributionRatio: 0.3
        },
        poolUnlimited: {
            saleAmount: '35,000,000 HOTCROSS',
            raiseAmount: '$1,750,000',
            cakeToBurn: '$875,000',
            distributionRatio: 0.7
        },
        currency: cakeBnbLpToken,
        token: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].hotcross */ .ZP.hotcross,
        releaseBlockNumber: 7477900,
        campaignId: '511100000',
        articleUrl: 'https://pancakeswap.medium.com/hot-cross-hotcross-ifo-to-be-hosted-on-pancakeswap-10e70f1f6841',
        tokenOfferingPrice: 0.05,
        version: 2
    },
    {
        id: 'horizon',
        address: '0x6137B571f7F1E44839ae10310a08be86D1A4D03B',
        isActive: false,
        name: 'Horizon Protocol (HZN)',
        poolBasic: {
            saleAmount: '3,000,000 HZN',
            raiseAmount: '$750,000',
            cakeToBurn: '$375,000',
            distributionRatio: 0.3
        },
        poolUnlimited: {
            saleAmount: '7,000,000 HZN',
            raiseAmount: '$1,750,000',
            cakeToBurn: '$875,000',
            distributionRatio: 0.7
        },
        currency: cakeBnbLpToken,
        token: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].hzn */ .ZP.hzn,
        releaseBlockNumber: 6581111,
        campaignId: '511090000',
        articleUrl: 'https://pancakeswap.medium.com/horizon-protocol-hzn-ifo-to-be-hosted-on-pancakeswap-51f79601c9d8',
        tokenOfferingPrice: 0.25,
        version: 2
    },
    {
        id: 'belt',
        address: '0xc9FBedC033a1c479a6AD451ffE463025E92a1d38',
        isActive: false,
        name: 'Belt (BELT)',
        poolUnlimited: {
            saleAmount: '150,000 BELT',
            raiseAmount: '$3,000,000',
            cakeToBurn: '$1,500,000',
            distributionRatio: 1
        },
        currency: cakeBnbLpToken,
        token: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].belt */ .ZP.belt,
        releaseBlockNumber: 5493919,
        campaignId: '511080000',
        articleUrl: 'https://pancakeswap.medium.com/belt-fi-belt-ifo-to-be-hosted-on-pancakeswap-353585117e32',
        tokenOfferingPrice: null,
        version: 1
    },
    {
        id: 'watch',
        address: '0x55344b55C71Ad8834C397E6e08dF5195cF84fe6d',
        isActive: false,
        name: 'Yieldwatch (WATCH)',
        poolUnlimited: {
            saleAmount: '8,000,000 WATCH',
            raiseAmount: '$800,000',
            cakeToBurn: '$400,000',
            distributionRatio: 1
        },
        currency: cakeBnbLpToken,
        token: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].watch */ .ZP.watch,
        releaseBlockNumber: 5294924,
        campaignId: '511070000',
        articleUrl: 'https://pancakeswap.medium.com/yieldwatch-watch-ifo-to-be-hosted-on-pancakeswap-d24301f17241',
        tokenOfferingPrice: null,
        version: 1
    },
    {
        id: 'berry',
        address: '0x5d028cE3435B2bB9AceBfaC599EEbA1ccD63d7dd',
        isActive: false,
        name: 'Berry (BRY)',
        poolUnlimited: {
            saleAmount: '2,000,000 BRY',
            raiseAmount: '$1,000,000',
            cakeToBurn: '$500,000',
            distributionRatio: 1
        },
        currency: cakeBnbLpToken,
        token: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].bry */ .ZP.bry,
        releaseBlockNumber: 4750968,
        campaignId: '511060000',
        articleUrl: 'https://pancakeswap.medium.com/berry-bry-ifo-to-be-hosted-on-pancakeswap-b4f9095e9cdb',
        tokenOfferingPrice: null,
        version: 1
    },
    {
        id: 'soteria',
        address: '0x9C21123D94b93361a29B2C2EFB3d5CD8B17e0A9e',
        isActive: false,
        name: 'Soteria (wSOTE)',
        poolUnlimited: {
            saleAmount: '1,500,000 wSOTE',
            raiseAmount: '$525,000',
            cakeToBurn: '$262,500',
            distributionRatio: 1
        },
        currency: cakeBnbLpToken,
        token: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].wsote */ .ZP.wsote,
        releaseBlockNumber: 4086064,
        campaignId: '511050000',
        articleUrl: 'https://pancakeswap.medium.com/soteria-sota-ifo-to-be-hosted-on-pancakeswap-64b727c272ae',
        tokenOfferingPrice: null,
        version: 1
    },
    {
        id: 'helmet',
        address: '0xa32509d760ee47Eb2Be96D338b5d69B5FBa4eFEB',
        isActive: false,
        name: 'Helmet.insure (Helmet)',
        poolUnlimited: {
            saleAmount: '10,000,000 Helmet',
            raiseAmount: '$1,000,000',
            cakeToBurn: '$500,000',
            distributionRatio: 1
        },
        currency: cakeBnbLpToken,
        token: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].helmet */ .ZP.helmet,
        releaseBlockNumber: 3771926,
        campaignId: '511040000',
        articleUrl: 'https://pancakeswap.medium.com/1-000-000-helmet-helmet-ifo-to-be-hosted-on-pancakeswap-3379a2a89a67',
        tokenOfferingPrice: null,
        version: 1
    },
    {
        id: 'tenet',
        address: '0xB922aA19A2603A07C6C9ED6c236618C9bac51f06',
        isActive: false,
        name: 'Tenet (TEN)',
        poolUnlimited: {
            saleAmount: '1,000,000 TEN',
            raiseAmount: '$1,000,000',
            cakeToBurn: '$500,000',
            distributionRatio: 1
        },
        currency: cakeBnbLpToken,
        token: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].ten */ .ZP.ten,
        releaseBlockNumber: 3483883,
        campaignId: '511030000',
        articleUrl: 'https://pancakeswap.medium.com/tenet-ten-ifo-to-be-hosted-on-pancakeswap-b7e1eb4cb272',
        tokenOfferingPrice: null,
        version: 1
    },
    {
        id: 'ditto',
        address: '0x570c9eB19553526Fb35895a531928E19C7D20788',
        isActive: false,
        name: 'Ditto (DITTO)',
        poolUnlimited: {
            saleAmount: '700,000 DITTO',
            raiseAmount: '$630,000',
            cakeToBurn: '$315,000',
            distributionRatio: 1
        },
        currency: cakeBnbLpToken,
        token: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].ditto */ .ZP.ditto,
        releaseBlockNumber: 3279767,
        campaignId: '511020000',
        articleUrl: 'https://pancakeswap.medium.com/ditto-money-ditto-ifo-to-be-hosted-on-pancakeswap-342da3059a66',
        tokenOfferingPrice: null,
        version: 1
    },
    {
        id: 'blink',
        address: '0x44a9Cc8463EC00937242b660BF65B10365d99baD',
        isActive: false,
        name: 'BLINk (BLK)',
        poolUnlimited: {
            saleAmount: '100,000,000 BLINK',
            raiseAmount: '$1,000,000',
            cakeToBurn: '$500,000',
            distributionRatio: 1
        },
        currency: cakeBnbLpToken,
        token: _tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].blink */ .ZP.blink,
        releaseBlockNumber: 3279767,
        campaignId: '511010000',
        articleUrl: 'https://medium.com/pancakeswap/1-000-000-ifo-blink-joins-pancakeswap-15841800bdd8',
        tokenOfferingPrice: null,
        version: 1
    }, 
];
/* unused harmony default export */ var __WEBPACK_DEFAULT_EXPORT__ = ((/* unused pure expression or super */ null && (ifos)));


/***/ }),

/***/ 3862:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "bR": () => (/* binding */ ROUTER_ADDRESS),
/* harmony export */   "lM": () => (/* binding */ BASES_TO_CHECK_TRADES_AGAINST),
/* harmony export */   "ck": () => (/* binding */ ADDITIONAL_BASES),
/* harmony export */   "IP": () => (/* binding */ CUSTOM_BASES),
/* harmony export */   "kx": () => (/* binding */ SUGGESTED_BASES),
/* harmony export */   "gv": () => (/* binding */ INITIAL_ALLOWED_SLIPPAGE),
/* harmony export */   "PY": () => (/* binding */ DEFAULT_DEADLINE_FROM_NOW),
/* harmony export */   "IS": () => (/* binding */ ONE_BIPS),
/* harmony export */   "PM": () => (/* binding */ BIPS_BASE),
/* harmony export */   "Bz": () => (/* binding */ ALLOWED_PRICE_IMPACT_LOW),
/* harmony export */   "p9": () => (/* binding */ ALLOWED_PRICE_IMPACT_MEDIUM),
/* harmony export */   "Uf": () => (/* binding */ ALLOWED_PRICE_IMPACT_HIGH),
/* harmony export */   "EV": () => (/* binding */ PRICE_IMPACT_WITHOUT_FEE_CONFIRM_MIN),
/* harmony export */   "lN": () => (/* binding */ BLOCKED_PRICE_IMPACT_NON_EXPERT),
/* harmony export */   "Uz": () => (/* binding */ MIN_BNB),
/* harmony export */   "Ru": () => (/* binding */ BETTER_TRADE_LESS_HOPS_THRESHOLD),
/* harmony export */   "fI": () => (/* binding */ ZERO_PERCENT),
/* harmony export */   "yC": () => (/* binding */ ONE_HUNDRED_PERCENT),
/* harmony export */   "mj": () => (/* binding */ BLOCKED_ADDRESSES),
/* harmony export */   "sR": () => (/* binding */ FAST_INTERVAL),
/* harmony export */   "KI": () => (/* binding */ SLOW_INTERVAL)
/* harmony export */ });
/* unused harmony exports BASES_TO_TRACK_LIQUIDITY_FOR, PINNED_PAIRS, NetworkContextName, BIG_INT_ZERO */
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(543);
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tokens__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9748);
/* harmony import */ var _farms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1115);
/* harmony import */ var _pools__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1080);
/* harmony import */ var _ifo__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(7829);


const ROUTER_ADDRESS = '0x10ED43C718714eb63d5aA57B78B54704E256024E';
// used to construct intermediary pairs for trading
const BASES_TO_CHECK_TRADES_AGAINST = {
    [_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ChainId.MAINNET]: [
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.wbnb */ .ke.wbnb,
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.cake */ .ke.cake,
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.busd */ .ke.busd,
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.usdt */ .ke.usdt,
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.btcb */ .ke.btcb,
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.ust */ .ke.ust,
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.eth */ .ke.eth,
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.usdc */ .ke.usdc, 
    ],
    [_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ChainId.TESTNET]: [
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .testnetTokens.wbnb */ .ux.wbnb,
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .testnetTokens.cake */ .ux.cake,
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .testnetTokens.busd */ .ux.busd
    ]
};
/**
 * Addittional bases for specific tokens
 * @example { [WBTC.address]: [renBTC], [renBTC.address]: [WBTC] }
 */ const ADDITIONAL_BASES = {
    [_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ChainId.MAINNET]: {}
};
/**
 * Some tokens can only be swapped via certain pairs, so we override the list of bases that are considered for these
 * tokens.
 * @example [AMPL.address]: [DAI, WETH[ChainId.MAINNET]]
 */ const CUSTOM_BASES = {
    [_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ChainId.MAINNET]: {}
};
// used for display in the default list when adding liquidity
const SUGGESTED_BASES = {
    [_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ChainId.MAINNET]: [
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.busd */ .ke.busd,
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.cake */ .ke.cake,
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.btcb */ .ke.btcb
    ],
    [_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ChainId.TESTNET]: [
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .testnetTokens.wbnb */ .ux.wbnb,
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .testnetTokens.cake */ .ux.cake,
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .testnetTokens.busd */ .ux.busd
    ]
};
// used to construct the list of all pairs we consider by default in the frontend
const BASES_TO_TRACK_LIQUIDITY_FOR = {
    [_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ChainId.MAINNET]: [
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.wbnb */ .ke.wbnb,
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.dai */ .ke.dai,
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.busd */ .ke.busd,
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.usdt */ .ke.usdt
    ],
    [_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ChainId.TESTNET]: [
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .testnetTokens.wbnb */ .ux.wbnb,
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .testnetTokens.cake */ .ux.cake,
        _tokens__WEBPACK_IMPORTED_MODULE_1__/* .testnetTokens.busd */ .ux.busd
    ]
};
const PINNED_PAIRS = {
    [_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ChainId.MAINNET]: [
        [
            _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.cake */ .ke.cake,
            _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.wbnb */ .ke.wbnb
        ],
        [
            _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.busd */ .ke.busd,
            _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.usdt */ .ke.usdt
        ],
        [
            _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.dai */ .ke.dai,
            _tokens__WEBPACK_IMPORTED_MODULE_1__/* .mainnetTokens.usdt */ .ke.usdt
        ], 
    ]
};
const NetworkContextName = 'NETWORK';
// default allowed slippage, in bips
const INITIAL_ALLOWED_SLIPPAGE = 50;
// 20 minutes, denominated in seconds
const DEFAULT_DEADLINE_FROM_NOW = 60 * 20;
const BIG_INT_ZERO = _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.BigInt(0);
// one basis point
const ONE_BIPS = new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Percent(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.BigInt(1), _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.BigInt(10000));
const BIPS_BASE = _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.BigInt(10000);
// used for warning states
const ALLOWED_PRICE_IMPACT_LOW = new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Percent(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.BigInt(100), BIPS_BASE) // 1%
;
const ALLOWED_PRICE_IMPACT_MEDIUM = new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Percent(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.BigInt(300), BIPS_BASE) // 3%
;
const ALLOWED_PRICE_IMPACT_HIGH = new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Percent(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.BigInt(500), BIPS_BASE) // 5%
;
// if the price slippage exceeds this number, force the user to type 'confirm' to execute
const PRICE_IMPACT_WITHOUT_FEE_CONFIRM_MIN = new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Percent(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.BigInt(1000), BIPS_BASE) // 10%
;
// for non expert mode disable swaps above this
const BLOCKED_PRICE_IMPACT_NON_EXPERT = new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Percent(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.BigInt(1500), BIPS_BASE) // 15%
;
// used to ensure the user doesn't send so much BNB so they end up with <.01
const MIN_BNB = _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.exponentiate(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.BigInt(10), _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.BigInt(16)) // .01 BNB
;
const BETTER_TRADE_LESS_HOPS_THRESHOLD = new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Percent(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.BigInt(50), _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.BigInt(10000));
const ZERO_PERCENT = new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Percent('0');
const ONE_HUNDRED_PERCENT = new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Percent('1');
// SDN OFAC addresses
const BLOCKED_ADDRESSES = [
    '0x7F367cC41522cE07553e823bf3be79A889DEbe1B',
    '0xd882cFc20F52f2599D84b8e8D58C7FB62cfE344b',
    '0x901bb9583b24D97e995513C6778dc6888AB6870e',
    '0xA7e5d5A720f06526557c513402f2e6B5fA20b008',
    '0x8576aCC5C05D6Ce88f4e49bf65BdF0C62F91353C', 
];



const FAST_INTERVAL = 10000;
const SLOW_INTERVAL = 60000;


/***/ }),

/***/ 428:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "US": () => (/* binding */ UNSUPPORTED_LIST_URLS),
/* harmony export */   "Lx": () => (/* binding */ DEFAULT_LIST_OF_LISTS),
/* harmony export */   "c8": () => (/* binding */ DEFAULT_ACTIVE_LIST_URLS)
/* harmony export */ });
const PANCAKE_EXTENDED = 'https://tokens.pancakeswap.finance/pancakeswap-extended.json';
const PANCAKE_TOP100 = 'https://tokens.pancakeswap.finance/pancakeswap-top-100.json';
const UNSUPPORTED_LIST_URLS = [];
// lower index == higher priority for token import
const DEFAULT_LIST_OF_LISTS = [
    PANCAKE_TOP100,
    PANCAKE_EXTENDED,
    ...UNSUPPORTED_LIST_URLS
];
// default lists to be 'active' aka searched across
const DEFAULT_ACTIVE_LIST_URLS = [];


/***/ }),

/***/ 1080:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* unused harmony export vaultPoolConfig */
/* harmony import */ var state_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5101);
/* harmony import */ var _tokens__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9748);
/* harmony import */ var _types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7971);



const serializedTokens = (0,_tokens__WEBPACK_IMPORTED_MODULE_1__/* .serializeTokens */ .uO)();
const vaultPoolConfig = {
    [state_types__WEBPACK_IMPORTED_MODULE_0__/* .VaultKey.CakeVault */ .om.CakeVault]: {
        name: 'Auto CAKE',
        description: 'Automatic restaking',
        autoCompoundFrequency: 5000,
        gasLimit: 380000,
        tokenImage: {
            primarySrc: `/images/tokens/${_tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].cake.address */ .ZP.cake.address}.svg`,
            secondarySrc: '/images/tokens/autorenew.svg'
        }
    },
    [state_types__WEBPACK_IMPORTED_MODULE_0__/* .VaultKey.IfoPool */ .om.IfoPool]: {
        name: 'IFO CAKE',
        description: 'Stake CAKE to participate in IFOs',
        autoCompoundFrequency: 1,
        gasLimit: 500000,
        tokenImage: {
            primarySrc: `/images/tokens/${_tokens__WEBPACK_IMPORTED_MODULE_1__/* ["default"].cake.address */ .ZP.cake.address}.svg`,
            secondarySrc: `/images/tokens/ifo-pool-icon.svg`
        }
    }
};
const pools = [
    {
        sousId: 0,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.cake,
        contractAddress: {
            97: '0xd3af5fe61dbaf8f73149bfcfa9fb653ff096029a',
            56: '0x73feaa1eE314F8c655E354234017bE2193C9E24E'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '10',
        sortOrder: 1,
        isFinished: false
    },
    {
        sousId: 258,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.fuse,
        contractAddress: {
            97: '',
            56: '0xeAd7b8fc5F2E5672FAe9dCf14E902287F35CB169'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.19',
        deployedBlockNumber: 14552132
    },
    {
        sousId: 257,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.froyo,
        contractAddress: {
            97: '',
            56: '0x1c9E3972fdBa29b40954Bb7594Da6611998F8830'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '2.893',
        deployedBlockNumber: 14436172
    },
    {
        sousId: 256,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.aog,
        contractAddress: {
            97: '',
            56: '0xa34832efe74133763A85060a64103542031B0A7E'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.6435',
        deployedBlockNumber: 14356854
    },
    {
        sousId: 255,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.apx,
        contractAddress: {
            97: '',
            56: '0x92c07c325cE7b340Da2591F5e9CbB1F5Bab73FCF'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '3.035',
        deployedBlockNumber: 14351799
    },
    {
        sousId: 254,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bcoin,
        contractAddress: {
            97: '',
            56: '0x25ca61796d786014ffe15e42ac11c7721d46e120'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.1493',
        deployedBlockNumber: 14301228
    },
    {
        sousId: 253,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bcoin,
        contractAddress: {
            97: '',
            56: '0xad8F6A9d58012DCa2303226B287E80e5fE27eff0'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.1493',
        deployedBlockNumber: 14271804
    },
    {
        sousId: 252,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.insur,
        contractAddress: {
            97: '',
            56: '0x1A777aE604CfBC265807A46Db2d228d4CC84E09D'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.3281',
        deployedBlockNumber: 14118300
    },
    {
        sousId: 251,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.gm,
        contractAddress: {
            97: '',
            56: '0x09e727c83a75fFdB729280639eDBf947dB76EeB7'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '7893'
    },
    {
        sousId: 250,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.woop,
        contractAddress: {
            97: '',
            56: '0x2718D56aE2b8F08B3076A409bBF729542233E451'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '1.226'
    },
    {
        sousId: 249,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.high,
        contractAddress: {
            97: '',
            56: '0x2461ea28907A2028b2bCa40040396F64B4141004'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.02679'
    },
    {
        sousId: 247,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.dpt,
        contractAddress: {
            97: '',
            56: '0x1c0C7F3B07a42efb4e15679a9ed7e70B2d7Cc157'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '4.62962963'
    },
    {
        sousId: 246,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.thg,
        contractAddress: {
            97: '',
            56: '0x56Bfb98EBEF4344dF2d88c6b80694Cba5EfC56c8'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.03761'
    },
    {
        sousId: 245,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.idia,
        contractAddress: {
            97: '',
            56: '0x07984aBb7489CD436d27875c07Eb532d4116795a'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.1262'
    },
    {
        sousId: 244,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.xcv,
        contractAddress: {
            97: '',
            56: '0xF1fA41f593547E406a203b681df18acCC3971A43'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.7413'
    },
    {
        sousId: 243,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.nabox,
        contractAddress: {
            97: '',
            56: '0x13A40BFab005D9284f8938FBb70Bf39982580e4D'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '1271'
    },
    {
        sousId: 242,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.santos,
        contractAddress: {
            97: '',
            56: '0x0914b2d9D4DD7043893DEF53ecFC0F1179F87d5c'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.0578'
    },
    {
        sousId: 241,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.quidd,
        contractAddress: {
            97: '',
            56: '0xd97ee2bfe79a4d4ab388553411c462fbb536a88c'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.07471'
    },
    {
        sousId: 240,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.zoo,
        contractAddress: {
            97: '',
            56: '0x2EfE8772EB97B74be742d578A654AB6C95bF18db'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.4516'
    },
    {
        sousId: 239,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.sfund,
        contractAddress: {
            97: '',
            56: '0x7F103689cabe17C2F70DA6faa298045d72a943b8'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.0311'
    },
    {
        sousId: 238,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.qi,
        contractAddress: {
            97: '',
            56: '0xbd52ef04DB1ad1c68A8FA24Fa71f2188978ba617'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '1.736'
    },
    {
        sousId: 237,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.kart,
        contractAddress: {
            97: '',
            56: '0x73bB10B89091f15e8FeD4d6e9EBa6415df6acb21'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.1897'
    },
    {
        sousId: 236,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.porto,
        contractAddress: {
            97: '',
            56: '0xdD52FAB121376432DBCBb47592742F9d86CF8952'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.0578'
    },
    {
        sousId: 235,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.dvi,
        contractAddress: {
            97: '',
            56: '0x2b8751B7141Efa7a9917f9C6fea2CEA071af5eE7'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.2516'
    },
    {
        sousId: 234,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.jgn,
        contractAddress: {
            97: '',
            56: '0xfDFb4DbE94916F9f55dBC2c14Ea8B3e386eCD9F9'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.5233'
    },
    {
        sousId: 232,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.xwg,
        contractAddress: {
            97: '',
            56: '0x79f5f7DDADeFa0A9e850DFFC4fBa77e5172Fe701'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '2.3379'
    },
    {
        sousId: 231,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.dar,
        contractAddress: {
            97: '',
            56: '0x9b861A078B2583373A7a3EEf815bE1A39125Ae08'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '2.3148'
    },
    {
        sousId: 230,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.fina,
        contractAddress: {
            97: '',
            56: '0xa35caA9509a2337E22C54C929146D5F7f6515794'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.06944'
    },
    {
        sousId: 229,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bmon,
        contractAddress: {
            97: '',
            56: '0x6e63B2B96c77532ea7ec2B3D3BFA9C8e1d383f3C'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '2.0254'
    },
    {
        sousId: 228,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.dkt,
        contractAddress: {
            97: '',
            56: '0xFef4B7a0194159d89717Efa592384d42B28D3926'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.09838'
    },
    {
        sousId: 227,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.ramp,
        contractAddress: {
            97: '',
            56: '0x2D26e4b9a5F19eD5BB7AF221DC02432D31DEB4dA'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '1.2152'
    },
    {
        sousId: 226,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.lazio,
        contractAddress: {
            97: '',
            56: '0xd008416c2c9cf23843bd179aa3cefedb4c8d1607'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.0694'
    },
    {
        sousId: 225,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.sps,
        contractAddress: {
            97: '',
            56: '0xd9b63bb6c62fe2e9a641699a91e680994b8b0081'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.40046'
    },
    {
        sousId: 224,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.mcb,
        contractAddress: {
            97: '',
            56: '0xCc2D359c3a99d9cfe8e6F31230142efF1C828e6D'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.00706'
    },
    {
        sousId: 223,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.rpg,
        contractAddress: {
            97: '',
            56: '0x65C0940C50A3C98AEEc95a115Ae62E9804588713'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.01863'
    },
    {
        sousId: 222,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.beta,
        contractAddress: {
            97: '',
            56: '0x6f660c58723922c6f866a058199ff4881019b4b5'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '1.7361'
    },
    {
        sousId: 233,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.eternal,
        contractAddress: {
            97: '',
            56: '0xc28c400F2B675b25894FA632205ddec71E432288'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.002893'
    },
    {
        sousId: 221,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.nft,
        contractAddress: {
            97: '',
            56: '0x8d018823d13c56d62ffb795151a9e629c21e047b'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '173727'
    },
    {
        sousId: 220,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.chess,
        contractAddress: {
            97: '',
            56: '0x4D1Ec426d0d7fb6bF344Dd372d0502EDD71c8d88'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.0861'
    },
    {
        sousId: 219,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.tlos,
        contractAddress: {
            97: '',
            56: '0xCB41a72067c227D6Ed7bc7CFAcd13eCe47Dfe5E9'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.3616'
    },
    {
        sousId: 218,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.stephero,
        contractAddress: {
            97: '',
            56: '0xcecba456fefe5b18d43df23419e7ab755b436655'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.1851'
    },
    {
        sousId: 217,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bp,
        contractAddress: {
            97: '',
            56: '0x8ed7acf12b08274d5cdaf03d43d0e54bcbdd487e'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.217'
    },
    {
        sousId: 216,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.cvp,
        contractAddress: {
            97: '',
            56: '0xC4b15117BC0be030c20754FF36197641477af5d1'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.14'
    },
    {
        sousId: 215,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.qbt,
        contractAddress: {
            97: '',
            56: '0xB72dEf58D0832f747d6B7197471Fe20AeA7EB463'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '1.412'
    },
    {
        sousId: 214,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.naos,
        contractAddress: {
            97: '',
            56: '0xb38b78529bCc895dA16CE2978D6cD6C56e8CfFC3'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.1736'
    },
    {
        sousId: 213,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.pha,
        contractAddress: {
            97: '',
            56: '0x2E101b5F7f910F2609e5AcE5f43bD274b1DE09AA'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.28'
    },
    {
        sousId: 212,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bel,
        contractAddress: {
            97: '',
            56: '0x52733Ad7b4D09BF613b0389045e33E2F287afa04'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.101'
    },
    {
        sousId: 211,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.ramp,
        contractAddress: {
            97: '',
            56: '0x401b9b97bdbc3197c1adfab9652dc78040bd1e13'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.9837'
    },
    {
        sousId: 210,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.pots,
        contractAddress: {
            97: '',
            56: '0xBeDb490970204cb3CC7B0fea94463BeD67d5364D'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.0868'
    },
    {
        sousId: 209,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.tusd,
        contractAddress: {
            97: '',
            56: '0xb6e510ae2da1ab4e350f837c70823ab75091780e'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.5787'
    },
    {
        sousId: 208,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.revv,
        contractAddress: {
            97: '',
            56: '0x8aa5b2c67852ed5334c8a7f0b5eb0ef975106793'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '1.331'
    },
    {
        sousId: 207,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.btt,
        contractAddress: {
            97: '',
            56: '0x3b804460c3c62f0f565af593984159f13b1ac976'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '263'
    },
    {
        sousId: 206,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.skill,
        contractAddress: {
            97: '',
            56: '0x455f4d4cc4d6ca15441a93c631e82aaf338ad843'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.00289'
    },
    {
        sousId: 205,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.win,
        contractAddress: {
            97: '',
            56: '0xde4aef42bb27a2cb45c746acde4e4d8ab711d27c'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '1815.39'
    },
    {
        sousId: 204,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.if,
        contractAddress: {
            97: '',
            56: '0x57d3524888ded4085d9124a422f13b27c8a43de7'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.02835'
    },
    {
        sousId: 203,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.sps,
        contractAddress: {
            97: '',
            56: '0xb56299d8fbf46c509014b103a164ad1fc65ea222'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '12.86'
    },
    {
        sousId: 202,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.c98,
        contractAddress: {
            97: '',
            56: '0x5e49531BA07bE577323e55666D46C6217164119E'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '1.7361'
    },
    {
        sousId: 201,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.axs,
        contractAddress: {
            97: '',
            56: '0xBB472601B3CB32723d0755094BA80B73F67f2AF3'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.00744'
    },
    {
        sousId: 200,
        stakingToken: serializedTokens.axs,
        earningToken: serializedTokens.cake,
        contractAddress: {
            97: '',
            56: '0x583A36816F3b8401C4fdf682203E0caDA6997740'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.00992'
    },
    {
        sousId: 199,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.pmon,
        contractAddress: {
            97: '',
            56: '0x28050e8f024e05f9ddbef5f60dd49f536dba0cf0'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.02696'
    },
    {
        sousId: 198,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.trx,
        contractAddress: {
            97: '',
            56: '0xb2b62f88ab82ed0bb4ab4da60d9dc9acf9e816e5'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '9.6643'
    },
    {
        sousId: 197,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.chess,
        contractAddress: {
            97: '',
            56: '0xd1812e7e28c39e78727592de030fc0e7c366d61a'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.4286'
    },
    {
        sousId: 196,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.titan,
        contractAddress: {
            97: '',
            56: '0x97058cf9b36c9ef1622485cef22e72d6fea32a36'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.08912'
    },
    {
        sousId: 195,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.harmony,
        contractAddress: {
            97: '',
            56: '0xe595456846155e23b24cc9cbee910ee97027db6d'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '3.0092'
    },
    {
        sousId: 194,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.mask,
        contractAddress: {
            97: '',
            56: '0xae611c6d4d3ca2cee44cd34eb7aac29d5a387fcf'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.06423'
    },
    {
        sousId: 193,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.dvi,
        contractAddress: {
            97: '',
            56: '0x135827eaf9746573c0b013f18ee12f138b9b0384'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.7233'
    },
    {
        sousId: 192,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.gum,
        contractAddress: {
            97: '',
            56: '0x09b8a5f51c9e245402057851ada274174fa00e2a'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.67129'
    },
    {
        sousId: 191,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.adx,
        contractAddress: {
            97: '',
            56: '0x53a2d1db049b5271c6b6db020dba0e8a7c4eb90d'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.7523'
    },
    {
        sousId: 190,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.suter,
        contractAddress: {
            97: '',
            56: '0x4da8da81647ee0aa7350e9959f3e4771eb753da0'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '69.9074'
    },
    {
        sousId: 189,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bscpad,
        contractAddress: {
            97: '',
            56: '0x0446b8f8474c590d2249a4acdd6eedbc2e004bca'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.3877'
    },
    {
        sousId: 188,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.rabbit,
        contractAddress: {
            97: '',
            56: '0x391240A007Bfd8A59bA74978D691219a76c64c5C'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '3.993'
    },
    {
        sousId: 187,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.waultx,
        contractAddress: {
            97: '',
            56: '0x017DEa5C58c2Bcf57FA73945073dF7AD4052a71C'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '7.9108'
    },
    {
        sousId: 186,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.wex,
        contractAddress: {
            97: '',
            56: '0x6Bd94783caCef3fb7eAa9284f1631c464479829f'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '11.574'
    },
    {
        sousId: 185,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.form,
        contractAddress: {
            97: '',
            56: '0x7c71723fB1F9Cfb250B702cfc4eBd5D9Ab2E83d9'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '1.4467'
    },
    {
        sousId: 184,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.orbs,
        contractAddress: {
            97: '',
            56: '0x9C8813d7D0A61d30610a7A5FdEF9109e196a3D77'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '3.8946'
    },
    {
        sousId: 183,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.$dg,
        contractAddress: {
            97: '',
            56: '0xa07a91da6d10173f33c294803684bceede325957'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.00135'
    },
    {
        sousId: 182,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.woo,
        contractAddress: {
            97: '',
            56: '0x88c321d444c88acf3e747dc90f20421b97648903'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.3854'
    },
    {
        sousId: 181,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.woo,
        contractAddress: {
            97: '',
            56: '0x3c7234c496d76133b48bd6a342e7aea4f8d87fc8'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.0003854'
    },
    {
        sousId: 180,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.htb,
        contractAddress: {
            97: '',
            56: '0x64473c33c360f315cab38674f1633505d1d8dcb2'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '5.2083'
    },
    {
        sousId: 179,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.jgn,
        contractAddress: {
            97: '',
            56: '0x5cc7a19a50be2a6b2540ebcd55bd728e732e59c3'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.1001'
    },
    {
        sousId: 178,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.dft,
        contractAddress: {
            97: '',
            56: '0x2666e2494e742301ffc8026e476acc1710a775ed'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '1.7361'
    },
    {
        sousId: 177,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.hai,
        contractAddress: {
            97: '',
            56: '0x6ac2213F09A404c86AFf506Aa51B6a5BF1F6e24E'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '1.3078'
    },
    {
        sousId: 176,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.o3,
        contractAddress: {
            97: '',
            56: '0x35BD47263f7E57368Df76339903C53bAa99076e1'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.107638'
    },
    {
        sousId: 175,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.ampl,
        contractAddress: {
            97: '',
            56: '0x62dEc3A560D2e8A84D30752bA454f97b26757877'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.398'
    },
    {
        sousId: 174,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.oddz,
        contractAddress: {
            97: '',
            56: '0x44d1f81e80e43e935d66d65874354ef91e5e49f6'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.4843'
    },
    {
        sousId: 173,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bondly,
        contractAddress: {
            97: '',
            56: '0x4ea43fce546975aae120c9eeceb172500be4a02b'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.6741'
    },
    {
        sousId: 172,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.marsh,
        contractAddress: {
            97: '',
            56: '0x567fd708e788e51b68666b9310ee9df163d60fae'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.1108'
    },
    {
        sousId: 171,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.mbox,
        contractAddress: {
            97: '',
            56: '0x36f9452083fc9bc469a31e7966b873f402292433'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.4803'
    },
    {
        sousId: 170,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.ata,
        contractAddress: {
            97: '',
            56: '0xc612680457751d0d01b5d901ad08132a3b001900'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '1.7361'
    },
    {
        sousId: 169,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.mx,
        contractAddress: {
            97: '',
            56: '0x336bcd59f2b6eb7221a99f7a50fd03c6bf9a306b'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.27777'
    },
    {
        sousId: 168,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bcfx,
        contractAddress: {
            97: '',
            56: '0x2b3974dda76b2d408b7d680a27fbb0393e3cf0e1'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.6944'
    },
    {
        sousId: 167,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.qkc,
        contractAddress: {
            97: '',
            56: '0xfa67f97eeee6de55d179ecabbfe701f27d9a1ed9'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '15.9143'
    },
    {
        sousId: 166,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.ktn,
        contractAddress: {
            97: '',
            56: '0x48852322a185dc5fc733ff8c8d7c6dcbd2b3b2a2'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.01215'
    },
    {
        sousId: 165,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.mtrg,
        contractAddress: {
            97: '',
            56: '0xf4d0f71698f58f221911515781b05e808a8635cb'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.05613'
    },
    {
        sousId: 164,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.tusd,
        contractAddress: {
            97: '',
            56: '0x9dceb1d92f7e0361d0766f3d98482424df857654'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.5787'
    },
    {
        sousId: 163,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.vrt,
        contractAddress: {
            97: '',
            56: '0xb77f1425ec3a7c78b1a1e892f72332c8b5e8ffcb'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '40.5092'
    },
    {
        sousId: 162,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.kalm,
        contractAddress: {
            97: '',
            56: '0xb9ff4da0954b300542e722097671ead8cf337c17'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.0868'
    },
    {
        sousId: 161,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.ez,
        contractAddress: {
            97: '',
            56: '0xb19395702460261e51edf7a7b130109c64f13af9'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.01736'
    },
    {
        sousId: 160,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.kalm,
        contractAddress: {
            97: '',
            56: '0x6e113ecb9ff2d271140f124c2cc5b5e4b5700c9f'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        isFinished: true,
        tokenPerBlock: '0.00868'
    },
    {
        sousId: 159,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.popen,
        contractAddress: {
            97: '',
            56: '0x7baf1763ce5d0da8c9d85927f08a8be9c481ce50'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.19097'
    },
    {
        sousId: 158,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.lien,
        contractAddress: {
            97: '',
            56: '0x2b8d6c9c62bfc1bed84724165d3000e61d332cab'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.023148'
    },
    {
        sousId: 157,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.well,
        contractAddress: {
            97: '',
            56: '0x8a06ff2748edcba3fb4e44a6bfda4e46769e557b'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.2025',
        sortOrder: 999
    },
    {
        sousId: 156,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.deri,
        contractAddress: {
            97: '',
            56: '0x3eba95f5493349bbe0cad33eaae05dc6a7e26b90'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '1.6087',
        sortOrder: 999
    },
    {
        sousId: 155,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.chr,
        contractAddress: {
            97: '',
            56: '0x593edbd14a5b7eec828336accca9c16cc12f04be'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '1.655',
        sortOrder: 999
    },
    {
        sousId: 154,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.cyc,
        contractAddress: {
            97: '',
            56: '0xD714738837944C3c592477249E8edB724A76e068'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.00015914',
        sortOrder: 999
    },
    {
        sousId: 153,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.xend,
        contractAddress: {
            97: '',
            56: '0x8ea9f2482b2f7b12744a831f81f8d08714adc093'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.31828',
        sortOrder: 999,
        isFinished: false
    },
    {
        sousId: 152,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.hget,
        contractAddress: {
            97: '',
            56: '0x8e8125f871eb5ba9d55361365f5391ab437f9acc'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.03553',
        sortOrder: 999,
        isFinished: false
    },
    {
        sousId: 151,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.hotcross,
        contractAddress: {
            97: '',
            56: '0x0e09205e993f78cd5b3a5df355ae98ee7d0b5834'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '3.4722',
        isFinished: false
    },
    {
        sousId: 150,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.rfox,
        contractAddress: {
            97: '',
            56: '0xf9f00d41b1f4b3c531ff750a9b986c1a530f33d9'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '1.7361',
        isFinished: false
    },
    {
        sousId: 149,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.wmass,
        contractAddress: {
            97: '',
            56: '0x4Af531EcD50167a9402Ce921ee6436dd4cFC04FD'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.24942',
        isFinished: false
    },
    {
        sousId: 148,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.ubxt,
        contractAddress: {
            97: '',
            56: '0x9b4bac2d8f69853aa29cb45478c77fc54532ac22'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '2.4074',
        isFinished: false
    },
    {
        sousId: 147,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.btr,
        contractAddress: {
            97: '',
            56: '0x20ee70a07ae1b475cb150dec27930d97915726ea'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '2.8935',
        isFinished: false
    },
    {
        sousId: 146,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.τdoge,
        contractAddress: {
            97: '',
            56: '0x017556dffb8c6a52fd7f4788adf6fb339309c81b'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.868',
        isFinished: false
    },
    {
        sousId: 145,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.pmon,
        contractAddress: {
            97: '',
            56: '0xdaa711ecf2ac0bff5c82fceeae96d0008791cc49'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.01261',
        isFinished: false
    },
    {
        sousId: 144,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.one,
        contractAddress: {
            97: '',
            56: '0x74af842ecd0b6588add455a47aa21ed9ba794108'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '57.87',
        isFinished: false
    },
    {
        sousId: 143,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.fine,
        contractAddress: {
            97: '',
            56: '0x42d41749d6e9a1c5b47e27f690d4531f181b2159'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.1342',
        isFinished: false
    },
    {
        sousId: 142,
        stakingToken: serializedTokens.doge,
        earningToken: serializedTokens.cake,
        contractAddress: {
            97: '',
            56: '0xbebd44824631b55991fa5f2bf5c7a4ec96ff805b'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.01388',
        isFinished: false
    },
    {
        sousId: 141,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bmxx,
        contractAddress: {
            97: '',
            56: '0x55131f330c886e3f0cae389cedb23766ac9aa3ed'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.01331',
        isFinished: false
    },
    {
        sousId: 140,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.oin,
        contractAddress: {
            97: '',
            56: '0x01453a74a94687fa3f99b80762435855a13664f4'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.3472',
        isFinished: false
    },
    {
        sousId: 139,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.hyfi,
        contractAddress: {
            97: '',
            56: '0x0032ceb978fe5fc8a5d5d6f5adfc005e76397e29'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '2.8935',
        isFinished: false
    },
    {
        sousId: 138,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.kun,
        contractAddress: {
            97: '',
            56: '0x439b46d467402cebc1a2fa05038b5b696b1f4417'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.23148',
        isFinished: false
    },
    {
        sousId: 137,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.math,
        contractAddress: {
            97: '',
            56: '0x377ae5f933aa4cfa41fa03e2cae8a2befccf53b2'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.15798',
        isFinished: false
    },
    {
        sousId: 136,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.ust,
        contractAddress: {
            97: '',
            56: '0xce3ebac3f549ebf1a174a6ac3b390c179422b5f6'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.17361',
        isFinished: false
    },
    {
        sousId: 135,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.win,
        contractAddress: {
            97: '',
            56: '0xd26dec254c699935c286cd90e9841dcabf1af72d'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '462.96',
        isFinished: false
    },
    {
        sousId: 134,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.trx,
        contractAddress: {
            97: '',
            56: '0x93e2867d9b74341c2d19101b7fbb81d6063cca4d'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '4.0509',
        isFinished: false
    },
    {
        sousId: 133,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.btt,
        contractAddress: {
            97: '',
            56: '0x3b644e44033cff70bd6b771904225f3dd69dfb6d'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '79.86',
        isFinished: false
    },
    {
        sousId: 132,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.lmt,
        contractAddress: {
            97: '',
            56: '0x0a687d7b951348d681f7ed5eea84c0ba7b9566dc'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.66145',
        isFinished: false
    },
    {
        sousId: 131,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.pnt,
        contractAddress: {
            97: '',
            56: '0x417df1c0e6a498eb1f2247f99032a01d4fafe922'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.09548',
        isFinished: false
    },
    {
        sousId: 130,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.xmark,
        contractAddress: {
            97: '',
            56: '0xdc8943d806f9dd64312d155284abf780455fd345'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.0298',
        isFinished: false
    },
    {
        sousId: 129,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.ramp,
        contractAddress: {
            97: '',
            56: '0xa90a894e5bc20ab2be46c7e033a38f8b8eaa771a'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.4861',
        isFinished: true
    },
    {
        sousId: 128,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.hakka,
        contractAddress: {
            97: '',
            56: '0x34ac807e34e534fe426da1e11f816422774aae1c'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '3.4722',
        isFinished: false
    },
    {
        sousId: 127,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.pbtc,
        contractAddress: {
            97: '',
            56: '0x31fa2f516b77c4273168b284ac6d9def5aa6dafb'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.0000031',
        isFinished: false
    },
    {
        sousId: 126,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.lien,
        contractAddress: {
            97: '',
            56: '0x7112f8988f075c7784666ab071927ae4109a8076'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.003472',
        isFinished: false
    },
    {
        sousId: 125,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bbadger,
        contractAddress: {
            97: '',
            56: '0x126dfbcef85c5bf335f8be99ca4006037f417892'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.00265278',
        isFinished: false
    },
    {
        sousId: 124,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bdigg,
        contractAddress: {
            97: '',
            56: '0x4f0ad2332b1f9983e8f63cbee617523bb7de5031'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.00000403',
        isFinished: false
    },
    {
        sousId: 123,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bondly,
        contractAddress: {
            97: '',
            56: '0x9483ca44324de06802576866b9d296f7614f45ac'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.4629',
        isFinished: false
    },
    {
        sousId: 122,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.xed,
        contractAddress: {
            97: '',
            56: '0x72ceec6e2a142678e703ab0710de78bc819f4ce0'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.3472',
        isFinished: false
    },
    {
        sousId: 121,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.cgg,
        contractAddress: {
            97: '',
            56: '0x1c6ed21d3313822ae73ed0d94811ffbbe543f341'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.10918',
        isFinished: false
    },
    {
        sousId: 120,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.perl,
        contractAddress: {
            97: '',
            56: '0x1ac0d0333640f57327c83053c581340ebc829e30'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '1.1574',
        isFinished: false
    },
    {
        sousId: 119,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.arpa,
        contractAddress: {
            97: '',
            56: '0xc707e5589aeb1dc117b0bb5a3622362f1812d4fc'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.949',
        isFinished: false
    },
    {
        sousId: 118,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.oddz,
        contractAddress: {
            97: '',
            56: '0x22106cdcf9787969e1672d8e6a9c03a889cda9c5'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.11284',
        isFinished: false
    },
    {
        sousId: 117,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.dexe,
        contractAddress: {
            97: '',
            56: '0x999b86e8bba3d4f05afb8155963999db70afa97f'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.009837',
        isFinished: true
    },
    {
        sousId: 116,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.dfd,
        contractAddress: {
            97: '',
            56: '0xAF3EfE5fCEeBc603Eada6A2b0172be11f7405102'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.46296',
        isFinished: false
    },
    {
        sousId: 115,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.alpaca,
        contractAddress: {
            97: '',
            56: '0xf73fdeb26a8c7a4abf3809d3db11a06ba5c13d0e'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.22743',
        isFinished: false
    },
    {
        sousId: 114,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.τbtc,
        contractAddress: {
            97: '',
            56: '0xaac7171afc93f4b75e1268d208040b152ac65e32'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.00000608',
        isFinished: false
    },
    {
        sousId: 113,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.τbtc,
        contractAddress: {
            97: '',
            56: '0x2c6017269b4324d016ca5d8e3267368652c18905'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.00000608',
        isFinished: true
    },
    {
        sousId: 112,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.swingby,
        contractAddress: {
            97: '',
            56: '0x675434c68f2672c983e36cf10ed13a4014720b79'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.21527'
    },
    {
        sousId: 111,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.xed,
        contractAddress: {
            97: '',
            56: '0x05d6c2d1d687eacfb5e6440d5a3511e91f2201a8'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.3472',
        isFinished: true,
        enableEmergencyWithdraw: true
    },
    {
        sousId: 110,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.hakka,
        contractAddress: {
            97: '',
            56: '0xd623a32da4a632ce01766c317d07cb2cad56949b'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '3.4722',
        isFinished: true,
        enableEmergencyWithdraw: true
    },
    {
        sousId: 109,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.cgg,
        contractAddress: {
            97: '',
            56: '0xdf75f38dbc98f9f26377414e567abcb8d57cca33'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.10918',
        isFinished: true,
        enableEmergencyWithdraw: true
    },
    {
        sousId: 108,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.mix,
        contractAddress: {
            97: '',
            56: '0xce64a930884b2c68cd93fc1c7c7cdc221d427692'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.3721'
    },
    {
        sousId: 107,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.suter,
        contractAddress: {
            97: '',
            56: '0xc1E70edd0141c454b834Deac7ddDeA413424aEf9'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '15.3356'
    },
    {
        sousId: 106,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.broobee,
        contractAddress: {
            97: '',
            56: '0x189d8228CdfDc404Bd9e5bD65ff958cb5fd8855c'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '14.4675'
    },
    {
        sousId: 105,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.hzn,
        contractAddress: {
            97: '',
            56: '0x0196c582216e2463f052E2B07Ef8667Bec9Fb17a'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.6944'
    },
    {
        sousId: 104,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.alpa,
        contractAddress: {
            97: '',
            56: '0x8f84106286c9c8A42bc3555C835E6e2090684ab7'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.23495'
    },
    {
        sousId: 103,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.perl,
        contractAddress: {
            97: '',
            56: '0xa8d32b31ECB5142f067548Bf0424389eE98FaF26'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '1.1574',
        isFinished: true,
        enableEmergencyWithdraw: true
    },
    {
        sousId: 102,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.tlm,
        contractAddress: {
            97: '',
            56: '0xC59aa49aE508050c2dF653E77bE13822fFf02E9A'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '17.361'
    },
    {
        sousId: 101,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.jgn,
        contractAddress: {
            97: '',
            56: '0x14AeA62384789EDA98f444cCb970F6730877d3F9'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.08796'
    },
    {
        sousId: 100,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.eps,
        contractAddress: {
            97: '',
            56: '0xebb87dF24D65977cbe62538E4B3cFBD5d0308642'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.07716'
    },
    {
        sousId: 99,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.arpa,
        contractAddress: {
            97: '',
            56: '0x40918EF8efFF4aA061656013a81E0e5A8A702eA7'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.949',
        isFinished: true,
        enableEmergencyWithdraw: true
    },
    {
        sousId: 98,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.itam,
        contractAddress: {
            97: '',
            56: '0x44eC1B26035865D9A7C130fD872670CD7Ebac2bC'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '3.096'
    },
    {
        sousId: 97,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bondly,
        contractAddress: {
            97: '',
            56: '0x1329ad151dE6C441184E32E108401126AE850937'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.4629',
        isFinished: true,
        enableEmergencyWithdraw: true
    },
    {
        sousId: 96,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.tko,
        contractAddress: {
            97: '',
            56: '0x9bbDc92474a7e7321B78dcDA5EF35f4981438760'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '1.1574'
    },
    {
        sousId: 95,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.apys,
        contractAddress: {
            97: '',
            56: '0x46530d79b238f809e80313e73715b160c66677aF'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.09953'
    },
    {
        sousId: 94,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.hoo,
        contractAddress: {
            97: '',
            56: '0x47fD853D5baD391899172892F91FAa6d0cd8A2Aa'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.489'
    },
    {
        sousId: 93,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.oddz,
        contractAddress: {
            97: '',
            56: '0xe25aB6F05BBF6C1be953BF2d7df15B3e01b8e5a5'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.11284',
        isFinished: true,
        enableEmergencyWithdraw: true
    },
    {
        sousId: 92,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.easy,
        contractAddress: {
            97: '',
            56: '0xEB8Fd597921E3Dd37B0F103a2625F855e2C9b9B5'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.0072338'
    },
    {
        sousId: 91,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.nrv,
        contractAddress: {
            97: '',
            56: '0xABFd8d1942628124aB971937154f826Bce86DcbC'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.15046'
    },
    {
        sousId: 90,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.dego,
        contractAddress: {
            97: '',
            56: '0x526d3c204255f807C95a99b69596f2f9f72345e5'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.00405'
    },
    {
        sousId: 89,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.gum,
        contractAddress: {
            97: '',
            56: '0xAa2082BeE04fc518300ec673F9497ffa6F669dB8'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.08912'
    },
    {
        sousId: 88,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.pbtc,
        contractAddress: {
            97: '',
            56: '0x9096625Bc0d36F5EDa6d44e511641667d89C28f4'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.0000031',
        isFinished: true,
        enableEmergencyWithdraw: true
    },
    {
        sousId: 87,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.dft,
        contractAddress: {
            97: '',
            56: '0x78BD4dB48F8983c3C36C8EAFbEF38f6aC7B55285'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.6811'
    },
    {
        sousId: 86,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.swth,
        contractAddress: {
            97: '',
            56: '0x35418e14F5aA615C4f020eFBa6e01C5DbF15AdD2'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '2.3148'
    },
    {
        sousId: 85,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.lien,
        contractAddress: {
            97: '',
            56: '0x3c7cC49a35942fbD3C2ad428a6c22490cd709d03'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.003472',
        isFinished: true,
        enableEmergencyWithdraw: true
    },
    {
        sousId: 84,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.zil,
        contractAddress: {
            97: '',
            56: '0xF795739737ABcFE0273f4Dced076460fdD024Dd9'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '1.0995'
    },
    {
        sousId: 83,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.pcws,
        contractAddress: {
            97: '',
            56: '0x06FF8960F7F4aE572A3f57FAe77B2882BE94Bf90'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.00434'
    },
    {
        sousId: 82,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bbadger,
        contractAddress: {
            97: '',
            56: '0xe4dD0C50fb314A8B2e84D211546F5B57eDd7c2b9'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.00265278',
        isFinished: true,
        enableEmergencyWithdraw: true
    },
    {
        sousId: 81,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bdigg,
        contractAddress: {
            97: '',
            56: '0xb627A7e33Db571bE792B0b69c5C2f5a8160d5500'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.00000403',
        isFinished: true,
        enableEmergencyWithdraw: true
    },
    {
        sousId: 80,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.lto,
        contractAddress: {
            97: '',
            56: '0xadBfFA25594AF8Bc421ecaDF54D057236a99781e'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.2893'
    },
    {
        sousId: 79,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.mir,
        contractAddress: {
            97: '',
            56: '0x3e31488f08EBcE6F2D8a2AA512aeFa49a3C7dFa7'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.01273',
        sortOrder: 999,
        isFinished: false
    },
    {
        sousId: 78,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.mir,
        contractAddress: {
            97: '',
            56: '0x453a75908fb5a36d482d5f8fe88eca836f32ead5'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.01273',
        sortOrder: 999,
        isFinished: true,
        enableEmergencyWithdraw: true
    },
    {
        sousId: 77,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.trade,
        contractAddress: {
            97: '',
            56: '0x509C99D73FB54b2c20689708b3F824147292D38e'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.4484',
        sortOrder: 999,
        isFinished: false
    },
    {
        sousId: 76,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.dusk,
        contractAddress: {
            97: '',
            56: '0xF1bd5673Ea4a1C415ec84fa3E402F2F7788E7717'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.4629',
        sortOrder: 999,
        isFinished: false
    },
    {
        sousId: 75,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bifi,
        contractAddress: {
            97: '',
            56: '0xB4C68A1C565298834360BbFF1652284275120D47'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.00007234',
        sortOrder: 999,
        isFinished: false
    },
    {
        sousId: 74,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.txl,
        contractAddress: {
            97: '',
            56: '0x153e62257F1AAe05d5d253a670Ca7585c8D3F94F'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.434027',
        sortOrder: 999,
        isFinished: false
    },
    {
        sousId: 73,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.cos,
        contractAddress: {
            97: '',
            56: '0xF682D186168b4114ffDbF1291F19429310727151'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '5.787',
        sortOrder: 999,
        isFinished: false
    },
    {
        sousId: 72,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bunny,
        contractAddress: {
            97: '',
            56: '0xaDdAE5f4dB84847ac9d947AED1304A8e7D19f7cA'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.00289',
        sortOrder: 999,
        isFinished: false
    },
    {
        sousId: 71,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.alice,
        contractAddress: {
            97: '',
            56: '0x4C32048628D0d32d4D6c52662FB4A92747782B56'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.14467',
        sortOrder: 999,
        isFinished: false
    },
    {
        sousId: 70,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.for,
        contractAddress: {
            97: '',
            56: '0x47642101e8D8578C42765d7AbcFd0bA31868c523'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '2.8935',
        sortOrder: 999,
        isFinished: false
    },
    {
        sousId: 69,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bux,
        contractAddress: {
            97: '',
            56: '0x07F8217c68ed9b838b0b8B58C19c79bACE746e9A'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.5787',
        sortOrder: 999,
        isFinished: false
    },
    {
        sousId: 68,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.nuls,
        contractAddress: {
            97: '',
            56: '0x580DC9bB9260A922E3A4355b9119dB990F09410d'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.0868',
        sortOrder: 999,
        isFinished: false
    },
    {
        sousId: 67,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.belt,
        contractAddress: {
            97: '',
            56: '0x6f0037d158eD1AeE395e1c12d21aE8583842F472'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.00868',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 66,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.ramp,
        contractAddress: {
            97: '',
            56: '0x423382f989C6C289c8D441000e1045e231bd7d90'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.4861',
        sortOrder: 999,
        isFinished: true,
        enableEmergencyWithdraw: true
    },
    {
        sousId: 65,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bfi,
        contractAddress: {
            97: '',
            56: '0x0A595623b58dFDe6eB468b613C11A7A8E84F09b9'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.0001157',
        sortOrder: 999,
        isFinished: false
    },
    {
        sousId: 64,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.dexe,
        contractAddress: {
            97: '',
            56: '0x9E6dA246d369a41DC44673ce658966cAf487f7b2'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.009837',
        sortOrder: 999,
        isFinished: true,
        enableEmergencyWithdraw: true
    },
    {
        sousId: 63,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bel,
        contractAddress: {
            97: '',
            56: '0x2C0f449387b15793B9da27c2d945dBed83ab1B07'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.0549',
        sortOrder: 999,
        isFinished: false
    },
    {
        sousId: 62,
        stakingToken: serializedTokens.tpt,
        earningToken: serializedTokens.cake,
        contractAddress: {
            97: '',
            56: '0x0c3D6892aa3b23811Af3bd1bbeA8b0740E8e4528'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.0462',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 61,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.tpt,
        contractAddress: {
            97: '',
            56: '0x75C91844c5383A68b7d3A427A44C32E3ba66Fe45'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '3.616',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 60,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.watch,
        contractAddress: {
            97: '',
            56: '0xC58954199E268505fa3D3Cb0A00b7207af8C2D1d'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.3472',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 59,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.xmark,
        contractAddress: {
            97: '',
            56: '0xA5137e08C48167E363Be8Ec42A68f4F54330964E'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.0413',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 58,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bmxx,
        contractAddress: {
            97: '',
            56: '0x6F31B87f51654424Ce57E9F8243E27ed13846CDB'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.00248',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 57,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.iotx,
        contractAddress: {
            97: '',
            56: '0xCE54BA909d23B9d4BE0Ff0d84e5aE83F0ADD8D9a'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '6.365',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 56,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bor,
        contractAddress: {
            97: '',
            56: '0x3e677dC00668d69c2A7724b9AFA7363e8A56994e'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.000395',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 55,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bopen,
        contractAddress: {
            97: '',
            56: '0x5Ac8406498dC1921735d559CeC271bEd23B294A7'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.0723',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 54,
        stakingToken: serializedTokens.sushi,
        earningToken: serializedTokens.cake,
        contractAddress: {
            97: '',
            56: '0xb69b6e390cba1F68442A886bC89E955048DAe7E3'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.0367',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 53,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.dodo,
        contractAddress: {
            97: '',
            56: '0xae3001ddb18A6A57BEC2C19D71680437CA87bA1D'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.0578',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 52,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.swingby,
        contractAddress: {
            97: '',
            56: '0x02aa767e855b8e80506fb47176202aA58A95315a'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.13',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 51,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bry,
        contractAddress: {
            97: '',
            56: '0x1c736F4FB20C7742Ee83a4099fE92abA61dFca41'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.1157',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 50,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.zee,
        contractAddress: {
            97: '',
            56: '0x02861B607a5E87daf3FD6ec19DFB715F1b371379'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.1736',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 49,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.swgb,
        contractAddress: {
            97: '',
            56: '0x73e4E8d010289267dEe3d1Fc48974B60363963CE'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.899',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 48,
        stakingToken: serializedTokens.comp,
        earningToken: serializedTokens.cake,
        contractAddress: {
            97: '',
            56: '0xE0565fBb109A3f3f8097D8A9D931277bfd795072'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.055',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 47,
        stakingToken: serializedTokens.comp,
        earningToken: serializedTokens.cake,
        contractAddress: {
            97: '',
            56: '0xc3693e3cbc3514d5d07EA5b27A721F184F617900'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.55',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 46,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.sfp,
        contractAddress: {
            97: '',
            56: '0x2B02d43967765b18E31a9621da640588f3550EFD'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.6',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 45,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.lina,
        contractAddress: {
            97: '',
            56: '0x212bb602418C399c29D52C55100fD6bBa12bea05'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.983',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 44,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.lina,
        contractAddress: {
            97: '',
            56: '0x04aE8ca68A116278026fB721c06dCe709eD7013C'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.0983',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 43,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.lit,
        contractAddress: {
            97: '',
            56: '0x1714bAAE9DD4738CDEA07756427FA8d4F08D9479'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.231',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 42,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.hget,
        contractAddress: {
            97: '',
            56: '0xcCD0b93cC6ce3dC6dFaA9DB68f70e5C8455aC5bd'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.0138',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 41,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bdo,
        contractAddress: {
            97: '',
            56: '0x9cB24e9460351bC51d4066BC6AEd1F3809b02B78'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.075',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 40,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.egld,
        contractAddress: {
            97: '',
            56: '0x2dcf4cDFf4Dd954683Fe0a6123077f8a025b66cF'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.001215',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 39,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.ust,
        contractAddress: {
            97: '',
            56: '0x6EFa207ACdE6e1caB77c1322CbdE9628929ba88F'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.1157',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 38,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.wsote,
        contractAddress: {
            97: '',
            56: '0xD0b738eC507571176D40f28bd56a0120E375f73a'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.23',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 37,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.front,
        contractAddress: {
            97: '',
            56: '0xf7a31366732F08E8e6B88519dC3E827e04616Fc9'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.2546',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 36,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.helmet,
        contractAddress: {
            97: '',
            56: '0x9F23658D5f4CEd69282395089B0f8E4dB85C6e79'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.81',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 35,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.btcst,
        contractAddress: {
            97: '',
            56: '0xB6fd2724cc9c90DD31DA35DbDf0300009dceF97d'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.011574',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 34,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bscx,
        contractAddress: {
            97: '',
            56: '0x108BFE84Ca8BCe0741998cb0F60d313823cEC143'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.17361',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 33,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.ten,
        contractAddress: {
            97: '',
            56: '0x4A26b082B432B060B1b00A84eE4E823F04a6f69a'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.05787',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 32,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.balbt,
        contractAddress: {
            97: '',
            56: '0x3cc08B7C6A31739CfEd9d8d38b484FDb245C79c8'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.4166',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 31,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.asr,
        contractAddress: {
            97: '',
            56: '0xd18E1AEb349ef0a6727eCe54597D98D263e05CAB'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.01',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 30,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.atm,
        contractAddress: {
            97: '',
            56: '0x68C7d180bD8F7086D91E65A422c59514e4aFD638'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.01',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 29,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.og,
        contractAddress: {
            97: '',
            56: '0xbE65d7e42E05aD2c4ad28769dc9c5b4b6EAff2C7'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.01',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 28,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.reef,
        contractAddress: {
            97: '',
            56: '0x1500fa1afbfe4f4277ed0345cdf12b2c9ca7e139'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '115.74',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 27,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.ditto,
        contractAddress: {
            97: '',
            56: '0x624ef5C2C6080Af188AF96ee5B3160Bb28bb3E02'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.01157',
        sortOrder: 999,
        isFinished: false
    },
    {
        sousId: 26,
        stakingToken: serializedTokens.twt,
        earningToken: serializedTokens.cake,
        contractAddress: {
            97: '',
            56: '0x0554a5D083Abf2f056ae3F6029e1714B9A655174'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.248',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 24,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.juv,
        contractAddress: {
            97: '',
            56: '0x543467B17cA5De50c8BF7285107A36785Ab57E56'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.02',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 25,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.psg,
        contractAddress: {
            97: '',
            56: '0x65aFEAFaec49F23159e897EFBDCe19D94A86A1B6'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.02',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 21,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.vai,
        contractAddress: {
            97: '',
            56: '0x1AD34D8d4D79ddE88c9B6b8490F8fC67831f2CAe'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.104',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 20,
        stakingToken: serializedTokens.bnb,
        earningToken: serializedTokens.cake,
        contractAddress: {
            97: '',
            56: '0x555Ea72d7347E82C614C16f005fA91cAf06DCB5a'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.BINANCE */ .jh.BINANCE,
        harvest: true,
        tokenPerBlock: '0.5',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 19,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.bnb,
        contractAddress: {
            97: '',
            56: '0x326D754c64329aD7cb35744770D56D0E1f3B3124'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.0041',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 18,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.blink,
        contractAddress: {
            97: '',
            56: '0x42Afc29b2dEa792974d1e9420696870f1Ca6d18b'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '23.14',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 17,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.blink,
        contractAddress: {
            97: '',
            56: '0xBb2B66a2c7C2fFFB06EA60BeaD69741b3f5BF831'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '23.14',
        sortOrder: 999,
        isFinished: true,
        enableEmergencyWithdraw: true
    },
    {
        sousId: 16,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.unfi,
        contractAddress: {
            97: '',
            56: '0xFb1088Dae0f03C5123587d2babb3F307831E6367'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.02893',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 15,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.twt,
        contractAddress: {
            97: '',
            56: '0x9c4EBADa591FFeC4124A7785CAbCfb7068fED2fb'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '5',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 14,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.hard,
        contractAddress: {
            97: '',
            56: '0x90F995b9d46b32c4a1908A8c6D0122e392B3Be97'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.346',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 13,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.broobee,
        contractAddress: {
            97: '',
            56: '0xdc8c45b7F3747Ca9CaAEB3fa5e0b5FCE9430646b'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.COMMUNITY */ .jh.COMMUNITY,
        harvest: true,
        tokenPerBlock: '12.5',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 12,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.stax,
        contractAddress: {
            97: '0xd3af5fe61dbaf8f73149bfcfa9fb653ff096029a',
            56: '0xFF02241a2A1d2a7088A344309400E9fE74772815'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.COMMUNITY */ .jh.COMMUNITY,
        harvest: true,
        tokenPerBlock: '0.2',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 11,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.nar,
        contractAddress: {
            97: '0xd3af5fe61dbaf8f73149bfcfa9fb653ff096029a',
            56: '0xDc938BA1967b06d666dA79A7B1E31a8697D1565E'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.COMMUNITY */ .jh.COMMUNITY,
        harvest: true,
        tokenPerBlock: '1',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 10,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.nya,
        contractAddress: {
            97: '0xd3af5fe61dbaf8f73149bfcfa9fb653ff096029a',
            56: '0x07a0A5B67136d40F4d7d95Bc8e0583bafD7A81b9'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.COMMUNITY */ .jh.COMMUNITY,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '10',
        isFinished: true
    },
    {
        sousId: 9,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.ctk,
        contractAddress: {
            97: '0xAfd61Dc94f11A70Ae110dC0E0F2061Af5633061A',
            56: '0x21A9A53936E812Da06B7623802DEc9A1f94ED23a'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.5',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 8,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.twt,
        contractAddress: {
            97: '0xAfd61Dc94f11A70Ae110dC0E0F2061Af5633061A',
            56: '0xe7f9A439Aa7292719aC817798DDd1c4D35934aAF'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '20',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 7,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.inj,
        contractAddress: {
            97: '0xAfd61Dc94f11A70Ae110dC0E0F2061Af5633061A',
            56: '0xcec2671C81a0Ecf7F8Ee796EFa6DBDc5Cb062693'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        tokenPerBlock: '0.25',
        sortOrder: 999,
        isFinished: true
    },
    {
        sousId: 248,
        stakingToken: serializedTokens.cake,
        earningToken: serializedTokens.ccar,
        contractAddress: {
            97: '',
            56: '0x9e31aef040941E67356519f44bcA07c5f82215e5'
        },
        poolCategory: _types__WEBPACK_IMPORTED_MODULE_2__/* .PoolCategory.CORE */ .jh.CORE,
        harvest: true,
        sortOrder: 999,
        tokenPerBlock: '0.6093'
    }, 
];
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (pools);


/***/ }),

/***/ 7275:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3206);

const teams = [
    {
        id: 1,
        name: 'Syrup Storm',
        description: "The storm's a-comin! Watch out! These bulls are stampeding in a syrupy surge!",
        images: {
            lg: 'syrup-storm-lg.png',
            md: 'syrup-storm-md.png',
            sm: 'syrup-storm-sm.png',
            alt: 'syrup-storm-alt.png',
            ipfs: `${_index__WEBPACK_IMPORTED_MODULE_0__/* .IPFS_GATEWAY */ .Gs}/QmXKzSojwzYjtDCVgR6mVx7w7DbyYpS7zip4ovJB9fQdMG/syrup-storm.png`
        },
        background: 'syrup-storm-bg.svg',
        textColor: '#191326',
        users: 0,
        points: 0
    },
    {
        id: 2,
        name: 'Fearsome Flippers',
        description: "The flippening is coming. Don't get in these bunnies' way, or you'll get flipped, too!",
        images: {
            lg: 'fearsome-flippers-lg.png',
            md: 'fearsome-flippers-md.png',
            sm: 'fearsome-flippers-sm.png',
            alt: 'fearsome-flippers-alt.png',
            ipfs: `${_index__WEBPACK_IMPORTED_MODULE_0__/* .IPFS_GATEWAY */ .Gs}/QmXKzSojwzYjtDCVgR6mVx7w7DbyYpS7zip4ovJB9fQdMG/fearsome-flippers.png`
        },
        background: 'fearsome-flippers-bg.svg',
        textColor: '#FFFFFF',
        users: 0,
        points: 0
    },
    {
        id: 3,
        name: 'Chaotic Cakers',
        description: 'Can you stand the heat? Stay out of the kitchen or you might get burned to a crisp!',
        images: {
            lg: 'chaotic-cakers-lg.png',
            md: 'chaotic-cakers-md.png',
            sm: 'chaotic-cakers-sm.png',
            alt: 'chaotic-cakers-alt.png',
            ipfs: `${_index__WEBPACK_IMPORTED_MODULE_0__/* .IPFS_GATEWAY */ .Gs}/QmXKzSojwzYjtDCVgR6mVx7w7DbyYpS7zip4ovJB9fQdMG/chaotic-cakers.png`
        },
        background: 'chaotic-cakers-bg.svg',
        textColor: '#191326',
        users: 0,
        points: 0
    }, 
];
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (teams);


/***/ }),

/***/ 9748:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ke": () => (/* binding */ mainnetTokens),
/* harmony export */   "ux": () => (/* binding */ testnetTokens),
/* harmony export */   "uO": () => (/* binding */ serializeTokens),
/* harmony export */   "ZP": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(543);
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var state_user_hooks_helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(787);


const { MAINNET , TESTNET  } = _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ChainId;
const defineTokens = (t)=>t
;
const mainnetTokens = defineTokens({
    wbnb: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c', 18, 'WBNB', 'Wrapped BNB', 'https://www.binance.com/'),
    // bnb here points to the wbnb contract. Wherever the currency BNB is required, conditional checks for the symbol 'BNB' can be used
    bnb: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c', 18, 'BNB', 'BNB', 'https://www.binance.com/'),
    cake: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x0E09FaBB73Bd3Ade0a17ECC321fD13a19e81cE82', 18, 'CAKE', 'PancakeSwap Token', 'https://pancakeswap.finance/'),
    tlos: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xb6C53431608E626AC81a9776ac3e999c5556717c', 18, 'TLOS', 'Telos', 'https://www.telos.net/'),
    beta: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xBe1a001FE942f96Eea22bA08783140B9Dcc09D28', 18, 'BETA', 'Beta Finance', 'https://betafinance.org'),
    nft: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x1fC9004eC7E5722891f5f38baE7678efCB11d34D', 6, 'NFT', 'APENFT', 'https://apenft.org'),
    stephero: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xE8176d414560cFE1Bf82Fd73B986823B89E4F545', 18, 'HERO', 'StepHero', 'https://stephero.io/'),
    pros: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xEd8c8Aa8299C10f067496BB66f8cC7Fb338A3405', 18, 'PROS', 'Prosper', 'https://prosper.so/'),
    qbt: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x17B7163cf1Dbd286E262ddc68b553D899B93f526', 18, 'QBT', 'Qubit Token', 'https://qbt.fi/'),
    cvp: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x5Ec3AdBDae549Dce842e24480Eb2434769e22B2E', 18, 'CVP', 'Concentrated Voting Power Token', 'https://powerpool.finance/'),
    bscdefi: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x40E46dE174dfB776BB89E04dF1C47d8a66855EB3', 18, 'BSCDEFI', 'BSC Defi blue chips token', 'https://powerpool.finance/'),
    busd: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56', 18, 'BUSD', 'Binance USD', 'https://www.paxos.com/busd/'),
    dai: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x1AF3F329e8BE154074D8769D1FFa4eE058B1DBc3', 18, 'DAI', 'Dai Stablecoin', 'https://www.makerdao.com/'),
    usdt: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x55d398326f99059fF775485246999027B3197955', 18, 'USDT', 'Tether USD', 'https://tether.to/'),
    btcb: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x7130d2A12B9BCbFAe4f2634d864A1Ee1Ce3Ead9c', 18, 'BTCB', 'Binance BTC', 'https://bitcoin.org/'),
    ust: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x23396cF899Ca06c4472205fC903bDB4de249D6fC', 18, 'UST', 'Wrapped UST Token', 'https://mirror.finance/'),
    eth: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x2170Ed0880ac9A755fd29B2688956BD959F933F8', 18, 'ETH', 'Binance-Peg Ethereum Token', 'https://ethereum.org/en/'),
    usdc: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x8AC76a51cc950d9822D68b83fE1Ad97B32Cd580d', 18, 'USDC', 'Binance-Peg USD Coin', 'https://www.centre.io/usdc'),
    kalm: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x4BA0057f784858a48fe351445C672FF2a3d43515', 18, 'KALM', 'Kalmar Token', 'https://kalmar.io/'),
    dkt: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x7Ceb519718A80Dd78a8545AD8e7f401dE4f2faA7', 18, 'DKT', 'Duelist King', 'https://duelistking.com/'),
    hotcross: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x4FA7163E153419E0E1064e418dd7A99314Ed27b6', 18, 'HOTCROSS', 'Hotcross Token', 'https://www.hotcross.com/'),
    belt: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xE0e514c71282b6f4e823703a39374Cf58dc3eA4f', 18, 'BELT', 'Belt Token', 'https://beta.belt.fi/'),
    watch: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x7A9f28EB62C791422Aa23CeAE1dA9C847cBeC9b0', 18, 'WATCH', 'Yieldwatch Token', 'https://yieldwatch.net/'),
    bry: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xf859Bf77cBe8699013d6Dbc7C2b926Aaf307F830', 18, 'BRY', 'Berry Token', 'https://berrydata.co/'),
    wsote: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x541E619858737031A1244A5d0Cd47E5ef480342c', 18, 'wSOTE', 'Soteria Token', 'https://soteria.finance/'),
    helmet: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x948d2a81086A075b3130BAc19e4c6DEe1D2E3fE8', 18, 'Helmet', 'Helmet Token', 'https://www.helmet.insure/'),
    ten: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xdFF8cb622790b7F92686c722b02CaB55592f152C', 18, 'TEN', 'Tenet Token', 'https://www.tenet.farm/'),
    ditto: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x233d91A0713155003fc4DcE0AFa871b508B3B715', 9, 'DITTO', 'Ditto Token', 'https://ditto.money/'),
    blink: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x63870A18B6e42b01Ef1Ad8A2302ef50B7132054F', 6, 'BLINK', 'Blink Token', 'https://blink.wink.org'),
    syrup: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x009cF7bC57584b7998236eff51b98A168DceA9B0', 18, 'SYRUP', 'SyrupBar Token', 'https://pancakeswap.finance/'),
    pha: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x0112e557d400474717056C4e6D40eDD846F38351', 18, 'PHA', 'Phala Token', 'https://phala.network'),
    babycake: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xdB8D30b74bf098aF214e862C90E647bbB1fcC58c', 18, 'BABYCAKE', 'Baby Cake Token', 'https://babycake.app/'),
    bmon: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x08ba0619b1e7A582E0BCe5BBE9843322C954C340', 18, 'BMON', 'Binamon Token', 'https://binamon.org/'),
    hero: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xD40bEDb44C081D2935eebA6eF5a3c8A31A1bBE13', 18, 'HERO', 'Metahero Token', 'https://metahero.io/'),
    wsg: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xA58950F05FeA2277d2608748412bf9F802eA4901', 18, 'WSG', 'Wall Street Games Token', 'https://wsg.gg/'),
    mcrn: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xacb2d47827C9813AE26De80965845D80935afd0B', 18, 'MCRN', 'Macaronswap Token', 'https://www.macaronswap.finance/'),
    revv: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x833F307aC507D47309fD8CDD1F835BeF8D702a93', 18, 'REVV', 'REVV Token', 'https://revvmotorsport.com/'),
    skill: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x154A9F9cbd3449AD22FDaE23044319D6eF2a1Fab', 18, 'SKILL', 'Cryptoblades Token', 'https://www.cryptoblades.io/'),
    if: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xB0e1fc65C1a741b4662B813eB787d369b8614Af1', 18, 'IF', 'Impossible Finance Token', 'https://impossible.finance/'),
    sps: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x1633b7157e7638C4d6593436111Bf125Ee74703F', 18, 'SPS', 'Splinterlands Token', 'https://splinterlands.com'),
    chess: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x20de22029ab63cf9A7Cf5fEB2b737Ca1eE4c82A6', 18, 'CHESS', 'Chess Token', 'https://tranchess.com/'),
    titan: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xe898EDc43920F357A93083F1d4460437dE6dAeC2', 18, 'TITAN', 'Titanswap Token', 'https://titanswap.org'),
    harmony: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x03fF0ff224f904be3118461335064bB48Df47938', 18, 'ONE', 'Harmony ONE Token', 'https://www.harmony.one/'),
    mask: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x2eD9a5C8C13b93955103B9a7C167B67Ef4d568a3', 18, 'MASK', 'Mask Token', 'https://mask.io/'),
    dvi: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x758FB037A375F17c7e195CC634D77dA4F554255B', 18, 'DVI', 'Dvision Network Token', 'https://dvision.network/'),
    adx: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x6bfF4Fb161347ad7de4A625AE5aa3A1CA7077819', 18, 'ADX', 'Adex Network Token', 'https://www.adex.network'),
    bscpad: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x5A3010d4d8D3B5fB49f8B6E57FB9E48063f16700', 18, 'BSCPAD', 'Bscpad Token', 'https://bscpad.com/'),
    rabbit: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x95a1199EBA84ac5f19546519e287d43D2F0E1b41', 18, 'RABBIT', 'Rabbit Finance Token', 'https://rabbitfinance.io/earn'),
    form: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x25A528af62e56512A19ce8c3cAB427807c28CC19', 18, 'FORM', 'Formation Token', 'https://formation.fi/'),
    txl: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x1FFD0b47127fdd4097E54521C9E2c7f0D66AafC5', 18, 'TXL', 'Tixl Token', 'https://tixl.org/'),
    orbs: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xeBd49b26169e1b52c04cFd19FCf289405dF55F80', 18, 'ORBS', 'Orbs Token', 'https://www.orbs.com/'),
    cos: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x96Dd399F9c3AFda1F194182F71600F1B65946501', 18, 'COS', 'Contentos Token', 'https://www.contentos.io/'),
    bunny: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xC9849E6fdB743d08fAeE3E34dd2D1bc69EA11a51', 18, 'BUNNY', 'Pancakebunny Token', 'https://pancakebunny.finance/'),
    alice: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xAC51066d7bEC65Dc4589368da368b212745d63E8', 6, 'ALICE', 'My Neighbor Alice Token', 'https://www.myneighboralice.com/'),
    for: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x658A109C5900BC6d2357c87549B651670E5b0539', 18, 'FOR', 'Fortube Token', 'https://www.for.tube/home'),
    bux: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x211FfbE424b90e25a15531ca322adF1559779E45', 18, 'BUX', 'Bux Crypto Token', 'https://getbux.com/bux-crypto/'),
    nuls: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x8CD6e29d3686d24d3C2018CEe54621eA0f89313B', 8, 'NULS', 'Nuls Token', 'https://www.nuls.io/'),
    ramp: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x8519EA49c997f50cefFa444d240fB655e89248Aa', 18, 'RAMP', 'RAMP DEFI Token', 'https://rampdefi.com/'),
    bfi: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x81859801b01764D4f0Fa5E64729f5a6C3b91435b', 18, 'BFI', 'bearn.fi Token', 'https://bearn.fi/'),
    dexe: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x039cB485212f996A9DBb85A9a75d898F94d38dA6', 18, 'DEXE', 'DeXe Token', 'https://dexe.network/'),
    bel: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x8443f091997f06a61670B735ED92734F5628692F', 18, 'BEL', 'Bella Protocol Token', 'https://bella.fi/'),
    tpt: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xECa41281c24451168a37211F0bc2b8645AF45092', 4, 'TPT', 'Tokenpocket Token', 'https://www.tokenpocket.pro/'),
    xmark: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x26A5dFab467d4f58fB266648CAe769503CEC9580', 9, 'xMARK', 'Benchmark Protocol Token', 'https://benchmarkprotocol.finance/'),
    bmxx: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x4131b87F74415190425ccD873048C708F8005823', 18, 'bMXX', 'Multiplier Token', 'https://multiplier.finance/'),
    iotx: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x9678E42ceBEb63F23197D726B29b1CB20d0064E5', 18, 'IOTX', 'Binance-Peg IoTeX Network Token', 'https://iotex.io/'),
    bor: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x92D7756c60dcfD4c689290E8A9F4d263b3b32241', 18, 'BOR', 'BoringDAO Token', 'https://www.boringdao.com/'),
    bopen: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xF35262a9d427F96d2437379eF090db986eaE5d42', 18, 'bOPEN', 'OPEN Governance Token', 'https://opendao.io/'),
    dodo: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x67ee3Cb086F8a16f34beE3ca72FAD36F7Db929e2', 18, 'DODO', 'Dodo Token', 'https://dodoex.io/'),
    swingby: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x71DE20e0C4616E7fcBfDD3f875d568492cBE4739', 18, 'SWINGBY', 'Swingby Network Token', 'https://swingby.network/'),
    zee: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x44754455564474A89358B2C2265883DF993b12F0', 18, 'ZEE', 'Zeroswap Token', 'https://zeroswap.io/'),
    swgb: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xE40255C5d7fa7ceEc5120408C78C787CECB4cfdb', 18, 'SWGb', 'SWGb Token', 'https://swirgepay.com/'),
    swg: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xe792f64C582698b8572AAF765bDC426AC3aEfb6B', 18, 'SWG', 'SWG Token', 'https://swirgepay.com/'),
    sfp: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xD41FDb03Ba84762dD66a0af1a6C8540FF1ba5dfb', 18, 'SFP', 'Safepal Token', 'https://www.safepal.io/'),
    lina: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x762539b45A1dCcE3D36d080F74d1AED37844b878', 18, 'LINA', 'Linear Finance Token', 'https://linear.finance/'),
    lit: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xb59490aB09A0f526Cc7305822aC65f2Ab12f9723', 18, 'LIT', 'Litentry Token', 'https://www.litentry.com/'),
    hget: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xC7d8D35EBA58a0935ff2D5a33Df105DD9f071731', 6, 'HGET', 'Hedget Token', 'https://www.hedget.com/'),
    bdo: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x190b589cf9Fb8DDEabBFeae36a813FFb2A702454', 18, 'BDO', 'Bdollar Token', 'https://bdollar.fi/'),
    egld: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xbF7c81FFF98BbE61B40Ed186e4AfD6DDd01337fe', 18, 'EGLD', 'Elrond Token', 'https://elrond.com/'),
    front: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x928e55daB735aa8260AF3cEDadA18B5f70C72f1b', 18, 'FRONT', 'Frontier Token', 'https://frontier.xyz/'),
    btcst: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x78650B139471520656b9E7aA7A5e9276814a38e9', 17, 'BTCST', 'StandardBTCHashrate Token', 'https://www.1-b.tc/'),
    bscx: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x5Ac52EE5b2a633895292Ff6d8A89bB9190451587', 18, 'BSCX', 'BSCX Token', 'https://bscex.org/'),
    balbt: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x72fAa679E1008Ad8382959FF48E392042A8b06f7', 18, 'bALBT', 'AllianceBlock Token', 'https://allianceblock.io/'),
    asr: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x80D5f92C2c8C682070C95495313dDB680B267320', 2, 'ASR', 'AS Roma Token', 'https://www.chiliz.com'),
    atm: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x25E9d05365c867E59C1904E7463Af9F312296f9E', 2, 'ATM', 'Athletico Madrid Token', 'https://www.chiliz.com'),
    og: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xf05E45aD22150677a017Fbd94b84fBB63dc9b44c', 2, 'OG', 'OG Nice Token', 'https://www.chiliz.com'),
    reef: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xF21768cCBC73Ea5B6fd3C687208a7c2def2d966e', 18, 'REEF', 'Reef.finance Token', 'https://reef.finance/'),
    juv: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xC40C9A843E1c6D01b7578284a9028854f6683b1B', 2, 'JUV', 'Juventus Token', 'https://www.chiliz.com'),
    psg: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xBc5609612b7C44BEf426De600B5fd1379DB2EcF1', 2, 'PSG', 'Paris Saint-Germain Token', 'https://www.chiliz.com'),
    vai: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x4BD17003473389A42DAF6a0a729f6Fdb328BbBd7', 18, 'VAI', 'VAI Stablecoin', '0x4BD17003473389A42DAF6a0a729f6Fdb328BbBd7'),
    unfi: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x728C5baC3C3e370E372Fc4671f9ef6916b814d8B', 18, 'UNFI', 'UNFI Token', 'https://unifiprotocol.com'),
    twt: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x4B0F1812e5Df2A09796481Ff14017e6005508003', 18, 'TWT', 'Trust Wallet Token', 'https://trustwallet.com/'),
    hard: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xf79037F6f6bE66832DE4E7516be52826BC3cBcc4', 6, 'HARD', 'HARD Token', 'https://hard.kava.io'),
    broobee: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xE64F5Cb844946C1F102Bd25bBD87a5aB4aE89Fbe', 18, 'bROOBEE', 'ROOBEE Token', 'https://roobee.io/'),
    stax: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x0Da6Ed8B13214Ff28e9Ca979Dd37439e8a88F6c4', 18, 'STAX', 'StableX Token', 'https://stablexswap.com/'),
    nar: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xA1303E6199b319a891b79685F0537D289af1FC83', 18, 'NAR', 'Narwhalswap Token', 'https://narwhalswap.org/'),
    nya: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xbFa0841F7a90c4CE6643f651756EE340991F99D5', 18, 'NYA', 'Nyanswop Token', 'https://nyanswop.org/'),
    ctk: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xA8c2B8eec3d368C0253ad3dae65a5F2BBB89c929', 6, 'CTK', 'Certik Token', 'https://www.certik.foundation/'),
    inj: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xa2B726B1145A4773F68593CF171187d8EBe4d495', 18, 'INJ', 'Injective Protocol Token', 'https://injectiveprotocol.com/'),
    sxp: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x47BEAd2563dCBf3bF2c9407fEa4dC236fAbA485A', 18, 'SXP', 'Swipe Token', 'https://swipe.io/'),
    alpha: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xa1faa113cbE53436Df28FF0aEe54275c13B40975', 18, 'ALPHA', 'Alpha Finance Token', 'https://alphafinance.io/'),
    xvs: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xcF6BB5389c92Bdda8a3747Ddb454cB7a64626C63', 18, 'XVS', 'Venus Token', 'https://venus.io/'),
    sushi: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x947950BcC74888a40Ffa2593C5798F11Fc9124C4', 18, 'SUSHI', 'Binance-Peg SushiToken', 'https://sushi.com/'),
    comp: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x52CE071Bd9b1C4B00A0b92D298c512478CaD67e8', 18, 'COMP', 'Compound Finance Token', 'https://compound.finance/'),
    bifi: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xCa3F508B8e4Dd382eE878A314789373D80A5190A', 18, 'BIFI', 'Beefy Finance Token', 'https://beefy.finance/'),
    dusk: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xB2BD0749DBE21f623d9BABa856D3B0f0e1BFEc9C', 18, 'DUSK', 'Dusk Network Token', 'https://dusk.network/'),
    beth: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x250632378E573c6Be1AC2f97Fcdf00515d0Aa91B', 18, 'BETH', 'Binance Beacon ETH', 'https://ethereum.org/en/eth2/beacon-chain/'),
    mamzn: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x3947B992DC0147D2D89dF0392213781b04B25075', 18, 'mAMZN', 'Wrapped Mirror AMZN Token', 'https://mirror.finance/'),
    mgoogl: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x62D71B23bF15218C7d2D7E48DBbD9e9c650B173f', 18, 'mGOOGL', 'Wrapped Mirror GOOGL Token', 'https://mirror.finance/'),
    mnflx: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xa04F060077D90Fe2647B61e4dA4aD1F97d6649dc', 18, 'mNFLX', 'Wrapped Mirror NFLX Token', 'https://mirror.finance/'),
    mtsla: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xF215A127A196e3988C09d052e16BcFD365Cd7AA3', 18, 'mTSLA', 'Wrapped Mirror TSLA Token', 'https://mirror.finance/'),
    ltc: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x4338665CBB7B2485A8855A139b75D5e34AB0DB94', 18, 'LTC', 'Binance-Peg Litecoin Token', 'https://litecoin.org/'),
    ada: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x3EE2200Efb3400fAbB9AacF31297cBdD1d435D47', 18, 'ADA', ' Binance-Peg Cardano Token', 'https://www.cardano.org/'),
    band: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xAD6cAEb32CD2c308980a548bD0Bc5AA4306c6c18', 18, 'BAND', 'Binance-Peg Band Protocol Token', 'https://bandprotocol.com/'),
    dot: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x7083609fCE4d1d8Dc0C979AAb8c869Ea2C873402', 18, 'DOT', 'Binance-Peg Polkadot Token', 'https://polkadot.network/'),
    eos: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x56b6fB708fC5732DEC1Afc8D8556423A2EDcCbD6', 18, 'EOS', 'Binance-Peg EOS Token', 'https://eos.io/'),
    link: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xF8A0BF9cF54Bb92F17374d9e9A321E6a111a51bD', 18, 'LINK', 'Binance-Peg Chainlink Token', 'https://chain.link/'),
    xrp: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x1D2F0da169ceB9fC7B3144628dB156f3F6c60dBE', 18, 'XRP', 'Binance-Peg XRP Token', 'https://ripple.com/xrp/'),
    atom: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x0Eb3a705fc54725037CC9e008bDede697f62F335', 18, 'ATOM', 'Binance-Peg Cosmos Token', 'https://cosmos.network/'),
    yfii: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x7F70642d88cf1C4a3a7abb072B53B929b653edA5', 18, 'YFII', 'Binance-Peg YFII.finance Token', 'https://dfi.money/#/'),
    xtz: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x16939ef78684453bfDFb47825F8a5F714f12623a', 18, 'XTZ', 'Binance-Peg Tezos Token', 'https://www.tezos.com/'),
    bch: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x8fF795a6F4D97E7887C79beA79aba5cc76444aDf', 18, 'BCH', 'Binance-Peg Bitcoin Cash Token', 'https://bch.info/'),
    yfi: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x88f1A5ae2A3BF98AEAF342D26B30a79438c9142e', 18, 'YFI', 'Binance-Peg yearn.finance Token', 'https://yearn.finance/'),
    uni: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xBf5140A22578168FD562DCcF235E5D43A02ce9B1', 18, 'UNI', 'Binance-Peg Uniswap Token', 'https://uniswap.org/'),
    fil: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x0D8Ce2A99Bb6e3B7Db580eD848240e4a0F9aE153', 18, 'FIL', 'Binance-Peg Filecoin Token', 'https://filecoin.io/'),
    bake: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xE02dF9e3e622DeBdD69fb838bB799E3F168902c5', 18, 'BAKE', 'Bakeryswap Token', 'https://www.bakeryswap.org/'),
    burger: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xAe9269f27437f0fcBC232d39Ec814844a51d6b8f', 18, 'BURGER', 'Burgerswap Token', 'https://burgerswap.org/'),
    bdigg: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x5986D5c77c65e5801a5cAa4fAE80089f870A71dA', 18, 'bDIGG', 'Badger Sett Digg Token', 'https://badger.finance/'),
    bbadger: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x1F7216fdB338247512Ec99715587bb97BBf96eae', 18, 'bBadger', 'Badger Sett Badger Token', 'https://badger.finance/'),
    trade: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x7af173F350D916358AF3e218Bdf2178494Beb748', 18, 'TRADE', 'Unitrade Token', 'https://unitrade.app/'),
    pnt: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xdaacB0Ab6Fb34d24E8a67BfA14BF4D95D4C7aF92', 18, 'PNT', 'pNetwork Token', 'https://ptokens.io/'),
    mir: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x5B6DcF557E2aBE2323c48445E8CC948910d8c2c9', 18, 'MIR', 'Mirror Protocol Token', 'https://mirror.finance/'),
    pbtc: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xeD28A457A5A76596ac48d87C0f577020F6Ea1c4C', 18, 'pBTC', 'pTokens BTC Token', 'https://ptokens.io/'),
    lto: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x857B222Fc79e1cBBf8Ca5f78CB133d1b7CF34BBd', 18, 'LTO', 'LTO Network Token', 'https://ltonetwork.com/'),
    pcws: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xbcf39F0EDDa668C58371E519AF37CA705f2bFcbd', 18, 'pCWS', 'PolyCrowns Token', 'https://game.seascape.network/'),
    zil: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xb86AbCb37C3A4B64f74f59301AFF131a1BEcC787', 12, 'ZIL', 'Zilliqa Token', 'https://www.zilliqa.com/'),
    lien: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x5d684ADaf3FcFe9CFb5ceDe3abf02F0Cdd1012E3', 8, 'LIEN', 'Lien Finance Token', 'https://lien.finance/'),
    swth: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x250b211EE44459dAd5Cd3bCa803dD6a7EcB5d46C', 8, 'SWTH', 'Switcheo Network Token', 'https://switcheo.network/'),
    dft: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x42712dF5009c20fee340B245b510c0395896cF6e', 18, 'DFT', 'Dfuture Token', 'https://www.dfuture.com/home'),
    gum: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xc53708664b99DF348dd27C3Ac0759d2DA9c40462', 18, 'GUM', 'GourmetGalaxy Token', 'https://gourmetgalaxy.io/'),
    dego: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x3FdA9383A84C05eC8f7630Fe10AdF1fAC13241CC', 18, 'DEGO', 'Dego Finance Token', 'https://bsc.dego.finance/home'),
    nrv: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x42F6f551ae042cBe50C739158b4f0CAC0Edb9096', 18, 'NRV', 'Nerve Finance Token', 'https://nerve.fi/'),
    easy: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x7C17c8bED8d14bAccE824D020f994F4880D6Ab3B', 18, 'EASY', 'EASY Token', 'https://easyfi.network/'),
    oddz: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xCD40F2670CF58720b694968698A5514e924F742d', 18, 'ODDZ', 'Oddz Token', 'https://oddz.fi/'),
    hoo: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xE1d1F66215998786110Ba0102ef558b22224C016', 8, 'HOO', 'Hoo Token', 'https://hoo.com/'),
    apys: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x37dfACfaeDA801437Ff648A1559d73f4C40aAcb7', 18, 'APYS', 'APY Swap Token', 'https://apyswap.com/'),
    bondly: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x96058f8C3e16576D9BD68766f3836d9A33158f89', 18, 'BONDLY', 'Bondly Token', 'https://www.bondly.finance/'),
    tko: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x9f589e3eabe42ebC94A44727b3f3531C0c877809', 18, 'TKO', 'Tokocrypto Token', 'https://www.tokocrypto.com/'),
    itam: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x04C747b40Be4D535fC83D09939fb0f626F32800B', 18, 'ITAM', 'Itam Network Token', 'https://itam.network/'),
    arpa: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x6F769E65c14Ebd1f68817F5f1DcDb61Cfa2D6f7e', 18, 'ARPA', 'Arpachain Token', 'https://arpachain.io/'),
    eps: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xA7f552078dcC247C2684336020c03648500C6d9F', 18, 'EPS', 'Ellipsis Finance Token', 'https://ellipsis.finance/'),
    jgn: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xC13B7a43223BB9Bf4B69BD68Ab20ca1B79d81C75', 18, 'JGN', 'Juggernaut DeFi Token', 'https://jgndefi.com/'),
    tlm: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x2222227E22102Fe3322098e4CBfE18cFebD57c95', 4, 'TLM', 'Alien Worlds Trilium Token', 'https://alienworlds.io/'),
    perl: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x0F9E4D49f25de22c2202aF916B681FBB3790497B', 18, 'PERL', 'Perlin', 'https://perlinx.finance/'),
    alpa: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xc5E6689C9c8B02be7C49912Ef19e79cF24977f03', 18, 'ALPA', 'AlpaToken', 'https://bsc.alpaca.city/'),
    fho: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x84da2d4024ac44307ea67fe10077dbbdc10c5654', 9, '$FHO', 'FhoToken', 'https://flashherobsc.com/'),
    hzn: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xC0eFf7749b125444953ef89682201Fb8c6A917CD', 18, 'HZN', 'Horizon Protocol Token', 'https://horizonprotocol.com/'),
    suter: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x4CfbBdfBd5BF0814472fF35C72717Bd095ADa055', 18, 'SUTER', 'Suterusu Token', 'https://shield.suterusu.io/'),
    cgg: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x1613957159E9B0ac6c80e824F7Eea748a32a0AE2', 18, 'CGG', 'pTokens CGG Token', 'https://chainguardians.io/'),
    mix: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xB67754f5b4C704A24d2db68e661b2875a4dDD197', 18, 'MIX', 'Mix Token', 'https://mixie.chainguardians.io/'),
    hakka: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x1D1eb8E8293222e1a29d2C0E4cE6C0Acfd89AaaC', 18, 'HAKKA', 'Hakka Token', 'https://hakka.finance/'),
    xed: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x5621b5A3f4a8008c4CCDd1b942B121c8B1944F1f', 18, 'XED', 'Exeedme Token', 'https://www.exeedme.com/'),
    τbtc: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x2cD1075682b0FCCaADd0Ca629e138E64015Ba11c', 9, 'τBTC', 'τBitcoin Token', 'https://www.btcst.finance/'),
    alpaca: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x8F0528cE5eF7B51152A59745bEfDD91D97091d2F', 18, 'ALPACA', 'AlpacaToken', 'https://www.alpacafinance.org/'),
    dfd: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x9899a98b222fCb2f3dbee7dF45d943093a4ff9ff', 18, 'DFD', 'DefiDollar DAO', 'https://dusd.finance/'),
    lmt: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x9617857E191354dbEA0b714d78Bc59e57C411087', 18, 'LMT', 'Lympo Market Token', 'https://lympo.io/lmt/'),
    btt: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x8595F9dA7b868b1822194fAEd312235E43007b49', 18, 'BTT', 'Binance-Peg BitTorrent Token', 'https://www.bittorrent.com/'),
    trx: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x85EAC5Ac2F758618dFa09bDbe0cf174e7d574D5B', 18, 'TRX', 'TRON Token', 'https://tron.network/'),
    win: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xaeF0d72a118ce24feE3cD1d43d383897D05B4e99', 18, 'WIN', 'WIN Token', 'https://winklink.org/'),
    mcoin: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x49022089e78a8D46Ec87A3AF86a1Db6c189aFA6f', 18, 'MCOIN', 'Wrapped Mirror COIN Token', 'https://mirror.finance/'),
    math: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xF218184Af829Cf2b0019F8E6F0b2423498a36983', 18, 'MATH', 'MATH Token', 'https://mathwallet.org/'),
    kun: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x1A2fb0Af670D0234c2857FaD35b789F8Cb725584', 18, 'KUN', 'QIAN governance token', 'https://chemix.io/home'),
    qsd: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x07AaA29E63FFEB2EBf59B33eE61437E1a91A3bb2', 18, 'QSD', 'QIAN second generation dollar', 'https://chemix.io/home'),
    hyfi: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x9a319b959e33369C5eaA494a770117eE3e585318', 18, 'HYFI', 'HYFI Token', 'https://hyfi.pro/#/'),
    oin: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x658E64FFcF40D240A43D52CA9342140316Ae44fA', 8, 'OIN', 'oinfinance Token', 'https://oin.finance/'),
    doge: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xbA2aE424d960c26247Dd6c32edC70B295c744C43', 8, 'DOGE', 'Binance-Peg Dogecoin', 'https://dogecoin.com/'),
    fine: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x4e6415a5727ea08aAE4580057187923aeC331227', 18, 'FINE', 'Refinable Token', 'https://refinable.com/'),
    one: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x04BAf95Fd4C52fd09a56D840bAEe0AB8D7357bf0', 18, 'ONE', 'BigONE Token', 'https://www.bigone.com/'),
    pmon: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x1796ae0b0fa4862485106a0de9b654eFE301D0b2', 18, 'PMON', 'Polkamon Token', 'https://polkamon.com/'),
    τdoge: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xe550a593d09FBC8DCD557b5C88Cea6946A8b404A', 8, 'τDOGE', 'τDogecoin', 'https://www.btcst.finance/'),
    btr: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x5a16E8cE8cA316407c6E6307095dc9540a8D62B3', 18, 'BTR', 'Bitrue Token', 'https://www.bitrue.com/'),
    ubxt: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xBbEB90cFb6FAFa1F69AA130B7341089AbeEF5811', 18, 'UBXT', 'UpBots Token', 'https://upbots.com/'),
    wmass: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x7e396BfC8a2f84748701167c2d622F041A1D7a17', 8, 'WMASS', 'Wrapped MASS Token', 'https://massnet.org/en/'),
    rfox: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x0a3A21356793B49154Fd3BbE91CBc2A16c0457f5', 18, 'RFOX', 'RFOX Token', 'https://www.redfoxlabs.io/'),
    xend: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x4a080377f83D669D7bB83B3184a8A5E61B500608', 18, 'XEND', 'XEND Token', 'https://xend.finance/'),
    cyc: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x810EE35443639348aDbbC467b33310d2AB43c168', 18, 'CYC', 'CYC Token', 'https://cyclone.xyz/'),
    chr: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xf9CeC8d50f6c8ad3Fb6dcCEC577e05aA32B224FE', 6, 'CHR', 'Chroma Token', 'https://chromia.com/'),
    deri: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xe60eaf5A997DFAe83739e035b005A33AfdCc6df5', 18, 'DERI', 'Deri Token', 'https://deri.finance/#/index'),
    well: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xf07a32Eb035b786898c00bB1C64d8c6F8E7a46D5', 18, 'WELL', 'BitWell Token', 'https://www.bitwellex.com/'),
    wex: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xa9c41A46a6B3531d28d5c32F6633dd2fF05dFB90', 18, 'WEX', 'WaultSwap Token', 'https://wault.finance/'),
    waultx: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xB64E638E60D154B43f660a6BF8fD8a3b249a6a21', 18, 'WAULTx', 'Wault Token', 'https://wault.finance/'),
    popen: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xaBaE871B7E3b67aEeC6B46AE9FE1A91660AadAC5', 18, 'pOPEN', 'OPEN Governance Token', 'https://opendao.io/'),
    ez: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x5512014efa6Cd57764Fa743756F7a6Ce3358cC83', 18, 'EZ', 'Easy V2 Token', 'https://easyfi.network/'),
    vrt: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x5F84ce30DC3cF7909101C69086c50De191895883', 18, 'VRT', 'Venus Reward Token', 'https://venus.io/'),
    tusd: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x14016E85a25aeb13065688cAFB43044C2ef86784', 18, 'TUSD', 'Binance-Peg TrueUSD Token', 'https://www.trueusd.com/'),
    mtrg: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xBd2949F67DcdC549c6Ebe98696449Fa79D988A9F', 18, 'MTRG', 'Wrapped MTRG Token', 'https://www.meter.io/'),
    ktn: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xDAe6c2A48BFAA66b43815c5548b10800919c993E', 18, 'KTN', 'Kattana Token', 'https://kattana.io/'),
    qkc: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xA1434F1FC3F437fa33F7a781E041961C0205B5Da', 18, 'QKC', 'QuarkChain Token', 'https://quarkchain.io/'),
    bcfx: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x045c4324039dA91c52C55DF5D785385Aab073DcF', 18, 'bCFX', 'BSC Conflux Token', 'https://www.confluxnetwork.org/'),
    mx: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x9F882567A62a5560d147d64871776EeA72Df41D3', 18, 'MX', 'MX Token', 'https://www.mxc.com/'),
    ata: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xA2120b9e674d3fC3875f415A7DF52e382F141225', 18, 'ATA', 'Automata Token', 'https://www.ata.network/'),
    mbox: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x3203c9E46cA618C8C1cE5dC67e7e9D75f5da2377', 18, 'MBOX', 'Mobox Token', 'https://www.mobox.io/#/'),
    boring: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xffEecbf8D7267757c2dc3d13D730E97E15BfdF7F', 18, 'BORING', 'BoringDAO Token', 'https://www.boringdao.com/'),
    marsh: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x2FA5dAF6Fe0708fBD63b1A7D1592577284f52256', 18, 'MARSH', 'Unmarshal Token', 'https://unmarshal.io/'),
    ampl: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xDB021b1B247fe2F1fa57e0A87C748Cc1E321F07F', 9, 'AMPL', 'AMPL Token', 'https://www.ampleforth.org/'),
    o3: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xEe9801669C6138E84bD50dEB500827b776777d28', 18, 'O3', 'O3 Swap Token', 'https://o3swap.com/'),
    hai: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xaA9E582e5751d703F85912903bacADdFed26484C', 8, 'HAI', 'Hacken Token', 'https://hacken.io/'),
    htb: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x4e840AADD28DA189B9906674B4Afcb77C128d9ea', 18, 'HTB', 'Hotbit Token', 'https://www.hotbit.io/'),
    woo: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x4691937a7508860F876c9c0a2a617E7d9E945D4B', 18, 'WOO', 'Wootrade Network Token', 'https://woo.network/'),
    $dg: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x9Fdc3ae5c814b79dcA2556564047C5e7e5449C19', 18, '$DG', 'Decentral Games Token', 'https://decentral.games/'),
    safemoon: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x8076C74C5e3F5852037F31Ff0093Eeb8c8ADd8D3', 9, 'SAFEMOON', 'Safemoon Token', 'https://safemoon.net/'),
    axs: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x715D400F88C167884bbCc41C5FeA407ed4D2f8A0', 18, 'AXS', 'Binance-Pegged Axie Infinity Shard', 'https://axieinfinity.com/'),
    c98: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xaEC945e04baF28b135Fa7c640f624f8D90F1C3a6', 18, 'c98', 'Coin98 Token', 'https://coin98.com/'),
    pots: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x3Fcca8648651E5b974DD6d3e50F61567779772A8', 18, 'POTS', 'Moonpot Token', 'https://moonpot.com/'),
    gnt: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xF750A26EB0aCf95556e8529E72eD530f3b60f348', 18, 'GNT', 'GreenTrust Token', 'https://www.greentrusttoken.com/'),
    rusd: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x07663837218A003e66310a01596af4bf4e44623D', 18, 'rUSD', 'rUSD Token', 'https://appv2.rampdefi.com/#/'),
    bp: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xACB8f52DC63BB752a51186D1c55868ADbFfEe9C1', 18, 'BP', 'BunnyPark Token', 'https://www.bunnypark.com/'),
    sfund: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x477bC8d23c634C154061869478bce96BE6045D12', 18, 'SFUND', 'Seedify Fund Token', 'https://seedify.fund/'),
    naos: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x758d08864fB6cCE3062667225ca10b8F00496cc2', 18, 'NAOS', 'NAOSToken', 'https://naos.finance/'),
    cart: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x5C8C8D560048F34E5f7f8ad71f2f81a89DBd273e', 18, 'CART', 'CryptoArt.ai', 'https://cryptoart.ai/'),
    light: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x037838b556d9c9d654148a284682C55bB5f56eF4', 18, 'LIGHT', 'Lightning', 'https://lightningprotocol.finance/'),
    rpg: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xc2098a8938119A52B1F7661893c0153A6CB116d5', 18, 'RPG', 'Rangers Protocol', 'https://www.rangersprotocol.com/'),
    mcb: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x5fE80d2CD054645b9419657d3d10d26391780A7B', 18, 'MCB', 'MCDEX', 'https://mcdex.io/homepage/'),
    lazio: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x77d547256A2cD95F32F67aE0313E450Ac200648d', 8, 'LAZIO', 'FC Lazio Fan Token', 'https://launchpad.binance.com/en/subscription/LAZIO_BNB'),
    arv: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x6679eB24F59dFe111864AEc72B443d1Da666B360', 8, 'ARV', 'ARIVA', 'https://ariva.digital'),
    moni: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x9573c88aE3e37508f87649f87c4dd5373C9F31e0', 18, 'MONI', 'Monsta Infinite', 'https://monstainfinite.com/'),
    xms: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x7859B01BbF675d67Da8cD128a50D155cd881B576', 18, 'XMS', 'Mars Ecosystem', 'https://marsecosystem.com/'),
    zoo: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x1D229B958D5DDFca92146585a8711aECbE56F095', 18, 'ZOO', 'ZOO Crypto World', 'https://zoogame.finance/'),
    fina: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x426c72701833fdDBdFc06c944737C6031645c708', 18, 'FINA', 'Defina Finance', 'https://defina.finance/'),
    dar: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x23CE9e926048273eF83be0A3A8Ba9Cb6D45cd978', 6, 'DAR', 'Mines of Dalarnia', 'https://www.minesofdalarnia.com/'),
    xwg: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x6b23C89196DeB721e6Fd9726E6C76E4810a464bc', 18, 'XWG', 'X World Games', 'https://xwg.games/'),
    eternal: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xD44FD09d74cd13838F137B590497595d6b3FEeA4', 18, 'ETERNAL', 'CryptoMines Eternal', 'https://cryptomines.app/'),
    porto: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x49f2145d6366099e13B10FbF80646C0F377eE7f6', 8, 'PORTO', 'FC Porto Fan Token', 'https://launchpad.binance.com/en/subscription/PORTO_BNB'),
    kart: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x8BDd8DBcBDf0C066cA5f3286d33673aA7A553C10', 18, 'KART', 'Dragon Kart', 'https://dragonkart.com/'),
    qi: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x8729438EB15e2C8B576fCc6AeCdA6A148776C0F5', 18, 'QI', 'BENQI', 'https://benqi.fi/'),
    sheesha: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x232FB065D9d24c34708eeDbF03724f2e95ABE768', 18, 'SHEESHA', 'Sheesha Finance', 'https://www.sheeshafinance.io/'),
    bcoin: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x00e1656e45f18ec6747F5a8496Fd39B50b38396D', 18, 'BCOIN', 'Bomb Crypto', 'https://bombcrypto.io/'),
    quidd: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x7961Ade0a767c0E5B67Dd1a1F78ba44F727642Ed', 18, 'QUIDD', 'Quidd Token', 'https://www.quiddtoken.com/'),
    santos: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xA64455a4553C9034236734FadDAddbb64aCE4Cc7', 8, 'SANTOS', 'FC Santos Fan Token', 'https://launchpad.binance.com/en/launchpool/SANTOS_BNB'),
    nabox: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x755f34709E369D37C6Fa52808aE84A32007d1155', 18, 'NABOX', 'Nabox Token', 'https://nabox.io/'),
    xcv: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x4be63a9b26EE89b9a3a13fd0aA1D0b2427C135f8', 18, 'XCV', 'XCarnival', 'https://xcarnival.fi/'),
    idia: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x0b15Ddf19D47E6a86A56148fb4aFFFc6929BcB89', 18, 'IDIA', 'Impossible Decentralized Incubator Access Token', 'https://impossible.finance/'),
    tt: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x990E7154bB999FAa9b2fa5Ed29E822703311eA85', 18, 'TT', 'Thunder Token', 'https://www.thundercore.com/'),
    gmee: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x84e9a6F9D240FdD33801f7135908BfA16866939A', 18, 'GMEE', 'GAMEE', 'https://www.gamee.com/token'),
    htd: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x5E2689412Fae5c29BD575fbe1d5C1CD1e0622A8f', 18, 'HTD', 'HeroesTD', 'https://heroestd.io/'),
    dpt: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xE69cAef10A488D7AF31Da46c89154d025546e990', 18, 'DPT', 'Diviner Protocol', 'https://diviner.finance/'),
    thg: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x9fD87aEfe02441B123c3c32466cD9dB4c578618f', 18, 'THG', 'Thetan Gem', 'https://thetanarena.com/'),
    ccar: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x50332bdca94673F33401776365b66CC4e81aC81d', 18, 'CCAR', 'CryptoCars', 'https://cryptocars.me/'),
    high: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x5f4Bde007Dc06b867f86EBFE4802e34A1fFEEd63', 18, 'HIGH', 'Highstreet Token', 'https://highstreet.market/'),
    sdao: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x90Ed8F1dc86388f14b64ba8fb4bbd23099f18240', 18, 'SDAO', 'Singularity Dao', 'https://app.singularitydao.ai/'),
    antex: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xCA1aCAB14e85F30996aC83c64fF93Ded7586977C', 8, 'ANTEX', 'Antex', 'https://antex.org/'),
    bbt: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xD48474E7444727bF500a32D5AbE01943f3A59A64', 8, 'BBT', 'BitBook', 'https://www.bitbook.network/'),
    woop: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x8b303d5BbfBbf46F1a4d9741E491e06986894e18', 18, 'WOOP', 'Woonkly Power', 'https://www.woonkly.com/'),
    gm: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xe2604C9561D490624AA35e156e65e590eB749519', 18, 'GM', 'GoldMiner', 'https://goldminer.games/'),
    aog: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x40C8225329Bd3e28A043B029E0D07a5344d2C27C', 18, 'AOG', 'AgeOfGods', 'https://ageofgods.net/'),
    '8pay': new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xFeea0bDd3D07eb6FE305938878C0caDBFa169042', 18, '8PAY', '8PAY Network', 'https://8pay.network/'),
    bath: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x0bc89aa98Ad94E6798Ec822d0814d934cCD0c0cE', 18, 'BATH', 'Battle Hero', 'https://battlehero.io/'),
    insur: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x3192CCDdf1CDcE4Ff055EbC80f3F0231b86A7E30', 18, 'INSUR', 'Bsc-Peg INSUR Token', 'https://www.insurace.io/'),
    froyo: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xe369fec23380f9F14ffD07a1DC4b7c1a9fdD81c9', 18, 'FROYO', 'Froyo Games', 'https://froyo.games/'),
    apx: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x78F5d389F5CDCcFc41594aBaB4B0Ed02F31398b3', 18, 'APX', 'ApolloX Token', 'https://www.apollox.finance/'),
    prl: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0xd07e82440A395f3F3551b42dA9210CD1Ef4f8B24', 18, 'PRL', 'Parallel Token', 'https://theparallel.io'),
    fuse: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(MAINNET, '0x5857c96DaE9cF8511B08Cb07f85753C472D36Ea3', 18, 'FUSE', 'Fuse Token', 'https://fuse.io/')
});
const testnetTokens = defineTokens({
    wbnb: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(TESTNET, '0x094616F0BdFB0b526bD735Bf66Eca0Ad254ca81F', 18, 'WBNB', 'Wrapped BNB', 'https://www.binance.com/'),
    cake: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(TESTNET, '0xa35062141Fa33BCA92Ce69FeD37D0E8908868AAe', 18, 'CAKE', 'PancakeSwap Token', 'https://pancakeswap.finance/'),
    busd: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(TESTNET, '0xeD24FC36d5Ee211Ea25A80239Fb8C4Cfd80f12Ee', 18, 'BUSD', 'Binance USD', 'https://www.paxos.com/busd/'),
    syrup: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(TESTNET, '0xfE1e507CeB712BDe086f3579d2c03248b2dB77f9', 18, 'SYRUP', 'SyrupBar Token', 'https://pancakeswap.finance/'),
    bake: new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(TESTNET, '0xE02dF9e3e622DeBdD69fb838bB799E3F168902c5', 18, 'BAKE', 'Bakeryswap Token', 'https://www.bakeryswap.org/')
});
const tokens = ()=>{
    const chainId = "56";
    // If testnet - return list comprised of testnetTokens wherever they exist, and mainnetTokens where they don't
    if (parseInt(chainId, 10) === _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ChainId.TESTNET) {
        return Object.keys(mainnetTokens).reduce((accum, key)=>{
            return {
                ...accum,
                [key]: testnetTokens[key] || mainnetTokens[key]
            };
        }, {});
    }
    return mainnetTokens;
};
const unserializedTokens = tokens();
const serializeTokens = ()=>{
    const serializedTokens = Object.keys(unserializedTokens).reduce((accum, key)=>{
        return {
            ...accum,
            [key]: (0,state_user_hooks_helpers__WEBPACK_IMPORTED_MODULE_1__/* .serializeToken */ .Mq)(unserializedTokens[key])
        };
    }, {});
    return serializedTokens;
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (unserializedTokens);


/***/ }),

/***/ 7971:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "jh": () => (/* binding */ PoolCategory),
/* harmony export */   "p3": () => (/* binding */ LotteryStatus),
/* harmony export */   "iF": () => (/* binding */ FetchStatus)
/* harmony export */ });
/* unused harmony exports PoolIds, AuctionStatus */
var PoolIds;
(function(PoolIds) {
    PoolIds["poolBasic"] = "poolBasic";
    PoolIds["poolUnlimited"] = "poolUnlimited";
})(PoolIds || (PoolIds = {}));
var PoolCategory;
(function(PoolCategory) {
    PoolCategory['COMMUNITY'] = 'Community';
    PoolCategory['CORE'] = 'Core';
    PoolCategory['BINANCE'] = 'Binance';
    PoolCategory['AUTO'] = 'Auto';
})(PoolCategory || (PoolCategory = {}));
var LotteryStatus;
(function(LotteryStatus) {
    LotteryStatus["PENDING"] = 'pending';
    LotteryStatus["OPEN"] = 'open';
    LotteryStatus["CLOSE"] = 'close';
    LotteryStatus["CLAIMABLE"] = 'claimable';
})(LotteryStatus || (LotteryStatus = {}));
var AuctionStatus;
(function(AuctionStatus) {
    AuctionStatus[AuctionStatus["ToBeAnnounced"] = 0] = "ToBeAnnounced";
    AuctionStatus[AuctionStatus["Pending"] = 1] = "Pending";
    AuctionStatus[AuctionStatus["Open"] = 2] = "Open";
    AuctionStatus[AuctionStatus["Finished"] = 3] = "Finished";
    AuctionStatus[AuctionStatus["Closed"] = 4] = "Closed";
})(AuctionStatus || (AuctionStatus = {}));
var FetchStatus;
(function(FetchStatus) {
    FetchStatus["Idle"] = 'IDLE';
    FetchStatus["Fetching"] = 'FETCHING';
    FetchStatus["Fetched"] = 'FETCHED';
    FetchStatus["Failed"] = 'FAILED';
})(FetchStatus || (FetchStatus = {}));


/***/ }),

/***/ 3206:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "hJ": () => (/* binding */ BSC_BLOCK_TIME),
/* harmony export */   "st": () => (/* binding */ BASE_BSC_SCAN_URLS),
/* harmony export */   "_n": () => (/* binding */ BASE_URL),
/* harmony export */   "OS": () => (/* binding */ BASE_BSC_SCAN_URL),
/* harmony export */   "Gs": () => (/* binding */ IPFS_GATEWAY)
/* harmony export */ });
/* unused harmony exports CAKE_PER_BLOCK, BLOCKS_PER_YEAR, CAKE_PER_YEAR, BASE_ADD_LIQUIDITY_URL, DEFAULT_TOKEN_DECIMAL, DEFAULT_GAS_LIMIT, AUCTION_BIDDERS_TO_FETCH, RECLAIM_AUCTIONS_TO_FETCH, AUCTION_WHITELISTED_BIDDERS_TO_FETCH, PANCAKE_BUNNIES_UPDATE_FREQUENCY */
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(543);
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var bignumber_js_bignumber__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(899);
/* harmony import */ var bignumber_js_bignumber__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(bignumber_js_bignumber__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var utils_bigNumber__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5128);



bignumber_js_bignumber__WEBPACK_IMPORTED_MODULE_1___default().config({
    EXPONENTIAL_AT: 1000,
    DECIMAL_PLACES: 80
});
const BSC_BLOCK_TIME = 3;
const BASE_BSC_SCAN_URLS = {
    [_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ChainId.MAINNET]: 'https://bscscan.com',
    [_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ChainId.TESTNET]: 'https://testnet.bscscan.com'
};
// CAKE_PER_BLOCK details
// 40 CAKE is minted per block
// 20 CAKE per block is sent to Burn pool (A farm just for burning cake)
// 10 CAKE per block goes to CAKE syrup pool
// 9 CAKE per block goes to Yield farms and lottery
// CAKE_PER_BLOCK in config/index.ts = 40 as we only change the amount sent to the burn pool which is effectively a farm.
// CAKE/Block in src/views/Home/components/CakeDataRow.tsx = 15 (40 - Amount sent to burn pool)
const CAKE_PER_BLOCK = 40;
const BLOCKS_PER_YEAR = 60 / BSC_BLOCK_TIME * 60 * 24 * 365 // 10512000
;
const CAKE_PER_YEAR = CAKE_PER_BLOCK * BLOCKS_PER_YEAR;
const BASE_URL = 'https://pancakeswap.finance';
const BASE_ADD_LIQUIDITY_URL = `${BASE_URL}/add`;
const BASE_BSC_SCAN_URL = BASE_BSC_SCAN_URLS[_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ChainId.MAINNET];
const DEFAULT_TOKEN_DECIMAL = utils_bigNumber__WEBPACK_IMPORTED_MODULE_2__/* .BIG_TEN.pow */ .xp.pow(18);
const DEFAULT_GAS_LIMIT = 200000;
const AUCTION_BIDDERS_TO_FETCH = 500;
const RECLAIM_AUCTIONS_TO_FETCH = 500;
const AUCTION_WHITELISTED_BIDDERS_TO_FETCH = 500;
const IPFS_GATEWAY = 'https://ipfs.io/ipfs';
// In reality its 10000 because of fast refresh, a bit less here to cover for possible long request times
const PANCAKE_BUNNIES_UPDATE_FREQUENCY = 8000;


/***/ }),

/***/ 9821:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EN": () => (/* binding */ EN),
/* harmony export */   "Mj": () => (/* binding */ languages),
/* harmony export */   "s0": () => (/* binding */ languageList)
/* harmony export */ });
/* unused harmony exports AR, BN, DE, EL, ESES, FI, FIL, FR, HI, HU, ID, IT, JA, KO, NL, PL, PTBR, PTPT, RO, RU, SVSE, TA, TR, UK, VI, ZHCN, ZHTW */
const AR = {
    locale: 'ar-SA',
    language: 'العربية',
    code: 'ar'
};
const BN = {
    locale: 'bn-BD',
    language: 'বাংলা',
    code: 'bn'
};
const EN = {
    locale: 'en-US',
    language: 'English',
    code: 'en'
};
const DE = {
    locale: 'de-DE',
    language: 'Deutsch',
    code: 'de'
};
const EL = {
    locale: 'el-GR',
    language: 'Ελληνικά',
    code: 'el'
};
const ESES = {
    locale: 'es-ES',
    language: 'Espa\xf1ol',
    code: 'es-ES'
};
const FI = {
    locale: 'fi-FI',
    language: 'Suomalainen',
    code: 'fi'
};
const FIL = {
    locale: 'fil-PH',
    language: 'Filipino',
    code: 'fil'
};
const FR = {
    locale: 'fr-FR',
    language: 'Fran\xe7ais',
    code: 'fr'
};
const HI = {
    locale: 'hi-IN',
    language: 'हिंदी',
    code: 'hi'
};
const HU = {
    locale: 'hu-HU',
    language: 'Magyar',
    code: 'hu'
};
const ID = {
    locale: 'id-ID',
    language: 'Bahasa Indonesia',
    code: 'id'
};
const IT = {
    locale: 'it-IT',
    language: 'Italiano',
    code: 'it'
};
const JA = {
    locale: 'ja-JP',
    language: '日本語',
    code: 'ja'
};
const KO = {
    locale: 'ko-KR',
    language: '한국어',
    code: 'ko'
};
const NL = {
    locale: 'nl-NL',
    language: 'Nederlands',
    code: 'nl'
};
const PL = {
    locale: 'pl-PL',
    language: 'Polski',
    code: 'pl'
};
const PTBR = {
    locale: 'pt-BR',
    language: 'Portugu\xeas (Brazil)',
    code: 'pt-br'
};
const PTPT = {
    locale: 'pt-PT',
    language: 'Portugu\xeas',
    code: 'pt-pt'
};
const RO = {
    locale: 'ro-RO',
    language: 'Rom\xe2nă',
    code: 'ro'
};
const RU = {
    locale: 'ru-RU',
    language: 'Русский',
    code: 'ru'
};
const SVSE = {
    locale: 'sv-SE',
    language: 'Svenska',
    code: 'sv'
};
const TA = {
    locale: 'ta-IN',
    language: 'தமிழ்',
    code: 'ta'
};
const TR = {
    locale: 'tr-TR',
    language: 'T\xfcrk\xe7e',
    code: 'tr'
};
const UK = {
    locale: 'uk-UA',
    language: 'Українська',
    code: 'uk'
};
const VI = {
    locale: 'vi-VN',
    language: 'Tiếng Việt',
    code: 'vi'
};
const ZHCN = {
    locale: 'zh-CN',
    language: '简体中文',
    code: 'zh-cn'
};
const ZHTW = {
    locale: 'zh-TW',
    language: '繁體中文',
    code: 'zh-tw'
};
const languages = {
    'ar-SA': AR,
    'bn-BD': BN,
    'en-US': EN,
    'de-DE': DE,
    'el-GR': EL,
    'es-ES': ESES,
    'fi-FI': FI,
    'fil-PH': FIL,
    'fr-FR': FR,
    'hi-IN': HI,
    'hu-HU': HU,
    'id-ID': ID,
    'it-IT': IT,
    'ja-JP': JA,
    'ko-KR': KO,
    'nl-NL': NL,
    'pl-PL': PL,
    'pt-BR': PTBR,
    'pt-PT': PTPT,
    'ro-RO': RO,
    'ru-RU': RU,
    'sv-SE': SVSE,
    'ta-IN': TA,
    'tr-TR': TR,
    'uk-UA': UK,
    'vi-VN': VI,
    'zh-CN': ZHCN,
    'zh-TW': ZHTW
};
const languageList = Object.values(languages);


/***/ }),

/***/ 5290:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SH": () => (/* binding */ LS_KEY),
/* harmony export */   "w2": () => (/* binding */ fetchLocale),
/* harmony export */   "jq": () => (/* binding */ getLanguageCodeFromLS)
/* harmony export */ });
/* harmony import */ var config_localization_languages__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9821);

const publicUrl = process.env.PUBLIC_URL || '';
const LS_KEY = 'pancakeswap_language';
const fetchLocale = async (locale)=>{
    const response = await fetch(`${publicUrl}/locales/${locale}.json`);
    const data = await response.json();
    return data;
};
const getLanguageCodeFromLS = ()=>{
    try {
        const codeFromStorage = localStorage.getItem(LS_KEY);
        return codeFromStorage || config_localization_languages__WEBPACK_IMPORTED_MODULE_0__.EN.locale;
    } catch  {
        return config_localization_languages__WEBPACK_IMPORTED_MODULE_0__.EN.locale;
    }
};


/***/ }),

/***/ 9150:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "iL": () => (/* reexport */ LanguageProvider),
  "$G": () => (/* reexport */ Localization_useTranslation)
});

// UNUSED EXPORTS: LanguageContext, languageMap

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./src/config/localization/languages.ts
var languages = __webpack_require__(9821);
;// CONCATENATED MODULE: ./src/config/localization/translations.json
const translations_namespaceObject = JSON.parse('{"Farm":"Farm","Staking":"Staking","Syrup Pool":"Syrup Pool","Exchange":"Exchange","Docs":"Docs","Voting":"Voting","Lottery":"Lottery","Connect Wallet":"Connect Wallet","Your %asset% Balance":"Your %asset% Balance","Total %asset% Supply":"Total %asset% Supply","Locked":"Locked","Pending harvest":"Pending harvest","New rewards per block":"New rewards per block","Total CAKE burned since launch":"Total CAKE burned since launch","See the Kitchen":"See the Kitchen","Telegram":"Telegram","Blog":"Blog","Github":"Github","Twitter":"Twitter","Deposit":"Deposit","Earn":"Earn","Stake LP tokens to stack CAKE":"Stake LP tokens to stack CAKE","You can swap back anytime":"You can swap back anytime","%asset% Earned":"%asset% Earned","Tokens Staked":"Tokens Staked","Every time you stake and unstake CAKE tokens, the contract will automagically harvest CAKE rewards for you!":"Every time you stake and unstake CAKE tokens, the contract will automagically harvest CAKE rewards for you!","XVS Tokens Earned":"XVS Tokens Earned","Rewards will be calculated per block and total rewards will be distributed automatically at the end of each project’s farming period.":"Rewards will be calculated per block and total rewards will be distributed automatically at the end of each project’s farming period.","Pool":"Pool","Coming Soon":"Coming Soon","APY":"APY","Total Liquidity":"Total Liquidity","View on BscScan":"View on BscScan","Cake price":"Cake price","%asset% Tokens Earned":"%asset% Tokens Earned","%num% blocks until farming ends":"%num% blocks until farming ends","Coming Soon...":"Coming Soon...","Your Stake":"Your Stake","Farming starts in %num% Blocks":"Farming starts in %num% Blocks","Finished":"Finished","Farming ends in %num% Blocks":"Farming ends in %num% Blocks","Project site":"Project site","Project Site":"Project Site","See Token Info":"See Token Info","You can unstake at any time.":"You can unstake at any time.","Rewards are calculated per block.":"Rewards are calculated per block.","Total":"Total","End":"End","View Project Site":"View Project Site","Your Project? 👀":"Your Project? 👀","Create a pool for your token":"Create a pool for your token","Apply now":"Apply now","Round 1: BUYING":"Round 1: BUYING","%num% CAKE":"%num% CAKE","Spend CAKE to buy tickets, contributing to the lottery pot. Ticket purchases end approx. 30 minutes before lottery. Win prizes if 2, 3, or 4 of your ticket numbers match the winning numbers and their positions! Good luck!":"Spend CAKE to buy tickets, contributing to the lottery pot. Ticket purchases end approx. 30 minutes before lottery. Win prizes if 2, 3, or 4 of your ticket numbers match the winning numbers and their positions! Good luck!","Your total tickets for this round":"Your total tickets for this round","Buy ticket":"Buy ticket","View your tickets":"View your tickets","Approx. time left to buy tickets":"Approx. time left to buy tickets","My Tickets (Total: %TICKETS%)":"My Tickets (Total: %TICKETS%)","Close":"Close","Latest Winning Numbers":"Latest Winning Numbers","Ticket":"Ticket","Tickets matching 4 numbers:":"Tickets matching 4 numbers:","Tickets matching 3 numbers:":"Tickets matching 3 numbers:","Tickets matching 2 numbers:":"Tickets matching 2 numbers:","Export recent winning numbers":"Export recent winning numbers","Enter amount of tickets to buy":"Enter amount of tickets to buy","Max":"Max","%num% CAKE Available":"%num% CAKE Available","%num% %symbol% Available":"%num% %symbol% Available","Your amount must be a multiple of 10 CAKE":"Your amount must be a multiple of 10 CAKE","1 Ticket = %lotteryPrice% CAKE":"1 Ticket = %lotteryPrice% CAKE","Until ticket sale:":"Until ticket sale:","You will spend: %num% CAKE":"You will spend: %num% CAKE","Cancel":"Cancel","Confirm":"Confirm","Warning":"Warning","Lottery ticket purchases are final.":"Lottery ticket purchases are final.","Your CAKE will not be returned to you after you spend it to buy tickets.":"Your CAKE will not be returned to you after you spend it to buy tickets.","Tickets are only valid for one lottery draw, and will be burned after the draw.":"Tickets are only valid for one lottery draw, and will be burned after the draw.","Buying tickets does not guarantee you will win anything. Please only participate once you understand the risks.":"Buying tickets does not guarantee you will win anything. Please only participate once you understand the risks.","I understand":"I understand","Ticket purchases are final. Your CAKE cannot be returned to you after buying tickets.":"Ticket purchases are final. Your CAKE cannot be returned to you after buying tickets.","Claim prizes":"Claim prizes","CAKE prizes to be claimed":"CAKE prizes to be claimed","Round 2: CLAIMING":"Round 2: CLAIMING","Pending Confirmation":"Pending Confirmation","Approx. time before next lottery start":"Approx. time before next lottery start","Enable CAKE":"Enable CAKE","IFO: Initial Farm Offerings":"IFO: Initial Farm Offerings","Buy new tokens launching on Binance Smart Chain":"Buy new tokens launching on Binance Smart Chain","You’ll pay for the new tokens using CAKE-BNB LP tokens, which means you need to stake equal amounts of CAKE and BNB in a liquidity pool to take part.":"You’ll pay for the new tokens using CAKE-BNB LP tokens, which means you need to stake equal amounts of CAKE and BNB in a liquidity pool to take part.","Get CAKE-BNB LP >":"Get CAKE-BNB LP >","The project gets the BNB, PancakeSwap burns the CAKE.":"The project gets the BNB, PancakeSwap burns the CAKE.","You get the tokens.":"You get the tokens.","Want to launch your own IFO?":"Want to launch your own IFO?","Launch your project with PancakeSwap, Binance Smart Chain’s most-used AMM project and liquidity provider, to bring your token directly to the most active and rapidly growing community on BSC.":"Launch your project with PancakeSwap, Binance Smart Chain’s most-used AMM project and liquidity provider, to bring your token directly to the most active and rapidly growing community on BSC.","Apply to launch":"Apply to launch","Community":"Community","Core":"Core","Available":"Available","My Wallet":"My Wallet","Sign out":"Sign out","Harvest all (%count%)":"Harvest all (%count%)","Cake Stats":"Cake Stats","Total CAKE Supply":"Total CAKE Supply","Total CAKE Burned":"Total CAKE Burned","New CAKE/block":"New CAKE/block","Farms & Staking":"Farms & Staking","CAKE to Harvest":"CAKE to Harvest","CAKE in Wallet":"CAKE in Wallet","Collecting CAKE":"Collecting CAKE","Your Lottery Winnings":"Your Lottery Winnings","CAKE to Collect":"CAKE to Collect","Total jackpot this round":"Total jackpot this round","Collect Winnings":"Collect Winnings","Buy Tickets":"Buy Tickets","Harvest":"Harvest","Select":"Select","Winning Numbers This Round":"Winning Numbers This Round","Winning numbers":"Winning numbers","Total prizes":"Total prizes","Round #%num%":"Round #%num%","Total Pot:":"Total Pot:","Staking Pool":"Staking Pool","PancakeSwap":"PancakeSwap","The #1 AMM and yield farm on Binance Smart Chain.":"The #1 AMM and yield farm on Binance Smart Chain.","Stake CAKE to earn new tokens.":"Stake CAKE to earn new tokens.","Launch Time":"Launch Time","For Sale":"For Sale","CAKE to burn (USD)":"CAKE to burn (USD)","CAKE to burn:":"CAKE to burn:","Unstake":"Unstake","⭐️ Every time you stake and unstake LP tokens, the contract will automagically harvest CAKE rewards for you!":"⭐️ Every time you stake and unstake LP tokens, the contract will automagically harvest CAKE rewards for you!","How to take part":"How to take part","Before Sale":"Before Sale","Buy CAKE and BNB tokens":"Buy CAKE and BNB tokens","Get CAKE-BNB LP tokens by adding CAKE and BNB liquidity":"Get CAKE-BNB LP tokens by adding CAKE and BNB liquidity","During Sale":"During Sale","While the sale is live, commit your CAKE-LP tokens to buy the IFO tokens":"While the sale is live, commit your CAKE-LP tokens to buy the IFO tokens","After Sale":"After Sale","Claim the tokens you bought, along with any unspent funds.":"Claim the tokens you bought, along with any unspent funds.","Done!":"Done!","Read more":"Read more","Connect":"Connect","Trade in for CAKE, or keep for your collection!":"Trade in for CAKE, or keep for your collection!","Register your interest in winning an NFT below.":"Register your interest in winning an NFT below.","Register for a chance to win":"Register for a chance to win","Learn more":"Learn more","Trade in NFT":"Trade in NFT","Trade in":"Trade in","You will receive":"You will receive","When you trade in this NFT to receive CAKE, you will lose access to it forever!":"When you trade in this NFT to receive CAKE, you will lose access to it forever!","Claim NFT":"Claim NFT","How it works":"How it works","Winners will be able to claim an NFT on this page once the claiming period starts.":"Winners will be able to claim an NFT on this page once the claiming period starts.","If you’re not selected, you won’t be able to claim. Better luck next time!":"If you’re not selected, you won’t be able to claim. Better luck next time!","Winners can trade in their NFTs for a CAKE value until the expiry date written below. If you don\'t trade in your NFT by then, don’t worry: you’ll still keep it in your wallet!":"Winners can trade in their NFTs for a CAKE value until the expiry date written below. If you don\'t trade in your NFT by then, don’t worry: you’ll still keep it in your wallet!","How are winners selected?":"How are winners selected?","Winners are selected at random! Good luck!":"Winners are selected at random! Good luck!","Value if traded in":"Value if traded in","Number minted":"Number minted","Number burned":"Number burned","Claim this NFT":"Claim this NFT","Trade in for CAKE":"Trade in for CAKE","Loading…":"Loading…","Details":"Details","You won!":"You won!","Claim an NFT from the options below!":"Claim an NFT from the options below!","Can be traded until":"Can be traded until","Wallet Disconnected":"Wallet Disconnected","Connect to see if you have won an NFT!":"Connect to see if you have won an NFT!","Home":"Home","Trade":"Trade","Farms":"Farms","Pools":"Pools","NFT":"NFT","Info":"Info","IFO":"IFO","More":"More","Liquidity":"Liquidity","Overview":"Overview","Token":"Token","Pairs":"Pairs","Accounts":"Accounts","Stake LP tokens to earn CAKE":"Stake LP tokens to earn CAKE","Active":"Active","Inactive":"Inactive","Dual":"Dual","Compound":"Compound","Unstake %asset%":"Unstake %asset%","The CAKE Lottery":"The CAKE Lottery","Buy tickets with CAKE":"Buy tickets with CAKE","Win if 2, 3 or 4 of your ticket numbers match!":"Win if 2, 3 or 4 of your ticket numbers match!","%time% Until lottery draw":"%time% Until lottery draw","Next draw":"Next draw","Past draws":"Past draws","Round %num%":"Round %num%","Total Pot":"Total Pot","Your tickets for this round":"Your tickets for this round","Sorry, no prizes to collect":"Sorry, no prizes to collect","In Wallet":"In Wallet","Loading...":"Loading...","Next IFO":"Next IFO","Past IFOs":"Past IFOs","APR":"APR","APR (incl. LP rewards)":"APR (incl. LP rewards)","Select lottery number:":"Select lottery number:","Search":"Search","History":"History","Pool Size":"Pool Size","Burned":"Burned","To burn":"To burn","Show Last":"Show Last","Prize Pot":"Prize Pot","Winners":"Winners","No. Matched":"No. Matched","Enable Contract":"Enable Contract","%asset% staked":"%asset% staked","Total Value Locked":"Total Value Locked","Across all LPs and Syrup Pools":"Across all LPs and Syrup Pools","Your wallet":"Your wallet","Logout":"Logout","Profile Setup":"Profile Setup","Show off your stats and collectibles with your unique profile.":"Show off your stats and collectibles with your unique profile.","Total cost: 10 CAKE":"Total cost: 10 CAKE","Get Starter Collectible":"Get Starter Collectible","Set Profile Picture":"Set Profile Picture","Join Team":"Join Team","Set Name":"Set Name","Step 1":"Step 1","Every profile starts by making a \\"starter\\" collectible (NFT).":"Every profile starts by making a \\"starter\\" collectible (NFT).","This starter will also become your first profile picture.":"This starter will also become your first profile picture.","You can change your profile pic later if you get another approved Pancake Collectible,":"You can change your profile pic later if you get another approved Pancake Collectible,","Choose your Starter!":"Choose your Starter!","Choose wisely: you can only ever make one starter collectible!":"Choose wisely: you can only ever make one starter collectible!","Cost: 5 CAKE":"Cost: 5 CAKE","Next Step":"Next Step","Confirming":"Confirming","Enabled":"Enabled","Confirmed":"Confirmed","Insufficient CAKE balance":"Insufficient CAKE balance","Step 2":"Step 2","Choose collectible":"Choose collectible","Choose a profile picture from the eligible collectibles (NFT) in your wallet, shown below.":"Choose a profile picture from the eligible collectibles (NFT) in your wallet, shown below.","Only approved Pancake Collectibles can be used.":"Only approved Pancake Collectibles can be used.","Allow collectible to be locked":"Allow collectible to be locked","The collectible you\'ve chosen will be locked in a smart contract while it\'s being used as your profile picture. ":"The collectible you\'ve chosen will be locked in a smart contract while it\'s being used as your profile picture. ","Don\'t worry - you\'ll be able to get it back at any time.":"Don\'t worry - you\'ll be able to get it back at any time.","Step 3":"Step 3","Join a Team":"Join a Team","It won\'t be possible to undo the choice you make for the foreseeable future!":"It won\'t be possible to undo the choice you make for the foreseeable future!","There\'s currently no big difference between teams, and no benefit of joining one team over another for now.":"There\'s currently no big difference between teams, and no benefit of joining one team over another for now.","So pick whichever you like!":"So pick whichever you like!","%count% Members":"%count% Members","Step 4":"Step 4","This name will be shown in team leaderboards and search results as long as your profile is active.":"This name will be shown in team leaderboards and search results as long as your profile is active.","Your name must be at least 3 and at most 15 standards letters and numbers long.":"Your name must be at least 3 and at most 15 standards letters and numbers long.","Complete Profile":"Complete Profile","Maximum length: 15 characters":"Maximum length: 15 characters","Minimum length: 3 characters":"Minimum length: 3 characters","No spaces or special characters":"No spaces or special characters","Submitting NFT to contract and confirming User Name and Team":"Submitting NFT to contract and confirming User Name and Team","Oops!":"Oops!","We couldn\'t find any Pancake Collectibles in your wallet.":"We couldn\'t find any Pancake Collectibles in your wallet.","You need a Pancake Collectible to finish setting up your profile. If you sold or transferred your starter collectible to another wallet, you\'ll need to get it back or acquire a new one somehow. You can\'t make a new starter with this wallet address.":"You need a Pancake Collectible to finish setting up your profile. If you sold or transferred your starter collectible to another wallet, you\'ll need to get it back or acquire a new one somehow. You can\'t make a new starter with this wallet address.","ROI Calculator":"ROI Calculator","Timeframe":"Timeframe","CAKE per $1000":"CAKE per $1000","Calculated based on current rates.":"Calculated based on current rates.","Compounding %freq%x daily.":"Compounding %freq%x daily.","All figures are estimates provided for your convenience only, and by no means represent guaranteed returns.":"All figures are estimates provided for your convenience only, and by no means represent guaranteed returns.","You can\'t change this once you click Confirm.":"You can\'t change this once you click Confirm.","Until ticket sale":"Until ticket sale","To burn:":"To burn:","On sale soon":"On sale soon","On sale":"On sale","Teams Overview":"Teams Overview","Teams":"Teams","See More":"See More","Team Achievements":"Team Achievements","Team Points":"Team Points","Active Members":"Active Members","Set up now":"Set up now","You haven’t set up your profile yet!":"You haven’t set up your profile yet!","You can do this at any time by clicking on your profile picture in the menu":"You can do this at any time by clicking on your profile picture in the menu","Collect":"Collect","Compounding":"Compounding","Harvesting":"Harvesting","Buy CAKE":"Buy CAKE","Show":"Show","Hide":"Hide","Stake LP tokens":"Stake LP tokens","Stake":"Stake","Earned":"Earned","Staked":"Staked","The lottery number you provided does not exist":"The lottery number you provided does not exist","Error fetching data":"Error fetching data","Teams & Profiles":"Teams & Profiles","Earn more points for completing larger quests!":"Earn more points for completing larger quests!","Collecting points for these tasks makes them available again.":"Collecting points for these tasks makes them available again.","Earn points by completing regular tasks!":"Earn points by completing regular tasks!","Task Center":"Task Center","Achievements":"Achievements","Enter your name...":"Enter your name...","I understand that people can view my wallet if they know my username":"I understand that people can view my wallet if they know my username","A minimum of %num% CAKE is required":"A minimum of %num% CAKE is required","Only reuse a name from other social media if you\'re OK with people viewing your wallet. You can\'t change your name once you click Confirm.":"Only reuse a name from other social media if you\'re OK with people viewing your wallet. You can\'t change your name once you click Confirm.","Please connect your wallet to continue":"Please connect your wallet to continue","Public Profile":"Public Profile","Show off your stats and collectibles with your unique profile. Team features will be revealed soon!":"Show off your stats and collectibles with your unique profile. Team features will be revealed soon!","Points":"Points","Set Your Name":"Set Your Name","Step %num%":"Step %num%","See the list >":"See the list >","Staked only":"Staked only","Get %symbol%":"Get %symbol%","Get %symbol1% or %symbol2%":"Get %symbol1% or %symbol2%","Balance":"Balance","Oops, page not found.":"Oops, page not found.","Click here to reset!":"Click here to reset!","Oops, something wrong.":"Oops, something wrong.","Back Home":"Back Home","Unstake LP tokens":"Unstake LP tokens","Get ready":"Get ready","Live":"Live","Start":"Start","Finish":"Finish","Connect wallet to view":"Connect wallet to view","Sorry, you needed to register during the “entry” period!":"Sorry, you needed to register during the “entry” period!","Check your Rank":"Check your Rank","You’re not participating this time.":"You’re not participating this time.","Rank in team":"Rank in team","Your volume":"Your volume","Since start":"Since start","Your Score":"Your Score","Enable":"Enable","Enabling":"Enabling","%amount% CAKE":"%amount% CAKE","IFO Shopper: %title%":"IFO Shopper: %title%","%num% of total":"%num% of total","All estimated rates take into account this pool’s %fee%% performance fee":"All estimated rates take into account this pool’s %fee%% performance fee","Sorry, you didn’t contribute enough CAKE to meet the minimum threshold. You didn’t buy anything in this sale, but you can still reclaim your CAKE.":"Sorry, you didn’t contribute enough CAKE to meet the minimum threshold. You didn’t buy anything in this sale, but you can still reclaim your CAKE.","Only applies within 3 days of staking. Unstaking after 3 days will not include a fee. Timer resets every time you stake new CAKE in the pool.":"Only applies within 3 days of staking. Unstaking after 3 days will not include a fee. Timer resets every time you stake new CAKE in the pool.","Unstaking Fee":"Unstaking Fee","unstaking fee until":"unstaking fee until","unstaking fee if withdrawn within 72h":"unstaking fee if withdrawn within 72h","Unstaking fee: %fee%%":"Unstaking fee: %fee%%","Performance Fee":"Performance Fee","Compound: collect and restake CAKE into pool.":"Compound: collect and restake CAKE into pool.","Harvest: collect CAKE and send to wallet":"Harvest: collect CAKE and send to wallet","%position% Entered":"%position% Entered","Sort by":"Sort by","Expired":"Expired","Calculating":"Calculating","Next":"Next","Later":"Later","Up":"Up","Down":"Down","%multiplier% Payout":"%multiplier% Payout","Enter %direction%":"Enter %direction%","Prize Pool:":"Prize Pool:","Charts":"Charts","Your history":"Your history","All":"All","Collected":"Collected","Uncollected":"Uncollected","Round":"Round","Rounds":"Rounds","PNL":"PNL","Your Result":"Your Result","Your direction":"Your direction","Your position":"Your position","Lose":"Lose","Entry starts":"Entry starts","Locked Price":"Locked Price","Last Price":"Last Price","Closed Price":"Closed Price","Win":"Win","Opening Block":"Opening Block","Closing Block":"Closing Block","Total Value Locked (TVL)":"Total Value Locked (TVL)","Automatic restaking":"Automatic restaking","Manual CAKE":"Manual CAKE","Auto CAKE":"Auto CAKE","Auto CAKE Bounty":"Auto CAKE Bounty","Claim":"Claim","Any funds you stake in this pool will be automagically harvested and restaked (compounded) for you.":"Any funds you stake in this pool will be automagically harvested and restaked (compounded) for you.","Total staked":"Total staked","Just stake some tokens to earn.":"Just stake some tokens to earn.","High APR, low risk.":"High APR, low risk.","Stake LP tokens to earn.":"Stake LP tokens to earn.","Basic Sale":"Basic Sale","Everyone can only commit a limited amount, but may expect a higher return per token committed.":"Everyone can only commit a limited amount, but may expect a higher return per token committed.","Unlimited Sale":"Unlimited Sale","No limits on the amount you can commit. Additional fee applies when claiming.":"No limits on the amount you can commit. Additional fee applies when claiming.","Every person can only commit a limited amount, but may expect a higher return per token committed.":"Every person can only commit a limited amount, but may expect a higher return per token committed.","You didn’t participate in this sale!":"You didn’t participate in this sale!","Max. token entry":"Max. token entry","Max. CAKE entry":"Max. CAKE entry","How to Take Part":"How to Take Part","Activate your Profile":"Activate your Profile","You’ll need an active PancakeSwap Profile to take part in an IFO!":"You’ll need an active PancakeSwap Profile to take part in an IFO!","Profile Active!":"Profile Active!","Get CAKE Tokens":"Get CAKE Tokens","Stake CAKE and BNB in the liquidity pool to get LP tokens.":"Stake CAKE and BNB in the liquidity pool to get LP tokens.","You’ll spend them to buy IFO sale tokens.":"You’ll spend them to buy IFO sale tokens.","Commit CAKE":"Commit CAKE","When the IFO sales are live, you can “commit” your CAKE to buy the tokens being sold.":"When the IFO sales are live, you can “commit” your CAKE to buy the tokens being sold.","We recommend committing to the Basic Sale first, but you can do both if you like.":"We recommend committing to the Basic Sale first, but you can do both if you like.","Claim your tokens and achievement":"Claim your tokens and achievement","After the IFO sales finish, you can claim any IFO tokens that you bought, and any unspent CAKE tokens will be returned to your wallet.":"After the IFO sales finish, you can claim any IFO tokens that you bought, and any unspent CAKE tokens will be returned to your wallet.","This round’s closing transaction has been submitted to the blockchain, and is awaiting confirmation.":"This round’s closing transaction has been submitted to the blockchain, and is awaiting confirmation.","No prediction history available":"No prediction history available","If you are sure you should see history here, make sure you’re connected to the correct wallet and try again.":"If you are sure you should see history here, make sure you’re connected to the correct wallet and try again.","Last price from Chainlink Oracle":"Last price from Chainlink Oracle","%num% Points to Collect":"%num% Points to Collect","Net results":"Net results","Average return / round":"Average return / round","Average position entered / round":"Average position entered / round","Won":"Won","Lost":"Lost","Entered":"Entered","Connect your wallet to view your prediction history":"Connect your wallet to view your prediction history","Best round: %roundId%":"Best round: %roundId%","You’ve already staked the maximum amount you can stake in this pool!":"You’ve already staked the maximum amount you can stake in this pool!","Markets Paused":"Markets Paused","Prediction markets have been paused due to an error.":"Prediction markets have been paused due to an error.","All open positions have been canceled.":"All open positions have been canceled.","You can reclaim any funds entered into existing positions via the History tab on this page.":"You can reclaim any funds entered into existing positions via the History tab on this page.","Show History":"Show History","Welcome!":"Welcome!","This product is still in beta (testing) phase.":"This product is still in beta (testing) phase.","Before you use this product, make sure you fully understand the risks involved.":"Before you use this product, make sure you fully understand the risks involved.","Once you enter a position, you cannot cancel or adjust it.":"Once you enter a position, you cannot cancel or adjust it.","All losses are final.":"All losses are final.","I understand that I am using this product at my own risk. Any loss incurred due to my actions are my own responsibility.":"I understand that I am using this product at my own risk. Any loss incurred due to my actions are my own responsibility.","You’ll spend CAKE to buy IFO sale tokens.":"You’ll spend CAKE to buy IFO sale tokens.","You\'ll need an active PancakeSwap Profile to take part in an IFO!":"You\'ll need an active PancakeSwap Profile to take part in an IFO!","Your LP tokens committed":"Your LP tokens committed","Max. Committed":"Max. Committed","Total committed:":"Total committed:","Additional fee:":"Additional fee:","Learn more about %symbol%":"Learn more about %symbol%","Earned since your last action":"Earned since your last action","Commit ~%amount% %symbol% in total to earn!":"Commit ~%amount% %symbol% in total to earn!","Learn more about %title%":"Learn more about %title%","365d (APY)":"365d (APY)","%num%d":"%num%d","d":"d","h":"h","m":"m","Hide or show expandable content":"Hide or show expandable content","No tokens to stake":"No tokens to stake","Successfully claimed!":"Successfully claimed!","Unable to claim NFT":"Unable to claim NFT","Unable to claim NFT, please try again.":"Unable to claim NFT, please try again.","Error":"Error","1x %nftName% Collectible":"1x %nftName% Collectible","1x %nftName% NFT":"1x %nftName% NFT","NFT successfully transferred!":"NFT successfully transferred!","View Contract":"View Contract","View IFO Contract":"View IFO Contract","See Pair Info":"See Pair Info","Stake LP":"Stake LP","Multiplier":"Multiplier","Start Farming":"Start Farming","Farming":"Farming","Enable Farm":"Enable Farm","Search Farms":"Search Farms","To Top":"To Top","Total value of the funds in this farm’s liquidity pool":"Total value of the funds in this farm’s liquidity pool","The Multiplier represents the proportion of CAKE rewards each farm receives, as a proportion of the CAKE produced each block.":"The Multiplier represents the proportion of CAKE rewards each farm receives, as a proportion of the CAKE produced each block.","For example, if a 1x farm received 1 CAKE per block, a 40x farm would receive 40 CAKE per block.":"For example, if a 1x farm received 1 CAKE per block, a 40x farm would receive 40 CAKE per block.","This amount is already included in all APR calculations for the farm.":"This amount is already included in all APR calculations for the farm.","Hot":"Hot","Success!":"Success!","You have successfully claimed your rewards.":"You have successfully claimed your rewards.","Commit":"Commit","If you don’t commit enough CAKE, you may not receive any IFO tokens at all and will only receive a full refund of your CAKE.":"If you don’t commit enough CAKE, you may not receive any IFO tokens at all and will only receive a full refund of your CAKE.","CAKE required":"CAKE required","Get CAKE, or make sure your tokens aren’t staked somewhere else.":"Get CAKE, or make sure your tokens aren’t staked somewhere else.","Funds to raise:":"Funds to raise:","Price per %symbol%:":"Price per %symbol%:","Price per %symbol% with fee:":"Price per %symbol% with fee:","You need an active PancakeSwap Profile to take part in an IFO!":"You need an active PancakeSwap Profile to take part in an IFO!","Achievement":"Achievement","Commit ~%minLp% LP in total to earn!":"Commit ~%minLp% LP in total to earn!","What’s the difference between a Basic Sale and Unlimited Sale?":"What’s the difference between a Basic Sale and Unlimited Sale?","In the Basic Sale, every user can commit a maximum of about 100 USD worth of CAKE. We calculate the maximum CAKE amount about 30 minutes before each IFO. The Basic Sale has no participation fee.":"In the Basic Sale, every user can commit a maximum of about 100 USD worth of CAKE. We calculate the maximum CAKE amount about 30 minutes before each IFO. The Basic Sale has no participation fee.","In the Unlimited Sale, there’s no limit to the amount of CAKE you can commit. However, there’s a fee for participation: see below.":"In the Unlimited Sale, there’s no limit to the amount of CAKE you can commit. However, there’s a fee for participation: see below.","Which sale should I commit to? Can I do both?":"Which sale should I commit to? Can I do both?","You can choose one or both at the same time! If you’re only committing a small amount, we recommend the Basic Sale first. Just remember you need a PancakeSwap Profile in order to participate.":"You can choose one or both at the same time! If you’re only committing a small amount, we recommend the Basic Sale first. Just remember you need a PancakeSwap Profile in order to participate.","How much is the participation fee?":"How much is the participation fee?","There’s only a participation fee for the Unlimited Sale: there’s no fee for the Basic Sale.":"There’s only a participation fee for the Unlimited Sale: there’s no fee for the Basic Sale.","The fee will start at 1%.":"The fee will start at 1%.","The 1% participation fee decreases in cliffs, based on the percentage of overflow from the “Unlimited” portion of the sale.":"The 1% participation fee decreases in cliffs, based on the percentage of overflow from the “Unlimited” portion of the sale.","Where does the participation fee go?":"Where does the participation fee go?","The CAKE from the participation fee will be thrown into the weekly token burn.":"The CAKE from the participation fee will be thrown into the weekly token burn.","How can I get an achievement for participating in the IFO?":"How can I get an achievement for participating in the IFO?","You need to contribute a minimum of about 10 USD worth of CAKE to either sale.":"You need to contribute a minimum of about 10 USD worth of CAKE to either sale.","You can contribute to one or both, it doesn’t matter. Only your overall contribution is counted for the achievement.":"You can contribute to one or both, it doesn’t matter. Only your overall contribution is counted for the achievement.","The Lottery Is Changing!":"The Lottery Is Changing!","Come back soon!":"Come back soon!","Staked (compounding)":"Staked (compounding)","Contract Enabled":"Contract Enabled","You can now stake in the %symbol% vault!":"You can now stake in the %symbol% vault!","You can now stake in the %symbol% pool!":"You can now stake in the %symbol% pool!","Please try again. Confirm the transaction and make sure you are paying enough gas!":"Please try again. Confirm the transaction and make sure you are paying enough gas!","Start earning":"Start earning","Recent CAKE profit":"Recent CAKE profit","Unstaked!":"Unstaked!","Unstaked":"Unstaked","Staked!":"Staked!","Your funds have been staked in the pool":"Your funds have been staked in the pool","Your earnings have also been harvested to your wallet":"Your earnings have also been harvested to your wallet","%error% - Please try again.":"%error% - Please try again.","Stake in Pool":"Stake in Pool","Balance: %balance%":"Balance: %balance%","%day%d : %hour%h : %minute%m":"%day%d : %hour%h : %minute%m","%day%d %hour%h %minute%m":"%day%d %hour%h %minute%m","Subtracted automatically from each yield harvest and burned.":"Subtracted automatically from each yield harvest and burned.","Blocks":"Blocks","Add to Metamask":"Add to Metamask","Compounded":"Compounded","Canceled":"Canceled","Please try again and confirm the transaction.":"Please try again and confirm the transaction.","Harvested":"Harvested","Close Window":"Close Window","Insufficient %symbol% balance":"Insufficient %symbol% balance","You’ll need %symbol% to stake in this pool!":"You’ll need %symbol% to stake in this pool!","Buy some %symbol%, or make sure your %symbol% isn’t in another pool or LP.":"Buy some %symbol%, or make sure your %symbol% isn’t in another pool or LP.","Buy":"Buy","Locate Assets":"Locate Assets","%symbol% required":"%symbol% required","Your %symbol% earnings have also been harvested to your wallet!":"Your %symbol% earnings have also been harvested to your wallet!","Your %symbol% funds have been staked in the pool!":"Your %symbol% funds have been staked in the pool!","Max stake for this pool: %amount% %token%":"Max stake for this pool: %amount% %token%","Maximum total stake: %amount% %token%":"Maximum total stake: %amount% %token%","APY includes compounding, APR doesn’t. This pool’s CAKE is compounded automatically, so we show APY.":"APY includes compounding, APR doesn’t. This pool’s CAKE is compounded automatically, so we show APY.","This pool’s rewards aren’t compounded automatically, so we show APR":"This pool’s rewards aren’t compounded automatically, so we show APR","Auto":"Auto","Manual":"Manual","Earn CAKE, stake CAKE":"Earn CAKE, stake CAKE","This bounty is given as a reward for providing a service to other users.":"This bounty is given as a reward for providing a service to other users.","Whenever you successfully claim the bounty, you’re also helping out by activating the Auto CAKE Pool’s compounding function for everyone.":"Whenever you successfully claim the bounty, you’re also helping out by activating the Auto CAKE Pool’s compounding function for everyone.","Auto-Compound Bounty: %fee%% of all Auto CAKE pool users pending yield":"Auto-Compound Bounty: %fee%% of all Auto CAKE pool users pending yield","Bounty collected!":"Bounty collected!","CAKE bounty has been sent to your wallet.":"CAKE bounty has been sent to your wallet.","Could not be collected":"Could not be collected","There may be an issue with your transaction, or another user claimed the bounty first.":"There may be an issue with your transaction, or another user claimed the bounty first.","Claim Bounty":"Claim Bounty","You’ll claim":"You’ll claim","Pool total pending yield":"Pool total pending yield","Bounty":"Bounty","What’s this?":"What’s this?","Help":"Help","Syrup Pools":"Syrup Pools","Best round: #%roundId%":"Best round: #%roundId%","View Reclaimed & Won":"View Reclaimed & Won","This round was automatically canceled due to an error. If you entered a position, please reclaim your funds below.":"This round was automatically canceled due to an error. If you entered a position, please reclaim your funds below.","Round History":"Round History","Your History":"Your History","Filter":"Filter","Starting Soon":"Starting Soon","Live Now":"Live Now","Reclaim":"Reclaim","This page can’t be displayed right now due to an error. Please check back soon.":"This page can’t be displayed right now due to an error. Please check back soon.","Round Canceled":"Round Canceled","Learn More":"Learn More","Payout":"Payout","%position% position entered":"%position% position entered","Enter UP":"Enter UP","Enter DOWN":"Enter DOWN","An error occurred, unable to enter your position":"An error occurred, unable to enter your position","Set Position":"Set Position","Insufficient BNB balance":"Insufficient BNB balance","A minimum amount of %num% %token% is required":"A minimum amount of %num% %token% is required","Enter an amount":"Enter an amount","You won’t be able to remove or change your position once you enter it.":"You won’t be able to remove or change your position once you enter it.","Prize Pool":"Prize Pool","Winnings collected!":"Winnings collected!","Your prizes have been sent to your wallet":"Your prizes have been sent to your wallet","Collecting":"Collecting","Closing":"Closing","Position reclaimed!":"Position reclaimed!","Reclaim Position":"Reclaim Position","This Product is in beta.":"This Product is in beta.","I understand that I am using this product at my own risk. Any losses incurred due to my actions are my own responsibility.":"I understand that I am using this product at my own risk. Any losses incurred due to my actions are my own responsibility.","I understand that this product is still in beta. I am participating at my own risk":"I understand that this product is still in beta. I am participating at my own risk","Continue":"Continue","Points Collected!":"Points Collected!","%num% points":"%num% points","Cost to update:":"Cost to update:","Cost to reactivate:":"Cost to reactivate:","Profile Updated!":"Profile Updated!","Choose a new Collectible to use as your profile pic.":"Choose a new Collectible to use as your profile pic.","Sorry! You don’t have any eligible Collectibles in your wallet to use!":"Sorry! You don’t have any eligible Collectibles in your wallet to use!","Make sure you have a Pancake Collectible in your wallet and try again!":"Make sure you have a Pancake Collectible in your wallet and try again!","Edit Profile":"Edit Profile","Change Profile Pic":"Change Profile Pic","Remove Profile Pic":"Remove Profile Pic","Profile Paused!":"Profile Paused!","This will suspend your profile and send your Collectible back to your wallet":"This will suspend your profile and send your Collectible back to your wallet","While your profile is suspended, you won\'t be able to earn points, but your achievements and points will stay associated with your profile":"While your profile is suspended, you won\'t be able to earn points, but your achievements and points will stay associated with your profile","Cost to reactivate in the future: %cost% CAKE":"Cost to reactivate in the future: %cost% CAKE","%minimum% CAKE required to change profile pic":"%minimum% CAKE required to change profile pic","Reactivate Profile":"Reactivate Profile","No achievements yet!":"No achievements yet!","Claim your Gift!":"Claim your Gift!","Thank you for being a day-one user of Pancake Profiles!":"Thank you for being a day-one user of Pancake Profiles!","If you haven\'t already noticed, we made a mistake and the starter bunny you chose got mixed up and changed into another bunny. Oops!":"If you haven\'t already noticed, we made a mistake and the starter bunny you chose got mixed up and changed into another bunny. Oops!","To make it up to you, we’ll refund you the full 4 CAKE it cost to make your bunny.":"To make it up to you, we’ll refund you the full 4 CAKE it cost to make your bunny.","We’re also preparing an all-new collectible for you to claim (for free!) in the near future.":"We’re also preparing an all-new collectible for you to claim (for free!) in the near future.","Once you claim the refund, you can make another account with another wallet, mint a new bunny, and send it to your main account via the NFT page.":"Once you claim the refund, you can make another account with another wallet, mint a new bunny, and send it to your main account via the NFT page.","Claim Your CAKE":"Claim Your CAKE","Pancake Collectibles":"Pancake Collectibles","Pancake Collectibles are special ERC-721 NFTs that can be used on the PancakeSwap platform.":"Pancake Collectibles are special ERC-721 NFTs that can be used on the PancakeSwap platform.","NFTs in this user’s wallet that aren’t approved Pancake Collectibles won’t be shown here.":"NFTs in this user’s wallet that aren’t approved Pancake Collectibles won’t be shown here.","No NFTs Found":"No NFTs Found","See all approved Pancake Collectibles":"See all approved Pancake Collectibles","Coming Soon!":"Coming Soon!","Profile created!":"Profile created!","Submitting NFT to contract and confirming User Name and Team.":"Submitting NFT to contract and confirming User Name and Team.","Cost":"Cost","Your Profile":"Your Profile","Check your stats and collect achievements":"Check your stats and collect achievements","You’ve got a gift to claim!":"You’ve got a gift to claim!","Show off your stats and collectibles with your unique profile":"Show off your stats and collectibles with your unique profile","Total cost: 1.5 CAKE":"Total cost: 1.5 CAKE","Cost: %num% CAKE":"Cost: %num% CAKE","You can change your profile pic later if you get another approved Pancake Collectible.":"You can change your profile pic later if you get another approved Pancake Collectible.","Every profile starts by making a “starter” collectible (NFT).":"Every profile starts by making a “starter” collectible (NFT).","We couldn’t find any Pancake Collectibles in your wallet.":"We couldn’t find any Pancake Collectibles in your wallet.","You need a Pancake Collectible to finish setting up your profile. If you sold or transferred your starter collectible to another wallet, you’ll need to get it back or acquire a new one somehow. You can’t make a new starter with this wallet address.":"You need a Pancake Collectible to finish setting up your profile. If you sold or transferred your starter collectible to another wallet, you’ll need to get it back or acquire a new one somehow. You can’t make a new starter with this wallet address.","The collectible you\'ve chosen will be locked in a smart contract while it’s being used as your profile picture. Don\'t worry - you\'ll be able to get it back at any time.":"The collectible you\'ve chosen will be locked in a smart contract while it’s being used as your profile picture. Don\'t worry - you\'ll be able to get it back at any time.","It won’t be possible to undo the choice you make for the foreseeable future!":"It won’t be possible to undo the choice you make for the foreseeable future!","There’s currently no big difference between teams, and no benefit of joining one team over another for now. So pick whichever one you like!":"There’s currently no big difference between teams, and no benefit of joining one team over another for now. So pick whichever one you like!","Your name must be at least 3 and at most 15 standard letters and numbers long. You can’t change this once you click Confirm.":"Your name must be at least 3 and at most 15 standard letters and numbers long. You can’t change this once you click Confirm.","Unable to verify username":"Unable to verify username","Created %dateCreated% ago":"Created %dateCreated% ago","Paused":"Paused","April":"April","Easter Battle":"Easter Battle","$120,000 in Prizes!":"$120,000 in Prizes!","Compete with other teams to win CAKE, collectible NFTs, achievements & more!":"Compete with other teams to win CAKE, collectible NFTs, achievements & more!","Now Live!":"Now Live!","Calculating prizes":"Calculating prizes","I want to Battle!":"I want to Battle!","Trade Now":"Trade Now","Claim period over":"Claim period over","Prizes Claimed!":"Prizes Claimed!","Nothing to claim":"Nothing to claim","Registered!":"Registered!","Congratulations! You won":"Congratulations! You won","Collectible NFT":"Collectible NFT","All prizes will be sent directly to your wallet and user account.":"All prizes will be sent directly to your wallet and user account.","How Can I Join?":"How Can I Join?","Set up your":"Set up your","Pancake Profile":"Pancake Profile","then register for the competition by clicking “I WANT TO BATTLE” button above.":"then register for the competition by clicking “I WANT TO BATTLE” button above.","Battle Time":"Battle Time","Trade BNB/BUSD, CAKE/BNB, ETH/BNB and BTCB/BNB during the battle period to raise both your and your team’s score. See the Rules section below for more.":"Trade BNB/BUSD, CAKE/BNB, ETH/BNB and BTCB/BNB during the battle period to raise both your and your team’s score. See the Rules section below for more.","Prize Claim":"Prize Claim","Teams and traders will be ranked in the order of trading volume. Collect your prizes and celebrate!":"Teams and traders will be ranked in the order of trading volume. Collect your prizes and celebrate!","Registration starting April 5":"Registration starting April 5","Tier":"Tier","Token Prizes (Split)":"Token Prizes (Split)","Gold":"Gold","Silver":"Silver","Bronze":"Bronze","Purple":"Purple","Teal":"Teal","#%team% Team":"#%team% Team","Prizes by Team":"Prizes by Team","Higher trading volume = higher rank!":"Higher trading volume = higher rank!","Prizes to be distributed in CAKE and shared by all members of a tier. CAKE price in USD to be determined on the day of distribution. Details below.":"Prizes to be distributed in CAKE and shared by all members of a tier. CAKE price in USD to be determined on the day of distribution. Details below.","Every eligible participant will win prizes at the end of the competition.":"Every eligible participant will win prizes at the end of the competition.","The better your team performs, the better prizes you will get!":"The better your team performs, the better prizes you will get!","The final winning team will be the team with the highest total volume score at the end of the competition period.":"The final winning team will be the team with the highest total volume score at the end of the competition period.","Make a profile!":"Make a profile!","It looks like you’ve disabled your account by removing your Pancake Collectible (NFT) profile picture.":"It looks like you’ve disabled your account by removing your Pancake Collectible (NFT) profile picture.","Reactivate your profile!":"Reactivate your profile!","You need to reactivate your profile by replacing your profile picture in order to join the competition.":"You need to reactivate your profile by replacing your profile picture in order to join the competition.","Go to my profile":"Go to my profile","You have registered for the competition!":"You have registered for the competition!","Registering for the competition will make your wallet address publicly visible on the leaderboard.":"Registering for the competition will make your wallet address publicly visible on the leaderboard.","This decision cannot be reversed.":"This decision cannot be reversed.","I understand that my address may be displayed publicly throughout the competition.":"I understand that my address may be displayed publicly throughout the competition.","Eligible trading pairs":"Eligible trading pairs","Only trades on SANTOS/BNB, PORTO/BNB, LAZIO/BNB, SANTOS/BUSD, PORTO/BUSD, LAZIO/BUSD, CAKE/BNB and CAKE/BUSD pairs will be included in volume calculations.":"Only trades on SANTOS/BNB, PORTO/BNB, LAZIO/BNB, SANTOS/BUSD, PORTO/BUSD, LAZIO/BUSD, CAKE/BNB and CAKE/BUSD pairs will be included in volume calculations.","Calculating team ranks and winners":"Calculating team ranks and winners","Team ranks are calculated by the total combined volume of the top 500 members of each respective team.":"Team ranks are calculated by the total combined volume of the top 500 members of each respective team.","Prize distribution":"Prize distribution","Prizes to be distributed in CAKE, LAZIO, PORTO and SANTOS in a distribution of 3:1:1:1 and shared by all members of each respective tier.":"Prizes to be distributed in CAKE, LAZIO, PORTO and SANTOS in a distribution of 3:1:1:1 and shared by all members of each respective tier.","The price of token prizes (CAKE, LAZIO, PORTO and SANTOS) in USD will be determined as per their BUSD pair price during the tally period.":"The price of token prizes (CAKE, LAZIO, PORTO and SANTOS) in USD will be determined as per their BUSD pair price during the tally period.","Every participant will win at least one prize at the end of the competition.":"Every participant will win at least one prize at the end of the competition.","Fine print":"Fine print","In the event of a disagreement concerning the final winning team or rank, PancakeSwap will have the final say.":"In the event of a disagreement concerning the final winning team or rank, PancakeSwap will have the final say.","PancakeSwap can and will disqualify any team or specific members that are proven to have taken malicious action or attempt to “cheat” in any way.":"PancakeSwap can and will disqualify any team or specific members that are proven to have taken malicious action or attempt to “cheat” in any way.","Can I join the battle after it starts?":"Can I join the battle after it starts?","Sorry, no. You need to register during the registration period, before the start of the event.":"Sorry, no. You need to register during the registration period, before the start of the event.","How do I know if I successfully registered?":"How do I know if I successfully registered?","At the top of the page, the text in the button “I Want to Battle” will change to “Registered”.":"At the top of the page, the text in the button “I Want to Battle” will change to “Registered”.","How can I see my current rank?":"How can I see my current rank?","Check Your Score section on the event page. You’ll need to connect your wallet, of course.":"Check Your Score section on the event page. You’ll need to connect your wallet, of course.","How do I claim my prize(s)?":"How do I claim my prize(s)?","After the battle ends, visit the event page and click the “Claim Prizes” button in the top section or in “Your Score” section. Once you claim your prizes successfully, the button text will change to “Prizes Claimed”.":"After the battle ends, visit the event page and click the “Claim Prizes” button in the top section or in “Your Score” section. Once you claim your prizes successfully, the button text will change to “Prizes Claimed”.","Trade to increase your rank":"Trade to increase your rank","Eligible pairs: SANTOS/BNB, PORTO/BNB, LAZIO/BNB, SANTOS/BUSD, PORTO/BUSD, LAZIO/BUSD, CAKE/BNB and CAKE/BUSD":"Eligible pairs: SANTOS/BNB, PORTO/BNB, LAZIO/BNB, SANTOS/BUSD, PORTO/BUSD, LAZIO/BUSD, CAKE/BNB and CAKE/BUSD","Play as a team":"Play as a team","The higher your team’s rank, the better your prizes":"The higher your team’s rank, the better your prizes","Top Traders":"Top Traders","Since start of the competition":"Since start of the competition","Show More":"Show More","Share Score":"Share Score","Your tier: gold":"Your tier: gold","Love, The Chefs x":"Love, The Chefs x","HECK YEAH!":"HECK YEAH!","Next tier":"Next tier","to become #%rank% in team":"to become #%rank% in team","#%global% Overall":"#%global% Overall","Token Prizes":"Token Prizes","Share Your Score":"Share Your Score","Brag to your friends and annoy your rivals with your custom scorecard!":"Brag to your friends and annoy your rivals with your custom scorecard!","Download Image":"Download Image","Screenshot or press & hold the image to share!":"Screenshot or press & hold the image to share!","Block %num%":"Block %num%","%max% %symbol% Available":"%max% %symbol% Available","Profile Pic":"Profile Pic","Transfer":"Transfer","Congratulations!":"Congratulations!","You won a collectible!":"You won a collectible!","Claim now":"Claim now","Claiming...":"Claiming...","NFT in wallet":"NFT in wallet","Trade in your NFT for CAKE, or just keep it for your collection.":"Trade in your NFT for CAKE, or just keep it for your collection.","No NFTs to claim":"No NFTs to claim","You have no NFTs to claim at this time, but you can still see the NFTs in this series below.":"You have no NFTs to claim at this time, but you can still see the NFTs in this series below.","Please wait...":"Please wait...","The claiming period hasn’t started yet. Check back soon.":"The claiming period hasn’t started yet. Check back soon.","Please enter a valid wallet address":"Please enter a valid wallet address","Unable to transfer NFT":"Unable to transfer NFT","Transfer NFT":"Transfer NFT","Transferring":"Transferring","Receiving address":"Receiving address","Paste address":"Paste address","Could not retrieve CAKE costs for profile":"Could not retrieve CAKE costs for profile","An error occurred approving transaction":"An error occurred approving transaction","Provider Error":"Provider Error","No provider was found":"No provider was found","Authorization Error":"Authorization Error","Please authorize to access your account":"Please authorize to access your account","Unable to find connector":"Unable to find connector","The connector config is wrong":"The connector config is wrong","You have contributed %amount% CAKE to this IFO!":"You have contributed %amount% CAKE to this IFO!","You’ll need CAKE tokens to participate in the IFO!":"You’ll need CAKE tokens to participate in the IFO!","Your %symbol% committed":"Your %symbol% committed","Your %symbol% RECLAIMED":"Your %symbol% RECLAIMED","Your %symbol% TO RECLAIM":"Your %symbol% TO RECLAIM","%symbol% received":"%symbol% received","%symbol% to receive":"%symbol% to receive","Stories were told, and songs were sung about Chef Mixie’s pancakes and her big Syrup gun.":"Stories were told, and songs were sung about Chef Mixie’s pancakes and her big Syrup gun.","Eggscellent! Celebrating Syrup Storm winning the Easter Battle!":"Eggscellent! Celebrating Syrup Storm winning the Easter Battle!","Melting Easter eggs and melting hearts!":"Melting Easter eggs and melting hearts!","Watch out for Flipsie’s spatula smash!":"Watch out for Flipsie’s spatula smash!","Do you like chocolate with your syrup? Go long!":"Do you like chocolate with your syrup? Go long!","Happy Niu Year! This bunny’s excited for the year of the bull (market!)":"Happy Niu Year! This bunny’s excited for the year of the bull (market!)","Oopsie daisy! Hiccup\'s had a bit of an accident. Poor little fella.":"Oopsie daisy! Hiccup\'s had a bit of an accident. Poor little fella.","Aww, looks like eating pancakes all day is not easy. Sweet dreams!":"Aww, looks like eating pancakes all day is not easy. Sweet dreams!","Sunny is always cheerful when there are pancakes around. Smile!":"Sunny is always cheerful when there are pancakes around. Smile!","Don\'t let that dopey smile deceive you... Churro\'s a master CAKE chef!":"Don\'t let that dopey smile deceive you... Churro\'s a master CAKE chef!","Nommm... Oh hi, I\'m just meditating on the meaning of CAKE.":"Nommm... Oh hi, I\'m just meditating on the meaning of CAKE.","Three guesses what\'s put that twinkle in those eyes! (Hint: it\'s CAKE)":"Three guesses what\'s put that twinkle in those eyes! (Hint: it\'s CAKE)","These bunnies love nothing more than swapping pancakes. Especially on BSC.":"These bunnies love nothing more than swapping pancakes. Especially on BSC.","It\'s raining syrup on this bunny, but he doesn\'t seem to mind. Can you blame him?":"It\'s raining syrup on this bunny, but he doesn\'t seem to mind. Can you blame him?","These bunnies like their pancakes with blueberries. What\'s your favorite topping?":"These bunnies like their pancakes with blueberries. What\'s your favorite topping?","Love makes the world go \'round... but so do pancakes. And these bunnies know it.":"Love makes the world go \'round... but so do pancakes. And these bunnies know it.","It’s sparkling syrup, pancakes, and even lottery tickets! This bunny really loves it.":"It’s sparkling syrup, pancakes, and even lottery tickets! This bunny really loves it.","The storm\'s a-comin! Watch out! These bulls are stampeding in a syrupy surge!":"The storm\'s a-comin! Watch out! These bulls are stampeding in a syrupy surge!","The flippening is coming. Don\'t get in these bunnies\' way, or you\'ll get flipped, too!":"The flippening is coming. Don\'t get in these bunnies\' way, or you\'ll get flipped, too!","Can you stand the heat? Stay out of the kitchen or you might get burned to a crisp!":"Can you stand the heat? Stay out of the kitchen or you might get burned to a crisp!","%ratio%% of total sale":"%ratio%% of total sale","Your %symbol% earnings have been sent to your wallet!":"Your %symbol% earnings have been sent to your wallet!","Your %symbol% earnings have been re-invested into the pool!":"Your %symbol% earnings have been re-invested into the pool!","Stake %symbol%":"Stake %symbol%","You must harvest and compound your earnings from this pool manually.":"You must harvest and compound your earnings from this pool manually.","Register":"Register","Contribute %symbol%":"Contribute %symbol%","Volume":"Volume","%symbol% per $1,000":"%symbol% per $1,000","Prediction (BETA)":"Prediction (BETA)","Collectibles":"Collectibles","Team Battle":"Team Battle","Teams & Profile":"Teams & Profile","Leaderboard":"Leaderboard","Tokens":"Tokens","Contact":"Contact","Merch":"Merch","LP Migration":"LP Migration","V1 Liquidity (Old)":"V1 Liquidity (Old)","Claim Collectible":"Claim Collectible","The higher your team’s rank, the better your prizes!":"The higher your team’s rank, the better your prizes!","Get Ready":"Get Ready","Earn up to %highestApr% APR in Farms":"Earn up to %highestApr% APR in Farms","Earn %assets% in Pools":"Earn %assets% in Pools","Activate PancakeSwap Profile to take part in next IFO‘s!":"Activate PancakeSwap Profile to take part in next IFO‘s!","Trading Battle":"Trading Battle","Prediction":"Prediction","Initial Farm Offering":"Initial Farm Offering","An error occurred confirming transaction":"An error occurred confirming transaction","Successfully Enabled!":"Successfully Enabled!","You can now participate in the %symbol% IFO.":"You can now participate in the %symbol% IFO.","Team Ranks":"Team Ranks","Prizes":"Prizes","Rules":"Rules","Entry":"Entry","Enable pool":"Enable pool","Start staking":"Start staking","Search Pools":"Search Pools","Ends in":"Ends in","Starts in":"Starts in","Total amount of %symbol% staked in this pool":"Total amount of %symbol% staked in this pool","These pools are no longer distributing rewards. Please unstake your tokens.":"These pools are no longer distributing rewards. Please unstake your tokens.","Max. stake per user":"Max. stake per user","You have claimed your rewards!":"You have claimed your rewards!","Already Collected":"Already Collected","Predictions Now Live":"Predictions Now Live","Beta Version":"Beta Version","Over %amount% in BNB won so far":"Over %amount% in BNB won so far","Try Now":"Try Now","New":"New","LP rewards: 0.17% trading fees, distributed proportionally among LP token holders.":"LP rewards: 0.17% trading fees, distributed proportionally among LP token holders.","CAKE + Fees":"CAKE + Fees","Prediction Markets":"Prediction Markets","Now Live":"Now Live","in Beta":"in Beta","Ended %date%":"Ended %date%","Starts %date%":"Starts %date%","Ends %date%":"Ends %date%","Closed":"Closed","Vote Now":"Vote Now","Soon":"Soon","No proposals found":"No proposals found","Creator":"Creator","Proposals":"Proposals","Make a Proposal":"Make a Proposal","Have your say in the future of the PancakeSwap Ecosystem":"Have your say in the future of the PancakeSwap Ecosystem","Got a suggestion?":"Got a suggestion?","Community proposals are a great way to see how the community feels about your ideas.":"Community proposals are a great way to see how the community feels about your ideas.","They won\'t necessarily be implemented if the community votes successful, but suggestions with a lot of community support may be made into Core proposals.":"They won\'t necessarily be implemented if the community votes successful, but suggestions with a lot of community support may be made into Core proposals.","%field% is required":"%field% is required","Please create a minimum of %num% choices":"Please create a minimum of %num% choices","Choices must not be empty":"Choices must not be empty","Please select a valid date":"Please select a valid date","Please select a valid time":"Please select a valid time","End date must be after the start date":"End date must be after the start date","Invalid snapshot":"Invalid snapshot","Proposal created!":"Proposal created!","Voted":"Voted","Time":"Time","Input choice text":"Input choice text","Start Time":"Start Time","Back to Vote Overview":"Back to Vote Overview","Voting For":"Voting For","Your Voting Power":"Your Voting Power","Confirm Vote":"Confirm Vote","Add Choice":"Add Choice","Title":"Title","Content":"Content","End Date":"End Date","Start Date":"Start Date","Actions":"Actions","End Time":"End Time","Publish":"Publish","No votes found":"No votes found","Vote Weight":"Vote Weight","Decision":"Decision","Voter":"Voter","Votes (%count%)":"Votes (%count%)","Snapshot":"Snapshot","Choices":"Choices","Unable to sign payload":"Unable to sign payload","Identifier":"Identifier","Current Results":"Current Results","Vote cast!":"Vote cast!","Are you sure you want to vote for the above choice? This action cannot be undone.":"Are you sure you want to vote for the above choice? This action cannot be undone.","Tip: write in Markdown!":"Tip: write in Markdown!","Cast your vote":"Cast your vote","Preview":"Preview","%total% Votes":"%total% Votes","Cast Vote":"Cast Vote","See All":"See All","Your voting power is determined by the amount of CAKE you held at the block detailed below. CAKE held in other places does not contribute to your voting power.":"Your voting power is determined by the amount of CAKE you held at the block detailed below. CAKE held in other places does not contribute to your voting power.","Your Cake Held Now":"Your Cake Held Now","Wallet":"Wallet","Manual CAKE Pool":"Manual CAKE Pool","Auto CAKE Pool":"Auto CAKE Pool","CAKE BNB LP":"CAKE BNB LP","Other Syrup Pools":"Other Syrup Pools","Voting Power":"Voting Power","You need at least %count% voting power to publish a proposal.":"You need at least %count% voting power to publish a proposal.","Check voting power":"Check voting power","Amount to collect":"Amount to collect","Your winnings":"Your winnings","Includes your original position and your winnings, minus the %fee% fee.":"Includes your original position and your winnings, minus the %fee% fee.","Your funds have been staked in the farm":"Your funds have been staked in the farm","Base APR (CAKE yield only)":"Base APR (CAKE yield only)","View/Edit Numbers":"View/Edit Numbers","Edit Numbers":"Edit Numbers","Total cost":"Total cost","Numbers are randomized, with no duplicates among your tickets. Tap a number to edit it. Available digits: 0-9":"Numbers are randomized, with no duplicates among your tickets. Tap a number to edit it. Available digits: 0-9","Randomize":"Randomize","Confirm and buy":"Confirm and buy","Go back":"Go back","Duplicate":"Duplicate","Get your tickets now!":"Get your tickets now!","Finished Rounds":"Finished Rounds","Only showing data for Lottery V2":"Only showing data for Lottery V2","Connect your wallet":"Connect your wallet","to check if you\'ve won":"to check if you\'ve won","No prizes to collect":"No prizes to collect","Better luck next time!":"Better luck next time!","Why not play again":"Why not play again","Are you a winner?":"Are you a winner?","Checking":"Checking","Check Now":"Check Now","On sale soon!":"On sale soon!","Date":"Date","Your Tickets":"Your Tickets","in prizes!":"in prizes!","Tickets on sale soon":"Tickets on sale soon","The PancakeSwap Lottery":"The PancakeSwap Lottery","All History":"All History","Winning Number":"Winning Number","tickets":"tickets","Prize pot":"Prize pot","Total players this round":"Total players this round","Draw":"Draw","Next Draw":"Next Draw","Your tickets":"Your tickets","Match the winning number in the same order to share prizes.":"Match the winning number in the same order to share prizes.","Current prizes up for grabs:":"Current prizes up for grabs:","Burn":"Burn","Match all %numberMatch%":"Match all %numberMatch%","Match first %numberMatch%":"Match first %numberMatch%","Matched first":"Matched first","Drawn":"Drawn","Connect your wallet to check your history":"Connect your wallet to check your history","Buy tickets for the next round!":"Buy tickets for the next round!","No lottery history found":"No lottery history found","Contract enabled - you can now purchase tickets":"Contract enabled - you can now purchase tickets","Lottery tickets purchased!":"Lottery tickets purchased!","The maximum number of tickets you can buy in one transaction is %maxTickets%":"The maximum number of tickets you can buy in one transaction is %maxTickets%","Bulk discount":"Bulk discount","Buy Instantly":"Buy Instantly","\\"Buy Instantly\\" chooses random numbers, with no duplicates among your tickets. Prices are set before each round starts, equal to $5 at that time. Purchases are final.":"\\"Buy Instantly\\" chooses random numbers, with no duplicates among your tickets. Prices are set before each round starts, equal to $5 at that time. Purchases are final.","Prizes Collected!":"Prizes Collected!","Your CAKE prizes for round %lotteryId% have been sent to your wallet":"Your CAKE prizes for round %lotteryId% have been sent to your wallet","Claim %claimNum% of %claimTotal% for round %lotteryId% was successful. Please confirm the next transaction":"Claim %claimNum% of %claimTotal% for round %lotteryId% was successful. Please confirm the next transaction","Claiming":"Claiming","Tickets must match the winning number in the exact same order, starting from the first digit.":"Tickets must match the winning number in the exact same order, starting from the first digit.","If the winning number is “123456”:":"If the winning number is “123456”:","“120000” matches the first 2 digits.":"“120000” matches the first 2 digits.","“000006” matches the last digit, but since the first five digits are wrong, it doesn’t win any prizes.":"“000006” matches the last digit, but since the first five digits are wrong, it doesn’t win any prizes.","Winning number":"Winning number","Total tickets":"Total tickets","Winning tickets":"Winning tickets","until the draw":"until the draw","Winners announced in":"Winners announced in","Tickets on sale in":"Tickets on sale in","to check if you\'ve won!":"to check if you\'ve won!","Why didn\'t I win?":"Why didn\'t I win?","How to Play":"How to Play","If the digits on your tickets match the winning numbers in the correct order, you win a portion of the prize pool.":"If the digits on your tickets match the winning numbers in the correct order, you win a portion of the prize pool.","Simple!":"Simple!","Prices are set when the round starts, equal to 5 USD in CAKE per ticket.":"Prices are set when the round starts, equal to 5 USD in CAKE per ticket.","Wait for the Draw":"Wait for the Draw","There are two draws every day: one every 12 hours.":"There are two draws every day: one every 12 hours.","Check for Prizes":"Check for Prizes","Once the round’s over, come back to the page and check to see if you’ve won!":"Once the round’s over, come back to the page and check to see if you’ve won!","Step %number%":"Step %number%","Winning Criteria":"Winning Criteria","The digits on your ticket must match in the correct order to win.":"The digits on your ticket must match in the correct order to win.","Here’s an example lottery draw, with two tickets, A and B.":"Here’s an example lottery draw, with two tickets, A and B.","Ticket A: The first 3 digits and the last 2 digits match, but the 4th digit is wrong, so this ticket only wins a “Match first 3” prize.":"Ticket A: The first 3 digits and the last 2 digits match, but the 4th digit is wrong, so this ticket only wins a “Match first 3” prize.","A":"A","Ticket B: Even though the last 5 digits match, the first digit is wrong, so this ticket doesn’t win a prize.":"Ticket B: Even though the last 5 digits match, the first digit is wrong, so this ticket doesn’t win a prize.","B":"B","Prize Funds":"Prize Funds","The prizes for each lottery round come from three sources:":"The prizes for each lottery round come from three sources:","Ticket Purchases":"Ticket Purchases","100% of the CAKE paid by people buying tickets that round goes back into the prize pools.":"100% of the CAKE paid by people buying tickets that round goes back into the prize pools.","Rollover Prizes":"Rollover Prizes","After every round, if nobody wins in one of the prize brackets, the unclaimed CAKE for that bracket rolls over into the next round and are redistributed among the prize pools.":"After every round, if nobody wins in one of the prize brackets, the unclaimed CAKE for that bracket rolls over into the next round and are redistributed among the prize pools.","CAKE Injections":"CAKE Injections","An average total of 35,000 CAKE from the treasury is added to lottery rounds over the course of a week. This CAKE is of course also included in rollovers! Read more in our guide to ":"An average total of 35,000 CAKE from the treasury is added to lottery rounds over the course of a week. This CAKE is of course also included in rollovers! Read more in our guide to ","CAKE Tokenomics":"CAKE Tokenomics","Digits matched":"Digits matched","Prize pool allocation":"Prize pool allocation","Matches first %digits%":"Matches first %digits%","Matches all 6":"Matches all 6","Burn Pool":"Burn Pool","Still got questions?":"Still got questions?","Check our in-depth guide on":"Check our in-depth guide on","how to play the PancakeSwap lottery!":"how to play the PancakeSwap lottery!","Prize brackets don’t ‘stack’: if you match the first 3 digits in order, you’ll only win prizes from the ‘Match 3’ bracket, and not from ‘Match 1’ and ‘Match 2’.":"Prize brackets don’t ‘stack’: if you match the first 3 digits in order, you’ll only win prizes from the ‘Match 3’ bracket, and not from ‘Match 1’ and ‘Match 2’.","Coming soon!":"Coming soon!","Tickets":"Tickets","Edit numbers":"Edit numbers","each":"each","Collect Prizes":"Collect Prizes","You won":"You won","Calculating rewards":"Calculating rewards","Buying multiple tickets in a single transaction gives a discount. The discount increases in a linear way, up to the maximum of 100 tickets:":"Buying multiple tickets in a single transaction gives a discount. The discount increases in a linear way, up to the maximum of 100 tickets:","2 tickets: 0.05%":"2 tickets: 0.05%","50 tickets: 2.45%":"50 tickets: 2.45%","100 tickets: 4.95%":"100 tickets: 4.95%","You pay":"You pay","Lottery Now Live":"Lottery Now Live","Play Now":"Play Now","Over %amount% in Prizes!":"Over %amount% in Prizes!","Next Auction":"Next Auction","Auction Schedule":"Auction Schedule","%numHours% hours":"%numHours% hours","To be announced":"To be announced","Auction Leaderboard":"Auction Leaderboard","How does it work?":"How does it work?","Step 1: Submit application":"Step 1: Submit application","Projects can submit an application to sponsor a yield farm and/or pool on PancakeSwap via the ":"Projects can submit an application to sponsor a yield farm and/or pool on PancakeSwap via the ","Application Form":"Application Form","Step 2: Await whitelisting":"Step 2: Await whitelisting","The PancakeSwap team will try to respond within a week.":"The PancakeSwap team will try to respond within a week.","Community Farm qualifiers will be asked to provide the address of the wallet which you’ll use for bidding CAKE in the auction.":"Community Farm qualifiers will be asked to provide the address of the wallet which you’ll use for bidding CAKE in the auction.","Core Farm/Pool qualifiers will receive further directions separately.":"Core Farm/Pool qualifiers will receive further directions separately.","Step 3: During the auction":"Step 3: During the auction","During the auction period, if you connect your project’s whitelisted wallet to the Auction page, you’ll see a “Place Bid” button during when the auction is live.":"During the auction period, if you connect your project’s whitelisted wallet to the Auction page, you’ll see a “Place Bid” button during when the auction is live.","You can then commit CAKE to bid during the auction, competing against other project for one of the available farms.":"You can then commit CAKE to bid during the auction, competing against other project for one of the available farms.","Step 4: After the auction":"Step 4: After the auction","If your bid was not successful, you can reclaim your CAKE on this page.":"If your bid was not successful, you can reclaim your CAKE on this page.","If your bid was successful, your farm will begin at the specified time. The CAKE you bid will not be returned to you and will be added to our weekly CAKE burn.":"If your bid was successful, your farm will begin at the specified time. The CAKE you bid will not be returned to you and will be added to our weekly CAKE burn.","So long as you are whitelisted, you’ll be able to participate in each new auction.":"So long as you are whitelisted, you’ll be able to participate in each new auction.","If two or more projects bid the exact same CAKE amount and are contending for a spot in the winning bidders, their bids may be invalidated.":"If two or more projects bid the exact same CAKE amount and are contending for a spot in the winning bidders, their bids may be invalidated.","Community Farm Auction":"Community Farm Auction","Each week, qualifying projects can bid CAKE for the right to host a 7-day Farm on PancakeSwap.":"Each week, qualifying projects can bid CAKE for the right to host a 7-day Farm on PancakeSwap.","Apply for a Farm/Pool":"Apply for a Farm/Pool","Community Auctions":"Community Auctions","FAQs":"FAQs","Auction duration":"Auction duration","Terms & Conditions":"Terms & Conditions","in Prizes!":"in Prizes!","Output is estimated. If the price changes by more than %slippage%% your transaction will revert.":"Output is estimated. If the price changes by more than %slippage%% your transaction will revert.","Supplying %amountA% %symbolA% and %amountB% %symbolB%":"Supplying %amountA% %symbolA% and %amountB% %symbolB%","Removing %amountA% %symbolA% and %amountB% %symbolB%":"Removing %amountA% %symbolA% and %amountB% %symbolB%","Swapping %amountA% %symbolA% for %amountB% %symbolB%":"Swapping %amountA% %symbolA% for %amountB% %symbolB%","Add Liquidity":"Add Liquidity","Add liquidity to receive LP tokens":"Add liquidity to receive LP tokens","Liquidity providers earn a 0.17% trading fee on all trades made for that token pair, proportional to their share of the liquidity pool.":"Liquidity providers earn a 0.17% trading fee on all trades made for that token pair, proportional to their share of the liquidity pool.","You are creating a pool":"You are creating a pool","You are the first liquidity provider.":"You are the first liquidity provider.","The ratio of tokens you add will set the price of this pool.":"The ratio of tokens you add will set the price of this pool.","Once you are happy with the rate click supply to review.":"Once you are happy with the rate click supply to review.","Initial prices and pool share":"Initial prices and pool share","Prices and pool share":"Prices and pool share","Unsupported Asset":"Unsupported Asset","Enabling %asset%":"Enabling %asset%","Enable %asset%":"Enable %asset%","Share of Pool":"Share of Pool","%assetA% per %assetB%":"%assetA% per %assetB%","%asset% Deposited":"%asset% Deposited","Rates":"Rates","Create Pool & Supply":"Create Pool & Supply","Confirm Supply":"Confirm Supply","Confirm Swap":"Confirm Swap","Connect to a wallet to view your liquidity.":"Connect to a wallet to view your liquidity.","Connect to a wallet to find pools":"Connect to a wallet to find pools","Select a token to find your liquidity.":"Select a token to find your liquidity.","No liquidity found.":"No liquidity found.","Don\'t see a pool you joined?":"Don\'t see a pool you joined?","Find other LP tokens":"Find other LP tokens","Import Pool":"Import Pool","Import an existing pool":"Import an existing pool","Select a Token":"Select a Token","Pool Found!":"Pool Found!","No pool found.":"No pool found.","Create pool.":"Create pool.","Manage this pool.":"Manage this pool.","Invalid pair.":"Invalid pair.","You don’t have liquidity in this pool yet.":"You don’t have liquidity in this pool yet.","%assetA%/%assetB% Burned":"%assetA%/%assetB% Burned","Price":"Price","Prices":"Prices","Remove %assetA%-%assetB% liquidity":"Remove %assetA%-%assetB% liquidity","Amount":"Amount","Simple":"Simple","Detailed":"Detailed","Receive WBNB":"Receive WBNB","Receive BNB":"Receive BNB","Remove":"Remove","Input":"Input","Output":"Output","Trade tokens in an instant":"Trade tokens in an instant","From (estimated)":"From (estimated)","From":"From","To (estimated)":"To (estimated)","To":"To","+ Add a send (optional)":"+ Add a send (optional)","- Remove send":"- Remove send","Slippage Tolerance":"Slippage Tolerance","Insufficient liquidity for this trade.":"Insufficient liquidity for this trade.","Try enabling multi-hop trades.":"Try enabling multi-hop trades.","Price Impact High":"Price Impact High","Swap":"Swap","Swap Anyway":"Swap Anyway","Recent Transactions":"Recent Transactions","clear all":"clear all","Clear all":"Clear all","No recent transactions":"No recent transactions","Are you sure?":"Are you sure?","Expert mode turns off the \'Confirm\' transaction prompt, and allows high slippage trades that often result in bad rates and lost funds.":"Expert mode turns off the \'Confirm\' transaction prompt, and allows high slippage trades that often result in bad rates and lost funds.","Only use this mode if you know what you’re doing.":"Only use this mode if you know what you’re doing.","Turn On Expert Mode":"Turn On Expert Mode","Disable Multihops":"Disable Multihops","Restricts swaps to direct pairs only.":"Restricts swaps to direct pairs only.","Enter a valid slippage percentage":"Enter a valid slippage percentage","Your transaction may fail":"Your transaction may fail","Your transaction may be frontrun":"Your transaction may be frontrun","Your transaction will revert if it is pending for more than this long.":"Your transaction will revert if it is pending for more than this long.","minutes":"minutes","Token Amount":"Token Amount","LP tokens in your wallet":"LP tokens in your wallet","Pooled %asset%":"Pooled %asset%","By adding liquidity you\'ll earn 0.17% of all trades on this pair proportional to your share of the pool. Fees are added to the pool, accrue in real time and can be claimed by withdrawing your liquidity.":"By adding liquidity you\'ll earn 0.17% of all trades on this pair proportional to your share of the pool. Fees are added to the pool, accrue in real time and can be claimed by withdrawing your liquidity.","Common bases":"Common bases","These tokens are commonly paired with other tokens.":"These tokens are commonly paired with other tokens.","Expanded results from inactive Token Lists":"Expanded results from inactive Token Lists","Tokens from inactive lists. Import specific tokens below or click \'Manage\' to activate more lists.":"Tokens from inactive lists. Import specific tokens below or click \'Manage\' to activate more lists.","No results found.":"No results found.","Manage":"Manage","Manage Tokens":"Manage Tokens","Import Tokens":"Import Tokens","Import List":"Import List","Import at your own risk":"Import at your own risk","By adding this list you are implicitly trusting that the data is correct. Anyone can create a list, including creating fake versions of existing lists and lists that claim to represent projects that do not have one.":"By adding this list you are implicitly trusting that the data is correct. Anyone can create a list, including creating fake versions of existing lists and lists that claim to represent projects that do not have one.","If you purchase a token from this list, you may not be able to sell it back.":"If you purchase a token from this list, you may not be able to sell it back.","Import":"Import","via":"via","Anyone can create a BEP20 token on BSC with any name, including creating fake versions of existing tokens and tokens that claim to represent projects that do not have a token.":"Anyone can create a BEP20 token on BSC with any name, including creating fake versions of existing tokens and tokens that claim to represent projects that do not have a token.","If you purchase an arbitrary token, you may be unable to sell it back.":"If you purchase an arbitrary token, you may be unable to sell it back.","Unknown Source":"Unknown Source","Lists":"Lists","See":"See","Update list":"Update list","https:// or ipfs:// or ENS name":"https:// or ipfs:// or ENS name","Loaded":"Loaded","Loading":"Loading","Enter valid token address":"Enter valid token address","Custom Token":"Custom Token","Custom Tokens":"Custom Tokens","Unknown Error":"Unknown Error","Select a currency":"Select a currency","Search name or paste address":"Search name or paste address","Add %asset% to Metamask":"Add %asset% to Metamask","Added %asset%":"Added %asset%","Transaction Submitted":"Transaction Submitted","Wallet Address or ENS name":"Wallet Address or ENS name","Recipient":"Recipient","Waiting For Confirmation":"Waiting For Confirmation","Confirm this transaction in your wallet":"Confirm this transaction in your wallet","Dismiss":"Dismiss","Latest":"Latest","Notice for trading %symbol%":"Notice for trading %symbol%","Warning: BONDLY has been compromised. Please remove liquidity until further notice.":"Warning: BONDLY has been compromised. Please remove liquidity until further notice.","Claimed":"Claimed","Settings":"Settings","Transaction deadline":"Transaction deadline","Convert ERC-20 to BEP-20":"Convert ERC-20 to BEP-20","Need help ?":"Need help ?","Select a token":"Select a token","Enter a recipient":"Enter a recipient","Invalid recipient":"Invalid recipient","Supply":"Supply","Your Liquidity":"Your Liquidity","Remove liquidity to receive tokens back":"Remove liquidity to receive tokens back","Trade anything. No registration, no hassle.":"Trade anything. No registration, no hassle.","Trade any token on Binance Smart Chain in seconds, just by connecting your wallet.":"Trade any token on Binance Smart Chain in seconds, just by connecting your wallet.","Learn":"Learn","BNB token":"BNB token","CAKE token":"CAKE token","BTC token":"BTC token","Earn passive income with crypto.":"Earn passive income with crypto.","PancakeSwap makes it easy to make your crypto work for you.":"PancakeSwap makes it easy to make your crypto work for you.","Explore":"Explore","Pie chart":"Pie chart","Stocks chart":"Stocks chart","Folder with cake token":"Folder with cake token","CAKE makes our world go round.":"CAKE makes our world go round.","CAKE token is at the heart of the PancakeSwap ecosystem. Buy it, win it, farm it, spend it, stake it... heck, you can even vote with it!":"CAKE token is at the heart of the PancakeSwap ecosystem. Buy it, win it, farm it, spend it, stake it... heck, you can even vote with it!","Small 3d pancake":"Small 3d pancake","%cakePrizeInUsd% in CAKE prizes this round":"%cakePrizeInUsd% in CAKE prizes this round","Buy tickets with CAKE, win CAKE if your numbers match":"Buy tickets with CAKE, win CAKE if your numbers match","$%bnbWonInUsd% in BNB won so far":"$%bnbWonInUsd% in BNB won so far","Will BNB price rise or fall? guess correctly to win!":"Will BNB price rise or fall? guess correctly to win!","And those users are now entrusting the platform with over $%tvl% in funds.":"And those users are now entrusting the platform with over $%tvl% in funds.","Trusted with billions.":"Trusted with billions.","PancakeSwap has the most users of any decentralized platform, ever.":"PancakeSwap has the most users of any decentralized platform, ever.","Will you join them?":"Will you join them?","$%tvl% staked":"$%tvl% staked","The moon is made of pancakes.":"The moon is made of pancakes.","Trade, earn, and win crypto on the most popular decentralized platform in the galaxy.":"Trade, earn, and win crypto on the most popular decentralized platform in the galaxy.","Lunar bunny":"Lunar bunny","Total supply":"Total supply","Burned to date":"Burned to date","Market cap":"Market cap","Current emissions":"Current emissions","$%marketCap%":"$%marketCap%","%cakeEmissions%/block":"%cakeEmissions%/block","%earningsBusd% to collect from %count% farm":"%earningsBusd% to collect from %count% farm","%earningsBusd% to collect from %count% farms":"%earningsBusd% to collect from %count% farms","%earningsBusd% to collect from %count% farm and CAKE pool":"%earningsBusd% to collect from %count% farm and CAKE pool","%earningsBusd% to collect from %count% farms and CAKE pool":"%earningsBusd% to collect from %count% farms and CAKE pool","%earningsBusd% to collect from CAKE pool":"%earningsBusd% to collect from CAKE pool","%earningsBusd% to collect":"%earningsBusd% to collect","Harvest all":"Harvest all","Start in seconds.":"Start in seconds.","Used by millions.":"Used by millions.","Hi, %userName%!":"Hi, %userName%!","Play":"Play","Connected with %address%":"Connected with %address%","Win millions in prizes":"Win millions in prizes","%users% users":"%users% users","Connect your crypto wallet to start using the app in seconds.":"Connect your crypto wallet to start using the app in seconds.","in the last 30 days":"in the last 30 days","No registration needed.":"No registration needed.","Learn how to start":"Learn how to start","%trades% trades":"%trades% trades","Provably fair, on-chain games.":"Provably fair, on-chain games.","made in the last 30 days":"made in the last 30 days","Win big with PancakeSwap.":"Win big with PancakeSwap.","Copied":"Copied","Make a Profile":"Make a Profile","Your Address":"Your Address","Your Wallet":"Your Wallet","Transactions":"Transactions","BNB Balance Low":"BNB Balance Low","BNB Balance":"BNB Balance","CAKE Balance":"CAKE Balance","You need BNB for transaction fees.":"You need BNB for transaction fees.","Disconnect":"Disconnect","Disconnect Wallet":"Disconnect Wallet","Ending in":"Ending in","Next auction":"Next auction","Contract approved - you can now place your bid!":"Contract approved - you can now place your bid!","Contract approved - you can now reclaim your bid!":"Contract approved - you can now reclaim your bid!","Bid placed!":"Bid placed!","Bid reclaimed!":"Bid reclaimed!","Reclaim Bid":"Reclaim Bid","Place a Bid":"Place a Bid","Reclaim your CAKE now.":"Reclaim your CAKE now.","Your total bid":"Your total bid","Your bid in Auction #%auctionId% was unsuccessful.":"Your bid in Auction #%auctionId% was unsuccessful.","Current Auction":"Current Auction","Farm schedule":"Farm schedule","Farm duration":"Farm duration","%num% days":"%num% days","Only whitelisted project wallets can bid in the auction to create Community Farms.":"Only whitelisted project wallets can bid in the auction to create Community Farms.","Bidding is only possible while the auction is live.":"Bidding is only possible while the auction is live.","If you’re sure your project should be able to bid in this auction, make sure you’re connected with the correct (whitelisted) wallet.":"If you’re sure your project should be able to bid in this auction, make sure you’re connected with the correct (whitelisted) wallet.","Why cant I bid for a farm?":"Why cant I bid for a farm?","Archive":"Archive","Auction #":"Auction #","Auction #%auctionId%":"Auction #%auctionId%","LP Info":"LP Info","Bidder Address":"Bidder Address","Top %num% bidders at the end of the auction will successfully create a community farm.":"Top %num% bidders at the end of the auction will successfully create a community farm.","Position":"Position","CAKE bid":"CAKE bid","Showing top 10 bids only.":"Showing top 10 bids only.","See all whitelisted bidders":"See all whitelisted bidders","Your existing bid":"Your existing bid","Bid a multiple of 10":"Bid a multiple of 10","Bid must be a multiple of 10":"Bid must be a multiple of 10","First bid must be %initialBidAmount% CAKE or more.":"First bid must be %initialBidAmount% CAKE or more.","If your bid is unsuccessful, you’ll be able to reclaim your CAKE after the auction.":"If your bid is unsuccessful, you’ll be able to reclaim your CAKE after the auction.","Notice":"Notice","This page is a functional page, for projects to bid for farms.":"This page is a functional page, for projects to bid for farms.","If you’re not a whitelisted project, you won’t be able to participate, but you can still view the auction bids in real time!":"If you’re not a whitelisted project, you won’t be able to participate, but you can still view the auction bids in real time!","Connect a whitelisted project wallet to participate in Auctions.":"Connect a whitelisted project wallet to participate in Auctions.","Your bid in Auction #%auctionId% was successful.":"Your bid in Auction #%auctionId% was successful.","Your Farm will be launched as follows:":"Your Farm will be launched as follows:","Multiplier per farm":"Multiplier per farm","Total whitelisted bidders":"Total whitelisted bidders","Please specify auction ID":"Please specify auction ID","No history yet":"No history yet","Connected as %projectName%":"Connected as %projectName%","Place bid":"Place bid","Farms available":"Farms available","top %num% bidders":"top %num% bidders","All Whitelisted Project Wallets":"All Whitelisted Project Wallets","Search address or token":"Search address or token","This page is for projects to bid for farms.":"This page is for projects to bid for farms.","If you’re not a whitelisted project, you won’t be able to participate, but you can still view what’s going on!":"If you’re not a whitelisted project, you won’t be able to participate, but you can still view what’s going on!","through community auctions so far!":"through community auctions so far!","%num% Winners":"%num% Winners","%num% Contenders...":"%num% Contenders...","Farm Auctions":"Farm Auctions","auction bunny":"auction bunny","Burnt CAKE":"Burnt CAKE","Stake %stakingSymbol% - Earn %earningSymbol%":"Stake %stakingSymbol% - Earn %earningSymbol%","Top Farms":"Top Farms","Top Syrup Pools":"Top Syrup Pools","Expert Mode":"Expert Mode","Don’t show this again":"Don’t show this again","Global":"Global","Tx deadline (mins)":"Tx deadline (mins)","Flippy sounds":"Flippy sounds","Your transaction will revert if it is left confirming for longer than this time.":"Your transaction will revert if it is left confirming for longer than this time.","Fun sounds to make a truly immersive pancake-flipping trading":"Fun sounds to make a truly immersive pancake-flipping trading","Found unknown key Setting a high slippage tolerance can help transactions succeed, but you may not get such a good price. Use with caution.":"Found unknown key Setting a high slippage tolerance can help transactions succeed, but you may not get such a good price. Use with caution.","Swaps & Liquidity":"Swaps & Liquidity","Setting a high slippage tolerance can help transactions succeed, but you may not get such a good price. Use with caution.":"Setting a high slippage tolerance can help transactions succeed, but you may not get such a good price. Use with caution.","Bypasses confirmation modals and allows high slippage trades. Use at your own risk.":"Bypasses confirmation modals and allows high slippage trades. Use at your own risk.","Fun sounds to make a truly immersive pancake-flipping trading experience":"Fun sounds to make a truly immersive pancake-flipping trading experience","ROI at current rates":"ROI at current rates","Farm Multiplier":"Farm Multiplier","“My Balance” here includes both LP Tokens in your wallet, and LP Tokens already staked in this farm.":"“My Balance” here includes both LP Tokens in your wallet, and LP Tokens already staked in this farm.","“My Balance” here includes both %assetSymbol% in your wallet, and %assetSymbol% already staked in this pool.":"“My Balance” here includes both %assetSymbol% in your wallet, and %assetSymbol% already staked in this pool.","APY (%compoundTimes%x daily compound)":"APY (%compoundTimes%x daily compound)","My Balance":"My Balance","Staked for":"Staked for","Compounding every":"Compounding every","1D":"1D","7D":"7D","14D":"14D","30D":"30D","1Y":"1Y","5Y":"5Y","Annual ROI at current rates":"Annual ROI at current rates","Calling all BSC projects":"Calling all BSC projects","Apply for whitelisting now!":"Apply for whitelisting now!","Default Transaction Speed (GWEI)":"Default Transaction Speed (GWEI)","Adjusts the gas price (transaction fee) for your transaction. Higher GWEI = higher speed = higher fees":"Adjusts the gas price (transaction fee) for your transaction. Higher GWEI = higher speed = higher fees","Standard (%gasPrice%)":"Standard (%gasPrice%)","Fast (%gasPrice%)":"Fast (%gasPrice%)","Instant (%gasPrice%)":"Instant (%gasPrice%)","You won multiple collectibles!":"You won multiple collectibles!","Prices shown on cards and charts are different":"Prices shown on cards and charts are different","The price you see come from difference places":"The price you see come from difference places","Prices on cards come from Chainlink’s verifiable price oracle.":"Prices on cards come from Chainlink’s verifiable price oracle.","Prices on charts come from Binance.com. Chart\'s are provided for your reference only.":"Prices on charts come from Binance.com. Chart\'s are provided for your reference only.","Only the price from Chainlink (shown on the cards) determines the round\'s result.":"Only the price from Chainlink (shown on the cards) determines the round\'s result.","Showing history for Prediction v0.2":"Showing history for Prediction v0.2","Check for unclaimed v0.1 winnings":"Check for unclaimed v0.1 winnings","You have no unclaimed v0.1 prizes.":"You have no unclaimed v0.1 prizes.","Nothing to collect":"Nothing to collect","Download .CSV":"Download .CSV","Download your v0.1 Prediction history below.":"Download your v0.1 Prediction history below.","Nothing to Collect":"Nothing to Collect","From round %round%":"From round %round%","From rounds %rounds%":"From rounds %rounds%","View More":"View More","User":"User","Net Winnings (BNB)":"Net Winnings (BNB)","Win Rate":"Win Rate","Rounds Won":"Rounds Won","Rank By":"Rank By","Net Winnings":"Net Winnings","Total BNB":"Total BNB","%num%m":"%num%m","Rounds Played":"Rounds Played","Winnings (BNB)":"Winnings (BNB)","Direction":"Direction","My Rankings":"My Rankings","View Stats":"View Stats","Last %num% Bets":"Last %num% Bets","Search %subject%":"Search %subject%","Address":"Address","Learn How to Connect":"Learn How to Connect","Haven’t got a crypto wallet yet?":"Haven’t got a crypto wallet yet?","Neither side wins this round":"Neither side wins this round","The Locked Price & Closed Price are exactly the same (within 8 decimals), so neither side wins. All funds entered into UP and DOWN positions will go to the weekly CAKE burn.":"The Locked Price & Closed Price are exactly the same (within 8 decimals), so neither side wins. All funds entered into UP and DOWN positions will go to the weekly CAKE burn.","To Burn":"To Burn","Account":"Account","PancakeSwap Info & Analytics":"PancakeSwap Info & Analytics","Top Tokens":"Top Tokens","Top Pools":"Top Pools","Your Watchlist":"Your Watchlist","Saved pools will appear here":"Saved pools will appear here","All Pools":"All Pools","All Tokens":"All Tokens","Saved tokens will appear here":"Saved tokens will appear here","Transactions 24H":"Transactions 24H","Volume 24H":"Volume 24H","Volume 7D":"Volume 7D","Total Tokens Locked":"Total Tokens Locked","LP reward fees 24H":"LP reward fees 24H","LP reward fees 7D":"LP reward fees 7D","No pool has been created with this token yet. Create one":"No pool has been created with this token yet. Create one","here.":"here.","Price Change":"Price Change","Page %page% of %maxPage%":"Page %page% of %maxPage%","Name":"Name","Swaps":"Swaps","Adds":"Adds","Removes":"Removes","No Transactions":"No Transactions","Action":"Action","Total Value":"Total Value","Add %token0% and %token1%":"Add %token0% and %token1%","Remove %token0% and %token1%":"Remove %token0% and %token1%","Swap %token0% for %token1%":"Swap %token0% for %token1%","Search pools or tokens":"Search pools or tokens","Watchlist":"Watchlist","See more...":"See more...","No results":"No results","Error occurred, please try again":"Error occurred, please try again","LP reward APR":"LP reward APR","out of $%totalFees% total fees":"out of $%totalFees% total fees","24H":"24H","7D performance":"7D performance","Based on last 7 days\' performance. Does not account for impermanent loss":"Based on last 7 days\' performance. Does not account for impermanent loss","View token on CoinMarketCap":"View token on CoinMarketCap","Top Movers":"Top Movers","Loading chart data...":"Loading chart data...","You have %amount% tickets this round":"You have %amount% tickets this round","You have %amount% ticket this round":"You have %amount% ticket this round","You had %amount% tickets this round":"You had %amount% tickets this round","You had %amount% ticket this round":"You had %amount% ticket this round","Remove Liquidity":"Remove Liquidity","Minimum received":"Minimum received","Maximum sold":"Maximum sold","The difference between the market price and estimated price due to trade size.":"The difference between the market price and estimated price due to trade size.","The difference between the market price and your price due to trade size.":"The difference between the market price and your price due to trade size.","Liquidity Provider Fee":"Liquidity Provider Fee","Import Token":"Import Token","Output is estimated. You will receive at least %amount% %symbol% or the transaction will revert.":"Output is estimated. You will receive at least %amount% %symbol% or the transaction will revert.","Input is estimated. You will sell at most %amount% %symbol% or the transaction will revert.":"Input is estimated. You will sell at most %amount% %symbol% or the transaction will revert.","Output will be sent to %recipient%":"Output will be sent to %recipient%","Price Updated":"Price Updated","Accept":"Accept","For each trade a %amount% fee is paid":"For each trade a %amount% fee is paid","%amount% to LP token holders":"%amount% to LP token holders","%amount% to the Treasury":"%amount% to the Treasury","%amount% towards CAKE buyback and burn":"%amount% towards CAKE buyback and burn","Routing through these tokens resulted in the best price for your trade.":"Routing through these tokens resulted in the best price for your trade.","Your transaction will revert if there is a large, unfavorable price movement before it is confirmed.":"Your transaction will revert if there is a large, unfavorable price movement before it is confirmed.","Route":"Route","Price Impact":"Price Impact","This swap has a price impact of at least %amount%%. Please type the word \\"%word%\\" to continue with this swap.":"This swap has a price impact of at least %amount%%. Please type the word \\"%word%\\" to continue with this swap.","This swap has a price impact of at least %amount%%. Please confirm that you would like to continue with this swap.":"This swap has a price impact of at least %amount%%. Please confirm that you would like to continue with this swap.","To receive %assetA% and %assetB%":"To receive %assetA% and %assetB%","Price Impact Too High":"Price Impact Too High","About":"About","Online Store":"Online Store","Customer Support":"Customer Support","Troubleshooting":"Troubleshooting","Guides":"Guides","Developers":"Developers","Documentation":"Documentation","Bug Bounty":"Bug Bounty","Audits":"Audits","Careers":"Careers","Dark mode":"Dark mode","Invalid pair":"Invalid pair","V1 (old)":"V1 (old)","Transaction receipt":"Transaction receipt","You won points for joining PancakeSwap during the first year of our journey!":"You won points for joining PancakeSwap during the first year of our journey!","NFT Collected":"NFT Collected","User profile picture":"User profile picture","User team banner":"User team banner","Items":"Items","Items listed":"Items listed","Lowest (%symbol%)":"Lowest (%symbol%)","Vol. (%symbol%)":"Vol. (%symbol%)","Profile":"Profile","Selling":"Selling","View BscScan for user address":"View BscScan for user address","Back to profile":"Back to profile","Activate Profile":"Activate Profile","Lowest price":"Lowest price","Your price":"Your price","Not on sale":"Not on sale","For Sale (%num%)":"For Sale (%num%)","Owner":"Owner","Contract address":"Contract address","Token id":"Token id","Token ID":"Token ID","Manage Yours":"Manage Yours","Properties":"Properties","Bunny id":"Bunny id","More from this collection":"More from this collection","Apply (%num%)":"Apply (%num%)","Apply":"Apply","Attributes":"Attributes","Min":"Min","Not enough %symbol% to purchase this NFT":"Not enough %symbol% to purchase this NFT","Convert between BNB and WBNB for free":"Convert between BNB and WBNB for free","Convert":"Convert","Checkout":"Checkout","%symbol% in wallet":"%symbol% in wallet","Total payment":"Total payment","Pay with":"Pay with","Please enable WBNB spending in your wallet":"Please enable WBNB spending in your wallet","Please confirm the transaction in your wallet":"Please confirm the transaction in your wallet","Your NFTs":"Your NFTs","Contract approved - you can now buy NFT with WBNB!":"Contract approved - you can now buy NFT with WBNB!","Your NFT has been sent to your wallet":"Your NFT has been sent to your wallet","Transaction Confirmed":"Transaction Confirmed","Activity":"Activity","For sale":"For sale","View Item":"View Item","The NFT will be removed from your wallet and put on sale at this price.":"The NFT will be removed from your wallet and put on sale at this price.","Sales are in WBNB. You can swap WBNB to BNB 1:1 for free with PancakeSwap.":"Sales are in WBNB. You can swap WBNB to BNB 1:1 for free with PancakeSwap.","Enable Listing":"Enable Listing","Please enable your NFT to be sent to the market":"Please enable your NFT to be sent to the market","Contract approved - you can now put your NFT for sale!":"Contract approved - you can now put your NFT for sale!","Adjust Sale Price":"Adjust Sale Price","Set Price":"Set Price","Input New Sale Price":"Input New Sale Price","Your NFT has been listed for sale!":"Your NFT has been listed for sale!","Your NFT listing has been changed.":"Your NFT listing has been changed.","Your NFT has been returned to your wallet":"Your NFT has been returned to your wallet","Removing this NFT from the marketplace will return it to your wallet.":"Removing this NFT from the marketplace will return it to your wallet.","Continue?":"Continue?","Paste BSC address":"Paste BSC address","That’s not a Binance Smart Chain wallet address.":"That’s not a Binance Smart Chain wallet address.","This action will send your NFT to the address you have indicated above. Make sure it’s the correct":"This action will send your NFT to the address you have indicated above. Make sure it’s the correct","This address is the one that is currently connected":"This address is the one that is currently connected","Recently Listed":"Recently Listed","Please progress to the next step.":"Please progress to the next step.","Press \'confirm\' to mint this NFT":"Press \'confirm\' to mint this NFT","You have minted your starter NFT":"You have minted your starter NFT","Go to your profile page to continue.":"Go to your profile page to continue.","Select All":"Select All","Clear":"Clear","Clear All":"Clear All","All Items":"All Items","Sort By":"Sort By","Collections":"Collections","View All":"View All","All Collections":"All Collections","Recently listed":"Recently listed","Hot Collections":"Hot Collections","Remove from Market":"Remove from Market","Lowest price on market":"Lowest price on market","Transfer to New Wallet":"Transfer to New Wallet","Less":"Less","No results found":"No results found","More Collections are on their way!":"More Collections are on their way!","Removing it will suspend your profile, and you won’t be able to earn points, participate in team activities, or be eligible for new NFT drops.":"Removing it will suspend your profile, and you won’t be able to earn points, participate in team activities, or be eligible for new NFT drops.","Traits":"Traits","Trait":"Trait","Count":"Count","Rarity":"Rarity","Lowest":"Lowest","You don’t have any of this item.":"You don’t have any of this item.","You don’t have this item.":"You don’t have this item.","Manage/Sell":"Manage/Sell","NFT Market":"NFT Market","Delisted":"Delisted","No NFTs found":"No NFTs found","Listed":"Listed","Transaction Details":"Transaction Details","Buy and Sell NFTs on Binance Smart Chain":"Buy and Sell NFTs on Binance Smart Chain","Modified":"Modified","From/To":"From/To","Item":"Item","Bought":"Bought","Event":"Event","Sold":"Sold","No NFT market history found":"No NFT market history found","Edit":"Edit","Token ID:":"Token ID:","Token ID: %id%":"Token ID: %id%","When selling NFTs from this collection, a portion of the BNB paid will be diverted before reaching the seller:":"When selling NFTs from this collection, a portion of the BNB paid will be diverted before reaching the seller:","%percentage%% royalties to the collection owner":"%percentage%% royalties to the collection owner","%percentage%% trading fee will be used to buy & burn CAKE":"%percentage%% trading fee will be used to buy & burn CAKE","Sell":"Sell","Set as Profile Pic":"Set as Profile Pic","Allowed price range is between %minPrice% and %maxPrice% WBNB":"Allowed price range is between %minPrice% and %maxPrice% WBNB","Seller pays %percentage%% platform fee on sale":"Seller pays %percentage%% platform fee on sale","Platform fee if sold":"Platform fee if sold","Click to view NFT":"Click to view NFT","Asking price":"Asking price","Please enter a valid address, or connect your wallet to view your profile":"Please enter a valid address, or connect your wallet to view your profile","No items for sale":"No items for sale","Load more":"Load more","Your NFT has been transferred to another wallet":"Your NFT has been transferred to another wallet","Back":"Back","Confirm transaction":"Confirm transaction","Collection":"Collection","Items in the table update every 10 seconds":"Items in the table update every 10 seconds","Pancake Squad":"Pancake Squad","Meet the Artist":"Meet the Artist","Please confirm your transaction in wallet.":"Please confirm your transaction in wallet.","View market":"View market","Mint":"Mint","Max purchased":"Max purchased","Fair, Random, Rare":"Fair, Random, Rare","Activate your profile":"Activate your profile","Sounds great, how can I get one?":"Sounds great, how can I get one?","View the smart contract address on BscScan":"View the smart contract address on BscScan","Your Claim Tickets: ":"Your Claim Tickets: ","Claiming Complete!":"Claiming Complete!","Buy how many?":"Buy how many?","Activate your profile and make sure you have at least the cost of 1 NFT in your wallet to buy a Squad Ticket.":"Activate your profile and make sure you have at least the cost of 1 NFT in your wallet to buy a Squad Ticket.","Sold Out!":"Sold Out!","Follow on Twitter":"Follow on Twitter","Confirming...":"Confirming...","Your Squad (%tokens%)":"Your Squad (%tokens%)","Minting...":"Minting...","Not eligible":"Not eligible","Out of the 10,000 total NFTs in the squad,":"Out of the 10,000 total NFTs in the squad,","You’ll need an active PancakeSwap Profile to buy Minting Tickets and mint a Pancake Squad NFT!":"You’ll need an active PancakeSwap Profile to buy Minting Tickets and mint a Pancake Squad NFT!","The minting period is now over: all 10,000 bunnies have now been minted.":"The minting period is now over: all 10,000 bunnies have now been minted.","View the":"View the","All 10,000 Pancake Squad NFTs have now been minted!":"All 10,000 Pancake Squad NFTs have now been minted!","Mint Cost: %minCost% CAKE each":"Mint Cost: %minCost% CAKE each","Cost per Ticket":"Cost per Ticket","Follow on Instagram":"Follow on Instagram","Please enable CAKE spending in your wallet":"Please enable CAKE spending in your wallet","Mint NFTs (%tickets%)":"Mint NFTs (%tickets%)","490 are available in the pre-sale for owners of Gen 0 Pancake Bunnies (bunnyID 0, 1, 2, 3, 4)":"490 are available in the pre-sale for owners of Gen 0 Pancake Bunnies (bunnyID 0, 1, 2, 3, 4)","Hold CAKE":"Hold CAKE","Head to the NFT Market to buy!":"Head to the NFT Market to buy!","documentation on our site":"documentation on our site","Max per wallet: %maxPerWallet%":"Max per wallet: %maxPerWallet%","Your CAKE Balance":"Your CAKE Balance","Public Sale: Any wallet with an active Pancake Profile can purchase up to 10 Squad Tickets, while stocks last.":"Public Sale: Any wallet with an active Pancake Profile can purchase up to 10 Squad Tickets, while stocks last.","Enabling...":"Enabling...","Gen 0 Pancake Bunnies":"Gen 0 Pancake Bunnies","Each NFT costs CAKE to mint. Remember you also need BNB to cover transaction fees too!":"Each NFT costs CAKE to mint. Remember you also need BNB to cover transaction fees too!","Check the rarity of each NFT’s traits on the":"Check the rarity of each NFT’s traits on the","PancakeSwap’s first official generative NFT collection.":"PancakeSwap’s first official generative NFT collection.","Your remaining limit":"Your remaining limit","During this phase, any wallet holding a Squad Ticket can redeem their ticket to mint a Pancake Squad NFT.":"During this phase, any wallet holding a Squad Ticket can redeem their ticket to mint a Pancake Squad NFT.","120 are reserved by the team for community giveaways, etc;":"120 are reserved by the team for community giveaways, etc;","Buy Squad Tickets":"Buy Squad Tickets","Pancake Squad page in the NFT Market":"Pancake Squad page in the NFT Market","Join the squad.":"Join the squad.","Total Cost":"Total Cost","Buy Minting Tickets":"Buy Minting Tickets","and the remaining NFTs can be minted by anyone with a ":"and the remaining NFTs can be minted by anyone with a ","Buy Squad Tickets, while stocks last. You’ll use them in step 4.":"Buy Squad Tickets, while stocks last. You’ll use them in step 4.","For more details, check the":"For more details, check the","The network may become busy during the sale period. Consider setting a high gas fee (GWEI).":"The network may become busy during the sale period. Consider setting a high gas fee (GWEI).","Pancake Profile!":"Pancake Profile!","Phase Complete!":"Phase Complete!","randomization documentation on our docs site":"randomization documentation on our docs site","All Pancake Squad NFTs are allocated to Minting Ticket holders through a provably-fair system based on ChainLink at the time of minting.":"All Pancake Squad NFTs are allocated to Minting Ticket holders through a provably-fair system based on ChainLink at the time of minting.","Claim Phase":"Claim Phase","by clicking the “Filter” view. The number of bunnies with a specific trait is displayed next to the trait name.":"by clicking the “Filter” view. The number of bunnies with a specific trait is displayed next to the trait name.","During this phase, any wallet holding a Squad Ticket can redeem their ticket to claim a Pancake Squad NFT.":"During this phase, any wallet holding a Squad Ticket can redeem their ticket to claim a Pancake Squad NFT.","Wait for the Reveal":"Wait for the Reveal","It’ll take a few days before your bunny’s image is revealed. Just hold tight and wait for the final image!":"It’ll take a few days before your bunny’s image is revealed. Just hold tight and wait for the final image!","Trading will open before the images are live, but you’ll still be able to check the bunnies’ stats.":"Trading will open before the images are live, but you’ll still be able to check the bunnies’ stats.","Pre-sale: Wallets which held “Gen 0” Pancake Bunnies NFTs (bunnyID 0,1,2,3,4) at a snapshot taken some time between 12 and 2 hours before the presale begins can purchase one Squad Ticket per Gen 0 NFT.":"Pre-sale: Wallets which held “Gen 0” Pancake Bunnies NFTs (bunnyID 0,1,2,3,4) at a snapshot taken some time between 12 and 2 hours before the presale begins can purchase one Squad Ticket per Gen 0 NFT.","Transaction has succeeded!":"Transaction has succeeded!","Gen 0 Pancake Bunnies (bunnyID 0, 1, 2, 3, 4)":"Gen 0 Pancake Bunnies (bunnyID 0, 1, 2, 3, 4)","The birthplace of Cecy is truly unknown":"The birthplace of Cecy is truly unknown","But legend tells us, she was sailing alone":"But legend tells us, she was sailing alone","Beyond the mountains, across the sea":"Beyond the mountains, across the sea","When she found an island, said “this is for me!”":"When she found an island, said “this is for me!”","‘Twas full of rabbits, who caught her attention":"‘Twas full of rabbits, who caught her attention","Making neat stuff beyond her comprehension":"Making neat stuff beyond her comprehension","The bunnies were working in a big ol’ kitchen":"The bunnies were working in a big ol’ kitchen","Cooking hot pancakes for their big ol’ mission:":"Cooking hot pancakes for their big ol’ mission:","To drizzle in syrup and hand them out soon":"To drizzle in syrup and hand them out soon","So that bunnies worldwide may fly to the moon.":"So that bunnies worldwide may fly to the moon.","All Pancake Squad NFTs are allocated to Squad Ticket holders through a provably-fair system based on ChainLink at the time of minting.":"All Pancake Squad NFTs are allocated to Squad Ticket holders through a provably-fair system based on ChainLink at the time of minting.","Coming Oct. 7":"Coming Oct. 7","Presale:":"Presale:","Public Sale:":"Public Sale:","Review":"Review","Not for sale":"Not for sale","This NFT is your profile picture, you must change it to some other NFT if you want to sell this one.":"This NFT is your profile picture, you must change it to some other NFT if you want to sell this one.","Owner information is not available for this item":"Owner information is not available for this item","List for sale":"List for sale","Adjust price":"Adjust price","starts in":"starts in","Presale max purchased":"Presale max purchased","end in":"end in","Insufficient Balance":"Insufficient Balance","Bunnies":"Bunnies","Squad":"Squad","Highest price":"Highest price","10,000 bunnies":"10,000 bunnies","10,000 bunnies.":"10,000 bunnies.","ZERO duplicates.":"ZERO duplicates.","Every Pancake Squad bunny is different.":"Every Pancake Squad bunny is different.","On top of that, there are a very small number of ultra-rare special unique bunnies as well...!":"On top of that, there are a very small number of ultra-rare special unique bunnies as well...!","View in Market":"View in Market","They’re all randomly generated from over 200 different features, with over eight BILLION possible combinations, so that no bunny is ever exactly alike.":"They’re all randomly generated from over 200 different features, with over eight BILLION possible combinations, so that no bunny is ever exactly alike.","View Documentation":"View Documentation","What’s the Smart Contract?":"What’s the Smart Contract?","Redeem Tickets to claim NFTs":"Redeem Tickets to claim NFTs","Are you ready?":"Are you ready?","%remaining% of %total% remaining":"%remaining% of %total% remaining","%remaining% of %total% claimed":"%remaining% of %total% claimed","Presale":"Presale","Ready for Pre-Sale!":"Ready for Pre-Sale!","I can’t see my NFT’s picture!":"I can’t see my NFT’s picture!","Sorry, you can’t claim any NFTs! Better luck next time.":"Sorry, you can’t claim any NFTs! Better luck next time.","You need a profile to participate!":"You need a profile to participate!","Randomizing NFT allocation with Chainlink":"Randomizing NFT allocation with Chainlink","Public Sale":"Public Sale","You’re all set!":"You’re all set!","How many can I mint?":"How many can I mint?","Ready for Public Sale!":"Ready for Public Sale!","The max limit per wallet is 10 NFTs.":"The max limit per wallet is 10 NFTs.","Where do the fees go?":"Where do the fees go?","100% of CAKE spent on Pancake Squad NFTs will be burned as part of our weekly CAKE burn.":"100% of CAKE spent on Pancake Squad NFTs will be burned as part of our weekly CAKE burn.","How are the NFTs randomly distributed?":"How are the NFTs randomly distributed?","What’s the value of each NFT?":"What’s the value of each NFT?","Wait for the reveal! After all 10,000 members of the Pancake Squad have been minted, their images will remain hidden for a few days. Just be patient :)":"Wait for the reveal! After all 10,000 members of the Pancake Squad have been minted, their images will remain hidden for a few days. Just be patient :)","Users holding Gen 0 Pancake Bunny NFTs at the snapshot may also purchase one additional Pancake Squad NFT in the presale for each Pancake Bunny they hold.":"Users holding Gen 0 Pancake Bunny NFTs at the snapshot may also purchase one additional Pancake Squad NFT in the presale for each Pancake Bunny they hold.","For example, if you have 5 Gen 0 bunnies, you can buy 5 minting tickets in the presale, then max. 10 in the public sale.":"For example, if you have 5 Gen 0 bunnies, you can buy 5 minting tickets in the presale, then max. 10 in the public sale.","Once all 10,000 Squad Tickets have been bought, Chainlink VRF will be used to randomly allocate the pre-generated NFTs to the purchased Tickets. Squad Tickets are allocated IDs numbered in order of their purchase.":"Once all 10,000 Squad Tickets have been bought, Chainlink VRF will be used to randomly allocate the pre-generated NFTs to the purchased Tickets. Squad Tickets are allocated IDs numbered in order of their purchase.","Once all 10,000 have been sold, VRF will pick numbers from 0 to 9999, which will be used to shift the Squad Ticket ID. This will ensure that the distribution of rare NFTs will be randomized, and prevents “sniping” of specific NFTs during the pre-sale or public sale phases.":"Once all 10,000 have been sold, VRF will pick numbers from 0 to 9999, which will be used to shift the Squad Ticket ID. This will ensure that the distribution of rare NFTs will be randomized, and prevents “sniping” of specific NFTs during the pre-sale or public sale phases.","Value is subjective, but since different traits have different levels of rarity, you can expect bunnies with rarer traits to trade for higher prices than others. If you’re planning to sell, check the NFT market for the price of bunnies with a similarly rare traits to yours.":"Value is subjective, but since different traits have different levels of rarity, you can expect bunnies with rarer traits to trade for higher prices than others. If you’re planning to sell, check the NFT market for the price of bunnies with a similarly rare traits to yours.","Bunny Id":"Bunny Id","Filter by":"Filter by","On Sale":"On Sale","Results":"Results","Newest Arrivals":"Newest Arrivals","I sold an NFT, where’s my BNB?":"I sold an NFT, where’s my BNB?","Trades are settled in WBNB, which is a wrapped version of BNB used on Binance Smart Chain. That means that when you sell an item, WBNB is sent to your wallet instead of BNB.":"Trades are settled in WBNB, which is a wrapped version of BNB used on Binance Smart Chain. That means that when you sell an item, WBNB is sent to your wallet instead of BNB.","You can instantly swap your WBNB for BNB with no trading fees on PancakeSwap.":"You can instantly swap your WBNB for BNB with no trading fees on PancakeSwap.","When can I trade other NFT Collections?":"When can I trade other NFT Collections?","Soon! The current NFT Market is a basic version (phase 1), with early access to trading PancakeSwap NFTs only.":"Soon! The current NFT Market is a basic version (phase 1), with early access to trading PancakeSwap NFTs only.","Other collections will be coming soon. We’ll make an announcement soon about the launch of the V2 Market.":"Other collections will be coming soon. We’ll make an announcement soon about the launch of the V2 Market.","How can I list my NFT collection on the Market?":"How can I list my NFT collection on the Market?","In Phase 2 of the NFT Marketplace, collections must be whitelisted before they may be listed.":"In Phase 2 of the NFT Marketplace, collections must be whitelisted before they may be listed.","We are now accepting applications from NFT collection owners seeking to list their collections. Please apply here: https://docs.pancakeswap.finance/contact-us/nft-market-applications":"We are now accepting applications from NFT collection owners seeking to list their collections. Please apply here: https://docs.pancakeswap.finance/contact-us/nft-market-applications","What are the fees?":"What are the fees?","100% of all platform fees taken by PancakeSwap from sales are used to buy back and BURN CAKE tokens in our weekly CAKE burns.":"100% of all platform fees taken by PancakeSwap from sales are used to buy back and BURN CAKE tokens in our weekly CAKE burns.","Platform fees: 2% is subtracted from NFT sales on the market. Subject to change.Collection fees: Additional fees may be taken by collection creators, once those collections are live. These will not contribute to the CAKE burns.":"Platform fees: 2% is subtracted from NFT sales on the market. Subject to change.Collection fees: Additional fees may be taken by collection creators, once those collections are live. These will not contribute to the CAKE burns.","Mint Cost: To Be Announced":"Mint Cost: To Be Announced","The smart contract address will be released soon before the sale starts.":"The smart contract address will be released soon before the sale starts.","Supply Count":"Supply Count","Brand":"Brand","Success":"Success","Wrap":"Wrap","Unwrap":"Unwrap","Go to IFO":"Go to IFO","Phishing warning: ":"Phishing warning: ","please make sure you\'re visiting https://pancakeswap.finance - check the URL carefully.":"please make sure you\'re visiting https://pancakeswap.finance - check the URL carefully.","No bids yet":"No bids yet","Please specify Round":"Please specify Round","1W":"1W","1M":"1M","Failed to load price chart for this pair":"Failed to load price chart for this pair","You can swap WBNB for BNB (and vice versa) with no trading fees.":"You can swap WBNB for BNB (and vice versa) with no trading fees.","Exchange rate is always 1 to 1.":"Exchange rate is always 1 to 1.","You can contribute to one or both, it doesn’t matter: only your overall contribution is counted for the achievement.":"You can contribute to one or both, it doesn’t matter: only your overall contribution is counted for the achievement.","You’ll need %symbol% tokens to participate in the IFO!":"You’ll need %symbol% tokens to participate in the IFO!","Get %symbol%, or make sure your tokens aren’t staked somewhere else.":"Get %symbol%, or make sure your tokens aren’t staked somewhere else.","Refresh":"Refresh","Basic":"Basic","TradingView chart not available":"TradingView chart not available","Everyone’s a winner!":"Everyone’s a winner!","Sign up for battle and you’re guaranteed a prize!":"Sign up for battle and you’re guaranteed a prize!","Binance Fan Token Trading Competition":"Binance Fan Token Trading Competition","Compete with other teams for the highest trading volume!":"Compete with other teams for the highest trading volume!","Trading Competition":"Trading Competition","Trade SANTOS/BNB, PORTO/BNB, LAZIO/BNB, SANTOS/BUSD, PORTO/BUSD, LAZIO/BUSD, CAKE/BNB and CAKE/BUSD during the battle period to raise both your and your team’s score. See the Rules section below for more.":"Trade SANTOS/BNB, PORTO/BNB, LAZIO/BNB, SANTOS/BUSD, PORTO/BUSD, LAZIO/BUSD, CAKE/BNB and CAKE/BUSD during the battle period to raise both your and your team’s score. See the Rules section below for more.","To participate in the next IFO, stake some CAKE in the IFO CAKE pool!":"To participate in the next IFO, stake some CAKE in the IFO CAKE pool!","IFO Credit":"IFO Credit","Stake CAKE in IFO pool":"Stake CAKE in IFO pool","The maximum amount of CAKE user can commit to all the sales combined, is equal to the average CAKE balance in the IFO CAKE pool prior to the IFO. Stake more CAKE to increase the maximum CAKE you can commit to the sale. Missed this IFO? You can keep staking in the IFO CAKE Pool to join the next IFO sale.":"The maximum amount of CAKE user can commit to all the sales combined, is equal to the average CAKE balance in the IFO CAKE pool prior to the IFO. Stake more CAKE to increase the maximum CAKE you can commit to the sale. Missed this IFO? You can keep staking in the IFO CAKE Pool to join the next IFO sale.","How does the IFO credit calculated?":"How does the IFO credit calculated?","IFO credit is calculated by average block balance in the IFO pool in over the staking period announced with each IFO proposal.":"IFO credit is calculated by average block balance in the IFO pool in over the staking period announced with each IFO proposal.","Please refer to our blog post for more details.":"Please refer to our blog post for more details.","Your max CAKE entry":"Your max CAKE entry","For the basic sale, Max CAKE entry is capped by minimum between your average CAKE balance in the IFO CAKE pool, or the pool’s hard cap. To increase the max entry, Stake more CAKE into the IFO CAKE pool":"For the basic sale, Max CAKE entry is capped by minimum between your average CAKE balance in the IFO CAKE pool, or the pool’s hard cap. To increase the max entry, Stake more CAKE into the IFO CAKE pool","For the unlimited sale, Max CAKE entry is capped by your average CAKE balance in the IFO CAKE pool. To increase the max entry, Stake more CAKE into the IFO CAKE pool":"For the unlimited sale, Max CAKE entry is capped by your average CAKE balance in the IFO CAKE pool. To increase the max entry, Stake more CAKE into the IFO CAKE pool","You don’t have any average CAKE balance available to commit in the IFO CAKE pool.":"You don’t have any average CAKE balance available to commit in the IFO CAKE pool.","Sale Finished!":"Sale Finished!","Start in":"Start in","Your entry limit in the next IFO sale is determined by your IFO credit. This is calculated by the average CAKE balance of the principal amount in the IFO pool during the last credit calculation period.":"Your entry limit in the next IFO sale is determined by your IFO credit. This is calculated by the average CAKE balance of the principal amount in the IFO pool during the last credit calculation period.","Please note: even the pool is auto compounding. Amount of profits will not be included during IFO credit calculations.":"Please note: even the pool is auto compounding. Amount of profits will not be included during IFO credit calculations.","Exceeded max CAKE entry":"Exceeded max CAKE entry","Only applies within %num% days of staking. Unstaking after %num% days will not include a fee. Timer resets every time you stake new CAKE in the pool.":"Only applies within %num% days of staking. Unstaking after %num% days will not include a fee. Timer resets every time you stake new CAKE in the pool.","unstaking fee if withdrawn within %num%h":"unstaking fee if withdrawn within %num%h","Earn %asset%":"Earn %asset%","Credit calculation starts:":"Credit calculation starts:","Credit calculation ended:":"Credit calculation ended:","The start block of the current calculation period. Your average IFO CAKE Pool staking balance is calculated throughout this period.":"The start block of the current calculation period. Your average IFO CAKE Pool staking balance is calculated throughout this period.","The latest credit calculation period has ended. After the coming IFO, credits will be reset and the calculation will resume.":"The latest credit calculation period has ended. After the coming IFO, credits will be reset and the calculation will resume.","Follow us on Twitter to catch the latest news about the coming IFO.":"Follow us on Twitter to catch the latest news about the coming IFO.","Check out our Medium article for more details.":"Check out our Medium article for more details.","IFO Pool":"IFO Pool","Chart":"Chart","by":"by","You’ve successfully claimed tokens back.":"You’ve successfully claimed tokens back.","Stake CAKE to participate in IFOs":"Stake CAKE to participate in IFOs","Celebrate Christmas and New Year with us! Enjoy this wonderful NFT crafted by Chef Cecy and the winner from our #PancakeChristmas event.":"Celebrate Christmas and New Year with us! Enjoy this wonderful NFT crafted by Chef Cecy and the winner from our #PancakeChristmas event.","Chain Head Block":"Chain Head Block","Latest Subgraph Block":"Latest Subgraph Block","Slight delay":"Slight delay","Delayed":"Delayed","Fast":"Fast","Subgraph is currently experiencing delays due to BSC issues. Performance may suffer until subgraph is restored.":"Subgraph is currently experiencing delays due to BSC issues. Performance may suffer until subgraph is restored.","No issues with the subgraph.":"No issues with the subgraph.","Turn on NFT market subgraph health indicator all the time. Default is to show the indicator only when the network is delayed":"Turn on NFT market subgraph health indicator all the time. Default is to show the indicator only when the network is delayed","Subgraph Health Indicator":"Subgraph Health Indicator","Delay":"Delay","Add liquidity instead":"Add liquidity instead","The latest credit calculation period has ended. Calculation will resume upon the next period starts.":"The latest credit calculation period has ended. Calculation will resume upon the next period starts.","If the amount you commit is too small, you may not receive a meaningful amount of IFO tokens.":"If the amount you commit is too small, you may not receive a meaningful amount of IFO tokens.","SAFEMOON has been migrated to":"SAFEMOON has been migrated to","a new contract address.":"a new contract address.","Trading on the old address may result in the complete loss of your assets. For more information please refer to":"Trading on the old address may result in the complete loss of your assets. For more information please refer to","Safemoon\'s announcement":"Safemoon\'s announcement","Newest Collections":"Newest Collections","Your average per hour":"Your average per hour","At this rate, you would earn":"At this rate, you would earn","CAKE per hour: %amount%":"CAKE per hour: %amount%","per hour: ~$%amount%":"per hour: ~$%amount%","per 1d: ~$%amount%":"per 1d: ~$%amount%","per 7d: ~$%amount%":"per 7d: ~$%amount%","per 30d: ~$%amount%":"per 30d: ~$%amount%","per 365d: ~$%amount%":"per 365d: ~$%amount%","Remember that you will pay the gas fee.":"Remember that you will pay the gas fee.","ITAM has been rebranded as ITAM CUBE.":"ITAM has been rebranded as ITAM CUBE.","Please proceed to ITAM bridge to conduct a one-way swap of your ITAM tokens.":"Please proceed to ITAM bridge to conduct a one-way swap of your ITAM tokens.","All transfers of the old ITAM token will be disabled after the swap.":"All transfers of the old ITAM token will be disabled after the swap.","Crypto Cars (CCAR) has been migrated to":"Crypto Cars (CCAR) has been migrated to","the announcement.":"the announcement."}');
// EXTERNAL MODULE: ./src/contexts/Localization/helpers.ts
var helpers = __webpack_require__(5290);
;// CONCATENATED MODULE: ./src/contexts/Localization/Provider.tsx





const initialState = {
    isFetching: true,
    currentLanguage: languages.EN
};
// Export the translations directly
const languageMap = new Map();
languageMap.set(languages.EN.locale, translations_namespaceObject);
const LanguageContext = /*#__PURE__*/ (0,external_react_.createContext)(undefined);
const LanguageProvider = ({ children  })=>{
    const { 0: state , 1: setState  } = (0,external_react_.useState)(()=>{
        const codeFromStorage = (0,helpers/* getLanguageCodeFromLS */.jq)();
        return {
            ...initialState,
            currentLanguage: languages/* languages */.Mj[codeFromStorage]
        };
    });
    const { currentLanguage  } = state;
    (0,external_react_.useEffect)(()=>{
        const fetchInitialLocales = async ()=>{
            const codeFromStorage = (0,helpers/* getLanguageCodeFromLS */.jq)();
            if (codeFromStorage !== languages.EN.locale) {
                const enLocale = languageMap.get(languages.EN.locale);
                const currentLocale = await (0,helpers/* fetchLocale */.w2)(codeFromStorage);
                languageMap.set(codeFromStorage, {
                    ...enLocale,
                    ...currentLocale
                });
            }
            setState((prevState)=>({
                    ...prevState,
                    isFetching: false
                })
            );
        };
        fetchInitialLocales();
    }, [
        setState
    ]);
    const setLanguage = (0,external_react_.useCallback)(async (language)=>{
        if (!languageMap.has(language.locale)) {
            setState((prevState)=>({
                    ...prevState,
                    isFetching: true
                })
            );
            const locale = await (0,helpers/* fetchLocale */.w2)(language.locale);
            const enLocale = languageMap.get(languages.EN.locale);
            // Merge the EN locale to ensure that any locale fetched has all the keys
            languageMap.set(language.locale, {
                ...enLocale,
                ...locale
            });
            localStorage.setItem(helpers/* LS_KEY */.SH, language.locale);
            setState((prevState)=>({
                    ...prevState,
                    isFetching: false,
                    currentLanguage: language
                })
            );
        } else {
            localStorage.setItem(helpers/* LS_KEY */.SH, language.locale);
            setState((prevState)=>({
                    ...prevState,
                    isFetching: false,
                    currentLanguage: language
                })
            );
        }
    }, []);
    const translate = (0,external_react_.useCallback)((key, data)=>{
        const translationSet = languageMap.has(currentLanguage.locale) ? languageMap.get(currentLanguage.locale) : languageMap.get(languages.EN.locale);
        const translatedText = translationSet[key] || key;
        // Check the existence of at least one combination of %%, separated by 1 or more non space characters
        const includesVariable = translatedText.match(/%\S+?%/gm);
        if (includesVariable && data) {
            let interpolatedText = translatedText;
            Object.keys(data).forEach((dataKey)=>{
                const templateKey = new RegExp(`%${dataKey}%`, 'g');
                interpolatedText = interpolatedText.replace(templateKey, data[dataKey].toString());
            });
            return interpolatedText;
        }
        return translatedText;
    }, [
        currentLanguage
    ]);
    return(/*#__PURE__*/ jsx_runtime_.jsx(LanguageContext.Provider, {
        value: {
            ...state,
            setLanguage,
            t: translate
        },
        children: children
    }));
};

;// CONCATENATED MODULE: ./src/contexts/Localization/useTranslation.ts


const useTranslation = ()=>{
    const languageContext = (0,external_react_.useContext)(LanguageContext);
    if (languageContext === undefined) {
        throw new Error('Language context is undefined');
    }
    return languageContext;
};
/* harmony default export */ const Localization_useTranslation = (useTranslation);

;// CONCATENATED MODULE: ./src/contexts/Localization/index.tsx




/***/ }),

/***/ 4011:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _web3_react_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(8054);
/* harmony import */ var _web3_react_core__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_web3_react_core__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var utils_providers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5922);



/**
 * Provides a web3 provider with or without user's signer
 * Recreate web3 instance only if the provider change
 */ const useActiveWeb3React = ()=>{
    const { library , chainId , ...web3React } = (0,_web3_react_core__WEBPACK_IMPORTED_MODULE_1__.useWeb3React)();
    const refEth = (0,react__WEBPACK_IMPORTED_MODULE_0__.useRef)(library);
    const { 0: provider , 1: setProvider  } = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(library || utils_providers__WEBPACK_IMPORTED_MODULE_2__/* .simpleRpcProvider */ .J);
    (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(()=>{
        if (library !== refEth.current) {
            setProvider(library || utils_providers__WEBPACK_IMPORTED_MODULE_2__/* .simpleRpcProvider */ .J);
            refEth.current = library;
        }
    }, [
        library
    ]);
    return {
        library: provider,
        chainId: chainId ?? parseInt("56", 10),
        ...web3React
    };
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (useActiveWeb3React);


/***/ }),

/***/ 4915:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "_G": () => (/* binding */ PairState),
/* harmony export */   "z$": () => (/* binding */ usePairs)
/* harmony export */ });
/* unused harmony export usePair */
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(543);
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var config_abi_IPancakePair_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3725);
/* harmony import */ var _ethersproject_abi__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6187);
/* harmony import */ var _ethersproject_abi__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_ethersproject_abi__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var hooks_useActiveWeb3React__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4011);
/* harmony import */ var _state_multicall_hooks__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1001);
/* harmony import */ var _utils_wrappedCurrency__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3854);







const PAIR_INTERFACE = new _ethersproject_abi__WEBPACK_IMPORTED_MODULE_3__.Interface(config_abi_IPancakePair_json__WEBPACK_IMPORTED_MODULE_2__);
var PairState;
(function(PairState) {
    PairState[PairState["LOADING"] = 0] = "LOADING";
    PairState[PairState["NOT_EXISTS"] = 1] = "NOT_EXISTS";
    PairState[PairState["EXISTS"] = 2] = "EXISTS";
    PairState[PairState["INVALID"] = 3] = "INVALID";
})(PairState || (PairState = {}));
function usePairs(currencies) {
    const { chainId  } = (0,hooks_useActiveWeb3React__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z)();
    const tokens = (0,react__WEBPACK_IMPORTED_MODULE_1__.useMemo)(()=>currencies.map(([currencyA, currencyB])=>[
                (0,_utils_wrappedCurrency__WEBPACK_IMPORTED_MODULE_6__/* .wrappedCurrency */ .pu)(currencyA, chainId),
                (0,_utils_wrappedCurrency__WEBPACK_IMPORTED_MODULE_6__/* .wrappedCurrency */ .pu)(currencyB, chainId), 
            ]
        )
    , [
        chainId,
        currencies
    ]);
    const pairAddresses = (0,react__WEBPACK_IMPORTED_MODULE_1__.useMemo)(()=>tokens.map(([tokenA, tokenB])=>{
            return tokenA && tokenB && !tokenA.equals(tokenB) ? _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Pair.getAddress(tokenA, tokenB) : undefined;
        })
    , [
        tokens
    ]);
    const results = (0,_state_multicall_hooks__WEBPACK_IMPORTED_MODULE_5__/* .useMultipleContractSingleData */ ._Y)(pairAddresses, PAIR_INTERFACE, 'getReserves');
    return (0,react__WEBPACK_IMPORTED_MODULE_1__.useMemo)(()=>{
        return results.map((result, i)=>{
            const { result: reserves , loading  } = result;
            const tokenA = tokens[i][0];
            const tokenB = tokens[i][1];
            if (loading) return [
                PairState.LOADING,
                null
            ];
            if (!tokenA || !tokenB || tokenA.equals(tokenB)) return [
                PairState.INVALID,
                null
            ];
            if (!reserves) return [
                PairState.NOT_EXISTS,
                null
            ];
            const { reserve0 , reserve1  } = reserves;
            const [token0, token1] = tokenA.sortsBefore(tokenB) ? [
                tokenA,
                tokenB
            ] : [
                tokenB,
                tokenA
            ];
            return [
                PairState.EXISTS,
                new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Pair(new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.TokenAmount(token0, reserve0.toString()), new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.TokenAmount(token1, reserve1.toString())), 
            ];
        });
    }, [
        results,
        tokens
    ]);
}
function usePair(tokenA, tokenB) {
    return usePairs([
        [
            tokenA,
            tokenB
        ]
    ])[0];
}


/***/ }),

/***/ 7063:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "hd": () => (/* binding */ usePollBlockNumber),
/* harmony export */   "je": () => (/* binding */ useCurrentBlock)
/* harmony export */ });
/* unused harmony exports useBlock, useInitialBlock */
/* harmony import */ var config_constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3862);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6022);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var state__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4211);
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(549);
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(swr__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var utils_providers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5922);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3139);






const REFRESH_BLOCK_INTERVAL = 6000;
const usePollBlockNumber = ()=>{
    const dispatch = (0,state__WEBPACK_IMPORTED_MODULE_2__/* .useAppDispatch */ .TL)();
    const { data  } = swr__WEBPACK_IMPORTED_MODULE_3___default()([
        'blockNumber'
    ], async ()=>{
        const blockNumber = await utils_providers__WEBPACK_IMPORTED_MODULE_4__/* .simpleRpcProvider.getBlockNumber */ .J.getBlockNumber();
        dispatch((0,___WEBPACK_IMPORTED_MODULE_5__/* .setBlock */ .sk)(blockNumber));
        return blockNumber;
    }, {
        refreshInterval: REFRESH_BLOCK_INTERVAL
    });
    swr__WEBPACK_IMPORTED_MODULE_3___default()([
        config_constants__WEBPACK_IMPORTED_MODULE_0__/* .FAST_INTERVAL */ .sR,
        'blockNumber'
    ], async ()=>{
        return data;
    }, {
        refreshInterval: config_constants__WEBPACK_IMPORTED_MODULE_0__/* .FAST_INTERVAL */ .sR
    });
    swr__WEBPACK_IMPORTED_MODULE_3___default()([
        config_constants__WEBPACK_IMPORTED_MODULE_0__/* .SLOW_INTERVAL */ .KI,
        'blockNumber'
    ], async ()=>{
        return data;
    }, {
        refreshInterval: config_constants__WEBPACK_IMPORTED_MODULE_0__/* .SLOW_INTERVAL */ .KI
    });
};
const useBlock = ()=>{
    return useSelector((state)=>state.block
    );
};
const useCurrentBlock = ()=>{
    return (0,react_redux__WEBPACK_IMPORTED_MODULE_1__.useSelector)((state)=>state.block.currentBlock
    );
};
const useInitialBlock = ()=>{
    return useSelector((state)=>state.block.initialBlock
    );
};


/***/ }),

/***/ 3139:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "sk": () => (/* binding */ setBlock),
/* harmony export */   "ZP": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* unused harmony export blockSlice */
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5184);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);

const initialState = {
    currentBlock: 0,
    initialBlock: 0
};
const blockSlice = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createSlice)({
    name: 'Block',
    initialState,
    reducers: {
        setBlock: (state, action)=>{
            if (state.initialBlock === 0) {
                state.initialBlock = action.payload;
            }
            state.currentBlock = action.payload;
        }
    }
});
// Actions
const { setBlock  } = blockSlice.actions;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (blockSlice.reducer);


/***/ }),

/***/ 7802:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "ZP": () => (/* binding */ farms),
  "eG": () => (/* binding */ fetchFarmsPublicDataAsync)
});

// UNUSED EXPORTS: farmsSlice, fetchFarmUserDataAsync, nonArchivedFarms

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: external "fast-json-stable-stringify"
var external_fast_json_stable_stringify_ = __webpack_require__(4175);
var external_fast_json_stable_stringify_default = /*#__PURE__*/__webpack_require__.n(external_fast_json_stable_stringify_);
// EXTERNAL MODULE: ./src/config/constants/farms.ts
var constants_farms = __webpack_require__(1115);
;// CONCATENATED MODULE: ./src/utils/farmHelpers.ts
const ARCHIVED_FARMS_START_PID = 139;
const ARCHIVED_FARMS_END_PID = 250;
const isArchivedPid = (pid)=>pid >= ARCHIVED_FARMS_START_PID && pid <= ARCHIVED_FARMS_END_PID
;
/* harmony default export */ const farmHelpers = (isArchivedPid);

// EXTERNAL MODULE: ./src/config/constants/tokens.ts
var tokens = __webpack_require__(9748);
;// CONCATENATED MODULE: ./src/config/constants/priceHelperLps.ts

const priceHelperLps = [
    /**
   * These LPs are just used to help with price calculation for MasterChef LPs (farms.ts).
   * This list is added to the MasterChefLps and passed to fetchFarm. The calls to get contract information about the token/quoteToken in the LP are still made.
   * The absence of a PID means the masterchef contract calls are skipped for this farm.
   * Prices are then fetched for all farms (masterchef + priceHelperLps).
   * Before storing to redux, farms without a PID are filtered out.
   */ {
        pid: null,
        lpSymbol: 'QSD-BNB LP',
        lpAddresses: {
            97: '',
            56: '0x7b3ae32eE8C532016f3E31C8941D937c59e055B9'
        },
        token: tokens/* default.qsd */.ZP.qsd,
        quoteToken: tokens/* default.wbnb */.ZP.wbnb
    }, 
];
/* harmony default export */ const constants_priceHelperLps = (priceHelperLps);

// EXTERNAL MODULE: external "bignumber.js"
var external_bignumber_js_ = __webpack_require__(4215);
var external_bignumber_js_default = /*#__PURE__*/__webpack_require__.n(external_bignumber_js_);
// EXTERNAL MODULE: ./src/utils/bigNumber.ts
var bigNumber = __webpack_require__(5128);
// EXTERNAL MODULE: ./src/config/abi/erc20.json
var erc20 = __webpack_require__(3324);
// EXTERNAL MODULE: external "lodash/chunk"
var chunk_ = __webpack_require__(221);
var chunk_default = /*#__PURE__*/__webpack_require__.n(chunk_);
// EXTERNAL MODULE: ./src/utils/addressHelpers.ts + 1 modules
var addressHelpers = __webpack_require__(5878);
// EXTERNAL MODULE: ./src/utils/multicall.ts
var multicall = __webpack_require__(1144);
;// CONCATENATED MODULE: ./src/state/farms/fetchPublicFarmData.ts




const fetchFarmCalls = (farm)=>{
    const { lpAddresses , token , quoteToken  } = farm;
    const lpAddress = (0,addressHelpers/* getAddress */.Kn)(lpAddresses);
    return [
        // Balance of token in the LP contract
        {
            address: token.address,
            name: 'balanceOf',
            params: [
                lpAddress
            ]
        },
        // Balance of quote token on LP contract
        {
            address: quoteToken.address,
            name: 'balanceOf',
            params: [
                lpAddress
            ]
        },
        // Balance of LP tokens in the master chef contract
        {
            address: lpAddress,
            name: 'balanceOf',
            params: [
                (0,addressHelpers/* getMasterChefAddress */.Oc)()
            ]
        },
        // Total supply of LP tokens
        {
            address: lpAddress,
            name: 'totalSupply'
        },
        // Token decimals
        {
            address: token.address,
            name: 'decimals'
        },
        // Quote token decimals
        {
            address: quoteToken.address,
            name: 'decimals'
        }, 
    ];
};
const fetchPublicFarmsData = async (farms)=>{
    const farmCalls = farms.flatMap((farm)=>fetchFarmCalls(farm)
    );
    const chunkSize = farmCalls.length / farms.length;
    const farmMultiCallResult = await (0,multicall/* multicallv2 */.v)(erc20, farmCalls);
    return chunk_default()(farmMultiCallResult, chunkSize);
};

// EXTERNAL MODULE: ./src/config/abi/masterchef.json
var masterchef = __webpack_require__(4951);
;// CONCATENATED MODULE: ./src/state/farms/fetchMasterChefData.ts




const fetchMasterChefFarmCalls = (farm)=>{
    const { pid  } = farm;
    return pid || pid === 0 ? [
        {
            address: (0,addressHelpers/* getMasterChefAddress */.Oc)(),
            name: 'poolInfo',
            params: [
                pid
            ]
        },
        {
            address: (0,addressHelpers/* getMasterChefAddress */.Oc)(),
            name: 'totalAllocPoint'
        }, 
    ] : [
        null,
        null
    ];
};
const fetchMasterChefData = async (farms)=>{
    const masterChefCalls = farms.map((farm)=>fetchMasterChefFarmCalls(farm)
    );
    const chunkSize = masterChefCalls.flat().length / farms.length;
    const masterChefAggregatedCalls = masterChefCalls.filter((masterChefCall)=>masterChefCall[0] !== null && masterChefCall[1] !== null
    ).flat();
    const masterChefMultiCallResult = await (0,multicall/* multicallv2 */.v)(masterchef, masterChefAggregatedCalls);
    const masterChefChunkedResultRaw = chunk_default()(masterChefMultiCallResult, chunkSize);
    let masterChefChunkedResultCounter = 0;
    return masterChefCalls.map((masterChefCall)=>{
        if (masterChefCall[0] === null && masterChefCall[1] === null) {
            return [
                null,
                null
            ];
        }
        const data = masterChefChunkedResultRaw[masterChefChunkedResultCounter];
        masterChefChunkedResultCounter++;
        return data;
    });
};

;// CONCATENATED MODULE: ./src/state/farms/fetchFarms.ts




const fetchFarms = async (farmsToFetch)=>{
    const farmResult = await fetchPublicFarmsData(farmsToFetch);
    const masterChefResult = await fetchMasterChefData(farmsToFetch);
    return farmsToFetch.map((farm, index)=>{
        const [tokenBalanceLP, quoteTokenBalanceLP, lpTokenBalanceMC, lpTotalSupply, tokenDecimals, quoteTokenDecimals] = farmResult[index];
        const [info, totalAllocPoint] = masterChefResult[index];
        // Ratio in % of LP tokens that are staked in the MC, vs the total number in circulation
        const lpTokenRatio = new (external_bignumber_js_default())(lpTokenBalanceMC).div(new (external_bignumber_js_default())(lpTotalSupply));
        // Raw amount of token in the LP, including those not staked
        const tokenAmountTotal = new (external_bignumber_js_default())(tokenBalanceLP).div(bigNumber/* BIG_TEN.pow */.xp.pow(tokenDecimals));
        const quoteTokenAmountTotal = new (external_bignumber_js_default())(quoteTokenBalanceLP).div(bigNumber/* BIG_TEN.pow */.xp.pow(quoteTokenDecimals));
        // Amount of quoteToken in the LP that are staked in the MC
        const quoteTokenAmountMc = quoteTokenAmountTotal.times(lpTokenRatio);
        // Total staked in LP, in quote token value
        const lpTotalInQuoteToken = quoteTokenAmountMc.times(new (external_bignumber_js_default())(2));
        const allocPoint = info ? new (external_bignumber_js_default())(info.allocPoint?._hex) : bigNumber/* BIG_ZERO */.HW;
        const poolWeight = totalAllocPoint ? allocPoint.div(new (external_bignumber_js_default())(totalAllocPoint)) : bigNumber/* BIG_ZERO */.HW;
        return {
            ...farm,
            token: farm.token,
            quoteToken: farm.quoteToken,
            tokenAmountTotal: tokenAmountTotal.toJSON(),
            lpTotalSupply: new (external_bignumber_js_default())(lpTotalSupply).toJSON(),
            lpTotalInQuoteToken: lpTotalInQuoteToken.toJSON(),
            tokenPriceVsQuote: quoteTokenAmountTotal.div(tokenAmountTotal).toJSON(),
            poolWeight: poolWeight.toJSON(),
            multiplier: `${allocPoint.div(100).toString()}X`
        };
    });
};
/* harmony default export */ const farms_fetchFarms = (fetchFarms);

;// CONCATENATED MODULE: ./src/utils/farmsPriceHelpers.ts
/**
 * Returns the first farm with a quote token that matches from an array of preferred quote tokens
 * @param farms Array of farms
 * @param preferredQuoteTokens Array of preferred quote tokens
 * @returns A preferred farm, if found - or the first element of the farms array
 */ const filterFarmsByQuoteToken = (farms, preferredQuoteTokens = [
    'BUSD',
    'WBNB'
])=>{
    const preferredFarm = farms.find((farm)=>{
        return preferredQuoteTokens.some((quoteToken)=>{
            return farm.quoteToken.symbol === quoteToken;
        });
    });
    return preferredFarm || farms[0];
};
/* harmony default export */ const farmsPriceHelpers = ((/* unused pure expression or super */ null && (filterFarmsByQuoteToken)));

;// CONCATENATED MODULE: ./src/state/farms/getFarmsPrices.ts




const getFarmFromTokenSymbol = (farms, tokenSymbol, preferredQuoteTokens)=>{
    const farmsWithTokenSymbol = farms.filter((farm)=>farm.token.symbol === tokenSymbol
    );
    const filteredFarm = filterFarmsByQuoteToken(farmsWithTokenSymbol, preferredQuoteTokens);
    return filteredFarm;
};
const getFarmBaseTokenPrice = (farm, quoteTokenFarm, bnbPriceBusd)=>{
    const hasTokenPriceVsQuote = Boolean(farm.tokenPriceVsQuote);
    if (farm.quoteToken.symbol === tokens/* default.busd.symbol */.ZP.busd.symbol) {
        return hasTokenPriceVsQuote ? new (external_bignumber_js_default())(farm.tokenPriceVsQuote) : bigNumber/* BIG_ZERO */.HW;
    }
    if (farm.quoteToken.symbol === tokens/* default.wbnb.symbol */.ZP.wbnb.symbol) {
        return hasTokenPriceVsQuote ? bnbPriceBusd.times(farm.tokenPriceVsQuote) : bigNumber/* BIG_ZERO */.HW;
    }
    // We can only calculate profits without a quoteTokenFarm for BUSD/BNB farms
    if (!quoteTokenFarm) {
        return bigNumber/* BIG_ZERO */.HW;
    }
    // Possible alternative farm quoteTokens:
    // UST (i.e. MIR-UST), pBTC (i.e. PNT-pBTC), BTCB (i.e. bBADGER-BTCB), ETH (i.e. SUSHI-ETH)
    // If the farm's quote token isn't BUSD or WBNB, we then use the quote token, of the original farm's quote token
    // i.e. for farm PNT - pBTC we use the pBTC farm's quote token - BNB, (pBTC - BNB)
    // from the BNB - pBTC price, we can calculate the PNT - BUSD price
    if (quoteTokenFarm.quoteToken.symbol === tokens/* default.wbnb.symbol */.ZP.wbnb.symbol) {
        const quoteTokenInBusd = bnbPriceBusd.times(quoteTokenFarm.tokenPriceVsQuote);
        return hasTokenPriceVsQuote && quoteTokenInBusd ? new (external_bignumber_js_default())(farm.tokenPriceVsQuote).times(quoteTokenInBusd) : bigNumber/* BIG_ZERO */.HW;
    }
    if (quoteTokenFarm.quoteToken.symbol === tokens/* default.busd.symbol */.ZP.busd.symbol) {
        const quoteTokenInBusd = quoteTokenFarm.tokenPriceVsQuote;
        return hasTokenPriceVsQuote && quoteTokenInBusd ? new (external_bignumber_js_default())(farm.tokenPriceVsQuote).times(quoteTokenInBusd) : bigNumber/* BIG_ZERO */.HW;
    }
    // Catch in case token does not have immediate or once-removed BUSD/WBNB quoteToken
    return bigNumber/* BIG_ZERO */.HW;
};
const getFarmQuoteTokenPrice = (farm, quoteTokenFarm, bnbPriceBusd)=>{
    if (farm.quoteToken.symbol === 'BUSD') {
        return bigNumber/* BIG_ONE */.cQ;
    }
    if (farm.quoteToken.symbol === 'WBNB') {
        return bnbPriceBusd;
    }
    if (!quoteTokenFarm) {
        return bigNumber/* BIG_ZERO */.HW;
    }
    if (quoteTokenFarm.quoteToken.symbol === 'WBNB') {
        return quoteTokenFarm.tokenPriceVsQuote ? bnbPriceBusd.times(quoteTokenFarm.tokenPriceVsQuote) : bigNumber/* BIG_ZERO */.HW;
    }
    if (quoteTokenFarm.quoteToken.symbol === 'BUSD') {
        return quoteTokenFarm.tokenPriceVsQuote ? new (external_bignumber_js_default())(quoteTokenFarm.tokenPriceVsQuote) : bigNumber/* BIG_ZERO */.HW;
    }
    return bigNumber/* BIG_ZERO */.HW;
};
const getFarmsPrices = (farms)=>{
    const bnbBusdFarm = farms.find((farm)=>farm.pid === 252
    );
    const bnbPriceBusd = bnbBusdFarm.tokenPriceVsQuote ? bigNumber/* BIG_ONE.div */.cQ.div(bnbBusdFarm.tokenPriceVsQuote) : bigNumber/* BIG_ZERO */.HW;
    const farmsWithPrices = farms.map((farm)=>{
        const quoteTokenFarm = getFarmFromTokenSymbol(farms, farm.quoteToken.symbol);
        const tokenPriceBusd = getFarmBaseTokenPrice(farm, quoteTokenFarm, bnbPriceBusd);
        const quoteTokenPriceBusd = getFarmQuoteTokenPrice(farm, quoteTokenFarm, bnbPriceBusd);
        return {
            ...farm,
            tokenPriceBusd: tokenPriceBusd.toJSON(),
            quoteTokenPriceBusd: quoteTokenPriceBusd.toJSON()
        };
    });
    return farmsWithPrices;
};
/* harmony default export */ const farms_getFarmsPrices = (getFarmsPrices);

;// CONCATENATED MODULE: ./src/state/farms/fetchFarmUser.ts





const fetchFarmUserAllowances = async (account, farmsToFetch)=>{
    const masterChefAddress = (0,addressHelpers/* getMasterChefAddress */.Oc)();
    const calls = farmsToFetch.map((farm)=>{
        const lpContractAddress = (0,addressHelpers/* getAddress */.Kn)(farm.lpAddresses);
        return {
            address: lpContractAddress,
            name: 'allowance',
            params: [
                account,
                masterChefAddress
            ]
        };
    });
    const rawLpAllowances = await (0,multicall/* default */.Z)(erc20, calls);
    const parsedLpAllowances = rawLpAllowances.map((lpBalance)=>{
        return new (external_bignumber_js_default())(lpBalance).toJSON();
    });
    return parsedLpAllowances;
};
const fetchFarmUserTokenBalances = async (account, farmsToFetch)=>{
    const calls = farmsToFetch.map((farm)=>{
        const lpContractAddress = (0,addressHelpers/* getAddress */.Kn)(farm.lpAddresses);
        return {
            address: lpContractAddress,
            name: 'balanceOf',
            params: [
                account
            ]
        };
    });
    const rawTokenBalances = await (0,multicall/* default */.Z)(erc20, calls);
    const parsedTokenBalances = rawTokenBalances.map((tokenBalance)=>{
        return new (external_bignumber_js_default())(tokenBalance).toJSON();
    });
    return parsedTokenBalances;
};
const fetchFarmUserStakedBalances = async (account, farmsToFetch)=>{
    const masterChefAddress = (0,addressHelpers/* getMasterChefAddress */.Oc)();
    const calls = farmsToFetch.map((farm)=>{
        return {
            address: masterChefAddress,
            name: 'userInfo',
            params: [
                farm.pid,
                account
            ]
        };
    });
    const rawStakedBalances = await (0,multicall/* default */.Z)(masterchef, calls);
    const parsedStakedBalances = rawStakedBalances.map((stakedBalance)=>{
        return new (external_bignumber_js_default())(stakedBalance[0]._hex).toJSON();
    });
    return parsedStakedBalances;
};
const fetchFarmUserEarnings = async (account, farmsToFetch)=>{
    const masterChefAddress = (0,addressHelpers/* getMasterChefAddress */.Oc)();
    const calls = farmsToFetch.map((farm)=>{
        return {
            address: masterChefAddress,
            name: 'pendingCake',
            params: [
                farm.pid,
                account
            ]
        };
    });
    const rawEarnings = await (0,multicall/* default */.Z)(masterchef, calls);
    const parsedEarnings = rawEarnings.map((earnings)=>{
        return new (external_bignumber_js_default())(earnings).toJSON();
    });
    return parsedEarnings;
};

;// CONCATENATED MODULE: ./src/state/farms/index.ts








const noAccountFarmConfig = constants_farms/* default.map */.Z.map((farm)=>({
        ...farm,
        userData: {
            allowance: '0',
            tokenBalance: '0',
            stakedBalance: '0',
            earnings: '0'
        }
    })
);
const initialState = {
    data: noAccountFarmConfig,
    loadArchivedFarmsData: false,
    userDataLoaded: false,
    loadingKeys: {}
};
const nonArchivedFarms = constants_farms/* default.filter */.Z.filter(({ pid  })=>!farmHelpers(pid)
);
// Async thunks
const fetchFarmsPublicDataAsync = (0,toolkit_.createAsyncThunk)('farms/fetchFarmsPublicDataAsync', async (pids)=>{
    const farmsToFetch = constants_farms/* default.filter */.Z.filter((farmConfig)=>pids.includes(farmConfig.pid)
    );
    // Add price helper farms
    const farmsWithPriceHelpers = farmsToFetch.concat(constants_priceHelperLps);
    const farms = await farms_fetchFarms(farmsWithPriceHelpers);
    const farmsWithPrices = farms_getFarmsPrices(farms);
    // Filter out price helper LP config farms
    const farmsWithoutHelperLps = farmsWithPrices.filter((farm)=>{
        return farm.pid || farm.pid === 0;
    });
    return farmsWithoutHelperLps;
}, {
    condition: (arg, { getState  })=>{
        const { farms  } = getState();
        if (farms.loadingKeys[external_fast_json_stable_stringify_default()({
            type: fetchFarmsPublicDataAsync.typePrefix,
            arg
        })]) {
            console.debug('farms action is fetching, skipping here');
            return false;
        }
        return true;
    }
});
const fetchFarmUserDataAsync = (0,toolkit_.createAsyncThunk)('farms/fetchFarmUserDataAsync', async ({ account , pids  })=>{
    const farmsToFetch = constants_farms/* default.filter */.Z.filter((farmConfig)=>pids.includes(farmConfig.pid)
    );
    const userFarmAllowances = await fetchFarmUserAllowances(account, farmsToFetch);
    const userFarmTokenBalances = await fetchFarmUserTokenBalances(account, farmsToFetch);
    const userStakedBalances = await fetchFarmUserStakedBalances(account, farmsToFetch);
    const userFarmEarnings = await fetchFarmUserEarnings(account, farmsToFetch);
    return userFarmAllowances.map((farmAllowance, index)=>{
        return {
            pid: farmsToFetch[index].pid,
            allowance: userFarmAllowances[index],
            tokenBalance: userFarmTokenBalances[index],
            stakedBalance: userStakedBalances[index],
            earnings: userFarmEarnings[index]
        };
    });
}, {
    condition: (arg, { getState  })=>{
        const { farms  } = getState();
        if (farms.loadingKeys[external_fast_json_stable_stringify_default()({
            type: fetchFarmUserDataAsync.typePrefix,
            arg
        })]) {
            console.debug('farms user action is fetching, skipping here');
            return false;
        }
        return true;
    }
});
const serializeLoadingKey = (action, suffix)=>{
    const type = action.type.split(`/${suffix}`)[0];
    return external_fast_json_stable_stringify_default()({
        arg: action.meta.arg,
        type
    });
};
const farmsSlice = (0,toolkit_.createSlice)({
    name: 'Farms',
    initialState,
    reducers: {},
    extraReducers: (builder)=>{
        // Update farms with live data
        builder.addCase(fetchFarmsPublicDataAsync.fulfilled, (state, action)=>{
            state.data = state.data.map((farm)=>{
                const liveFarmData = action.payload.find((farmData)=>farmData.pid === farm.pid
                );
                return {
                    ...farm,
                    ...liveFarmData
                };
            });
        });
        // Update farms with user data
        builder.addCase(fetchFarmUserDataAsync.fulfilled, (state, action)=>{
            action.payload.forEach((userDataEl)=>{
                const { pid  } = userDataEl;
                const index = state.data.findIndex((farm)=>farm.pid === pid
                );
                state.data[index] = {
                    ...state.data[index],
                    userData: userDataEl
                };
            });
            state.userDataLoaded = true;
        });
        builder.addMatcher((0,toolkit_.isAnyOf)(fetchFarmUserDataAsync.pending, fetchFarmsPublicDataAsync.pending), (state, action)=>{
            state.loadingKeys[serializeLoadingKey(action, 'pending')] = true;
        });
        builder.addMatcher((0,toolkit_.isAnyOf)(fetchFarmUserDataAsync.fulfilled, fetchFarmsPublicDataAsync.fulfilled), (state, action)=>{
            state.loadingKeys[serializeLoadingKey(action, 'fulfilled')] = false;
        });
        builder.addMatcher((0,toolkit_.isAnyOf)(fetchFarmsPublicDataAsync.rejected, fetchFarmUserDataAsync.rejected), (state, action)=>{
            state.loadingKeys[serializeLoadingKey(action, 'rejected')] = false;
        });
    }
});
/* harmony default export */ const farms = (farmsSlice.reducer);


/***/ }),

/***/ 4211:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Dj": () => (/* binding */ persistor),
  "TL": () => (/* binding */ useAppDispatch),
  "oR": () => (/* binding */ useStore)
});

// UNUSED EXPORTS: default, initializeStore

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(6022);
// EXTERNAL MODULE: external "redux-persist"
var external_redux_persist_ = __webpack_require__(4161);
// EXTERNAL MODULE: external "redux-persist/lib/storage"
var storage_ = __webpack_require__(8936);
var storage_default = /*#__PURE__*/__webpack_require__.n(storage_);
// EXTERNAL MODULE: ./src/state/block/index.ts
var block = __webpack_require__(3139);
;// CONCATENATED MODULE: ./src/state/burn/actions.ts

var Field;
(function(Field) {
    Field["LIQUIDITY_PERCENT"] = "LIQUIDITY_PERCENT";
    Field["LIQUIDITY"] = "LIQUIDITY";
    Field["CURRENCY_A"] = "CURRENCY_A";
    Field["CURRENCY_B"] = "CURRENCY_B";
})(Field || (Field = {}));
const typeInput = (0,toolkit_.createAction)('burn/typeInputBurn');

;// CONCATENATED MODULE: ./src/state/burn/reducer.ts


const initialState = {
    independentField: Field.LIQUIDITY_PERCENT,
    typedValue: '0'
};
/* harmony default export */ const reducer = ((0,toolkit_.createReducer)(initialState, (builder)=>builder.addCase(typeInput, (state, { payload: { field , typedValue  }  })=>{
        return {
            ...state,
            independentField: field,
            typedValue
        };
    })
));

// EXTERNAL MODULE: ./src/state/farms/index.ts + 8 modules
var farms = __webpack_require__(7802);
;// CONCATENATED MODULE: ./src/state/global/actions.ts

// fired once when the app reloads but before the app renders
// allows any updates to be applied to store data loaded from localStorage
const updateVersion = (0,toolkit_.createAction)('global/updateVersion');
/* harmony default export */ const actions = ((/* unused pure expression or super */ null && (updateVersion)));

// EXTERNAL MODULE: ./src/state/info/actions.ts
var info_actions = __webpack_require__(4522);
;// CONCATENATED MODULE: ./src/state/info/index.ts
/* eslint-disable no-param-reassign */ 

const info_initialState = {
    protocol: {
        overview: undefined,
        chartData: undefined,
        transactions: undefined
    },
    pools: {
        byAddress: {}
    },
    tokens: {
        byAddress: {}
    }
};
/* harmony default export */ const info = ((0,toolkit_.createReducer)(info_initialState, (builder)=>builder// Protocol actions
    .addCase(info_actions/* updateProtocolData */.Uo, (state, { payload: { protocolData  }  })=>{
        state.protocol.overview = protocolData;
    }).addCase(info_actions/* updateProtocolChartData */.bj, (state, { payload: { chartData  }  })=>{
        state.protocol.chartData = chartData;
    }).addCase(info_actions/* updateProtocolTransactions */.oz, (state, { payload: { transactions  }  })=>{
        state.protocol.transactions = transactions;
    })// Pools actions
    .addCase(info_actions/* updatePoolData */.Bp, (state, { payload: { pools  }  })=>{
        pools.forEach((poolData)=>{
            state.pools.byAddress[poolData.address] = {
                ...state.pools.byAddress[poolData.address],
                data: poolData
            };
        });
    }).addCase(info_actions/* addPoolKeys */.iF, (state, { payload: { poolAddresses  }  })=>{
        poolAddresses.forEach((address)=>{
            if (!state.pools.byAddress[address]) {
                state.pools.byAddress[address] = {
                    data: undefined,
                    chartData: undefined,
                    transactions: undefined
                };
            }
        });
    }).addCase(info_actions/* updatePoolChartData */.jw, (state, { payload: { poolAddress , chartData  }  })=>{
        state.pools.byAddress[poolAddress] = {
            ...state.pools.byAddress[poolAddress],
            chartData
        };
    }).addCase(info_actions/* updatePoolTransactions */.oG, (state, { payload: { poolAddress , transactions  }  })=>{
        state.pools.byAddress[poolAddress] = {
            ...state.pools.byAddress[poolAddress],
            transactions
        };
    })// Tokens actions
    .addCase(info_actions/* updateTokenData */.I6, (state, { payload: { tokens  }  })=>{
        tokens.forEach((tokenData)=>{
            state.tokens.byAddress[tokenData.address] = {
                ...state.tokens.byAddress[tokenData.address],
                data: tokenData
            };
        });
    }).addCase(info_actions/* addTokenKeys */.uP, (state, { payload: { tokenAddresses  }  })=>{
        tokenAddresses.forEach((address)=>{
            if (!state.tokens.byAddress[address]) {
                state.tokens.byAddress[address] = {
                    poolAddresses: undefined,
                    data: undefined,
                    chartData: undefined,
                    priceData: {},
                    transactions: undefined
                };
            }
        });
    }).addCase(info_actions/* addTokenPoolAddresses */.TV, (state, { payload: { tokenAddress , poolAddresses  }  })=>{
        state.tokens.byAddress[tokenAddress] = {
            ...state.tokens.byAddress[tokenAddress],
            poolAddresses
        };
    }).addCase(info_actions/* updateTokenChartData */.fo, (state, { payload: { tokenAddress , chartData  }  })=>{
        state.tokens.byAddress[tokenAddress] = {
            ...state.tokens.byAddress[tokenAddress],
            chartData
        };
    }).addCase(info_actions/* updateTokenTransactions */.Mw, (state, { payload: { tokenAddress , transactions  }  })=>{
        state.tokens.byAddress[tokenAddress] = {
            ...state.tokens.byAddress[tokenAddress],
            transactions
        };
    }).addCase(info_actions/* updateTokenPriceData */.Db, (state, { payload: { tokenAddress , secondsInterval , priceData , oldestFetchedTimestamp  }  })=>{
        state.tokens.byAddress[tokenAddress] = {
            ...state.tokens.byAddress[tokenAddress],
            priceData: {
                ...state.tokens.byAddress[tokenAddress]?.priceData,
                [secondsInterval]: priceData,
                oldestFetchedTimestamp
            }
        };
    })
));

// EXTERNAL MODULE: external "@uniswap/token-lists"
var token_lists_ = __webpack_require__(1554);
// EXTERNAL MODULE: ./src/config/constants/lists.ts
var lists = __webpack_require__(428);
// EXTERNAL MODULE: ./src/state/lists/actions.ts
var lists_actions = __webpack_require__(8412);
;// CONCATENATED MODULE: ./src/state/lists/reducer.ts





const NEW_LIST_STATE = {
    error: null,
    current: null,
    loadingRequestId: null,
    pendingUpdate: null
};
const reducer_initialState = {
    lastInitializedDefaultListOfLists: lists/* DEFAULT_LIST_OF_LISTS */.Lx,
    byUrl: {
        ...lists/* DEFAULT_LIST_OF_LISTS.concat */.Lx.concat(...lists/* UNSUPPORTED_LIST_URLS */.US).reduce((memo, listUrl)=>{
            memo[listUrl] = NEW_LIST_STATE;
            return memo;
        }, {})
    },
    activeListUrls: lists/* DEFAULT_ACTIVE_LIST_URLS */.c8
};
/* harmony default export */ const lists_reducer = ((0,toolkit_.createReducer)(reducer_initialState, (builder)=>builder.addCase(lists_actions/* fetchTokenList.pending */.Dn.pending, (state, { payload: { requestId , url  }  })=>{
        state.byUrl[url] = {
            current: null,
            pendingUpdate: null,
            ...state.byUrl[url],
            loadingRequestId: requestId,
            error: null
        };
    }).addCase(lists_actions/* fetchTokenList.fulfilled */.Dn.fulfilled, (state, { payload: { requestId , tokenList , url  }  })=>{
        const current = state.byUrl[url]?.current;
        const loadingRequestId = state.byUrl[url]?.loadingRequestId;
        // no-op if update does nothing
        if (current) {
            const upgradeType = (0,token_lists_.getVersionUpgrade)(current.version, tokenList.version);
            if (upgradeType === token_lists_.VersionUpgrade.NONE) return;
            if (loadingRequestId === null || loadingRequestId === requestId) {
                state.byUrl[url] = {
                    ...state.byUrl[url],
                    loadingRequestId: null,
                    error: null,
                    current,
                    pendingUpdate: tokenList
                };
            }
        } else {
            // activate if on default active
            if (lists/* DEFAULT_ACTIVE_LIST_URLS.includes */.c8.includes(url)) {
                state.activeListUrls?.push(url);
            }
            state.byUrl[url] = {
                ...state.byUrl[url],
                loadingRequestId: null,
                error: null,
                current: tokenList,
                pendingUpdate: null
            };
        }
    }).addCase(lists_actions/* fetchTokenList.rejected */.Dn.rejected, (state, { payload: { url , requestId , errorMessage  }  })=>{
        if (state.byUrl[url]?.loadingRequestId !== requestId) {
            // no-op since it's not the latest request
            return;
        }
        state.byUrl[url] = {
            ...state.byUrl[url],
            loadingRequestId: null,
            error: errorMessage,
            current: null,
            pendingUpdate: null
        };
    }).addCase(lists_actions/* addList */.$8, (state, { payload: url  })=>{
        if (!state.byUrl[url]) {
            state.byUrl[url] = NEW_LIST_STATE;
        }
    }).addCase(lists_actions/* removeList */.J_, (state, { payload: url  })=>{
        if (state.byUrl[url]) {
            delete state.byUrl[url];
        }
        // remove list from active urls if needed
        if (state.activeListUrls && state.activeListUrls.includes(url)) {
            state.activeListUrls = state.activeListUrls.filter((u)=>u !== url
            );
        }
    }).addCase(lists_actions/* enableList */.ic, (state, { payload: url  })=>{
        if (!state.byUrl[url]) {
            state.byUrl[url] = NEW_LIST_STATE;
        }
        if (state.activeListUrls && !state.activeListUrls.includes(url)) {
            state.activeListUrls.push(url);
        }
        if (!state.activeListUrls) {
            state.activeListUrls = [
                url
            ];
        }
    }).addCase(lists_actions/* disableList */.K$, (state, { payload: url  })=>{
        if (state.activeListUrls && state.activeListUrls.includes(url)) {
            state.activeListUrls = state.activeListUrls.filter((u)=>u !== url
            );
        }
    }).addCase(lists_actions/* acceptListUpdate */.xJ, (state, { payload: url  })=>{
        if (!state.byUrl[url]?.pendingUpdate) {
            throw new Error('accept list update called without pending update');
        }
        state.byUrl[url] = {
            ...state.byUrl[url],
            pendingUpdate: null,
            current: state.byUrl[url].pendingUpdate
        };
    }).addCase(updateVersion, (state)=>{
        // state loaded from localStorage, but new lists have never been initialized
        if (!state.lastInitializedDefaultListOfLists) {
            state.byUrl = reducer_initialState.byUrl;
            state.activeListUrls = reducer_initialState.activeListUrls;
        } else if (state.lastInitializedDefaultListOfLists) {
            const lastInitializedSet = state.lastInitializedDefaultListOfLists.reduce((s, l)=>s.add(l)
            , new Set());
            const newListOfListsSet = lists/* DEFAULT_LIST_OF_LISTS.reduce */.Lx.reduce((s, l)=>s.add(l)
            , new Set());
            lists/* DEFAULT_LIST_OF_LISTS.forEach */.Lx.forEach((listUrl)=>{
                if (!lastInitializedSet.has(listUrl)) {
                    state.byUrl[listUrl] = NEW_LIST_STATE;
                }
            });
            state.lastInitializedDefaultListOfLists.forEach((listUrl)=>{
                if (!newListOfListsSet.has(listUrl)) {
                    delete state.byUrl[listUrl];
                }
            });
        }
        state.lastInitializedDefaultListOfLists = lists/* DEFAULT_LIST_OF_LISTS */.Lx;
        // if no active lists, activate defaults
        if (!state.activeListUrls) {
            state.activeListUrls = lists/* DEFAULT_ACTIVE_LIST_URLS */.c8;
            // for each list on default list, initialize if needed
            lists/* DEFAULT_ACTIVE_LIST_URLS.forEach */.c8.forEach((listUrl)=>{
                if (!state.byUrl[listUrl]) {
                    state.byUrl[listUrl] = NEW_LIST_STATE;
                }
                return true;
            });
        }
    })
));

// EXTERNAL MODULE: ./src/config/constants/types.ts
var types = __webpack_require__(7971);
// EXTERNAL MODULE: external "bignumber.js"
var external_bignumber_js_ = __webpack_require__(4215);
var external_bignumber_js_default = /*#__PURE__*/__webpack_require__.n(external_bignumber_js_);
// EXTERNAL MODULE: ./src/config/abi/lotteryV2.json
var lotteryV2 = __webpack_require__(8592);
// EXTERNAL MODULE: ./src/utils/addressHelpers.ts + 1 modules
var addressHelpers = __webpack_require__(5878);
// EXTERNAL MODULE: ./src/utils/multicall.ts
var utils_multicall = __webpack_require__(1144);
// EXTERNAL MODULE: ./src/utils/contractHelpers.ts + 22 modules
var contractHelpers = __webpack_require__(1553);
// EXTERNAL MODULE: ./src/utils/bigNumber.ts
var bigNumber = __webpack_require__(5128);
;// CONCATENATED MODULE: ./src/config/constants/lottery.ts
const TICKET_LIMIT_PER_REQUEST = 2500;
const NUM_ROUNDS_TO_CHECK_FOR_REWARDS = 3;
const NUM_ROUNDS_TO_FETCH_FROM_NODES = 3;

;// CONCATENATED MODULE: ./src/state/lottery/helpers.ts









const lotteryContract = (0,contractHelpers/* getLotteryV2Contract */.yd)();
const processViewLotterySuccessResponse = (response, lotteryId)=>{
    const { status , startTime , endTime , priceTicketInCake , discountDivisor , treasuryFee , firstTicketId , lastTicketId , amountCollectedInCake , finalNumber , cakePerBracket , countWinnersPerBracket , rewardsBreakdown ,  } = response;
    const statusKey = Object.keys(types/* LotteryStatus */.p3)[status];
    const serializedCakePerBracket = cakePerBracket.map((cakeInBracket)=>(0,bigNumber/* ethersToSerializedBigNumber */.L8)(cakeInBracket)
    );
    const serializedCountWinnersPerBracket = countWinnersPerBracket.map((winnersInBracket)=>(0,bigNumber/* ethersToSerializedBigNumber */.L8)(winnersInBracket)
    );
    const serializedRewardsBreakdown = rewardsBreakdown.map((reward)=>(0,bigNumber/* ethersToSerializedBigNumber */.L8)(reward)
    );
    return {
        isLoading: false,
        lotteryId,
        status: types/* LotteryStatus */.p3[statusKey],
        startTime: startTime?.toString(),
        endTime: endTime?.toString(),
        priceTicketInCake: (0,bigNumber/* ethersToSerializedBigNumber */.L8)(priceTicketInCake),
        discountDivisor: discountDivisor?.toString(),
        treasuryFee: treasuryFee?.toString(),
        firstTicketId: firstTicketId?.toString(),
        lastTicketId: lastTicketId?.toString(),
        amountCollectedInCake: (0,bigNumber/* ethersToSerializedBigNumber */.L8)(amountCollectedInCake),
        finalNumber,
        cakePerBracket: serializedCakePerBracket,
        countWinnersPerBracket: serializedCountWinnersPerBracket,
        rewardsBreakdown: serializedRewardsBreakdown
    };
};
const processViewLotteryErrorResponse = (lotteryId)=>{
    return {
        isLoading: true,
        lotteryId,
        status: types/* LotteryStatus.PENDING */.p3.PENDING,
        startTime: '',
        endTime: '',
        priceTicketInCake: '',
        discountDivisor: '',
        treasuryFee: '',
        firstTicketId: '',
        lastTicketId: '',
        amountCollectedInCake: '',
        finalNumber: null,
        cakePerBracket: [],
        countWinnersPerBracket: [],
        rewardsBreakdown: []
    };
};
const fetchLottery = async (lotteryId)=>{
    try {
        const lotteryData = await lotteryContract.viewLottery(lotteryId);
        return processViewLotterySuccessResponse(lotteryData, lotteryId);
    } catch (error) {
        return processViewLotteryErrorResponse(lotteryId);
    }
};
const fetchMultipleLotteries = async (lotteryIds)=>{
    const calls = lotteryIds.map((id)=>({
            name: 'viewLottery',
            address: (0,addressHelpers/* getLotteryV2Address */.kN)(),
            params: [
                id
            ]
        })
    );
    try {
        const multicallRes = await (0,utils_multicall/* multicallv2 */.v)(lotteryV2, calls, {
            requireSuccess: false
        });
        const processedResponses = multicallRes.map((res, index)=>processViewLotterySuccessResponse(res[0], lotteryIds[index])
        );
        return processedResponses;
    } catch (error) {
        console.error(error);
        return calls.map((call, index)=>processViewLotteryErrorResponse(lotteryIds[index])
        );
    }
};
const fetchCurrentLotteryIdAndMaxBuy = async ()=>{
    try {
        const calls = [
            'currentLotteryId',
            'maxNumberTicketsPerBuyOrClaim'
        ].map((method)=>({
                address: (0,addressHelpers/* getLotteryV2Address */.kN)(),
                name: method
            })
        );
        const [[currentLotteryId], [maxNumberTicketsPerBuyOrClaim]] = await (0,utils_multicall/* multicallv2 */.v)(lotteryV2, calls);
        return {
            currentLotteryId: currentLotteryId ? currentLotteryId.toString() : null,
            maxNumberTicketsPerBuyOrClaim: maxNumberTicketsPerBuyOrClaim ? maxNumberTicketsPerBuyOrClaim.toString() : null
        };
    } catch (error) {
        return {
            currentLotteryId: null,
            maxNumberTicketsPerBuyOrClaim: null
        };
    }
};
const getRoundIdsArray = (currentLotteryId)=>{
    const currentIdAsInt = parseInt(currentLotteryId, 10);
    const roundIds = [];
    for(let i = 0; i < NUM_ROUNDS_TO_FETCH_FROM_NODES; i++){
        roundIds.push(currentIdAsInt - i);
    }
    return roundIds.map((roundId)=>roundId.toString()
    );
};
const useProcessLotteryResponse = (lotteryData)=>{
    const { priceTicketInCake: priceTicketInCakeAsString , discountDivisor: discountDivisorAsString , amountCollectedInCake: amountCollectedInCakeAsString ,  } = lotteryData;
    const discountDivisor = useMemo(()=>{
        return new BigNumber(discountDivisorAsString);
    }, [
        discountDivisorAsString
    ]);
    const priceTicketInCake = useMemo(()=>{
        return new BigNumber(priceTicketInCakeAsString);
    }, [
        priceTicketInCakeAsString
    ]);
    const amountCollectedInCake = useMemo(()=>{
        return new BigNumber(amountCollectedInCakeAsString);
    }, [
        amountCollectedInCakeAsString
    ]);
    return {
        isLoading: lotteryData.isLoading,
        lotteryId: lotteryData.lotteryId,
        userTickets: lotteryData.userTickets,
        status: lotteryData.status,
        startTime: lotteryData.startTime,
        endTime: lotteryData.endTime,
        priceTicketInCake,
        discountDivisor,
        treasuryFee: lotteryData.treasuryFee,
        firstTicketId: lotteryData.firstTicketId,
        lastTicketId: lotteryData.lastTicketId,
        amountCollectedInCake,
        finalNumber: lotteryData.finalNumber,
        cakePerBracket: lotteryData.cakePerBracket,
        countWinnersPerBracket: lotteryData.countWinnersPerBracket,
        rewardsBreakdown: lotteryData.rewardsBreakdown
    };
};
const hasRoundBeenClaimed = (tickets)=>{
    const claimedTickets = tickets.filter((ticket)=>ticket.status
    );
    return claimedTickets.length > 0;
};

// EXTERNAL MODULE: external "graphql-request"
var external_graphql_request_ = __webpack_require__(5805);
var external_graphql_request_default = /*#__PURE__*/__webpack_require__.n(external_graphql_request_);
// EXTERNAL MODULE: ./src/config/constants/endpoints.ts
var endpoints = __webpack_require__(5906);
;// CONCATENATED MODULE: ./src/state/lottery/getLotteriesData.ts



const MAX_LOTTERIES_REQUEST_SIZE = 100;
const applyNodeDataToLotteriesGraphResponse = (nodeData, graphResponse)=>{
    //   If no graph response - return node data
    if (graphResponse.length === 0) {
        return nodeData.map((nodeRound)=>{
            return {
                endTime: nodeRound.endTime,
                finalNumber: nodeRound.finalNumber.toString(),
                startTime: nodeRound.startTime,
                status: nodeRound.status,
                id: nodeRound.lotteryId.toString(),
                ticketPrice: nodeRound.priceTicketInCake,
                totalTickets: '',
                totalUsers: '',
                winningTickets: ''
            };
        });
    }
    // Populate all nodeRound data with supplementary graphResponse round data when available
    const nodeRoundsWithGraphData = nodeData.map((nodeRoundData)=>{
        const graphRoundData = graphResponse.find((graphResponseRound)=>graphResponseRound.id === nodeRoundData.lotteryId
        );
        return {
            endTime: nodeRoundData.endTime,
            finalNumber: nodeRoundData.finalNumber.toString(),
            startTime: nodeRoundData.startTime,
            status: nodeRoundData.status,
            id: nodeRoundData.lotteryId,
            ticketPrice: graphRoundData?.ticketPrice,
            totalTickets: graphRoundData?.totalTickets,
            totalUsers: graphRoundData?.totalUsers,
            winningTickets: graphRoundData?.winningTickets
        };
    });
    // Return the rounds with combined node + subgraph data, plus all remaining subgraph rounds.
    const [lastCombinedDataRound] = nodeRoundsWithGraphData.slice(-1);
    const lastCombinedDataRoundIndex = graphResponse.map((graphRound)=>graphRound?.id
    ).indexOf(lastCombinedDataRound?.id);
    const remainingSubgraphRounds = graphResponse ? graphResponse.splice(lastCombinedDataRoundIndex + 1) : [];
    const mergedResponse = [
        ...nodeRoundsWithGraphData,
        ...remainingSubgraphRounds
    ];
    return mergedResponse;
};
const getGraphLotteries = async (first = MAX_LOTTERIES_REQUEST_SIZE, skip = 0, where = {})=>{
    try {
        const response = await (0,external_graphql_request_.request)(endpoints/* GRAPH_API_LOTTERY */.Xr, external_graphql_request_.gql`
        query getLotteries($first: Int!, $skip: Int!, $where: Lottery_filter) {
          lotteries(first: $first, skip: $skip, where: $where, orderDirection: desc, orderBy: block) {
            id
            totalUsers
            totalTickets
            winningTickets
            status
            finalNumber
            startTime
            endTime
            ticketPrice
          }
        }
      `, {
            skip,
            first,
            where
        });
        return response.lotteries;
    } catch (error) {
        console.error(error);
        return [];
    }
};
const getLotteriesData = async (currentLotteryId)=>{
    const idsForNodesCall = getRoundIdsArray(currentLotteryId);
    const nodeData = await fetchMultipleLotteries(idsForNodesCall);
    const graphResponse = await getGraphLotteries();
    const mergedData = applyNodeDataToLotteriesGraphResponse(nodeData, graphResponse);
    return mergedData;
};
/* harmony default export */ const lottery_getLotteriesData = (getLotteriesData);

;// CONCATENATED MODULE: ./src/state/lottery/getUserTicketsData.ts


const getUserTicketsData_lotteryContract = (0,contractHelpers/* getLotteryV2Contract */.yd)();
const processRawTicketsResponse = (ticketsResponse)=>{
    const [ticketIds, ticketNumbers, ticketStatuses] = ticketsResponse;
    if (ticketIds?.length > 0) {
        return ticketIds.map((ticketId, index)=>{
            return {
                id: ticketId.toString(),
                number: ticketNumbers[index].toString(),
                status: ticketStatuses[index]
            };
        });
    }
    return [];
};
const viewUserInfoForLotteryId = async (account, lotteryId, cursor, perRequestLimit)=>{
    try {
        const data = await getUserTicketsData_lotteryContract.viewUserInfoForLotteryId(account, lotteryId, cursor, perRequestLimit);
        return processRawTicketsResponse(data);
    } catch (error) {
        console.error('viewUserInfoForLotteryId', error);
        return null;
    }
};
const fetchUserTicketsForOneRound = async (account, lotteryId)=>{
    let cursor = 0;
    let numReturned = TICKET_LIMIT_PER_REQUEST;
    const ticketData = [];
    while(numReturned === TICKET_LIMIT_PER_REQUEST){
        // eslint-disable-next-line no-await-in-loop
        const response = await viewUserInfoForLotteryId(account, lotteryId, cursor, TICKET_LIMIT_PER_REQUEST);
        cursor += TICKET_LIMIT_PER_REQUEST;
        numReturned = response.length;
        ticketData.push(...response);
    }
    return ticketData;
};
const fetchUserTicketsForMultipleRounds = async (idsToCheck, account)=>{
    const ticketsForMultipleRounds = [];
    for(let i = 0; i < idsToCheck.length; i += 1){
        const roundId = idsToCheck[i];
        // eslint-disable-next-line no-await-in-loop
        const ticketsForRound = await fetchUserTicketsForOneRound(account, roundId);
        ticketsForMultipleRounds.push({
            roundId,
            userTickets: ticketsForRound
        });
    }
    return ticketsForMultipleRounds;
};

;// CONCATENATED MODULE: ./src/state/lottery/getUserLotteryData.ts




const MAX_USER_LOTTERIES_REQUEST_SIZE = 100;
const applyNodeDataToUserGraphResponse = (userNodeData, userGraphData, lotteryNodeData)=>{
    //   If no graph rounds response - return node data
    if (userGraphData.length === 0) {
        return lotteryNodeData.map((nodeRound)=>{
            const ticketDataForRound = userNodeData.find((roundTickets)=>roundTickets.roundId === nodeRound.lotteryId
            );
            return {
                endTime: nodeRound.endTime,
                status: nodeRound.status,
                lotteryId: nodeRound.lotteryId.toString(),
                claimed: hasRoundBeenClaimed(ticketDataForRound.userTickets),
                totalTickets: `${ticketDataForRound.userTickets.length.toString()}`,
                tickets: ticketDataForRound.userTickets
            };
        });
    }
    // Return the rounds with combined node + subgraph data, plus all remaining subgraph rounds.
    const nodeRoundsWithGraphData = userNodeData.map((userNodeRound)=>{
        const userGraphRound = userGraphData.find((graphResponseRound)=>graphResponseRound.lotteryId === userNodeRound.roundId
        );
        const nodeRoundData = lotteryNodeData.find((nodeRound)=>nodeRound.lotteryId === userNodeRound.roundId
        );
        return {
            endTime: nodeRoundData.endTime,
            status: nodeRoundData.status,
            lotteryId: nodeRoundData.lotteryId.toString(),
            claimed: hasRoundBeenClaimed(userNodeRound.userTickets),
            totalTickets: userGraphRound?.totalTickets || userNodeRound.userTickets.length.toString(),
            tickets: userNodeRound.userTickets
        };
    });
    // Return the rounds with combined data, plus all remaining subgraph rounds.
    const [lastCombinedDataRound] = nodeRoundsWithGraphData.slice(-1);
    const lastCombinedDataRoundIndex = userGraphData.map((graphRound)=>graphRound?.lotteryId
    ).indexOf(lastCombinedDataRound?.lotteryId);
    const remainingSubgraphRounds = userGraphData ? userGraphData.splice(lastCombinedDataRoundIndex + 1) : [];
    const mergedResponse = [
        ...nodeRoundsWithGraphData,
        ...remainingSubgraphRounds
    ];
    return mergedResponse;
};
const getGraphLotteryUser = async (account, first = MAX_USER_LOTTERIES_REQUEST_SIZE, skip = 0, where = {})=>{
    let user;
    const blankUser = {
        account,
        totalCake: '',
        totalTickets: '',
        rounds: []
    };
    try {
        const response = await (0,external_graphql_request_.request)(endpoints/* GRAPH_API_LOTTERY */.Xr, external_graphql_request_.gql`
        query getUserLotteries($account: ID!, $first: Int!, $skip: Int!, $where: Round_filter) {
          user(id: $account) {
            id
            totalTickets
            totalCake
            rounds(first: $first, skip: $skip, where: $where, orderDirection: desc, orderBy: block) {
              id
              lottery {
                id
                endTime
                status
              }
              claimed
              totalTickets
            }
          }
        }
      `, {
            account: account.toLowerCase(),
            first,
            skip,
            where
        });
        const userRes = response.user;
        // If no user returned - return blank user
        if (!userRes) {
            user = blankUser;
        } else {
            user = {
                account: userRes.id,
                totalCake: userRes.totalCake,
                totalTickets: userRes.totalTickets,
                rounds: userRes.rounds.map((round)=>{
                    return {
                        lotteryId: round?.lottery?.id,
                        endTime: round?.lottery?.endTime,
                        claimed: round?.claimed,
                        totalTickets: round?.totalTickets,
                        status: round?.lottery?.status.toLowerCase()
                    };
                })
            };
        }
    } catch (error) {
        console.error(error);
        user = blankUser;
    }
    return user;
};
const getUserLotteryData = async (account, currentLotteryId)=>{
    const idsForTicketsNodeCall = getRoundIdsArray(currentLotteryId);
    const roundDataAndUserTickets = await fetchUserTicketsForMultipleRounds(idsForTicketsNodeCall, account);
    const userRoundsNodeData = roundDataAndUserTickets.filter((round)=>round.userTickets.length > 0
    );
    const idsForLotteriesNodeCall = userRoundsNodeData.map((round)=>round.roundId
    );
    const lotteriesNodeData = await fetchMultipleLotteries(idsForLotteriesNodeCall);
    const graphResponse = await getGraphLotteryUser(account);
    const mergedRoundData = applyNodeDataToUserGraphResponse(userRoundsNodeData, graphResponse.rounds, lotteriesNodeData);
    const graphResponseWithNodeRounds = {
        ...graphResponse,
        rounds: mergedRoundData
    };
    return graphResponseWithNodeRounds;
};
/* harmony default export */ const lottery_getUserLotteryData = (getUserLotteryData);

;// CONCATENATED MODULE: ./src/state/lottery/index.ts
/* eslint-disable no-param-reassign */ 




const lottery_initialState = {
    currentLotteryId: null,
    isTransitioning: false,
    maxNumberTicketsPerBuyOrClaim: null,
    currentRound: {
        isLoading: true,
        lotteryId: null,
        status: types/* LotteryStatus.PENDING */.p3.PENDING,
        startTime: '',
        endTime: '',
        priceTicketInCake: '',
        discountDivisor: '',
        treasuryFee: '',
        firstTicketId: '',
        lastTicketId: '',
        amountCollectedInCake: '',
        finalNumber: null,
        cakePerBracket: [],
        countWinnersPerBracket: [],
        rewardsBreakdown: [],
        userTickets: {
            isLoading: true,
            tickets: []
        }
    },
    lotteriesData: null,
    userLotteryData: {
        account: '',
        totalCake: '',
        totalTickets: '',
        rounds: []
    }
};
const fetchCurrentLottery = (0,toolkit_.createAsyncThunk)('lottery/fetchCurrentLottery', async ({ currentLotteryId  })=>{
    const lotteryInfo = await fetchLottery(currentLotteryId);
    return lotteryInfo;
});
const fetchCurrentLotteryId = (0,toolkit_.createAsyncThunk)('lottery/fetchCurrentLotteryId', async ()=>{
    const currentIdAndMaxBuy = await fetchCurrentLotteryIdAndMaxBuy();
    return currentIdAndMaxBuy;
});
const fetchUserTicketsAndLotteries = (0,toolkit_.createAsyncThunk)('lottery/fetchUserTicketsAndLotteries', async ({ account , currentLotteryId  })=>{
    const userLotteriesRes = await lottery_getUserLotteryData(account, currentLotteryId);
    const userParticipationInCurrentRound = userLotteriesRes.rounds?.find((round)=>round.lotteryId === currentLotteryId
    );
    const userTickets = userParticipationInCurrentRound?.tickets;
    // User has not bought tickets for the current lottery, or there has been an error
    if (!userTickets || userTickets.length === 0) {
        return {
            userTickets: null,
            userLotteries: userLotteriesRes
        };
    }
    return {
        userTickets,
        userLotteries: userLotteriesRes
    };
});
const fetchPublicLotteries = (0,toolkit_.createAsyncThunk)('lottery/fetchPublicLotteries', async ({ currentLotteryId  })=>{
    const lotteries = await lottery_getLotteriesData(currentLotteryId);
    return lotteries;
});
const fetchUserLotteries = (0,toolkit_.createAsyncThunk)('lottery/fetchUserLotteries', async ({ account , currentLotteryId  })=>{
    const userLotteries = await lottery_getUserLotteryData(account, currentLotteryId);
    return userLotteries;
});
const fetchAdditionalUserLotteries = (0,toolkit_.createAsyncThunk)('lottery/fetchAdditionalUserLotteries', async ({ account , skip  })=>{
    const additionalUserLotteries = await getGraphLotteryUser(account, undefined, skip);
    return additionalUserLotteries;
});
const setLotteryIsTransitioning = (0,toolkit_.createAsyncThunk)(`lottery/setIsTransitioning`, async ({ isTransitioning  })=>{
    return {
        isTransitioning
    };
});
const LotterySlice = (0,toolkit_.createSlice)({
    name: 'Lottery',
    initialState: lottery_initialState,
    reducers: {},
    extraReducers: (builder)=>{
        builder.addCase(fetchCurrentLottery.fulfilled, (state, action)=>{
            state.currentRound = {
                ...state.currentRound,
                ...action.payload
            };
        });
        builder.addCase(fetchCurrentLotteryId.fulfilled, (state, action)=>{
            state.currentLotteryId = action.payload.currentLotteryId;
            state.maxNumberTicketsPerBuyOrClaim = action.payload.maxNumberTicketsPerBuyOrClaim;
        });
        builder.addCase(fetchUserTicketsAndLotteries.fulfilled, (state, action)=>{
            state.currentRound.userTickets.isLoading = false;
            state.currentRound.userTickets.tickets = action.payload.userTickets;
            state.userLotteryData = action.payload.userLotteries;
        });
        builder.addCase(fetchPublicLotteries.fulfilled, (state, action)=>{
            state.lotteriesData = action.payload;
        });
        builder.addCase(fetchUserLotteries.fulfilled, (state, action)=>{
            state.userLotteryData = action.payload;
        });
        builder.addCase(fetchAdditionalUserLotteries.fulfilled, (state, action)=>{
            const mergedRounds = [
                ...state.userLotteryData.rounds,
                ...action.payload.rounds
            ];
            state.userLotteryData.rounds = mergedRounds;
        });
        builder.addCase(setLotteryIsTransitioning.fulfilled, (state, action)=>{
            state.isTransitioning = action.payload.isTransitioning;
        });
    }
});
/* harmony default export */ const lottery = (LotterySlice.reducer);

;// CONCATENATED MODULE: ./src/state/mint/actions.ts

var actions_Field;
(function(Field) {
    Field["CURRENCY_A"] = "CURRENCY_A";
    Field["CURRENCY_B"] = "CURRENCY_B";
})(actions_Field || (actions_Field = {}));
const actions_typeInput = (0,toolkit_.createAction)('mint/typeInputMint');
const resetMintState = (0,toolkit_.createAction)('mint/resetMintState');

;// CONCATENATED MODULE: ./src/state/mint/reducer.ts


const mint_reducer_initialState = {
    independentField: actions_Field.CURRENCY_A,
    typedValue: '',
    otherTypedValue: ''
};
/* harmony default export */ const mint_reducer = ((0,toolkit_.createReducer)(mint_reducer_initialState, (builder)=>builder.addCase(resetMintState, ()=>mint_reducer_initialState
    ).addCase(actions_typeInput, (state, { payload: { field , typedValue , noLiquidity  }  })=>{
        if (noLiquidity) {
            // they're typing into the field they've last typed in
            if (field === state.independentField) {
                return {
                    ...state,
                    independentField: field,
                    typedValue
                };
            }
            // they're typing into a new field, store the other value
            return {
                ...state,
                independentField: field,
                typedValue,
                otherTypedValue: state.typedValue
            };
        }
        return {
            ...state,
            independentField: field,
            typedValue,
            otherTypedValue: ''
        };
    })
));

// EXTERNAL MODULE: ./src/state/multicall/actions.ts
var multicall_actions = __webpack_require__(3884);
;// CONCATENATED MODULE: ./src/state/multicall/reducer.ts


const multicall_reducer_initialState = {
    callResults: {}
};
/* harmony default export */ const multicall_reducer = ((0,toolkit_.createReducer)(multicall_reducer_initialState, (builder)=>builder.addCase(multicall_actions/* addMulticallListeners */.Dd, (state, { payload: { calls , chainId , options: { blocksPerFetch =1  } = {}  }  })=>{
        const listeners = state.callListeners ? state.callListeners : state.callListeners = {};
        listeners[chainId] = listeners[chainId] ?? {};
        calls.forEach((call)=>{
            const callKey = (0,multicall_actions/* toCallKey */.kG)(call);
            listeners[chainId][callKey] = listeners[chainId][callKey] ?? {};
            listeners[chainId][callKey][blocksPerFetch] = (listeners[chainId][callKey][blocksPerFetch] ?? 0) + 1;
        });
    }).addCase(multicall_actions/* removeMulticallListeners */.$x, (state, { payload: { chainId , calls , options: { blocksPerFetch =1  } = {}  }  })=>{
        const listeners = state.callListeners ? state.callListeners : state.callListeners = {};
        if (!listeners[chainId]) return;
        calls.forEach((call)=>{
            const callKey = (0,multicall_actions/* toCallKey */.kG)(call);
            if (!listeners[chainId][callKey]) return;
            if (!listeners[chainId][callKey][blocksPerFetch]) return;
            if (listeners[chainId][callKey][blocksPerFetch] === 1) {
                delete listeners[chainId][callKey][blocksPerFetch];
            } else {
                listeners[chainId][callKey][blocksPerFetch]--;
            }
        });
    }).addCase(multicall_actions/* fetchingMulticallResults */.nu, (state, { payload: { chainId , fetchingBlockNumber , calls  }  })=>{
        state.callResults[chainId] = state.callResults[chainId] ?? {};
        calls.forEach((call)=>{
            const callKey = (0,multicall_actions/* toCallKey */.kG)(call);
            const current = state.callResults[chainId][callKey];
            if (!current) {
                state.callResults[chainId][callKey] = {
                    fetchingBlockNumber
                };
            } else {
                if ((current.fetchingBlockNumber ?? 0) >= fetchingBlockNumber) return;
                state.callResults[chainId][callKey].fetchingBlockNumber = fetchingBlockNumber;
            }
        });
    }).addCase(multicall_actions/* errorFetchingMulticallResults */.wC, (state, { payload: { fetchingBlockNumber , chainId , calls  }  })=>{
        state.callResults[chainId] = state.callResults[chainId] ?? {};
        calls.forEach((call)=>{
            const callKey = (0,multicall_actions/* toCallKey */.kG)(call);
            const current = state.callResults[chainId][callKey];
            if (!current) return; // only should be dispatched if we are already fetching
            if (current.fetchingBlockNumber === fetchingBlockNumber) {
                delete current.fetchingBlockNumber;
                current.data = null;
                current.blockNumber = fetchingBlockNumber;
            }
        });
    }).addCase(multicall_actions/* updateMulticallResults */.zT, (state, { payload: { chainId , results , blockNumber  }  })=>{
        state.callResults[chainId] = state.callResults[chainId] ?? {};
        Object.keys(results).forEach((callKey)=>{
            const current = state.callResults[chainId][callKey];
            if ((current?.blockNumber ?? 0) > blockNumber) return;
            state.callResults[chainId][callKey] = {
                data: results[callKey],
                blockNumber
            };
        });
    })
));

// EXTERNAL MODULE: external "lodash/isEmpty"
var isEmpty_ = __webpack_require__(9699);
var isEmpty_default = /*#__PURE__*/__webpack_require__.n(isEmpty_);
// EXTERNAL MODULE: ./src/views/Nft/market/constants.ts + 2 modules
var constants = __webpack_require__(1940);
// EXTERNAL MODULE: ./src/state/nftMarket/helpers.ts + 1 modules
var helpers = __webpack_require__(6849);
// EXTERNAL MODULE: ./src/state/nftMarket/types.ts
var nftMarket_types = __webpack_require__(5186);
;// CONCATENATED MODULE: ./src/state/nftMarket/reducer.ts






const initialNftFilterState = {
    loadingState: types/* FetchStatus.Idle */.iF.Idle,
    activeFilters: {},
    showOnlyOnSale: true,
    ordering: {
        field: 'currentAskPrice',
        direction: 'asc'
    }
};
const initialNftActivityFilterState = {
    typeFilters: [],
    collectionFilters: []
};
const nftMarket_reducer_initialState = {
    initializationState: nftMarket_types/* NFTMarketInitializationState.UNINITIALIZED */.IB.UNINITIALIZED,
    data: {
        collections: {},
        nfts: {},
        filters: {},
        activityFilters: {},
        loadingState: {
            isUpdatingPancakeBunnies: false,
            latestPancakeBunniesUpdateAt: 0
        }
    }
};
/**
 * Fetch all collections data by combining data from the API (static metadata) and the Subgraph (dynamic market data)
 */ const fetchCollections = (0,toolkit_.createAsyncThunk)('nft/fetchCollections', async ()=>{
    const [collections, collectionsMarket] = await Promise.all([
        (0,helpers/* getCollectionsApi */.g_)(),
        (0,helpers/* getCollectionsSg */.JN)()
    ]);
    return (0,helpers/* combineCollectionData */.Ye)(collections?.data ?? [], collectionsMarket);
});
/**
 * Fetch collection data by combining data from the API (static metadata) and the Subgraph (dynamic market data)
 */ const fetchCollection = (0,toolkit_.createAsyncThunk)('nft/fetchCollection', async (collectionAddress)=>{
    const [collection, collectionMarket] = await Promise.all([
        (0,helpers/* getCollectionApi */.F7)(collectionAddress),
        (0,helpers/* getCollectionSg */.zS)(collectionAddress), 
    ]);
    return (0,helpers/* combineCollectionData */.Ye)([
        collection
    ], [
        collectionMarket
    ]);
});
/**
 * Fetch all NFT data for a collections by combining data from the API (static metadata)
 * and the Subgraph (dynamic market data)
 * @param collectionAddress
 */ const fetchNftsFromCollections = (0,toolkit_.createAsyncThunk)('nft/fetchNftsFromCollections', async ({ collectionAddress , page , size  })=>{
    try {
        if (collectionAddress === constants/* pancakeBunniesAddress */.Jr) {
            // PancakeBunnies don't need to pre-fetch "all nfts" from the collection
            // When user visits IndividualNFTPage required nfts will be fetched via bunny id
            return [];
        }
        const nfts = await (0,helpers/* getNftsFromCollectionApi */.Rq)(collectionAddress, size, page);
        if (!nfts?.data) {
            return [];
        }
        const tokenIds = Object.values(nfts.data).map((nft)=>nft.tokenId
        );
        const nftsMarket = await (0,helpers/* getMarketDataForTokenIds */.Z1)(collectionAddress, tokenIds);
        return tokenIds.map((id)=>{
            const apiMetadata = nfts.data[id];
            const marketData = nftsMarket.find((nft)=>nft.tokenId === id
            );
            return {
                tokenId: id,
                name: apiMetadata.name,
                description: apiMetadata.description,
                collectionName: apiMetadata.collection.name,
                collectionAddress,
                image: apiMetadata.image,
                attributes: apiMetadata.attributes,
                marketData
            };
        });
    } catch (error) {
        console.error(`Failed to fetch collection NFTs for ${collectionAddress}`, error);
        return [];
    }
});
const filterNftsFromCollection = (0,toolkit_.createAsyncThunk)('nft/filterNftsFromCollection', async ({ collectionAddress , nftFilters  })=>{
    try {
        const attrParams = Object.values(nftFilters).reduce((accum, attr)=>({
                ...accum,
                [attr.traitType]: attr.value
            })
        , {});
        if (isEmpty_default()(attrParams)) {
            return [];
        }
        const attrFilters = await (0,helpers/* fetchNftsFiltered */.nh)(collectionAddress, attrParams);
        // Fetch market data for each token returned
        const tokenIds = Object.values(attrFilters.data).map((apiToken)=>apiToken.tokenId
        );
        const marketData = await (0,helpers/* getNftsMarketData */.T5)({
            tokenId_in: tokenIds,
            collection: collectionAddress.toLowerCase()
        });
        const nftTokens = Object.values(attrFilters.data).map((apiToken)=>{
            const apiTokenMarketData = marketData.find((tokenMarketData)=>tokenMarketData.tokenId === apiToken.tokenId
            );
            return {
                tokenId: apiToken.tokenId,
                name: apiToken.name,
                description: apiToken.description,
                collectionName: apiToken.collection.name,
                collectionAddress,
                image: apiToken.image,
                attributes: apiToken.attributes,
                marketData: apiTokenMarketData
            };
        });
        return nftTokens;
    } catch  {
        return [];
    }
});
/**
 * This action keeps data on the individual PancakeBunny page up-to-date. Operation is a twofold
 * 1. Update existing NFTs in the state in case some were sold or got price modified
 * 2. Fetch 30 more NFTs with specified bunny id
 */ const fetchNewPBAndUpdateExisting = (0,toolkit_.createAsyncThunk)('nft/fetchNewPBAndUpdateExisting', async ({ bunnyId , existingTokensWithBunnyId , allExistingPBTokenIds , existingMetadata , orderDirection  })=>{
    try {
        // 1. Update existing NFTs in the state in case some were sold or got price modified
        const [updatedNfts, updatedNftsMarket] = await Promise.all([
            (0,helpers/* getNftsFromCollectionApi */.Rq)(constants/* pancakeBunniesAddress */.Jr),
            (0,helpers/* getMarketDataForTokenIds */.Z1)(constants/* pancakeBunniesAddress */.Jr, allExistingPBTokenIds), 
        ]);
        if (!updatedNfts?.data) {
            return [];
        }
        const updatedTokens = updatedNftsMarket.map((marketData)=>{
            const apiMetadata = (0,helpers/* getMetadataWithFallback */.M2)(updatedNfts.data, marketData.otherId);
            const attributes = (0,helpers/* getPancakeBunniesAttributesField */.rD)(marketData.otherId);
            return (0,helpers/* combineApiAndSgResponseToNftToken */.HD)(apiMetadata, marketData, attributes);
        });
        // 2. Fetch 30 more NFTs with specified bunny id
        let newNfts = {
            data: {
                [bunnyId]: existingMetadata
            }
        };
        if (!existingMetadata) {
            newNfts = await (0,helpers/* getNftsFromCollectionApi */.Rq)(constants/* pancakeBunniesAddress */.Jr);
        }
        const nftsMarket = await (0,helpers/* getNftsByBunnyIdSg */.p9)(bunnyId, existingTokensWithBunnyId, orderDirection);
        if (!newNfts?.data) {
            return updatedTokens;
        }
        const moreTokensWithRequestedBunnyId = nftsMarket.map((marketData)=>{
            const apiMetadata = (0,helpers/* getMetadataWithFallback */.M2)(newNfts.data, marketData.otherId);
            const attributes = (0,helpers/* getPancakeBunniesAttributesField */.rD)(marketData.otherId);
            return (0,helpers/* combineApiAndSgResponseToNftToken */.HD)(apiMetadata, marketData, attributes);
        });
        return [
            ...updatedTokens,
            ...moreTokensWithRequestedBunnyId
        ];
    } catch (error) {
        console.error(`Failed to update PancakeBunnies NFTs`, error);
        return [];
    }
});
const NftMarket = (0,toolkit_.createSlice)({
    name: 'NftMarket',
    initialState: nftMarket_reducer_initialState,
    reducers: {
        removeAllFilters: (state, action)=>{
            state.data.filters[action.payload] = {
                ...initialNftFilterState
            };
            state.data.nfts[action.payload] = [];
        },
        addActivityTypeFilters: (state, action)=>{
            if (state.data.activityFilters[action.payload.collection]) {
                state.data.activityFilters[action.payload.collection].typeFilters.push(action.payload.field);
            } else {
                state.data.activityFilters[action.payload.collection] = {
                    ...initialNftActivityFilterState,
                    typeFilters: [
                        action.payload.field
                    ]
                };
            }
        },
        removeActivityTypeFilters: (state, action)=>{
            if (state.data.activityFilters[action.payload.collection]) {
                state.data.activityFilters[action.payload.collection].typeFilters = state.data.activityFilters[action.payload.collection].typeFilters.filter((activeFilter)=>activeFilter !== action.payload.field
                );
            }
        },
        addActivityCollectionFilters: (state, action)=>{
            if (state.data.activityFilters['']) {
                state.data.activityFilters[''].collectionFilters.push(action.payload.collection);
            } else {
                state.data.activityFilters[''] = {
                    ...initialNftActivityFilterState,
                    collectionFilters: [
                        action.payload.collection
                    ]
                };
            }
        },
        removeActivityCollectionFilters: (state, action)=>{
            if (state.data.activityFilters['']) {
                state.data.activityFilters[''].collectionFilters = state.data.activityFilters[''].collectionFilters.filter((activeFilter)=>activeFilter !== action.payload.collection
                );
            }
        },
        removeAllActivityCollectionFilters: (state)=>{
            if (state.data.activityFilters['']) {
                state.data.activityFilters[''].collectionFilters = [];
            }
        },
        removeAllActivityFilters: (state, action)=>{
            state.data.activityFilters[action.payload] = {
                ...initialNftActivityFilterState
            };
        },
        setOrdering: (state, action)=>{
            if (state.data.filters[action.payload.collection]) {
                state.data.filters[action.payload.collection].ordering = {
                    field: action.payload.field,
                    direction: action.payload.direction
                };
            } else {
                state.data.filters[action.payload.collection] = {
                    ...initialNftFilterState,
                    ordering: {
                        field: action.payload.field,
                        direction: action.payload.direction
                    }
                };
            }
        },
        setShowOnlyOnSale: (state, action)=>{
            if (state.data.filters[action.payload.collection]) {
                state.data.filters[action.payload.collection].showOnlyOnSale = action.payload.showOnlyOnSale;
            } else {
                state.data.filters[action.payload.collection] = {
                    ...initialNftFilterState,
                    showOnlyOnSale: action.payload.showOnlyOnSale
                };
            }
        }
    },
    extraReducers: (builder)=>{
        builder.addCase(filterNftsFromCollection.pending, (state, action)=>{
            const { collectionAddress  } = action.meta.arg;
            if (state.data.filters[collectionAddress]) {
                state.data.filters[collectionAddress].loadingState = types/* FetchStatus.Fetching */.iF.Fetching;
            } else {
                state.data.filters[collectionAddress] = {
                    ...initialNftFilterState,
                    loadingState: types/* FetchStatus.Fetching */.iF.Fetching
                };
            }
        });
        builder.addCase(filterNftsFromCollection.fulfilled, (state, action)=>{
            const { collectionAddress , nftFilters  } = action.meta.arg;
            state.data.filters[collectionAddress] = {
                ...state.data.filters[collectionAddress],
                loadingState: types/* FetchStatus.Fetched */.iF.Fetched,
                activeFilters: nftFilters
            };
            state.data.nfts[collectionAddress] = action.payload;
        });
        builder.addCase(fetchCollection.fulfilled, (state, action)=>{
            state.data.collections = {
                ...state.data.collections,
                ...action.payload
            };
        });
        builder.addCase(fetchCollections.fulfilled, (state, action)=>{
            state.data.collections = action.payload;
            state.initializationState = nftMarket_types/* NFTMarketInitializationState.INITIALIZED */.IB.INITIALIZED;
        });
        builder.addCase(fetchNftsFromCollections.pending, (state, action)=>{
            const { collectionAddress  } = action.meta.arg;
            if (state.data.filters[collectionAddress]) {
                state.data.filters[collectionAddress].loadingState = types/* FetchStatus.Fetching */.iF.Fetching;
            } else {
                state.data.filters[collectionAddress] = {
                    ...initialNftFilterState,
                    loadingState: types/* FetchStatus.Fetching */.iF.Fetching
                };
            }
        });
        builder.addCase(fetchNftsFromCollections.fulfilled, (state, action)=>{
            const { collectionAddress  } = action.meta.arg;
            const existingNfts = state.data.nfts[collectionAddress] ?? [];
            const existingNftsWithoutNewOnes = existingNfts.filter((nftToken)=>!action.payload.find((newToken)=>newToken.tokenId === nftToken.tokenId
                )
            );
            state.data.filters[collectionAddress] = {
                ...state.data.filters[collectionAddress],
                loadingState: types/* FetchStatus.Fetched */.iF.Fetched,
                activeFilters: {}
            };
            state.data.nfts[collectionAddress] = [
                ...existingNftsWithoutNewOnes,
                ...action.payload
            ];
        });
        builder.addCase(fetchNewPBAndUpdateExisting.pending, (state)=>{
            state.data.loadingState.isUpdatingPancakeBunnies = true;
        });
        builder.addCase(fetchNewPBAndUpdateExisting.fulfilled, (state, action)=>{
            if (action.payload.length > 0) {
                state.data.nfts[constants/* pancakeBunniesAddress */.Jr] = action.payload;
            }
            state.data.loadingState.isUpdatingPancakeBunnies = false;
            state.data.loadingState.latestPancakeBunniesUpdateAt = Date.now();
        });
        builder.addCase(fetchNewPBAndUpdateExisting.rejected, (state)=>{
            state.data.loadingState.isUpdatingPancakeBunnies = false;
            state.data.loadingState.latestPancakeBunniesUpdateAt = Date.now();
        });
    }
});
// Actions
const { removeAllFilters , removeAllActivityFilters , removeActivityTypeFilters , removeActivityCollectionFilters , removeAllActivityCollectionFilters , addActivityTypeFilters , addActivityCollectionFilters , setOrdering , setShowOnlyOnSale ,  } = NftMarket.actions;
/* harmony default export */ const nftMarket_reducer = (NftMarket.reducer);

// EXTERNAL MODULE: ./src/config/constants/pools.ts
var pools = __webpack_require__(1080);
// EXTERNAL MODULE: ./src/config/abi/cake.json
var cake = __webpack_require__(3361);
// EXTERNAL MODULE: ./src/config/constants/tokens.ts
var constants_tokens = __webpack_require__(9748);
// EXTERNAL MODULE: ./src/config/abi/masterchef.json
var masterchef = __webpack_require__(4951);
// EXTERNAL MODULE: ./src/config/index.ts
var config = __webpack_require__(3206);
;// CONCATENATED MODULE: ./src/config/constants/lpAprs.json
const lpAprs_namespaceObject = {};
;// CONCATENATED MODULE: ./src/utils/apr.ts



/**
 * Get the APR value in %
 * @param stakingTokenPrice Token price in the same quote currency
 * @param rewardTokenPrice Token price in the same quote currency
 * @param totalStaked Total amount of stakingToken in the pool
 * @param tokenPerBlock Amount of new cake allocated to the pool for each new block
 * @returns Null if the APR is NaN or infinite.
 */ const apr_getPoolApr = (stakingTokenPrice, rewardTokenPrice, totalStaked, tokenPerBlock)=>{
    const totalRewardPricePerYear = new BigNumber(rewardTokenPrice).times(tokenPerBlock).times(BLOCKS_PER_YEAR);
    const totalStakingTokenInPool = new BigNumber(stakingTokenPrice).times(totalStaked);
    const apr = totalRewardPricePerYear.div(totalStakingTokenInPool).times(100);
    return apr.isNaN() || !apr.isFinite() ? null : apr.toNumber();
};
/**
 * Get farm APR value in %
 * @param poolWeight allocationPoint / totalAllocationPoint
 * @param cakePriceUsd Cake price in USD
 * @param poolLiquidityUsd Total pool liquidity in USD
 * @param farmAddress Farm Address
 * @returns Farm Apr
 */ const getFarmApr = (poolWeight, cakePriceUsd, poolLiquidityUsd, farmAddress)=>{
    const yearlyCakeRewardAllocation = poolWeight ? poolWeight.times(CAKE_PER_YEAR) : new BigNumber(NaN);
    const cakeRewardsApr = yearlyCakeRewardAllocation.times(cakePriceUsd).div(poolLiquidityUsd).times(100);
    let cakeRewardsAprAsNumber = null;
    if (!cakeRewardsApr.isNaN() && cakeRewardsApr.isFinite()) {
        cakeRewardsAprAsNumber = cakeRewardsApr.toNumber();
    }
    const lpRewardsApr = lpAprs[farmAddress?.toLocaleLowerCase()] ?? 0;
    return {
        cakeRewardsApr: cakeRewardsAprAsNumber,
        lpRewardsApr
    };
};
/* harmony default export */ const apr = (null);

// EXTERNAL MODULE: ./src/utils/formatBalance.ts
var formatBalance = __webpack_require__(5044);
// EXTERNAL MODULE: ./src/utils/providers.ts
var providers = __webpack_require__(5922);
;// CONCATENATED MODULE: ./src/views/Pools/helpers.tsx




const convertSharesToCake = (shares, cakePerFullShare, decimals = 18, decimalsToRound = 3)=>{
    const sharePriceNumber = (0,formatBalance/* getBalanceNumber */.mW)(cakePerFullShare, decimals);
    const amountInCake = new (external_bignumber_js_default())(shares.multipliedBy(sharePriceNumber));
    const cakeAsNumberBalance = (0,formatBalance/* getBalanceNumber */.mW)(amountInCake, decimals);
    const cakeAsBigNumber = (0,formatBalance/* getDecimalAmount */.Qe)(new (external_bignumber_js_default())(cakeAsNumberBalance), decimals);
    const cakeAsDisplayBalance = (0,formatBalance/* getFullDisplayBalance */.NJ)(amountInCake, decimals, decimalsToRound);
    return {
        cakeAsNumberBalance,
        cakeAsBigNumber,
        cakeAsDisplayBalance
    };
};
const convertCakeToShares = (cake, cakePerFullShare, decimals = 18, decimalsToRound = 3)=>{
    const sharePriceNumber = getBalanceNumber(cakePerFullShare, decimals);
    const amountInShares = new BigNumber(cake.dividedBy(sharePriceNumber));
    const sharesAsNumberBalance = getBalanceNumber(amountInShares, decimals);
    const sharesAsBigNumber = getDecimalAmount(new BigNumber(sharesAsNumberBalance), decimals);
    const sharesAsDisplayBalance = getFullDisplayBalance(amountInShares, decimals, decimalsToRound);
    return {
        sharesAsNumberBalance,
        sharesAsBigNumber,
        sharesAsDisplayBalance
    };
};
const MANUAL_POOL_AUTO_COMPOUND_FREQUENCY = 0;
const getAprData = (pool, performanceFee)=>{
    const { vaultKey , apr  } = pool;
    //   Estimate & manual for now. 288 = once every 5 mins. We can change once we have a better sense of this
    const autoCompoundFrequency = vaultKey ? vaultPoolConfig[vaultKey].autoCompoundFrequency : MANUAL_POOL_AUTO_COMPOUND_FREQUENCY;
    if (vaultKey) {
        const autoApr = getApy(apr, autoCompoundFrequency, 365, performanceFee) * 100;
        return {
            apr: autoApr,
            autoCompoundFrequency
        };
    }
    return {
        apr,
        autoCompoundFrequency
    };
};
const getCakeVaultEarnings = (account, cakeAtLastUserAction, userShares, pricePerFullShare, earningTokenPrice)=>{
    const hasAutoEarnings = account && cakeAtLastUserAction && cakeAtLastUserAction.gt(0) && userShares && userShares.gt(0);
    const { cakeAsBigNumber  } = convertSharesToCake(userShares, pricePerFullShare);
    const autoCakeProfit = cakeAsBigNumber.minus(cakeAtLastUserAction);
    const autoCakeToDisplay = autoCakeProfit.gte(0) ? getBalanceNumber(autoCakeProfit, 18) : 0;
    const autoUsdProfit = autoCakeProfit.times(earningTokenPrice);
    const autoUsdToDisplay = autoUsdProfit.gte(0) ? getBalanceNumber(autoUsdProfit, 18) : 0;
    return {
        hasAutoEarnings,
        autoCakeToDisplay,
        autoUsdToDisplay
    };
};
const getPoolBlockInfo = (pool, currentBlock)=>{
    const { startBlock , endBlock , isFinished  } = pool;
    const shouldShowBlockCountdown = Boolean(!isFinished && startBlock && endBlock);
    const blocksUntilStart = Math.max(startBlock - currentBlock, 0);
    const blocksRemaining = Math.max(endBlock - currentBlock, 0);
    const hasPoolStarted = blocksUntilStart === 0 && blocksRemaining > 0;
    const blocksToDisplay = hasPoolStarted ? blocksRemaining : blocksUntilStart;
    return {
        shouldShowBlockCountdown,
        blocksUntilStart,
        blocksRemaining,
        hasPoolStarted,
        blocksToDisplay
    };
};

// EXTERNAL MODULE: ./src/config/abi/ifoPool.json
var ifoPool = __webpack_require__(9704);
;// CONCATENATED MODULE: ./src/state/pools/fetchIfoPoolPublic.ts






const fetchPublicIfoPoolData = async ()=>{
    try {
        const calls = [
            'getPricePerFullShare',
            'totalShares',
            'calculateHarvestCakeRewards',
            'calculateTotalPendingCakeRewards',
            'startBlock',
            'endBlock', 
        ].map((method)=>({
                address: (0,addressHelpers/* getIfoPoolAddress */.rp)(),
                name: method
            })
        );
        const [[sharePrice], [shares], [estimatedCakeBountyReward], [totalPendingCakeHarvest], [startBlock], [endBlock]] = await (0,utils_multicall/* multicallv2 */.v)(ifoPool, calls);
        const totalSharesAsBigNumber = shares ? new (external_bignumber_js_default())(shares.toString()) : bigNumber/* BIG_ZERO */.HW;
        const sharePriceAsBigNumber = sharePrice ? new (external_bignumber_js_default())(sharePrice.toString()) : bigNumber/* BIG_ZERO */.HW;
        const totalCakeInVaultEstimate = convertSharesToCake(totalSharesAsBigNumber, sharePriceAsBigNumber);
        return {
            totalShares: totalSharesAsBigNumber.toJSON(),
            pricePerFullShare: sharePriceAsBigNumber.toJSON(),
            totalCakeInVault: totalCakeInVaultEstimate.cakeAsBigNumber.toJSON(),
            estimatedCakeBountyReward: new (external_bignumber_js_default())(estimatedCakeBountyReward.toString()).toJSON(),
            totalPendingCakeHarvest: new (external_bignumber_js_default())(totalPendingCakeHarvest.toString()).toJSON(),
            creditStartBlock: startBlock.toNumber(),
            creditEndBlock: endBlock.toNumber()
        };
    } catch (error) {
        return {
            totalShares: null,
            pricePerFullShare: null,
            totalCakeInVault: null,
            estimatedCakeBountyReward: null,
            totalPendingCakeHarvest: null
        };
    }
};
const fetchIfoPoolFeesData = async ()=>{
    try {
        const calls = [
            'performanceFee',
            'callFee',
            'withdrawFee',
            'withdrawFeePeriod'
        ].map((method)=>({
                address: (0,addressHelpers/* getIfoPoolAddress */.rp)(),
                name: method
            })
        );
        const [[performanceFee], [callFee], [withdrawalFee], [withdrawalFeePeriod]] = await (0,utils_multicall/* multicallv2 */.v)(ifoPool, calls);
        return {
            performanceFee: performanceFee.toNumber(),
            callFee: callFee.toNumber(),
            withdrawalFee: withdrawalFee.toNumber(),
            withdrawalFeePeriod: withdrawalFeePeriod.toNumber()
        };
    } catch (error) {
        return {
            performanceFee: null,
            callFee: null,
            withdrawalFee: null,
            withdrawalFeePeriod: null
        };
    }
};
/* harmony default export */ const fetchIfoPoolPublic = ((/* unused pure expression or super */ null && (fetchPublicIfoPoolData)));

;// CONCATENATED MODULE: ./src/state/pools/fetchIfoPoolUser.ts




const fetchIfoPoolUser = async (account)=>{
    try {
        const calls = [
            'userInfo',
            'getUserCredit'
        ].map((method)=>({
                address: (0,addressHelpers/* getIfoPoolAddress */.rp)(),
                name: method,
                params: [
                    account
                ]
            })
        );
        const [userContractResponse, creditResponse] = await (0,utils_multicall/* multicallv2 */.v)(ifoPool, calls);
        return {
            isLoading: false,
            userShares: new (external_bignumber_js_default())(userContractResponse.shares.toString()).toJSON(),
            lastDepositedTime: userContractResponse.lastDepositedTime.toString(),
            lastUserActionTime: userContractResponse.lastUserActionTime.toString(),
            cakeAtLastUserAction: new (external_bignumber_js_default())(userContractResponse.cakeAtLastUserAction.toString()).toJSON(),
            credit: new (external_bignumber_js_default())(creditResponse.avgBalance.toString()).toJSON()
        };
    } catch (error) {
        return {
            isLoading: true,
            userShares: null,
            lastDepositedTime: null,
            lastUserActionTime: null,
            cakeAtLastUserAction: null,
            credit: null
        };
    }
};
/* harmony default export */ const pools_fetchIfoPoolUser = (fetchIfoPoolUser);

// EXTERNAL MODULE: ./src/config/abi/sousChef.json
var sousChef = __webpack_require__(6477);
// EXTERNAL MODULE: ./src/config/abi/weth.json
var weth = __webpack_require__(9253);
// EXTERNAL MODULE: external "lodash/chunk"
var chunk_ = __webpack_require__(221);
var chunk_default = /*#__PURE__*/__webpack_require__.n(chunk_);
// EXTERNAL MODULE: ./src/config/abi/sousChefV2.json
var abi_sousChefV2 = __webpack_require__(9022);
;// CONCATENATED MODULE: ./src/state/pools/fetchPools.ts












const fetchPools_fetchPoolsBlockLimits = async ()=>{
    const poolsWithEnd = poolsConfig.filter((p)=>p.sousId !== 0
    );
    const startEndBlockCalls = poolsWithEnd.flatMap((poolConfig)=>{
        return [
            {
                address: getAddress(poolConfig.contractAddress),
                name: 'startBlock'
            },
            {
                address: getAddress(poolConfig.contractAddress),
                name: 'bonusEndBlock'
            }, 
        ];
    });
    const startEndBlockRaw = await multicall(sousChefABI, startEndBlockCalls);
    const startEndBlockResult = startEndBlockRaw.reduce((resultArray, item, index)=>{
        const chunkIndex = Math.floor(index / 2);
        if (!resultArray[chunkIndex]) {
            // eslint-disable-next-line no-param-reassign
            resultArray[chunkIndex] = [] // start a new chunk
            ;
        }
        resultArray[chunkIndex].push(item);
        return resultArray;
    }, []);
    return poolsWithEnd.map((cakePoolConfig, index)=>{
        const [startBlock, endBlock] = startEndBlockResult[index];
        return {
            sousId: cakePoolConfig.sousId,
            startBlock: new BigNumber(startBlock).toJSON(),
            endBlock: new BigNumber(endBlock).toJSON()
        };
    });
};
const fetchPools_fetchPoolsTotalStaking = async ()=>{
    const nonBnbPools = poolsConfig.filter((p)=>p.stakingToken.symbol !== 'BNB'
    );
    const bnbPool = poolsConfig.filter((p)=>p.stakingToken.symbol === 'BNB'
    );
    const callsNonBnbPools = nonBnbPools.map((poolConfig)=>{
        return {
            address: poolConfig.stakingToken.address,
            name: 'balanceOf',
            params: [
                getAddress(poolConfig.contractAddress)
            ]
        };
    });
    const callsBnbPools = bnbPool.map((poolConfig)=>{
        return {
            address: tokens.wbnb.address,
            name: 'balanceOf',
            params: [
                getAddress(poolConfig.contractAddress)
            ]
        };
    });
    const nonBnbPoolsTotalStaked = await multicall(cakeABI, callsNonBnbPools);
    const bnbPoolsTotalStaked = await multicall(wbnbABI, callsBnbPools);
    return [
        ...nonBnbPools.map((p, index)=>({
                sousId: p.sousId,
                totalStaked: new BigNumber(nonBnbPoolsTotalStaked[index]).toJSON()
            })
        ),
        ...bnbPool.map((p, index)=>({
                sousId: p.sousId,
                totalStaked: new BigNumber(bnbPoolsTotalStaked[index]).toJSON()
            })
        ), 
    ];
};
const fetchPoolStakingLimit = async (sousId)=>{
    try {
        const sousContract = getSouschefV2Contract(sousId);
        const hasUserLimit = await sousContract.hasUserLimit();
        if (hasUserLimit) {
            const stakingLimit = await sousContract.poolLimitPerUser();
            return new BigNumber(stakingLimit.toString());
        }
        return BIG_ZERO;
    } catch (error) {
        return BIG_ZERO;
    }
};
const fetchPools_fetchPoolsStakingLimits = async (poolsWithStakingLimit)=>{
    const validPools = poolsConfig.filter((p)=>p.stakingToken.symbol !== 'BNB' && !p.isFinished
    ).filter((p)=>!poolsWithStakingLimit.includes(p.sousId)
    );
    // Get the staking limit for each valid pool
    const poolStakingCalls = validPools.map((validPool)=>{
        const contractAddress = getAddress(validPool.contractAddress);
        return [
            'hasUserLimit',
            'poolLimitPerUser'
        ].map((method)=>({
                address: contractAddress,
                name: method
            })
        );
    }).flat();
    const poolStakingResultRaw = await multicallv2(sousChefV2, poolStakingCalls, {
        requireSuccess: false
    });
    const chunkSize = poolStakingCalls.length / validPools.length;
    const poolStakingChunkedResultRaw = chunk(poolStakingResultRaw.flat(), chunkSize);
    return poolStakingChunkedResultRaw.reduce((accum, stakingLimitRaw, index)=>{
        const hasUserLimit = stakingLimitRaw[0];
        const stakingLimit = hasUserLimit && stakingLimitRaw[1] ? new BigNumber(stakingLimitRaw[1].toString()) : BIG_ZERO;
        return {
            ...accum,
            [validPools[index].sousId]: stakingLimit
        };
    }, {});
};

// EXTERNAL MODULE: ./src/config/abi/erc20.json
var erc20 = __webpack_require__(3324);
;// CONCATENATED MODULE: ./src/state/pools/fetchPoolsUser.ts








// Pool 0, Cake / Cake is a different kind of contract (master chef)
// BNB pools use the native BNB token (wrapping ? unwrapping is done at the contract level)
const nonBnbPools = pools/* default.filter */.Z.filter((pool)=>pool.stakingToken.symbol !== 'BNB'
);
const bnbPools = pools/* default.filter */.Z.filter((pool)=>pool.stakingToken.symbol === 'BNB'
);
const nonMasterPools = pools/* default.filter */.Z.filter((pool)=>pool.sousId !== 0
);
const masterChefContract = (0,contractHelpers/* getMasterchefContract */.aE)();
const fetchPoolsUser_fetchPoolsAllowance = async (account)=>{
    const calls = nonBnbPools.map((pool)=>({
            address: pool.stakingToken.address,
            name: 'allowance',
            params: [
                account,
                getAddress(pool.contractAddress)
            ]
        })
    );
    const allowances = await multicall(erc20ABI, calls);
    return nonBnbPools.reduce((acc, pool, index)=>({
            ...acc,
            [pool.sousId]: new BigNumber(allowances[index]).toJSON()
        })
    , {});
};
const fetchPoolsUser_fetchUserBalances = async (account)=>{
    // Non BNB pools
    const calls = nonBnbPools.map((pool)=>({
            address: pool.stakingToken.address,
            name: 'balanceOf',
            params: [
                account
            ]
        })
    );
    const tokenBalancesRaw = await multicall(erc20ABI, calls);
    const tokenBalances = nonBnbPools.reduce((acc, pool, index)=>({
            ...acc,
            [pool.sousId]: new BigNumber(tokenBalancesRaw[index]).toJSON()
        })
    , {});
    // BNB pools
    const bnbBalance = await simpleRpcProvider.getBalance(account);
    const bnbBalances = bnbPools.reduce((acc, pool)=>({
            ...acc,
            [pool.sousId]: new BigNumber(bnbBalance.toString()).toJSON()
        })
    , {});
    return {
        ...tokenBalances,
        ...bnbBalances
    };
};
const fetchPoolsUser_fetchUserStakeBalances = async (account)=>{
    const calls = nonMasterPools.map((p)=>({
            address: getAddress(p.contractAddress),
            name: 'userInfo',
            params: [
                account
            ]
        })
    );
    const userInfo = await multicall(sousChefABI, calls);
    const stakedBalances = nonMasterPools.reduce((acc, pool, index)=>({
            ...acc,
            [pool.sousId]: new BigNumber(userInfo[index].amount._hex).toJSON()
        })
    , {});
    // Cake / Cake pool
    const { amount: masterPoolAmount  } = await masterChefContract.userInfo('0', account);
    return {
        ...stakedBalances,
        0: new BigNumber(masterPoolAmount.toString()).toJSON()
    };
};
const fetchPoolsUser_fetchUserPendingRewards = async (account)=>{
    const calls = nonMasterPools.map((p)=>({
            address: getAddress(p.contractAddress),
            name: 'pendingReward',
            params: [
                account
            ]
        })
    );
    const res = await multicall(sousChefABI, calls);
    const pendingRewards = nonMasterPools.reduce((acc, pool, index)=>({
            ...acc,
            [pool.sousId]: new BigNumber(res[index]).toJSON()
        })
    , {});
    // Cake / Cake pool
    const pendingReward = await masterChefContract.pendingCake('0', account);
    return {
        ...pendingRewards,
        0: new BigNumber(pendingReward.toString()).toJSON()
    };
};

// EXTERNAL MODULE: ./src/config/abi/cakeVault.json
var cakeVault = __webpack_require__(1430);
;// CONCATENATED MODULE: ./src/state/pools/fetchVaultPublic.ts






const fetchPublicVaultData = async ()=>{
    try {
        const calls = [
            'getPricePerFullShare',
            'totalShares',
            'calculateHarvestCakeRewards',
            'calculateTotalPendingCakeRewards', 
        ].map((method)=>({
                address: (0,addressHelpers/* getCakeVaultAddress */.O9)(),
                name: method
            })
        );
        const [[sharePrice], [shares], [estimatedCakeBountyReward], [totalPendingCakeHarvest]] = await (0,utils_multicall/* multicallv2 */.v)(cakeVault, calls);
        const totalSharesAsBigNumber = shares ? new (external_bignumber_js_default())(shares.toString()) : bigNumber/* BIG_ZERO */.HW;
        const sharePriceAsBigNumber = sharePrice ? new (external_bignumber_js_default())(sharePrice.toString()) : bigNumber/* BIG_ZERO */.HW;
        const totalCakeInVaultEstimate = convertSharesToCake(totalSharesAsBigNumber, sharePriceAsBigNumber);
        return {
            totalShares: totalSharesAsBigNumber.toJSON(),
            pricePerFullShare: sharePriceAsBigNumber.toJSON(),
            totalCakeInVault: totalCakeInVaultEstimate.cakeAsBigNumber.toJSON(),
            estimatedCakeBountyReward: new (external_bignumber_js_default())(estimatedCakeBountyReward.toString()).toJSON(),
            totalPendingCakeHarvest: new (external_bignumber_js_default())(totalPendingCakeHarvest.toString()).toJSON()
        };
    } catch (error) {
        return {
            totalShares: null,
            pricePerFullShare: null,
            totalCakeInVault: null,
            estimatedCakeBountyReward: null,
            totalPendingCakeHarvest: null
        };
    }
};
const fetchVaultFees = async ()=>{
    try {
        const calls = [
            'performanceFee',
            'callFee',
            'withdrawFee',
            'withdrawFeePeriod'
        ].map((method)=>({
                address: (0,addressHelpers/* getCakeVaultAddress */.O9)(),
                name: method
            })
        );
        const [[performanceFee], [callFee], [withdrawalFee], [withdrawalFeePeriod]] = await (0,utils_multicall/* multicallv2 */.v)(cakeVault, calls);
        return {
            performanceFee: performanceFee.toNumber(),
            callFee: callFee.toNumber(),
            withdrawalFee: withdrawalFee.toNumber(),
            withdrawalFeePeriod: withdrawalFeePeriod.toNumber()
        };
    } catch (error) {
        return {
            performanceFee: null,
            callFee: null,
            withdrawalFee: null,
            withdrawalFeePeriod: null
        };
    }
};
/* harmony default export */ const fetchVaultPublic = ((/* unused pure expression or super */ null && (fetchPublicVaultData)));

;// CONCATENATED MODULE: ./src/state/pools/fetchVaultUser.ts


const cakeVaultContract = (0,contractHelpers/* getCakeVaultContract */.bR)();
const fetchVaultUser = async (account)=>{
    try {
        const userContractResponse = await cakeVaultContract.userInfo(account);
        return {
            isLoading: false,
            userShares: new (external_bignumber_js_default())(userContractResponse.shares.toString()).toJSON(),
            lastDepositedTime: userContractResponse.lastDepositedTime.toString(),
            lastUserActionTime: userContractResponse.lastUserActionTime.toString(),
            cakeAtLastUserAction: new (external_bignumber_js_default())(userContractResponse.cakeAtLastUserAction.toString()).toJSON()
        };
    } catch (error) {
        return {
            isLoading: true,
            userShares: null,
            lastDepositedTime: null,
            lastUserActionTime: null,
            cakeAtLastUserAction: null
        };
    }
};
/* harmony default export */ const pools_fetchVaultUser = (fetchVaultUser);

// EXTERNAL MODULE: ./src/state/user/hooks/helpers.ts
var hooks_helpers = __webpack_require__(787);
;// CONCATENATED MODULE: ./src/state/pools/helpers.ts



const transformUserData = (userData)=>{
    return {
        allowance: userData ? new BigNumber(userData.allowance) : BIG_ZERO,
        stakingTokenBalance: userData ? new BigNumber(userData.stakingTokenBalance) : BIG_ZERO,
        stakedBalance: userData ? new BigNumber(userData.stakedBalance) : BIG_ZERO,
        pendingReward: userData ? new BigNumber(userData.pendingReward) : BIG_ZERO
    };
};
const transformPool = (pool)=>{
    const { totalStaked , stakingLimit , userData , stakingToken , earningToken , ...rest } = pool;
    return {
        ...rest,
        stakingToken: deserializeToken(stakingToken),
        earningToken: deserializeToken(earningToken),
        userData: transformUserData(userData),
        totalStaked: new BigNumber(totalStaked),
        stakingLimit: new BigNumber(stakingLimit)
    };
};
const helpers_getTokenPricesFromFarm = (farms)=>{
    return farms.reduce((prices, farm)=>{
        const quoteTokenAddress = farm.quoteToken.address.toLocaleLowerCase();
        const tokenAddress = farm.token.address.toLocaleLowerCase();
        /* eslint-disable no-param-reassign */ if (!prices[quoteTokenAddress]) {
            prices[quoteTokenAddress] = new BigNumber(farm.quoteTokenPriceBusd).toNumber();
        }
        if (!prices[tokenAddress]) {
            prices[tokenAddress] = new BigNumber(farm.tokenPriceBusd).toNumber();
        }
        /* eslint-enable no-param-reassign */ return prices;
    }, {});
};

;// CONCATENATED MODULE: ./src/state/pools/index.ts




















const initialPoolVaultState = Object.freeze({
    totalShares: null,
    pricePerFullShare: null,
    totalCakeInVault: null,
    estimatedCakeBountyReward: null,
    totalPendingCakeHarvest: null,
    fees: {
        performanceFee: null,
        callFee: null,
        withdrawalFee: null,
        withdrawalFeePeriod: null
    },
    userData: {
        isLoading: true,
        userShares: null,
        cakeAtLastUserAction: null,
        lastDepositedTime: null,
        lastUserActionTime: null,
        credit: null
    },
    creditStartBlock: null
});
const pools_initialState = {
    data: [
        ...pools/* default */.Z
    ],
    userDataLoaded: false,
    cakeVault: initialPoolVaultState,
    ifoPool: initialPoolVaultState
};
// Thunks
const cakePool = pools/* default.find */.Z.find((pool)=>pool.sousId === 0
);
const cakePoolAddress = (0,addressHelpers/* getAddress */.Kn)(cakePool.contractAddress);
const cakeContract = (0,contractHelpers/* getCakeContract */.XT)();
const fetchCakePoolPublicDataAsync = ()=>async (dispatch, getState)=>{
        const prices = getTokenPricesFromFarm(getState().farms.data);
        const stakingTokenAddress = cakePool.stakingToken.address ? cakePool.stakingToken.address.toLowerCase() : null;
        const stakingTokenPrice = stakingTokenAddress ? prices[stakingTokenAddress] : 0;
        const earningTokenAddress = cakePool.earningToken.address ? cakePool.earningToken.address.toLowerCase() : null;
        const earningTokenPrice = earningTokenAddress ? prices[earningTokenAddress] : 0;
        const totalStaking = await cakeContract.balanceOf(cakePoolAddress);
        const apr = getPoolApr(stakingTokenPrice, earningTokenPrice, getBalanceNumber(new BigNumber(totalStaking ? totalStaking.toString() : 0), cakePool.stakingToken.decimals), parseFloat(cakePool.tokenPerBlock));
        dispatch(setPoolPublicData({
            sousId: 0,
            data: {
                totalStaked: new BigNumber(totalStaking.toString()).toJSON(),
                stakingTokenPrice,
                earningTokenPrice,
                apr
            }
        }));
    }
;
const fetchCakePoolUserDataAsync = (account)=>async (dispatch)=>{
        const allowanceCall = {
            address: tokens.cake.address,
            name: 'allowance',
            params: [
                account,
                cakePoolAddress
            ]
        };
        const balanceOfCall = {
            address: tokens.cake.address,
            name: 'balanceOf',
            params: [
                account
            ]
        };
        const cakeContractCalls = [
            allowanceCall,
            balanceOfCall
        ];
        const [[allowance], [stakingTokenBalance]] = await multicallv2(cakeAbi, cakeContractCalls);
        const masterChefCalls = [
            'pendingCake',
            'userInfo'
        ].map((method)=>({
                address: getMasterChefAddress(),
                name: method,
                params: [
                    '0',
                    account
                ]
            })
        );
        const [[pendingReward], { amount: masterPoolAmount  }] = await multicallv2(masterChef, masterChefCalls);
        dispatch(setPoolUserData({
            sousId: 0,
            data: {
                allowance: new BigNumber(allowance.toString()).toJSON(),
                stakingTokenBalance: new BigNumber(stakingTokenBalance.toString()).toJSON(),
                pendingReward: new BigNumber(pendingReward.toString()).toJSON(),
                stakedBalances: new BigNumber(masterPoolAmount.toString()).toJSON()
            }
        }));
    }
;
const fetchPoolsPublicDataAsync = ()=>async (dispatch, getState)=>{
        try {
            const blockLimits = await fetchPoolsBlockLimits();
            const totalStakings = await fetchPoolsTotalStaking();
            let currentBlock = getState().block?.currentBlock;
            if (!currentBlock) {
                currentBlock = await simpleRpcProvider.getBlockNumber();
            }
            const prices = getTokenPricesFromFarm(getState().farms.data);
            const liveData = poolsConfig.map((pool)=>{
                const blockLimit = blockLimits.find((entry)=>entry.sousId === pool.sousId
                );
                const totalStaking = totalStakings.find((entry)=>entry.sousId === pool.sousId
                );
                const isPoolEndBlockExceeded = currentBlock > 0 && blockLimit ? currentBlock > Number(blockLimit.endBlock) : false;
                const isPoolFinished = pool.isFinished || isPoolEndBlockExceeded;
                const stakingTokenAddress = pool.stakingToken.address ? pool.stakingToken.address.toLowerCase() : null;
                const stakingTokenPrice = stakingTokenAddress ? prices[stakingTokenAddress] : 0;
                const earningTokenAddress = pool.earningToken.address ? pool.earningToken.address.toLowerCase() : null;
                const earningTokenPrice = earningTokenAddress ? prices[earningTokenAddress] : 0;
                const apr = !isPoolFinished ? getPoolApr(stakingTokenPrice, earningTokenPrice, getBalanceNumber(new BigNumber(totalStaking.totalStaked), pool.stakingToken.decimals), parseFloat(pool.tokenPerBlock)) : 0;
                return {
                    ...blockLimit,
                    ...totalStaking,
                    stakingTokenPrice,
                    earningTokenPrice,
                    apr,
                    isFinished: isPoolFinished
                };
            });
            dispatch(setPoolsPublicData(liveData));
        } catch (error) {
            console.error('[Pools Action] error when getting public data', error);
        }
    }
;
const fetchPoolsStakingLimitsAsync = ()=>async (dispatch, getState)=>{
        const poolsWithStakingLimit = getState().pools.data.filter(({ stakingLimit  })=>stakingLimit !== null && stakingLimit !== undefined
        ).map((pool)=>pool.sousId
        );
        try {
            const stakingLimits = await fetchPoolsStakingLimits(poolsWithStakingLimit);
            const stakingLimitData = poolsConfig.map((pool)=>{
                if (poolsWithStakingLimit.includes(pool.sousId)) {
                    return {
                        sousId: pool.sousId
                    };
                }
                const stakingLimit = stakingLimits[pool.sousId] || BIG_ZERO;
                return {
                    sousId: pool.sousId,
                    stakingLimit: stakingLimit.toJSON()
                };
            });
            dispatch(setPoolsPublicData(stakingLimitData));
        } catch (error) {
            console.error('[Pools Action] error when getting staking limits', error);
        }
    }
;
const fetchPoolsUserDataAsync = (account)=>async (dispatch)=>{
        try {
            const allowances = await fetchPoolsAllowance(account);
            const stakingTokenBalances = await fetchUserBalances(account);
            const stakedBalances = await fetchUserStakeBalances(account);
            const pendingRewards = await fetchUserPendingRewards(account);
            const userData = poolsConfig.map((pool)=>({
                    sousId: pool.sousId,
                    allowance: allowances[pool.sousId],
                    stakingTokenBalance: stakingTokenBalances[pool.sousId],
                    stakedBalance: stakedBalances[pool.sousId],
                    pendingReward: pendingRewards[pool.sousId]
                })
            );
            dispatch(setPoolsUserData(userData));
        } catch (error) {
            console.error('[Pools Action] Error fetching pool user data', error);
        }
    }
;
const updateUserAllowance = (sousId, account)=>async (dispatch)=>{
        const allowances = await fetchPoolsAllowance(account);
        dispatch(updatePoolsUserData({
            sousId,
            field: 'allowance',
            value: allowances[sousId]
        }));
    }
;
const updateUserBalance = (sousId, account)=>async (dispatch)=>{
        const tokenBalances = await fetchUserBalances(account);
        dispatch(updatePoolsUserData({
            sousId,
            field: 'stakingTokenBalance',
            value: tokenBalances[sousId]
        }));
    }
;
const updateUserStakedBalance = (sousId, account)=>async (dispatch)=>{
        const stakedBalances = await fetchUserStakeBalances(account);
        dispatch(updatePoolsUserData({
            sousId,
            field: 'stakedBalance',
            value: stakedBalances[sousId]
        }));
    }
;
const updateUserPendingReward = (sousId, account)=>async (dispatch)=>{
        const pendingRewards = await fetchUserPendingRewards(account);
        dispatch(updatePoolsUserData({
            sousId,
            field: 'pendingReward',
            value: pendingRewards[sousId]
        }));
    }
;
const fetchCakeVaultPublicData = (0,toolkit_.createAsyncThunk)('cakeVault/fetchPublicData', async ()=>{
    const publicVaultInfo = await fetchPublicVaultData();
    return publicVaultInfo;
});
const fetchCakeVaultFees = (0,toolkit_.createAsyncThunk)('cakeVault/fetchFees', async ()=>{
    const vaultFees = await fetchVaultFees();
    return vaultFees;
});
const fetchCakeVaultUserData = (0,toolkit_.createAsyncThunk)('cakeVault/fetchUser', async ({ account  })=>{
    const userData = await pools_fetchVaultUser(account);
    return userData;
});
const fetchIfoPoolPublicData = (0,toolkit_.createAsyncThunk)('ifoPool/fetchPublicData', async ()=>{
    const publicVaultInfo = await fetchPublicIfoPoolData();
    return publicVaultInfo;
});
const fetchIfoPoolFees = (0,toolkit_.createAsyncThunk)('ifoPool/fetchFees', async ()=>{
    const vaultFees = await fetchIfoPoolFeesData();
    return vaultFees;
});
const fetchIfoPoolUserAndCredit = (0,toolkit_.createAsyncThunk)('ifoPool/fetchUser', async ({ account  })=>{
    const userData = await pools_fetchIfoPoolUser(account);
    return userData;
});
const PoolsSlice = (0,toolkit_.createSlice)({
    name: 'Pools',
    initialState: pools_initialState,
    reducers: {
        setPoolPublicData: (state, action)=>{
            const { sousId  } = action.payload;
            const poolIndex = state.data.findIndex((pool)=>pool.sousId === sousId
            );
            state.data[poolIndex] = {
                ...state.data[poolIndex],
                ...action.payload.data
            };
        },
        setPoolUserData: (state, action)=>{
            const { sousId  } = action.payload;
            const poolIndex = state.data.findIndex((pool)=>pool.sousId === sousId
            );
            state.data[poolIndex].userData = action.payload.data;
        },
        setPoolsPublicData: (state, action)=>{
            const livePoolsData = action.payload;
            state.data = state.data.map((pool)=>{
                const livePoolData = livePoolsData.find((entry)=>entry.sousId === pool.sousId
                );
                return {
                    ...pool,
                    ...livePoolData
                };
            });
        },
        setPoolsUserData: (state, action)=>{
            const userData = action.payload;
            state.data = state.data.map((pool)=>{
                const userPoolData = userData.find((entry)=>entry.sousId === pool.sousId
                );
                return {
                    ...pool,
                    userData: userPoolData
                };
            });
            state.userDataLoaded = true;
        },
        updatePoolsUserData: (state, action)=>{
            const { field , value , sousId  } = action.payload;
            const index = state.data.findIndex((p)=>p.sousId === sousId
            );
            if (index >= 0) {
                state.data[index] = {
                    ...state.data[index],
                    userData: {
                        ...state.data[index].userData,
                        [field]: value
                    }
                };
            }
        }
    },
    extraReducers: (builder)=>{
        // Vault public data that updates frequently
        builder.addCase(fetchCakeVaultPublicData.fulfilled, (state, action)=>{
            state.cakeVault = {
                ...state.cakeVault,
                ...action.payload
            };
        });
        // Vault fees
        builder.addCase(fetchCakeVaultFees.fulfilled, (state, action)=>{
            const fees = action.payload;
            state.cakeVault = {
                ...state.cakeVault,
                fees
            };
        });
        // Vault user data
        builder.addCase(fetchCakeVaultUserData.fulfilled, (state, action)=>{
            const userData = action.payload;
            userData.isLoading = false;
            state.cakeVault = {
                ...state.cakeVault,
                userData
            };
        });
        // Vault public data that updates frequently
        builder.addCase(fetchIfoPoolPublicData.fulfilled, (state, action)=>{
            state.ifoPool = {
                ...state.ifoPool,
                ...action.payload
            };
        });
        // Vault fees
        builder.addCase(fetchIfoPoolFees.fulfilled, (state, action)=>{
            const fees = action.payload;
            state.ifoPool = {
                ...state.ifoPool,
                fees
            };
        });
        // Vault user data
        builder.addCase(fetchIfoPoolUserAndCredit.fulfilled, (state, action)=>{
            const userData = action.payload;
            userData.isLoading = false;
            state.ifoPool = {
                ...state.ifoPool,
                userData
            };
        });
    }
});
// Actions
const { setPoolsPublicData , setPoolsUserData , updatePoolsUserData , setPoolPublicData , setPoolUserData  } = PoolsSlice.actions;
/* harmony default export */ const state_pools = (PoolsSlice.reducer);

// EXTERNAL MODULE: external "@ethersproject/bignumber"
var bignumber_ = __webpack_require__(5757);
// EXTERNAL MODULE: external "@ethersproject/units"
var units_ = __webpack_require__(3138);
// EXTERNAL MODULE: external "lodash/maxBy"
var maxBy_ = __webpack_require__(1341);
var maxBy_default = /*#__PURE__*/__webpack_require__.n(maxBy_);
// EXTERNAL MODULE: external "lodash/merge"
var merge_ = __webpack_require__(1831);
var merge_default = /*#__PURE__*/__webpack_require__.n(merge_);
// EXTERNAL MODULE: external "lodash/range"
var range_ = __webpack_require__(4042);
var range_default = /*#__PURE__*/__webpack_require__.n(range_);
// EXTERNAL MODULE: ./src/state/types.ts
var state_types = __webpack_require__(5101);
;// CONCATENATED MODULE: ./src/state/predictions/config.ts
const REWARD_RATE = 0.97;
// Estimated number of seconds it takes to submit a transaction (3 blocks) in seconds
const ROUND_BUFFER = 9;
const PAST_ROUND_COUNT = 5;
const FUTURE_ROUND_COUNT = 2;
const ROUNDS_PER_PAGE = 200;
const LEADERBOARD_MIN_ROUNDS_PLAYED = 10;

// EXTERNAL MODULE: ./src/config/abi/predictions.json
var predictions = __webpack_require__(5883);
;// CONCATENATED MODULE: ./src/state/predictions/queries.ts
/**
 * Base fields are the all the top-level fields available in the api. Used in multiple queries
 */ const queries_getRoundBaseFields = ()=>`
  id
  epoch
  position
  failed
  startAt
  startBlock
  startHash
  lockAt
  lockBlock
  lockHash
  lockPrice
  lockRoundId
  closeAt
  closeBlock
  closeHash
  closePrice
  closeRoundId
  totalBets
  totalAmount
  bullBets
  bullAmount
  bearBets
  bearAmount
`
;
const queries_getBetBaseFields = ()=>`
 id
 hash  
 amount
 position
 claimed
 claimedAt
 claimedHash
 claimedBlock
 claimedBNB
 claimedNetBNB
 createdAt
 updatedAt
`
;
const queries_getUserBaseFields = ()=>`
  id
  createdAt
  updatedAt
  block
  totalBets
  totalBetsBull
  totalBetsBear
  totalBNB
  totalBNBBull
  totalBNBBear
  totalBetsClaimed
  totalBNBClaimed
  winRate
  averageBNB
  netBNB
`
;

;// CONCATENATED MODULE: ./src/state/predictions/helpers.ts










var Result;
(function(Result) {
    Result["WIN"] = 'win';
    Result["LOSE"] = 'lose';
    Result["CANCELED"] = 'canceled';
    Result["HOUSE"] = 'house';
    Result["LIVE"] = 'live';
})(Result || (Result = {}));
const numberOrNull = (value)=>{
    if (value === null) {
        return null;
    }
    const valueNum = Number(value);
    return Number.isNaN(valueNum) ? null : valueNum;
};
const getRoundPosition = (positionResponse)=>{
    if (positionResponse === 'Bull') {
        return state_types/* BetPosition.BULL */.Tu.BULL;
    }
    if (positionResponse === 'Bear') {
        return state_types/* BetPosition.BEAR */.Tu.BEAR;
    }
    if (positionResponse === 'House') {
        return state_types/* BetPosition.HOUSE */.Tu.HOUSE;
    }
    return null;
};
const transformBetResponse = (betResponse)=>{
    const bet = {
        id: betResponse.id,
        hash: betResponse.hash,
        block: numberOrNull(betResponse.block),
        amount: betResponse.amount ? parseFloat(betResponse.amount) : 0,
        position: betResponse.position === 'Bull' ? state_types/* BetPosition.BULL */.Tu.BULL : state_types/* BetPosition.BEAR */.Tu.BEAR,
        claimed: betResponse.claimed,
        claimedAt: numberOrNull(betResponse.claimedAt),
        claimedBlock: numberOrNull(betResponse.claimedBlock),
        claimedHash: betResponse.claimedHash,
        claimedBNB: betResponse.claimedBNB ? parseFloat(betResponse.claimedBNB) : 0,
        claimedNetBNB: betResponse.claimedNetBNB ? parseFloat(betResponse.claimedNetBNB) : 0,
        createdAt: numberOrNull(betResponse.createdAt),
        updatedAt: numberOrNull(betResponse.updatedAt)
    };
    if (betResponse.user) {
        bet.user = transformUserResponse(betResponse.user);
    }
    if (betResponse.round) {
        bet.round = transformRoundResponse(betResponse.round);
    }
    return bet;
};
const transformUserResponse = (userResponse)=>{
    const { id , createdAt , updatedAt , block , totalBets , totalBetsBull , totalBetsBear , totalBNB , totalBNBBull , totalBNBBear , totalBetsClaimed , totalBNBClaimed , winRate , averageBNB , netBNB ,  } = userResponse;
    return {
        id,
        createdAt: numberOrNull(createdAt),
        updatedAt: numberOrNull(updatedAt),
        block: numberOrNull(block),
        totalBets: numberOrNull(totalBets),
        totalBetsBull: numberOrNull(totalBetsBull),
        totalBetsBear: numberOrNull(totalBetsBear),
        totalBNB: totalBNB ? parseFloat(totalBNB) : 0,
        totalBNBBull: totalBNBBull ? parseFloat(totalBNBBull) : 0,
        totalBNBBear: totalBNBBear ? parseFloat(totalBNBBear) : 0,
        totalBetsClaimed: numberOrNull(totalBetsClaimed),
        totalBNBClaimed: totalBNBClaimed ? parseFloat(totalBNBClaimed) : 0,
        winRate: winRate ? parseFloat(winRate) : 0,
        averageBNB: averageBNB ? parseFloat(averageBNB) : 0,
        netBNB: netBNB ? parseFloat(netBNB) : 0
    };
};
const transformRoundResponse = (roundResponse)=>{
    const { id , epoch , failed , position , startAt , startBlock , startHash , lockAt , lockBlock , lockHash , lockPrice , lockRoundId , closeAt , closeBlock , closeHash , closePrice , closeRoundId , totalBets , totalAmount , bullBets , bullAmount , bearBets , bearAmount , bets =[] ,  } = roundResponse;
    return {
        id,
        failed,
        startHash,
        lockHash,
        lockRoundId,
        closeRoundId,
        closeHash,
        position: getRoundPosition(position),
        epoch: numberOrNull(epoch),
        startAt: numberOrNull(startAt),
        startBlock: numberOrNull(startBlock),
        lockAt: numberOrNull(lockAt),
        lockBlock: numberOrNull(lockBlock),
        lockPrice: lockPrice ? parseFloat(lockPrice) : 0,
        closeAt: numberOrNull(closeAt),
        closeBlock: numberOrNull(closeBlock),
        closePrice: closePrice ? parseFloat(closePrice) : 0,
        totalBets: numberOrNull(totalBets),
        totalAmount: totalAmount ? parseFloat(totalAmount) : 0,
        bullBets: numberOrNull(bullBets),
        bullAmount: bullAmount ? parseFloat(bullAmount) : 0,
        bearBets: numberOrNull(bearBets),
        bearAmount: bearAmount ? parseFloat(bearAmount) : 0,
        bets: bets.map(transformBetResponse)
    };
};
const getRoundResult = (bet, currentEpoch)=>{
    const { round  } = bet;
    if (round.failed) {
        return Result.CANCELED;
    }
    if (round.epoch >= currentEpoch - 1) {
        return Result.LIVE;
    }
    if (bet.round.position === BetPosition.HOUSE) {
        return Result.HOUSE;
    }
    const roundResultPosition = round.closePrice > round.lockPrice ? BetPosition.BULL : BetPosition.BEAR;
    return bet.position === roundResultPosition ? Result.WIN : Result.LOSE;
};
const getFilteredBets = (bets, filter)=>{
    switch(filter){
        case HistoryFilter.COLLECTED:
            return bets.filter((bet)=>bet.claimed === true
            );
        case HistoryFilter.UNCOLLECTED:
            return bets.filter((bet)=>{
                return !bet.claimed && (bet.position === bet.round.position || bet.round.failed === true);
            });
        case HistoryFilter.ALL:
        default:
            return bets;
    }
};
const getTotalWon = async ()=>{
    const { market  } = await request(GRAPH_API_PREDICTION, gql`
      query getTotalWonData {
        market(id: 1) {
          totalBNB
          totalBNBTreasury
        }
      }
    `);
    const totalBNB = market.totalBNB ? parseFloat(market.totalBNB) : 0;
    const totalBNBTreasury = market.totalBNBTreasury ? parseFloat(market.totalBNBTreasury) : 0;
    return Math.max(totalBNB - totalBNBTreasury, 0);
};
const getBetHistory = async (where = {}, first = 1000, skip = 0)=>{
    const response = await (0,external_graphql_request_.request)(endpoints/* GRAPH_API_PREDICTION */.v5, external_graphql_request_.gql`
      query getBetHistory($first: Int!, $skip: Int!, $where: Bet_filter) {
        bets(first: $first, skip: $skip, where: $where, order: createdAt, orderDirection: desc) {
          ${queries_getBetBaseFields()}
          round {
            ${queries_getRoundBaseFields()}
          }
          user {
            ${queries_getUserBaseFields()}
          }
        }
      }
    `, {
        first,
        skip,
        where
    });
    return response.bets;
};
const getBet = async (betId)=>{
    const response = await request(GRAPH_API_PREDICTION, gql`
      query getBet($id: ID!) {
        bet(id: $id) {
          ${getBetBaseFields()}
          round {
            ${getRoundBaseFields()}
          }
          user {
            ${getUserBaseFields()}
          }
        }
      }
  `, {
        id: betId.toLowerCase()
    });
    return response.bet;
};
const getLedgerData = async (account, epochs)=>{
    const address = (0,addressHelpers/* getPredictionsAddress */.Df)();
    const ledgerCalls = epochs.map((epoch)=>({
            address,
            name: 'ledger',
            params: [
                epoch,
                account
            ]
        })
    );
    const response = await (0,utils_multicall/* multicallv2 */.v)(predictions, ledgerCalls);
    return response;
};
const LEADERBOARD_RESULTS_PER_PAGE = 20;
const defaultPredictionUserOptions = {
    skip: 0,
    first: LEADERBOARD_RESULTS_PER_PAGE,
    orderBy: 'createdAt',
    orderDir: 'desc'
};
const getPredictionUsers = async (options = {})=>{
    const { first , skip , where , orderBy , orderDir  } = {
        ...defaultPredictionUserOptions,
        ...options
    };
    const response = await (0,external_graphql_request_.request)(endpoints/* GRAPH_API_PREDICTION */.v5, external_graphql_request_.gql`
      query getUsers($first: Int!, $skip: Int!, $where: User_filter, $orderBy: User_orderBy, $orderDir: OrderDirection) {
        users(first: $first, skip: $skip, where: $where, orderBy: $orderBy, orderDirection: $orderDir) {
          ${queries_getUserBaseFields()}
        }
      }
    `, {
        first,
        skip,
        where,
        orderBy,
        orderDir
    });
    return response.users;
};
const getPredictionUser = async (account)=>{
    const response = await (0,external_graphql_request_.request)(endpoints/* GRAPH_API_PREDICTION */.v5, external_graphql_request_.gql`
      query getUser($id: ID!) {
        user(id: $id) {
          ${queries_getUserBaseFields()}
        }
      }
  `, {
        id: account.toLowerCase()
    });
    return response.user;
};
const getClaimStatuses = async (account, epochs)=>{
    const address = (0,addressHelpers/* getPredictionsAddress */.Df)();
    const claimableCalls = epochs.map((epoch)=>({
            address,
            name: 'claimable',
            params: [
                epoch,
                account
            ]
        })
    );
    const claimableResponses = await (0,utils_multicall/* multicallv2 */.v)(predictions, claimableCalls);
    return claimableResponses.reduce((accum, claimableResponse, index)=>{
        const epoch = epochs[index];
        const [claimable] = claimableResponse;
        return {
            ...accum,
            [epoch]: claimable
        };
    }, {});
};
const getPredictionData = async ()=>{
    const address = (0,addressHelpers/* getPredictionsAddress */.Df)();
    const staticCalls = [
        'currentEpoch',
        'intervalSeconds',
        'minBetAmount',
        'paused',
        'bufferSeconds'
    ].map((method)=>({
            address,
            name: method
        })
    );
    const [[currentEpoch], [intervalSeconds], [minBetAmount], [paused], [bufferSeconds]] = await (0,utils_multicall/* multicallv2 */.v)(predictions, staticCalls);
    return {
        status: paused ? state_types/* PredictionStatus.PAUSED */.Gw.PAUSED : state_types/* PredictionStatus.LIVE */.Gw.LIVE,
        currentEpoch: currentEpoch.toNumber(),
        intervalSeconds: intervalSeconds.toNumber(),
        minBetAmount: minBetAmount.toString(),
        bufferSeconds: bufferSeconds.toNumber()
    };
};
const getRoundsData = async (epochs)=>{
    const address = (0,addressHelpers/* getPredictionsAddress */.Df)();
    const calls = epochs.map((epoch)=>({
            address,
            name: 'rounds',
            params: [
                epoch
            ]
        })
    );
    const response = await (0,utils_multicall/* multicallv2 */.v)(predictions, calls);
    return response;
};
const makeFutureRoundResponse = (epoch, startTimestamp)=>{
    return {
        epoch,
        startTimestamp,
        lockTimestamp: null,
        closeTimestamp: null,
        lockPrice: null,
        closePrice: null,
        totalAmount: bignumber_.BigNumber.from(0).toJSON(),
        bullAmount: bignumber_.BigNumber.from(0).toJSON(),
        bearAmount: bignumber_.BigNumber.from(0).toJSON(),
        rewardBaseCalAmount: bignumber_.BigNumber.from(0).toJSON(),
        rewardAmount: bignumber_.BigNumber.from(0).toJSON(),
        oracleCalled: false,
        lockOracleId: null,
        closeOracleId: null
    };
};
const makeRoundData = (rounds)=>{
    return rounds.reduce((accum, round)=>{
        return {
            ...accum,
            [round.epoch.toString()]: round
        };
    }, {});
};
const serializePredictionsLedgerResponse = (ledgerResponse)=>({
        position: ledgerResponse.position === 0 ? state_types/* BetPosition.BULL */.Tu.BULL : state_types/* BetPosition.BEAR */.Tu.BEAR,
        amount: ledgerResponse.amount.toJSON(),
        claimed: ledgerResponse.claimed
    })
;
const makeLedgerData = (account, ledgers, epochs)=>{
    return ledgers.reduce((accum, ledgerResponse, index)=>{
        if (!ledgerResponse) {
            return accum;
        }
        // If the amount is zero that means the user did not bet
        if (ledgerResponse.amount.eq(0)) {
            return accum;
        }
        const epoch = epochs[index].toString();
        return {
            ...accum,
            [account]: {
                ...accum[account],
                [epoch]: serializePredictionsLedgerResponse(ledgerResponse)
            }
        };
    }, {});
};
/**
 * Serializes the return from the "rounds" call for redux
 */ const serializePredictionsRoundsResponse = (response)=>{
    const { epoch , startTimestamp , lockTimestamp , closeTimestamp , lockPrice , closePrice , totalAmount , bullAmount , bearAmount , rewardBaseCalAmount , rewardAmount , oracleCalled , lockOracleId , closeOracleId ,  } = response;
    return {
        oracleCalled,
        epoch: epoch.toNumber(),
        startTimestamp: startTimestamp.eq(0) ? null : startTimestamp.toNumber(),
        lockTimestamp: lockTimestamp.eq(0) ? null : lockTimestamp.toNumber(),
        closeTimestamp: closeTimestamp.eq(0) ? null : closeTimestamp.toNumber(),
        lockPrice: lockPrice.eq(0) ? null : lockPrice.toJSON(),
        closePrice: closePrice.eq(0) ? null : closePrice.toJSON(),
        totalAmount: totalAmount.toJSON(),
        bullAmount: bullAmount.toJSON(),
        bearAmount: bearAmount.toJSON(),
        rewardBaseCalAmount: rewardBaseCalAmount.toJSON(),
        rewardAmount: rewardAmount.toJSON(),
        lockOracleId: lockOracleId.toString(),
        closeOracleId: closeOracleId.toString()
    };
};
/**
 * Parse serialized values back into BigNumber
 * BigNumber values are stored with the "toJSON()" method, e.g  { type: "BigNumber", hex: string }
 */ const parseBigNumberObj = (data)=>{
    return Object.keys(data).reduce((accum, key)=>{
        const value = data[key];
        if (value && value?.type === 'BigNumber') {
            return {
                ...accum,
                [key]: BigNumber.from(value)
            };
        }
        return {
            ...accum,
            [key]: value
        };
    }, {});
};
const fetchUsersRoundsLength = async (account)=>{
    try {
        const contract = (0,contractHelpers/* getPredictionsContract */.qi)();
        const length = await contract.getUserRoundsLength(account);
        return length;
    } catch  {
        return bignumber_.BigNumber.from(0);
    }
};
/**
 * Fetches rounds a user has participated in
 */ const fetchUserRounds = async (account, cursor = 0, size = ROUNDS_PER_PAGE)=>{
    const contract = (0,contractHelpers/* getPredictionsContract */.qi)();
    try {
        const [rounds, ledgers] = await contract.getUserRounds(account, cursor, size);
        return rounds.reduce((accum, round, index)=>{
            return {
                ...accum,
                [round.toString()]: serializePredictionsLedgerResponse(ledgers[index])
            };
        }, {});
    } catch  {
        // When the results run out the contract throws an error.
        return null;
    }
};

;// CONCATENATED MODULE: ./src/state/predictions/index.ts












const predictions_initialState = {
    status: state_types/* PredictionStatus.INITIAL */.Gw.INITIAL,
    isLoading: false,
    isHistoryPaneOpen: false,
    isChartPaneOpen: false,
    isFetchingHistory: false,
    historyFilter: state_types/* HistoryFilter.ALL */.dZ.ALL,
    currentEpoch: 0,
    intervalSeconds: 300,
    minBetAmount: '10000000000000',
    bufferSeconds: 60,
    lastOraclePrice: bigNumber/* BIG_ZERO.toJSON */.HW.toJSON(),
    rounds: {},
    history: [],
    totalHistory: 0,
    currentHistoryPage: 1,
    hasHistoryLoaded: false,
    ledgers: {},
    claimableStatuses: {},
    leaderboard: {
        selectedAddress: null,
        loadingState: types/* FetchStatus.Idle */.iF.Idle,
        filters: {
            address: null,
            orderBy: 'netBNB',
            timePeriod: 'all'
        },
        skip: 0,
        hasMoreResults: true,
        addressResults: {},
        results: []
    }
};
const initializePredictions = (0,toolkit_.createAsyncThunk)('predictions/initialize', async (account = null)=>{
    // Static values
    const marketData = await getPredictionData();
    const epochs = marketData.currentEpoch > PAST_ROUND_COUNT ? range_default()(marketData.currentEpoch, marketData.currentEpoch - PAST_ROUND_COUNT) : [
        marketData.currentEpoch
    ];
    // Round data
    const roundsResponse = await getRoundsData(epochs);
    const initialRoundData = roundsResponse.reduce((accum, roundResponse)=>{
        const reduxNodeRound = serializePredictionsRoundsResponse(roundResponse);
        return {
            ...accum,
            [reduxNodeRound.epoch.toString()]: reduxNodeRound
        };
    }, {});
    const initializedData = {
        ...marketData,
        rounds: initialRoundData,
        ledgers: {},
        claimableStatuses: {}
    };
    if (!account) {
        return initializedData;
    }
    // Bet data
    const ledgerResponses = await getLedgerData(account, epochs);
    // Claim statuses
    const claimableStatuses = await getClaimStatuses(account, epochs);
    return merge_default()({}, initializedData, {
        ledgers: makeLedgerData(account, ledgerResponses, epochs),
        claimableStatuses
    });
});
const fetchRound = (0,toolkit_.createAsyncThunk)('predictions/fetchRound', async (epoch)=>{
    const predictionContract = (0,contractHelpers/* getPredictionsContract */.qi)();
    const response = await predictionContract.rounds(epoch);
    return serializePredictionsRoundsResponse(response);
});
const fetchRounds = (0,toolkit_.createAsyncThunk)('predictions/fetchRounds', async (epochs)=>{
    const rounds = await getRoundsData(epochs);
    return rounds.reduce((accum, round)=>{
        if (!round) {
            return accum;
        }
        const reduxNodeRound = serializePredictionsRoundsResponse(round);
        return {
            ...accum,
            [reduxNodeRound.epoch.toString()]: reduxNodeRound
        };
    }, {});
});
const fetchMarketData = (0,toolkit_.createAsyncThunk)('predictions/fetchMarketData', async ()=>{
    const marketData = await getPredictionData();
    return marketData;
});
const fetchLedgerData = (0,toolkit_.createAsyncThunk)('predictions/fetchLedgerData', async ({ account , epochs  })=>{
    const ledgers = await getLedgerData(account, epochs);
    return makeLedgerData(account, ledgers, epochs);
});
const fetchClaimableStatuses = (0,toolkit_.createAsyncThunk)('predictions/fetchClaimableStatuses', async ({ account , epochs  })=>{
    const ledgers = await getClaimStatuses(account, epochs);
    return ledgers;
});
const fetchHistory = (0,toolkit_.createAsyncThunk)('predictions/fetchHistory', async ({ account , claimed  })=>{
    const response = await getBetHistory({
        user: account.toLowerCase(),
        claimed
    });
    const bets = response.map(transformBetResponse);
    return {
        account,
        bets
    };
});
const fetchNodeHistory = (0,toolkit_.createAsyncThunk)('predictions/fetchNodeHistory', async ({ account , page =1  })=>{
    const userRoundsLength = await fetchUsersRoundsLength(account);
    const emptyResult = {
        bets: [],
        claimableStatuses: {},
        totalHistory: userRoundsLength.toNumber()
    };
    const maxPages = userRoundsLength.lte(ROUNDS_PER_PAGE) ? 1 : Math.ceil(userRoundsLength.toNumber() / ROUNDS_PER_PAGE);
    if (userRoundsLength.eq(0)) {
        return emptyResult;
    }
    if (page > maxPages) {
        return emptyResult;
    }
    const cursor = userRoundsLength.sub(ROUNDS_PER_PAGE * page);
    // If the page request is the final one we only want to retrieve the amount of rounds up to the next cursor.
    const size = maxPages === page ? userRoundsLength.sub(ROUNDS_PER_PAGE * (page - 1)) // Previous page's cursor
    .toNumber() : ROUNDS_PER_PAGE;
    const userRounds = await fetchUserRounds(account, cursor.lt(0) ? 0 : cursor.toNumber(), size);
    if (!userRounds) {
        return emptyResult;
    }
    const epochs = Object.keys(userRounds).map((epochStr)=>Number(epochStr)
    );
    const roundData = await getRoundsData(epochs);
    const claimableStatuses = await getClaimStatuses(account, epochs);
    // Turn the data from the node into an Bet object that comes from the graph
    const bets = roundData.reduce((accum, round)=>{
        const reduxRound = serializePredictionsRoundsResponse(round);
        const ledger = userRounds[reduxRound.epoch];
        const ledgerAmount = bignumber_.BigNumber.from(ledger.amount);
        const closePrice = round.closePrice ? parseFloat((0,units_.formatUnits)(round.closePrice, 8)) : null;
        const lockPrice = round.lockPrice ? parseFloat((0,units_.formatUnits)(round.lockPrice, 8)) : null;
        const getRoundPosition = ()=>{
            if (!closePrice) {
                return null;
            }
            if (round.closePrice.eq(round.lockPrice)) {
                return state_types/* BetPosition.HOUSE */.Tu.HOUSE;
            }
            return round.closePrice.gt(round.lockPrice) ? state_types/* BetPosition.BULL */.Tu.BULL : state_types/* BetPosition.BEAR */.Tu.BEAR;
        };
        return [
            ...accum,
            {
                id: null,
                hash: null,
                amount: parseFloat((0,units_.formatUnits)(ledgerAmount)),
                position: ledger.position,
                claimed: ledger.claimed,
                claimedAt: null,
                claimedHash: null,
                claimedBNB: 0,
                claimedNetBNB: 0,
                createdAt: null,
                updatedAt: null,
                block: 0,
                round: {
                    id: null,
                    epoch: round.epoch.toNumber(),
                    failed: false,
                    startBlock: null,
                    startAt: round.startTimestamp ? round.startTimestamp.toNumber() : null,
                    startHash: null,
                    lockAt: round.lockTimestamp ? round.lockTimestamp.toNumber() : null,
                    lockBlock: null,
                    lockPrice,
                    lockHash: null,
                    lockRoundId: round.lockOracleId ? round.lockOracleId.toString() : null,
                    closeRoundId: round.closeOracleId ? round.closeOracleId.toString() : null,
                    closeHash: null,
                    closeAt: null,
                    closePrice,
                    closeBlock: null,
                    totalBets: 0,
                    totalAmount: parseFloat((0,units_.formatUnits)(round.totalAmount)),
                    bullBets: 0,
                    bullAmount: parseFloat((0,units_.formatUnits)(round.bullAmount)),
                    bearBets: 0,
                    bearAmount: parseFloat((0,units_.formatUnits)(round.bearAmount)),
                    position: getRoundPosition()
                }
            }, 
        ];
    }, []);
    return {
        bets,
        claimableStatuses,
        page,
        totalHistory: userRoundsLength.toNumber()
    };
});
// Leaderboard
const filterLeaderboard = (0,toolkit_.createAsyncThunk)('predictions/filterLeaderboard', async ({ filters  })=>{
    const usersResponse = await getPredictionUsers({
        skip: 0,
        orderBy: filters.orderBy,
        where: {
            totalBets_gte: LEADERBOARD_MIN_ROUNDS_PLAYED,
            [`${filters.orderBy}_gt`]: 0
        }
    });
    return {
        results: usersResponse.map(transformUserResponse)
    };
});
const fetchAddressResult = (0,toolkit_.createAsyncThunk)('predictions/fetchAddressResult', async (account, { rejectWithValue  })=>{
    const userResponse = await getPredictionUser(account);
    if (!userResponse) {
        return rejectWithValue(account);
    }
    return {
        account,
        data: transformUserResponse(userResponse)
    };
});
const filterNextPageLeaderboard = (0,toolkit_.createAsyncThunk)('predictions/filterNextPageLeaderboard', async (skip, { getState  })=>{
    const state = getState();
    const usersResponse = await getPredictionUsers({
        skip,
        orderBy: state.predictions.leaderboard.filters.orderBy,
        where: {
            totalBets_gte: LEADERBOARD_MIN_ROUNDS_PLAYED,
            [`${state.predictions.leaderboard.filters.orderBy}_gt`]: 0
        }
    });
    return {
        results: usersResponse.map(transformUserResponse),
        skip
    };
});
const predictionsSlice = (0,toolkit_.createSlice)({
    name: 'predictions',
    initialState: predictions_initialState,
    reducers: {
        setLeaderboardFilter: (state, action)=>{
            state.leaderboard.filters = {
                ...state.leaderboard.filters,
                ...action.payload
            };
            // Anytime we filters change we need to reset back to page 1
            state.leaderboard.skip = 0;
            state.leaderboard.hasMoreResults = true;
        },
        setHistoryPaneState: (state, action)=>{
            state.isHistoryPaneOpen = action.payload;
            state.historyFilter = state_types/* HistoryFilter.ALL */.dZ.ALL;
        },
        setChartPaneState: (state, action)=>{
            state.isChartPaneOpen = action.payload;
        },
        setHistoryFilter: (state, action)=>{
            state.historyFilter = action.payload;
        },
        setLastOraclePrice: (state, action)=>{
            state.lastOraclePrice = action.payload;
        },
        markAsCollected: (state, action)=>{
            state.claimableStatuses = {
                ...state.claimableStatuses,
                ...action.payload
            };
        },
        setSelectedAddress: (state, action)=>{
            state.leaderboard.selectedAddress = action.payload;
        }
    },
    extraReducers: (builder)=>{
        // Leaderboard filter
        builder.addCase(filterLeaderboard.pending, (state)=>{
            // Only mark as loading if we come from Fetched. This allows initialization.
            if (state.leaderboard.loadingState === types/* FetchStatus.Fetched */.iF.Fetched) {
                state.leaderboard.loadingState = types/* FetchStatus.Fetched */.iF.Fetched;
            }
        });
        builder.addCase(filterLeaderboard.fulfilled, (state, action)=>{
            const { results  } = action.payload;
            state.leaderboard.loadingState = types/* FetchStatus.Fetched */.iF.Fetched;
            state.leaderboard.results = results;
            if (results.length < LEADERBOARD_RESULTS_PER_PAGE) {
                state.leaderboard.hasMoreResults = false;
            }
            // Populate address results to reduce calls
            state.leaderboard.addressResults = {
                ...state.leaderboard.addressResults,
                ...results.reduce((accum, result)=>{
                    return {
                        ...accum,
                        [result.id]: result
                    };
                }, {})
            };
        });
        // Leaderboard account result
        builder.addCase(fetchAddressResult.pending, (state)=>{
            state.leaderboard.loadingState = types/* FetchStatus.Fetching */.iF.Fetching;
        });
        builder.addCase(fetchAddressResult.fulfilled, (state, action)=>{
            const { account , data  } = action.payload;
            state.leaderboard.loadingState = types/* FetchStatus.Fetched */.iF.Fetched;
            state.leaderboard.addressResults[account] = data;
        });
        builder.addCase(fetchAddressResult.rejected, (state, action)=>{
            state.leaderboard.loadingState = types/* FetchStatus.Fetched */.iF.Fetched // TODO: should handle error
            ;
            state.leaderboard.addressResults[action.payload] = null;
        });
        // Leaderboard next page
        builder.addCase(filterNextPageLeaderboard.pending, (state)=>{
            state.leaderboard.loadingState = types/* FetchStatus.Fetching */.iF.Fetching;
        });
        builder.addCase(filterNextPageLeaderboard.fulfilled, (state, action)=>{
            const { results , skip  } = action.payload;
            state.leaderboard.loadingState = types/* FetchStatus.Fetched */.iF.Fetched;
            state.leaderboard.results = [
                ...state.leaderboard.results,
                ...results
            ];
            state.leaderboard.skip = skip;
            if (results.length < LEADERBOARD_RESULTS_PER_PAGE) {
                state.leaderboard.hasMoreResults = false;
            }
        });
        // Claimable statuses
        builder.addCase(fetchClaimableStatuses.fulfilled, (state, action)=>{
            state.claimableStatuses = merge_default()({}, state.claimableStatuses, action.payload);
        });
        // Ledger (bet) records
        builder.addCase(fetchLedgerData.fulfilled, (state, action)=>{
            state.ledgers = merge_default()({}, state.ledgers, action.payload);
        });
        // Get static market data
        builder.addCase(fetchMarketData.fulfilled, (state, action)=>{
            const { status , currentEpoch , intervalSeconds , minBetAmount  } = action.payload;
            // If the round has change add a new future round
            if (state.currentEpoch !== currentEpoch) {
                const newestRound = maxBy_default()(Object.values(state.rounds), 'epoch');
                const futureRound = makeFutureRoundResponse(newestRound.epoch + 1, newestRound.startTimestamp + intervalSeconds + ROUND_BUFFER);
                state.rounds[futureRound.epoch] = futureRound;
            }
            state.status = status;
            state.currentEpoch = currentEpoch;
            state.intervalSeconds = intervalSeconds;
            state.minBetAmount = minBetAmount;
        });
        // Initialize predictions
        builder.addCase(initializePredictions.fulfilled, (state, action)=>{
            const { status , currentEpoch , intervalSeconds , bufferSeconds , rounds , claimableStatuses , ledgers  } = action.payload;
            const futureRounds = [];
            const currentRound = rounds[currentEpoch];
            for(let i = 1; i <= FUTURE_ROUND_COUNT; i++){
                futureRounds.push(makeFutureRoundResponse(currentEpoch + i, currentRound.startTimestamp + intervalSeconds * i));
            }
            return {
                ...state,
                status,
                currentEpoch,
                intervalSeconds,
                bufferSeconds,
                claimableStatuses,
                ledgers,
                rounds: merge_default()({}, rounds, makeRoundData(futureRounds))
            };
        });
        // Get single round
        builder.addCase(fetchRound.fulfilled, (state, action)=>{
            state.rounds = merge_default()({}, state.rounds, {
                [action.payload.epoch.toString()]: action.payload
            });
        });
        // Get multiple rounds
        builder.addCase(fetchRounds.fulfilled, (state, action)=>{
            state.rounds = merge_default()({}, state.rounds, action.payload);
        });
        // Show History
        builder.addCase(fetchHistory.pending, (state)=>{
            state.isFetchingHistory = true;
        });
        builder.addCase(fetchHistory.rejected, (state)=>{
            state.isFetchingHistory = false;
        });
        builder.addCase(fetchHistory.fulfilled, (state, action)=>{
            const { account , bets  } = action.payload;
            state.isFetchingHistory = false;
            state.history[account] = merge_default()([], state.history[account] ?? [], bets);
        });
        // History from the node
        builder.addCase(fetchNodeHistory.pending, (state)=>{
            state.isFetchingHistory = true;
        });
        builder.addCase(fetchNodeHistory.rejected, (state)=>{
            state.isFetchingHistory = false;
        });
        builder.addCase(fetchNodeHistory.fulfilled, (state, action)=>{
            const { bets , claimableStatuses , page , totalHistory  } = action.payload;
            state.isFetchingHistory = false;
            state.history = page === 1 ? bets : [
                ...state.history,
                ...bets
            ];
            state.claimableStatuses = {
                ...state.claimableStatuses,
                ...claimableStatuses
            };
            state.hasHistoryLoaded = state.history.length === totalHistory || bets.length === 0;
            state.totalHistory = totalHistory;
            state.currentHistoryPage = page;
        });
    }
});
// Actions
const { setChartPaneState , setHistoryFilter , setHistoryPaneState , setLastOraclePrice , markAsCollected , setLeaderboardFilter , setSelectedAddress ,  } = predictionsSlice.actions;
/* harmony default export */ const state_predictions = (predictionsSlice.reducer);

// EXTERNAL MODULE: ./src/state/profile/index.tsx
var profile = __webpack_require__(5952);
// EXTERNAL MODULE: ./src/state/swap/actions.ts
var swap_actions = __webpack_require__(193);
;// CONCATENATED MODULE: ./src/state/swap/reducer.ts


const swap_reducer_initialState = {
    independentField: swap_actions/* Field.INPUT */.gN.INPUT,
    typedValue: '',
    [swap_actions/* Field.INPUT */.gN.INPUT]: {
        currencyId: ''
    },
    [swap_actions/* Field.OUTPUT */.gN.OUTPUT]: {
        currencyId: ''
    },
    pairDataById: {},
    derivedPairDataById: {},
    recipient: null
};
/* harmony default export */ const swap_reducer = ((0,toolkit_.createReducer)(swap_reducer_initialState, (builder)=>builder.addCase(swap_actions/* replaceSwapState */.mV, (state, { payload: { typedValue , recipient , field , inputCurrencyId , outputCurrencyId  }  })=>{
        return {
            [swap_actions/* Field.INPUT */.gN.INPUT]: {
                currencyId: inputCurrencyId
            },
            [swap_actions/* Field.OUTPUT */.gN.OUTPUT]: {
                currencyId: outputCurrencyId
            },
            independentField: field,
            typedValue,
            recipient,
            pairDataById: state.pairDataById,
            derivedPairDataById: state.derivedPairDataById
        };
    }).addCase(swap_actions/* selectCurrency */.j, (state, { payload: { currencyId , field  }  })=>{
        const otherField = field === swap_actions/* Field.INPUT */.gN.INPUT ? swap_actions/* Field.OUTPUT */.gN.OUTPUT : swap_actions/* Field.INPUT */.gN.INPUT;
        if (currencyId === state[otherField].currencyId) {
            // the case where we have to swap the order
            return {
                ...state,
                independentField: state.independentField === swap_actions/* Field.INPUT */.gN.INPUT ? swap_actions/* Field.OUTPUT */.gN.OUTPUT : swap_actions/* Field.INPUT */.gN.INPUT,
                [field]: {
                    currencyId
                },
                [otherField]: {
                    currencyId: state[field].currencyId
                }
            };
        }
        // the normal case
        return {
            ...state,
            [field]: {
                currencyId
            }
        };
    }).addCase(swap_actions/* switchCurrencies */.KS, (state)=>{
        return {
            ...state,
            independentField: state.independentField === swap_actions/* Field.INPUT */.gN.INPUT ? swap_actions/* Field.OUTPUT */.gN.OUTPUT : swap_actions/* Field.INPUT */.gN.INPUT,
            [swap_actions/* Field.INPUT */.gN.INPUT]: {
                currencyId: state[swap_actions/* Field.OUTPUT */.gN.OUTPUT].currencyId
            },
            [swap_actions/* Field.OUTPUT */.gN.OUTPUT]: {
                currencyId: state[swap_actions/* Field.INPUT */.gN.INPUT].currencyId
            }
        };
    }).addCase(swap_actions/* typeInput */.LC, (state, { payload: { field , typedValue  }  })=>{
        return {
            ...state,
            independentField: field,
            typedValue
        };
    }).addCase(swap_actions/* setRecipient */.He, (state, { payload: { recipient  }  })=>{
        state.recipient = recipient;
    }).addCase(swap_actions/* updatePairData */.Wk, (state, { payload: { pairData , pairId , timeWindow  }  })=>{
        if (!state.pairDataById[pairId]) {
            state.pairDataById[pairId] = {};
        }
        state.pairDataById[pairId][timeWindow] = pairData;
    }).addCase(swap_actions/* updateDerivedPairData */._U, (state, { payload: { pairData , pairId , timeWindow  }  })=>{
        if (!state.derivedPairDataById[pairId]) {
            state.derivedPairDataById[pairId] = {};
        }
        state.derivedPairDataById[pairId][timeWindow] = pairData;
    })
));

// EXTERNAL MODULE: ./src/config/constants/teams.ts
var teams = __webpack_require__(7275);
// EXTERNAL MODULE: ./src/state/teams/helpers.ts
var teams_helpers = __webpack_require__(9423);
;// CONCATENATED MODULE: ./src/state/teams/index.tsx



const teamsById = teams/* default.reduce */.Z.reduce((accum, team)=>{
    return {
        ...accum,
        [team.id]: team
    };
}, {});
const teams_initialState = {
    isInitialized: false,
    isLoading: true,
    data: teamsById
};
const teamsSlice = (0,toolkit_.createSlice)({
    name: 'teams',
    initialState: teams_initialState,
    reducers: {
        fetchStart: (state)=>{
            state.isLoading = true;
        },
        fetchFailed: (state)=>{
            state.isLoading = false;
            state.isInitialized = true;
        },
        teamFetchSucceeded: (state, action)=>{
            state.isInitialized = true;
            state.isLoading = false;
            state.data[action.payload.id] = action.payload;
        },
        teamsFetchSucceeded: (state, action)=>{
            state.isInitialized = true;
            state.isLoading = false;
            state.data = action.payload;
        }
    }
});
// Actions
const { fetchStart , teamFetchSucceeded , fetchFailed , teamsFetchSucceeded  } = teamsSlice.actions;
// Thunks
const fetchTeam = (teamId)=>async (dispatch)=>{
        try {
            dispatch(fetchStart());
            const team = await getTeam(teamId);
            dispatch(teamFetchSucceeded(team));
        } catch (error) {
            dispatch(fetchFailed());
        }
    }
;
const fetchTeams = ()=>async (dispatch)=>{
        try {
            dispatch(fetchStart());
            const teams = await getTeams();
            dispatch(teamsFetchSucceeded(teams));
        } catch (error) {
            dispatch(fetchFailed());
        }
    }
;
/* harmony default export */ const state_teams = (teamsSlice.reducer);

// EXTERNAL MODULE: ./src/state/transactions/actions.ts
var transactions_actions = __webpack_require__(1564);
;// CONCATENATED MODULE: ./src/state/transactions/reducer.ts
/* eslint-disable no-param-reassign */ 

const now = ()=>new Date().getTime()
;
const transactions_reducer_initialState = {};
/* harmony default export */ const transactions_reducer = ((0,toolkit_.createReducer)(transactions_reducer_initialState, (builder)=>builder.addCase(transactions_actions/* addTransaction */.dT, (transactions, { payload: { chainId , from , hash , approval , summary , claim  }  })=>{
        if (transactions[chainId]?.[hash]) {
            throw Error('Attempted to add existing transaction.');
        }
        const txs = transactions[chainId] ?? {};
        txs[hash] = {
            hash,
            approval,
            summary,
            claim,
            from,
            addedTime: now()
        };
        transactions[chainId] = txs;
    }).addCase(transactions_actions/* clearAllTransactions */.fY, (transactions, { payload: { chainId  }  })=>{
        if (!transactions[chainId]) return;
        transactions[chainId] = {};
    }).addCase(transactions_actions/* checkedTransaction */.LN, (transactions, { payload: { chainId , hash , blockNumber  }  })=>{
        const tx = transactions[chainId]?.[hash];
        if (!tx) {
            return;
        }
        if (!tx.lastCheckedBlockNumber) {
            tx.lastCheckedBlockNumber = blockNumber;
        } else {
            tx.lastCheckedBlockNumber = Math.max(blockNumber, tx.lastCheckedBlockNumber);
        }
    }).addCase(transactions_actions/* finalizeTransaction */.Aw, (transactions, { payload: { hash , chainId , receipt  }  })=>{
        const tx = transactions[chainId]?.[hash];
        if (!tx) {
            return;
        }
        tx.receipt = receipt;
        tx.confirmedTime = now();
    })
));

// EXTERNAL MODULE: ./src/config/constants/index.ts
var config_constants = __webpack_require__(3862);
// EXTERNAL MODULE: ./src/state/user/actions.ts
var user_actions = __webpack_require__(6245);
;// CONCATENATED MODULE: ./src/state/user/reducer.ts





const currentTimestamp = ()=>new Date().getTime()
;
function pairKey(token0Address, token1Address) {
    return `${token0Address};${token1Address}`;
}
const user_reducer_initialState = {
    userExpertMode: false,
    userSingleHopOnly: false,
    userSlippageTolerance: config_constants/* INITIAL_ALLOWED_SLIPPAGE */.gv,
    userDeadline: config_constants/* DEFAULT_DEADLINE_FROM_NOW */.PY,
    tokens: {},
    pairs: {},
    timestamp: currentTimestamp(),
    audioPlay: true,
    isDark: false,
    isExchangeChartDisplayed: true,
    isSubgraphHealthIndicatorDisplayed: false,
    userChartViewMode: user_actions/* ChartViewMode.BASIC */.UU.BASIC,
    userFarmStakedOnly: user_actions/* FarmStakedOnly.ON_FINISHED */.GR.ON_FINISHED,
    userPoolStakedOnly: false,
    userPoolsViewMode: user_actions/* ViewMode.TABLE */.wO.TABLE,
    userFarmsViewMode: user_actions/* ViewMode.TABLE */.wO.TABLE,
    userPredictionAcceptedRisk: false,
    userPredictionChartDisclaimerShow: true,
    userExpertModeAcknowledgementShow: true,
    userUsernameVisibility: false,
    gasPrice: hooks_helpers/* GAS_PRICE_GWEI.default */.j4["default"],
    watchlistTokens: [],
    watchlistPools: [],
    hideTimestampPhishingWarningBanner: null
};
/* harmony default export */ const user_reducer = ((0,toolkit_.createReducer)(user_reducer_initialState, (builder)=>builder.addCase(updateVersion, (state)=>{
        // slippage isnt being tracked in local storage, reset to default
        // noinspection SuspiciousTypeOfGuard
        if (typeof state.userSlippageTolerance !== 'number') {
            state.userSlippageTolerance = config_constants/* INITIAL_ALLOWED_SLIPPAGE */.gv;
        }
        // deadline isnt being tracked in local storage, reset to default
        // noinspection SuspiciousTypeOfGuard
        if (typeof state.userDeadline !== 'number') {
            state.userDeadline = config_constants/* DEFAULT_DEADLINE_FROM_NOW */.PY;
        }
        state.lastUpdateVersionTimestamp = currentTimestamp();
    }).addCase(user_actions/* updateUserExpertMode */.zv, (state, action)=>{
        state.userExpertMode = action.payload.userExpertMode;
        state.timestamp = currentTimestamp();
    }).addCase(user_actions/* updateUserSlippageTolerance */.rQ, (state, action)=>{
        state.userSlippageTolerance = action.payload.userSlippageTolerance;
        state.timestamp = currentTimestamp();
    }).addCase(user_actions/* updateUserDeadline */.gw, (state, action)=>{
        state.userDeadline = action.payload.userDeadline;
        state.timestamp = currentTimestamp();
    }).addCase(user_actions/* updateUserSingleHopOnly */.fO, (state, action)=>{
        state.userSingleHopOnly = action.payload.userSingleHopOnly;
    }).addCase(user_actions/* addSerializedToken */.eg, (state, { payload: { serializedToken  }  })=>{
        if (!state.tokens) {
            state.tokens = {};
        }
        state.tokens[serializedToken.chainId] = state.tokens[serializedToken.chainId] || {};
        state.tokens[serializedToken.chainId][serializedToken.address] = serializedToken;
        state.timestamp = currentTimestamp();
    }).addCase(user_actions/* removeSerializedToken */.zQ, (state, { payload: { address , chainId  }  })=>{
        if (!state.tokens) {
            state.tokens = {};
        }
        state.tokens[chainId] = state.tokens[chainId] || {};
        delete state.tokens[chainId][address];
        state.timestamp = currentTimestamp();
    }).addCase(user_actions/* addSerializedPair */.f9, (state, { payload: { serializedPair  }  })=>{
        if (serializedPair.token0.chainId === serializedPair.token1.chainId && serializedPair.token0.address !== serializedPair.token1.address) {
            const { chainId  } = serializedPair.token0;
            state.pairs[chainId] = state.pairs[chainId] || {};
            state.pairs[chainId][pairKey(serializedPair.token0.address, serializedPair.token1.address)] = serializedPair;
        }
        state.timestamp = currentTimestamp();
    }).addCase(user_actions/* removeSerializedPair */.cd, (state, { payload: { chainId , tokenAAddress , tokenBAddress  }  })=>{
        if (state.pairs[chainId]) {
            // just delete both keys if either exists
            delete state.pairs[chainId][pairKey(tokenAAddress, tokenBAddress)];
            delete state.pairs[chainId][pairKey(tokenBAddress, tokenAAddress)];
        }
        state.timestamp = currentTimestamp();
    }).addCase(user_actions/* muteAudio */.B8, (state)=>{
        state.audioPlay = false;
    }).addCase(user_actions/* unmuteAudio */.u7, (state)=>{
        state.audioPlay = true;
    }).addCase(user_actions/* toggleTheme */.X8, (state)=>{
        state.isDark = !state.isDark;
    }).addCase(user_actions/* updateUserFarmStakedOnly */.Gs, (state, { payload: { userFarmStakedOnly  }  })=>{
        state.userFarmStakedOnly = userFarmStakedOnly;
    }).addCase(user_actions/* updateUserPoolStakedOnly */.mm, (state, { payload: { userPoolStakedOnly  }  })=>{
        state.userPoolStakedOnly = userPoolStakedOnly;
    }).addCase(user_actions/* updateUserPoolsViewMode */.d4, (state, { payload: { userPoolsViewMode  }  })=>{
        state.userPoolsViewMode = userPoolsViewMode;
    }).addCase(user_actions/* updateUserFarmsViewMode */.gk, (state, { payload: { userFarmsViewMode  }  })=>{
        state.userFarmsViewMode = userFarmsViewMode;
    }).addCase(user_actions/* updateUserPredictionAcceptedRisk */.RC, (state, { payload: { userAcceptedRisk  }  })=>{
        state.userPredictionAcceptedRisk = userAcceptedRisk;
    }).addCase(user_actions/* updateUserPredictionChartDisclaimerShow */.c4, (state, { payload: { userShowDisclaimer  }  })=>{
        state.userPredictionChartDisclaimerShow = userShowDisclaimer;
    }).addCase(user_actions/* updateUserExpertModeAcknowledgementShow */._C, (state, { payload: { userExpertModeAcknowledgementShow  }  })=>{
        state.userExpertModeAcknowledgementShow = userExpertModeAcknowledgementShow;
    }).addCase(user_actions/* updateUserUsernameVisibility */.zk, (state, { payload: { userUsernameVisibility  }  })=>{
        state.userUsernameVisibility = userUsernameVisibility;
    }).addCase(user_actions/* updateGasPrice */.dy, (state, action)=>{
        state.gasPrice = action.payload.gasPrice;
    }).addCase(user_actions/* addWatchlistToken */.zS, (state, { payload: { address  }  })=>{
        // state.watchlistTokens can be undefined for pre-loaded localstorage user state
        const tokenWatchlist = state.watchlistTokens ?? [];
        if (!tokenWatchlist.includes(address)) {
            state.watchlistTokens = [
                ...tokenWatchlist,
                address
            ];
        } else {
            // Remove token from watchlist
            const newTokens = state.watchlistTokens.filter((x)=>x !== address
            );
            state.watchlistTokens = newTokens;
        }
    }).addCase(user_actions/* addWatchlistPool */.Dn, (state, { payload: { address  }  })=>{
        // state.watchlistPools can be undefined for pre-loaded localstorage user state
        const poolsWatchlist = state.watchlistPools ?? [];
        if (!poolsWatchlist.includes(address)) {
            state.watchlistPools = [
                ...poolsWatchlist,
                address
            ];
        } else {
            // Remove pool from watchlist
            const newPools = state.watchlistPools.filter((x)=>x !== address
            );
            state.watchlistPools = newPools;
        }
    }).addCase(user_actions/* hidePhishingWarningBanner */.l5, (state)=>{
        state.hideTimestampPhishingWarningBanner = currentTimestamp();
    }).addCase(user_actions/* setIsExchangeChartDisplayed */.hN, (state, { payload  })=>{
        state.isExchangeChartDisplayed = payload;
    }).addCase(user_actions/* setChartViewMode */.p9, (state, { payload  })=>{
        state.userChartViewMode = payload;
    }).addCase(user_actions/* setSubgraphHealthIndicatorDisplayed */.Hr, (state, { payload  })=>{
        state.isSubgraphHealthIndicatorDisplayed = payload;
    })
));

// EXTERNAL MODULE: external "lodash/keyBy"
var keyBy_ = __webpack_require__(3385);
var keyBy_default = /*#__PURE__*/__webpack_require__.n(keyBy_);
;// CONCATENATED MODULE: ./src/config/constants/poolsDeployedBlockNumber.ts
const poolsDeployedBlockNumber = {
    '0x73feaa1eE314F8c655E354234017bE2193C9E24E': 699498,
    '0x09e727c83a75fFdB729280639eDBf947dB76EeB7': 14068005,
    '0x2718D56aE2b8F08B3076A409bBF729542233E451': 13930663,
    '0x2461ea28907A2028b2bCa40040396F64B4141004': 13723466,
    '0x9e31aef040941E67356519f44bcA07c5f82215e5': 13694629,
    '0x1c0C7F3B07a42efb4e15679a9ed7e70B2d7Cc157': 13668585,
    '0x56Bfb98EBEF4344dF2d88c6b80694Cba5EfC56c8': 13517499,
    '0x07984aBb7489CD436d27875c07Eb532d4116795a': 13315659,
    '0xF1fA41f593547E406a203b681df18acCC3971A43': 13265669,
    '0x13A40BFab005D9284f8938FBb70Bf39982580e4D': 13156390,
    '0x0914b2d9D4DD7043893DEF53ecFC0F1179F87d5c': 13100655,
    '0xd97ee2bfe79a4d4ab388553411c462fbb536a88c': 13073445,
    '0x2EfE8772EB97B74be742d578A654AB6C95bF18db': 12967830,
    '0x7F103689cabe17C2F70DA6faa298045d72a943b8': 12942691,
    '0xbd52ef04DB1ad1c68A8FA24Fa71f2188978ba617': 12748434,
    '0x73bB10B89091f15e8FeD4d6e9EBa6415df6acb21': 12720810,
    '0xdD52FAB121376432DBCBb47592742F9d86CF8952': 12692166,
    '0x2b8751B7141Efa7a9917f9C6fea2CEA071af5eE7': 12583296,
    '0xfDFb4DbE94916F9f55dBC2c14Ea8B3e386eCD9F9': 12556338,
    '0x79f5f7DDADeFa0A9e850DFFC4fBa77e5172Fe701': 12386824,
    '0x9b861A078B2583373A7a3EEf815bE1A39125Ae08': 12352983,
    '0xa35caA9509a2337E22C54C929146D5F7f6515794': 12331745,
    '0x6e63B2B96c77532ea7ec2B3D3BFA9C8e1d383f3C': 12161143,
    '0xFef4B7a0194159d89717Efa592384d42B28D3926': 12133079,
    '0x2D26e4b9a5F19eD5BB7AF221DC02432D31DEB4dA': 12105173,
    '0xd008416c2c9cf23843bd179aa3cefedb4c8d1607': 11964262,
    '0xd9b63bb6c62fe2e9a641699a91e680994b8b0081': 11964263,
    '0xCc2D359c3a99d9cfe8e6F31230142efF1C828e6D': 11789725,
    '0x65C0940C50A3C98AEEc95a115Ae62E9804588713': 11707445,
    '0x6f660c58723922c6f866a058199ff4881019b4b5': 11593585,
    '0xc28c400F2B675b25894FA632205ddec71E432288': 12499891,
    '0x8d018823d13c56d62ffb795151a9e629c21e047b': 11131942,
    '0x4D1Ec426d0d7fb6bF344Dd372d0502EDD71c8d88': 10874023,
    '0xCB41a72067c227D6Ed7bc7CFAcd13eCe47Dfe5E9': 10731524,
    '0xcecba456fefe5b18d43df23419e7ab755b436655': 10673172,
    '0x8ed7acf12b08274d5cdaf03d43d0e54bcbdd487e': 10560115,
    '0xC4b15117BC0be030c20754FF36197641477af5d1': 10531042,
    '0xB72dEf58D0832f747d6B7197471Fe20AeA7EB463': 10502091,
    '0xb38b78529bCc895dA16CE2978D6cD6C56e8CfFC3': 10360932,
    '0x2E101b5F7f910F2609e5AcE5f43bD274b1DE09AA': 10305925,
    '0x52733Ad7b4D09BF613b0389045e33E2F287afa04': 10189589,
    '0x401b9b97bdbc3197c1adfab9652dc78040bd1e13': 9933419,
    '0xBeDb490970204cb3CC7B0fea94463BeD67d5364D': 9875995,
    '0xb6e510ae2da1ab4e350f837c70823ab75091780e': 9787670,
    '0x8aa5b2c67852ed5334c8a7f0b5eb0ef975106793': 9677422,
    '0x3b804460c3c62f0f565af593984159f13b1ac976': 9677361,
    '0x455f4d4cc4d6ca15441a93c631e82aaf338ad843': 9593263,
    '0xde4aef42bb27a2cb45c746acde4e4d8ab711d27c': 9570241,
    '0x57d3524888ded4085d9124a422f13b27c8a43de7': 9543912,
    '0xb56299d8fbf46c509014b103a164ad1fc65ea222': 9515302,
    '0x5e49531BA07bE577323e55666D46C6217164119E': 9408776,
    '0xBB472601B3CB32723d0755094BA80B73F67f2AF3': 9371201,
    '0x583A36816F3b8401C4fdf682203E0caDA6997740': 9371225,
    '0x28050e8f024e05f9ddbef5f60dd49f536dba0cf0': 9313000,
    '0xb2b62f88ab82ed0bb4ab4da60d9dc9acf9e816e5': 9197147,
    '0xd1812e7e28c39e78727592de030fc0e7c366d61a': 9082458,
    '0x97058cf9b36c9ef1622485cef22e72d6fea32a36': 9000546,
    '0xe595456846155e23b24cc9cbee910ee97027db6d': 8966747,
    '0xae611c6d4d3ca2cee44cd34eb7aac29d5a387fcf': 8936162,
    '0x135827eaf9746573c0b013f18ee12f138b9b0384': 8768702,
    '0x09b8a5f51c9e245402057851ada274174fa00e2a': 8738260,
    '0x53a2d1db049b5271c6b6db020dba0e8a7c4eb90d': 8707191,
    '0x4da8da81647ee0aa7350e9959f3e4771eb753da0': 8686147,
    '0x0446b8f8474c590d2249a4acdd6eedbc2e004bca': 8600355,
    '0x391240A007Bfd8A59bA74978D691219a76c64c5C': 8571013,
    '0x017DEa5C58c2Bcf57FA73945073dF7AD4052a71C': 8542692,
    '0x6Bd94783caCef3fb7eAa9284f1631c464479829f': 8542727,
    '0x7c71723fB1F9Cfb250B702cfc4eBd5D9Ab2E83d9': 8514242,
    '0x9C8813d7D0A61d30610a7A5FdEF9109e196a3D77': 8509358,
    '0xa07a91da6d10173f33c294803684bceede325957': 8485573,
    '0x88c321d444c88acf3e747dc90f20421b97648903': 8481687,
    '0x3c7234c496d76133b48bd6a342e7aea4f8d87fc8': 8479284,
    '0x64473c33c360f315cab38674f1633505d1d8dcb2': 8399111,
    '0x5cc7a19a50be2a6b2540ebcd55bd728e732e59c3': 8371219,
    '0x2666e2494e742301ffc8026e476acc1710a775ed': 8366484,
    '0x6ac2213F09A404c86AFf506Aa51B6a5BF1F6e24E': 8338815,
    '0x35BD47263f7E57368Df76339903C53bAa99076e1': 8313186,
    '0x62dEc3A560D2e8A84D30752bA454f97b26757877': 8285705,
    '0x44d1f81e80e43e935d66d65874354ef91e5e49f6': 8199967,
    '0x4ea43fce546975aae120c9eeceb172500be4a02b': 8171307,
    '0x567fd708e788e51b68666b9310ee9df163d60fae': 8142496,
    '0x36f9452083fc9bc469a31e7966b873f402292433': 8113900,
    '0xc612680457751d0d01b5d901ad08132a3b001900': 8081951,
    '0x336bcd59f2b6eb7221a99f7a50fd03c6bf9a306b': 7993938,
    '0x2b3974dda76b2d408b7d680a27fbb0393e3cf0e1': 7970128,
    '0xfa67f97eeee6de55d179ecabbfe701f27d9a1ed9': 7941368,
    '0x48852322a185dc5fc733ff8c8d7c6dcbd2b3b2a2': 7936185,
    '0xf4d0f71698f58f221911515781b05e808a8635cb': 7912600,
    '0x9dceb1d92f7e0361d0766f3d98482424df857654': 7886546,
    '0xb77f1425ec3a7c78b1a1e892f72332c8b5e8ffcb': 7797976,
    '0xb9ff4da0954b300542e722097671ead8cf337c17': 7795721,
    '0xb19395702460261e51edf7a7b130109c64f13af9': 7791094,
    '0x6e113ecb9ff2d271140f124c2cc5b5e4b5700c9f': 7774682,
    '0x7baf1763ce5d0da8c9d85927f08a8be9c481ce50': 7763795,
    '0x2b8d6c9c62bfc1bed84724165d3000e61d332cab': 7740949,
    '0x8a06ff2748edcba3fb4e44a6bfda4e46769e557b': 7734914,
    '0x3eba95f5493349bbe0cad33eaae05dc6a7e26b90': 7712180,
    '0x593edbd14a5b7eec828336accca9c16cc12f04be': 7707414,
    '0xD714738837944C3c592477249E8edB724A76e068': 7683439,
    '0x8ea9f2482b2f7b12744a831f81f8d08714adc093': 7595034,
    '0x8e8125f871eb5ba9d55361365f5391ab437f9acc': 7592584,
    '0x0e09205e993f78cd5b3a5df355ae98ee7d0b5834': 7569884,
    '0xf9f00d41b1f4b3c531ff750a9b986c1a530f33d9': 7563950,
    '0x4Af531EcD50167a9402Ce921ee6436dd4cFC04FD': 7537787,
    '0x9b4bac2d8f69853aa29cb45478c77fc54532ac22': 7504587,
    '0x20ee70a07ae1b475cb150dec27930d97915726ea': 7504711,
    '0x017556dffb8c6a52fd7f4788adf6fb339309c81b': 7483047,
    '0xdaa711ecf2ac0bff5c82fceeae96d0008791cc49': 7478312,
    '0x74af842ecd0b6588add455a47aa21ed9ba794108': 7398899,
    '0x42d41749d6e9a1c5b47e27f690d4531f181b2159': 7393436,
    '0xbebd44824631b55991fa5f2bf5c7a4ec96ff805b': 7364604,
    '0x55131f330c886e3f0cae389cedb23766ac9aa3ed': 7364573,
    '0x01453a74a94687fa3f99b80762435855a13664f4': 7364532,
    '0x0032ceb978fe5fc8a5d5d6f5adfc005e76397e29': 7336569,
    '0x439b46d467402cebc1a2fa05038b5b696b1f4417': 7315245,
    '0x377ae5f933aa4cfa41fa03e2cae8a2befccf53b2': 7310911,
    '0xce3ebac3f549ebf1a174a6ac3b390c179422b5f6': 7288415,
    '0xd26dec254c699935c286cd90e9841dcabf1af72d': 7202120,
    '0x93e2867d9b74341c2d19101b7fbb81d6063cca4d': 7202111,
    '0x3b644e44033cff70bd6b771904225f3dd69dfb6d': 7202129,
    '0x0a687d7b951348d681f7ed5eea84c0ba7b9566dc': 7202095,
    '0x417df1c0e6a498eb1f2247f99032a01d4fafe922': 7174742,
    '0xdc8943d806f9dd64312d155284abf780455fd345': 7174739,
    '0xa90a894e5bc20ab2be46c7e033a38f8b8eaa771a': 7155121,
    '0x34ac807e34e534fe426da1e11f816422774aae1c': 7155211,
    '0x31fa2f516b77c4273168b284ac6d9def5aa6dafb': 7155474,
    '0x7112f8988f075c7784666ab071927ae4109a8076': 7155441,
    '0x126dfbcef85c5bf335f8be99ca4006037f417892': 7155150,
    '0x4f0ad2332b1f9983e8f63cbee617523bb7de5031': 7155480,
    '0x9483ca44324de06802576866b9d296f7614f45ac': 7155292,
    '0x72ceec6e2a142678e703ab0710de78bc819f4ce0': 7154396,
    '0x1c6ed21d3313822ae73ed0d94811ffbbe543f341': 7155235,
    '0x1ac0d0333640f57327c83053c581340ebc829e30': 7155279,
    '0xc707e5589aeb1dc117b0bb5a3622362f1812d4fc': 7155305,
    '0x22106cdcf9787969e1672d8e6a9c03a889cda9c5': 7155338,
    '0x999b86e8bba3d4f05afb8155963999db70afa97f': 7155544,
    '0xAF3EfE5fCEeBc603Eada6A2b0172be11f7405102': 6919515,
    '0xf73fdeb26a8c7a4abf3809d3db11a06ba5c13d0e': 6917653,
    '0xaac7171afc93f4b75e1268d208040b152ac65e32': 6788373,
    '0x2c6017269b4324d016ca5d8e3267368652c18905': 6755563,
    '0x675434c68f2672c983e36cf10ed13a4014720b79': 6727114,
    '0x05d6c2d1d687eacfb5e6440d5a3511e91f2201a8': 6725610,
    '0xd623a32da4a632ce01766c317d07cb2cad56949b': 6704396,
    '0xdf75f38dbc98f9f26377414e567abcb8d57cca33': 6701895,
    '0xce64a930884b2c68cd93fc1c7c7cdc221d427692': 6704147,
    '0xc1E70edd0141c454b834Deac7ddDeA413424aEf9': 6610197,
    '0x189d8228CdfDc404Bd9e5bD65ff958cb5fd8855c': 6591299,
    '0x0196c582216e2463f052E2B07Ef8667Bec9Fb17a': 6581224,
    '0x8f84106286c9c8A42bc3555C835E6e2090684ab7': 6559387,
    '0xa8d32b31ECB5142f067548Bf0424389eE98FaF26': 6525159,
    '0xC59aa49aE508050c2dF653E77bE13822fFf02E9A': 6521430,
    '0x14AeA62384789EDA98f444cCb970F6730877d3F9': 6504137,
    '0xebb87dF24D65977cbe62538E4B3cFBD5d0308642': 6470268,
    '0x40918EF8efFF4aA061656013a81E0e5A8A702eA7': 6418014,
    '0x44eC1B26035865D9A7C130fD872670CD7Ebac2bC': 6403550,
    '0x1329ad151dE6C441184E32E108401126AE850937': 6376967,
    '0x9bbDc92474a7e7321B78dcDA5EF35f4981438760': 6348645,
    '0x46530d79b238f809e80313e73715b160c66677aF': 6331114,
    '0x47fD853D5baD391899172892F91FAa6d0cd8A2Aa': 6306408,
    '0xe25aB6F05BBF6C1be953BF2d7df15B3e01b8e5a5': 6305969,
    '0xEB8Fd597921E3Dd37B0F103a2625F855e2C9b9B5': 6290206,
    '0xABFd8d1942628124aB971937154f826Bce86DcbC': 6212173,
    '0x526d3c204255f807C95a99b69596f2f9f72345e5': 6157690,
    '0xAa2082BeE04fc518300ec673F9497ffa6F669dB8': 6157299,
    '0x9096625Bc0d36F5EDa6d44e511641667d89C28f4': 6124392,
    '0x78BD4dB48F8983c3C36C8EAFbEF38f6aC7B55285': 6101629,
    '0x35418e14F5aA615C4f020eFBa6e01C5DbF15AdD2': 6013791,
    '0x3c7cC49a35942fbD3C2ad428a6c22490cd709d03': 5976512,
    '0xF795739737ABcFE0273f4Dced076460fdD024Dd9': 5960102,
    '0x06FF8960F7F4aE572A3f57FAe77B2882BE94Bf90': 5946614,
    '0xe4dD0C50fb314A8B2e84D211546F5B57eDd7c2b9': 5919331,
    '0xb627A7e33Db571bE792B0b69c5C2f5a8160d5500': 5919756,
    '0xadBfFA25594AF8Bc421ecaDF54D057236a99781e': 5919806,
    '0x3e31488f08EBcE6F2D8a2AA512aeFa49a3C7dFa7': 5897526,
    '0x453a75908fb5a36d482d5f8fe88eca836f32ead5': 5890495,
    '0x509C99D73FB54b2c20689708b3F824147292D38e': 5809987,
    '0xF1bd5673Ea4a1C415ec84fa3E402F2F7788E7717': 5756176,
    '0xB4C68A1C565298834360BbFF1652284275120D47': 5729929,
    '0x153e62257F1AAe05d5d253a670Ca7585c8D3F94F': 5674205,
    '0xF682D186168b4114ffDbF1291F19429310727151': 5702890,
    '0xaDdAE5f4dB84847ac9d947AED1304A8e7D19f7cA': 5674130,
    '0x4C32048628D0d32d4D6c52662FB4A92747782B56': 5673909,
    '0x47642101e8D8578C42765d7AbcFd0bA31868c523': 5617915,
    '0x07F8217c68ed9b838b0b8B58C19c79bACE746e9A': 5592245,
    '0x580DC9bB9260A922E3A4355b9119dB990F09410d': 5560943,
    '0x6f0037d158eD1AeE395e1c12d21aE8583842F472': 5492608,
    '0x423382f989C6C289c8D441000e1045e231bd7d90': 5518237,
    '0x0A595623b58dFDe6eB468b613C11A7A8E84F09b9': 5497431,
    '0x9E6dA246d369a41DC44673ce658966cAf487f7b2': 5492640,
    '0x2C0f449387b15793B9da27c2d945dBed83ab1B07': 5416686,
    '0x0c3D6892aa3b23811Af3bd1bbeA8b0740E8e4528': 5405432,
    '0x75C91844c5383A68b7d3A427A44C32E3ba66Fe45': 5405196,
    '0xC58954199E268505fa3D3Cb0A00b7207af8C2D1d': 5376195,
    '0xA5137e08C48167E363Be8Ec42A68f4F54330964E': 5344826,
    '0x6F31B87f51654424Ce57E9F8243E27ed13846CDB': 5297712,
    '0xCE54BA909d23B9d4BE0Ff0d84e5aE83F0ADD8D9a': 5287795,
    '0x3e677dC00668d69c2A7724b9AFA7363e8A56994e': 5205303,
    '0x5Ac8406498dC1921735d559CeC271bEd23B294A7': 5177787,
    '0xb69b6e390cba1F68442A886bC89E955048DAe7E3': 5161578,
    '0xae3001ddb18A6A57BEC2C19D71680437CA87bA1D': 5074913,
    '0x02aa767e855b8e80506fb47176202aA58A95315a': 4933042,
    '0x1c736F4FB20C7742Ee83a4099fE92abA61dFca41': 4920677,
    '0x02861B607a5E87daf3FD6ec19DFB715F1b371379': 4878111,
    '0x73e4E8d010289267dEe3d1Fc48974B60363963CE': 4782073,
    '0xE0565fBb109A3f3f8097D8A9D931277bfd795072': 4747958,
    '0xc3693e3cbc3514d5d07EA5b27A721F184F617900': 4717966,
    '0x2B02d43967765b18E31a9621da640588f3550EFD': 4693262,
    '0x212bb602418C399c29D52C55100fD6bBa12bea05': 4615412,
    '0x04aE8ca68A116278026fB721c06dCe709eD7013C': 4583358,
    '0x1714bAAE9DD4738CDEA07756427FA8d4F08D9479': 4557534,
    '0xcCD0b93cC6ce3dC6dFaA9DB68f70e5C8455aC5bd': 4522354,
    '0x9cB24e9460351bC51d4066BC6AEd1F3809b02B78': 4464382,
    '0x2dcf4cDFf4Dd954683Fe0a6123077f8a025b66cF': 4328377,
    '0x6EFa207ACdE6e1caB77c1322CbdE9628929ba88F': 4272350,
    '0xD0b738eC507571176D40f28bd56a0120E375f73a': 4154100,
    '0xf7a31366732F08E8e6B88519dC3E827e04616Fc9': 3986450,
    '0x9F23658D5f4CEd69282395089B0f8E4dB85C6e79': 3942408,
    '0xB6fd2724cc9c90DD31DA35DbDf0300009dceF97d': 3926970,
    '0x108BFE84Ca8BCe0741998cb0F60d313823cEC143': 3775547,
    '0x4A26b082B432B060B1b00A84eE4E823F04a6f69a': 3692047,
    '0x3cc08B7C6A31739CfEd9d8d38b484FDb245C79c8': 3666642,
    '0xd18E1AEb349ef0a6727eCe54597D98D263e05CAB': 3542873,
    '0x68C7d180bD8F7086D91E65A422c59514e4aFD638': 3542859,
    '0xbE65d7e42E05aD2c4ad28769dc9c5b4b6EAff2C7': 3542809,
    '0x1500fa1afbfe4f4277ed0345cdf12b2c9ca7e139': 3498358,
    '0x624ef5C2C6080Af188AF96ee5B3160Bb28bb3E02': 3337878,
    '0x0554a5D083Abf2f056ae3F6029e1714B9A655174': 3309748,
    '0x543467B17cA5De50c8BF7285107A36785Ab57E56': 3285707,
    '0x65aFEAFaec49F23159e897EFBDCe19D94A86A1B6': 3285727,
    '0x1AD34D8d4D79ddE88c9B6b8490F8fC67831f2CAe': 2711886,
    '0x555Ea72d7347E82C614C16f005fA91cAf06DCB5a': 2600409,
    '0x326D754c64329aD7cb35744770D56D0E1f3B3124': 2590161,
    '0x42Afc29b2dEa792974d1e9420696870f1Ca6d18b': 2402640,
    '0xBb2B66a2c7C2fFFB06EA60BeaD69741b3f5BF831': 2387608,
    '0xFb1088Dae0f03C5123587d2babb3F307831E6367': 2337016,
    '0x9c4EBADa591FFeC4124A7785CAbCfb7068fED2fb': 2128860,
    '0x90F995b9d46b32c4a1908A8c6D0122e392B3Be97': 1991882,
    '0xdc8c45b7F3747Ca9CaAEB3fa5e0b5FCE9430646b': 1985544,
    '0xFF02241a2A1d2a7088A344309400E9fE74772815': 1937661,
    '0xDc938BA1967b06d666dA79A7B1E31a8697D1565E': 1937785,
    '0x07a0A5B67136d40F4d7d95Bc8e0583bafD7A81b9': 1937819,
    '0x21A9A53936E812Da06B7623802DEc9A1f94ED23a': 1913459,
    '0xe7f9A439Aa7292719aC817798DDd1c4D35934aAF': 1913419,
    '0xcec2671C81a0Ecf7F8Ee796EFa6DBDc5Cb062693': 1913395
};
/* harmony default export */ const constants_poolsDeployedBlockNumber = (poolsDeployedBlockNumber);

;// CONCATENATED MODULE: ./src/utils/calls/pools.ts
/* eslint-disable import/prefer-default-export */ 






/**
 * Returns the total number of pools that were active at a given block
 */ const getActivePools = async (block)=>{
    const eligiblePools = pools/* default.filter */.Z.filter((pool)=>pool.sousId !== 0
    ).filter((pool)=>pool.isFinished === false || pool.isFinished === undefined
    ).filter((pool)=>{
        const { contractAddress , deployedBlockNumber  } = pool;
        const address = (0,addressHelpers/* getAddress */.Kn)(contractAddress);
        return deployedBlockNumber && deployedBlockNumber < block || constants_poolsDeployedBlockNumber[address] < block;
    });
    const blockNumber = block || await providers/* simpleRpcProvider.getBlockNumber */.J.getBlockNumber();
    const startBlockCalls = eligiblePools.map(({ contractAddress  })=>({
            address: (0,addressHelpers/* getAddress */.Kn)(contractAddress),
            name: 'startBlock'
        })
    );
    const endBlockCalls = eligiblePools.map(({ contractAddress  })=>({
            address: (0,addressHelpers/* getAddress */.Kn)(contractAddress),
            name: 'bonusEndBlock'
        })
    );
    const startBlocks = await (0,utils_multicall/* default */.Z)(abi_sousChefV2, startBlockCalls);
    const endBlocks = await (0,utils_multicall/* default */.Z)(abi_sousChefV2, endBlockCalls);
    return eligiblePools.reduce((accum, poolCheck, index)=>{
        const startBlock = startBlocks[index] ? new (external_bignumber_js_default())(startBlocks[index]) : null;
        const endBlock = endBlocks[index] ? new (external_bignumber_js_default())(endBlocks[index]) : null;
        if (!startBlock || !endBlock) {
            return accum;
        }
        if (startBlock.gte(blockNumber) || endBlock.lte(blockNumber)) {
            return accum;
        }
        return [
            ...accum,
            poolCheck
        ];
    }, []);
};

// EXTERNAL MODULE: external "@snapshot-labs/snapshot.js"
var snapshot_js_ = __webpack_require__(7248);
var snapshot_js_default = /*#__PURE__*/__webpack_require__.n(snapshot_js_);
;// CONCATENATED MODULE: ./src/config/constants/snapshot.ts


const cakeLpAddress = '0x0eD7e52944161450477ee417DE9Cd3a859b14fD0';
const CakeBalanceStrategy = {
    name: 'erc20-balance-of',
    params: {
        address: constants_tokens/* default.cake.address */.ZP.cake.address,
        decimals: 0,
        symbol: constants_tokens/* default.cake.symbol */.ZP.cake.symbol
    }
};
const CakeVaultSharesStrategy = {
    name: 'contract-call',
    params: {
        address: (0,addressHelpers/* getCakeVaultAddress */.O9)(),
        decimals: 0,
        output: 'shares',
        methodABI: {
            inputs: [
                {
                    internalType: 'address',
                    name: '',
                    type: 'address'
                }, 
            ],
            name: 'userInfo',
            outputs: [
                {
                    internalType: 'uint256',
                    name: 'shares',
                    type: 'uint256'
                },
                {
                    internalType: 'uint256',
                    name: 'lastDepositedTime',
                    type: 'uint256'
                },
                {
                    internalType: 'uint256',
                    name: 'cakeAtLastUserAction',
                    type: 'uint256'
                },
                {
                    internalType: 'uint256',
                    name: 'lastUserActionTime',
                    type: 'uint256'
                }, 
            ],
            stateMutability: 'view',
            type: 'function'
        }
    }
};
const CakeVaultPricePerFullShareStrategy = {
    name: 'contract-call',
    params: {
        address: (0,addressHelpers/* getCakeVaultAddress */.O9)(),
        decimals: 0,
        args: [],
        methodABI: {
            inputs: [],
            name: 'getPricePerFullShare',
            outputs: [
                {
                    internalType: 'uint256',
                    name: '',
                    type: 'uint256'
                }, 
            ],
            stateMutability: 'view',
            type: 'function'
        }
    }
};
const IFOPoolSharesStrategy = {
    name: 'contract-call',
    params: {
        address: (0,addressHelpers/* getIfoPoolAddress */.rp)(),
        decimals: 0,
        output: 'shares',
        methodABI: {
            inputs: [
                {
                    internalType: 'address',
                    name: '',
                    type: 'address'
                }, 
            ],
            name: 'userInfo',
            outputs: [
                {
                    internalType: 'uint256',
                    name: 'shares',
                    type: 'uint256'
                },
                {
                    internalType: 'uint256',
                    name: 'lastDepositedTime',
                    type: 'uint256'
                },
                {
                    internalType: 'uint256',
                    name: 'cakeAtLastUserAction',
                    type: 'uint256'
                },
                {
                    internalType: 'uint256',
                    name: 'lastUserActionTime',
                    type: 'uint256'
                }, 
            ],
            stateMutability: 'view',
            type: 'function'
        }
    }
};
const IFOPoolPricePerFullShareStrategy = {
    name: 'contract-call',
    params: {
        address: (0,addressHelpers/* getIfoPoolAddress */.rp)(),
        decimals: 0,
        args: [],
        methodABI: {
            inputs: [],
            name: 'getPricePerFullShare',
            outputs: [
                {
                    internalType: 'uint256',
                    name: '',
                    type: 'uint256'
                }, 
            ],
            stateMutability: 'view',
            type: 'function'
        }
    }
};
const UserStakeInCakePoolStrategy = {
    name: 'contract-call',
    params: {
        address: (0,addressHelpers/* getMasterChefAddress */.Oc)(),
        decimals: 0,
        args: [
            0,
            '%{address}'
        ],
        output: 'amount',
        methodABI: {
            inputs: [
                {
                    internalType: 'uint256',
                    name: '',
                    type: 'uint256'
                },
                {
                    internalType: 'address',
                    name: '',
                    type: 'address'
                }, 
            ],
            name: 'userInfo',
            outputs: [
                {
                    internalType: 'uint256',
                    name: 'amount',
                    type: 'uint256'
                },
                {
                    internalType: 'uint256',
                    name: 'rewardDebt',
                    type: 'uint256'
                }, 
            ],
            stateMutability: 'view',
            type: 'function'
        }
    }
};
const CakeBnbLpTotalSupplyStrategy = {
    name: 'contract-call',
    params: {
        address: cakeLpAddress,
        decimals: 0,
        args: [],
        methodABI: {
            constant: true,
            inputs: [],
            name: 'totalSupply',
            outputs: [
                {
                    internalType: 'uint256',
                    name: '',
                    type: 'uint256'
                }, 
            ],
            payable: false,
            stateMutability: 'view',
            type: 'function'
        }
    }
};
const CakeBnbLpReserve0Strategy = {
    name: 'contract-call',
    params: {
        address: cakeLpAddress,
        decimals: 0,
        args: [],
        output: '_reserve0',
        methodABI: {
            constant: true,
            inputs: [],
            name: 'getReserves',
            outputs: [
                {
                    internalType: 'uint112',
                    name: '_reserve0',
                    type: 'uint112'
                },
                {
                    internalType: 'uint112',
                    name: '_reserve1',
                    type: 'uint112'
                },
                {
                    internalType: 'uint32',
                    name: '_blockTimestampLast',
                    type: 'uint32'
                }, 
            ],
            payable: false,
            stateMutability: 'view',
            type: 'function'
        }
    }
};
const CakeBnbLpCakeBnbBalanceStrategy = {
    name: 'contract-call',
    params: {
        address: (0,addressHelpers/* getMasterChefAddress */.Oc)(),
        decimals: 0,
        args: [
            251,
            '%{address}'
        ],
        output: 'amount',
        methodABI: {
            inputs: [
                {
                    internalType: 'uint256',
                    name: '',
                    type: 'uint256'
                },
                {
                    internalType: 'address',
                    name: '',
                    type: 'address'
                }, 
            ],
            name: 'userInfo',
            outputs: [
                {
                    internalType: 'uint256',
                    name: 'amount',
                    type: 'uint256'
                },
                {
                    internalType: 'uint256',
                    name: 'rewardDebt',
                    type: 'uint256'
                }, 
            ],
            stateMutability: 'view',
            type: 'function'
        }
    }
};
function createPoolStrategy(poolAddress) {
    return {
        name: 'contract-call',
        params: {
            address: poolAddress,
            decimals: 0,
            output: 'amount',
            methodABI: {
                inputs: [
                    {
                        internalType: 'address',
                        name: '',
                        type: 'address'
                    }, 
                ],
                name: 'userInfo',
                outputs: [
                    {
                        internalType: 'uint256',
                        name: 'amount',
                        type: 'uint256'
                    },
                    {
                        internalType: 'uint256',
                        name: 'rewardDebt',
                        type: 'uint256'
                    }, 
                ],
                stateMutability: 'view',
                type: 'function'
            }
        }
    };
}

const snapshotStrategies = [
    CakeBalanceStrategy,
    CakeVaultSharesStrategy,
    CakeVaultPricePerFullShareStrategy,
    IFOPoolSharesStrategy,
    IFOPoolPricePerFullShareStrategy,
    UserStakeInCakePoolStrategy,
    CakeBnbLpTotalSupplyStrategy,
    CakeBnbLpReserve0Strategy,
    CakeBnbLpCakeBnbBalanceStrategy, 
];

;// CONCATENATED MODULE: ./src/views/Voting/config.ts
const PROPOSALS_TO_SHOW = 10;
const config_ADMINS = [
    '0x842B508681eE336E74600974B4623B709477d29D',
    '0x977e0c1005dff8749f8cac22f4df0bd5f013d1a7',
    '0x6eaf1b33b8672c5dc40ab8f4ba3a0111723126c7'
].map((address)=>address.toLowerCase()
);
const IPFS_GATEWAY = 'https://gateway.ipfs.io/ipfs';
const config_SNAPSHOT_VERSION = '0.1.3';
const config_PANCAKE_SPACE = 'cake.eth';
const VOTE_THRESHOLD = 10;

;// CONCATENATED MODULE: ./src/views/Voting/helpers.ts







const isCoreProposal = (proposal)=>{
    return ADMINS.includes(proposal.author.toLowerCase());
};
const filterProposalsByType = (proposals, proposalType)=>{
    switch(proposalType){
        case ProposalType.COMMUNITY:
            return proposals.filter((proposal)=>!isCoreProposal(proposal)
            );
        case ProposalType.CORE:
            return proposals.filter((proposal)=>isCoreProposal(proposal)
            );
        case ProposalType.ALL:
        default:
            return proposals;
    }
};
const filterProposalsByState = (proposals, state)=>{
    return proposals.filter((proposal)=>proposal.state === state
    );
};
/**
 * Generates metadata required by snapshot to validate payload
 */ const generateMetaData = ()=>{
    return {
        plugins: {},
        network: 56,
        strategies: [
            {
                name: 'cake',
                params: {
                    symbol: 'CAKE',
                    address: tokens.cake.address,
                    decimals: 18
                }
            }
        ]
    };
};
/**
 * Returns data that is required on all snapshot payloads
 */ const generatePayloadData = ()=>{
    return {
        version: SNAPSHOT_VERSION,
        timestamp: (Date.now() / 1000).toFixed(),
        space: PANCAKE_SPACE
    };
};
/**
 * General function to send commands to the snapshot api
 */ const sendSnapshotData = async (message)=>{
    const response = await fetch(SNAPSHOT_HUB_API, {
        method: 'post',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(message)
    });
    if (!response.ok) {
        const error = await response.json();
        throw new Error(error?.error_description);
    }
    const data = await response.json();
    return data;
};
const getVotingPower = async (account, poolAddresses, blockNumber)=>{
    const votingPowerList = await getVotingPowerList([
        account
    ], poolAddresses, blockNumber);
    return votingPowerList[0];
};
const calculateVoteResults = (votes)=>{
    return votes.reduce((accum, vote)=>{
        const choiceText = vote.proposal.choices[vote.choice - 1];
        return {
            ...accum,
            [choiceText]: accum[choiceText] ? [
                ...accum[choiceText],
                vote
            ] : [
                vote
            ]
        };
    }, {});
};
const getTotalFromVotes = (votes)=>{
    return votes.reduce((accum, vote)=>{
        let power = parseFloat(vote.metadata?.votingPower);
        if (!power) {
            power = 0;
        }
        return accum + power;
    }, 0);
};
const TEN_POW_18 = new (external_bignumber_js_default())(10).pow(18);
function calculateVotingPower(scoresList, voters, scoresListIndex) {
    let [cakeBalances, cakeVaultShares, cakeVaultPricePerFullShares, ifoPoolShares, ifoPoolPricePerFullShares, userStakeInCakePools, cakeBnbLpTotalSupplies, cakeBnbLpReserve0s, cakeBnbLpCakeBnbBalances, ] = new Array(9);
    const defaultScore = {};
    for(let i1 = 0; i1 < voters.length; i1++){
        defaultScore[voters[i1]] = 0;
    }
    cakeBalances = scoresListIndex.cakeBalances > -1 ? scoresList[scoresListIndex.cakeBalances] : defaultScore;
    cakeVaultShares = scoresListIndex.cakeVaultShares > -1 ? scoresList[scoresListIndex.cakeVaultShares] : defaultScore;
    cakeVaultPricePerFullShares = scoresListIndex.cakeVaultPricePerFullShares > -1 ? scoresList[scoresListIndex.cakeVaultPricePerFullShares] : defaultScore;
    ifoPoolShares = scoresListIndex.ifoPoolShares > -1 ? scoresList[scoresListIndex.ifoPoolShares] : defaultScore;
    ifoPoolPricePerFullShares = scoresListIndex.ifoPoolPricePerFullShares > -1 ? scoresList[scoresListIndex.ifoPoolPricePerFullShares] : defaultScore;
    userStakeInCakePools = scoresListIndex.userStakeInCakePools > -1 ? scoresList[scoresListIndex.userStakeInCakePools] : defaultScore;
    cakeBnbLpTotalSupplies = scoresListIndex.cakeBnbLpTotalSupplies > -1 ? scoresList[scoresListIndex.cakeBnbLpTotalSupplies] : defaultScore;
    cakeBnbLpReserve0s = scoresListIndex.cakeBnbLpReserve0s > -1 ? scoresList[scoresListIndex.cakeBnbLpReserve0s] : defaultScore;
    cakeBnbLpCakeBnbBalances = scoresListIndex.cakeBnbLpCakeBnbBalances > -1 ? scoresList[scoresListIndex.cakeBnbLpCakeBnbBalances] : defaultScore;
    const result = voters.map((address)=>{
        const cakeBalance = new (external_bignumber_js_default())(cakeBalances[address]);
        // calculate cakeVaultBalance
        const sharePrice = new (external_bignumber_js_default())(cakeVaultPricePerFullShares[address]).div(TEN_POW_18);
        const cakeVaultBalance = new (external_bignumber_js_default())(cakeVaultShares[address]).times(sharePrice);
        // calculate ifoPoolBalance
        const IFOPoolsharePrice = new (external_bignumber_js_default())(ifoPoolPricePerFullShares[address]).div(TEN_POW_18);
        const IFOPoolBalance = new (external_bignumber_js_default())(ifoPoolShares[address]).times(IFOPoolsharePrice);
        const cakePoolBalance = new (external_bignumber_js_default())(userStakeInCakePools[address]);
        // calculate cakeBnbLpBalance
        const totalSupplyLP = new (external_bignumber_js_default())(cakeBnbLpTotalSupplies[address]);
        const cakeBnbLpReserve0 = new (external_bignumber_js_default())(cakeBnbLpReserve0s[address]);
        const cakeBnbLpCakeBnbBalance = new (external_bignumber_js_default())(cakeBnbLpCakeBnbBalances[address]);
        const cakeBnbLpBalance = cakeBnbLpCakeBnbBalance.times(cakeBnbLpReserve0).div(totalSupplyLP);
        // calculate poolsBalance
        const poolStartIndex = scoresListIndex.poolStart;
        let poolsBalance = new (external_bignumber_js_default())(0);
        for(let i = poolStartIndex; i < scoresList.length; i++){
            const currentPoolBalance = new (external_bignumber_js_default())(scoresList[i][address]);
            poolsBalance = poolsBalance.plus(currentPoolBalance);
        }
        const total = cakeBalance.plus(cakeVaultBalance).plus(cakePoolBalance).plus(IFOPoolBalance).plus(cakeBnbLpBalance).plus(poolsBalance).div(TEN_POW_18).toFixed(18);
        return {
            cakeBalance: cakeBalance.div(TEN_POW_18).toFixed(18),
            cakeVaultBalance: cakeVaultBalance.div(TEN_POW_18).toFixed(18),
            IFOPoolBalance: IFOPoolBalance.div(TEN_POW_18).toFixed(18),
            cakePoolBalance: cakePoolBalance.div(TEN_POW_18).toFixed(18),
            cakeBnbLpBalance: cakeBnbLpBalance.div(TEN_POW_18).toFixed(18),
            poolsBalance: poolsBalance.div(TEN_POW_18).toFixed(18),
            total,
            voter: address
        };
    });
    return result;
}
const ContractDeployedNumber = {
    Cake: 693963,
    CakeVault: 6975840,
    IFOPool: 13463954,
    MasterChef: 699498,
    CakeLp: 6810706
};
function verifyDefaultContract(blockNumber) {
    return {
        Cake: ContractDeployedNumber.Cake < blockNumber,
        CakeVault: ContractDeployedNumber.CakeVault < blockNumber,
        IFOPool: ContractDeployedNumber.IFOPool < blockNumber,
        MasterChef: ContractDeployedNumber.MasterChef < blockNumber,
        CakeLp: ContractDeployedNumber.CakeLp < blockNumber
    };
}
async function getVotingPowerList(voters, poolAddresses, blockNumber) {
    const poolsStrategyList = poolAddresses.map((address)=>createPoolStrategy(address)
    );
    const contractsValid = verifyDefaultContract(blockNumber);
    const scoresListIndex = {
        cakeBalances: -1,
        cakeVaultShares: -1,
        cakeVaultPricePerFullShares: -1,
        ifoPoolShares: -1,
        ifoPoolPricePerFullShares: -1,
        userStakeInCakePools: -1,
        cakeBnbLpTotalSupplies: -1,
        cakeBnbLpReserve0s: -1,
        cakeBnbLpCakeBnbBalances: -1,
        poolStart: 0
    };
    const defaultStrategy = [];
    let indexCounter = 0;
    if (contractsValid.Cake) {
        defaultStrategy.push(snapshotStrategies[0]);
        scoresListIndex.cakeBalances = indexCounter++;
    }
    if (contractsValid.CakeVault) {
        defaultStrategy.push(snapshotStrategies[1]);
        scoresListIndex.cakeVaultShares = indexCounter++;
        defaultStrategy.push(snapshotStrategies[2]);
        scoresListIndex.cakeVaultPricePerFullShares = indexCounter++;
    }
    if (contractsValid.IFOPool) {
        defaultStrategy.push(snapshotStrategies[3]);
        scoresListIndex.ifoPoolShares = indexCounter++;
        defaultStrategy.push(snapshotStrategies[4]);
        scoresListIndex.ifoPoolPricePerFullShares = indexCounter++;
    }
    if (contractsValid.MasterChef) {
        defaultStrategy.push(snapshotStrategies[5]);
        scoresListIndex.userStakeInCakePools = indexCounter++;
    }
    if (contractsValid.CakeLp) {
        defaultStrategy.push(snapshotStrategies[6]);
        scoresListIndex.cakeBnbLpTotalSupplies = indexCounter++;
        defaultStrategy.push(snapshotStrategies[7]);
        scoresListIndex.cakeBnbLpReserve0s = indexCounter++;
        defaultStrategy.push(snapshotStrategies[8]);
        scoresListIndex.cakeBnbLpCakeBnbBalances = indexCounter++;
    }
    scoresListIndex.poolStart = indexCounter;
    const strategies = [
        ...defaultStrategy,
        ...poolsStrategyList
    ];
    const network = '56';
    const strategyResponse = await snapshot_js_default().utils.getScores(config_PANCAKE_SPACE, strategies, network, voters, blockNumber);
    const votingPowerList = calculateVotingPower(strategyResponse, voters, scoresListIndex);
    return votingPowerList;
}

// EXTERNAL MODULE: external "lodash/flatten"
var flatten_ = __webpack_require__(8579);
var flatten_default = /*#__PURE__*/__webpack_require__.n(flatten_);
;// CONCATENATED MODULE: ./src/state/voting/helpers.ts









const getProposals = async (first = 5, skip = 0, state = state_types/* ProposalState.ACTIVE */.r7.ACTIVE)=>{
    const response = await external_graphql_request_default()(endpoints/* SNAPSHOT_API */.TY, external_graphql_request_.gql`
      query getProposals($first: Int!, $skip: Int!, $state: String!) {
        proposals(
          first: $first
          skip: $skip
          orderBy: "end"
          orderDirection: desc
          where: { space_in: "cake.eth", state: $state }
        ) {
          id
          title
          body
          choices
          start
          end
          snapshot
          state
          author
          space {
            id
            name
          }
        }
      }
    `, {
        first,
        skip,
        state
    });
    return response.proposals;
};
const getProposal = async (id)=>{
    const response = await external_graphql_request_default()(endpoints/* SNAPSHOT_API */.TY, external_graphql_request_.gql`
      query getProposal($id: String) {
        proposal(id: $id) {
          id
          title
          body
          choices
          start
          end
          snapshot
          state
          author
          space {
            id
            name
          }
        }
      }
    `, {
        id
    });
    return response.proposal;
};
const getVotes = async (first, skip, where)=>{
    const response = await external_graphql_request_default()(endpoints/* SNAPSHOT_API */.TY, external_graphql_request_.gql`
      query getVotes($first: Int, $skip: Int, $where: VoteWhere) {
        votes(first: $first, skip: $skip, where: $where) {
          id
          voter
          created
          choice
          space {
            id
            name
          }
          proposal {
            choices
          }
        }
      }
    `, {
        first,
        skip,
        where
    });
    return response.votes;
};
const NUMBER_OF_VOTERS_PER_SNAPSHOT_REQUEST = 250;
const getAllVotes = async (proposalId, block, votesPerChunk = 1000)=>{
    const eligiblePools = await getActivePools(block);
    const poolAddresses = eligiblePools.map(({ contractAddress  })=>(0,addressHelpers/* getAddress */.Kn)(contractAddress)
    );
    return new Promise((resolve, reject)=>{
        let votes = [];
        const fetchVoteChunk = async (newSkip)=>{
            try {
                const voteChunk = await getVotes(votesPerChunk, newSkip, {
                    proposal: proposalId
                });
                const voteChunkVoters = voteChunk.map((vote)=>{
                    return vote.voter;
                });
                const snapshotVotersChunk = chunk_default()(voteChunkVoters, NUMBER_OF_VOTERS_PER_SNAPSHOT_REQUEST);
                const votingPowers = await Promise.all(snapshotVotersChunk.map((votersChunk)=>getVotingPowerList(votersChunk, poolAddresses, block)
                ));
                const vpByVoter = keyBy_default()(flatten_default()(votingPowers), 'voter');
                const voteChunkWithVP = voteChunk.map((vote)=>{
                    return {
                        ...vote,
                        metadata: {
                            votingPower: vpByVoter[vote.voter]?.total
                        }
                    };
                });
                if (voteChunk.length === 0) {
                    resolve(votes);
                } else {
                    votes = [
                        ...votes,
                        ...voteChunkWithVP
                    ];
                    fetchVoteChunk(newSkip + votesPerChunk);
                }
            } catch (error) {
                reject(error);
            }
        };
        fetchVoteChunk(0);
    });
};

;// CONCATENATED MODULE: ./src/state/voting/index.tsx





const voting_initialState = {
    proposalLoadingStatus: types/* FetchStatus.Idle */.iF.Idle,
    proposals: {},
    voteLoadingStatus: types/* FetchStatus.Idle */.iF.Idle,
    votes: {}
};
// Thunks
const fetchProposals = (0,toolkit_.createAsyncThunk)('voting/fetchProposals', async ({ first , skip =0 , state =state_types/* ProposalState.ACTIVE */.r7.ACTIVE  })=>{
    const response = await getProposals(first, skip, state);
    return response;
});
const fetchProposal = (0,toolkit_.createAsyncThunk)('voting/fetchProposal', async (proposalId)=>{
    const response = await getProposal(proposalId);
    return response;
});
const fetchVotes = (0,toolkit_.createAsyncThunk)('voting/fetchVotes', async ({ proposalId , block  })=>{
    const response = await getAllVotes(proposalId, block);
    return {
        votes: response,
        proposalId
    };
});
const votingSlice = (0,toolkit_.createSlice)({
    name: 'voting',
    initialState: voting_initialState,
    reducers: {},
    extraReducers: (builder)=>{
        // Fetch Proposals
        builder.addCase(fetchProposals.pending, (state)=>{
            state.proposalLoadingStatus = types/* FetchStatus.Fetching */.iF.Fetching;
        });
        builder.addCase(fetchProposals.fulfilled, (state, action)=>{
            const proposals = action.payload.reduce((accum, proposal)=>{
                return {
                    ...accum,
                    [proposal.id]: proposal
                };
            }, {});
            state.proposals = merge_default()({}, state.proposals, proposals);
            state.proposalLoadingStatus = types/* FetchStatus.Fetched */.iF.Fetched;
        });
        // Fetch Proposal
        builder.addCase(fetchProposal.pending, (state)=>{
            state.proposalLoadingStatus = types/* FetchStatus.Fetching */.iF.Fetching;
        });
        builder.addCase(fetchProposal.fulfilled, (state, action)=>{
            state.proposals[action.payload.id] = action.payload;
            state.proposalLoadingStatus = types/* FetchStatus.Fetched */.iF.Fetched;
        });
        // Fetch Votes
        builder.addCase(fetchVotes.pending, (state)=>{
            state.voteLoadingStatus = types/* FetchStatus.Fetching */.iF.Fetching;
        });
        builder.addCase(fetchVotes.fulfilled, (state, action)=>{
            const { votes , proposalId  } = action.payload;
            state.votes = {
                ...state.votes,
                [proposalId]: votes
            };
            state.voteLoadingStatus = types/* FetchStatus.Fetched */.iF.Fetched;
        });
    }
});
/* harmony default export */ const voting = (votingSlice.reducer);

;// CONCATENATED MODULE: ./src/state/index.ts























const PERSISTED_KEYS = [
    'user',
    'transactions',
    'lists',
    'profile'
];
const persistConfig = {
    key: 'primary',
    whitelist: PERSISTED_KEYS,
    storage: (storage_default())
};
const persistedReducer = (0,external_redux_persist_.persistReducer)(persistConfig, (0,toolkit_.combineReducers)({
    block: block/* default */.ZP,
    farms: farms/* default */.ZP,
    pools: state_pools,
    predictions: state_predictions,
    profile: profile/* default */.ZP,
    teams: state_teams,
    voting: voting,
    lottery: lottery,
    info: info,
    nftMarket: nftMarket_reducer,
    // Exchange
    user: user_reducer,
    transactions: transactions_reducer,
    swap: swap_reducer,
    mint: mint_reducer,
    burn: reducer,
    multicall: multicall_reducer,
    lists: lists_reducer
}));
// eslint-disable-next-line import/no-mutable-exports
let store;
function makeStore(preloadedState = undefined) {
    return (0,toolkit_.configureStore)({
        reducer: persistedReducer,
        middleware: (getDefaultMiddleware)=>getDefaultMiddleware({
                thunk: true,
                serializableCheck: {
                    ignoredActions: [
                        external_redux_persist_.FLUSH,
                        external_redux_persist_.REHYDRATE,
                        external_redux_persist_.PAUSE,
                        external_redux_persist_.PERSIST,
                        external_redux_persist_.PURGE,
                        external_redux_persist_.REGISTER
                    ]
                }
            })
        ,
        devTools: "production" === 'development',
        preloadedState
    });
}
const initializeStore = (preloadedState = undefined)=>{
    let _store = store ?? makeStore(preloadedState);
    // After navigating to a page with an initial Redux state, merge that state
    // with the current state in the store, and create a new store
    if (preloadedState && store) {
        _store = makeStore({
            ...store.getState(),
            ...preloadedState
        });
        // Reset the current store
        store = undefined;
    }
    // For SSG and SSR always create a new store
    if (true) return _store;
    // Create the store once in the client
    if (!store) {
        store = _store;
        store.dispatch(updateVersion());
    }
    return _store;
};
store = initializeStore();
const useAppDispatch = ()=>(0,external_react_redux_.useDispatch)()
;
/* harmony default export */ const state = ((/* unused pure expression or super */ null && (store)));
const persistor = (0,external_redux_persist_.persistStore)(store);
function useStore(initialState) {
    return (0,external_react_.useMemo)(()=>initializeStore(initialState)
    , [
        initialState
    ]);
}


/***/ }),

/***/ 4522:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Uo": () => (/* binding */ updateProtocolData),
/* harmony export */   "bj": () => (/* binding */ updateProtocolChartData),
/* harmony export */   "oz": () => (/* binding */ updateProtocolTransactions),
/* harmony export */   "Bp": () => (/* binding */ updatePoolData),
/* harmony export */   "iF": () => (/* binding */ addPoolKeys),
/* harmony export */   "jw": () => (/* binding */ updatePoolChartData),
/* harmony export */   "oG": () => (/* binding */ updatePoolTransactions),
/* harmony export */   "I6": () => (/* binding */ updateTokenData),
/* harmony export */   "uP": () => (/* binding */ addTokenKeys),
/* harmony export */   "TV": () => (/* binding */ addTokenPoolAddresses),
/* harmony export */   "fo": () => (/* binding */ updateTokenChartData),
/* harmony export */   "Mw": () => (/* binding */ updateTokenTransactions),
/* harmony export */   "Db": () => (/* binding */ updateTokenPriceData)
/* harmony export */ });
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5184);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);

const updateProtocolData = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('info/protocol/updateProtocolData');
const updateProtocolChartData = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('info/protocol/updateProtocolChartData');
const updateProtocolTransactions = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('info/protocol/updateProtocolTransactions');
const updatePoolData = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('info/pools/updatePoolData');
const addPoolKeys = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('info/pools/addPoolKeys');
const updatePoolChartData = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('info/pools/updatePoolChartData');
const updatePoolTransactions = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('info/pools/updatePoolTransactions');
const updateTokenData = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('info/tokens/updateTokenData');
const addTokenKeys = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('info/tokens/addTokenKeys');
const addTokenPoolAddresses = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('info/tokens/addTokenPoolAddresses');
const updateTokenChartData = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('info/tokens/updateTokenChartData');
const updateTokenTransactions = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('info/tokens/updateTokenTransactions');
const updateTokenPriceData = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('info/tokens/updateTokenPriceData');


/***/ }),

/***/ 8412:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Dn": () => (/* binding */ fetchTokenList),
/* harmony export */   "$8": () => (/* binding */ addList),
/* harmony export */   "J_": () => (/* binding */ removeList),
/* harmony export */   "ic": () => (/* binding */ enableList),
/* harmony export */   "K$": () => (/* binding */ disableList),
/* harmony export */   "xJ": () => (/* binding */ acceptListUpdate)
/* harmony export */ });
/* unused harmony export rejectVersionUpdate */
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5184);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);

const fetchTokenList = {
    pending: (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('lists/fetchTokenList/pending'),
    fulfilled: (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('lists/fetchTokenList/fulfilled'),
    rejected: (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('lists/fetchTokenList/rejected')
};
// add and remove from list options
const addList = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('lists/addList');
const removeList = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('lists/removeList');
// select which lists to search across from loaded lists
const enableList = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('lists/enableList');
const disableList = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('lists/disableList');
// versioning
const acceptListUpdate = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('lists/acceptListUpdate');
const rejectVersionUpdate = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('lists/rejectVersionUpdate');


/***/ }),

/***/ 3884:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "kG": () => (/* binding */ toCallKey),
/* harmony export */   "gl": () => (/* binding */ parseCallKey),
/* harmony export */   "Dd": () => (/* binding */ addMulticallListeners),
/* harmony export */   "$x": () => (/* binding */ removeMulticallListeners),
/* harmony export */   "nu": () => (/* binding */ fetchingMulticallResults),
/* harmony export */   "wC": () => (/* binding */ errorFetchingMulticallResults),
/* harmony export */   "zT": () => (/* binding */ updateMulticallResults)
/* harmony export */ });
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5184);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);

const ADDRESS_REGEX = /^0x[a-fA-F0-9]{40}$/;
const LOWER_HEX_REGEX = /^0x[a-f0-9]*$/;
function toCallKey(call) {
    if (!ADDRESS_REGEX.test(call.address)) {
        throw new Error(`Invalid address: ${call.address}`);
    }
    if (!LOWER_HEX_REGEX.test(call.callData)) {
        throw new Error(`Invalid hex: ${call.callData}`);
    }
    return `${call.address}-${call.callData}`;
}
function parseCallKey(callKey) {
    const pcs = callKey.split('-');
    if (pcs.length !== 2) {
        throw new Error(`Invalid call key: ${callKey}`);
    }
    return {
        address: pcs[0],
        callData: pcs[1]
    };
}
const addMulticallListeners = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('multicall/addMulticallListeners');
const removeMulticallListeners = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('multicall/removeMulticallListeners');
const fetchingMulticallResults = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('multicall/fetchingMulticallResults');
const errorFetchingMulticallResults = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('multicall/errorFetchingMulticallResults');
const updateMulticallResults = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('multicall/updateMulticallResults');


/***/ }),

/***/ 1001:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DB": () => (/* binding */ NEVER_RELOAD),
/* harmony export */   "es": () => (/* binding */ useSingleContractMultipleData),
/* harmony export */   "_Y": () => (/* binding */ useMultipleContractSingleData),
/* harmony export */   "Wk": () => (/* binding */ useSingleCallResult)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6022);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var hooks_useActiveWeb3React__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4011);
/* harmony import */ var state_block_hooks__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7063);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3884);





function isMethodArg(x) {
    return [
        'string',
        'number'
    ].indexOf(typeof x) !== -1;
}
function isValidMethodArgs(x) {
    return x === undefined || Array.isArray(x) && x.every((xi)=>isMethodArg(xi) || Array.isArray(xi) && xi.every(isMethodArg)
    );
}
const INVALID_RESULT = {
    valid: false,
    blockNumber: undefined,
    data: undefined
};
// use this options object
const NEVER_RELOAD = {
    blocksPerFetch: Infinity
};
// the lowest level call for subscribing to contract data
function useCallsData(calls1, options) {
    const { chainId  } = (0,hooks_useActiveWeb3React__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z)();
    const callResults = (0,react_redux__WEBPACK_IMPORTED_MODULE_1__.useSelector)((state)=>state.multicall.callResults
    );
    const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_1__.useDispatch)();
    const serializedCallKeys = (0,react__WEBPACK_IMPORTED_MODULE_0__.useMemo)(()=>JSON.stringify(calls1?.filter((c)=>Boolean(c)
        )?.map(_actions__WEBPACK_IMPORTED_MODULE_4__/* .toCallKey */ .kG)?.sort() ?? [])
    , [
        calls1
    ]);
    // update listeners when there is an actual change that persists for at least 100ms
    (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(()=>{
        const callKeys = JSON.parse(serializedCallKeys);
        if (!chainId || callKeys.length === 0) return undefined;
        // eslint-disable-next-line @typescript-eslint/no-shadow
        const calls = callKeys.map((key)=>(0,_actions__WEBPACK_IMPORTED_MODULE_4__/* .parseCallKey */ .gl)(key)
        );
        dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_4__/* .addMulticallListeners */ .Dd)({
            chainId,
            calls,
            options
        }));
        return ()=>{
            dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_4__/* .removeMulticallListeners */ .$x)({
                chainId,
                calls,
                options
            }));
        };
    }, [
        chainId,
        dispatch,
        options,
        serializedCallKeys
    ]);
    return (0,react__WEBPACK_IMPORTED_MODULE_0__.useMemo)(()=>calls1.map((call)=>{
            if (!chainId || !call) return INVALID_RESULT;
            const result = callResults[chainId]?.[(0,_actions__WEBPACK_IMPORTED_MODULE_4__/* .toCallKey */ .kG)(call)];
            let data;
            if (result?.data && result?.data !== '0x') {
                // eslint-disable-next-line prefer-destructuring
                data = result.data;
            }
            return {
                valid: true,
                data,
                blockNumber: result?.blockNumber
            };
        })
    , [
        callResults,
        calls1,
        chainId
    ]);
}
const INVALID_CALL_STATE = {
    valid: false,
    result: undefined,
    loading: false,
    syncing: false,
    error: false
};
const LOADING_CALL_STATE = {
    valid: true,
    result: undefined,
    loading: true,
    syncing: true,
    error: false
};
function toCallState(callResult, contractInterface, fragment, latestBlockNumber) {
    if (!callResult) return INVALID_CALL_STATE;
    const { valid , data , blockNumber  } = callResult;
    if (!valid) return INVALID_CALL_STATE;
    if (valid && !blockNumber) return LOADING_CALL_STATE;
    if (!contractInterface || !fragment || !latestBlockNumber) return LOADING_CALL_STATE;
    const success = data && data.length > 2;
    const syncing = (blockNumber ?? 0) < latestBlockNumber;
    let result;
    if (success && data) {
        try {
            result = contractInterface.decodeFunctionResult(fragment, data);
        } catch (error) {
            console.debug('Result data parsing failed', fragment, data);
            return {
                valid: true,
                loading: false,
                error: true,
                syncing,
                result
            };
        }
    }
    return {
        valid: true,
        loading: false,
        syncing,
        result,
        error: !success
    };
}
function useSingleContractMultipleData(contract, methodName, callInputs, options) {
    const fragment = (0,react__WEBPACK_IMPORTED_MODULE_0__.useMemo)(()=>contract?.interface?.getFunction(methodName)
    , [
        contract,
        methodName
    ]);
    const calls = (0,react__WEBPACK_IMPORTED_MODULE_0__.useMemo)(()=>contract && fragment && callInputs && callInputs.length > 0 ? callInputs.map((inputs)=>{
            return {
                address: contract.address,
                callData: contract.interface.encodeFunctionData(fragment, inputs)
            };
        }) : []
    , [
        callInputs,
        contract,
        fragment
    ]);
    const results = useCallsData(calls, options);
    const currentBlock = (0,state_block_hooks__WEBPACK_IMPORTED_MODULE_3__/* .useCurrentBlock */ .je)();
    return (0,react__WEBPACK_IMPORTED_MODULE_0__.useMemo)(()=>{
        return results.map((result)=>toCallState(result, contract?.interface, fragment, currentBlock)
        );
    }, [
        fragment,
        contract,
        results,
        currentBlock
    ]);
}
function useMultipleContractSingleData(addresses, contractInterface, methodName, callInputs, options) {
    const fragment = (0,react__WEBPACK_IMPORTED_MODULE_0__.useMemo)(()=>contractInterface.getFunction(methodName)
    , [
        contractInterface,
        methodName
    ]);
    const callData = (0,react__WEBPACK_IMPORTED_MODULE_0__.useMemo)(()=>fragment && isValidMethodArgs(callInputs) ? contractInterface.encodeFunctionData(fragment, callInputs) : undefined
    , [
        callInputs,
        contractInterface,
        fragment
    ]);
    const calls = (0,react__WEBPACK_IMPORTED_MODULE_0__.useMemo)(()=>fragment && addresses && addresses.length > 0 && callData ? addresses.map((address)=>{
            return address && callData ? {
                address,
                callData
            } : undefined;
        }) : []
    , [
        addresses,
        callData,
        fragment
    ]);
    const results = useCallsData(calls, options);
    const currentBlock = (0,state_block_hooks__WEBPACK_IMPORTED_MODULE_3__/* .useCurrentBlock */ .je)();
    return (0,react__WEBPACK_IMPORTED_MODULE_0__.useMemo)(()=>{
        return results.map((result)=>toCallState(result, contractInterface, fragment, currentBlock)
        );
    }, [
        fragment,
        results,
        contractInterface,
        currentBlock
    ]);
}
function useSingleCallResult(contract, methodName, inputs, options) {
    const fragment = (0,react__WEBPACK_IMPORTED_MODULE_0__.useMemo)(()=>contract?.interface?.getFunction(methodName)
    , [
        contract,
        methodName
    ]);
    const calls = (0,react__WEBPACK_IMPORTED_MODULE_0__.useMemo)(()=>{
        return contract && fragment && isValidMethodArgs(inputs) ? [
            {
                address: contract.address,
                callData: contract.interface.encodeFunctionData(fragment, inputs)
            }, 
        ] : [];
    }, [
        contract,
        fragment,
        inputs
    ]);
    const result = useCallsData(calls, options)[0];
    const currentBlock = (0,state_block_hooks__WEBPACK_IMPORTED_MODULE_3__/* .useCurrentBlock */ .je)();
    return (0,react__WEBPACK_IMPORTED_MODULE_0__.useMemo)(()=>{
        return toCallState(result, contract?.interface, fragment, currentBlock);
    }, [
        result,
        contract,
        fragment,
        currentBlock
    ]);
}


/***/ }),

/***/ 6849:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "HD": () => (/* binding */ combineApiAndSgResponseToNftToken),
  "Ye": () => (/* binding */ combineCollectionData),
  "nh": () => (/* binding */ fetchNftsFiltered),
  "F7": () => (/* binding */ getCollectionApi),
  "zS": () => (/* binding */ getCollectionSg),
  "g_": () => (/* binding */ getCollectionsApi),
  "JN": () => (/* binding */ getCollectionsSg),
  "Z1": () => (/* binding */ getMarketDataForTokenIds),
  "M2": () => (/* binding */ getMetadataWithFallback),
  "hb": () => (/* binding */ getNftApi),
  "p9": () => (/* binding */ getNftsByBunnyIdSg),
  "Rq": () => (/* binding */ getNftsFromCollectionApi),
  "T5": () => (/* binding */ getNftsMarketData),
  "rD": () => (/* binding */ getPancakeBunniesAttributesField)
});

// UNUSED EXPORTS: attachMarketDataToWalletNfts, combineNftMarketAndMetadata, fetchWalletTokenIdsForCollections, getAllPancakeBunniesLowestPrice, getAllPancakeBunniesRecentUpdatedAt, getCollectionActivity, getCollectionDistributionApi, getCompleteAccountNftData, getLatestListedNfts, getLowestPriceInCollection, getNftLocationForMarketNft, getNftsFromCollectionSg, getNftsFromDifferentCollectionsApi, getTokenActivity, getUserActivity

// EXTERNAL MODULE: external "graphql-request"
var external_graphql_request_ = __webpack_require__(5805);
// EXTERNAL MODULE: external "querystring"
var external_querystring_ = __webpack_require__(9819);
// EXTERNAL MODULE: ./src/config/constants/endpoints.ts
var endpoints = __webpack_require__(5906);
// EXTERNAL MODULE: ./src/utils/multicall.ts
var multicall = __webpack_require__(1144);
// EXTERNAL MODULE: ./src/config/abi/erc721.json
var erc721 = __webpack_require__(3400);
// EXTERNAL MODULE: external "lodash/range"
var range_ = __webpack_require__(4042);
// EXTERNAL MODULE: external "lodash/uniq"
var uniq_ = __webpack_require__(8459);
// EXTERNAL MODULE: ./src/views/Nft/market/constants.ts + 2 modules
var constants = __webpack_require__(1940);
// EXTERNAL MODULE: ./src/state/nftMarket/types.ts
var types = __webpack_require__(5186);
;// CONCATENATED MODULE: ./src/state/nftMarket/queries.ts
const queries_getBaseNftFields = ()=>`
  tokenId
  metadataUrl
  currentAskPrice
  currentSeller
  latestTradedPriceInBNB
  tradeVolumeBNB
  totalTrades
  isTradable
  updatedAt
  otherId
  collection {
    id
  }
`
;
const queries_getBaseTransactionFields = ()=>`
  id
  block
  timestamp
  askPrice
  netPrice
  withBNB
  buyer {
    id
  }
  seller {
    id
  }
`
;
const getCollectionBaseFields = ()=>`
  id
  name
  symbol
  active
  totalTrades
  totalVolumeBNB
  numberTokensListed
  creatorAddress
  tradingFee
  creatorFee
  whitelistChecker
`
;

;// CONCATENATED MODULE: ./src/state/nftMarket/helpers.ts










/**
 * API HELPERS
 */ /**
 * Fetch static data from all collections using the API
 * @returns
 */ const getCollectionsApi = async ()=>{
    const res = await fetch(`${endpoints/* API_NFT */.C1}/collections`);
    if (res.ok) {
        const json = await res.json();
        return json;
    }
    console.error('Failed to fetch NFT collections', res.statusText);
    return null;
};
/**
 * Fetch static data from a collection using the API
 * @returns
 */ const getCollectionApi = async (collectionAddress)=>{
    const res = await fetch(`${endpoints/* API_NFT */.C1}/collections/${collectionAddress}`);
    if (res.ok) {
        const json = await res.json();
        return json.data;
    }
    console.error(`API: Failed to fetch NFT collection ${collectionAddress}`, res.statusText);
    return null;
};
/**
 * Fetch static data for all nfts in a collection using the API
 * @param collectionAddress
 * @param size
 * @param page
 * @returns
 */ const getNftsFromCollectionApi = async (collectionAddress, size = 100, page = 1)=>{
    const isPBCollection = collectionAddress.toLowerCase() === constants/* pancakeBunniesAddress.toLowerCase */.Jr.toLowerCase();
    const requestPath = `${endpoints/* API_NFT */.C1}/collections/${collectionAddress}/tokens${!isPBCollection ? `?page=${page}&size=${size}` : ``}`;
    const res = await fetch(requestPath);
    if (res.ok) {
        const data = await res.json();
        return data;
    }
    console.error(`API: Failed to fetch NFT tokens for ${collectionAddress} collection`, res.statusText);
    return null;
};
/**
 * Fetch a single NFT using the API
 * @param collectionAddress
 * @param tokenId
 * @returns NFT from API
 */ const getNftApi = async (collectionAddress, tokenId)=>{
    const res = await fetch(`${endpoints/* API_NFT */.C1}/collections/${collectionAddress}/tokens/${tokenId}`);
    if (res.ok) {
        const json = await res.json();
        return json.data;
    }
    console.error(`API: Can't fetch NFT token ${tokenId} in ${collectionAddress}`, res.status);
    return null;
};
/**
 * Fetch a list of NFT from different collections
 * @param from Array of { collectionAddress: string; tokenId: string }
 * @returns Array of NFT from API
 */ const getNftsFromDifferentCollectionsApi = async (from)=>{
    const promises = from.map((nft)=>getNftApi(nft.collectionAddress, nft.tokenId)
    );
    const responses = await Promise.all(promises);
    // Sometimes API can't find some tokens (e.g. 404 response)
    // at least return the ones that returned successfully
    return responses.filter((resp)=>resp
    ).map((res, index)=>({
            tokenId: res.tokenId,
            name: res.name,
            collectionName: res.collection.name,
            collectionAddress: from[index].collectionAddress,
            description: res.description,
            attributes: res.attributes,
            createdAt: res.createdAt,
            updatedAt: res.updatedAt,
            image: res.image
        })
    );
};
/**
 * SUBGRAPH HELPERS
 */ /**
 * Fetch market data from a collection using the Subgraph
 * @returns
 */ const getCollectionSg = async (collectionAddress)=>{
    try {
        const res = await (0,external_graphql_request_.request)(endpoints/* GRAPH_API_NFTMARKET */.Bd, external_graphql_request_.gql`
        query getCollectionData($collectionAddress: String!) {
          collection(id: $collectionAddress) {
            ${getCollectionBaseFields()}
          }
        }
      `, {
            collectionAddress: collectionAddress.toLowerCase()
        });
        return res.collection;
    } catch (error) {
        console.error('Failed to fetch collection', error);
        return null;
    }
};
/**
 * Fetch market data from all collections using the Subgraph
 * @returns
 */ const getCollectionsSg = async ()=>{
    try {
        const res = await (0,external_graphql_request_.request)(endpoints/* GRAPH_API_NFTMARKET */.Bd, external_graphql_request_.gql`
        {
          collections {
            ${getCollectionBaseFields()}
          }
        }
      `);
        return res.collections;
    } catch (error) {
        console.error('Failed to fetch NFT collections', error);
        return [];
    }
};
/**
 * Fetch market data for nfts in a collection using the Subgraph
 * @param collectionAddress
 * @param first
 * @param skip
 * @returns
 */ const getNftsFromCollectionSg = async (collectionAddress, first = 1000, skip = 0)=>{
    // Squad to be sorted by tokenId as this matches the order of the paginated API return. For PBs - get the most recent,
    const isPBCollection = collectionAddress.toLowerCase() === pancakeBunniesAddress.toLowerCase();
    try {
        const res = await request(GRAPH_API_NFTMARKET, gql`
        query getNftCollectionMarketData($collectionAddress: String!) {
          collection(id: $collectionAddress) {
            id
            nfts(orderBy:${isPBCollection ? 'updatedAt' : 'tokenId'}, skip: $skip, first: $first) {
             ${getBaseNftFields()}
            }
          }
        }
      `, {
            collectionAddress: collectionAddress.toLowerCase(),
            skip,
            first
        });
        return res.collection.nfts;
    } catch (error) {
        console.error('Failed to fetch NFTs from collection', error);
        return [];
    }
};
/**
 * Fetch market data for PancakeBunnies NFTs by bunny id using the Subgraph
 * @param bunnyId - bunny id to query
 * @param existingTokenIds - tokens that are already loaded into redux
 * @returns
 */ const getNftsByBunnyIdSg = async (bunnyId, existingTokenIds, orderDirection)=>{
    try {
        const where = existingTokenIds.length > 0 ? {
            otherId: bunnyId,
            isTradable: true,
            tokenId_not_in: existingTokenIds
        } : {
            otherId: bunnyId,
            isTradable: true
        };
        const res = await (0,external_graphql_request_.request)(endpoints/* GRAPH_API_NFTMARKET */.Bd, external_graphql_request_.gql`
        query getNftsByBunnyIdSg($collectionAddress: String!, $where: NFT_filter, $orderDirection: String!) {
          nfts(first: 30, where: $where, orderBy: currentAskPrice, orderDirection: $orderDirection) {
            ${queries_getBaseNftFields()}
          }
        }
      `, {
            collectionAddress: constants/* pancakeBunniesAddress.toLowerCase */.Jr.toLowerCase(),
            where,
            orderDirection
        });
        return res.nfts;
    } catch (error) {
        console.error(`Failed to fetch collection NFTs for bunny id ${bunnyId}`, error);
        return [];
    }
};
/**
 * Fetch market data for PancakeBunnies NFTs by bunny id using the Subgraph
 * @param bunnyId - bunny id to query
 * @param existingTokenIds - tokens that are already loaded into redux
 * @returns
 */ const getMarketDataForTokenIds = async (collectionAddress, existingTokenIds)=>{
    try {
        if (existingTokenIds.length === 0) {
            return [];
        }
        const res = await (0,external_graphql_request_.request)(endpoints/* GRAPH_API_NFTMARKET */.Bd, external_graphql_request_.gql`
        query getMarketDataForTokenIds($collectionAddress: String!, $where: NFT_filter) {
          collection(id: $collectionAddress) {
            id
            nfts(first: 1000, where: $where) {
              ${queries_getBaseNftFields()}
            }
          }
        }
      `, {
            collectionAddress: collectionAddress.toLowerCase(),
            where: {
                tokenId_in: existingTokenIds
            }
        });
        return res.collection.nfts;
    } catch (error) {
        console.error(`Failed to fetch market data for NFTs stored tokens`, error);
        return [];
    }
};
const getNftsMarketData = async (where = {}, first = 1000, orderBy = 'id', orderDirection = 'desc', skip = 0)=>{
    try {
        const res = await (0,external_graphql_request_.request)(endpoints/* GRAPH_API_NFTMARKET */.Bd, external_graphql_request_.gql`
        query getNftsMarketData($first: Int, $skip: Int!, $where: NFT_filter, $orderBy: NFT_orderBy, $orderDirection: OrderDirection) {
          nfts(where: $where, first: $first, orderBy: $orderBy, orderDirection: $orderDirection, skip: $skip) {
            ${queries_getBaseNftFields()}
            transactionHistory {
              ${queries_getBaseTransactionFields()}
            }
          }
        }
      `, {
            where,
            first,
            skip,
            orderBy,
            orderDirection
        });
        return res.nfts;
    } catch (error) {
        console.error('Failed to fetch NFTs market data', error);
        return [];
    }
};
const getAllPancakeBunniesLowestPrice = async (bunnyIds)=>{
    try {
        const singlePancakeBunnySubQueries = bunnyIds.map((bunnyId)=>`b${bunnyId}:nfts(first: 1, where: { otherId: ${bunnyId}, isTradable: true }, orderBy: currentAskPrice, orderDirection: asc) {
        currentAskPrice
      }
    `
        );
        const rawResponse = await request(GRAPH_API_NFTMARKET, gql`
        query getAllPancakeBunniesLowestPrice {
          ${singlePancakeBunnySubQueries}
        }
      `);
        return Object.keys(rawResponse).reduce((lowestPricesData, subQueryKey)=>{
            const bunnyId = subQueryKey.split('b')[1];
            return {
                ...lowestPricesData,
                [bunnyId]: rawResponse[subQueryKey].length > 0 ? parseFloat(rawResponse[subQueryKey][0].currentAskPrice) : Infinity
            };
        }, {});
    } catch (error) {
        console.error('Failed to fetch PancakeBunnies lowest prices', error);
        return {};
    }
};
const getAllPancakeBunniesRecentUpdatedAt = async (bunnyIds)=>{
    try {
        const singlePancakeBunnySubQueries = bunnyIds.map((bunnyId)=>`b${bunnyId}:nfts(first: 1, where: { otherId: ${bunnyId}, isTradable: true }, orderBy: updatedAt, orderDirection: desc) {
        updatedAt
      }
    `
        );
        const rawResponse = await request(GRAPH_API_NFTMARKET, gql`
        query getAllPancakeBunniesLowestPrice {
          ${singlePancakeBunnySubQueries}
        }
      `);
        return Object.keys(rawResponse).reduce((updatedAtData, subQueryKey)=>{
            const bunnyId = subQueryKey.split('b')[1];
            return {
                ...updatedAtData,
                [bunnyId]: rawResponse[subQueryKey].length > 0 ? Number(rawResponse[subQueryKey][0].updatedAt) : -Infinity
            };
        }, {});
    } catch (error) {
        console.error('Failed to fetch PancakeBunnies latest market updates', error);
        return {};
    }
};
/**
 * Returns the lowest price of any NFT in a collection
 */ const getLowestPriceInCollection = async (collectionAddress)=>{
    try {
        const response = await getNftsMarketData({
            collection: collectionAddress.toLowerCase(),
            isTradable: true
        }, 1, 'currentAskPrice', 'asc');
        if (response.length === 0) {
            return 0;
        }
        const [nftSg] = response;
        return parseFloat(nftSg.currentAskPrice);
    } catch (error) {
        console.error(`Failed to lowest price NFTs in collection ${collectionAddress}`, error);
        return 0;
    }
};
/**
 * Fetch user trading data for buyTradeHistory, sellTradeHistory and askOrderHistory from the Subgraph
 * @param where a User_filter where condition
 * @returns a UserActivity object
 */ const getUserActivity = async (address)=>{
    try {
        const res = await request(GRAPH_API_NFTMARKET, gql`
        query getUserActivity($address: String!) {
          user(id: $address) {
            buyTradeHistory(first: 250, orderBy: timestamp, orderDirection: desc) {
              ${getBaseTransactionFields()}
              nft {
                ${getBaseNftFields()}
              }
            }
            sellTradeHistory(first: 250, orderBy: timestamp, orderDirection: desc) {
              ${getBaseTransactionFields()}
              nft {
                ${getBaseNftFields()}
              }
            }
            askOrderHistory(first: 500, orderBy: timestamp, orderDirection: desc) {
              id
              block
              timestamp
              orderType
              askPrice
              nft {
                ${getBaseNftFields()}
              }
            }
          }
        }
      `, {
            address
        });
        return res.user || {
            askOrderHistory: [],
            buyTradeHistory: [],
            sellTradeHistory: []
        };
    } catch (error) {
        console.error('Failed to fetch user Activity', error);
        return {
            askOrderHistory: [],
            buyTradeHistory: [],
            sellTradeHistory: []
        };
    }
};
const getCollectionActivity = async (address, nftActivityFilter, itemPerQuery)=>{
    const getAskOrderEvent = (orderType)=>{
        switch(orderType){
            case MarketEvent.CANCEL:
                return AskOrderType.CANCEL;
            case MarketEvent.MODIFY:
                return AskOrderType.MODIFY;
            case MarketEvent.NEW:
                return AskOrderType.NEW;
            default:
                return AskOrderType.MODIFY;
        }
    };
    const isFetchAllCollections = address === '';
    const hasCollectionFilter = nftActivityFilter.collectionFilters.length > 0;
    const collectionFilterGql = !isFetchAllCollections ? `collection: ${JSON.stringify(address)}` : hasCollectionFilter ? `collection_in: ${JSON.stringify(nftActivityFilter.collectionFilters)}` : ``;
    const askOrderTypeFilter = nftActivityFilter.typeFilters.filter((marketEvent)=>marketEvent !== MarketEvent.SELL
    ).map((marketEvent)=>getAskOrderEvent(marketEvent)
    );
    const askOrderIncluded = nftActivityFilter.typeFilters.length === 0 || askOrderTypeFilter.length > 0;
    const askOrderTypeFilterGql = askOrderTypeFilter.length > 0 ? `orderType_in: ${JSON.stringify(askOrderTypeFilter)}` : ``;
    const transactionIncluded = nftActivityFilter.typeFilters.length === 0 || nftActivityFilter.typeFilters.some((marketEvent)=>marketEvent === MarketEvent.BUY || marketEvent === MarketEvent.SELL
    );
    let askOrderQueryItem = itemPerQuery / 2;
    let transactionQueryItem = itemPerQuery / 2;
    if (!askOrderIncluded || !transactionIncluded) {
        askOrderQueryItem = !askOrderIncluded ? 0 : itemPerQuery;
        transactionQueryItem = !transactionIncluded ? 0 : itemPerQuery;
    }
    const askOrderGql = askOrderIncluded ? `askOrders(first: ${askOrderQueryItem}, orderBy: timestamp, orderDirection: desc, where:{
            ${collectionFilterGql}, ${askOrderTypeFilterGql}
          }) {
              id
              block
              timestamp
              orderType
              askPrice
              seller {
                id
              }
              nft {
                ${getBaseNftFields()}
              }
          }` : ``;
    const transactionGql = transactionIncluded ? `transactions(first: ${transactionQueryItem}, orderBy: timestamp, orderDirection: desc, where:{
            ${collectionFilterGql}
          }) {
            ${getBaseTransactionFields()}
              nft {
                ${getBaseNftFields()}
              }
          }` : ``;
    try {
        const res = await request(GRAPH_API_NFTMARKET, gql`
        query getCollectionActivity {
          ${askOrderGql}
          ${transactionGql}
        }
      `);
        return res || {
            askOrders: [],
            transactions: []
        };
    } catch (error) {
        console.error('Failed to fetch collection Activity', error);
        return {
            askOrders: [],
            transactions: []
        };
    }
};
const getTokenActivity = async (tokenId, collectionAddress)=>{
    try {
        const res = await request(GRAPH_API_NFTMARKET, gql`
        query getCollectionActivity($tokenId: BigInt!, $address: ID!) {
          nfts(where:{tokenId: $tokenId, collection: $address}) {
            transactionHistory(orderBy: timestamp, orderDirection: desc) {
              ${getBaseTransactionFields()}
                nft {
                  ${getBaseNftFields()}
                }
            }
            askHistory(orderBy: timestamp, orderDirection: desc) {
                id
                block
                timestamp
                orderType
                askPrice
                seller {
                  id
                }
                nft {
                  ${getBaseNftFields()}
                }
            }
          }
        }
      `, {
            tokenId,
            address: collectionAddress
        });
        if (res.nfts.length > 0) {
            return {
                askOrders: res.nfts[0].askHistory,
                transactions: res.nfts[0].transactionHistory
            };
        }
        return {
            askOrders: [],
            transactions: []
        };
    } catch (error) {
        console.error('Failed to fetch token Activity', error);
        return {
            askOrders: [],
            transactions: []
        };
    }
};
/**
 * Get the most recently listed NFTs
 * @param first Number of nfts to retrieve
 * @returns NftTokenSg[]
 */ const getLatestListedNfts = async (first)=>{
    try {
        const res = await request(GRAPH_API_NFTMARKET, gql`
        query getLatestNftMarketData($first: Int) {
          nfts(where: { isTradable: true }, orderBy: updatedAt , orderDirection: desc, first: $first) {
            ${getBaseNftFields()}
            collection {
              id
            }
          }
        }
      `, {
            first
        });
        return res.nfts;
    } catch (error) {
        console.error('Failed to fetch NFTs market data', error);
        return [];
    }
};
/**
 * Filter NFTs from a collection
 * @param collectionAddress
 * @returns
 */ const fetchNftsFiltered = async (collectionAddress, filters)=>{
    const res = await fetch(`${endpoints/* API_NFT */.C1}/collections/${collectionAddress}/filter?${(0,external_querystring_.stringify)(filters)}`);
    if (res.ok) {
        const data = await res.json();
        return data;
    }
    console.error(`API: Failed to fetch NFT collection ${collectionAddress}`, res.statusText);
    return null;
};
/**
 * OTHER HELPERS
 */ const getMetadataWithFallback = (apiMetadata, bunnyId)=>{
    // The fallback is just for the testnet where some bunnies don't exist
    return apiMetadata[bunnyId] ?? {
        name: '',
        description: '',
        collection: {
            name: 'Pancake Bunnies'
        },
        image: {
            original: '',
            thumbnail: ''
        }
    };
};
const getPancakeBunniesAttributesField = (bunnyId)=>{
    // Generating attributes field that is not returned by API
    // but can be "faked" since objects are keyed with bunny id
    return [
        {
            traitType: 'bunnyId',
            value: bunnyId,
            displayType: null
        }, 
    ];
};
const combineApiAndSgResponseToNftToken = (apiMetadata, marketData, attributes)=>{
    return {
        tokenId: marketData.tokenId,
        name: apiMetadata.name,
        description: apiMetadata.description,
        collectionName: apiMetadata.collection.name,
        collectionAddress: constants/* pancakeBunniesAddress */.Jr,
        image: apiMetadata.image,
        marketData,
        attributes
    };
};
const fetchWalletTokenIdsForCollections = async (account, collections)=>{
    const balanceOfCalls = Object.values(collections).map((collection)=>{
        const { address: collectionAddress  } = collection;
        return {
            address: collectionAddress,
            name: 'balanceOf',
            params: [
                account
            ]
        };
    });
    const balanceOfCallsResultRaw = await multicallv2(erc721Abi, balanceOfCalls, {
        requireSuccess: false
    });
    const balanceOfCallsResult = balanceOfCallsResultRaw.flat();
    const tokenIdCalls = Object.values(collections).map((collection, index)=>{
        const balanceOf = balanceOfCallsResult[index]?.toNumber() ?? 0;
        const { address: collectionAddress  } = collection;
        return range(balanceOf).map((tokenIndex)=>{
            return {
                address: collectionAddress,
                name: 'tokenOfOwnerByIndex',
                params: [
                    account,
                    tokenIndex
                ]
            };
        });
    }).flat();
    const tokenIdResultRaw = await multicallv2(erc721Abi, tokenIdCalls, {
        requireSuccess: false
    });
    const tokenIdResult = tokenIdResultRaw.flat();
    const nftLocation = NftLocation.WALLET;
    const walletNfts = tokenIdResult.reduce((acc, tokenIdBn, index)=>{
        if (tokenIdBn) {
            const { address: collectionAddress  } = tokenIdCalls[index];
            acc.push({
                tokenId: tokenIdBn.toString(),
                collectionAddress,
                nftLocation
            });
        }
        return acc;
    }, []);
    return walletNfts;
};
/**
 * Helper to combine data from the collections' API and subgraph
 */ const combineCollectionData = (collectionApiData, collectionSgData)=>{
    const collectionsMarketObj = collectionSgData.reduce((prev, current)=>({
            ...prev,
            [current.id]: {
                ...current
            }
        })
    , {});
    return collectionApiData.reduce((accum, current)=>{
        const collectionMarket = collectionsMarketObj[current.address.toLowerCase()];
        const collection = {
            ...current,
            ...collectionMarket
        };
        if (current.name) {
            collection.name = current.name;
        }
        return {
            ...accum,
            [current.address]: collection
        };
    }, {});
};
/**
 * Evaluate whether a market NFT is in a users wallet, their profile picture, or on sale
 * @param tokenId string
 * @param tokenIdsInWallet array of tokenIds in wallet
 * @param tokenIdsForSale array of tokenIds on sale
 * @param profileNftId Optional tokenId of users' profile picture
 * @returns NftLocation enum value
 */ const getNftLocationForMarketNft = (tokenId, tokenIdsInWallet, tokenIdsForSale, profileNftId)=>{
    if (tokenId === profileNftId) {
        return NftLocation.PROFILE;
    }
    if (tokenIdsForSale.includes(tokenId)) {
        return NftLocation.FORSALE;
    }
    if (tokenIdsInWallet.includes(tokenId)) {
        return NftLocation.WALLET;
    }
    console.error(`Cannot determine location for tokenID ${tokenId}, defaulting to NftLocation.WALLET`);
    return NftLocation.WALLET;
};
/**
 * Construct complete TokenMarketData entities with a users' wallet NFT ids and market data for their wallet NFTs
 * @param walletNfts TokenIdWithCollectionAddress
 * @param marketDataForWalletNfts TokenMarketData[]
 * @returns TokenMarketData[]
 */ const attachMarketDataToWalletNfts = (walletNfts, marketDataForWalletNfts)=>{
    const walletNftsWithMarketData = walletNfts.map((walletNft)=>{
        const marketData = marketDataForWalletNfts.find((marketNft)=>marketNft.tokenId === walletNft.tokenId && marketNft.collection.id.toLowerCase() === walletNft.collectionAddress.toLowerCase()
        );
        return marketData ?? {
            tokenId: walletNft.tokenId,
            collection: {
                id: walletNft.collectionAddress.toLowerCase()
            },
            nftLocation: walletNft.nftLocation,
            metadataUrl: null,
            transactionHistory: null,
            currentSeller: null,
            isTradable: null,
            currentAskPrice: null,
            latestTradedPriceInBNB: null,
            tradeVolumeBNB: null,
            totalTrades: null,
            otherId: null
        };
    });
    return walletNftsWithMarketData;
};
/**
 * Attach TokenMarketData and location to NftToken
 * @param nftsWithMetadata NftToken[] with API metadata
 * @param nftsForSale  market data for nfts that are on sale (i.e. not in a user's wallet)
 * @param walletNfts market data for nfts in a user's wallet
 * @param tokenIdsInWallet array of token ids in user's wallet
 * @param tokenIdsForSale array of token ids of nfts that are on sale
 * @param profileNftId profile picture token id
 * @returns NFT[]
 */ const combineNftMarketAndMetadata = (nftsWithMetadata, nftsForSale, walletNfts, tokenIdsInWallet, tokenIdsForSale, profileNftId)=>{
    const completeNftData = nftsWithMetadata.map((nft)=>{
        // Get metadata object
        const isOnSale = nftsForSale.filter((forSaleNft)=>forSaleNft.tokenId === nft.tokenId && forSaleNft.collection && forSaleNft.collection.id === nft.collectionAddress
        ).length > 0;
        let marketData;
        if (isOnSale) {
            marketData = nftsForSale.find((marketNft)=>marketNft.collection && marketNft.collection.id === nft.collectionAddress && marketNft.tokenId === nft.tokenId
            );
        } else {
            marketData = walletNfts.find((marketNft)=>marketNft.collection && marketNft.collection.id === nft.collectionAddress && marketNft.tokenId === nft.tokenId
            );
        }
        const location = getNftLocationForMarketNft(nft.tokenId, tokenIdsInWallet, tokenIdsForSale, profileNftId);
        return {
            ...nft,
            marketData,
            location
        };
    });
    return completeNftData;
};
/**
 * Get in-wallet, on-sale & profile pic NFT metadata, complete with market data for a given account
 * @param account
 * @param collections
 * @param profileNftWithCollectionAddress
 * @returns Promise<NftToken[]>
 */ const getCompleteAccountNftData = async (account, collections, profileNftWithCollectionAddress)=>{
    const walletNftIdsWithCollectionAddress = await fetchWalletTokenIdsForCollections(account, collections);
    if (profileNftWithCollectionAddress?.tokenId) {
        walletNftIdsWithCollectionAddress.unshift(profileNftWithCollectionAddress);
    }
    const uniqueCollectionAddresses = uniq(walletNftIdsWithCollectionAddress.map((walletNftId)=>walletNftId.collectionAddress
    ));
    const walletNftsByCollection = uniqueCollectionAddresses.map((collectionAddress)=>{
        return {
            collectionAddress,
            idWithCollectionAddress: walletNftIdsWithCollectionAddress.filter((walletNft)=>walletNft.collectionAddress === collectionAddress
            )
        };
    });
    const walletMarketDataRequests = walletNftsByCollection.map((walletNftByCollection)=>{
        const tokenIdIn = walletNftByCollection.idWithCollectionAddress.map((walletNft)=>walletNft.tokenId
        );
        return getNftsMarketData({
            tokenId_in: tokenIdIn,
            collection: walletNftByCollection.collectionAddress.toLowerCase()
        });
    });
    const walletMarketDataResponses = await Promise.all(walletMarketDataRequests);
    const walletMarketData = walletMarketDataResponses.flat();
    const walletNftsWithMarketData = attachMarketDataToWalletNfts(walletNftIdsWithCollectionAddress, walletMarketData);
    const walletTokenIds = walletNftIdsWithCollectionAddress.filter((walletNft)=>{
        // Profile Pic NFT is no longer wanted in this array, hence the filter
        return profileNftWithCollectionAddress?.tokenId !== walletNft.tokenId;
    }).map((nft)=>nft.tokenId
    );
    const marketDataForSaleNfts = await getNftsMarketData({
        currentSeller: account.toLowerCase()
    });
    const tokenIdsForSale = marketDataForSaleNfts.map((nft)=>nft.tokenId
    );
    const forSaleNftIds = marketDataForSaleNfts.map((nft)=>{
        return {
            collectionAddress: nft.collection.id,
            tokenId: nft.tokenId
        };
    });
    const metadataForAllNfts = await getNftsFromDifferentCollectionsApi([
        ...forSaleNftIds,
        ...walletNftIdsWithCollectionAddress, 
    ]);
    const completeNftData = combineNftMarketAndMetadata(metadataForAllNfts, marketDataForSaleNfts, walletNftsWithMarketData, walletTokenIds, tokenIdsForSale, profileNftWithCollectionAddress?.tokenId);
    return completeNftData;
};
/**
 * Fetch distribution information for a collection
 * @returns
 */ const getCollectionDistributionApi = async (collectionAddress)=>{
    const res = await fetch(`${API_NFT}/collections/${collectionAddress}/distribution`);
    if (res.ok) {
        const data = await res.json();
        return data;
    }
    console.error(`API: Failed to fetch NFT collection ${collectionAddress} distribution`, res.statusText);
    return null;
};


/***/ }),

/***/ 5186:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "IB": () => (/* binding */ NFTMarketInitializationState)
/* harmony export */ });
/* unused harmony exports UserNftInitializationState, AskOrderType, NftLocation, MarketEvent */
var NFTMarketInitializationState;
(function(NFTMarketInitializationState) {
    NFTMarketInitializationState["UNINITIALIZED"] = "UNINITIALIZED";
    NFTMarketInitializationState["INITIALIZED"] = "INITIALIZED";
    NFTMarketInitializationState["ERROR"] = "ERROR";
})(NFTMarketInitializationState || (NFTMarketInitializationState = {}));
var UserNftInitializationState;
(function(UserNftInitializationState) {
    UserNftInitializationState["UNINITIALIZED"] = "UNINITIALIZED";
    UserNftInitializationState["INITIALIZING"] = "INITIALIZING";
    UserNftInitializationState["INITIALIZED"] = "INITIALIZED";
    UserNftInitializationState["ERROR"] = "ERROR";
})(UserNftInitializationState || (UserNftInitializationState = {}));
var AskOrderType;
(function(AskOrderType) {
    AskOrderType["NEW"] = 'New';
    AskOrderType["MODIFY"] = 'Modify';
    AskOrderType["CANCEL"] = 'Cancel';
})(AskOrderType || (AskOrderType = {}));
var NftLocation;
(function(NftLocation) {
    NftLocation["FORSALE"] = 'For Sale';
    NftLocation["PROFILE"] = 'Profile Pic';
    NftLocation["WALLET"] = 'In Wallet';
})(NftLocation || (NftLocation = {}));
var MarketEvent;
(function(MarketEvent) {
    MarketEvent["NEW"] = "NEW";
    MarketEvent["CANCEL"] = "CANCEL";
    MarketEvent["MODIFY"] = "MODIFY";
    MarketEvent["BUY"] = "BUY";
    MarketEvent["SELL"] = "SELL";
})(MarketEvent || (MarketEvent = {}));


/***/ }),

/***/ 5712:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Ms": () => (/* binding */ getUsername),
/* harmony export */   "rC": () => (/* binding */ getProfileAvatar),
/* harmony export */   "Ai": () => (/* binding */ getProfile)
/* harmony export */ });
/* harmony import */ var config_abi_pancakeProfile_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2335);
/* harmony import */ var state_teams_helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9423);
/* harmony import */ var state_nftMarket_helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6849);
/* harmony import */ var utils_multicall__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1144);
/* harmony import */ var utils_addressHelpers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5878);





const transformProfileResponse = (profileResponse)=>{
    const { 0: userId , 1: numberPoints , 2: teamId , 3: collectionAddress , 4: tokenId , 5: isActive  } = profileResponse;
    return {
        userId: userId.toNumber(),
        points: numberPoints.toNumber(),
        teamId: teamId.toNumber(),
        tokenId: tokenId.toNumber(),
        collectionAddress,
        isActive
    };
};
const profileApi = "https://profile.pancakeswap.com";
const getUsername = async (address)=>{
    try {
        const response = await fetch(`${profileApi}/api/users/${address.toLowerCase()}`);
        if (!response.ok) {
            return '';
        }
        const { username =''  } = await response.json();
        return username;
    } catch (error) {
        return '';
    }
};
/**
 * Intended to be used for getting a profile avatar
 */ const getProfileAvatar = async (address)=>{
    try {
        const profileCalls = [
            'hasRegistered',
            'getUserProfile'
        ].map((method)=>{
            return {
                address: (0,utils_addressHelpers__WEBPACK_IMPORTED_MODULE_4__/* .getPancakeProfileAddress */ .Re)(),
                name: method,
                params: [
                    address
                ]
            };
        });
        const profileCallsResult = await (0,utils_multicall__WEBPACK_IMPORTED_MODULE_3__/* .multicallv2 */ .v)(config_abi_pancakeProfile_json__WEBPACK_IMPORTED_MODULE_0__, profileCalls, {
            requireSuccess: false
        });
        const [[hasRegistered], profileResponse] = profileCallsResult;
        if (!hasRegistered) {
            return null;
        }
        const { tokenId , collectionAddress , isActive  } = transformProfileResponse(profileResponse);
        let nft = null;
        if (isActive) {
            const apiRes = await (0,state_nftMarket_helpers__WEBPACK_IMPORTED_MODULE_2__/* .getNftApi */ .hb)(collectionAddress, tokenId.toString());
            nft = {
                tokenId: apiRes.tokenId,
                name: apiRes.name,
                collectionName: apiRes.collection.name,
                collectionAddress,
                description: apiRes.description,
                attributes: apiRes.attributes,
                createdAt: apiRes.createdAt,
                updatedAt: apiRes.updatedAt,
                image: {
                    original: apiRes.image?.original,
                    thumbnail: apiRes.image?.thumbnail
                }
            };
        }
        return {
            nft,
            hasRegistered
        };
    } catch  {
        return {
            nft: null,
            hasRegistered: false
        };
    }
};
const getProfile = async (address)=>{
    try {
        const profileCalls = [
            'hasRegistered',
            'getUserProfile'
        ].map((method)=>{
            return {
                address: (0,utils_addressHelpers__WEBPACK_IMPORTED_MODULE_4__/* .getPancakeProfileAddress */ .Re)(),
                name: method,
                params: [
                    address
                ]
            };
        });
        const profileCallsResult = await (0,utils_multicall__WEBPACK_IMPORTED_MODULE_3__/* .multicallv2 */ .v)(config_abi_pancakeProfile_json__WEBPACK_IMPORTED_MODULE_0__, profileCalls, {
            requireSuccess: false
        });
        const [[hasRegistered], profileResponse] = profileCallsResult;
        if (!hasRegistered) {
            return {
                hasRegistered,
                profile: null
            };
        }
        const { userId , points , teamId , tokenId , collectionAddress , isActive  } = transformProfileResponse(profileResponse);
        const team = await (0,state_teams_helpers__WEBPACK_IMPORTED_MODULE_1__/* .getTeam */ .V)(teamId);
        const username = await getUsername(address);
        let nftToken;
        // If the profile is not active the tokenId returns 0, which is still a valid token id
        // so only fetch the nft data if active
        if (isActive) {
            const apiRes = await (0,state_nftMarket_helpers__WEBPACK_IMPORTED_MODULE_2__/* .getNftApi */ .hb)(collectionAddress, tokenId.toString());
            if (apiRes) {
                nftToken = {
                    tokenId: apiRes.tokenId,
                    name: apiRes.name,
                    collectionName: apiRes.collection.name,
                    collectionAddress,
                    description: apiRes.description,
                    attributes: apiRes.attributes,
                    createdAt: apiRes.createdAt,
                    updatedAt: apiRes.updatedAt,
                    image: {
                        original: apiRes.image?.original,
                        thumbnail: apiRes.image?.thumbnail
                    }
                };
            }
        }
        const profile = {
            userId,
            points,
            teamId,
            tokenId,
            username,
            collectionAddress,
            isActive,
            nft: nftToken,
            team
        };
        return {
            hasRegistered,
            profile
        };
    } catch (e) {
        console.error(e);
        return null;
    }
};


/***/ }),

/***/ 5952:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "In": () => (/* binding */ fetchProfile),
/* harmony export */   "Gb": () => (/* binding */ profileClear),
/* harmony export */   "ZP": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* unused harmony exports initialState, fetchProfileAvatar, fetchProfileUsername, profileSlice, addPoints */
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5184);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var config_constants_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7971);
/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5712);



const initialState = {
    isInitialized: false,
    isLoading: false,
    hasRegistered: false,
    data: null,
    profileAvatars: {}
};
const fetchProfile = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAsyncThunk)('profile/fetchProfile', async (account)=>{
    const { hasRegistered , profile  } = await (0,_helpers__WEBPACK_IMPORTED_MODULE_2__/* .getProfile */ .Ai)(account);
    return {
        hasRegistered,
        profile
    };
});
const fetchProfileAvatar = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAsyncThunk)('profile/fetchProfileAvatar', async (account)=>{
    const { nft , hasRegistered  } = await (0,_helpers__WEBPACK_IMPORTED_MODULE_2__/* .getProfileAvatar */ .rC)(account);
    return {
        account,
        nft,
        hasRegistered
    };
});
const fetchProfileUsername = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAsyncThunk)('profile/fetchProfileUsername', async ({ account , hasRegistered  })=>{
    if (!hasRegistered) {
        return {
            account,
            username: ''
        };
    }
    const username = await (0,_helpers__WEBPACK_IMPORTED_MODULE_2__/* .getUsername */ .Ms)(account);
    return {
        account,
        username
    };
});
const profileSlice = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createSlice)({
    name: 'profile',
    initialState,
    reducers: {
        profileClear: ()=>({
                ...initialState,
                isLoading: false
            })
        ,
        addPoints: (state, action)=>{
            state.data.points += action.payload;
        }
    },
    extraReducers: (builder)=>{
        builder.addCase(fetchProfile.pending, (state)=>{
            state.isLoading = true;
        });
        builder.addCase(fetchProfile.fulfilled, (state, action)=>{
            const { hasRegistered , profile  } = action.payload;
            state.isInitialized = true;
            state.isLoading = false;
            state.hasRegistered = hasRegistered;
            state.data = profile;
        });
        builder.addCase(fetchProfile.rejected, (state)=>{
            state.isLoading = false;
            state.isInitialized = true;
        });
        builder.addCase(fetchProfileUsername.pending, (state, action)=>{
            const { account  } = action.meta.arg;
            if (state.profileAvatars[account]) {
                state.profileAvatars[account] = {
                    ...state.profileAvatars[account],
                    usernameFetchStatus: config_constants_types__WEBPACK_IMPORTED_MODULE_1__/* .FetchStatus.Fetching */ .iF.Fetching
                };
            } else {
                state.profileAvatars[account] = {
                    hasRegistered: false,
                    username: null,
                    nft: null,
                    // I think in theory this else should never be reached since we only check for username after we checked for profile/avatar
                    // just in case I set isFetchingAvatar will be Fetched.Fetching at this point to avoid refetching
                    usernameFetchStatus: config_constants_types__WEBPACK_IMPORTED_MODULE_1__/* .FetchStatus.Fetching */ .iF.Fetching,
                    avatarFetchStatus: config_constants_types__WEBPACK_IMPORTED_MODULE_1__/* .FetchStatus.Fetched */ .iF.Fetched
                };
            }
        });
        builder.addCase(fetchProfileUsername.fulfilled, (state, action)=>{
            const { account , username  } = action.payload;
            if (state.profileAvatars[account]) {
                state.profileAvatars[account] = {
                    ...state.profileAvatars[account],
                    username,
                    usernameFetchStatus: config_constants_types__WEBPACK_IMPORTED_MODULE_1__/* .FetchStatus.Fetched */ .iF.Fetched
                };
            } else {
                state.profileAvatars[account] = {
                    username,
                    nft: null,
                    hasRegistered: true,
                    usernameFetchStatus: config_constants_types__WEBPACK_IMPORTED_MODULE_1__/* .FetchStatus.Fetched */ .iF.Fetched,
                    // I think in theory this else should never be reached since we only check for username after we checked for profile/avatar
                    // just in case I set isFetchingAvatar will be FetchStatus.Fetched at this point to avoid refetching
                    avatarFetchStatus: config_constants_types__WEBPACK_IMPORTED_MODULE_1__/* .FetchStatus.Fetched */ .iF.Fetched
                };
            }
        });
        builder.addCase(fetchProfileUsername.rejected, (state, action)=>{
            const { account  } = action.meta.arg;
            if (state.profileAvatars[account]) {
                state.profileAvatars[account] = {
                    ...state.profileAvatars[account],
                    username: '',
                    usernameFetchStatus: config_constants_types__WEBPACK_IMPORTED_MODULE_1__/* .FetchStatus.Fetched */ .iF.Fetched
                };
            } else {
                state.profileAvatars[account] = {
                    hasRegistered: false,
                    username: '',
                    nft: null,
                    usernameFetchStatus: config_constants_types__WEBPACK_IMPORTED_MODULE_1__/* .FetchStatus.Fetched */ .iF.Fetched,
                    avatarFetchStatus: config_constants_types__WEBPACK_IMPORTED_MODULE_1__/* .FetchStatus.Fetched */ .iF.Fetched
                };
            }
        });
        builder.addCase(fetchProfileAvatar.pending, (state, action)=>{
            const account = action.meta.arg;
            if (state.profileAvatars[account]) {
                state.profileAvatars[account] = {
                    ...state.profileAvatars[account],
                    hasRegistered: false,
                    avatarFetchStatus: config_constants_types__WEBPACK_IMPORTED_MODULE_1__/* .FetchStatus.Fetching */ .iF.Fetching
                };
            } else {
                state.profileAvatars[account] = {
                    username: null,
                    nft: null,
                    hasRegistered: false,
                    usernameFetchStatus: config_constants_types__WEBPACK_IMPORTED_MODULE_1__/* .FetchStatus.Idle */ .iF.Idle,
                    avatarFetchStatus: config_constants_types__WEBPACK_IMPORTED_MODULE_1__/* .FetchStatus.Fetching */ .iF.Fetching
                };
            }
        });
        builder.addCase(fetchProfileAvatar.fulfilled, (state, action)=>{
            const { account , nft , hasRegistered  } = action.payload;
            if (state.profileAvatars[account]) {
                state.profileAvatars[account] = {
                    ...state.profileAvatars[account],
                    nft,
                    hasRegistered,
                    avatarFetchStatus: config_constants_types__WEBPACK_IMPORTED_MODULE_1__/* .FetchStatus.Fetched */ .iF.Fetched
                };
            } else {
                state.profileAvatars[account] = {
                    username: null,
                    nft,
                    hasRegistered,
                    usernameFetchStatus: config_constants_types__WEBPACK_IMPORTED_MODULE_1__/* .FetchStatus.Idle */ .iF.Idle,
                    avatarFetchStatus: config_constants_types__WEBPACK_IMPORTED_MODULE_1__/* .FetchStatus.Fetched */ .iF.Fetched
                };
            }
        });
        builder.addCase(fetchProfileAvatar.rejected, (state, action)=>{
            const account = action.meta.arg;
            if (state.profileAvatars[account]) {
                state.profileAvatars[account] = {
                    ...state.profileAvatars[account],
                    nft: null,
                    hasRegistered: false,
                    avatarFetchStatus: config_constants_types__WEBPACK_IMPORTED_MODULE_1__/* .FetchStatus.Fetched */ .iF.Fetched
                };
            } else {
                state.profileAvatars[account] = {
                    username: null,
                    nft: null,
                    hasRegistered: false,
                    usernameFetchStatus: config_constants_types__WEBPACK_IMPORTED_MODULE_1__/* .FetchStatus.Idle */ .iF.Idle,
                    avatarFetchStatus: config_constants_types__WEBPACK_IMPORTED_MODULE_1__/* .FetchStatus.Fetched */ .iF.Fetched
                };
            }
        });
    }
});
// Actions
const { profileClear , addPoints  } = profileSlice.actions;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (profileSlice.reducer);


/***/ }),

/***/ 193:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "gN": () => (/* binding */ Field),
/* harmony export */   "j": () => (/* binding */ selectCurrency),
/* harmony export */   "KS": () => (/* binding */ switchCurrencies),
/* harmony export */   "LC": () => (/* binding */ typeInput),
/* harmony export */   "mV": () => (/* binding */ replaceSwapState),
/* harmony export */   "He": () => (/* binding */ setRecipient),
/* harmony export */   "Wk": () => (/* binding */ updatePairData),
/* harmony export */   "_U": () => (/* binding */ updateDerivedPairData)
/* harmony export */ });
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5184);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);

var Field;
(function(Field) {
    Field["INPUT"] = "INPUT";
    Field["OUTPUT"] = "OUTPUT";
})(Field || (Field = {}));
const selectCurrency = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('swap/selectCurrency');
const switchCurrencies = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('swap/switchCurrencies');
const typeInput = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('swap/typeInput');
const replaceSwapState = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('swap/replaceSwapState');
const setRecipient = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('swap/setRecipient');
const updatePairData = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('swap/updatePairData');
const updateDerivedPairData = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('swap/updateDerivedPairData');


/***/ }),

/***/ 9423:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "V": () => (/* binding */ getTeam)
/* harmony export */ });
/* unused harmony export getTeams */
/* harmony import */ var lodash_merge__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1831);
/* harmony import */ var lodash_merge__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash_merge__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var config_constants_teams__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7275);
/* harmony import */ var utils_contractHelpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1553);
/* harmony import */ var utils_multicall__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1144);
/* harmony import */ var config_abi_pancakeProfile_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2335);
/* harmony import */ var utils_addressHelpers__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5878);






const profileContract = (0,utils_contractHelpers__WEBPACK_IMPORTED_MODULE_2__/* .getProfileContract */ .Y4)();
const getTeam = async (teamId)=>{
    try {
        const { 0: teamName , 2: numberUsers , 3: numberPoints , 4: isJoinable  } = await profileContract.getTeamProfile(teamId);
        const staticTeamInfo = config_constants_teams__WEBPACK_IMPORTED_MODULE_1__/* ["default"].find */ .Z.find((staticTeam)=>staticTeam.id === teamId
        );
        return lodash_merge__WEBPACK_IMPORTED_MODULE_0___default()({}, staticTeamInfo, {
            isJoinable,
            name: teamName,
            users: numberUsers.toNumber(),
            points: numberPoints.toNumber()
        });
    } catch (error) {
        return null;
    }
};
/**
 * Gets on-chain data and merges it with the existing static list of teams
 */ const getTeams = async ()=>{
    try {
        const teamsById = teamsList.reduce((accum, team)=>{
            return {
                ...accum,
                [team.id]: team
            };
        }, {});
        const nbTeams = await profileContract.numberTeams();
        const calls = [];
        for(let i = 1; i <= nbTeams.toNumber(); i++){
            calls.push({
                address: getPancakeProfileAddress(),
                name: 'getTeamProfile',
                params: [
                    i
                ]
            });
        }
        const teamData = await multicallv2(profileABI, calls);
        const onChainTeamData = teamData.reduce((accum, team, index)=>{
            const { 0: teamName , 2: numberUsers , 3: numberPoints , 4: isJoinable  } = team;
            return {
                ...accum,
                [index + 1]: {
                    name: teamName,
                    users: numberUsers.toNumber(),
                    points: numberPoints.toNumber(),
                    isJoinable
                }
            };
        }, {});
        return merge({}, teamsById, onChainTeamData);
    } catch (error) {
        return null;
    }
};


/***/ }),

/***/ 1564:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "dT": () => (/* binding */ addTransaction),
/* harmony export */   "fY": () => (/* binding */ clearAllTransactions),
/* harmony export */   "Aw": () => (/* binding */ finalizeTransaction),
/* harmony export */   "LN": () => (/* binding */ checkedTransaction)
/* harmony export */ });
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5184);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);

const addTransaction = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('transactions/addTransaction');
const clearAllTransactions = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('transactions/clearAllTransactions');
const finalizeTransaction = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('transactions/finalizeTransaction');
const checkedTransaction = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('transactions/checkedTransaction');


/***/ }),

/***/ 5101:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "om": () => (/* binding */ VaultKey),
/* harmony export */   "Tu": () => (/* binding */ BetPosition),
/* harmony export */   "Gw": () => (/* binding */ PredictionStatus),
/* harmony export */   "dZ": () => (/* binding */ HistoryFilter),
/* harmony export */   "r7": () => (/* binding */ ProposalState)
/* harmony export */ });
/* unused harmony exports SnapshotCommand, ProposalType */
var VaultKey;
(function(VaultKey) {
    VaultKey["CakeVault"] = 'cakeVault';
    VaultKey["IfoPool"] = 'ifoPool';
})(VaultKey || (VaultKey = {}));
var BetPosition;
(function(BetPosition) {
    BetPosition["BULL"] = 'Bull';
    BetPosition["BEAR"] = 'Bear';
    BetPosition["HOUSE"] = 'House';
})(BetPosition || (BetPosition = {}));
var PredictionStatus;
(function(PredictionStatus) {
    PredictionStatus["INITIAL"] = 'initial';
    PredictionStatus["LIVE"] = 'live';
    PredictionStatus["PAUSED"] = 'paused';
    PredictionStatus["ERROR"] = 'error';
})(PredictionStatus || (PredictionStatus = {}));
var HistoryFilter;
(function(HistoryFilter) {
    HistoryFilter["ALL"] = 'all';
    HistoryFilter["COLLECTED"] = 'collected';
    HistoryFilter["UNCOLLECTED"] = 'uncollected';
})(HistoryFilter || (HistoryFilter = {}));
var SnapshotCommand;
(function(SnapshotCommand) {
    SnapshotCommand["PROPOSAL"] = 'proposal';
    SnapshotCommand["VOTE"] = 'vote';
})(SnapshotCommand || (SnapshotCommand = {}));
var ProposalType;
(function(ProposalType) {
    ProposalType["ALL"] = 'all';
    ProposalType["CORE"] = 'core';
    ProposalType["COMMUNITY"] = 'community';
})(ProposalType || (ProposalType = {}));
var ProposalState;
(function(ProposalState) {
    ProposalState["ACTIVE"] = 'active';
    ProposalState["PENDING"] = 'pending';
    ProposalState["CLOSED"] = 'closed';
})(ProposalState || (ProposalState = {}));


/***/ }),

/***/ 6245:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GR": () => (/* binding */ FarmStakedOnly),
/* harmony export */   "wO": () => (/* binding */ ViewMode),
/* harmony export */   "UU": () => (/* binding */ ChartViewMode),
/* harmony export */   "zv": () => (/* binding */ updateUserExpertMode),
/* harmony export */   "fO": () => (/* binding */ updateUserSingleHopOnly),
/* harmony export */   "rQ": () => (/* binding */ updateUserSlippageTolerance),
/* harmony export */   "gw": () => (/* binding */ updateUserDeadline),
/* harmony export */   "eg": () => (/* binding */ addSerializedToken),
/* harmony export */   "zQ": () => (/* binding */ removeSerializedToken),
/* harmony export */   "f9": () => (/* binding */ addSerializedPair),
/* harmony export */   "cd": () => (/* binding */ removeSerializedPair),
/* harmony export */   "B8": () => (/* binding */ muteAudio),
/* harmony export */   "u7": () => (/* binding */ unmuteAudio),
/* harmony export */   "X8": () => (/* binding */ toggleTheme),
/* harmony export */   "Gs": () => (/* binding */ updateUserFarmStakedOnly),
/* harmony export */   "mm": () => (/* binding */ updateUserPoolStakedOnly),
/* harmony export */   "d4": () => (/* binding */ updateUserPoolsViewMode),
/* harmony export */   "gk": () => (/* binding */ updateUserFarmsViewMode),
/* harmony export */   "RC": () => (/* binding */ updateUserPredictionAcceptedRisk),
/* harmony export */   "c4": () => (/* binding */ updateUserPredictionChartDisclaimerShow),
/* harmony export */   "_C": () => (/* binding */ updateUserExpertModeAcknowledgementShow),
/* harmony export */   "zk": () => (/* binding */ updateUserUsernameVisibility),
/* harmony export */   "dy": () => (/* binding */ updateGasPrice),
/* harmony export */   "zS": () => (/* binding */ addWatchlistToken),
/* harmony export */   "Dn": () => (/* binding */ addWatchlistPool),
/* harmony export */   "l5": () => (/* binding */ hidePhishingWarningBanner),
/* harmony export */   "hN": () => (/* binding */ setIsExchangeChartDisplayed),
/* harmony export */   "p9": () => (/* binding */ setChartViewMode),
/* harmony export */   "Hr": () => (/* binding */ setSubgraphHealthIndicatorDisplayed)
/* harmony export */ });
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5184);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);

var FarmStakedOnly;
(function(FarmStakedOnly) {
    FarmStakedOnly["ON_FINISHED"] = 'onFinished';
    FarmStakedOnly["TRUE"] = 'true';
    FarmStakedOnly["FALSE"] = 'false';
})(FarmStakedOnly || (FarmStakedOnly = {}));
var ViewMode;
(function(ViewMode) {
    ViewMode["TABLE"] = "TABLE";
    ViewMode["CARD"] = "CARD";
})(ViewMode || (ViewMode = {}));
var ChartViewMode;
(function(ChartViewMode) {
    ChartViewMode["BASIC"] = "BASIC";
    ChartViewMode["TRADING_VIEW"] = "TRADING_VIEW";
})(ChartViewMode || (ChartViewMode = {}));
const updateUserExpertMode = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/updateUserExpertMode');
const updateUserSingleHopOnly = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/updateUserSingleHopOnly');
const updateUserSlippageTolerance = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/updateUserSlippageTolerance');
const updateUserDeadline = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/updateUserDeadline');
const addSerializedToken = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/addSerializedToken');
const removeSerializedToken = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/removeSerializedToken');
const addSerializedPair = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/addSerializedPair');
const removeSerializedPair = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/removeSerializedPair');
const muteAudio = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/muteAudio');
const unmuteAudio = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/unmuteAudio');
const toggleTheme = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/toggleTheme');
const updateUserFarmStakedOnly = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/updateUserFarmStakedOnly');
const updateUserPoolStakedOnly = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/updateUserPoolStakedOnly');
const updateUserPoolsViewMode = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/updateUserPoolsViewMode');
const updateUserFarmsViewMode = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/updateUserFarmsViewMode');
const updateUserPredictionAcceptedRisk = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/updateUserPredictionAcceptedRisk');
const updateUserPredictionChartDisclaimerShow = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/updateUserPredictionChartDisclaimerShow');
const updateUserExpertModeAcknowledgementShow = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/updateUserExpertModeAcknowledgementShow');
const updateUserUsernameVisibility = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/updateUserUsernameVisibility');
const updateGasPrice = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/updateGasPrice');
const addWatchlistToken = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/addWatchlistToken');
const addWatchlistPool = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/addWatchlistPool');
const hidePhishingWarningBanner = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/hidePhishingWarningBanner');
const setIsExchangeChartDisplayed = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/toggleIsExchangeChartDisplayed');
const setChartViewMode = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/setChartViewMode');
const setSubgraphHealthIndicatorDisplayed = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createAction)('user/setSubgraphHealthIndicatorDisplayed');


/***/ }),

/***/ 787:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Mq": () => (/* binding */ serializeToken),
/* harmony export */   "iG": () => (/* binding */ deserializeToken),
/* harmony export */   "DB": () => (/* binding */ GAS_PRICE),
/* harmony export */   "j4": () => (/* binding */ GAS_PRICE_GWEI)
/* harmony export */ });
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(543);
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ethersproject_units__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3138);
/* harmony import */ var _ethersproject_units__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_ethersproject_units__WEBPACK_IMPORTED_MODULE_1__);


function serializeToken(token) {
    return {
        chainId: token.chainId,
        address: token.address,
        decimals: token.decimals,
        symbol: token.symbol,
        name: token.name,
        projectLink: token.projectLink
    };
}
function deserializeToken(serializedToken) {
    return new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token(serializedToken.chainId, serializedToken.address, serializedToken.decimals, serializedToken.symbol, serializedToken.name, serializedToken.projectLink);
}
var GAS_PRICE;
(function(GAS_PRICE) {
    GAS_PRICE["default"] = '5';
    GAS_PRICE["fast"] = '6';
    GAS_PRICE["instant"] = '7';
    GAS_PRICE["testnet"] = '10';
})(GAS_PRICE || (GAS_PRICE = {}));
const GAS_PRICE_GWEI = {
    default: (0,_ethersproject_units__WEBPACK_IMPORTED_MODULE_1__.parseUnits)(GAS_PRICE.default, 'gwei').toString(),
    fast: (0,_ethersproject_units__WEBPACK_IMPORTED_MODULE_1__.parseUnits)(GAS_PRICE.fast, 'gwei').toString(),
    instant: (0,_ethersproject_units__WEBPACK_IMPORTED_MODULE_1__.parseUnits)(GAS_PRICE.instant, 'gwei').toString(),
    testnet: (0,_ethersproject_units__WEBPACK_IMPORTED_MODULE_1__.parseUnits)(GAS_PRICE.testnet, 'gwei').toString()
};


/***/ }),

/***/ 5878:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Kn": () => (/* binding */ getAddress),
  "Jc": () => (/* binding */ getAnniversaryAchievement),
  "O9": () => (/* binding */ getCakeVaultAddress),
  "rp": () => (/* binding */ getIfoPoolAddress),
  "kN": () => (/* binding */ getLotteryV2Address),
  "Oc": () => (/* binding */ getMasterChefAddress),
  "I8": () => (/* binding */ getMulticallAddress),
  "Re": () => (/* binding */ getPancakeProfileAddress),
  "Df": () => (/* binding */ getPredictionsAddress)
});

// UNUSED EXPORTS: getBunnyFactoryAddress, getBunnySpecialAddress, getBunnySpecialCakeVaultAddress, getBunnySpecialLotteryAddress, getBunnySpecialPredictionAddress, getBunnySpecialXmasAddress, getChainlinkOracleAddress, getClaimRefundAddress, getEasterNftAddress, getFarmAuctionAddress, getNftMarketAddress, getNftSaleAddress, getPancakeRabbitsAddress, getPancakeSquadAddress, getPointCenterIfoAddress, getTradingCompetitionAddress, getTradingCompetitionAddressV2, getVaultPoolAddress

// EXTERNAL MODULE: external "@pancakeswap/sdk"
var sdk_ = __webpack_require__(543);
;// CONCATENATED MODULE: ./src/config/constants/contracts.ts
/* harmony default export */ const contracts = ({
    masterChef: {
        97: '0x1d32c2945C8FDCBc7156c553B7cEa4325a17f4f9',
        56: '0x73feaa1eE314F8c655E354234017bE2193C9E24E'
    },
    sousChef: {
        97: '0xd3af5fe61dbaf8f73149bfcfa9fb653ff096029a',
        56: '0x6ab8463a4185b80905e05a9ff80a2d6b714b9e95'
    },
    lotteryV2: {
        97: '0x5790c3534F30437641541a0FA04C992799602998',
        56: '0x5aF6D33DE2ccEC94efb1bDF8f92Bd58085432d2c'
    },
    multiCall: {
        56: '0xfF6FD90A470Aaa0c1B8A54681746b07AcdFedc9B',
        97: '0x8F3273Fb89B075b1645095ABaC6ed17B2d4Bc576'
    },
    pancakeProfile: {
        56: '0xDf4dBf6536201370F95e06A0F8a7a70fE40E388a',
        97: '0x4B683C7E13B6d5D7fd1FeA9530F451954c1A7c8A'
    },
    pancakeRabbits: {
        56: '0xDf7952B35f24aCF7fC0487D01c8d5690a60DBa07',
        97: '0x60935F36e4631F73f0f407e68642144e07aC7f5E'
    },
    bunnyFactory: {
        56: '0xfa249Caa1D16f75fa159F7DFBAc0cC5EaB48CeFf',
        97: '0x707CBF373175fdB601D34eeBF2Cf665d08f01148'
    },
    claimRefund: {
        56: '0xE7e53A7e9E3Cf6b840f167eF69519175c497e149',
        97: ''
    },
    pointCenterIfo: {
        56: '0x3C6919b132462C1FEc572c6300E83191f4F0012a',
        97: '0xd2Ac1B1728Bb1C11ae02AB6e75B76Ae41A2997e3'
    },
    bunnySpecial: {
        56: '0xFee8A195570a18461146F401d6033f5ab3380849',
        97: '0x7b7b1583De1DeB32Ce6605F6deEbF24A0671c17C'
    },
    tradingCompetition: {
        56: '0xd718baa0B1F4f70dcC8458154042120FFE0DEFFA',
        97: '0xC787F45B833721ED3aC46E99b703B3E1E01abb97'
    },
    tradingCompetitionV2: {
        56: '0xA8FECf847e28aa1Df39E995a45b7FCfb91b676d4',
        97: ''
    },
    easterNft: {
        56: '0x23c41D28A239dDCAABd1bb1deF8d057189510066',
        97: '0x24ec6962dbe874F6B67B5C50857565667fA0854F'
    },
    cakeVault: {
        56: '0xa80240Eb5d7E05d3F250cF000eEc0891d00b51CC',
        97: ''
    },
    ifoPool: {
        56: '0x1B2A2f6ed4A1401E8C73B4c2B6172455ce2f78E8',
        97: ''
    },
    predictions: {
        56: '0x18B2A687610328590Bc8F2e5fEdDe3b582A49cdA',
        97: ''
    },
    chainlinkOracle: {
        56: '0xD276fCF34D54A926773c399eBAa772C12ec394aC',
        97: ''
    },
    bunnySpecialCakeVault: {
        56: '0x5B4a770Abe7Eafb2601CA4dF9d73EA99363E60a4',
        97: ''
    },
    bunnySpecialPrediction: {
        56: '0x342c99e9aC24157657095eC69CB04b73257e7A9C',
        97: ''
    },
    bunnySpecialLottery: {
        56: '0x24ED31d31C5868e5a96aA77fdcB890f3511fa0b2',
        97: '0x382cB497110F398F0f152cae82821476AE51c9cF'
    },
    bunnySpecialXmas: {
        56: '0x59EdDF3c21509dA3b0aCCd7c5ccc596d930f4783',
        97: ''
    },
    farmAuction: {
        56: '0xb92Ab7c1edcb273AbA24b0656cEb3681654805D2',
        97: '0x3F9602593b4f7C67ab045DB51BbDEa94E40fA9Fe'
    },
    AnniversaryAchievement: {
        56: '0x787980da5491118C3cB33B21aB50c8c379D2C552',
        97: '0x981aE96378e770DE44F89cD9175E708f9EDB70a9'
    },
    nftMarket: {
        56: '0x17539cCa21C7933Df5c980172d22659B8C345C5A',
        97: '0x7f9f37ddcaa33893f9beb3d8748c8d6bfbde6ab2'
    },
    nftSale: {
        56: '0x29fE7148636b7Ae0b1E53777b28dfbaA9327af8E',
        97: '0xe486De509c5381cbdBF3e71F57D7F1f7570f5c46'
    },
    pancakeSquad: {
        56: '0x0a8901b0E25DEb55A87524f0cC164E9644020EBA',
        97: '0xfC0c3F11fDA72Cb9A56F28Ec8D44C0ae4B3ABF86'
    }
});

;// CONCATENATED MODULE: ./src/utils/addressHelpers.ts


const getAddress = (address)=>{
    const chainId = "56";
    return address[chainId] ? address[chainId] : address[sdk_.ChainId.MAINNET];
};
const getMasterChefAddress = ()=>{
    return getAddress(contracts.masterChef);
};
const getMulticallAddress = ()=>{
    return getAddress(contracts.multiCall);
};
const getLotteryV2Address = ()=>{
    return getAddress(contracts.lotteryV2);
};
const getPancakeProfileAddress = ()=>{
    return getAddress(contracts.pancakeProfile);
};
const getPancakeRabbitsAddress = ()=>{
    return getAddress(addresses.pancakeRabbits);
};
const getBunnyFactoryAddress = ()=>{
    return getAddress(addresses.bunnyFactory);
};
const getClaimRefundAddress = ()=>{
    return getAddress(addresses.claimRefund);
};
const getPointCenterIfoAddress = ()=>{
    return getAddress(addresses.pointCenterIfo);
};
const getBunnySpecialAddress = ()=>{
    return getAddress(addresses.bunnySpecial);
};
const getTradingCompetitionAddress = ()=>{
    return getAddress(addresses.tradingCompetition);
};
const getTradingCompetitionAddressV2 = ()=>{
    return getAddress(addresses.tradingCompetitionV2);
};
const getEasterNftAddress = ()=>{
    return getAddress(addresses.easterNft);
};
const getVaultPoolAddress = (vaultKey)=>{
    if (!vaultKey) {
        return null;
    }
    return getAddress(addresses[vaultKey]);
};
const getCakeVaultAddress = ()=>{
    return getAddress(contracts.cakeVault);
};
const getIfoPoolAddress = ()=>{
    return getAddress(contracts.ifoPool);
};
const getPredictionsAddress = ()=>{
    return getAddress(contracts.predictions);
};
const getChainlinkOracleAddress = ()=>{
    return getAddress(addresses.chainlinkOracle);
};
const getBunnySpecialCakeVaultAddress = ()=>{
    return getAddress(addresses.bunnySpecialCakeVault);
};
const getBunnySpecialPredictionAddress = ()=>{
    return getAddress(addresses.bunnySpecialPrediction);
};
const getBunnySpecialLotteryAddress = ()=>{
    return getAddress(addresses.bunnySpecialLottery);
};
const getBunnySpecialXmasAddress = ()=>{
    return getAddress(addresses.bunnySpecialXmas);
};
const getFarmAuctionAddress = ()=>{
    return getAddress(addresses.farmAuction);
};
const getAnniversaryAchievement = ()=>{
    return getAddress(contracts.AnniversaryAchievement);
};
const getNftMarketAddress = ()=>{
    return getAddress(addresses.nftMarket);
};
const getNftSaleAddress = ()=>{
    return getAddress(addresses.nftSale);
};
const getPancakeSquadAddress = ()=>{
    return getAddress(addresses.pancakeSquad);
};


/***/ }),

/***/ 5128:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HW": () => (/* binding */ BIG_ZERO),
/* harmony export */   "cQ": () => (/* binding */ BIG_ONE),
/* harmony export */   "xp": () => (/* binding */ BIG_TEN),
/* harmony export */   "L8": () => (/* binding */ ethersToSerializedBigNumber)
/* harmony export */ });
/* unused harmony exports BIG_NINE, ethersToBigNumber */
/* harmony import */ var bignumber_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4215);
/* harmony import */ var bignumber_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(bignumber_js__WEBPACK_IMPORTED_MODULE_0__);

const BIG_ZERO = new (bignumber_js__WEBPACK_IMPORTED_MODULE_0___default())(0);
const BIG_ONE = new (bignumber_js__WEBPACK_IMPORTED_MODULE_0___default())(1);
const BIG_NINE = new (bignumber_js__WEBPACK_IMPORTED_MODULE_0___default())(9);
const BIG_TEN = new (bignumber_js__WEBPACK_IMPORTED_MODULE_0___default())(10);
const ethersToSerializedBigNumber = (ethersBn)=>ethersToBigNumber(ethersBn).toJSON()
;
const ethersToBigNumber = (ethersBn)=>new (bignumber_js__WEBPACK_IMPORTED_MODULE_0___default())(ethersBn.toString())
;


/***/ }),

/***/ 1553:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "f$": () => (/* binding */ getAnniversaryAchievementContract),
  "XT": () => (/* binding */ getCakeContract),
  "bR": () => (/* binding */ getCakeVaultContract),
  "yd": () => (/* binding */ getLotteryV2Contract),
  "aE": () => (/* binding */ getMasterchefContract),
  "tV": () => (/* binding */ getMulticallContract),
  "qi": () => (/* binding */ getPredictionsContract),
  "Y4": () => (/* binding */ getProfileContract)
});

// UNUSED EXPORTS: getBep20Contract, getBunnyFactoryContract, getBunnySpecialCakeVaultContract, getBunnySpecialContract, getBunnySpecialLotteryContract, getBunnySpecialPredictionContract, getBunnySpecialXmasContract, getChainlinkOracleContract, getClaimRefundContract, getEasterNftContract, getErc721CollectionContract, getErc721Contract, getFarmAuctionContract, getIfoPoolContract, getIfoV1Contract, getIfoV2Contract, getLpContract, getNftMarketContract, getNftSaleContract, getPancakeRabbitContract, getPancakeSquadContract, getPointCenterIfoContract, getSouschefContract, getSouschefV2Contract, getTradingCompetitionContract, getTradingCompetitionContractV2

// EXTERNAL MODULE: external "@ethersproject/contracts"
var contracts_ = __webpack_require__(2792);
// EXTERNAL MODULE: ./src/utils/providers.ts
var providers = __webpack_require__(5922);
// EXTERNAL MODULE: ./src/config/constants/index.ts
var constants = __webpack_require__(3862);
// EXTERNAL MODULE: ./src/config/constants/types.ts
var types = __webpack_require__(7971);
// EXTERNAL MODULE: ./src/config/constants/tokens.ts
var tokens = __webpack_require__(9748);
// EXTERNAL MODULE: ./src/utils/addressHelpers.ts + 1 modules
var addressHelpers = __webpack_require__(5878);
// EXTERNAL MODULE: ./src/config/abi/pancakeProfile.json
var pancakeProfile = __webpack_require__(2335);
;// CONCATENATED MODULE: ./src/config/abi/pancakeRabbits.json
const pancakeRabbits_namespaceObject = [];
;// CONCATENATED MODULE: ./src/config/abi/bunnyFactory.json
const bunnyFactory_namespaceObject = [];
;// CONCATENATED MODULE: ./src/config/abi/bunnySpecial.json
const bunnySpecial_namespaceObject = [];
// EXTERNAL MODULE: ./src/config/abi/erc20.json
var erc20 = __webpack_require__(3324);
// EXTERNAL MODULE: ./src/config/abi/erc721.json
var erc721 = __webpack_require__(3400);
;// CONCATENATED MODULE: ./src/config/abi/lpToken.json
const lpToken_namespaceObject = [];
// EXTERNAL MODULE: ./src/config/abi/cake.json
var cake = __webpack_require__(3361);
;// CONCATENATED MODULE: ./src/config/abi/ifoV1.json
const ifoV1_namespaceObject = [];
;// CONCATENATED MODULE: ./src/config/abi/ifoV2.json
const ifoV2_namespaceObject = [];
// EXTERNAL MODULE: ./src/config/abi/pointCenterIfo.json
var abi_pointCenterIfo = __webpack_require__(8750);
// EXTERNAL MODULE: ./src/config/abi/lotteryV2.json
var lotteryV2 = __webpack_require__(8592);
// EXTERNAL MODULE: ./src/config/abi/masterchef.json
var masterchef = __webpack_require__(4951);
// EXTERNAL MODULE: ./src/config/abi/sousChef.json
var abi_sousChef = __webpack_require__(6477);
// EXTERNAL MODULE: ./src/config/abi/sousChefV2.json
var abi_sousChefV2 = __webpack_require__(9022);
;// CONCATENATED MODULE: ./src/config/abi/sousChefBnb.json
const sousChefBnb_namespaceObject = [];
;// CONCATENATED MODULE: ./src/config/abi/claimRefund.json
const claimRefund_namespaceObject = [];
;// CONCATENATED MODULE: ./src/config/abi/tradingCompetition.json
const tradingCompetition_namespaceObject = [];
;// CONCATENATED MODULE: ./src/config/abi/tradingCompetitionV2.json
const tradingCompetitionV2_namespaceObject = [];
;// CONCATENATED MODULE: ./src/config/abi/easterNft.json
const easterNft_namespaceObject = [];
// EXTERNAL MODULE: ./src/config/abi/cakeVault.json
var cakeVault = __webpack_require__(1430);
// EXTERNAL MODULE: ./src/config/abi/ifoPool.json
var ifoPool = __webpack_require__(9704);
// EXTERNAL MODULE: ./src/config/abi/predictions.json
var predictions = __webpack_require__(5883);
;// CONCATENATED MODULE: ./src/config/abi/chainlinkOracle.json
const chainlinkOracle_namespaceObject = [];
// EXTERNAL MODULE: ./src/config/abi/Multicall.json
var Multicall = __webpack_require__(3373);
;// CONCATENATED MODULE: ./src/config/abi/bunnySpecialCakeVault.json
const bunnySpecialCakeVault_namespaceObject = [];
;// CONCATENATED MODULE: ./src/config/abi/bunnySpecialPrediction.json
const bunnySpecialPrediction_namespaceObject = [];
;// CONCATENATED MODULE: ./src/config/abi/bunnySpecialLottery.json
const bunnySpecialLottery_namespaceObject = [];
;// CONCATENATED MODULE: ./src/config/abi/bunnySpecialXmas.json
const bunnySpecialXmas_namespaceObject = [];
;// CONCATENATED MODULE: ./src/config/abi/farmAuction.json
const farmAuction_namespaceObject = [];
;// CONCATENATED MODULE: ./src/config/abi/anniversaryAchievement.json
const anniversaryAchievement_namespaceObject = JSON.parse('[{"inputs":[{"internalType":"address","name":"_pancakeProfile","type":"address"},{"internalType":"uint256","name":"_numberPoints","type":"uint256"},{"internalType":"uint256","name":"_thresholdPoints","type":"uint256"},{"internalType":"uint256","name":"_campaignId","type":"uint256"},{"internalType":"uint256","name":"_endBlock","type":"uint256"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"campaignId","type":"uint256"}],"name":"NewCampaignId","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"endBlock","type":"uint256"}],"name":"NewEndBlock","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"numberPoints","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"thresholdPoints","type":"uint256"}],"name":"NewNumberPointsAndThreshold","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"inputs":[],"name":"campaignId","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_user","type":"address"}],"name":"canClaim","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_campaignId","type":"uint256"}],"name":"changeCampaignId","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_endBlock","type":"uint256"}],"name":"changeEndBlock","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_numberPoints","type":"uint256"},{"internalType":"uint256","name":"_thresholdPoints","type":"uint256"}],"name":"changeNumberPointsAndThreshold","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"claimAnniversaryPoints","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"endBlock","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"hasClaimed","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"numberPoints","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"pancakeProfile","outputs":[{"internalType":"contract PancakeProfile","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"thresholdPoints","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"}]');
;// CONCATENATED MODULE: ./src/config/abi/nftMarket.json
const nftMarket_namespaceObject = [];
;// CONCATENATED MODULE: ./src/config/abi/nftSale.json
const nftSale_namespaceObject = [];
;// CONCATENATED MODULE: ./src/config/abi/pancakeSquad.json
const pancakeSquad_namespaceObject = [];
;// CONCATENATED MODULE: ./src/config/abi/erc721collection.json
const erc721collection_namespaceObject = [];
;// CONCATENATED MODULE: ./src/utils/contractHelpers.ts





// Addresses

// ABI



































const getContract = (abi, address, signer)=>{
    const signerOrProvider = signer ?? providers/* simpleRpcProvider */.J;
    return new contracts_.Contract(address, abi, signerOrProvider);
};
const getBep20Contract = (address, signer)=>{
    return getContract(bep20Abi, address, signer);
};
const getErc721Contract = (address, signer)=>{
    return getContract(erc721Abi, address, signer);
};
const getLpContract = (address, signer)=>{
    return getContract(lpTokenAbi, address, signer);
};
const getIfoV1Contract = (address, signer)=>{
    return getContract(ifoV1Abi, address, signer);
};
const getIfoV2Contract = (address, signer)=>{
    return getContract(ifoV2Abi, address, signer);
};
const getSouschefContract = (id, signer)=>{
    const config = poolsConfig.find((pool)=>pool.sousId === id
    );
    const abi = config.poolCategory === PoolCategory.BINANCE ? sousChefBnb : sousChef;
    return getContract(abi, getAddress(config.contractAddress), signer);
};
const getSouschefV2Contract = (id, signer)=>{
    const config = poolsConfig.find((pool)=>pool.sousId === id
    );
    return getContract(sousChefV2, getAddress(config.contractAddress), signer);
};
const getPointCenterIfoContract = (signer)=>{
    return getContract(pointCenterIfo, getPointCenterIfoAddress(), signer);
};
const getCakeContract = (signer)=>{
    return getContract(cake, tokens/* default.cake.address */.ZP.cake.address, signer);
};
const getProfileContract = (signer)=>{
    return getContract(pancakeProfile, (0,addressHelpers/* getPancakeProfileAddress */.Re)(), signer);
};
const getPancakeRabbitContract = (signer)=>{
    return getContract(pancakeRabbitsAbi, getPancakeRabbitsAddress(), signer);
};
const getBunnyFactoryContract = (signer)=>{
    return getContract(bunnyFactoryAbi, getBunnyFactoryAddress(), signer);
};
const getBunnySpecialContract = (signer)=>{
    return getContract(bunnySpecialAbi, getBunnySpecialAddress(), signer);
};
const getLotteryV2Contract = (signer)=>{
    return getContract(lotteryV2, (0,addressHelpers/* getLotteryV2Address */.kN)(), signer);
};
const getMasterchefContract = (signer)=>{
    return getContract(masterchef, (0,addressHelpers/* getMasterChefAddress */.Oc)(), signer);
};
const getClaimRefundContract = (signer)=>{
    return getContract(claimRefundAbi, getClaimRefundAddress(), signer);
};
const getTradingCompetitionContract = (signer)=>{
    return getContract(tradingCompetitionAbi, getTradingCompetitionAddress(), signer);
};
const getTradingCompetitionContractV2 = (signer)=>{
    return getContract(tradingCompetitionV2Abi, getTradingCompetitionAddressV2(), signer);
};
const getEasterNftContract = (signer)=>{
    return getContract(easterNftAbi, getEasterNftAddress(), signer);
};
const getCakeVaultContract = (signer)=>{
    return getContract(cakeVault, (0,addressHelpers/* getCakeVaultAddress */.O9)(), signer);
};
const getIfoPoolContract = (signer)=>{
    return getContract(ifoPoolAbi, getIfoPoolAddress(), signer);
};
const getPredictionsContract = (signer)=>{
    return getContract(predictions, (0,addressHelpers/* getPredictionsAddress */.Df)(), signer);
};
const getChainlinkOracleContract = (signer)=>{
    return getContract(chainlinkOracleAbi, getChainlinkOracleAddress(), signer);
};
const getMulticallContract = (signer)=>{
    return getContract(Multicall, (0,addressHelpers/* getMulticallAddress */.I8)(), signer);
};
const getBunnySpecialCakeVaultContract = (signer)=>{
    return getContract(bunnySpecialCakeVaultAbi, getBunnySpecialCakeVaultAddress(), signer);
};
const getBunnySpecialPredictionContract = (signer)=>{
    return getContract(bunnySpecialPredictionAbi, getBunnySpecialPredictionAddress(), signer);
};
const getBunnySpecialLotteryContract = (signer)=>{
    return getContract(bunnySpecialLotteryAbi, getBunnySpecialLotteryAddress(), signer);
};
const getBunnySpecialXmasContract = (signer)=>{
    return getContract(bunnySpecialXmasAbi, getBunnySpecialXmasAddress(), signer);
};
const getFarmAuctionContract = (signer)=>{
    return getContract(farmAuctionAbi, getFarmAuctionAddress(), signer);
};
const getAnniversaryAchievementContract = (signer)=>{
    return getContract(anniversaryAchievement_namespaceObject, (0,addressHelpers/* getAnniversaryAchievement */.Jc)(), signer);
};
const getNftMarketContract = (signer)=>{
    return getContract(nftMarketAbi, getNftMarketAddress(), signer);
};
const getNftSaleContract = (signer)=>{
    return getContract(nftSaleAbi, getNftSaleAddress(), signer);
};
const getPancakeSquadContract = (signer)=>{
    return getContract(pancakeSquadAbi, getPancakeSquadAddress(), signer);
};
const getErc721CollectionContract = (signer, address)=>{
    return getContract(erc721CollectionAbi, address, signer);
};


/***/ }),

/***/ 5044:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Qe": () => (/* binding */ getDecimalAmount),
/* harmony export */   "mW": () => (/* binding */ getBalanceNumber),
/* harmony export */   "NJ": () => (/* binding */ getFullDisplayBalance),
/* harmony export */   "dp": () => (/* binding */ formatBigNumber)
/* harmony export */ });
/* unused harmony exports getBalanceAmount, formatNumber, formatBigNumberToFixed, formatFixedNumber, formatLocalisedCompactNumber */
/* harmony import */ var bignumber_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4215);
/* harmony import */ var bignumber_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(bignumber_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ethersproject_bignumber__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5757);
/* harmony import */ var _ethersproject_bignumber__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_ethersproject_bignumber__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ethersproject_units__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3138);
/* harmony import */ var _ethersproject_units__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_ethersproject_units__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var contexts_Localization_helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5290);
/* harmony import */ var _bigNumber__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5128);





/**
 * Take a formatted amount, e.g. 15 BNB and convert it to full decimal value, e.g. 15000000000000000
 */ const getDecimalAmount = (amount, decimals = 18)=>{
    return new (bignumber_js__WEBPACK_IMPORTED_MODULE_0___default())(amount).times(_bigNumber__WEBPACK_IMPORTED_MODULE_4__/* .BIG_TEN.pow */ .xp.pow(decimals));
};
const getBalanceAmount = (amount, decimals = 18)=>{
    return new (bignumber_js__WEBPACK_IMPORTED_MODULE_0___default())(amount).dividedBy(_bigNumber__WEBPACK_IMPORTED_MODULE_4__/* .BIG_TEN.pow */ .xp.pow(decimals));
};
/**
 * This function is not really necessary but is used throughout the site.
 */ const getBalanceNumber = (balance, decimals = 18)=>{
    return getBalanceAmount(balance, decimals).toNumber();
};
const getFullDisplayBalance = (balance, decimals = 18, displayDecimals)=>{
    return getBalanceAmount(balance, decimals).toFixed(displayDecimals);
};
const formatNumber = (number, minPrecision = 2, maxPrecision = 2)=>{
    const options = {
        minimumFractionDigits: minPrecision,
        maximumFractionDigits: maxPrecision
    };
    return number.toLocaleString(undefined, options);
};
/**
 * Method to format the display of wei given an EthersBigNumber object
 * Note: does NOT round
 */ const formatBigNumber = (number, displayDecimals = 18, decimals = 18)=>{
    const remainder = number.mod(_ethersproject_bignumber__WEBPACK_IMPORTED_MODULE_1__.BigNumber.from(10).pow(decimals - displayDecimals));
    return (0,_ethersproject_units__WEBPACK_IMPORTED_MODULE_2__.formatUnits)(number.sub(remainder), decimals);
};
/**
 * Method to format the display of wei given an EthersBigNumber object with toFixed
 * Note: rounds
 */ const formatBigNumberToFixed = (number, displayDecimals = 18, decimals = 18)=>{
    const formattedString = formatUnits(number, decimals);
    return (+formattedString).toFixed(displayDecimals);
};
/**
 * Formats a FixedNumber like BigNumber
 * i.e. Formats 9763410526137450427.1196 into 9.763 (3 display decimals)
 */ const formatFixedNumber = (number, displayDecimals = 18, decimals = 18)=>{
    // Remove decimal
    const [leftSide] = number.toString().split('.');
    return formatBigNumber(EthersBigNumber.from(leftSide), displayDecimals, decimals);
};
const formatLocalisedCompactNumber = (number)=>{
    const codeFromStorage = getLanguageCodeFromLS();
    return new Intl.NumberFormat(codeFromStorage, {
        notation: 'compact',
        compactDisplay: 'long',
        maximumSignificantDigits: 2
    }).format(number);
};
/* unused harmony default export */ var __WEBPACK_DEFAULT_EXPORT__ = ((/* unused pure expression or super */ null && (formatLocalisedCompactNumber)));


/***/ }),

/***/ 8328:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "P4": () => (/* binding */ basisPointsToPercent),
  "yC": () => (/* binding */ calculateGasMargin),
  "hr": () => (/* binding */ escapeRegExp),
  "s6": () => (/* binding */ getBscScanLink),
  "uN": () => (/* binding */ getContract),
  "TY": () => (/* binding */ getProviderOrSigner),
  "iY": () => (/* binding */ getRouterContract),
  "UJ": () => (/* binding */ isAddress),
  "wK": () => (/* binding */ isTokenOnList)
});

// UNUSED EXPORTS: calculateSlippageAmount, getBscScanLinkForNft, getSigner

// EXTERNAL MODULE: external "@ethersproject/contracts"
var contracts_ = __webpack_require__(2792);
// EXTERNAL MODULE: external "@ethersproject/address"
var address_ = __webpack_require__(1541);
// EXTERNAL MODULE: external "@ethersproject/constants"
var constants_ = __webpack_require__(6644);
// EXTERNAL MODULE: external "@ethersproject/bignumber"
var bignumber_ = __webpack_require__(5757);
;// CONCATENATED MODULE: ./src/config/abi/IPancakeRouter02.json
const IPancakeRouter02_namespaceObject = JSON.parse('[{"inputs":[],"name":"WETH","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"pure","type":"function"},{"inputs":[{"internalType":"address","name":"tokenA","type":"address"},{"internalType":"address","name":"tokenB","type":"address"},{"internalType":"uint256","name":"amountADesired","type":"uint256"},{"internalType":"uint256","name":"amountBDesired","type":"uint256"},{"internalType":"uint256","name":"amountAMin","type":"uint256"},{"internalType":"uint256","name":"amountBMin","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"addLiquidity","outputs":[{"internalType":"uint256","name":"amountA","type":"uint256"},{"internalType":"uint256","name":"amountB","type":"uint256"},{"internalType":"uint256","name":"liquidity","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"token","type":"address"},{"internalType":"uint256","name":"amountTokenDesired","type":"uint256"},{"internalType":"uint256","name":"amountTokenMin","type":"uint256"},{"internalType":"uint256","name":"amountETHMin","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"addLiquidityETH","outputs":[{"internalType":"uint256","name":"amountToken","type":"uint256"},{"internalType":"uint256","name":"amountETH","type":"uint256"},{"internalType":"uint256","name":"liquidity","type":"uint256"}],"stateMutability":"payable","type":"function"},{"inputs":[],"name":"factory","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"pure","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountOut","type":"uint256"},{"internalType":"uint256","name":"reserveIn","type":"uint256"},{"internalType":"uint256","name":"reserveOut","type":"uint256"}],"name":"getAmountIn","outputs":[{"internalType":"uint256","name":"amountIn","type":"uint256"}],"stateMutability":"pure","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountIn","type":"uint256"},{"internalType":"uint256","name":"reserveIn","type":"uint256"},{"internalType":"uint256","name":"reserveOut","type":"uint256"}],"name":"getAmountOut","outputs":[{"internalType":"uint256","name":"amountOut","type":"uint256"}],"stateMutability":"pure","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountOut","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"}],"name":"getAmountsIn","outputs":[{"internalType":"uint256[]","name":"amounts","type":"uint256[]"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountIn","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"}],"name":"getAmountsOut","outputs":[{"internalType":"uint256[]","name":"amounts","type":"uint256[]"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountA","type":"uint256"},{"internalType":"uint256","name":"reserveA","type":"uint256"},{"internalType":"uint256","name":"reserveB","type":"uint256"}],"name":"quote","outputs":[{"internalType":"uint256","name":"amountB","type":"uint256"}],"stateMutability":"pure","type":"function"},{"inputs":[{"internalType":"address","name":"tokenA","type":"address"},{"internalType":"address","name":"tokenB","type":"address"},{"internalType":"uint256","name":"liquidity","type":"uint256"},{"internalType":"uint256","name":"amountAMin","type":"uint256"},{"internalType":"uint256","name":"amountBMin","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"removeLiquidity","outputs":[{"internalType":"uint256","name":"amountA","type":"uint256"},{"internalType":"uint256","name":"amountB","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"token","type":"address"},{"internalType":"uint256","name":"liquidity","type":"uint256"},{"internalType":"uint256","name":"amountTokenMin","type":"uint256"},{"internalType":"uint256","name":"amountETHMin","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"removeLiquidityETH","outputs":[{"internalType":"uint256","name":"amountToken","type":"uint256"},{"internalType":"uint256","name":"amountETH","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"token","type":"address"},{"internalType":"uint256","name":"liquidity","type":"uint256"},{"internalType":"uint256","name":"amountTokenMin","type":"uint256"},{"internalType":"uint256","name":"amountETHMin","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"removeLiquidityETHSupportingFeeOnTransferTokens","outputs":[{"internalType":"uint256","name":"amountETH","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"token","type":"address"},{"internalType":"uint256","name":"liquidity","type":"uint256"},{"internalType":"uint256","name":"amountTokenMin","type":"uint256"},{"internalType":"uint256","name":"amountETHMin","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"},{"internalType":"bool","name":"approveMax","type":"bool"},{"internalType":"uint8","name":"v","type":"uint8"},{"internalType":"bytes32","name":"r","type":"bytes32"},{"internalType":"bytes32","name":"s","type":"bytes32"}],"name":"removeLiquidityETHWithPermit","outputs":[{"internalType":"uint256","name":"amountToken","type":"uint256"},{"internalType":"uint256","name":"amountETH","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"token","type":"address"},{"internalType":"uint256","name":"liquidity","type":"uint256"},{"internalType":"uint256","name":"amountTokenMin","type":"uint256"},{"internalType":"uint256","name":"amountETHMin","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"},{"internalType":"bool","name":"approveMax","type":"bool"},{"internalType":"uint8","name":"v","type":"uint8"},{"internalType":"bytes32","name":"r","type":"bytes32"},{"internalType":"bytes32","name":"s","type":"bytes32"}],"name":"removeLiquidityETHWithPermitSupportingFeeOnTransferTokens","outputs":[{"internalType":"uint256","name":"amountETH","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"tokenA","type":"address"},{"internalType":"address","name":"tokenB","type":"address"},{"internalType":"uint256","name":"liquidity","type":"uint256"},{"internalType":"uint256","name":"amountAMin","type":"uint256"},{"internalType":"uint256","name":"amountBMin","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"},{"internalType":"bool","name":"approveMax","type":"bool"},{"internalType":"uint8","name":"v","type":"uint8"},{"internalType":"bytes32","name":"r","type":"bytes32"},{"internalType":"bytes32","name":"s","type":"bytes32"}],"name":"removeLiquidityWithPermit","outputs":[{"internalType":"uint256","name":"amountA","type":"uint256"},{"internalType":"uint256","name":"amountB","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountOut","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"swapETHForExactTokens","outputs":[{"internalType":"uint256[]","name":"amounts","type":"uint256[]"}],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountOutMin","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"swapExactETHForTokens","outputs":[{"internalType":"uint256[]","name":"amounts","type":"uint256[]"}],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountOutMin","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"swapExactETHForTokensSupportingFeeOnTransferTokens","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountIn","type":"uint256"},{"internalType":"uint256","name":"amountOutMin","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"swapExactTokensForETH","outputs":[{"internalType":"uint256[]","name":"amounts","type":"uint256[]"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountIn","type":"uint256"},{"internalType":"uint256","name":"amountOutMin","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"swapExactTokensForETHSupportingFeeOnTransferTokens","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountIn","type":"uint256"},{"internalType":"uint256","name":"amountOutMin","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"swapExactTokensForTokens","outputs":[{"internalType":"uint256[]","name":"amounts","type":"uint256[]"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountIn","type":"uint256"},{"internalType":"uint256","name":"amountOutMin","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"swapExactTokensForTokensSupportingFeeOnTransferTokens","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountOut","type":"uint256"},{"internalType":"uint256","name":"amountInMax","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"swapTokensForExactETH","outputs":[{"internalType":"uint256[]","name":"amounts","type":"uint256[]"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amountOut","type":"uint256"},{"internalType":"uint256","name":"amountInMax","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"deadline","type":"uint256"}],"name":"swapTokensForExactTokens","outputs":[{"internalType":"uint256[]","name":"amounts","type":"uint256[]"}],"stateMutability":"nonpayable","type":"function"}]');
// EXTERNAL MODULE: external "@pancakeswap/sdk"
var sdk_ = __webpack_require__(543);
// EXTERNAL MODULE: ./src/config/constants/index.ts
var constants = __webpack_require__(3862);
// EXTERNAL MODULE: ./src/config/index.ts
var config = __webpack_require__(3206);
// EXTERNAL MODULE: ./src/utils/providers.ts
var providers = __webpack_require__(5922);
;// CONCATENATED MODULE: ./src/utils/index.ts









// returns the checksummed address if the address is valid, otherwise returns false
function isAddress(value) {
    try {
        return (0,address_.getAddress)(value);
    } catch  {
        return false;
    }
}
function getBscScanLink(data, type, chainId = sdk_.ChainId.MAINNET) {
    switch(type){
        case 'transaction':
            {
                return `${config/* BASE_BSC_SCAN_URLS */.st[chainId]}/tx/${data}`;
            }
        case 'token':
            {
                return `${config/* BASE_BSC_SCAN_URLS */.st[chainId]}/token/${data}`;
            }
        case 'block':
            {
                return `${config/* BASE_BSC_SCAN_URLS */.st[chainId]}/block/${data}`;
            }
        case 'countdown':
            {
                return `${config/* BASE_BSC_SCAN_URLS */.st[chainId]}/block/countdown/${data}`;
            }
        default:
            {
                return `${config/* BASE_BSC_SCAN_URLS */.st[chainId]}/address/${data}`;
            }
    }
}
function getBscScanLinkForNft(collectionAddress, tokenId, chainId = ChainId.MAINNET) {
    return `${BASE_BSC_SCAN_URLS[chainId]}/token/${collectionAddress}?a=${tokenId}`;
}
// add 10%
function calculateGasMargin(value) {
    return value.mul(bignumber_.BigNumber.from(10000).add(bignumber_.BigNumber.from(1000))).div(bignumber_.BigNumber.from(10000));
}
// converts a basis points value to a sdk percent
function basisPointsToPercent(num) {
    return new sdk_.Percent(sdk_.JSBI.BigInt(num), sdk_.JSBI.BigInt(10000));
}
function calculateSlippageAmount(value, slippage) {
    if (slippage < 0 || slippage > 10000) {
        throw Error(`Unexpected slippage value: ${slippage}`);
    }
    return [
        JSBI.divide(JSBI.multiply(value.raw, JSBI.BigInt(10000 - slippage)), JSBI.BigInt(10000)),
        JSBI.divide(JSBI.multiply(value.raw, JSBI.BigInt(10000 + slippage)), JSBI.BigInt(10000)), 
    ];
}
// account is not optional
function getSigner(library, account) {
    return library.getSigner(account).connectUnchecked();
}
// account is optional
function getProviderOrSigner(library, account) {
    return account ? getSigner(library, account) : library;
}
// account is optional
function getContract(address, ABI, signer) {
    if (!isAddress(address) || address === constants_.AddressZero) {
        throw Error(`Invalid 'address' parameter '${address}'.`);
    }
    return new contracts_.Contract(address, ABI, signer ?? providers/* simpleRpcProvider */.J);
}
// account is optional
function getRouterContract(_, library, account) {
    return getContract(constants/* ROUTER_ADDRESS */.bR, IPancakeRouter02_namespaceObject, getProviderOrSigner(library, account));
}
function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&') // $& means the whole matched string
    ;
}
function isTokenOnList(defaultTokens, currency) {
    if (currency === sdk_.ETHER) return true;
    return Boolean(currency instanceof sdk_.Token && defaultTokens[currency.chainId]?.[currency.address]);
}


/***/ }),

/***/ 1144:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "v": () => (/* binding */ multicallv2),
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _ethersproject_abi__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6187);
/* harmony import */ var _ethersproject_abi__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_ethersproject_abi__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var utils_contractHelpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1553);


const multicall = async (abi, calls)=>{
    const multi = (0,utils_contractHelpers__WEBPACK_IMPORTED_MODULE_1__/* .getMulticallContract */ .tV)();
    const itf = new _ethersproject_abi__WEBPACK_IMPORTED_MODULE_0__.Interface(abi);
    const calldata = calls.map((call)=>({
            target: call.address.toLowerCase(),
            callData: itf.encodeFunctionData(call.name, call.params)
        })
    );
    const { returnData  } = await multi.aggregate(calldata);
    const res = returnData.map((call, i)=>itf.decodeFunctionResult(calls[i].name, call)
    );
    return res;
};
/**
 * Multicall V2 uses the new "tryAggregate" function. It is different in 2 ways
 *
 * 1. If "requireSuccess" is false multicall will not bail out if one of the calls fails
 * 2. The return includes a boolean whether the call was successful e.g. [wasSuccessful, callResult]
 */ const multicallv2 = async (abi, calls, options = {
    requireSuccess: true
})=>{
    const { requireSuccess  } = options;
    const multi = (0,utils_contractHelpers__WEBPACK_IMPORTED_MODULE_1__/* .getMulticallContract */ .tV)();
    const itf = new _ethersproject_abi__WEBPACK_IMPORTED_MODULE_0__.Interface(abi);
    const calldata = calls.map((call)=>({
            target: call.address.toLowerCase(),
            callData: itf.encodeFunctionData(call.name, call.params)
        })
    );
    const returnData = await multi.tryAggregate(requireSuccess, calldata);
    const res = returnData.map((call, i)=>{
        const [result, data] = call;
        return result ? itf.decodeFunctionResult(calls[i].name, data) : null;
    });
    return res;
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (multicall);


/***/ }),

/***/ 7879:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "U7": () => (/* binding */ computeTradePriceBreakdown),
/* harmony export */   "b5": () => (/* binding */ computeSlippageAdjustedAmounts),
/* harmony export */   "oX": () => (/* binding */ warningSeverity),
/* harmony export */   "Kh": () => (/* binding */ formatExecutionPrice)
/* harmony export */ });
/* unused harmony export multiplyPriceByAmount */
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(543);
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _config_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3862);
/* harmony import */ var _state_swap_actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(193);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8328);




const BASE_FEE = new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Percent(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.BigInt(25), _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.BigInt(10000));
const ONE_HUNDRED_PERCENT = new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Percent(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.BigInt(10000), _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.JSBI.BigInt(10000));
const INPUT_FRACTION_AFTER_FEE = ONE_HUNDRED_PERCENT.subtract(BASE_FEE);
// computes price breakdown for the trade
function computeTradePriceBreakdown(trade) {
    // for each hop in our trade, take away the x*y=k price impact from 0.3% fees
    // e.g. for 3 tokens/2 hops: 1 - ((1 - .03) * (1-.03))
    const realizedLPFee = !trade ? undefined : ONE_HUNDRED_PERCENT.subtract(trade.route.pairs.reduce((currentFee)=>currentFee.multiply(INPUT_FRACTION_AFTER_FEE)
    , ONE_HUNDRED_PERCENT));
    // remove lp fees from price impact
    const priceImpactWithoutFeeFraction = trade && realizedLPFee ? trade.priceImpact.subtract(realizedLPFee) : undefined;
    // the x*y=k impact
    const priceImpactWithoutFeePercent = priceImpactWithoutFeeFraction ? new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Percent(priceImpactWithoutFeeFraction?.numerator, priceImpactWithoutFeeFraction?.denominator) : undefined;
    // the amount of the input that accrues to LPs
    const realizedLPFeeAmount = realizedLPFee && trade && (trade.inputAmount instanceof _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.TokenAmount ? new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.TokenAmount(trade.inputAmount.token, realizedLPFee.multiply(trade.inputAmount.raw).quotient) : _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.CurrencyAmount.ether(realizedLPFee.multiply(trade.inputAmount.raw).quotient));
    return {
        priceImpactWithoutFee: priceImpactWithoutFeePercent,
        realizedLPFee: realizedLPFeeAmount
    };
}
// computes the minimum amount out and maximum amount in for a trade given a user specified allowed slippage in bips
function computeSlippageAdjustedAmounts(trade, allowedSlippage) {
    const pct = (0,_index__WEBPACK_IMPORTED_MODULE_3__/* .basisPointsToPercent */ .P4)(allowedSlippage);
    return {
        [_state_swap_actions__WEBPACK_IMPORTED_MODULE_2__/* .Field.INPUT */ .gN.INPUT]: trade?.maximumAmountIn(pct),
        [_state_swap_actions__WEBPACK_IMPORTED_MODULE_2__/* .Field.OUTPUT */ .gN.OUTPUT]: trade?.minimumAmountOut(pct)
    };
}
function warningSeverity(priceImpact) {
    if (!priceImpact?.lessThan(_config_constants__WEBPACK_IMPORTED_MODULE_1__/* .BLOCKED_PRICE_IMPACT_NON_EXPERT */ .lN)) return 4;
    if (!priceImpact?.lessThan(_config_constants__WEBPACK_IMPORTED_MODULE_1__/* .ALLOWED_PRICE_IMPACT_HIGH */ .Uf)) return 3;
    if (!priceImpact?.lessThan(_config_constants__WEBPACK_IMPORTED_MODULE_1__/* .ALLOWED_PRICE_IMPACT_MEDIUM */ .p9)) return 2;
    if (!priceImpact?.lessThan(_config_constants__WEBPACK_IMPORTED_MODULE_1__/* .ALLOWED_PRICE_IMPACT_LOW */ .Bz)) return 1;
    return 0;
}
function formatExecutionPrice(trade, inverted) {
    if (!trade) {
        return '';
    }
    return inverted ? `${trade.executionPrice.invert().toSignificant(6)} ${trade.inputAmount.currency.symbol} / ${trade.outputAmount.currency.symbol}` : `${trade.executionPrice.toSignificant(6)} ${trade.outputAmount.currency.symbol} / ${trade.inputAmount.currency.symbol}`;
}
/**
 * Helper to multiply a Price object by an arbitrary amount
 */ const multiplyPriceByAmount = (price, amount, significantDigits = 18)=>{
    if (!price) {
        return 0;
    }
    return parseFloat(price.toSignificant(significantDigits)) * amount;
};


/***/ }),

/***/ 5922:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "J": () => (/* binding */ simpleRpcProvider)
/* harmony export */ });
/* harmony import */ var _ethersproject_providers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(399);
/* harmony import */ var _ethersproject_providers__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_ethersproject_providers__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var utils_getRpcUrl__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7308);


const RPC_URL = (0,utils_getRpcUrl__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z)();
const simpleRpcProvider = new _ethersproject_providers__WEBPACK_IMPORTED_MODULE_0__.StaticJsonRpcProvider(RPC_URL);
/* unused harmony default export */ var __WEBPACK_DEFAULT_EXPORT__ = (null);


/***/ }),

/***/ 3854:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "pu": () => (/* binding */ wrappedCurrency),
/* harmony export */   "Bv": () => (/* binding */ unwrappedToken)
/* harmony export */ });
/* unused harmony export wrappedCurrencyAmount */
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(543);
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__);

function wrappedCurrency(currency, chainId) {
    return chainId && currency === _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ETHER ? _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.WETH[chainId] : currency instanceof _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.Token ? currency : undefined;
}
function wrappedCurrencyAmount(currencyAmount, chainId) {
    const token = currencyAmount && chainId ? wrappedCurrency(currencyAmount.currency, chainId) : undefined;
    return token && currencyAmount ? new TokenAmount(token, currencyAmount.raw) : undefined;
}
function unwrappedToken(token) {
    if (token.equals(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.WETH[token.chainId])) return _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ETHER;
    return token;
}


/***/ }),

/***/ 1940:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Vf": () => (/* binding */ nftsBaseUrl),
  "Jr": () => (/* binding */ pancakeBunniesAddress)
});

// UNUSED EXPORTS: pancakeSquadAddress

;// CONCATENATED MODULE: ./src/config/constants/nftsCollections/types.ts
var PancakeCollectionKey;
(function(PancakeCollectionKey) {
    PancakeCollectionKey["PANCAKE"] = 'pancake';
    PancakeCollectionKey["SQUAD"] = 'pancakeSquad';
})(PancakeCollectionKey || (PancakeCollectionKey = {}));

;// CONCATENATED MODULE: ./src/config/constants/nftsCollections/index.ts

const pancakeCollections = {
    [PancakeCollectionKey.PANCAKE]: {
        name: 'Pancake Bunnies',
        slug: 'pancake-bunnies',
        address: {
            56: '0xDf7952B35f24aCF7fC0487D01c8d5690a60DBa07',
            97: '0x60935F36e4631F73f0f407e68642144e07aC7f5E'
        }
    },
    [PancakeCollectionKey.SQUAD]: {
        name: 'Pancake Squad',
        description: "PancakeSwap's first official generative NFT collection.. Join the squad.",
        slug: 'pancake-squad',
        address: {
            56: '0x0a8901b0E25DEb55A87524f0cC164E9644020EBA',
            97: '0xEf12ef570300bFA65c4F022deAaA3dfF4f5d5c91'
        }
    }
};
/* harmony default export */ const nftsCollections = (pancakeCollections);

// EXTERNAL MODULE: ./src/utils/addressHelpers.ts + 1 modules
var addressHelpers = __webpack_require__(5878);
;// CONCATENATED MODULE: ./src/views/Nft/market/constants.ts



const nftsBaseUrl = '/nfts';
const pancakeBunniesAddress = (0,addressHelpers/* getAddress */.Kn)(nftsCollections[PancakeCollectionKey.PANCAKE].address);
const pancakeSquadAddress = (0,addressHelpers/* getAddress */.Kn)(nftsCollections[PancakeCollectionKey.SQUAD].address);


/***/ }),

/***/ 3725:
/***/ ((module) => {

module.exports = JSON.parse('[{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"spender","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount0","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount1","type":"uint256"},{"indexed":true,"internalType":"address","name":"to","type":"address"}],"name":"Burn","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount0","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount1","type":"uint256"}],"name":"Mint","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount0In","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount1In","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount0Out","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount1Out","type":"uint256"},{"indexed":true,"internalType":"address","name":"to","type":"address"}],"name":"Swap","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint112","name":"reserve0","type":"uint112"},{"indexed":false,"internalType":"uint112","name":"reserve1","type":"uint112"}],"name":"Sync","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"inputs":[],"name":"DOMAIN_SEPARATOR","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"MINIMUM_LIQUIDITY","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"pure","type":"function"},{"inputs":[],"name":"PERMIT_TYPEHASH","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"pure","type":"function"},{"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"spender","type":"address"}],"name":"allowance","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"value","type":"uint256"}],"name":"approve","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"owner","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"to","type":"address"}],"name":"burn","outputs":[{"internalType":"uint256","name":"amount0","type":"uint256"},{"internalType":"uint256","name":"amount1","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"decimals","outputs":[{"internalType":"uint8","name":"","type":"uint8"}],"stateMutability":"pure","type":"function"},{"inputs":[],"name":"factory","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getReserves","outputs":[{"internalType":"uint112","name":"reserve0","type":"uint112"},{"internalType":"uint112","name":"reserve1","type":"uint112"},{"internalType":"uint32","name":"blockTimestampLast","type":"uint32"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"},{"internalType":"address","name":"","type":"address"}],"name":"initialize","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"kLast","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"to","type":"address"}],"name":"mint","outputs":[{"internalType":"uint256","name":"liquidity","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"pure","type":"function"},{"inputs":[{"internalType":"address","name":"owner","type":"address"}],"name":"nonces","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"value","type":"uint256"},{"internalType":"uint256","name":"deadline","type":"uint256"},{"internalType":"uint8","name":"v","type":"uint8"},{"internalType":"bytes32","name":"r","type":"bytes32"},{"internalType":"bytes32","name":"s","type":"bytes32"}],"name":"permit","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"price0CumulativeLast","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"price1CumulativeLast","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"to","type":"address"}],"name":"skim","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amount0Out","type":"uint256"},{"internalType":"uint256","name":"amount1Out","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"bytes","name":"data","type":"bytes"}],"name":"swap","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"pure","type":"function"},{"inputs":[],"name":"sync","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"token0","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"token1","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"totalSupply","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"value","type":"uint256"}],"name":"transfer","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"value","type":"uint256"}],"name":"transferFrom","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"}]');

/***/ }),

/***/ 3373:
/***/ ((module) => {

module.exports = JSON.parse('[{"inputs":[{"components":[{"internalType":"address","name":"target","type":"address"},{"internalType":"bytes","name":"callData","type":"bytes"}],"internalType":"struct Multicall2.Call[]","name":"calls","type":"tuple[]"}],"name":"aggregate","outputs":[{"internalType":"uint256","name":"blockNumber","type":"uint256"},{"internalType":"bytes[]","name":"returnData","type":"bytes[]"}],"stateMutability":"view","type":"function"},{"inputs":[{"components":[{"internalType":"address","name":"target","type":"address"},{"internalType":"bytes","name":"callData","type":"bytes"}],"internalType":"struct Multicall2.Call[]","name":"calls","type":"tuple[]"}],"name":"blockAndAggregate","outputs":[{"internalType":"uint256","name":"blockNumber","type":"uint256"},{"internalType":"bytes32","name":"blockHash","type":"bytes32"},{"components":[{"internalType":"bool","name":"success","type":"bool"},{"internalType":"bytes","name":"returnData","type":"bytes"}],"internalType":"struct Multicall2.Result[]","name":"returnData","type":"tuple[]"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"blockNumber","type":"uint256"}],"name":"getBlockHash","outputs":[{"internalType":"bytes32","name":"blockHash","type":"bytes32"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getBlockNumber","outputs":[{"internalType":"uint256","name":"blockNumber","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getCurrentBlockCoinbase","outputs":[{"internalType":"address","name":"coinbase","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getCurrentBlockDifficulty","outputs":[{"internalType":"uint256","name":"difficulty","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getCurrentBlockGasLimit","outputs":[{"internalType":"uint256","name":"gaslimit","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getCurrentBlockTimestamp","outputs":[{"internalType":"uint256","name":"timestamp","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"addr","type":"address"}],"name":"getEthBalance","outputs":[{"internalType":"uint256","name":"balance","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getLastBlockHash","outputs":[{"internalType":"bytes32","name":"blockHash","type":"bytes32"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bool","name":"requireSuccess","type":"bool"},{"components":[{"internalType":"address","name":"target","type":"address"},{"internalType":"bytes","name":"callData","type":"bytes"}],"internalType":"struct Multicall2.Call[]","name":"calls","type":"tuple[]"}],"name":"tryAggregate","outputs":[{"components":[{"internalType":"bool","name":"success","type":"bool"},{"internalType":"bytes","name":"returnData","type":"bytes"}],"internalType":"struct Multicall2.Result[]","name":"returnData","type":"tuple[]"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bool","name":"requireSuccess","type":"bool"},{"components":[{"internalType":"address","name":"target","type":"address"},{"internalType":"bytes","name":"callData","type":"bytes"}],"internalType":"struct Multicall2.Call[]","name":"calls","type":"tuple[]"}],"name":"tryBlockAndAggregate","outputs":[{"internalType":"uint256","name":"blockNumber","type":"uint256"},{"internalType":"bytes32","name":"blockHash","type":"bytes32"},{"components":[{"internalType":"bool","name":"success","type":"bool"},{"internalType":"bytes","name":"returnData","type":"bytes"}],"internalType":"struct Multicall2.Result[]","name":"returnData","type":"tuple[]"}],"stateMutability":"nonpayable","type":"function"}]');

/***/ }),

/***/ 3361:
/***/ ((module) => {

module.exports = JSON.parse('[{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"spender","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"delegator","type":"address"},{"indexed":true,"internalType":"address","name":"fromDelegate","type":"address"},{"indexed":true,"internalType":"address","name":"toDelegate","type":"address"}],"name":"DelegateChanged","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"delegate","type":"address"},{"indexed":false,"internalType":"uint256","name":"previousBalance","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"newBalance","type":"uint256"}],"name":"DelegateVotesChanged","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"inputs":[],"name":"DELEGATION_TYPEHASH","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"DOMAIN_TYPEHASH","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"spender","type":"address"}],"name":"allowance","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"approve","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"account","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"},{"internalType":"uint32","name":"","type":"uint32"}],"name":"checkpoints","outputs":[{"internalType":"uint32","name":"fromBlock","type":"uint32"},{"internalType":"uint256","name":"votes","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"decimals","outputs":[{"internalType":"uint8","name":"","type":"uint8"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"subtractedValue","type":"uint256"}],"name":"decreaseAllowance","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"delegatee","type":"address"}],"name":"delegate","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"delegatee","type":"address"},{"internalType":"uint256","name":"nonce","type":"uint256"},{"internalType":"uint256","name":"expiry","type":"uint256"},{"internalType":"uint8","name":"v","type":"uint8"},{"internalType":"bytes32","name":"r","type":"bytes32"},{"internalType":"bytes32","name":"s","type":"bytes32"}],"name":"delegateBySig","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"delegator","type":"address"}],"name":"delegates","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"account","type":"address"}],"name":"getCurrentVotes","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"account","type":"address"},{"internalType":"uint256","name":"blockNumber","type":"uint256"}],"name":"getPriorVotes","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"addedValue","type":"uint256"}],"name":"increaseAllowance","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_to","type":"address"},{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"mint","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"nonces","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"numCheckpoints","outputs":[{"internalType":"uint32","name":"","type":"uint32"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"totalSupply","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"recipient","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"transfer","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"sender","type":"address"},{"internalType":"address","name":"recipient","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"transferFrom","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"}]');

/***/ }),

/***/ 1430:
/***/ ((module) => {

module.exports = JSON.parse('[{"inputs":[{"internalType":"contract IERC20","name":"_token","type":"address"},{"internalType":"contract IERC20","name":"_receiptToken","type":"address"},{"internalType":"contract IMasterChef","name":"_masterchef","type":"address"},{"internalType":"address","name":"_admin","type":"address"},{"internalType":"address","name":"_treasury","type":"address"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"shares","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"lastDepositedTime","type":"uint256"}],"name":"Deposit","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":false,"internalType":"uint256","name":"performanceFee","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"callFee","type":"uint256"}],"name":"Harvest","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[],"name":"Pause","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"account","type":"address"}],"name":"Paused","type":"event"},{"anonymous":false,"inputs":[],"name":"Unpause","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"account","type":"address"}],"name":"Unpaused","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"shares","type":"uint256"}],"name":"Withdraw","type":"event"},{"inputs":[],"name":"MAX_CALL_FEE","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"MAX_PERFORMANCE_FEE","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"MAX_WITHDRAW_FEE","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"MAX_WITHDRAW_FEE_PERIOD","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"admin","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"available","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"calculateHarvestCakeRewards","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"calculateTotalPendingCakeRewards","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"callFee","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"deposit","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"emergencyWithdraw","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"getPricePerFullShare","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"harvest","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_token","type":"address"}],"name":"inCaseTokensGetStuck","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"lastHarvestedTime","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"masterchef","outputs":[{"internalType":"contract IMasterChef","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"pause","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"paused","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"performanceFee","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"receiptToken","outputs":[{"internalType":"contract IERC20","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_admin","type":"address"}],"name":"setAdmin","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_callFee","type":"uint256"}],"name":"setCallFee","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_performanceFee","type":"uint256"}],"name":"setPerformanceFee","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_treasury","type":"address"}],"name":"setTreasury","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_withdrawFee","type":"uint256"}],"name":"setWithdrawFee","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_withdrawFeePeriod","type":"uint256"}],"name":"setWithdrawFeePeriod","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"token","outputs":[{"internalType":"contract IERC20","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"totalShares","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"treasury","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"unpause","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"userInfo","outputs":[{"internalType":"uint256","name":"shares","type":"uint256"},{"internalType":"uint256","name":"lastDepositedTime","type":"uint256"},{"internalType":"uint256","name":"cakeAtLastUserAction","type":"uint256"},{"internalType":"uint256","name":"lastUserActionTime","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_shares","type":"uint256"}],"name":"withdraw","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"withdrawAll","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"withdrawFee","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"withdrawFeePeriod","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}]');

/***/ }),

/***/ 3324:
/***/ ((module) => {

module.exports = JSON.parse('[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"anonymous":false,"inputs":[{"indexed":true,"name":"owner","type":"address"},{"indexed":true,"name":"spender","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Transfer","type":"event"}]');

/***/ }),

/***/ 3400:
/***/ ((module) => {

module.exports = [];

/***/ }),

/***/ 9704:
/***/ ((module) => {

module.exports = JSON.parse('[{"inputs":[{"internalType":"contract IERC20","name":"_token","type":"address"},{"internalType":"contract IERC20","name":"_receiptToken","type":"address"},{"internalType":"contract IMasterChef","name":"_masterchef","type":"address"},{"internalType":"address","name":"_admin","type":"address"},{"internalType":"address","name":"_treasury","type":"address"},{"internalType":"uint256","name":"_startBlock","type":"uint256"},{"internalType":"uint256","name":"_endBlock","type":"uint256"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"shares","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"lastDepositedTime","type":"uint256"}],"name":"Deposit","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":false,"internalType":"uint256","name":"performanceFee","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"callFee","type":"uint256"}],"name":"Harvest","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[],"name":"Pause","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"account","type":"address"}],"name":"Paused","type":"event"},{"anonymous":false,"inputs":[],"name":"Unpause","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"account","type":"address"}],"name":"Unpaused","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"endBlock","type":"uint256"}],"name":"UpdateEndBlock","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"startBlock","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"endBlock","type":"uint256"}],"name":"UpdateStartAndEndBlocks","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"shares","type":"uint256"}],"name":"Withdraw","type":"event"},{"inputs":[],"name":"MAX_CALL_FEE","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"MAX_PERFORMANCE_FEE","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"MAX_WITHDRAW_FEE","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"MAX_WITHDRAW_FEE_PERIOD","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"admin","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"available","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"calculateHarvestCakeRewards","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"calculateTotalPendingCakeRewards","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"callFee","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"deposit","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"emergencyWithdraw","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"emergencyWithdrawAll","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"endBlock","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getPricePerFullShare","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_user","type":"address"}],"name":"getUserCredit","outputs":[{"internalType":"uint256","name":"avgBalance","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"harvest","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_token","type":"address"}],"name":"inCaseTokensGetStuck","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"lastHarvestedTime","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"masterchef","outputs":[{"internalType":"contract IMasterChef","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"pause","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"paused","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"performanceFee","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"receiptToken","outputs":[{"internalType":"contract IERC20","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_admin","type":"address"}],"name":"setAdmin","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_callFee","type":"uint256"}],"name":"setCallFee","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_performanceFee","type":"uint256"}],"name":"setPerformanceFee","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_treasury","type":"address"}],"name":"setTreasury","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_withdrawFee","type":"uint256"}],"name":"setWithdrawFee","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_withdrawFeePeriod","type":"uint256"}],"name":"setWithdrawFeePeriod","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"startBlock","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"token","outputs":[{"internalType":"contract IERC20","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"totalShares","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"treasury","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"unpause","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_endBlock","type":"uint256"}],"name":"updateEndBlock","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_startBlock","type":"uint256"},{"internalType":"uint256","name":"_endBlock","type":"uint256"}],"name":"updateStartAndEndBlocks","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"userIFOInfo","outputs":[{"internalType":"uint256","name":"lastActionBalance","type":"uint256"},{"internalType":"uint256","name":"lastValidActionBalance","type":"uint256"},{"internalType":"uint256","name":"lastActionBlock","type":"uint256"},{"internalType":"uint256","name":"lastValidActionBlock","type":"uint256"},{"internalType":"uint256","name":"lastAvgBalance","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"userInfo","outputs":[{"internalType":"uint256","name":"shares","type":"uint256"},{"internalType":"uint256","name":"lastDepositedTime","type":"uint256"},{"internalType":"uint256","name":"cakeAtLastUserAction","type":"uint256"},{"internalType":"uint256","name":"lastUserActionTime","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_shares","type":"uint256"}],"name":"withdraw","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"withdrawAll","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"withdrawFee","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"withdrawFeePeriod","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}]');

/***/ }),

/***/ 8592:
/***/ ((module) => {

module.exports = JSON.parse('[{"inputs":[{"internalType":"address","name":"_cakeTokenAddress","type":"address"},{"internalType":"address","name":"_randomGeneratorAddress","type":"address"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"token","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"AdminTokenRecovery","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint256","name":"lotteryId","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"firstTicketIdNextLottery","type":"uint256"}],"name":"LotteryClose","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint256","name":"lotteryId","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"injectedAmount","type":"uint256"}],"name":"LotteryInjection","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint256","name":"lotteryId","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"finalNumber","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"countWinningTickets","type":"uint256"}],"name":"LotteryNumberDrawn","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint256","name":"lotteryId","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"startTime","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"endTime","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"priceTicketInCake","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"firstTicketId","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"injectedAmount","type":"uint256"}],"name":"LotteryOpen","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"operator","type":"address"},{"indexed":false,"internalType":"address","name":"treasury","type":"address"},{"indexed":false,"internalType":"address","name":"injector","type":"address"}],"name":"NewOperatorAndTreasuryAndInjectorAddresses","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"randomGenerator","type":"address"}],"name":"NewRandomGenerator","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"claimer","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"},{"indexed":true,"internalType":"uint256","name":"lotteryId","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"numberTickets","type":"uint256"}],"name":"TicketsClaim","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"buyer","type":"address"},{"indexed":true,"internalType":"uint256","name":"lotteryId","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"numberTickets","type":"uint256"}],"name":"TicketsPurchase","type":"event"},{"inputs":[],"name":"MAX_LENGTH_LOTTERY","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"MAX_TREASURY_FEE","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"MIN_DISCOUNT_DIVISOR","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"MIN_LENGTH_LOTTERY","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_lotteryId","type":"uint256"},{"internalType":"uint32[]","name":"_ticketNumbers","type":"uint32[]"}],"name":"buyTickets","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"cakeToken","outputs":[{"internalType":"contract IERC20","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_discountDivisor","type":"uint256"},{"internalType":"uint256","name":"_priceTicket","type":"uint256"},{"internalType":"uint256","name":"_numberTickets","type":"uint256"}],"name":"calculateTotalPriceForBulkTickets","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"pure","type":"function"},{"inputs":[{"internalType":"address","name":"_randomGeneratorAddress","type":"address"}],"name":"changeRandomGenerator","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_lotteryId","type":"uint256"},{"internalType":"uint256[]","name":"_ticketIds","type":"uint256[]"},{"internalType":"uint32[]","name":"_brackets","type":"uint32[]"}],"name":"claimTickets","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_lotteryId","type":"uint256"}],"name":"closeLottery","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"currentLotteryId","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"currentTicketId","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_lotteryId","type":"uint256"},{"internalType":"bool","name":"_autoInjection","type":"bool"}],"name":"drawFinalNumberAndMakeLotteryClaimable","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_lotteryId","type":"uint256"},{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"injectFunds","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"injectorAddress","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"maxNumberTicketsPerBuyOrClaim","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"maxPriceTicketInCake","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"minPriceTicketInCake","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"operatorAddress","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"pendingInjectionNextLottery","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"randomGenerator","outputs":[{"internalType":"contract IRandomNumberGenerator","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_tokenAddress","type":"address"},{"internalType":"uint256","name":"_tokenAmount","type":"uint256"}],"name":"recoverWrongTokens","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_maxNumberTicketsPerBuy","type":"uint256"}],"name":"setMaxNumberTicketsPerBuy","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_minPriceTicketInCake","type":"uint256"},{"internalType":"uint256","name":"_maxPriceTicketInCake","type":"uint256"}],"name":"setMinAndMaxTicketPriceInCake","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_operatorAddress","type":"address"},{"internalType":"address","name":"_treasuryAddress","type":"address"},{"internalType":"address","name":"_injectorAddress","type":"address"}],"name":"setOperatorAndTreasuryAndInjectorAddresses","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_endTime","type":"uint256"},{"internalType":"uint256","name":"_priceTicketInCake","type":"uint256"},{"internalType":"uint256","name":"_discountDivisor","type":"uint256"},{"internalType":"uint256[6]","name":"_rewardsBreakdown","type":"uint256[6]"},{"internalType":"uint256","name":"_treasuryFee","type":"uint256"}],"name":"startLottery","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"treasuryAddress","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"viewCurrentLotteryId","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_lotteryId","type":"uint256"}],"name":"viewLottery","outputs":[{"components":[{"internalType":"enum PancakeSwapLottery.Status","name":"status","type":"uint8"},{"internalType":"uint256","name":"startTime","type":"uint256"},{"internalType":"uint256","name":"endTime","type":"uint256"},{"internalType":"uint256","name":"priceTicketInCake","type":"uint256"},{"internalType":"uint256","name":"discountDivisor","type":"uint256"},{"internalType":"uint256[6]","name":"rewardsBreakdown","type":"uint256[6]"},{"internalType":"uint256","name":"treasuryFee","type":"uint256"},{"internalType":"uint256[6]","name":"cakePerBracket","type":"uint256[6]"},{"internalType":"uint256[6]","name":"countWinnersPerBracket","type":"uint256[6]"},{"internalType":"uint256","name":"firstTicketId","type":"uint256"},{"internalType":"uint256","name":"firstTicketIdNextLottery","type":"uint256"},{"internalType":"uint256","name":"amountCollectedInCake","type":"uint256"},{"internalType":"uint32","name":"finalNumber","type":"uint32"}],"internalType":"struct PancakeSwapLottery.Lottery","name":"","type":"tuple"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256[]","name":"_ticketIds","type":"uint256[]"}],"name":"viewNumbersAndStatusesForTicketIds","outputs":[{"internalType":"uint32[]","name":"","type":"uint32[]"},{"internalType":"bool[]","name":"","type":"bool[]"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_lotteryId","type":"uint256"},{"internalType":"uint256","name":"_ticketId","type":"uint256"},{"internalType":"uint32","name":"_bracket","type":"uint32"}],"name":"viewRewardsForTicketId","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_user","type":"address"},{"internalType":"uint256","name":"_lotteryId","type":"uint256"},{"internalType":"uint256","name":"_cursor","type":"uint256"},{"internalType":"uint256","name":"_size","type":"uint256"}],"name":"viewUserInfoForLotteryId","outputs":[{"internalType":"uint256[]","name":"","type":"uint256[]"},{"internalType":"uint32[]","name":"","type":"uint32[]"},{"internalType":"bool[]","name":"","type":"bool[]"},{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}]');

/***/ }),

/***/ 4951:
/***/ ((module) => {

module.exports = JSON.parse('[{"inputs":[{"internalType":"contract CakeToken","name":"_cake","type":"address"},{"internalType":"contract SyrupBar","name":"_syrup","type":"address"},{"internalType":"address","name":"_devaddr","type":"address"},{"internalType":"uint256","name":"_cakePerBlock","type":"uint256"},{"internalType":"uint256","name":"_startBlock","type":"uint256"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"user","type":"address"},{"indexed":true,"internalType":"uint256","name":"pid","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"Deposit","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"user","type":"address"},{"indexed":true,"internalType":"uint256","name":"pid","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"EmergencyWithdraw","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"user","type":"address"},{"indexed":true,"internalType":"uint256","name":"pid","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"Withdraw","type":"event"},{"inputs":[],"name":"BONUS_MULTIPLIER","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_allocPoint","type":"uint256"},{"internalType":"contract IBEP20","name":"_lpToken","type":"address"},{"internalType":"bool","name":"_withUpdate","type":"bool"}],"name":"add","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"cake","outputs":[{"internalType":"contract CakeToken","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"cakePerBlock","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_pid","type":"uint256"},{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"deposit","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_devaddr","type":"address"}],"name":"dev","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"devaddr","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_pid","type":"uint256"}],"name":"emergencyWithdraw","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"enterStaking","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_from","type":"uint256"},{"internalType":"uint256","name":"_to","type":"uint256"}],"name":"getMultiplier","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"leaveStaking","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"massUpdatePools","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_pid","type":"uint256"}],"name":"migrate","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"migrator","outputs":[{"internalType":"contract IMigratorChef","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_pid","type":"uint256"},{"internalType":"address","name":"_user","type":"address"}],"name":"pendingCake","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"poolInfo","outputs":[{"internalType":"contract IBEP20","name":"lpToken","type":"address"},{"internalType":"uint256","name":"allocPoint","type":"uint256"},{"internalType":"uint256","name":"lastRewardBlock","type":"uint256"},{"internalType":"uint256","name":"accCakePerShare","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"poolLength","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_pid","type":"uint256"},{"internalType":"uint256","name":"_allocPoint","type":"uint256"},{"internalType":"bool","name":"_withUpdate","type":"bool"}],"name":"set","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"contract IMigratorChef","name":"_migrator","type":"address"}],"name":"setMigrator","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"startBlock","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"syrup","outputs":[{"internalType":"contract SyrupBar","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"totalAllocPoint","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"multiplierNumber","type":"uint256"}],"name":"updateMultiplier","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_pid","type":"uint256"}],"name":"updatePool","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"address","name":"","type":"address"}],"name":"userInfo","outputs":[{"internalType":"uint256","name":"amount","type":"uint256"},{"internalType":"uint256","name":"rewardDebt","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_pid","type":"uint256"},{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"withdraw","outputs":[],"stateMutability":"nonpayable","type":"function"}]');

/***/ }),

/***/ 2335:
/***/ ((module) => {

module.exports = JSON.parse('[{"inputs":[{"internalType":"contract IBEP20","name":"_cakeToken","type":"address"},{"internalType":"uint256","name":"_numberCakeToReactivate","type":"uint256"},{"internalType":"uint256","name":"_numberCakeToRegister","type":"uint256"},{"internalType":"uint256","name":"_numberCakeToUpdate","type":"uint256"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"role","type":"bytes32"},{"indexed":true,"internalType":"bytes32","name":"previousAdminRole","type":"bytes32"},{"indexed":true,"internalType":"bytes32","name":"newAdminRole","type":"bytes32"}],"name":"RoleAdminChanged","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"role","type":"bytes32"},{"indexed":true,"internalType":"address","name":"account","type":"address"},{"indexed":true,"internalType":"address","name":"sender","type":"address"}],"name":"RoleGranted","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"role","type":"bytes32"},{"indexed":true,"internalType":"address","name":"account","type":"address"},{"indexed":true,"internalType":"address","name":"sender","type":"address"}],"name":"RoleRevoked","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"teamId","type":"uint256"},{"indexed":false,"internalType":"string","name":"teamName","type":"string"}],"name":"TeamAdd","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint256","name":"teamId","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"numberPoints","type":"uint256"},{"indexed":true,"internalType":"uint256","name":"campaignId","type":"uint256"}],"name":"TeamPointIncrease","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"userAddress","type":"address"},{"indexed":false,"internalType":"uint256","name":"oldTeamId","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"newTeamId","type":"uint256"}],"name":"UserChangeTeam","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"userAddress","type":"address"},{"indexed":false,"internalType":"uint256","name":"teamId","type":"uint256"},{"indexed":false,"internalType":"address","name":"nftAddress","type":"address"},{"indexed":false,"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"UserNew","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"userAddress","type":"address"},{"indexed":false,"internalType":"uint256","name":"teamId","type":"uint256"}],"name":"UserPause","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"userAddress","type":"address"},{"indexed":false,"internalType":"uint256","name":"numberPoints","type":"uint256"},{"indexed":true,"internalType":"uint256","name":"campaignId","type":"uint256"}],"name":"UserPointIncrease","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address[]","name":"userAddresses","type":"address[]"},{"indexed":false,"internalType":"uint256","name":"numberPoints","type":"uint256"},{"indexed":true,"internalType":"uint256","name":"campaignId","type":"uint256"}],"name":"UserPointIncreaseMultiple","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"userAddress","type":"address"},{"indexed":false,"internalType":"uint256","name":"teamId","type":"uint256"},{"indexed":false,"internalType":"address","name":"nftAddress","type":"address"},{"indexed":false,"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"UserReactivate","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"userAddress","type":"address"},{"indexed":false,"internalType":"address","name":"nftAddress","type":"address"},{"indexed":false,"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"UserUpdate","type":"event"},{"inputs":[],"name":"DEFAULT_ADMIN_ROLE","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"NFT_ROLE","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"POINT_ROLE","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"SPECIAL_ROLE","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_nftAddress","type":"address"}],"name":"addNftAddress","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"string","name":"_teamName","type":"string"},{"internalType":"string","name":"_teamDescription","type":"string"}],"name":"addTeam","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"cakeToken","outputs":[{"internalType":"contract IBEP20","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_userAddress","type":"address"},{"internalType":"uint256","name":"_newTeamId","type":"uint256"}],"name":"changeTeam","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"claimFee","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_teamId","type":"uint256"},{"internalType":"address","name":"_nftAddress","type":"address"},{"internalType":"uint256","name":"_tokenId","type":"uint256"}],"name":"createProfile","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes32","name":"role","type":"bytes32"}],"name":"getRoleAdmin","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bytes32","name":"role","type":"bytes32"},{"internalType":"uint256","name":"index","type":"uint256"}],"name":"getRoleMember","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bytes32","name":"role","type":"bytes32"}],"name":"getRoleMemberCount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_teamId","type":"uint256"}],"name":"getTeamProfile","outputs":[{"internalType":"string","name":"","type":"string"},{"internalType":"string","name":"","type":"string"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_userAddress","type":"address"}],"name":"getUserProfile","outputs":[{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"address","name":"","type":"address"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bytes32","name":"role","type":"bytes32"},{"internalType":"address","name":"account","type":"address"}],"name":"grantRole","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"hasRegistered","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bytes32","name":"role","type":"bytes32"},{"internalType":"address","name":"account","type":"address"}],"name":"hasRole","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_teamId","type":"uint256"},{"internalType":"uint256","name":"_numberPoints","type":"uint256"},{"internalType":"uint256","name":"_campaignId","type":"uint256"}],"name":"increaseTeamPoints","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_userAddress","type":"address"},{"internalType":"uint256","name":"_numberPoints","type":"uint256"},{"internalType":"uint256","name":"_campaignId","type":"uint256"}],"name":"increaseUserPoints","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address[]","name":"_userAddresses","type":"address[]"},{"internalType":"uint256","name":"_numberPoints","type":"uint256"},{"internalType":"uint256","name":"_campaignId","type":"uint256"}],"name":"increaseUserPointsMultiple","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_teamId","type":"uint256"}],"name":"makeTeamJoinable","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_teamId","type":"uint256"}],"name":"makeTeamNotJoinable","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"numberActiveProfiles","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"numberCakeToReactivate","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"numberCakeToRegister","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"numberCakeToUpdate","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"numberTeams","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"},{"internalType":"address","name":"","type":"address"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"bytes","name":"","type":"bytes"}],"name":"onERC721Received","outputs":[{"internalType":"bytes4","name":"","type":"bytes4"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"pauseProfile","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_nftAddress","type":"address"},{"internalType":"uint256","name":"_tokenId","type":"uint256"}],"name":"reactivateProfile","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_teamId","type":"uint256"},{"internalType":"uint256","name":"_numberPoints","type":"uint256"}],"name":"removeTeamPoints","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_userAddress","type":"address"},{"internalType":"uint256","name":"_numberPoints","type":"uint256"}],"name":"removeUserPoints","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address[]","name":"_userAddresses","type":"address[]"},{"internalType":"uint256","name":"_numberPoints","type":"uint256"}],"name":"removeUserPointsMultiple","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_teamId","type":"uint256"},{"internalType":"string","name":"_teamName","type":"string"},{"internalType":"string","name":"_teamDescription","type":"string"}],"name":"renameTeam","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes32","name":"role","type":"bytes32"},{"internalType":"address","name":"account","type":"address"}],"name":"renounceRole","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes32","name":"role","type":"bytes32"},{"internalType":"address","name":"account","type":"address"}],"name":"revokeRole","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_newNumberCakeToReactivate","type":"uint256"},{"internalType":"uint256","name":"_newNumberCakeToRegister","type":"uint256"},{"internalType":"uint256","name":"_newNumberCakeToUpdate","type":"uint256"}],"name":"updateNumberCake","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_nftAddress","type":"address"},{"internalType":"uint256","name":"_tokenId","type":"uint256"}],"name":"updateProfile","outputs":[],"stateMutability":"nonpayable","type":"function"}]');

/***/ }),

/***/ 8750:
/***/ ((module) => {

module.exports = [];

/***/ }),

/***/ 5883:
/***/ ((module) => {

module.exports = JSON.parse('[{"inputs":[{"internalType":"address","name":"_oracleAddress","type":"address"},{"internalType":"address","name":"_adminAddress","type":"address"},{"internalType":"address","name":"_operatorAddress","type":"address"},{"internalType":"uint256","name":"_intervalSeconds","type":"uint256"},{"internalType":"uint256","name":"_bufferSeconds","type":"uint256"},{"internalType":"uint256","name":"_minBetAmount","type":"uint256"},{"internalType":"uint256","name":"_oracleUpdateAllowance","type":"uint256"},{"internalType":"uint256","name":"_treasuryFee","type":"uint256"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":true,"internalType":"uint256","name":"epoch","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"BetBear","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":true,"internalType":"uint256","name":"epoch","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"BetBull","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":true,"internalType":"uint256","name":"epoch","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"Claim","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint256","name":"epoch","type":"uint256"},{"indexed":true,"internalType":"uint256","name":"roundId","type":"uint256"},{"indexed":false,"internalType":"int256","name":"price","type":"int256"}],"name":"EndRound","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint256","name":"epoch","type":"uint256"},{"indexed":true,"internalType":"uint256","name":"roundId","type":"uint256"},{"indexed":false,"internalType":"int256","name":"price","type":"int256"}],"name":"LockRound","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"admin","type":"address"}],"name":"NewAdminAddress","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"bufferSeconds","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"intervalSeconds","type":"uint256"}],"name":"NewBufferAndIntervalSeconds","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint256","name":"epoch","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"minBetAmount","type":"uint256"}],"name":"NewMinBetAmount","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"operator","type":"address"}],"name":"NewOperatorAddress","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"oracle","type":"address"}],"name":"NewOracle","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"oracleUpdateAllowance","type":"uint256"}],"name":"NewOracleUpdateAllowance","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint256","name":"epoch","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"treasuryFee","type":"uint256"}],"name":"NewTreasuryFee","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint256","name":"epoch","type":"uint256"}],"name":"Pause","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"account","type":"address"}],"name":"Paused","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint256","name":"epoch","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"rewardBaseCalAmount","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"rewardAmount","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"treasuryAmount","type":"uint256"}],"name":"RewardsCalculated","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint256","name":"epoch","type":"uint256"}],"name":"StartRound","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"token","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"TokenRecovery","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"TreasuryClaim","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint256","name":"epoch","type":"uint256"}],"name":"Unpause","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"account","type":"address"}],"name":"Unpaused","type":"event"},{"inputs":[],"name":"MAX_TREASURY_FEE","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"adminAddress","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"epoch","type":"uint256"}],"name":"betBear","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"uint256","name":"epoch","type":"uint256"}],"name":"betBull","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[],"name":"bufferSeconds","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256[]","name":"epochs","type":"uint256[]"}],"name":"claim","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"claimTreasury","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"epoch","type":"uint256"},{"internalType":"address","name":"user","type":"address"}],"name":"claimable","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"currentEpoch","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"executeRound","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"genesisLockOnce","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"genesisLockRound","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"genesisStartOnce","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"genesisStartRound","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"user","type":"address"},{"internalType":"uint256","name":"cursor","type":"uint256"},{"internalType":"uint256","name":"size","type":"uint256"}],"name":"getUserRounds","outputs":[{"internalType":"uint256[]","name":"","type":"uint256[]"},{"components":[{"internalType":"enum PancakePredictionV2.Position","name":"position","type":"uint8"},{"internalType":"uint256","name":"amount","type":"uint256"},{"internalType":"bool","name":"claimed","type":"bool"}],"internalType":"struct PancakePredictionV2.BetInfo[]","name":"","type":"tuple[]"},{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"user","type":"address"}],"name":"getUserRoundsLength","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"intervalSeconds","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"address","name":"","type":"address"}],"name":"ledger","outputs":[{"internalType":"enum PancakePredictionV2.Position","name":"position","type":"uint8"},{"internalType":"uint256","name":"amount","type":"uint256"},{"internalType":"bool","name":"claimed","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"minBetAmount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"operatorAddress","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"oracle","outputs":[{"internalType":"contract AggregatorV3Interface","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"oracleLatestRoundId","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"oracleUpdateAllowance","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"pause","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"paused","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_token","type":"address"},{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"recoverToken","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"epoch","type":"uint256"},{"internalType":"address","name":"user","type":"address"}],"name":"refundable","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"rounds","outputs":[{"internalType":"uint256","name":"epoch","type":"uint256"},{"internalType":"uint256","name":"startTimestamp","type":"uint256"},{"internalType":"uint256","name":"lockTimestamp","type":"uint256"},{"internalType":"uint256","name":"closeTimestamp","type":"uint256"},{"internalType":"int256","name":"lockPrice","type":"int256"},{"internalType":"int256","name":"closePrice","type":"int256"},{"internalType":"uint256","name":"lockOracleId","type":"uint256"},{"internalType":"uint256","name":"closeOracleId","type":"uint256"},{"internalType":"uint256","name":"totalAmount","type":"uint256"},{"internalType":"uint256","name":"bullAmount","type":"uint256"},{"internalType":"uint256","name":"bearAmount","type":"uint256"},{"internalType":"uint256","name":"rewardBaseCalAmount","type":"uint256"},{"internalType":"uint256","name":"rewardAmount","type":"uint256"},{"internalType":"bool","name":"oracleCalled","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_adminAddress","type":"address"}],"name":"setAdmin","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_bufferSeconds","type":"uint256"},{"internalType":"uint256","name":"_intervalSeconds","type":"uint256"}],"name":"setBufferAndIntervalSeconds","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_minBetAmount","type":"uint256"}],"name":"setMinBetAmount","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_operatorAddress","type":"address"}],"name":"setOperator","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_oracle","type":"address"}],"name":"setOracle","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_oracleUpdateAllowance","type":"uint256"}],"name":"setOracleUpdateAllowance","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_treasuryFee","type":"uint256"}],"name":"setTreasuryFee","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"treasuryAmount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"treasuryFee","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"unpause","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"},{"internalType":"uint256","name":"","type":"uint256"}],"name":"userRounds","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}]');

/***/ }),

/***/ 6477:
/***/ ((module) => {

module.exports = [];

/***/ }),

/***/ 9022:
/***/ ((module) => {

module.exports = JSON.parse('[{"inputs":[{"internalType":"contract IBEP20","name":"_stakedToken","type":"address"},{"internalType":"contract IBEP20","name":"_rewardToken","type":"address"},{"internalType":"uint256","name":"_rewardPerBlock","type":"uint256"},{"internalType":"uint256","name":"_startBlock","type":"uint256"},{"internalType":"uint256","name":"_bonusEndBlock","type":"uint256"},{"internalType":"uint256","name":"_poolLimitPerUser","type":"uint256"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"tokenRecovered","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"AdminTokenRecovery","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"user","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"Deposit","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"user","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"EmergencyWithdraw","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"poolLimitPerUser","type":"uint256"}],"name":"NewPoolLimit","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"rewardPerBlock","type":"uint256"}],"name":"NewRewardPerBlock","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"startBlock","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"endBlock","type":"uint256"}],"name":"NewStartAndEndBlocks","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"blockNumber","type":"uint256"}],"name":"RewardsStop","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"user","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"Withdraw","type":"event"},{"inputs":[],"name":"PRECISION_FACTOR","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"accTokenPerShare","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"bonusEndBlock","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"deposit","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"emergencyRewardWithdraw","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"emergencyWithdraw","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"hasUserLimit","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"lastRewardBlock","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_user","type":"address"}],"name":"pendingReward","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"poolLimitPerUser","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_tokenAddress","type":"address"},{"internalType":"uint256","name":"_tokenAmount","type":"uint256"}],"name":"recoverWrongTokens","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"rewardPerBlock","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"rewardToken","outputs":[{"internalType":"contract IBEP20","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"stakedToken","outputs":[{"internalType":"contract IBEP20","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"startBlock","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"stopReward","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bool","name":"_hasUserLimit","type":"bool"},{"internalType":"uint256","name":"_poolLimitPerUser","type":"uint256"}],"name":"updatePoolLimitPerUser","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_rewardPerBlock","type":"uint256"}],"name":"updateRewardPerBlock","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_startBlock","type":"uint256"},{"internalType":"uint256","name":"_bonusEndBlock","type":"uint256"}],"name":"updateStartAndEndBlocks","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"userInfo","outputs":[{"internalType":"uint256","name":"amount","type":"uint256"},{"internalType":"uint256","name":"rewardDebt","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"withdraw","outputs":[],"stateMutability":"nonpayable","type":"function"}]');

/***/ }),

/***/ 9253:
/***/ ((module) => {

module.exports = JSON.parse('[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"guy","type":"address"},{"name":"wad","type":"uint256"}],"name":"approve","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"src","type":"address"},{"name":"dst","type":"address"},{"name":"wad","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"wad","type":"uint256"}],"name":"withdraw","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"dst","type":"address"},{"name":"wad","type":"uint256"}],"name":"transfer","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"deposit","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"},{"name":"","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"anonymous":false,"inputs":[{"indexed":true,"name":"src","type":"address"},{"indexed":true,"name":"guy","type":"address"},{"indexed":false,"name":"wad","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"src","type":"address"},{"indexed":true,"name":"dst","type":"address"},{"indexed":false,"name":"wad","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"dst","type":"address"},{"indexed":false,"name":"wad","type":"uint256"}],"name":"Deposit","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"src","type":"address"},{"indexed":false,"name":"wad","type":"uint256"}],"name":"Withdrawal","type":"event"}]');

/***/ })

};
;
//# sourceMappingURL=624.js.map