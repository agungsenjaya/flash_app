"use strict";
exports.id = 308;
exports.ids = [308];
exports.modules = {

/***/ 7308:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "t": () => (/* binding */ nodes),
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var lodash_sample__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7657);
/* harmony import */ var lodash_sample__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash_sample__WEBPACK_IMPORTED_MODULE_0__);

if (false) {}
// Array of available nodes to connect to
const nodes = [
    "https://bsc-dataseed1.ninicoin.io",
    "https://bsc-dataseed1.defibit.io",
    "https://bsc-dataseed.binance.org"
];
const getNodeUrl = ()=>{
    // Use custom node if available (both for development and production)
    // However on the testnet it wouldn't work, so if on testnet - comment out the NEXT_PUBLIC_NODE_PRODUCTION from env file
    if (true) {
        return "https://nodes.pancakeswap.com";
    }
    return lodash_sample__WEBPACK_IMPORTED_MODULE_0___default()(nodes);
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (getNodeUrl);


/***/ })

};
;
//# sourceMappingURL=308.js.map