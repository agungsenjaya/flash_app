"use strict";
exports.id = 601;
exports.ids = [601];
exports.modules = {

/***/ 5601:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ Layout_Page)
});

// UNUSED EXPORTS: PageMeta

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: ./src/contexts/Localization/index.tsx + 3 modules
var Localization = __webpack_require__(9150);
// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(968);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
;// CONCATENATED MODULE: ./src/config/constants/meta.ts
const meta_DEFAULT_META = {
    title: 'PancakeSwap',
    description: 'The most popular AMM on BSC by user count! Earn CAKE through yield farming or win it in the Lottery, then stake it in Syrup Pools to earn more tokens! Initial Farm Offerings (new token launch model pioneered by PancakeSwap), NFTs, and more, on a platform you can trust.',
    image: 'https://lh3.googleusercontent.com/Sgr9vr3SXIgkUgALlzy_0-C0uYc-7K8DZvvsSGbQOQZtbLqa_DPq84xmO0zzC-9Pg5dABNRd6PvIFscEoPVMZZ7Km4W5O1ndFe_yEsDNgilBXAOW4ggSdGlgKwChwiG7sOoKu2JwIs77F0bzo3jbYg0YmbvbbnI4_Gbc0MnpgcOGYBqfQ6XkxL96_skZobb-mTzZz8rIpF2s2bp9BKOTsCAIcV6VdjAxla3JTwjQNAsJf0sLYLRUjs80CRVke7JRTZqUjokW1mTvCFpzsaAs4nBTAH4MBAa5A02aUbM-KyoqIeN5xVUDjQ-jxXoC74LFDhFK-NPGnf7Q8PQBBzS-yAWjShg34iF6wIk0dBYgbo7Q29dk5g3qv2oCOYjSyyMP21GSpRiM1hBpFif2CshOSFAWCRnZjhI95YSQQytFk_dTb6iW8P_332A_Qtuf98DsbijVemoM5EsmuRryc30OPucMt_llDLGw2Lb0puXPa9143aA_S_GT5oSMCk1k5yaiXJql2-mlB52t6YTcQ8iyU46CUR5Vavf9QZcU3CbXi96u1t6JkX6nIT0YOdwI7YbE4bEgxeiLQNcFJgdOzRPPLTHNZze8IkFsaq0adbWCqxdxntSEheQAWiSXmTGpSTnQ-2gu5IvpOABsq9yhOBAHGU8089iGAJRrRC16CdYKhW1o-wkdzelGWBSgMaFrk85WcnS4S0tYP9otrF5GatV0TzgD=w370-h208'
};
const meta_getCustomMeta = (path, t)=>{
    let basePath;
    if (path.startsWith('/swap')) {
        basePath = '/swap';
    } else if (path.startsWith('/add')) {
        basePath = '/add';
    } else if (path.startsWith('/remove')) {
        basePath = '/remove';
    } else if (path.startsWith('/teams')) {
        basePath = '/teams';
    } else if (path.startsWith('/voting/proposal') && path !== '/voting/proposal/create') {
        basePath = '/voting/proposal';
    } else if (path.startsWith('/nfts/collections')) {
        basePath = '/nfts/collections';
    } else if (path.startsWith('/nfts/profile')) {
        basePath = '/nfts/profile';
    } else if (path.startsWith('/pancake-squad')) {
        basePath = '/pancake-squad';
    } else {
        basePath = path;
    }
    switch(basePath){
        case '/':
            return {
                title: `${t('Home')} | ${t('PancakeSwap')}`
            };
        case '/swap':
            return {
                title: `${t('Exchange')} | ${t('PancakeSwap')}`
            };
        case '/add':
            return {
                title: `${t('Add Liquidity')} | ${t('PancakeSwap')}`
            };
        case '/remove':
            return {
                title: `${t('Remove Liquidity')} | ${t('PancakeSwap')}`
            };
        case '/liquidity':
            return {
                title: `${t('Liquidity')} | ${t('PancakeSwap')}`
            };
        case '/find':
            return {
                title: `${t('Import Pool')} | ${t('PancakeSwap')}`
            };
        case '/competition':
            return {
                title: `${t('Trading Battle')} | ${t('PancakeSwap')}`
            };
        case '/prediction':
            return {
                title: `${t('Prediction')} | ${t('PancakeSwap')}`
            };
        case '/prediction/leaderboard':
            return {
                title: `${t('Leaderboard')} | ${t('PancakeSwap')}`
            };
        case '/farms':
            return {
                title: `${t('Farms')} | ${t('PancakeSwap')}`
            };
        case '/farms/auction':
            return {
                title: `${t('Farm Auctions')} | ${t('PancakeSwap')}`
            };
        case '/pools':
            return {
                title: `${t('Pools')} | ${t('PancakeSwap')}`
            };
        case '/lottery':
            return {
                title: `${t('Lottery')} | ${t('PancakeSwap')}`
            };
        case '/ifo':
            return {
                title: `${t('Initial Farm Offering')} | ${t('PancakeSwap')}`
            };
        case '/teams':
            return {
                title: `${t('Leaderboard')} | ${t('PancakeSwap')}`
            };
        case '/voting':
            return {
                title: `${t('Voting')} | ${t('PancakeSwap')}`
            };
        case '/voting/proposal':
            return {
                title: `${t('Proposals')} | ${t('PancakeSwap')}`
            };
        case '/voting/proposal/create':
            return {
                title: `${t('Make a Proposal')} | ${t('PancakeSwap')}`
            };
        case '/info':
            return {
                title: `${t('Overview')} | ${t('PancakeSwap Info & Analytics')}`,
                description: 'View statistics for Pancakeswap exchanges.'
            };
        case '/info/pools':
            return {
                title: `${t('Pools')} | ${t('PancakeSwap Info & Analytics')}`,
                description: 'View statistics for Pancakeswap exchanges.'
            };
        case '/info/tokens':
            return {
                title: `${t('Tokens')} | ${t('PancakeSwap Info & Analytics')}`,
                description: 'View statistics for Pancakeswap exchanges.'
            };
        case '/nfts':
            return {
                title: `${t('Overview')} | ${t('PancakeSwap')}`
            };
        case '/nfts/collections':
            return {
                title: `${t('Collections')} | ${t('PancakeSwap')}`
            };
        case '/nfts/activity':
            return {
                title: `${t('Activity')} | ${t('PancakeSwap')}`
            };
        case '/nfts/profile':
            return {
                title: `${t('Profile')} | ${t('PancakeSwap')}`
            };
        case '/pancake-squad':
            return {
                title: `${t('Pancake Squad')} | ${t('PancakeSwap')}`
            };
        default:
            return null;
    }
};

// EXTERNAL MODULE: external "@pancakeswap/sdk"
var sdk_ = __webpack_require__(543);
// EXTERNAL MODULE: ./src/config/constants/tokens.ts
var constants_tokens = __webpack_require__(9748);
// EXTERNAL MODULE: ./src/hooks/useActiveWeb3React.ts
var hooks_useActiveWeb3React = __webpack_require__(4011);
// EXTERNAL MODULE: ./src/utils/prices.ts
var prices = __webpack_require__(7879);
// EXTERNAL MODULE: ./src/utils/wrappedCurrency.ts
var utils_wrappedCurrency = __webpack_require__(3854);
// EXTERNAL MODULE: ./src/hooks/usePairs.ts
var hooks_usePairs = __webpack_require__(4915);
;// CONCATENATED MODULE: ./src/hooks/useBUSDPrice.ts







const BUSD_MAINNET = constants_tokens/* mainnetTokens.busd */.ke.busd;
const { wbnb: WBNB  } = constants_tokens/* default */.ZP;
/**
 * Returns the price in BUSD of the input currency
 * @param currency currency to compute the BUSD price of
 */ function useBUSDPrice(currency) {
    const { chainId  } = useActiveWeb3React();
    const wrapped = wrappedCurrency(currency, chainId);
    const tokenPairs = useMemo(()=>[
            [
                chainId && wrapped && currencyEquals(WBNB, wrapped) ? undefined : currency,
                chainId ? WBNB : undefined
            ],
            [
                wrapped?.equals(BUSD_MAINNET) ? undefined : wrapped,
                chainId === ChainId.MAINNET ? BUSD_MAINNET : undefined
            ],
            [
                chainId ? WBNB : undefined,
                chainId === ChainId.MAINNET ? BUSD_MAINNET : undefined
            ], 
        ]
    , [
        chainId,
        currency,
        wrapped
    ]);
    const [[ethPairState, ethPair], [busdPairState, busdPair], [busdEthPairState, busdEthPair]] = usePairs(tokenPairs);
    return useMemo(()=>{
        if (!currency || !wrapped || !chainId) {
            return undefined;
        }
        // handle weth/eth
        if (wrapped.equals(WBNB)) {
            if (busdPair) {
                const price = busdPair.priceOf(WBNB);
                return new Price(currency, BUSD_MAINNET, price.denominator, price.numerator);
            }
            return undefined;
        }
        // handle busd
        if (wrapped.equals(BUSD_MAINNET)) {
            return new Price(BUSD_MAINNET, BUSD_MAINNET, '1', '1');
        }
        const ethPairETHAmount = ethPair?.reserveOf(WBNB);
        const ethPairETHBUSDValue = ethPairETHAmount && busdEthPair ? busdEthPair.priceOf(WBNB).quote(ethPairETHAmount).raw : JSBI.BigInt(0);
        // all other tokens
        // first try the busd pair
        if (busdPairState === PairState.EXISTS && busdPair && busdPair.reserveOf(BUSD_MAINNET).greaterThan(ethPairETHBUSDValue)) {
            const price = busdPair.priceOf(wrapped);
            return new Price(currency, BUSD_MAINNET, price.denominator, price.numerator);
        }
        if (ethPairState === PairState.EXISTS && ethPair && busdEthPairState === PairState.EXISTS && busdEthPair) {
            if (busdEthPair.reserveOf(BUSD_MAINNET).greaterThan('0') && ethPair.reserveOf(WBNB).greaterThan('0')) {
                const ethBusdPrice = busdEthPair.priceOf(BUSD_MAINNET);
                const currencyEthPrice = ethPair.priceOf(WBNB);
                const busdPrice = ethBusdPrice.multiply(currencyEthPrice).invert();
                return new Price(currency, BUSD_MAINNET, busdPrice.denominator, busdPrice.numerator);
            }
        }
        return undefined;
    }, [
        chainId,
        currency,
        ethPair,
        ethPairState,
        busdEthPair,
        busdEthPairState,
        busdPair,
        busdPairState,
        wrapped
    ]);
};
const useBUSDPrice_useCakeBusdPrice = ()=>{
    const cakeBusdPrice = useBUSDPrice(tokens.cake);
    return cakeBusdPrice;
};
const useBUSDCurrencyAmount = (currency, amount)=>{
    const { chainId  } = useActiveWeb3React();
    const busdPrice = useBUSDPrice(currency);
    const wrapped = wrappedCurrency(currency, chainId);
    if (busdPrice) {
        return multiplyPriceByAmount(busdPrice, amount, wrapped.decimals);
    }
    return undefined;
};
const useBUSDCakeAmount = (amount)=>{
    const cakeBusdPrice = useBUSDPrice_useCakeBusdPrice();
    if (cakeBusdPrice) {
        return multiplyPriceByAmount(cakeBusdPrice, amount);
    }
    return undefined;
};
const useBNBBusdPrice = ()=>{
    const bnbBusdPrice = useBUSDPrice(tokens.wbnb);
    return bnbBusdPrice;
};

// EXTERNAL MODULE: external "@pancakeswap/uikit"
var uikit_ = __webpack_require__(2829);
;// CONCATENATED MODULE: ./src/components/Layout/Container.tsx



const Container = ({ children , ...props })=>/*#__PURE__*/ jsx_runtime_.jsx(uikit_.Box, {
        px: [
            '16px',
            '24px'
        ],
        mx: "auto",
        maxWidth: "1200px",
        ...props,
        children: children
    })
;
/* harmony default export */ const Layout_Container = (Container);

;// CONCATENATED MODULE: ./src/components/Layout/Page.tsx









const StyledPage = external_styled_components_default()(Layout_Container).withConfig({
    componentId: "sc-433ccbbd-0"
})`
  min-height: calc(100vh - 64px);
  padding-top: 16px;
  padding-bottom: 16px;

  ${({ theme  })=>theme.mediaQueries.sm
} {
    padding-top: 24px;
    padding-bottom: 24px;
  }

  ${({ theme  })=>theme.mediaQueries.lg
} {
    padding-top: 32px;
    padding-bottom: 32px;
  }
`;
const PageMeta = ({ symbol  })=>{
    const { t  } = useTranslation();
    const { pathname  } = useRouter();
    const cakePriceUsd = useCakeBusdPrice();
    const cakePriceUsdDisplay = cakePriceUsd ? `$${cakePriceUsd.toFixed(3)}` : '...';
    const pageMeta = getCustomMeta(pathname, t) || {};
    const { title , description , image  } = {
        ...DEFAULT_META,
        ...pageMeta
    };
    let pageTitle = cakePriceUsdDisplay ? [
        title,
        cakePriceUsdDisplay
    ].join(' - ') : title;
    if (symbol) {
        pageTitle = [
            symbol,
            title
        ].join(' - ');
    }
    return(/*#__PURE__*/ _jsxs(Head, {
        children: [
            /*#__PURE__*/ _jsx("title", {
                children: pageTitle
            }),
            /*#__PURE__*/ _jsx("meta", {
                property: "og:title",
                content: title
            }),
            /*#__PURE__*/ _jsx("meta", {
                property: "og:description",
                content: description
            }),
            /*#__PURE__*/ _jsx("meta", {
                property: "og:image",
                content: image
            })
        ]
    }));
};
const Page = ({ children , symbol , ...props })=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
        children: /*#__PURE__*/ jsx_runtime_.jsx(StyledPage, {
            ...props,
            children: children
        })
    }));
};
/* harmony default export */ const Layout_Page = (Page);


/***/ })

};
;
//# sourceMappingURL=601.js.map