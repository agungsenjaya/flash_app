"use strict";
exports.id = 24;
exports.ids = [24];
exports.modules = {

/***/ 3629:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "a": () => (/* binding */ NextLinkFromReactRouter)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1664);




const A = styled_components__WEBPACK_IMPORTED_MODULE_2___default().a.withConfig({
    componentId: "sc-c6707b7e-0"
})``;
/**
 * temporary solution for migrating React Router to Next.js Link
 */ const NextLinkFromReactRouter = /*#__PURE__*/ (0,react__WEBPACK_IMPORTED_MODULE_1__.forwardRef)(({ to , replace , children , prefetch , ...props }, ref)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_3__["default"], {
        href: to,
        replace: replace,
        passHref: true,
        prefetch: prefetch,
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(A, {
            ref: ref,
            ...props,
            children: children
        })
    })
);


/***/ }),

/***/ 3985:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "KX": () => (/* binding */ useAddPoolKeys),
  "Mn": () => (/* binding */ useAddTokenKeys),
  "JX": () => (/* binding */ useAllPoolData),
  "jv": () => (/* binding */ useAllTokenData),
  "lR": () => (/* binding */ usePoolChartData),
  "zV": () => (/* binding */ usePoolDatas),
  "j8": () => (/* binding */ usePoolTransactions),
  "CN": () => (/* binding */ usePoolsForToken),
  "B5": () => (/* binding */ useProtocolChartData),
  "rf": () => (/* binding */ useProtocolData),
  "Wz": () => (/* binding */ useProtocolTransactions),
  "ku": () => (/* binding */ useTokenChartData),
  "Ws": () => (/* binding */ useTokenData),
  "_n": () => (/* binding */ useTokenDatas),
  "Q4": () => (/* binding */ useTokenPriceData),
  "Vq": () => (/* binding */ useTokenTransactions),
  "b0": () => (/* binding */ useUpdatePoolData),
  "pV": () => (/* binding */ useUpdateTokenData)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(6022);
// EXTERNAL MODULE: external "date-fns"
var external_date_fns_ = __webpack_require__(4146);
// EXTERNAL MODULE: ./src/utils/index.ts + 1 modules
var utils = __webpack_require__(8328);
// EXTERNAL MODULE: external "graphql-request"
var external_graphql_request_ = __webpack_require__(5805);
// EXTERNAL MODULE: ./src/utils/graphql.ts
var graphql = __webpack_require__(5509);
// EXTERNAL MODULE: ./src/config/constants/info.ts
var info = __webpack_require__(4003);
// EXTERNAL MODULE: ./src/state/info/queries/helpers.ts
var helpers = __webpack_require__(4472);
;// CONCATENATED MODULE: ./src/state/info/queries/pools/chartData.ts




const getPoolChartData = async (skip, address)=>{
    try {
        const query = external_graphql_request_.gql`
      query pairDayDatas($startTime: Int!, $skip: Int!, $address: Bytes!) {
        pairDayDatas(
          first: 1000
          skip: $skip
          where: { pairAddress: $address, date_gt: $startTime }
          orderBy: date
          orderDirection: asc
        ) {
          date
          dailyVolumeUSD
          reserveUSD
        }
      }
    `;
        const { pairDayDatas  } = await graphql/* infoClient.request */.d.request(query, {
            startTime: info/* PCS_V2_START */.Rr,
            skip,
            address
        });
        const data = pairDayDatas.map(helpers/* mapPairDayData */.Iq);
        return {
            data,
            error: false
        };
    } catch (error) {
        console.error('Failed to fetch pool chart data', error);
        return {
            error: true
        };
    }
};
const fetchPoolChartData = async (address)=>{
    return (0,helpers/* fetchChartData */.Hr)(getPoolChartData, address);
};
/* harmony default export */ const pools_chartData = (fetchPoolChartData);

;// CONCATENATED MODULE: ./src/state/info/queries/pools/transactions.ts



/**
 * Transactions of the given pool, used on Pool page
 */ const POOL_TRANSACTIONS = external_graphql_request_.gql`
  query poolTransactions($address: Bytes!) {
    mints(first: 35, orderBy: timestamp, orderDirection: desc, where: { pair: $address }) {
      id
      timestamp
      pair {
        token0 {
          id
          symbol
        }
        token1 {
          id
          symbol
        }
      }
      to
      amount0
      amount1
      amountUSD
    }
    swaps(first: 35, orderBy: timestamp, orderDirection: desc, where: { pair: $address }) {
      id
      timestamp
      pair {
        token0 {
          id
          symbol
        }
        token1 {
          id
          symbol
        }
      }
      from
      amount0In
      amount1In
      amount0Out
      amount1Out
      amountUSD
    }
    burns(first: 35, orderBy: timestamp, orderDirection: desc, where: { pair: $address }) {
      id
      timestamp
      pair {
        token0 {
          id
          symbol
        }
        token1 {
          id
          symbol
        }
      }
      sender
      amount0
      amount1
      amountUSD
    }
  }
`;
const fetchPoolTransactions = async (address)=>{
    try {
        const data = await graphql/* infoClient.request */.d.request(POOL_TRANSACTIONS, {
            address
        });
        const mints = data.mints.map(helpers/* mapMints */.k9);
        const burns = data.burns.map(helpers/* mapBurns */._h);
        const swaps = data.swaps.map(helpers/* mapSwaps */.H_);
        return {
            data: [
                ...mints,
                ...burns,
                ...swaps
            ],
            error: false
        };
    } catch (error) {
        console.error(`Failed to fetch transactions for pool ${address}`, error);
        return {
            error: true
        };
    }
};
/* harmony default export */ const pools_transactions = (fetchPoolTransactions);

;// CONCATENATED MODULE: ./src/state/info/queries/tokens/chartData.ts




const getTokenChartData = async (skip, address)=>{
    try {
        const query = external_graphql_request_.gql`
      query tokenDayDatas($startTime: Int!, $skip: Int!, $address: Bytes!) {
        tokenDayDatas(
          first: 1000
          skip: $skip
          where: { token: $address, date_gt: $startTime }
          orderBy: date
          orderDirection: asc
        ) {
          date
          dailyVolumeUSD
          totalLiquidityUSD
        }
      }
    `;
        const { tokenDayDatas  } = await graphql/* infoClient.request */.d.request(query, {
            startTime: info/* PCS_V2_START */.Rr,
            skip,
            address
        });
        const data = tokenDayDatas.map(helpers/* mapDayData */.v5);
        return {
            data,
            error: false
        };
    } catch (error) {
        console.error('Failed to fetch token chart data', error);
        return {
            error: true
        };
    }
};
const fetchTokenChartData = async (address)=>{
    return (0,helpers/* fetchChartData */.Hr)(getTokenChartData, address);
};
/* harmony default export */ const tokens_chartData = (fetchTokenChartData);

;// CONCATENATED MODULE: ./src/state/info/queries/tokens/transactions.ts



/**
 * Data to display transaction table on Token page
 */ const TOKEN_TRANSACTIONS = external_graphql_request_.gql`
  query tokenTransactions($address: Bytes!) {
    mintsAs0: mints(first: 10, orderBy: timestamp, orderDirection: desc, where: { token0: $address }) {
      id
      timestamp
      pair {
        token0 {
          id
          symbol
        }
        token1 {
          id
          symbol
        }
      }
      to
      amount0
      amount1
      amountUSD
    }
    mintsAs1: mints(first: 10, orderBy: timestamp, orderDirection: desc, where: { token0: $address }) {
      id
      timestamp
      pair {
        token0 {
          id
          symbol
        }
        token1 {
          id
          symbol
        }
      }
      to
      amount0
      amount1
      amountUSD
    }
    swapsAs0: swaps(first: 10, orderBy: timestamp, orderDirection: desc, where: { token0: $address }) {
      id
      timestamp
      pair {
        token0 {
          id
          symbol
        }
        token1 {
          id
          symbol
        }
      }
      from
      amount0In
      amount1In
      amount0Out
      amount1Out
      amountUSD
    }
    swapsAs1: swaps(first: 10, orderBy: timestamp, orderDirection: desc, where: { token1: $address }) {
      id
      timestamp
      pair {
        token0 {
          id
          symbol
        }
        token1 {
          id
          symbol
        }
      }
      from
      amount0In
      amount1In
      amount0Out
      amount1Out
      amountUSD
    }
    burnsAs0: burns(first: 10, orderBy: timestamp, orderDirection: desc, where: { token0: $address }) {
      id
      timestamp
      pair {
        token0 {
          id
          symbol
        }
        token1 {
          id
          symbol
        }
      }
      sender
      amount0
      amount1
      amountUSD
    }
    burnsAs1: burns(first: 10, orderBy: timestamp, orderDirection: desc, where: { token1: $address }) {
      id
      timestamp
      pair {
        token0 {
          id
          symbol
        }
        token1 {
          id
          symbol
        }
      }
      sender
      amount0
      amount1
      amountUSD
    }
  }
`;
const fetchTokenTransactions = async (address)=>{
    try {
        const data = await graphql/* infoClient.request */.d.request(TOKEN_TRANSACTIONS, {
            address
        });
        const mints0 = data.mintsAs0.map(helpers/* mapMints */.k9);
        const mints1 = data.mintsAs1.map(helpers/* mapMints */.k9);
        const burns0 = data.burnsAs0.map(helpers/* mapBurns */._h);
        const burns1 = data.burnsAs1.map(helpers/* mapBurns */._h);
        const swaps0 = data.swapsAs0.map(helpers/* mapSwaps */.H_);
        const swaps1 = data.swapsAs1.map(helpers/* mapSwaps */.H_);
        return {
            data: [
                ...mints0,
                ...mints1,
                ...burns0,
                ...burns1,
                ...swaps0,
                ...swaps1
            ],
            error: false
        };
    } catch (error) {
        console.error(`Failed to fetch transactions for token ${address}`, error);
        return {
            error: true
        };
    }
};
/* harmony default export */ const tokens_transactions = (fetchTokenTransactions);

// EXTERNAL MODULE: ./src/views/Info/hooks/useBlocksFromTimestamps.ts
var useBlocksFromTimestamps = __webpack_require__(9566);
// EXTERNAL MODULE: ./src/views/Info/utils/infoQueryHelpers.ts
var infoQueryHelpers = __webpack_require__(5126);
// EXTERNAL MODULE: ./src/config/constants/endpoints.ts
var endpoints = __webpack_require__(5906);
;// CONCATENATED MODULE: ./src/state/info/queries/tokens/priceData.ts





const getPriceSubqueries = (tokenAddress, blocks)=>blocks.map((block)=>`
      t${block.timestamp}:token(id:"${tokenAddress}", block: { number: ${block.number} }) { 
        derivedBNB
      }
      b${block.timestamp}: bundle(id:"1", block: { number: ${block.number} }) { 
        bnbPrice
      }
    `
    )
;
/**
 * Price data for token and bnb based on block number
 */ const priceQueryConstructor = (subqueries)=>{
    return external_graphql_request_.gql`
    query tokenPriceData {
      ${subqueries}
    }
  `;
};
const fetchTokenPriceData = async (address, interval, startTimestamp)=>{
    // Construct timestamps to query against
    const endTimestamp = (0,external_date_fns_.getUnixTime)(new Date());
    const timestamps = [];
    let time = startTimestamp;
    while(time <= endTimestamp){
        timestamps.push(time);
        time += interval;
    }
    try {
        const blocks = await (0,useBlocksFromTimestamps/* getBlocksFromTimestamps */.z)(timestamps, 'asc', 500);
        if (!blocks || blocks.length === 0) {
            console.error('Error fetching blocks for timestamps', timestamps);
            return {
                error: false
            };
        }
        const prices = await (0,infoQueryHelpers/* multiQuery */.L)(priceQueryConstructor, getPriceSubqueries(address, blocks), endpoints/* INFO_CLIENT */.JY, 200);
        if (!prices) {
            console.error('Price data failed to load');
            return {
                error: false
            };
        }
        // format token BNB price results
        const tokenPrices = [];
        // Get Token prices in BNB
        Object.keys(prices).forEach((priceKey)=>{
            const timestamp = priceKey.split('t')[1];
            // if its BNB price e.g. `b123` split('t')[1] will be undefined and skip BNB price entry
            if (timestamp) {
                tokenPrices.push({
                    timestamp,
                    derivedBNB: prices[priceKey]?.derivedBNB ? parseFloat(prices[priceKey].derivedBNB) : 0,
                    priceUSD: 0
                });
            }
        });
        // Go through BNB USD prices and calculate Token price based on it
        Object.keys(prices).forEach((priceKey)=>{
            const timestamp = priceKey.split('b')[1];
            // if its Token price e.g. `t123` split('b')[1] will be undefined and skip Token price entry
            if (timestamp) {
                const tokenPriceIndex = tokenPrices.findIndex((tokenPrice)=>tokenPrice.timestamp === timestamp
                );
                if (tokenPriceIndex >= 0) {
                    const { derivedBNB  } = tokenPrices[tokenPriceIndex];
                    tokenPrices[tokenPriceIndex].priceUSD = parseFloat(prices[priceKey]?.bnbPrice ?? 0) * derivedBNB;
                }
            }
        });
        // graphql-request does not guarantee same ordering of batched requests subqueries, hence sorting by timestamp from oldest to newest
        tokenPrices.sort((a, b)=>parseInt(a.timestamp, 10) - parseInt(b.timestamp, 10)
        );
        const formattedHistory = [];
        // for each timestamp, construct the open and close price
        for(let i = 0; i < tokenPrices.length - 1; i++){
            formattedHistory.push({
                time: parseFloat(tokenPrices[i].timestamp),
                open: tokenPrices[i].priceUSD,
                close: tokenPrices[i + 1].priceUSD,
                high: tokenPrices[i + 1].priceUSD,
                low: tokenPrices[i].priceUSD
            });
        }
        return {
            data: formattedHistory,
            error: false
        };
    } catch (error) {
        console.error(`Failed to fetch price data for token ${address}`, error);
        return {
            error: true
        };
    }
};
/* harmony default export */ const tokens_priceData = (fetchTokenPriceData);

;// CONCATENATED MODULE: ./src/state/info/queries/tokens/poolsForToken.ts



/**
 * Data for showing Pools table on the Token page
 */ const POOLS_FOR_TOKEN = external_graphql_request_.gql`
  query poolsForToken($address: Bytes!, $blacklist: [String!]) {
    asToken0: pairs(
      first: 15
      orderBy: trackedReserveBNB
      orderDirection: desc
      where: { totalTransactions_gt: 100, token0: $address, token1_not_in: $blacklist }
    ) {
      id
    }
    asToken1: pairs(
      first: 15
      orderBy: trackedReserveBNB
      orderDirection: desc
      where: { totalTransactions_gt: 100, token1: $address, token0_not_in: $blacklist }
    ) {
      id
    }
  }
`;
const fetchPoolsForToken = async (address)=>{
    try {
        const data = await graphql/* infoClient.request */.d.request(POOLS_FOR_TOKEN, {
            address,
            blacklist: info/* TOKEN_BLACKLIST */.tE
        });
        return {
            error: false,
            addresses: data.asToken0.concat(data.asToken1).map((p)=>p.id
            )
        };
    } catch (error) {
        console.error(`Failed to fetch pools for token ${address}`, error);
        return {
            error: true
        };
    }
};
/* harmony default export */ const tokens_poolsForToken = (fetchPoolsForToken);

// EXTERNAL MODULE: ./src/state/info/actions.ts
var actions = __webpack_require__(4522);
;// CONCATENATED MODULE: ./src/state/info/hooks.ts











// Protocol hooks
const useProtocolData = ()=>{
    const protocolData = (0,external_react_redux_.useSelector)((state)=>state.info.protocol.overview
    );
    const dispatch = (0,external_react_redux_.useDispatch)();
    const setProtocolData = (0,external_react_.useCallback)((data)=>dispatch((0,actions/* updateProtocolData */.Uo)({
            protocolData: data
        }))
    , [
        dispatch
    ]);
    return [
        protocolData,
        setProtocolData
    ];
};
const useProtocolChartData = ()=>{
    const chartData = (0,external_react_redux_.useSelector)((state)=>state.info.protocol.chartData
    );
    const dispatch = (0,external_react_redux_.useDispatch)();
    const setChartData = (0,external_react_.useCallback)((data)=>dispatch((0,actions/* updateProtocolChartData */.bj)({
            chartData: data
        }))
    , [
        dispatch
    ]);
    return [
        chartData,
        setChartData
    ];
};
const useProtocolTransactions = ()=>{
    const transactions = (0,external_react_redux_.useSelector)((state)=>state.info.protocol.transactions
    );
    const dispatch = (0,external_react_redux_.useDispatch)();
    const setTransactions = (0,external_react_.useCallback)((transactionsData)=>dispatch((0,actions/* updateProtocolTransactions */.oz)({
            transactions: transactionsData
        }))
    , [
        dispatch
    ]);
    return [
        transactions,
        setTransactions
    ];
};
// Pools hooks
const useAllPoolData = ()=>{
    return (0,external_react_redux_.useSelector)((state)=>state.info.pools.byAddress
    );
};
const useUpdatePoolData = ()=>{
    const dispatch = (0,external_react_redux_.useDispatch)();
    return (0,external_react_.useCallback)((pools)=>dispatch((0,actions/* updatePoolData */.Bp)({
            pools
        }))
    , [
        dispatch
    ]);
};
const useAddPoolKeys = ()=>{
    const dispatch = (0,external_react_redux_.useDispatch)();
    return (0,external_react_.useCallback)((poolAddresses)=>dispatch((0,actions/* addPoolKeys */.iF)({
            poolAddresses
        }))
    , [
        dispatch
    ]);
};
const usePoolDatas = (poolAddresses)=>{
    const allPoolData = useAllPoolData();
    const addNewPoolKeys = useAddPoolKeys();
    const untrackedAddresses = poolAddresses.reduce((accum, address)=>{
        if (!Object.keys(allPoolData).includes(address)) {
            accum.push(address);
        }
        return accum;
    }, []);
    (0,external_react_.useEffect)(()=>{
        if (untrackedAddresses) {
            addNewPoolKeys(untrackedAddresses);
        }
    }, [
        addNewPoolKeys,
        untrackedAddresses
    ]);
    const poolsWithData = poolAddresses.map((address)=>{
        return allPoolData[address]?.data;
    }).filter((pool)=>pool
    );
    return poolsWithData;
};
const usePoolChartData = (address)=>{
    const dispatch = (0,external_react_redux_.useDispatch)();
    const pool = (0,external_react_redux_.useSelector)((state)=>state.info.pools.byAddress[address]
    );
    const chartData = pool?.chartData;
    const { 0: error , 1: setError  } = (0,external_react_.useState)(false);
    (0,external_react_.useEffect)(()=>{
        const fetch = async ()=>{
            const { error: fetchError , data  } = await pools_chartData(address);
            if (!fetchError && data) {
                dispatch((0,actions/* updatePoolChartData */.jw)({
                    poolAddress: address,
                    chartData: data
                }));
            }
            if (fetchError) {
                setError(fetchError);
            }
        };
        if (!chartData && !error) {
            fetch();
        }
    }, [
        address,
        dispatch,
        error,
        chartData
    ]);
    return chartData;
};
const usePoolTransactions = (address)=>{
    const dispatch = (0,external_react_redux_.useDispatch)();
    const pool = (0,external_react_redux_.useSelector)((state)=>state.info.pools.byAddress[address]
    );
    const transactions = pool?.transactions;
    const { 0: error , 1: setError  } = (0,external_react_.useState)(false);
    (0,external_react_.useEffect)(()=>{
        const fetch = async ()=>{
            const { error: fetchError , data  } = await pools_transactions(address);
            if (fetchError) {
                setError(true);
            } else {
                dispatch((0,actions/* updatePoolTransactions */.oG)({
                    poolAddress: address,
                    transactions: data
                }));
            }
        };
        if (!transactions && !error) {
            fetch();
        }
    }, [
        address,
        dispatch,
        error,
        transactions
    ]);
    return transactions;
};
// Tokens hooks
const useAllTokenData = ()=>{
    return (0,external_react_redux_.useSelector)((state)=>state.info.tokens.byAddress
    );
};
const useUpdateTokenData = ()=>{
    const dispatch = (0,external_react_redux_.useDispatch)();
    return (0,external_react_.useCallback)((tokens)=>{
        dispatch((0,actions/* updateTokenData */.I6)({
            tokens
        }));
    }, [
        dispatch
    ]);
};
const useAddTokenKeys = ()=>{
    const dispatch = (0,external_react_redux_.useDispatch)();
    return (0,external_react_.useCallback)((tokenAddresses)=>dispatch((0,actions/* addTokenKeys */.uP)({
            tokenAddresses
        }))
    , [
        dispatch
    ]);
};
const useTokenDatas = (addresses)=>{
    const allTokenData = useAllTokenData();
    const addNewTokenKeys = useAddTokenKeys();
    // if token not tracked yet track it
    addresses?.forEach((a)=>{
        if (!allTokenData[a]) {
            addNewTokenKeys([
                a
            ]);
        }
    });
    const tokensWithData = (0,external_react_.useMemo)(()=>{
        if (!addresses) {
            return undefined;
        }
        return addresses.map((a)=>{
            return allTokenData[a]?.data;
        }).filter((token)=>token
        );
    }, [
        addresses,
        allTokenData
    ]);
    return tokensWithData;
};
const useTokenData = (address)=>{
    const allTokenData = useAllTokenData();
    const addNewTokenKeys = useAddTokenKeys();
    if (!address || !(0,utils/* isAddress */.UJ)(address)) {
        return undefined;
    }
    // if token not tracked yet track it
    if (!allTokenData[address]) {
        addNewTokenKeys([
            address
        ]);
    }
    return allTokenData[address]?.data;
};
const usePoolsForToken = (address)=>{
    const dispatch = (0,external_react_redux_.useDispatch)();
    const token = (0,external_react_redux_.useSelector)((state)=>state.info.tokens.byAddress[address]
    );
    const poolsForToken = token.poolAddresses;
    const { 0: error , 1: setError  } = (0,external_react_.useState)(false);
    (0,external_react_.useEffect)(()=>{
        const fetch = async ()=>{
            const { error: fetchError , addresses  } = await tokens_poolsForToken(address);
            if (!fetchError && addresses) {
                dispatch((0,actions/* addTokenPoolAddresses */.TV)({
                    tokenAddress: address,
                    poolAddresses: addresses
                }));
            }
            if (fetchError) {
                setError(fetchError);
            }
        };
        if (!poolsForToken && !error) {
            fetch();
        }
    }, [
        address,
        dispatch,
        error,
        poolsForToken
    ]);
    return poolsForToken;
};
const useTokenChartData = (address)=>{
    const dispatch = (0,external_react_redux_.useDispatch)();
    const token = (0,external_react_redux_.useSelector)((state)=>state.info.tokens.byAddress[address]
    );
    const { chartData  } = token;
    const { 0: error , 1: setError  } = (0,external_react_.useState)(false);
    (0,external_react_.useEffect)(()=>{
        const fetch = async ()=>{
            const { error: fetchError , data  } = await tokens_chartData(address);
            if (!fetchError && data) {
                dispatch((0,actions/* updateTokenChartData */.fo)({
                    tokenAddress: address,
                    chartData: data
                }));
            }
            if (fetchError) {
                setError(fetchError);
            }
        };
        if (!chartData && !error) {
            fetch();
        }
    }, [
        address,
        dispatch,
        error,
        chartData
    ]);
    return chartData;
};
const useTokenPriceData = (address, interval, timeWindow)=>{
    const dispatch = (0,external_react_redux_.useDispatch)();
    const token = (0,external_react_redux_.useSelector)((state)=>state.info.tokens.byAddress[address]
    );
    const priceData = token?.priceData[interval];
    const { 0: error , 1: setError  } = (0,external_react_.useState)(false);
    // construct timestamps and check if we need to fetch more data
    const oldestTimestampFetched = token?.priceData.oldestFetchedTimestamp;
    const utcCurrentTime = (0,external_date_fns_.getUnixTime)(new Date()) * 1000;
    const startTimestamp = (0,external_date_fns_.getUnixTime)((0,external_date_fns_.startOfHour)((0,external_date_fns_.sub)(utcCurrentTime, timeWindow)));
    (0,external_react_.useEffect)(()=>{
        const fetch = async ()=>{
            const { data , error: fetchingError  } = await tokens_priceData(address, interval, startTimestamp);
            if (data) {
                dispatch((0,actions/* updateTokenPriceData */.Db)({
                    tokenAddress: address,
                    secondsInterval: interval,
                    priceData: data,
                    oldestFetchedTimestamp: startTimestamp
                }));
            }
            if (fetchingError) {
                setError(true);
            }
        };
        if (!priceData && !error) {
            fetch();
        }
    }, [
        address,
        dispatch,
        error,
        interval,
        oldestTimestampFetched,
        priceData,
        startTimestamp,
        timeWindow
    ]);
    return priceData;
};
const useTokenTransactions = (address)=>{
    const dispatch = (0,external_react_redux_.useDispatch)();
    const token = (0,external_react_redux_.useSelector)((state)=>state.info.tokens.byAddress[address]
    );
    const { transactions  } = token;
    const { 0: error , 1: setError  } = (0,external_react_.useState)(false);
    (0,external_react_.useEffect)(()=>{
        const fetch = async ()=>{
            const { error: fetchError , data  } = await tokens_transactions(address);
            if (fetchError) {
                setError(true);
            } else if (data) {
                dispatch((0,actions/* updateTokenTransactions */.Mw)({
                    tokenAddress: address,
                    transactions: data
                }));
            }
        };
        if (!transactions && !error) {
            fetch();
        }
    }, [
        address,
        dispatch,
        error,
        transactions
    ]);
    return transactions;
};


/***/ }),

/***/ 4472:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "k9": () => (/* binding */ mapMints),
/* harmony export */   "_h": () => (/* binding */ mapBurns),
/* harmony export */   "H_": () => (/* binding */ mapSwaps),
/* harmony export */   "v5": () => (/* binding */ mapDayData),
/* harmony export */   "Iq": () => (/* binding */ mapPairDayData),
/* harmony export */   "Hr": () => (/* binding */ fetchChartData)
/* harmony export */ });
/* harmony import */ var config_constants_info__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4003);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4146);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(date_fns__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var state_info_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2157);



const mapMints = (mint)=>{
    return {
        type: state_info_types__WEBPACK_IMPORTED_MODULE_1__/* .TransactionType.MINT */ .i.MINT,
        hash: mint.id.split('-')[0],
        timestamp: mint.timestamp,
        sender: mint.to,
        token0Symbol: mint.pair.token0.symbol,
        token1Symbol: mint.pair.token1.symbol,
        token0Address: mint.pair.token0.id,
        token1Address: mint.pair.token1.id,
        amountUSD: parseFloat(mint.amountUSD),
        amountToken0: parseFloat(mint.amount0),
        amountToken1: parseFloat(mint.amount1)
    };
};
const mapBurns = (burn)=>{
    return {
        type: state_info_types__WEBPACK_IMPORTED_MODULE_1__/* .TransactionType.BURN */ .i.BURN,
        hash: burn.id.split('-')[0],
        timestamp: burn.timestamp,
        sender: burn.sender,
        token0Symbol: burn.pair.token0.symbol,
        token1Symbol: burn.pair.token1.symbol,
        token0Address: burn.pair.token0.id,
        token1Address: burn.pair.token1.id,
        amountUSD: parseFloat(burn.amountUSD),
        amountToken0: parseFloat(burn.amount0),
        amountToken1: parseFloat(burn.amount1)
    };
};
const mapSwaps = (swap)=>{
    return {
        type: state_info_types__WEBPACK_IMPORTED_MODULE_1__/* .TransactionType.SWAP */ .i.SWAP,
        hash: swap.id.split('-')[0],
        timestamp: swap.timestamp,
        sender: swap.from,
        token0Symbol: swap.pair.token0.symbol,
        token1Symbol: swap.pair.token1.symbol,
        token0Address: swap.pair.token0.id,
        token1Address: swap.pair.token1.id,
        amountUSD: parseFloat(swap.amountUSD),
        amountToken0: parseFloat(swap.amount0In) - parseFloat(swap.amount0Out),
        amountToken1: parseFloat(swap.amount1In) - parseFloat(swap.amount1Out)
    };
};
const mapDayData = (tokenDayData)=>({
        date: tokenDayData.date,
        volumeUSD: parseFloat(tokenDayData.dailyVolumeUSD),
        liquidityUSD: parseFloat(tokenDayData.totalLiquidityUSD)
    })
;
const mapPairDayData = (pairDayData)=>({
        date: pairDayData.date,
        volumeUSD: parseFloat(pairDayData.dailyVolumeUSD),
        liquidityUSD: parseFloat(pairDayData.reserveUSD)
    })
;
// Common helper function to retrieve chart data
// Used for both Pool and Token charts
const fetchChartData = async (getEntityDayDatas, address)=>{
    let chartEntries = [];
    let error = false;
    let skip = 0;
    let allFound = false;
    while(!allFound){
        // eslint-disable-next-line no-await-in-loop
        const { data , error: fetchError  } = await getEntityDayDatas(skip, address);
        skip += 1000;
        allFound = data.length < 1000;
        error = fetchError;
        if (data) {
            chartEntries = chartEntries.concat(data);
        }
    }
    if (error || chartEntries.length === 0) {
        return {
            error: true
        };
    }
    const formattedDayDatas = chartEntries.reduce((accum, dayData)=>{
        // At this stage we track unix day ordinal for each data point to check for empty days later
        const dayOrdinal = parseInt((dayData.date / config_constants_info__WEBPACK_IMPORTED_MODULE_2__/* .ONE_DAY_UNIX */ .Bq).toFixed(0));
        return {
            [dayOrdinal]: dayData,
            ...accum
        };
    }, {});
    const availableDays = Object.keys(formattedDayDatas).map((dayOrdinal)=>parseInt(dayOrdinal, 10)
    );
    const firstAvailableDayData = formattedDayDatas[availableDays[0]];
    // fill in empty days ( there will be no day datas if no trades made that day )
    let timestamp = firstAvailableDayData?.date ?? config_constants_info__WEBPACK_IMPORTED_MODULE_2__/* .PCS_V2_START */ .Rr;
    let latestLiquidityUSD = firstAvailableDayData?.liquidityUSD ?? 0;
    const endTimestamp = (0,date_fns__WEBPACK_IMPORTED_MODULE_0__.getUnixTime)(new Date());
    while(timestamp < endTimestamp - config_constants_info__WEBPACK_IMPORTED_MODULE_2__/* .ONE_DAY_UNIX */ .Bq){
        timestamp += config_constants_info__WEBPACK_IMPORTED_MODULE_2__/* .ONE_DAY_UNIX */ .Bq;
        const dayOrdinal = parseInt((timestamp / config_constants_info__WEBPACK_IMPORTED_MODULE_2__/* .ONE_DAY_UNIX */ .Bq).toFixed(0), 10);
        if (!Object.keys(formattedDayDatas).includes(dayOrdinal.toString())) {
            formattedDayDatas[dayOrdinal] = {
                date: timestamp,
                volumeUSD: 0,
                liquidityUSD: latestLiquidityUSD
            };
        } else {
            latestLiquidityUSD = formattedDayDatas[dayOrdinal].liquidityUSD;
        }
    }
    return {
        data: Object.values(formattedDayDatas),
        error: false
    };
};


/***/ }),

/***/ 2157:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "i": () => (/* binding */ TransactionType)
/* harmony export */ });
var TransactionType;
(function(TransactionType) {
    TransactionType[TransactionType["SWAP"] = 0] = "SWAP";
    TransactionType[TransactionType["MINT"] = 1] = "MINT";
    TransactionType[TransactionType["BURN"] = 2] = "BURN";
})(TransactionType || (TransactionType = {}));


/***/ }),

/***/ 764:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "X": () => (/* binding */ CurrencyLogo),
  "g": () => (/* binding */ DoubleCurrencyLogo)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: ./src/utils/index.ts + 1 modules
var utils = __webpack_require__(8328);
// EXTERNAL MODULE: external "@pancakeswap/uikit"
var uikit_ = __webpack_require__(2829);
;// CONCATENATED MODULE: ./src/views/Info/components/CurrencyLogo/LogoLoader.tsx



const BAD_SRCS = [];
/**
 * Renders an image by sequentially trying a list of URIs, and then eventually a fallback to HelpIcon
 */ const LogoLoader = ({ src , alt , ...rest })=>{
    const { 1: refresh  } = (0,external_react_.useState)(0);
    const srcFailedLoading = BAD_SRCS.includes(src);
    if (src && !srcFailedLoading) {
        return(/*#__PURE__*/ jsx_runtime_.jsx("img", {
            ...rest,
            alt: alt,
            src: src,
            onError: ()=>{
                if (src) BAD_SRCS.push(src);
                refresh((i)=>i + 1
                );
            }
        }));
    }
    return(/*#__PURE__*/ jsx_runtime_.jsx(uikit_.HelpIcon, {
        ...rest
    }));
};
/* harmony default export */ const CurrencyLogo_LogoLoader = (LogoLoader);

;// CONCATENATED MODULE: ./src/views/Info/components/CurrencyLogo/index.tsx





const StyledLogo = external_styled_components_default()(CurrencyLogo_LogoLoader).withConfig({
    componentId: "sc-8f734d72-0"
})`
  width: ${({ size  })=>size
};
  height: ${({ size  })=>size
};
  border-radius: ${({ size  })=>size
};
  box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.075);
  background-color: ${({ theme  })=>theme.colors.background
};
  color: ${({ theme  })=>theme.colors.text
};
`;
const CurrencyLogo = ({ address , size ='24px' , ...rest })=>{
    const src = (0,external_react_.useMemo)(()=>{
        const checksummedAddress = (0,utils/* isAddress */.UJ)(address);
        if (checksummedAddress) {
            return `https://assets.trustwalletapp.com/blockchains/smartchain/assets/${checksummedAddress}/logo.png`;
        }
        return null;
    }, [
        address
    ]);
    return(/*#__PURE__*/ jsx_runtime_.jsx(StyledLogo, {
        size: size,
        src: src,
        alt: "token logo",
        ...rest
    }));
};
const DoubleCurrencyWrapper = external_styled_components_default().div.withConfig({
    componentId: "sc-8f734d72-1"
})`
  position: relative;
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 32px;
`;
const DoubleCurrencyLogo = ({ address0 , address1 , size =16  })=>{
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(DoubleCurrencyWrapper, {
        children: [
            address0 && /*#__PURE__*/ jsx_runtime_.jsx(CurrencyLogo, {
                address: address0,
                size: `${size.toString()}px`
            }),
            address1 && /*#__PURE__*/ jsx_runtime_.jsx(CurrencyLogo, {
                address: address1,
                size: `${size.toString()}px`
            })
        ]
    }));
};


/***/ }),

/***/ 8910:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "_J": () => (/* binding */ ClickableColumnHeader),
/* harmony export */   "y6": () => (/* binding */ TableWrapper),
/* harmony export */   "Ob": () => (/* binding */ PageButtons),
/* harmony export */   "Eh": () => (/* binding */ Arrow),
/* harmony export */   "SS": () => (/* binding */ Break)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2829);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_1__);


const ClickableColumnHeader = styled_components__WEBPACK_IMPORTED_MODULE_0___default()(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_1__.Text).withConfig({
    componentId: "sc-4dd460a5-0"
})`
  cursor: pointer;
`;
const TableWrapper = styled_components__WEBPACK_IMPORTED_MODULE_0___default()(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_1__.Flex).withConfig({
    componentId: "sc-4dd460a5-1"
})`
  width: 100%;
  padding-top: 16px;
  flex-direction: column;
  gap: 16px;
  background-color: ${({ theme  })=>theme.card.background
};
  border-radius: ${({ theme  })=>theme.radii.card
};
  border: 1px solid ${({ theme  })=>theme.colors.cardBorder
};
`;
const PageButtons = styled_components__WEBPACK_IMPORTED_MODULE_0___default().div.withConfig({
    componentId: "sc-4dd460a5-2"
})`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 0.2em;
  margin-bottom: 1.2em;
`;
const Arrow = styled_components__WEBPACK_IMPORTED_MODULE_0___default().div.withConfig({
    componentId: "sc-4dd460a5-3"
})`
  color: ${({ theme  })=>theme.colors.primary
};
  padding: 0 20px;
  :hover {
    cursor: pointer;
  }
`;
const Break = styled_components__WEBPACK_IMPORTED_MODULE_0___default().div.withConfig({
    componentId: "sc-4dd460a5-4"
})`
  height: 1px;
  background-color: ${({ theme  })=>theme.colors.cardBorder
};
  width: 100%;
`;


/***/ }),

/***/ 3614:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2829);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var hooks_useTheme__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3917);





const HoverIcon = styled_components__WEBPACK_IMPORTED_MODULE_2___default().div.withConfig({
    componentId: "sc-eadf5b50-0"
})`
  display: flex;
  justify-content: center;
  align-items: center;
  :hover {
    cursor: pointer;
    opacity: 0.6;
  }
`;
const SaveIcon = ({ fill =false , ...rest })=>{
    const { theme  } = (0,hooks_useTheme__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z)();
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(HoverIcon, {
        ...rest,
        children: fill ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_3__.StarFillIcon, {
            stroke: theme.colors.warning,
            color: theme.colors.warning
        }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_3__.StarLineIcon, {
            stroke: theme.colors.textDisabled
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SaveIcon);


/***/ }),

/***/ 7661:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "O": () => (/* binding */ InfoPageLayout)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "graphql-request"
var external_graphql_request_ = __webpack_require__(5805);
// EXTERNAL MODULE: ./src/utils/graphql.ts
var graphql = __webpack_require__(5509);
// EXTERNAL MODULE: ./src/views/Info/hooks/useBlocksFromTimestamps.ts
var useBlocksFromTimestamps = __webpack_require__(9566);
// EXTERNAL MODULE: ./src/config/constants/info.ts
var info = __webpack_require__(4003);
;// CONCATENATED MODULE: ./src/views/Info/utils/infoDataHelpers.ts

/**
 * Get increase/decrease of value compared to the previous value (e.g. 24h volume compared to 24h volume the day before )
 * @param valueNow - more recent value
 * @param valueBefore - value to compare with
 */ const getAmountChange = (valueNow, valueBefore)=>{
    if (valueNow && valueBefore) {
        return valueNow - valueBefore;
    }
    if (valueNow) {
        return valueNow;
    }
    return 0;
};
/**
 * Get increase/decrease of value compared to the previous value as a percentage
 * @param valueNow - more recent value
 * @param valueBefore - value to compare with
 */ const getPercentChange = (valueNow, valueBefore)=>{
    if (valueNow && valueBefore) {
        return (valueNow - valueBefore) / valueBefore * 100;
    }
    return 0;
};
/**
 * Given current value and value 1 and 2 periods (e.g. 1day + 2days, 1week - 2weeks) returns the amount change for latest period
 * and percentage change compared to the previous period.
 * @param valueNow - current value
 * @param valueOnePeriodAgo - value 1 period ago (e.g. 1 day or 1 week ago), period unit must be same as valueTwoPeriodsAgo
 * @param valueTwoPeriodsAgo - value 2 periods ago (e.g. 2 days or 2 weeks ago), period unit must be same as valueOnePeriodAgo
 * @returns amount change for the latest period and percentage change compared to previous period
 */ const getChangeForPeriod = (valueNow, valueOnePeriodAgo, valueTwoPeriodsAgo)=>{
    const currentPeriodAmount = getAmountChange(valueNow, valueOnePeriodAgo);
    const previousPeriodAmount = getAmountChange(valueOnePeriodAgo, valueTwoPeriodsAgo);
    const percentageChange = getPercentChange(currentPeriodAmount, previousPeriodAmount);
    return [
        currentPeriodAmount,
        percentageChange
    ];
};
const getLpFeesAndApr = (volumeUSD, volumeUSDWeek, liquidityUSD)=>{
    const totalFees24h = volumeUSD * info/* TOTAL_FEE */.om;
    const totalFees7d = volumeUSDWeek * info/* TOTAL_FEE */.om;
    const lpFees24h = volumeUSD * info/* LP_HOLDERS_FEE */.BY;
    const lpFees7d = volumeUSDWeek * info/* LP_HOLDERS_FEE */.BY;
    const lpApr7d = liquidityUSD > 0 ? volumeUSDWeek * info/* LP_HOLDERS_FEE */.BY * info/* WEEKS_IN_YEAR */.MV * 100 / liquidityUSD : 0;
    return {
        totalFees24h,
        totalFees7d,
        lpFees24h,
        lpFees7d,
        lpApr7d: lpApr7d !== Infinity ? lpApr7d : 0
    };
};

// EXTERNAL MODULE: ./src/views/Info/utils/infoQueryHelpers.ts
var infoQueryHelpers = __webpack_require__(5126);
;// CONCATENATED MODULE: ./src/state/info/queries/protocol/overview.ts






/**
 * Latest Liquidity, Volume and Transaction count
 */ const getOverviewData = async (block)=>{
    try {
        const query = external_graphql_request_.gql`query overview {
      pancakeFactories(
        ${block ? `block: { number: ${block}}` : ``}
        first: 1) {
        totalTransactions
        totalVolumeUSD
        totalLiquidityUSD
      }
    }`;
        const data = await graphql/* infoClient.request */.d.request(query);
        return {
            data,
            error: false
        };
    } catch (error) {
        console.error('Failed to fetch info overview', error);
        return {
            data: null,
            error: true
        };
    }
};
const formatPancakeFactoryResponse = (rawPancakeFactory)=>{
    if (rawPancakeFactory) {
        return {
            totalTransactions: parseFloat(rawPancakeFactory.totalTransactions),
            totalVolumeUSD: parseFloat(rawPancakeFactory.totalVolumeUSD),
            totalLiquidityUSD: parseFloat(rawPancakeFactory.totalLiquidityUSD)
        };
    }
    return null;
};
const useFetchProtocolData = ()=>{
    const { 0: fetchState , 1: setFetchState  } = (0,external_react_.useState)({
        error: false
    });
    const [t24, t48] = (0,infoQueryHelpers/* getDeltaTimestamps */.z)();
    const { blocks , error: blockError  } = (0,useBlocksFromTimestamps/* useBlocksFromTimestamps */.Z)([
        t24,
        t48
    ]);
    const [block24, block48] = blocks ?? [];
    (0,external_react_.useEffect)(()=>{
        const fetch = async ()=>{
            const { error , data  } = await getOverviewData();
            const { error: error24 , data: data24  } = await getOverviewData(block24?.number ?? undefined);
            const { error: error48 , data: data48  } = await getOverviewData(block48?.number ?? undefined);
            const anyError = error || error24 || error48;
            const overviewData = formatPancakeFactoryResponse(data?.pancakeFactories?.[0]);
            const overviewData24 = formatPancakeFactoryResponse(data24?.pancakeFactories?.[0]);
            const overviewData48 = formatPancakeFactoryResponse(data48?.pancakeFactories?.[0]);
            const allDataAvailable = overviewData && overviewData24 && overviewData48;
            if (anyError || !allDataAvailable) {
                setFetchState({
                    error: true
                });
            } else {
                const [volumeUSD, volumeUSDChange] = getChangeForPeriod(overviewData.totalVolumeUSD, overviewData24.totalVolumeUSD, overviewData48.totalVolumeUSD);
                const liquidityUSDChange = getPercentChange(overviewData.totalLiquidityUSD, overviewData24.totalLiquidityUSD);
                // 24H transactions
                const [txCount, txCountChange] = getChangeForPeriod(overviewData.totalTransactions, overviewData24.totalTransactions, overviewData48.totalTransactions);
                const protocolData = {
                    volumeUSD,
                    volumeUSDChange: typeof volumeUSDChange === 'number' ? volumeUSDChange : 0,
                    liquidityUSD: overviewData.totalLiquidityUSD,
                    liquidityUSDChange,
                    txCount,
                    txCountChange
                };
                setFetchState({
                    error: false,
                    data: protocolData
                });
            }
        };
        const allBlocksAvailable = block24?.number && block48?.number;
        if (allBlocksAvailable && !blockError && !fetchState.data) {
            fetch();
        }
    }, [
        block24,
        block48,
        blockError,
        fetchState
    ]);
    return fetchState;
};
/* harmony default export */ const overview = (useFetchProtocolData);

// EXTERNAL MODULE: ./src/state/info/queries/helpers.ts
var helpers = __webpack_require__(4472);
;// CONCATENATED MODULE: ./src/state/info/queries/protocol/chart.ts
/* eslint-disable no-await-in-loop */ 




/**
 * Data for displaying Liquidity and Volume charts on Overview page
 */ const PANCAKE_DAY_DATAS = external_graphql_request_.gql`
  query overviewCharts($startTime: Int!, $skip: Int!) {
    pancakeDayDatas(first: 1000, skip: $skip, where: { date_gt: $startTime }, orderBy: date, orderDirection: asc) {
      date
      dailyVolumeUSD
      totalLiquidityUSD
    }
  }
`;
const getOverviewChartData = async (skip)=>{
    try {
        const { pancakeDayDatas  } = await graphql/* infoClient.request */.d.request(PANCAKE_DAY_DATAS, {
            startTime: info/* PCS_V2_START */.Rr,
            skip
        });
        const data = pancakeDayDatas.map(helpers/* mapDayData */.v5);
        return {
            data,
            error: false
        };
    } catch (error) {
        console.error('Failed to fetch overview chart data', error);
        return {
            error: true
        };
    }
};
/**
 * Fetch historic chart data
 */ const useFetchGlobalChartData = ()=>{
    const { 0: overviewChartData , 1: setOverviewChartData  } = (0,external_react_.useState)();
    const { 0: error , 1: setError  } = (0,external_react_.useState)(false);
    (0,external_react_.useEffect)(()=>{
        const fetch = async ()=>{
            const { data  } = await (0,helpers/* fetchChartData */.Hr)(getOverviewChartData);
            if (data) {
                setOverviewChartData(data);
            } else {
                setError(true);
            }
        };
        if (!overviewChartData && !error) {
            fetch();
        }
    }, [
        overviewChartData,
        error
    ]);
    return {
        error,
        data: overviewChartData
    };
};
/* harmony default export */ const chart = (useFetchGlobalChartData);

;// CONCATENATED MODULE: ./src/state/info/queries/protocol/transactions.ts



/**
 * Transactions for Transaction table on the Home page
 */ const GLOBAL_TRANSACTIONS = external_graphql_request_.gql`
  query overviewTransactions {
    mints: mints(first: 33, orderBy: timestamp, orderDirection: desc) {
      id
      timestamp
      pair {
        token0 {
          id
          symbol
        }
        token1 {
          id
          symbol
        }
      }
      to
      amount0
      amount1
      amountUSD
    }
    swaps: swaps(first: 33, orderBy: timestamp, orderDirection: desc) {
      id
      timestamp
      pair {
        token0 {
          id
          symbol
        }
        token1 {
          id
          symbol
        }
      }
      from
      amount0In
      amount1In
      amount0Out
      amount1Out
      amountUSD
    }
    burns: burns(first: 33, orderBy: timestamp, orderDirection: desc) {
      id
      timestamp
      pair {
        token0 {
          id
          symbol
        }
        token1 {
          id
          symbol
        }
      }
      sender
      amount0
      amount1
      amountUSD
    }
  }
`;
const fetchTopTransactions = async ()=>{
    try {
        const data = await graphql/* infoClient.request */.d.request(GLOBAL_TRANSACTIONS);
        if (!data) {
            return undefined;
        }
        const mints = data.mints.map(helpers/* mapMints */.k9);
        const burns = data.burns.map(helpers/* mapBurns */._h);
        const swaps = data.swaps.map(helpers/* mapSwaps */.H_);
        return [
            ...mints,
            ...burns,
            ...swaps
        ].sort((a, b)=>{
            return parseInt(b.timestamp, 10) - parseInt(a.timestamp, 10);
        });
    } catch  {
        return undefined;
    }
};
/* harmony default export */ const protocol_transactions = (fetchTopTransactions);

;// CONCATENATED MODULE: ./src/state/info/queries/pools/topPools.ts





/**
 * Initial pools to display on the home page
 */ const fetchTopPools = async (timestamp24hAgo)=>{
    try {
        const query = external_graphql_request_.gql`
      query topPools($blacklist: [String!], $timestamp24hAgo: Int) {
        pairDayDatas(
          first: 30
          where: { dailyTxns_gt: 300, token0_not_in: $blacklist, token1_not_in: $blacklist, date_gt: $timestamp24hAgo }
          orderBy: dailyVolumeUSD
          orderDirection: desc
        ) {
          id
        }
      }
    `;
        const data = await graphql/* infoClient.request */.d.request(query, {
            blacklist: info/* TOKEN_BLACKLIST */.tE,
            timestamp24hAgo
        });
        // pairDayDatas id has compound id "0xPOOLADDRESS-NUMBERS", extracting pool address with .split('-')
        return data.pairDayDatas.map((p)=>p.id.split('-')[0]
        );
    } catch (error) {
        console.error('Failed to fetch top pools', error);
        return [];
    }
};
/**
 * Fetch top addresses by volume
 */ const useTopPoolAddresses = ()=>{
    const { 0: topPoolAddresses , 1: setTopPoolAddresses  } = (0,external_react_.useState)([]);
    const [timestamp24hAgo] = (0,infoQueryHelpers/* getDeltaTimestamps */.z)();
    (0,external_react_.useEffect)(()=>{
        const fetch = async ()=>{
            const addresses = await fetchTopPools(timestamp24hAgo);
            setTopPoolAddresses(addresses);
        };
        if (topPoolAddresses.length === 0) {
            fetch();
        }
    }, [
        topPoolAddresses,
        timestamp24hAgo
    ]);
    return topPoolAddresses;
};
/* harmony default export */ const topPools = (useTopPoolAddresses);

;// CONCATENATED MODULE: ./src/state/info/queries/pools/poolData.ts
/* eslint-disable no-param-reassign */ 





/**
 * Data for displaying pool tables (on multiple pages, used throughout the site)
 * Note: Don't try to refactor it to use variables, server throws error if blocks passed as undefined variable
 * only works if its hard-coded into query string
 */ const POOL_AT_BLOCK = (block, pools)=>{
    const blockString = block ? `block: {number: ${block}}` : ``;
    const addressesString = `["${pools.join('","')}"]`;
    return `pairs(
    where: { id_in: ${addressesString} }
    ${blockString}
    orderBy: trackedReserveBNB
    orderDirection: desc
  ) {
    id
    reserve0
    reserve1
    reserveUSD
    volumeUSD
    token0Price
    token1Price
    token0 {
      id
      symbol
      name
    }
    token1 {
      id
      symbol
      name
    }
  }`;
};
const fetchPoolData = async (block24h, block48h, block7d, block14d, poolAddresses)=>{
    try {
        const query = external_graphql_request_.gql`
      query pools {
        now: ${POOL_AT_BLOCK(null, poolAddresses)}
        oneDayAgo: ${POOL_AT_BLOCK(block24h, poolAddresses)}
        twoDaysAgo: ${POOL_AT_BLOCK(block48h, poolAddresses)}
        oneWeekAgo: ${POOL_AT_BLOCK(block7d, poolAddresses)}
        twoWeeksAgo: ${POOL_AT_BLOCK(block14d, poolAddresses)}
      }
    `;
        const data = await graphql/* infoClient.request */.d.request(query);
        return {
            data,
            error: false
        };
    } catch (error) {
        console.error('Failed to fetch pool data', error);
        return {
            error: true
        };
    }
};
// Transforms pools into "0xADDRESS: { ...PoolFields }" format and cast strings to numbers
const parsePoolData = (pairs)=>{
    if (!pairs) {
        return {};
    }
    return pairs.reduce((accum, poolData)=>{
        const { volumeUSD , reserveUSD , reserve0 , reserve1 , token0Price , token1Price  } = poolData;
        accum[poolData.id] = {
            ...poolData,
            volumeUSD: parseFloat(volumeUSD),
            reserveUSD: parseFloat(reserveUSD),
            reserve0: parseFloat(reserve0),
            reserve1: parseFloat(reserve1),
            token0Price: parseFloat(token0Price),
            token1Price: parseFloat(token1Price)
        };
        return accum;
    }, {});
};
/**
 * Fetch top pools by liquidity
 */ const usePoolDatas = (poolAddresses)=>{
    const { 0: fetchState , 1: setFetchState  } = (0,external_react_.useState)({
        error: false
    });
    const [t24h, t48h, t7d, t14d] = (0,infoQueryHelpers/* getDeltaTimestamps */.z)();
    const { blocks , error: blockError  } = (0,useBlocksFromTimestamps/* useBlocksFromTimestamps */.Z)([
        t24h,
        t48h,
        t7d,
        t14d
    ]);
    const [block24h, block48h, block7d, block14d] = blocks ?? [];
    (0,external_react_.useEffect)(()=>{
        const fetch = async ()=>{
            const { error , data  } = await fetchPoolData(block24h.number, block48h.number, block7d.number, block14d.number, poolAddresses);
            if (error) {
                setFetchState({
                    error: true
                });
            } else {
                const formattedPoolData = parsePoolData(data?.now);
                const formattedPoolData24h = parsePoolData(data?.oneDayAgo);
                const formattedPoolData48h = parsePoolData(data?.twoDaysAgo);
                const formattedPoolData7d = parsePoolData(data?.oneWeekAgo);
                const formattedPoolData14d = parsePoolData(data?.twoWeeksAgo);
                // Calculate data and format
                const formatted = poolAddresses.reduce((accum, address)=>{
                    // Undefined data is possible if pool is brand new and didn't exist one day ago or week ago.
                    const current = formattedPoolData[address];
                    const oneDay = formattedPoolData24h[address];
                    const twoDays = formattedPoolData48h[address];
                    const week = formattedPoolData7d[address];
                    const twoWeeks = formattedPoolData14d[address];
                    const [volumeUSD, volumeUSDChange] = getChangeForPeriod(current?.volumeUSD, oneDay?.volumeUSD, twoDays?.volumeUSD);
                    const [volumeUSDWeek, volumeUSDChangeWeek] = getChangeForPeriod(current?.volumeUSD, week?.volumeUSD, twoWeeks?.volumeUSD);
                    const liquidityUSD = current ? current.reserveUSD : 0;
                    const liquidityUSDChange = getPercentChange(current?.reserveUSD, oneDay?.reserveUSD);
                    const liquidityToken0 = current ? current.reserve0 : 0;
                    const liquidityToken1 = current ? current.reserve1 : 0;
                    const { totalFees24h , totalFees7d , lpFees24h , lpFees7d , lpApr7d  } = getLpFeesAndApr(volumeUSD, volumeUSDWeek, liquidityUSD);
                    if (current) {
                        accum[address] = {
                            address,
                            token0: {
                                address: current.token0.id,
                                name: current.token0.name,
                                symbol: current.token0.symbol
                            },
                            token1: {
                                address: current.token1.id,
                                name: current.token1.name,
                                symbol: current.token1.symbol
                            },
                            token0Price: current.token0Price,
                            token1Price: current.token1Price,
                            volumeUSD,
                            volumeUSDChange,
                            volumeUSDWeek,
                            volumeUSDChangeWeek,
                            totalFees24h,
                            totalFees7d,
                            lpFees24h,
                            lpFees7d,
                            lpApr7d,
                            liquidityUSD,
                            liquidityUSDChange,
                            liquidityToken0,
                            liquidityToken1
                        };
                    }
                    return accum;
                }, {});
                setFetchState({
                    data: formatted,
                    error: false
                });
            }
        };
        const allBlocksAvailable = block24h?.number && block48h?.number && block7d?.number && block14d?.number;
        if (poolAddresses.length > 0 && allBlocksAvailable && !blockError) {
            fetch();
        }
    }, [
        poolAddresses,
        block24h,
        block48h,
        block7d,
        block14d,
        blockError
    ]);
    return fetchState;
};
/* harmony default export */ const poolData = (usePoolDatas);

;// CONCATENATED MODULE: ./src/views/Info/hooks/useBnbPrices.ts





const BNB_PRICES = external_graphql_request_.gql`
  query prices($block24: Int!, $block48: Int!, $blockWeek: Int!) {
    current: bundle(id: "1") {
      bnbPrice
    }
    oneDay: bundle(id: "1", block: { number: $block24 }) {
      bnbPrice
    }
    twoDay: bundle(id: "1", block: { number: $block48 }) {
      bnbPrice
    }
    oneWeek: bundle(id: "1", block: { number: $blockWeek }) {
      bnbPrice
    }
  }
`;
const fetchBnbPrices = async (block24, block48, blockWeek)=>{
    try {
        const data = await graphql/* infoClient.request */.d.request(BNB_PRICES, {
            block24,
            block48,
            blockWeek
        });
        return {
            error: false,
            bnbPrices: {
                current: parseFloat(data.current?.bnbPrice ?? '0'),
                oneDay: parseFloat(data.oneDay?.bnbPrice ?? '0'),
                twoDay: parseFloat(data.twoDay?.bnbPrice ?? '0'),
                week: parseFloat(data.oneWeek?.bnbPrice ?? '0')
            }
        };
    } catch (error) {
        console.error('Failed to fetch BNB prices', error);
        return {
            error: true,
            bnbPrices: undefined
        };
    }
};
/**
 * Returns BNB prices at current, 24h, 48h, and 7d intervals
 */ const useBnbPrices = ()=>{
    const { 0: prices , 1: setPrices  } = (0,external_react_.useState)();
    const { 0: error , 1: setError  } = (0,external_react_.useState)(false);
    const [t24, t48, tWeek] = (0,infoQueryHelpers/* getDeltaTimestamps */.z)();
    const { blocks , error: blockError  } = (0,useBlocksFromTimestamps/* useBlocksFromTimestamps */.Z)([
        t24,
        t48,
        tWeek
    ]);
    (0,external_react_.useEffect)(()=>{
        const fetch = async ()=>{
            const [block24, block48, blockWeek] = blocks;
            const { bnbPrices , error: fetchError  } = await fetchBnbPrices(block24.number, block48.number, blockWeek.number);
            if (fetchError) {
                setError(true);
            } else {
                setPrices(bnbPrices);
            }
        };
        if (!prices && !error && blocks && !blockError) {
            fetch();
        }
    }, [
        error,
        prices,
        blocks,
        blockError
    ]);
    return prices;
};

;// CONCATENATED MODULE: ./src/state/info/queries/tokens/tokenData.ts
/* eslint-disable no-param-reassign */ 






/**
 * Main token data to display on Token page
 */ const TOKEN_AT_BLOCK = (block, tokens)=>{
    const addressesString = `["${tokens.join('","')}"]`;
    const blockString = block ? `block: {number: ${block}}` : ``;
    return `tokens(
      where: {id_in: ${addressesString}}
      ${blockString}
      orderBy: tradeVolumeUSD
      orderDirection: desc
    ) {
      id
      symbol
      name
      derivedBNB
      derivedUSD
      tradeVolumeUSD
      totalTransactions
      totalLiquidity
    }
  `;
};
const fetchTokenData = async (block24h, block48h, block7d, block14d, tokenAddresses)=>{
    try {
        const query = external_graphql_request_.gql`
      query tokens {
        now: ${TOKEN_AT_BLOCK(null, tokenAddresses)}
        oneDayAgo: ${TOKEN_AT_BLOCK(block24h, tokenAddresses)}
        twoDaysAgo: ${TOKEN_AT_BLOCK(block48h, tokenAddresses)}
        oneWeekAgo: ${TOKEN_AT_BLOCK(block7d, tokenAddresses)}
        twoWeeksAgo: ${TOKEN_AT_BLOCK(block14d, tokenAddresses)}
      }
    `;
        const data = await graphql/* infoClient.request */.d.request(query);
        return {
            data,
            error: false
        };
    } catch (error) {
        console.error('Failed to fetch token data', error);
        return {
            error: true
        };
    }
};
// Transforms tokens into "0xADDRESS: { ...TokenFields }" format and cast strings to numbers
const parseTokenData = (tokens)=>{
    if (!tokens) {
        return {};
    }
    return tokens.reduce((accum, tokenData)=>{
        const { derivedBNB , derivedUSD , tradeVolumeUSD , totalTransactions , totalLiquidity  } = tokenData;
        accum[tokenData.id] = {
            ...tokenData,
            derivedBNB: parseFloat(derivedBNB),
            derivedUSD: parseFloat(derivedUSD),
            tradeVolumeUSD: parseFloat(tradeVolumeUSD),
            totalTransactions: parseFloat(totalTransactions),
            totalLiquidity: parseFloat(totalLiquidity)
        };
        return accum;
    }, {});
};
/**
 * Fetch top addresses by volume
 */ const useFetchedTokenDatas = (tokenAddresses)=>{
    const { 0: fetchState , 1: setFetchState  } = (0,external_react_.useState)({
        error: false
    });
    const [t24h, t48h, t7d, t14d] = (0,infoQueryHelpers/* getDeltaTimestamps */.z)();
    const { blocks , error: blockError  } = (0,useBlocksFromTimestamps/* useBlocksFromTimestamps */.Z)([
        t24h,
        t48h,
        t7d,
        t14d
    ]);
    const [block24h, block48h, block7d, block14d] = blocks ?? [];
    const bnbPrices = useBnbPrices();
    (0,external_react_.useEffect)(()=>{
        const fetch = async ()=>{
            const { error , data  } = await fetchTokenData(block24h.number, block48h.number, block7d.number, block14d.number, tokenAddresses);
            if (error) {
                setFetchState({
                    error: true
                });
            } else {
                const parsed = parseTokenData(data?.now);
                const parsed24 = parseTokenData(data?.oneDayAgo);
                const parsed48 = parseTokenData(data?.twoDaysAgo);
                const parsed7d = parseTokenData(data?.oneWeekAgo);
                const parsed14d = parseTokenData(data?.twoWeeksAgo);
                // Calculate data and format
                const formatted = tokenAddresses.reduce((accum, address)=>{
                    const current = parsed[address];
                    const oneDay = parsed24[address];
                    const twoDays = parsed48[address];
                    const week = parsed7d[address];
                    const twoWeeks = parsed14d[address];
                    const [volumeUSD, volumeUSDChange] = getChangeForPeriod(current?.tradeVolumeUSD, oneDay?.tradeVolumeUSD, twoDays?.tradeVolumeUSD);
                    const [volumeUSDWeek] = getChangeForPeriod(current?.tradeVolumeUSD, week?.tradeVolumeUSD, twoWeeks?.tradeVolumeUSD);
                    const liquidityUSD = current ? current.totalLiquidity * current.derivedUSD : 0;
                    const liquidityUSDOneDayAgo = oneDay ? oneDay.totalLiquidity * oneDay.derivedUSD : 0;
                    const liquidityUSDChange = getPercentChange(liquidityUSD, liquidityUSDOneDayAgo);
                    const liquidityToken = current ? current.totalLiquidity : 0;
                    // Prices of tokens for now, 24h ago and 7d ago
                    const priceUSD = current ? current.derivedBNB * bnbPrices.current : 0;
                    const priceUSDOneDay = oneDay ? oneDay.derivedBNB * bnbPrices.oneDay : 0;
                    const priceUSDWeek = week ? week.derivedBNB * bnbPrices.week : 0;
                    const priceUSDChange = getPercentChange(priceUSD, priceUSDOneDay);
                    const priceUSDChangeWeek = getPercentChange(priceUSD, priceUSDWeek);
                    const txCount = getAmountChange(current?.totalTransactions, oneDay?.totalTransactions);
                    accum[address] = {
                        exists: !!current,
                        address,
                        name: current ? current.name : '',
                        symbol: current ? current.symbol : '',
                        volumeUSD,
                        volumeUSDChange,
                        volumeUSDWeek,
                        txCount,
                        liquidityUSD,
                        liquidityUSDChange,
                        liquidityToken,
                        priceUSD,
                        priceUSDChange,
                        priceUSDChangeWeek
                    };
                    return accum;
                }, {});
                setFetchState({
                    data: formatted,
                    error: false
                });
            }
        };
        const allBlocksAvailable = block24h?.number && block48h?.number && block7d?.number && block14d?.number;
        if (tokenAddresses.length > 0 && allBlocksAvailable && !blockError && bnbPrices) {
            fetch();
        }
    }, [
        tokenAddresses,
        block24h,
        block48h,
        block7d,
        block14d,
        blockError,
        bnbPrices
    ]);
    return fetchState;
};
/* harmony default export */ const tokenData = (useFetchedTokenDatas);

;// CONCATENATED MODULE: ./src/state/info/queries/tokens/topTokens.ts





/**
 * Tokens to display on Home page
 * The actual data is later requested in tokenData.ts
 * Note: dailyTxns_gt: 300 is there to prevent fetching incorrectly priced tokens with high dailyVolumeUSD
 */ const fetchTopTokens = async (timestamp24hAgo)=>{
    try {
        const query = external_graphql_request_.gql`
      query topTokens($blacklist: [String!], $timestamp24hAgo: Int) {
        tokenDayDatas(
          first: 30
          where: { dailyTxns_gt: 300, id_not_in: $blacklist, date_gt: $timestamp24hAgo }
          orderBy: dailyVolumeUSD
          orderDirection: desc
        ) {
          id
        }
      }
    `;
        const data = await graphql/* infoClient.request */.d.request(query, {
            blacklist: info/* TOKEN_BLACKLIST */.tE,
            timestamp24hAgo
        });
        // tokenDayDatas id has compound id "0xTOKENADDRESS-NUMBERS", extracting token address with .split('-')
        return data.tokenDayDatas.map((t)=>t.id.split('-')[0]
        );
    } catch (error) {
        console.error('Failed to fetch top tokens', error);
        return [];
    }
};
/**
 * Fetch top addresses by volume
 */ const useTopTokenAddresses = ()=>{
    const { 0: topTokenAddresses , 1: setTopTokenAddresses  } = (0,external_react_.useState)([]);
    const [timestamp24hAgo] = (0,infoQueryHelpers/* getDeltaTimestamps */.z)();
    (0,external_react_.useEffect)(()=>{
        const fetch = async ()=>{
            const addresses = await fetchTopTokens(timestamp24hAgo);
            setTopTokenAddresses(addresses);
        };
        if (topTokenAddresses.length === 0) {
            fetch();
        }
    }, [
        topTokenAddresses,
        timestamp24hAgo
    ]);
    return topTokenAddresses;
};
/* harmony default export */ const topTokens = (useTopTokenAddresses);

// EXTERNAL MODULE: ./src/state/info/hooks.ts + 6 modules
var hooks = __webpack_require__(3985);
;// CONCATENATED MODULE: ./src/state/info/updaters.ts









const ProtocolUpdater = ()=>{
    const [protocolData, setProtocolData] = (0,hooks/* useProtocolData */.rf)();
    const { data: fetchedProtocolData , error  } = overview();
    const [chartData, updateChartData] = (0,hooks/* useProtocolChartData */.B5)();
    const { data: fetchedChartData , error: chartError  } = chart();
    const [transactions, updateTransactions] = (0,hooks/* useProtocolTransactions */.Wz)();
    // update overview data if available and not set
    (0,external_react_.useEffect)(()=>{
        if (protocolData === undefined && fetchedProtocolData && !error) {
            setProtocolData(fetchedProtocolData);
        }
    }, [
        error,
        fetchedProtocolData,
        protocolData,
        setProtocolData
    ]);
    // update global chart data if available and not set
    (0,external_react_.useEffect)(()=>{
        if (chartData === undefined && fetchedChartData && !chartError) {
            updateChartData(fetchedChartData);
        }
    }, [
        chartData,
        chartError,
        fetchedChartData,
        updateChartData
    ]);
    (0,external_react_.useEffect)(()=>{
        const fetch = async ()=>{
            const data = await protocol_transactions();
            if (data) {
                updateTransactions(data);
            }
        };
        if (!transactions) {
            fetch();
        }
    }, [
        transactions,
        updateTransactions
    ]);
    return null;
};
const PoolUpdater = ()=>{
    const updatePoolData = (0,hooks/* useUpdatePoolData */.b0)();
    const addPoolKeys = (0,hooks/* useAddPoolKeys */.KX)();
    const allPoolData = (0,hooks/* useAllPoolData */.JX)();
    const addresses = topPools();
    // add top pools on first load
    (0,external_react_.useEffect)(()=>{
        if (addresses.length > 0) {
            addPoolKeys(addresses);
        }
    }, [
        addPoolKeys,
        addresses
    ]);
    // detect for which addresses we havent loaded pool data yet
    const unfetchedPoolAddresses = (0,external_react_.useMemo)(()=>{
        return Object.keys(allPoolData).reduce((accum, address)=>{
            const poolData = allPoolData[address];
            if (!poolData.data) {
                accum.push(address);
            }
            return accum;
        }, []);
    }, [
        allPoolData
    ]);
    // fetch data for unfetched pools and update them
    const { error: poolDataError , data: poolDatas  } = poolData(unfetchedPoolAddresses);
    (0,external_react_.useEffect)(()=>{
        if (poolDatas && !poolDataError) {
            updatePoolData(Object.values(poolDatas));
        }
    }, [
        poolDataError,
        poolDatas,
        updatePoolData
    ]);
    return null;
};
const TokenUpdater = ()=>{
    const updateTokenDatas = (0,hooks/* useUpdateTokenData */.pV)();
    const addTokenKeys = (0,hooks/* useAddTokenKeys */.Mn)();
    const allTokenData = (0,hooks/* useAllTokenData */.jv)();
    const addresses = topTokens();
    // add top tokens on first load
    (0,external_react_.useEffect)(()=>{
        if (addresses.length > 0) {
            addTokenKeys(addresses);
        }
    }, [
        addTokenKeys,
        addresses
    ]);
    // detect for which addresses we havent loaded token data yet
    const unfetchedTokenAddresses = (0,external_react_.useMemo)(()=>{
        return Object.keys(allTokenData).reduce((accum, key)=>{
            const tokenData = allTokenData[key];
            if (!tokenData.data) {
                accum.push(key);
            }
            return accum;
        }, []);
    }, [
        allTokenData
    ]);
    // fetch data for unfetched tokens and update them
    const { error: tokenDataError , data: tokenDatas  } = tokenData(unfetchedTokenAddresses);
    (0,external_react_.useEffect)(()=>{
        if (tokenDatas && !tokenDataError) {
            updateTokenDatas(Object.values(tokenDatas));
        }
    }, [
        tokenDataError,
        tokenDatas,
        updateTokenDatas
    ]);
    return null;
};

// EXTERNAL MODULE: external "@pancakeswap/uikit"
var uikit_ = __webpack_require__(2829);
// EXTERNAL MODULE: ./src/components/NextLink.tsx
var NextLink = __webpack_require__(3629);
// EXTERNAL MODULE: ./src/contexts/Localization/index.tsx + 3 modules
var Localization = __webpack_require__(9150);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
;// CONCATENATED MODULE: ./src/state/info/queries/search/index.ts





const TOKEN_SEARCH = external_graphql_request_.gql`
  query tokens($symbol: String, $name: String, $id: String) {
    asSymbol: tokens(first: 10, where: { symbol_contains: $symbol }, orderBy: tradeVolumeUSD, orderDirection: desc) {
      id
    }
    asName: tokens(first: 10, where: { name_contains: $name }, orderBy: tradeVolumeUSD, orderDirection: desc) {
      id
    }
    asAddress: tokens(first: 1, where: { id: $id }, orderBy: tradeVolumeUSD, orderDirection: desc) {
      id
    }
  }
`;
const POOL_SEARCH = external_graphql_request_.gql`
  query pools($tokens: [Bytes]!, $id: String) {
    as0: pairs(first: 10, where: { token0_in: $tokens }) {
      id
    }
    as1: pairs(first: 10, where: { token1_in: $tokens }) {
      id
    }
    asAddress: pairs(first: 1, where: { id: $id }) {
      id
    }
  }
`;
const getIds = (entityArrays)=>{
    const ids = entityArrays.reduce((entities, currentTokenArray)=>[
            ...entities,
            ...currentTokenArray
        ]
    , []).map((entity)=>entity.id
    );
    return Array.from(new Set(ids));
};
const useFetchSearchResults = (searchString)=>{
    const { 0: searchResults , 1: setSearchResults  } = (0,external_react_.useState)({
        tokens: [],
        pools: [],
        loading: false,
        error: false
    });
    const searchStringTooShort = searchString.length < info/* MINIMUM_SEARCH_CHARACTERS */.OZ;
    // New value received, reset state
    (0,external_react_.useEffect)(()=>{
        setSearchResults({
            tokens: [],
            pools: [],
            loading: !searchStringTooShort,
            error: false
        });
    }, [
        searchString,
        searchStringTooShort
    ]);
    (0,external_react_.useEffect)(()=>{
        const search = async ()=>{
            try {
                const tokens = await graphql/* infoClient.request */.d.request(TOKEN_SEARCH, {
                    symbol: searchString.toUpperCase(),
                    // Most well known tokens have first letter capitalized
                    name: searchString.charAt(0).toUpperCase() + searchString.slice(1),
                    id: searchString.toLowerCase()
                });
                const tokenIds = getIds([
                    tokens.asAddress,
                    tokens.asSymbol,
                    tokens.asName
                ]);
                const pools = await graphql/* infoClient.request */.d.request(POOL_SEARCH, {
                    tokens: tokenIds,
                    id: searchString.toLowerCase()
                });
                setSearchResults({
                    tokens: tokenIds,
                    pools: getIds([
                        pools.asAddress,
                        pools.as0,
                        pools.as1
                    ]),
                    loading: false,
                    error: false
                });
            } catch (error) {
                console.error(`Search failed for ${searchString}`, error);
                setSearchResults({
                    tokens: [],
                    pools: [],
                    loading: false,
                    error: true
                });
            }
        };
        if (!searchStringTooShort) {
            search();
        }
    }, [
        searchString,
        searchStringTooShort
    ]);
    // Save ids to Redux
    // Token and Pool updater will then go fetch full data for these addresses
    // These hooks in turn will return data of tokens that have been fetched
    const tokenDatasFull = (0,hooks/* useTokenDatas */._n)(searchResults.tokens);
    const poolDatasFull = (0,hooks/* usePoolDatas */.zV)(searchResults.pools);
    // If above hooks returned not all tokens/pools it means
    // that some requests for full data are in progress
    const tokensLoading = tokenDatasFull.length !== searchResults.tokens.length || searchResults.loading;
    const poolsLoading = poolDatasFull.length !== searchResults.pools.length || searchResults.loading;
    return {
        tokens: tokenDatasFull,
        pools: poolDatasFull,
        tokensLoading,
        poolsLoading,
        error: searchResults.error
    };
};
/* harmony default export */ const search = (useFetchSearchResults);

// EXTERNAL MODULE: ./src/views/Info/components/CurrencyLogo/index.tsx + 1 modules
var CurrencyLogo = __webpack_require__(764);
// EXTERNAL MODULE: ./src/views/Info/utils/formatInfoNumbers.ts
var formatInfoNumbers = __webpack_require__(3589);
// EXTERNAL MODULE: ./src/state/user/hooks/index.tsx
var user_hooks = __webpack_require__(8605);
// EXTERNAL MODULE: ./src/views/Info/components/SaveIcon/index.tsx
var SaveIcon = __webpack_require__(3614);
// EXTERNAL MODULE: ./src/hooks/useDebounce.ts
var useDebounce = __webpack_require__(5999);
;// CONCATENATED MODULE: ./src/views/Info/components/InfoSearch/index.tsx














const Container = external_styled_components_default().div.withConfig({
    componentId: "sc-2e6f6e4b-0"
})`
  position: relative;
  z-index: 30;
  width: 100%;
`;
const StyledInput = external_styled_components_default()(uikit_.Input).withConfig({
    componentId: "sc-2e6f6e4b-1"
})`
  z-index: 9999;
  border: 1px solid ${({ theme  })=>theme.colors.inputSecondary
};
`;
const Menu = external_styled_components_default().div.withConfig({
    componentId: "sc-2e6f6e4b-2"
})`
  display: ${({ hide  })=>hide ? 'none' : 'flex'
};
  flex-direction: column;
  z-index: 9999;
  width: 100%;
  top: 50px;
  max-height: 400px;
  overflow: auto;
  right: 0;
  padding: 1.5rem;
  padding-bottom: 2.5rem;
  position: absolute;
  background: ${({ theme  })=>theme.colors.background
};
  border-radius: 8px;
  box-shadow: 0px 0px 1px rgba(0, 0, 0, 0.04), 0px 4px 8px rgba(0, 0, 0, 0.04), 0px 16px 24px rgba(0, 0, 0, 0.04),
    0px 24px 32px rgba(0, 0, 0, 0.04);
  border: 1px solid ${({ theme  })=>theme.colors.secondary
};
  margin-top: 4px;
  ${({ theme  })=>theme.mediaQueries.sm
} {
    margin-top: 0;
    width: 500px;
    max-height: 600px;
  }
  ${({ theme  })=>theme.mediaQueries.md
} {
    margin-top: 0;
    width: 800px;
    max-height: 600px;
  }
`;
const Blackout = external_styled_components_default().div.withConfig({
    componentId: "sc-2e6f6e4b-3"
})`
  position: absolute;
  min-height: 100vh;
  width: 100vw;
  z-index: 10;
  background-color: black;
  opacity: 0.7;
  left: 0;
  top: 0;
`;
const ResponsiveGrid = external_styled_components_default().div.withConfig({
    componentId: "sc-2e6f6e4b-4"
})`
  display: grid;
  grid-gap: 1em;
  grid-template-columns: 1fr;
  margin: 8px 0;
  align-items: center;
  ${({ theme  })=>theme.mediaQueries.sm
} {
    grid-template-columns: 1.5fr repeat(3, 1fr);
  }
`;
const Break = external_styled_components_default().div.withConfig({
    componentId: "sc-2e6f6e4b-5"
})`
  height: 1px;
  background-color: ${({ theme  })=>theme.colors.cardBorder
};
  width: 100%;
  margin: 16px 0;
`;
const HoverText = external_styled_components_default().div.withConfig({
    componentId: "sc-2e6f6e4b-6"
})`
  color: ${({ theme  })=>theme.colors.secondary
};
  display: ${({ hide  })=>hide ? 'none' : 'block'
};
  margin-top: 16px;
  :hover {
    cursor: pointer;
    opacity: 0.6;
  }
`;
const HoverRowLink = external_styled_components_default().div.withConfig({
    componentId: "sc-2e6f6e4b-7"
})`
  :hover {
    cursor: pointer;
    opacity: 0.6;
  }
`;
const OptionButton = external_styled_components_default().div.withConfig({
    componentId: "sc-2e6f6e4b-8"
})`
  width: fit-content;
  padding: 4px 8px;
  border-radius: 8px;
  display: flex;
  font-size: 12px;
  font-weight: 600;
  margin-right: 10px;
  justify-content: center;
  align-items: center;
  background-color: ${({ theme , enabled  })=>enabled ? theme.colors.primary : 'transparent'
};
  color: ${({ theme , enabled  })=>enabled ? theme.card.background : theme.colors.secondary
};
  :hover {
    opacity: 0.6;
    cursor: pointer;
  }
`;
const tokenIncludesSearchTerm = (token, value)=>{
    return token.address.toLowerCase().includes(value.toLowerCase()) || token.symbol.toLowerCase().includes(value.toLowerCase()) || token.name.toLowerCase().includes(value.toLowerCase());
};
const poolIncludesSearchTerm = (pool, value)=>{
    return pool.address.toLowerCase().includes(value.toLowerCase()) || tokenIncludesSearchTerm(pool.token0, value) || tokenIncludesSearchTerm(pool.token1, value);
};
const Search = ()=>{
    const router = (0,router_.useRouter)();
    const { isXs , isSm  } = (0,uikit_.useMatchBreakpoints)();
    const { t  } = (0,Localization/* useTranslation */.$G)();
    const inputRef = (0,external_react_.useRef)(null);
    const menuRef = (0,external_react_.useRef)(null);
    const showMoreRef = (0,external_react_.useRef)(null);
    const { 0: showMenu , 1: setShowMenu  } = (0,external_react_.useState)(false);
    const { 0: value , 1: setValue  } = (0,external_react_.useState)('');
    const debouncedSearchTerm = (0,useDebounce/* default */.Z)(value, 600);
    const { tokens , pools , tokensLoading , poolsLoading , error  } = search(debouncedSearchTerm);
    const { 0: tokensShown , 1: setTokensShown  } = (0,external_react_.useState)(3);
    const { 0: poolsShown , 1: setPoolsShown  } = (0,external_react_.useState)(3);
    (0,external_react_.useEffect)(()=>{
        setTokensShown(3);
        setPoolsShown(3);
    }, [
        debouncedSearchTerm
    ]);
    const handleOutsideClick = (e)=>{
        const menuClick = menuRef.current && menuRef.current.contains(e.target);
        const inputCLick = inputRef.current && inputRef.current.contains(e.target);
        const showMoreClick = showMoreRef.current && showMoreRef.current.contains(e.target);
        if (!menuClick && !inputCLick && !showMoreClick) {
            setPoolsShown(3);
            setTokensShown(3);
            setShowMenu(false);
        }
    };
    (0,external_react_.useEffect)(()=>{
        if (showMenu) {
            document.addEventListener('click', handleOutsideClick);
            document.querySelector('body').style.overflow = 'hidden';
        } else {
            document.removeEventListener('click', handleOutsideClick);
            document.querySelector('body').style.overflow = 'visible';
        }
        return ()=>{
            document.removeEventListener('click', handleOutsideClick);
        };
    }, [
        showMenu
    ]);
    // watchlist
    const [savedTokens, addSavedToken] = (0,user_hooks/* useWatchlistTokens */.z6)();
    const [savedPools, addSavedPool] = (0,user_hooks/* useWatchlistPools */.Hx)();
    const handleItemClick = (to)=>{
        setShowMenu(false);
        setPoolsShown(3);
        setTokensShown(3);
        router.push(to);
    };
    // get date for watchlist
    const watchListTokenData = (0,hooks/* useTokenDatas */._n)(savedTokens);
    const watchListTokenLoading = watchListTokenData.length !== savedTokens.length;
    const watchListPoolData = (0,hooks/* usePoolDatas */.zV)(savedPools);
    const watchListPoolLoading = watchListPoolData.length !== savedPools.length;
    // filter on view
    const { 0: showWatchlist , 1: setShowWatchlist  } = (0,external_react_.useState)(false);
    const tokensForList = (0,external_react_.useMemo)(()=>{
        if (showWatchlist) {
            return watchListTokenData.filter((token)=>tokenIncludesSearchTerm(token, value)
            );
        }
        return tokens.sort((t0, t1)=>t0.volumeUSD > t1.volumeUSD ? -1 : 1
        );
    }, [
        showWatchlist,
        tokens,
        watchListTokenData,
        value
    ]);
    const poolForList = (0,external_react_.useMemo)(()=>{
        if (showWatchlist) {
            return watchListPoolData.filter((pool)=>poolIncludesSearchTerm(pool, value)
            );
        }
        return pools.sort((p0, p1)=>p0.volumeUSD > p1.volumeUSD ? -1 : 1
        );
    }, [
        pools,
        showWatchlist,
        watchListPoolData,
        value
    ]);
    const contentUnderTokenList = ()=>{
        const isLoading = showWatchlist ? watchListTokenLoading : tokensLoading;
        const noTokensFound = tokensForList.length === 0 && !isLoading && debouncedSearchTerm.length >= info/* MINIMUM_SEARCH_CHARACTERS */.OZ;
        const noWatchlistTokens = tokensForList.length === 0 && !isLoading;
        const showMessage = showWatchlist ? noWatchlistTokens : noTokensFound;
        const noTokensMessage = showWatchlist ? t('Saved tokens will appear here') : t('No results');
        return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
            children: [
                isLoading && /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Skeleton, {}),
                showMessage && /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                    children: noTokensMessage
                }),
                !showWatchlist && debouncedSearchTerm.length < info/* MINIMUM_SEARCH_CHARACTERS */.OZ && /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                    children: t('Search pools or tokens')
                })
            ]
        }));
    };
    const contentUnderPoolList = ()=>{
        const isLoading = showWatchlist ? watchListPoolLoading : poolsLoading;
        const noPoolsFound = poolForList.length === 0 && !poolsLoading && debouncedSearchTerm.length >= info/* MINIMUM_SEARCH_CHARACTERS */.OZ;
        const noWatchlistPools = poolForList.length === 0 && !isLoading;
        const showMessage = showWatchlist ? noWatchlistPools : noPoolsFound;
        const noPoolsMessage = showWatchlist ? t('Saved tokens will appear here') : t('No results');
        return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
            children: [
                isLoading && /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Skeleton, {}),
                showMessage && /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                    children: noPoolsMessage
                }),
                !showWatchlist && debouncedSearchTerm.length < info/* MINIMUM_SEARCH_CHARACTERS */.OZ && /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                    children: t('Search pools or tokens')
                })
            ]
        }));
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            showMenu ? /*#__PURE__*/ jsx_runtime_.jsx(Blackout, {}) : null,
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(Container, {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(StyledInput, {
                        type: "text",
                        value: value,
                        onChange: (e)=>{
                            setValue(e.target.value);
                        },
                        placeholder: t('Search pools or tokens'),
                        ref: inputRef,
                        onFocus: ()=>{
                            setShowMenu(true);
                        }
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)(Menu, {
                        hide: !showMenu,
                        ref: menuRef,
                        children: [
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                                mb: "16px",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx(OptionButton, {
                                        enabled: !showWatchlist,
                                        onClick: ()=>setShowWatchlist(false)
                                        ,
                                        children: t('Search')
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx(OptionButton, {
                                        enabled: showWatchlist,
                                        onClick: ()=>setShowWatchlist(true)
                                        ,
                                        children: t('Watchlist')
                                    })
                                ]
                            }),
                            error && /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                color: "failure",
                                children: t('Error occurred, please try again')
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(ResponsiveGrid, {
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                        bold: true,
                                        color: "secondary",
                                        children: t('Tokens')
                                    }),
                                    !isXs && !isSm && /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                        textAlign: "end",
                                        fontSize: "12px",
                                        children: t('Price')
                                    }),
                                    !isXs && !isSm && /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                        textAlign: "end",
                                        fontSize: "12px",
                                        children: t('Volume 24H')
                                    }),
                                    !isXs && !isSm && /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                        textAlign: "end",
                                        fontSize: "12px",
                                        children: t('Liquidity')
                                    })
                                ]
                            }),
                            tokensForList.slice(0, tokensShown).map((token, i)=>{
                                return(// eslint-disable-next-line react/no-array-index-key
                                /*#__PURE__*/ jsx_runtime_.jsx(HoverRowLink, {
                                    onClick: ()=>handleItemClick(`/info/token/${token.address}`)
                                    ,
                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(ResponsiveGrid, {
                                        children: [
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx(CurrencyLogo/* CurrencyLogo */.X, {
                                                        address: token.address
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                                        ml: "10px",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                                            children: `${token.name} (${token.symbol})`
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx(SaveIcon/* default */.Z, {
                                                        id: "watchlist-icon",
                                                        style: {
                                                            marginLeft: '8px'
                                                        },
                                                        fill: savedTokens.includes(token.address),
                                                        onClick: (e)=>{
                                                            e.stopPropagation();
                                                            addSavedToken(token.address);
                                                        }
                                                    })
                                                ]
                                            }),
                                            !isXs && !isSm && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Text, {
                                                textAlign: "end",
                                                children: [
                                                    "$",
                                                    (0,formatInfoNumbers/* formatAmount */.d)(token.priceUSD)
                                                ]
                                            }),
                                            !isXs && !isSm && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Text, {
                                                textAlign: "end",
                                                children: [
                                                    "$",
                                                    (0,formatInfoNumbers/* formatAmount */.d)(token.volumeUSD)
                                                ]
                                            }),
                                            !isXs && !isSm && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Text, {
                                                textAlign: "end",
                                                children: [
                                                    "$",
                                                    (0,formatInfoNumbers/* formatAmount */.d)(token.liquidityUSD)
                                                ]
                                            })
                                        ]
                                    })
                                }, i));
                            }),
                            contentUnderTokenList(),
                            /*#__PURE__*/ jsx_runtime_.jsx(HoverText, {
                                onClick: ()=>{
                                    setTokensShown(tokensShown + 5);
                                },
                                hide: tokensForList.length <= tokensShown,
                                ref: showMoreRef,
                                children: t('See more...')
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(Break, {}),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(ResponsiveGrid, {
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                        bold: true,
                                        color: "secondary",
                                        mb: "8px",
                                        children: t('Pools')
                                    }),
                                    !isXs && !isSm && /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                        textAlign: "end",
                                        fontSize: "12px",
                                        children: t('Volume 24H')
                                    }),
                                    !isXs && !isSm && /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                        textAlign: "end",
                                        fontSize: "12px",
                                        children: t('Volume 7D')
                                    }),
                                    !isXs && !isSm && /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                        textAlign: "end",
                                        fontSize: "12px",
                                        children: t('Liquidity')
                                    })
                                ]
                            }),
                            poolForList.slice(0, poolsShown).map((p, i)=>{
                                return(// eslint-disable-next-line react/no-array-index-key
                                /*#__PURE__*/ jsx_runtime_.jsx(HoverRowLink, {
                                    onClick: ()=>handleItemClick(`/info/pool/${p.address}`)
                                    ,
                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(ResponsiveGrid, {
                                        children: [
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx(CurrencyLogo/* DoubleCurrencyLogo */.g, {
                                                        address0: p.token0.address,
                                                        address1: p.token1.address
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                                        ml: "10px",
                                                        style: {
                                                            whiteSpace: 'nowrap'
                                                        },
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                                                            children: `${p.token0.symbol} / ${p.token1.symbol}`
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx(SaveIcon/* default */.Z, {
                                                        id: "watchlist-icon",
                                                        style: {
                                                            marginLeft: '10px'
                                                        },
                                                        fill: savedPools.includes(p.address),
                                                        onClick: (e)=>{
                                                            e.stopPropagation();
                                                            addSavedPool(p.address);
                                                        }
                                                    })
                                                ]
                                            }),
                                            !isXs && !isSm && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Text, {
                                                textAlign: "end",
                                                children: [
                                                    "$",
                                                    (0,formatInfoNumbers/* formatAmount */.d)(p.volumeUSD)
                                                ]
                                            }),
                                            !isXs && !isSm && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Text, {
                                                textAlign: "end",
                                                children: [
                                                    "$",
                                                    (0,formatInfoNumbers/* formatAmount */.d)(p.volumeUSDWeek)
                                                ]
                                            }),
                                            !isXs && !isSm && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Text, {
                                                textAlign: "end",
                                                children: [
                                                    "$",
                                                    (0,formatInfoNumbers/* formatAmount */.d)(p.liquidityUSD)
                                                ]
                                            })
                                        ]
                                    })
                                }, i));
                            }),
                            contentUnderPoolList(),
                            /*#__PURE__*/ jsx_runtime_.jsx(HoverText, {
                                onClick: ()=>{
                                    setPoolsShown(poolsShown + 5);
                                },
                                hide: poolForList.length <= poolsShown,
                                ref: showMoreRef,
                                children: t('See more...')
                            })
                        ]
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const InfoSearch = (Search);

;// CONCATENATED MODULE: ./src/views/Info/components/InfoNav/index.tsx








const NavWrapper = external_styled_components_default()(uikit_.Flex).withConfig({
    componentId: "sc-47fb6527-0"
})`
  background: ${({ theme  })=>theme.colors.gradients.cardHeader
};
  justify-content: space-between;
  padding: 20px 16px;
  flex-direction: column;
  gap: 8px;
  ${({ theme  })=>theme.mediaQueries.sm
} {
    padding: 20px 40px;
    flex-direction: row;
  }
`;
const InfoNav = ()=>{
    const { t  } = (0,Localization/* useTranslation */.$G)();
    const router = (0,router_.useRouter)();
    const isPools = router.asPath === '/info/pools';
    const isTokens = router.asPath === '/info/tokens';
    let activeIndex = 0;
    if (isPools) {
        activeIndex = 1;
    }
    if (isTokens) {
        activeIndex = 2;
    }
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(NavWrapper, {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Box, {
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.ButtonMenu, {
                    activeIndex: activeIndex,
                    scale: "sm",
                    variant: "subtle",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.ButtonMenuItem, {
                            as: NextLink/* NextLinkFromReactRouter */.a,
                            to: "/info",
                            children: t('Overview')
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.ButtonMenuItem, {
                            as: NextLink/* NextLinkFromReactRouter */.a,
                            to: "/info/pools",
                            children: t('Pools')
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(uikit_.ButtonMenuItem, {
                            as: NextLink/* NextLinkFromReactRouter */.a,
                            to: "/info/tokens",
                            children: t('Tokens')
                        })
                    ]
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Box, {
                width: [
                    '100%',
                    '100%',
                    '250px'
                ],
                children: /*#__PURE__*/ jsx_runtime_.jsx(InfoSearch, {})
            })
        ]
    }));
};
/* harmony default export */ const components_InfoNav = (InfoNav);

;// CONCATENATED MODULE: ./src/views/Info/index.tsx




const InfoPageLayout = ({ children  })=>{
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(ProtocolUpdater, {}),
            /*#__PURE__*/ jsx_runtime_.jsx(PoolUpdater, {}),
            /*#__PURE__*/ jsx_runtime_.jsx(TokenUpdater, {}),
            /*#__PURE__*/ jsx_runtime_.jsx(components_InfoNav, {}),
            children
        ]
    }));
};


/***/ })

};
;
//# sourceMappingURL=24.js.map