"use strict";
exports.id = 275;
exports.ids = [275];
exports.modules = {

/***/ 5275:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ InfoCharts_ChartCard)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "@pancakeswap/uikit"
var uikit_ = __webpack_require__(2829);
// EXTERNAL MODULE: ./src/views/Info/components/InfoCharts/LineChart/index.tsx
var LineChart = __webpack_require__(5091);
// EXTERNAL MODULE: ./src/views/Info/components/InfoCharts/BarChart/index.tsx
var BarChart = __webpack_require__(5224);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
;// CONCATENATED MODULE: ./src/components/TabToggle/index.tsx




const Wrapper = external_styled_components_default()(uikit_.Flex).withConfig({
    componentId: "sc-e5d8cc08-0"
})`
  overflow-x: scroll;
  padding: 0;
  border-radius: 24px 24px 0 0;
  ::-webkit-scrollbar {
    display: none;
  }
  scrollbar-width: none; /* Firefox */
`;
const Inner = external_styled_components_default()(uikit_.Flex).withConfig({
    componentId: "sc-e5d8cc08-1"
})`
  justify-content: space-between;
  background-color: ${({ theme  })=>theme.colors.input
};
  width: 100%;
`;
const TabToggle = external_styled_components_default().button.withConfig({
    componentId: "sc-e5d8cc08-2"
})`
  display: inline-flex;
  justify-content: center;
  cursor: pointer;
  flex: 1;
  border: 0;
  outline: 0;
  padding: 16px;
  margin: 0;
  border-radius: 24px 24px 0 0;
  font-size: 16px;
  font-weight: 600;
  color: ${({ theme , isActive  })=>isActive ? theme.colors.text : theme.colors.textSubtle
};
  background-color: ${({ theme , isActive  })=>isActive ? theme.card.background : theme.colors.input
};
`;
const TabToggleGroup = ({ children  })=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx(Wrapper, {
        p: [
            '0 4px',
            '0 16px'
        ],
        children: /*#__PURE__*/ jsx_runtime_.jsx(Inner, {
            children: children
        })
    }));
};

// EXTERNAL MODULE: ./src/contexts/Localization/index.tsx + 3 modules
var Localization = __webpack_require__(9150);
// EXTERNAL MODULE: ./src/views/Info/utils/formatInfoNumbers.ts
var formatInfoNumbers = __webpack_require__(3589);
// EXTERNAL MODULE: external "date-fns"
var external_date_fns_ = __webpack_require__(4146);
// EXTERNAL MODULE: ./node_modules/next/dynamic.js
var dynamic = __webpack_require__(5152);
;// CONCATENATED MODULE: ./src/views/Info/components/InfoCharts/ChartCard/index.tsx










const CandleChart = (0,dynamic["default"])(null, {
    loadableGenerated: {
        modules: [
            "../views/Info/components/InfoCharts/ChartCard/index.tsx -> " + "views/Info/components/InfoCharts/CandleChart"
        ]
    },
    ssr: false
});
var ChartView;
(function(ChartView) {
    ChartView[ChartView["LIQUIDITY"] = 0] = "LIQUIDITY";
    ChartView[ChartView["VOLUME"] = 1] = "VOLUME";
    ChartView[ChartView["PRICE"] = 2] = "PRICE";
})(ChartView || (ChartView = {}));
const ChartCard = ({ variant , chartData , tokenData , tokenPriceData  })=>{
    const { 0: view , 1: setView  } = (0,external_react_.useState)(ChartView.VOLUME);
    const { 0: hoverValue , 1: setHoverValue  } = (0,external_react_.useState)();
    const { 0: hoverDate , 1: setHoverDate  } = (0,external_react_.useState)();
    const { t , currentLanguage: { locale  } ,  } = (0,Localization/* useTranslation */.$G)();
    const currentDate = new Date().toLocaleString(locale, {
        month: 'short',
        year: 'numeric',
        day: 'numeric'
    });
    const formattedTvlData = (0,external_react_.useMemo)(()=>{
        if (chartData) {
            return chartData.map((day)=>{
                return {
                    time: (0,external_date_fns_.fromUnixTime)(day.date),
                    value: day.liquidityUSD
                };
            });
        }
        return [];
    }, [
        chartData
    ]);
    const formattedVolumeData = (0,external_react_.useMemo)(()=>{
        if (chartData) {
            return chartData.map((day)=>{
                return {
                    time: (0,external_date_fns_.fromUnixTime)(day.date),
                    value: day.volumeUSD
                };
            });
        }
        return [];
    }, [
        chartData
    ]);
    const getLatestValueDisplay = ()=>{
        let valueToDisplay = null;
        if (hoverValue) {
            valueToDisplay = (0,formatInfoNumbers/* formatAmount */.d)(hoverValue);
        } else if (view === ChartView.VOLUME && formattedVolumeData.length > 0) {
            valueToDisplay = (0,formatInfoNumbers/* formatAmount */.d)(formattedVolumeData[formattedVolumeData.length - 1]?.value);
        } else if (view === ChartView.LIQUIDITY && formattedTvlData.length > 0) {
            valueToDisplay = (0,formatInfoNumbers/* formatAmount */.d)(formattedTvlData[formattedTvlData.length - 1]?.value);
        } else if (view === ChartView.PRICE && tokenData?.priceUSD) {
            valueToDisplay = (0,formatInfoNumbers/* formatAmount */.d)(tokenData.priceUSD);
        }
        return valueToDisplay ? /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Text, {
            fontSize: "24px",
            bold: true,
            children: [
                "$",
                valueToDisplay
            ]
        }) : /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Skeleton, {
            height: "36px",
            width: "128px"
        });
    };
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Card, {
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(TabToggleGroup, {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(TabToggle, {
                        isActive: view === ChartView.VOLUME,
                        onClick: ()=>setView(ChartView.VOLUME)
                        ,
                        children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                            children: t('Volume')
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(TabToggle, {
                        isActive: view === ChartView.LIQUIDITY,
                        onClick: ()=>setView(ChartView.LIQUIDITY)
                        ,
                        children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                            children: t('Liquidity')
                        })
                    }),
                    variant === 'token' && /*#__PURE__*/ jsx_runtime_.jsx(TabToggle, {
                        isActive: view === ChartView.PRICE,
                        onClick: ()=>setView(ChartView.PRICE)
                        ,
                        children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                            children: t('Price')
                        })
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Flex, {
                flexDirection: "column",
                px: "24px",
                pt: "24px",
                children: [
                    getLatestValueDisplay(),
                    /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                        small: true,
                        color: "secondary",
                        children: hoverDate || currentDate
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Box, {
                px: "24px",
                height: variant === 'token' ? '250px' : '335px',
                children: view === ChartView.LIQUIDITY ? /*#__PURE__*/ jsx_runtime_.jsx(LineChart/* default */.Z, {
                    data: formattedTvlData,
                    setHoverValue: setHoverValue,
                    setHoverDate: setHoverDate
                }) : view === ChartView.VOLUME ? /*#__PURE__*/ jsx_runtime_.jsx(BarChart/* default */.Z, {
                    data: formattedVolumeData,
                    setHoverValue: setHoverValue,
                    setHoverDate: setHoverDate
                }) : view === ChartView.PRICE ? /*#__PURE__*/ jsx_runtime_.jsx(CandleChart, {
                    data: tokenPriceData,
                    setValue: setHoverValue,
                    setLabel: setHoverDate
                }) : null
            })
        ]
    }));
};
/* harmony default export */ const InfoCharts_ChartCard = (ChartCard);


/***/ })

};
;
//# sourceMappingURL=275.js.map