"use strict";
exports.id = 54;
exports.ids = [54];
exports.modules = {

/***/ 7910:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "l": () => (/* binding */ filterTokens),
/* harmony export */   "T": () => (/* binding */ useSortedTokensByQuery)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(8328);


function filterTokens(tokens, search) {
    if (search.length === 0) return tokens;
    const searchingAddress = (0,_utils__WEBPACK_IMPORTED_MODULE_1__/* .isAddress */ .UJ)(search);
    if (searchingAddress) {
        return tokens.filter((token)=>token.address === searchingAddress
        );
    }
    const lowerSearchParts = search.toLowerCase().split(/\s+/).filter((s)=>s.length > 0
    );
    if (lowerSearchParts.length === 0) {
        return tokens;
    }
    const matchesSearch = (s)=>{
        const sParts = s.toLowerCase().split(/\s+/).filter((s_)=>s_.length > 0
        );
        return lowerSearchParts.every((p)=>p.length === 0 || sParts.some((sp)=>sp.startsWith(p) || sp.endsWith(p)
            )
        );
    };
    return tokens.filter((token)=>{
        const { symbol , name  } = token;
        return symbol && matchesSearch(symbol) || name && matchesSearch(name);
    });
}
function useSortedTokensByQuery(tokens, searchQuery) {
    return (0,react__WEBPACK_IMPORTED_MODULE_0__.useMemo)(()=>{
        if (!tokens) {
            return [];
        }
        const symbolMatch = searchQuery.toLowerCase().split(/\s+/).filter((s)=>s.length > 0
        );
        if (symbolMatch.length > 1) {
            return tokens;
        }
        const exactMatches = [];
        const symbolSubstrings = [];
        const rest = [];
        // sort tokens by exact match -> substring on symbol match -> rest
        tokens.forEach((token)=>{
            if (token.symbol?.toLowerCase() === symbolMatch[0]) {
                return exactMatches.push(token);
            }
            if (token.symbol?.toLowerCase().startsWith(searchQuery.toLowerCase().trim())) {
                return symbolSubstrings.push(token);
            }
            return rest.push(token);
        });
        return [
            ...exactMatches,
            ...symbolSubstrings,
            ...rest
        ];
    }, [
        tokens,
        searchQuery
    ]);
}


/***/ }),

/***/ 5169:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Jz": () => (/* reexport */ erc20_bytes32_namespaceObject),
  "ZP": () => (/* binding */ abi_erc20)
});

// UNUSED EXPORTS: ERC20_ABI, ERC20_BYTES32_INTERFACE

// EXTERNAL MODULE: external "@ethersproject/abi"
var abi_ = __webpack_require__(6187);
// EXTERNAL MODULE: ./src/config/abi/erc20.json
var erc20 = __webpack_require__(3324);
;// CONCATENATED MODULE: ./src/config/abi/erc20_bytes32.json
const erc20_bytes32_namespaceObject = JSON.parse('[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"}]');
;// CONCATENATED MODULE: ./src/config/abi/erc20.ts



const ERC20_INTERFACE = new abi_.Interface(erc20);
const ERC20_BYTES32_INTERFACE = new abi_.Interface(erc20_bytes32_namespaceObject);
/* harmony default export */ const abi_erc20 = (ERC20_INTERFACE);



/***/ }),

/***/ 4003:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OZ": () => (/* binding */ MINIMUM_SEARCH_CHARACTERS),
/* harmony export */   "MV": () => (/* binding */ WEEKS_IN_YEAR),
/* harmony export */   "om": () => (/* binding */ TOTAL_FEE),
/* harmony export */   "BY": () => (/* binding */ LP_HOLDERS_FEE),
/* harmony export */   "Rr": () => (/* binding */ PCS_V2_START),
/* harmony export */   "Bq": () => (/* binding */ ONE_DAY_UNIX),
/* harmony export */   "Tb": () => (/* binding */ ONE_HOUR_SECONDS),
/* harmony export */   "si": () => (/* binding */ ITEMS_PER_INFO_TABLE_PAGE),
/* harmony export */   "tE": () => (/* binding */ TOKEN_BLACKLIST)
/* harmony export */ });
/* unused harmony exports TREASURY_FEE, BUYBACK_FEE */
const MINIMUM_SEARCH_CHARACTERS = 2;
const WEEKS_IN_YEAR = 52.1429;
const TOTAL_FEE = 0.0025;
const LP_HOLDERS_FEE = 0.0017;
const TREASURY_FEE = 0.0003;
const BUYBACK_FEE = 0.0005;
const PCS_V2_START = 1619136000 // April 23, 2021, 12:00:00 AM
;
const ONE_DAY_UNIX = 86400 // 24h * 60m * 60s
;
const ONE_HOUR_SECONDS = 3600;
const ITEMS_PER_INFO_TABLE_PAGE = 10;
// These tokens are either incorrectly priced or have some other issues that spoil the query data
// None of them present any interest as they have almost 0 daily trade volume
const TOKEN_BLACKLIST = [
    // These ones are copied from v1 info
    '0x495c7f3a713870f68f8b418b355c085dfdc412c3',
    '0xc3761eb917cd790b30dad99f6cc5b4ff93c4f9ea',
    '0xe31debd7abff90b06bca21010dd860d8701fd901',
    '0xfc989fbb6b3024de5ca0144dc23c18a063942ac1',
    '0xe40fc6ff5f2895b44268fd2e1a421e07f567e007',
    '0xfd158609228b43aa380140b46fff3cdf9ad315de',
    '0xc00af6212fcf0e6fd3143e692ccd4191dc308bea',
    '0x205969b3ad459f7eba0dee07231a6357183d3fb6',
    '0x0bd67d358636fd7b0597724aa4f20beedbf3073a',
    '0xedf5d2a561e8a3cb5a846fbce24d2ccd88f50075',
    '0x702b0789a3d4dade1688a0c8b7d944e5ba80fc30',
    '0x041929a760d7049edaef0db246fa76ec975e90cc',
    '0xba098df8c6409669f5e6ec971ac02cd5982ac108',
    '0x1bbed115afe9e8d6e9255f18ef10d43ce6608d94',
    '0xe99512305bf42745fae78003428dcaf662afb35d',
    '0xbE609EAcbFca10F6E5504D39E3B113F808389056',
    '0x847daf9dfdc22d5c61c4a857ec8733ef5950e82e',
    '0xdbf8913dfe14536c0dae5dd06805afb2731f7e7b',
    // These ones are newly found
    '0xF1D50dB2C40b63D2c598e2A808d1871a40b1E653',
    '0x4269e4090ff9dfc99d8846eb0d42e67f01c3ac8b', 
];


/***/ }),

/***/ 6435:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "e_": () => (/* binding */ useAllTokens),
/* harmony export */   "EK": () => (/* binding */ useAllInactiveTokens),
/* harmony export */   "l6": () => (/* binding */ useUnsupportedTokens),
/* harmony export */   "ZN": () => (/* binding */ useIsTokenActive),
/* harmony export */   "FM": () => (/* binding */ useFoundOnInactiveList),
/* harmony export */   "EH": () => (/* binding */ useIsUserAddedToken),
/* harmony export */   "dQ": () => (/* binding */ useToken),
/* harmony export */   "U8": () => (/* binding */ useCurrency)
/* harmony export */ });
/* unused harmony export useDefaultTokens */
/* harmony import */ var _ethersproject_strings__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9213);
/* harmony import */ var _ethersproject_strings__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_ethersproject_strings__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(543);
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _ethersproject_bytes__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9935);
/* harmony import */ var _ethersproject_bytes__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_ethersproject_bytes__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var hooks_useActiveWeb3React__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4011);
/* harmony import */ var _state_lists_hooks__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(7952);
/* harmony import */ var _state_multicall_hooks__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1001);
/* harmony import */ var _state_user_hooks_useUserAddedTokens__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(4797);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(8328);
/* harmony import */ var _useContract__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(6063);
/* harmony import */ var _components_SearchModal_filtering__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(7910);
/* eslint-disable no-param-reassign */ 










// reduce token map into standard address <-> Token mapping, optionally include user added tokens
function useTokensFromMap(tokenMap, includeUserAdded) {
    const { chainId  } = (0,hooks_useActiveWeb3React__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z)();
    const userAddedTokens = (0,_state_user_hooks_useUserAddedTokens__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z)();
    return (0,react__WEBPACK_IMPORTED_MODULE_2__.useMemo)(()=>{
        if (!chainId) return {};
        // reduce to just tokens
        const mapWithoutUrls = Object.keys(tokenMap[chainId]).reduce((newMap, address)=>{
            newMap[address] = tokenMap[chainId][address].token;
            return newMap;
        }, {});
        if (includeUserAdded) {
            return userAddedTokens// reduce into all ALL_TOKENS filtered by the current chain
            .reduce((tokenMap_, token)=>{
                tokenMap_[token.address] = token;
                return tokenMap_;
            }, // must make a copy because reduce modifies the map, and we do not
            // want to make a copy in every iteration
            {
                ...mapWithoutUrls
            });
        }
        return mapWithoutUrls;
    }, [
        chainId,
        userAddedTokens,
        tokenMap,
        includeUserAdded
    ]);
}
function useDefaultTokens() {
    const defaultList = useDefaultTokenList();
    return useTokensFromMap(defaultList, false);
}
function useAllTokens() {
    const allTokens = (0,_state_lists_hooks__WEBPACK_IMPORTED_MODULE_5__/* .useCombinedActiveList */ .z0)();
    return useTokensFromMap(allTokens, true);
}
function useAllInactiveTokens() {
    // get inactive tokens
    const inactiveTokensMap = (0,_state_lists_hooks__WEBPACK_IMPORTED_MODULE_5__/* .useCombinedInactiveList */ .qB)();
    const inactiveTokens = useTokensFromMap(inactiveTokensMap, false);
    // filter out any token that are on active list
    const activeTokensAddresses = Object.keys(useAllTokens());
    const filteredInactive = activeTokensAddresses ? Object.keys(inactiveTokens).reduce((newMap, address)=>{
        if (!activeTokensAddresses.includes(address)) {
            newMap[address] = inactiveTokens[address];
        }
        return newMap;
    }, {}) : inactiveTokens;
    return filteredInactive;
}
function useUnsupportedTokens() {
    const unsupportedTokensMap = (0,_state_lists_hooks__WEBPACK_IMPORTED_MODULE_5__/* .useUnsupportedTokenList */ .Rx)();
    return useTokensFromMap(unsupportedTokensMap, false);
}
function useIsTokenActive(token) {
    const activeTokens = useAllTokens();
    if (!activeTokens || !token) {
        return false;
    }
    return !!activeTokens[token.address];
}
// used to detect extra search results
function useFoundOnInactiveList(searchQuery) {
    const { chainId  } = (0,hooks_useActiveWeb3React__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z)();
    const inactiveTokens = useAllInactiveTokens();
    return (0,react__WEBPACK_IMPORTED_MODULE_2__.useMemo)(()=>{
        if (!chainId || searchQuery === '') {
            return undefined;
        }
        const tokens = (0,_components_SearchModal_filtering__WEBPACK_IMPORTED_MODULE_10__/* .filterTokens */ .l)(Object.values(inactiveTokens), searchQuery);
        return tokens;
    }, [
        chainId,
        inactiveTokens,
        searchQuery
    ]);
}
// Check if currency is included in custom list from user storage
function useIsUserAddedToken(currency) {
    const userAddedTokens = (0,_state_user_hooks_useUserAddedTokens__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z)();
    if (!currency) {
        return false;
    }
    return !!userAddedTokens.find((token)=>(0,_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_1__.currencyEquals)(currency, token)
    );
}
// parse a name or symbol from a token response
const BYTES32_REGEX = /^0x[a-fA-F0-9]{64}$/;
function parseStringOrBytes32(str, bytes32, defaultValue) {
    return str && str.length > 0 ? str : bytes32 && BYTES32_REGEX.test(bytes32) && (0,_ethersproject_bytes__WEBPACK_IMPORTED_MODULE_3__.arrayify)(bytes32)[31] === 0 ? (0,_ethersproject_strings__WEBPACK_IMPORTED_MODULE_0__.parseBytes32String)(bytes32) : defaultValue;
}
// undefined if invalid or does not exist
// null if loading
// otherwise returns the token
function useToken(tokenAddress) {
    const { chainId  } = (0,hooks_useActiveWeb3React__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z)();
    const tokens = useAllTokens();
    const address = (0,_utils__WEBPACK_IMPORTED_MODULE_8__/* .isAddress */ .UJ)(tokenAddress);
    const tokenContract = (0,_useContract__WEBPACK_IMPORTED_MODULE_9__/* .useTokenContract */ .Ib)(address || undefined, false);
    const tokenContractBytes32 = (0,_useContract__WEBPACK_IMPORTED_MODULE_9__/* .useBytes32TokenContract */ .gs)(address || undefined, false);
    const token = address ? tokens[address] : undefined;
    const tokenName = (0,_state_multicall_hooks__WEBPACK_IMPORTED_MODULE_6__/* .useSingleCallResult */ .Wk)(token ? undefined : tokenContract, 'name', undefined, _state_multicall_hooks__WEBPACK_IMPORTED_MODULE_6__/* .NEVER_RELOAD */ .DB);
    const tokenNameBytes32 = (0,_state_multicall_hooks__WEBPACK_IMPORTED_MODULE_6__/* .useSingleCallResult */ .Wk)(token ? undefined : tokenContractBytes32, 'name', undefined, _state_multicall_hooks__WEBPACK_IMPORTED_MODULE_6__/* .NEVER_RELOAD */ .DB);
    const symbol = (0,_state_multicall_hooks__WEBPACK_IMPORTED_MODULE_6__/* .useSingleCallResult */ .Wk)(token ? undefined : tokenContract, 'symbol', undefined, _state_multicall_hooks__WEBPACK_IMPORTED_MODULE_6__/* .NEVER_RELOAD */ .DB);
    const symbolBytes32 = (0,_state_multicall_hooks__WEBPACK_IMPORTED_MODULE_6__/* .useSingleCallResult */ .Wk)(token ? undefined : tokenContractBytes32, 'symbol', undefined, _state_multicall_hooks__WEBPACK_IMPORTED_MODULE_6__/* .NEVER_RELOAD */ .DB);
    const decimals = (0,_state_multicall_hooks__WEBPACK_IMPORTED_MODULE_6__/* .useSingleCallResult */ .Wk)(token ? undefined : tokenContract, 'decimals', undefined, _state_multicall_hooks__WEBPACK_IMPORTED_MODULE_6__/* .NEVER_RELOAD */ .DB);
    return (0,react__WEBPACK_IMPORTED_MODULE_2__.useMemo)(()=>{
        if (token) return token;
        if (!chainId || !address) return undefined;
        if (decimals.loading || symbol.loading || tokenName.loading) return null;
        if (decimals.result) {
            return new _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_1__.Token(chainId, address, decimals.result[0], parseStringOrBytes32(symbol.result?.[0], symbolBytes32.result?.[0], 'UNKNOWN'), parseStringOrBytes32(tokenName.result?.[0], tokenNameBytes32.result?.[0], 'Unknown Token'));
        }
        return undefined;
    }, [
        address,
        chainId,
        decimals.loading,
        decimals.result,
        symbol.loading,
        symbol.result,
        symbolBytes32.result,
        token,
        tokenName.loading,
        tokenName.result,
        tokenNameBytes32.result, 
    ]);
}
function useCurrency(currencyId) {
    const isBNB = currencyId?.toUpperCase() === 'BNB';
    const token = useToken(isBNB ? undefined : currencyId);
    return isBNB ? _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_1__.ETHER : token;
}


/***/ }),

/***/ 6063:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "gs": () => (/* binding */ useBytes32TokenContract),
  "zb": () => (/* binding */ useENSRegistrarContract),
  "uU": () => (/* binding */ useENSResolverContract),
  "gq": () => (/* binding */ useMulticallContract),
  "Ib": () => (/* binding */ useTokenContract),
  "Hj": () => (/* binding */ useWETHContract)
});

// UNUSED EXPORTS: useAnniversaryAchievementContract, useBunnyFactory, useBunnySpecialContract, useBunnySpecialLotteryContract, useBunnySpecialXmasContract, useCake, useCakeVaultContract, useChainlinkOracleContract, useClaimRefundContract, useERC20, useERC721, useEasterNftContract, useErc721CollectionContract, useFarmAuctionContract, useIfoPoolContract, useIfoV1Contract, useIfoV2Contract, useLotteryV2Contract, useMasterchef, useNftMarketContract, useNftSaleContract, usePairContract, usePancakeRabbits, usePancakeSquadContract, usePointCenterIfoContract, usePredictionsContract, useProfileContract, useSousChef, useSousChefV2, useSpecialBunnyCakeVaultContract, useSpecialBunnyPredictionContract, useTradingCompetitionContract, useTradingCompetitionContractV2, useVaultPoolContract

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./src/hooks/useActiveWeb3React.ts
var hooks_useActiveWeb3React = __webpack_require__(4011);
// EXTERNAL MODULE: ./src/utils/contractHelpers.ts + 22 modules
var contractHelpers = __webpack_require__(1553);
// EXTERNAL MODULE: ./src/utils/addressHelpers.ts + 1 modules
var addressHelpers = __webpack_require__(5878);
// EXTERNAL MODULE: ./src/state/types.ts
var types = __webpack_require__(5101);
// EXTERNAL MODULE: external "@pancakeswap/sdk"
var sdk_ = __webpack_require__(543);
// EXTERNAL MODULE: ./src/config/abi/IPancakePair.json
var IPancakePair = __webpack_require__(3725);
;// CONCATENATED MODULE: ./src/config/abi/ens-public-resolver.json
const ens_public_resolver_namespaceObject = JSON.parse('[{"inputs":[{"internalType":"contract ENS","name":"_ens","type":"address"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"node","type":"bytes32"},{"indexed":true,"internalType":"uint256","name":"contentType","type":"uint256"}],"name":"ABIChanged","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"node","type":"bytes32"},{"indexed":false,"internalType":"address","name":"a","type":"address"}],"name":"AddrChanged","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"node","type":"bytes32"},{"indexed":false,"internalType":"uint256","name":"coinType","type":"uint256"},{"indexed":false,"internalType":"bytes","name":"newAddress","type":"bytes"}],"name":"AddressChanged","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"node","type":"bytes32"},{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"target","type":"address"},{"indexed":false,"internalType":"bool","name":"isAuthorised","type":"bool"}],"name":"AuthorisationChanged","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"node","type":"bytes32"},{"indexed":false,"internalType":"bytes","name":"hash","type":"bytes"}],"name":"ContenthashChanged","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"node","type":"bytes32"},{"indexed":false,"internalType":"bytes","name":"name","type":"bytes"},{"indexed":false,"internalType":"uint16","name":"resource","type":"uint16"},{"indexed":false,"internalType":"bytes","name":"record","type":"bytes"}],"name":"DNSRecordChanged","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"node","type":"bytes32"},{"indexed":false,"internalType":"bytes","name":"name","type":"bytes"},{"indexed":false,"internalType":"uint16","name":"resource","type":"uint16"}],"name":"DNSRecordDeleted","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"node","type":"bytes32"}],"name":"DNSZoneCleared","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"node","type":"bytes32"},{"indexed":true,"internalType":"bytes4","name":"interfaceID","type":"bytes4"},{"indexed":false,"internalType":"address","name":"implementer","type":"address"}],"name":"InterfaceChanged","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"node","type":"bytes32"},{"indexed":false,"internalType":"string","name":"name","type":"string"}],"name":"NameChanged","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"node","type":"bytes32"},{"indexed":false,"internalType":"bytes32","name":"x","type":"bytes32"},{"indexed":false,"internalType":"bytes32","name":"y","type":"bytes32"}],"name":"PubkeyChanged","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"node","type":"bytes32"},{"indexed":true,"internalType":"string","name":"indexedKey","type":"string"},{"indexed":false,"internalType":"string","name":"key","type":"string"}],"name":"TextChanged","type":"event"},{"constant":true,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"uint256","name":"contentTypes","type":"uint256"}],"name":"ABI","outputs":[{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"bytes","name":"","type":"bytes"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"}],"name":"addr","outputs":[{"internalType":"address payable","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"bytes32","name":"","type":"bytes32"},{"internalType":"address","name":"","type":"address"},{"internalType":"address","name":"","type":"address"}],"name":"authorisations","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"}],"name":"clearDNSZone","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"}],"name":"contenthash","outputs":[{"internalType":"bytes","name":"","type":"bytes"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"bytes32","name":"name","type":"bytes32"},{"internalType":"uint16","name":"resource","type":"uint16"}],"name":"dnsRecord","outputs":[{"internalType":"bytes","name":"","type":"bytes"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"bytes32","name":"name","type":"bytes32"}],"name":"hasDNSRecords","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"bytes4","name":"interfaceID","type":"bytes4"}],"name":"interfaceImplementer","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"}],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"}],"name":"pubkey","outputs":[{"internalType":"bytes32","name":"x","type":"bytes32"},{"internalType":"bytes32","name":"y","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"uint256","name":"contentType","type":"uint256"},{"internalType":"bytes","name":"data","type":"bytes"}],"name":"setABI","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"uint256","name":"coinType","type":"uint256"},{"internalType":"bytes","name":"a","type":"bytes"}],"name":"setAddr","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"address","name":"a","type":"address"}],"name":"setAddr","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"address","name":"target","type":"address"},{"internalType":"bool","name":"isAuthorised","type":"bool"}],"name":"setAuthorisation","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"bytes","name":"hash","type":"bytes"}],"name":"setContenthash","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"bytes","name":"data","type":"bytes"}],"name":"setDNSRecords","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"bytes4","name":"interfaceID","type":"bytes4"},{"internalType":"address","name":"implementer","type":"address"}],"name":"setInterface","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"string","name":"name","type":"string"}],"name":"setName","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"bytes32","name":"x","type":"bytes32"},{"internalType":"bytes32","name":"y","type":"bytes32"}],"name":"setPubkey","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"string","name":"key","type":"string"},{"internalType":"string","name":"value","type":"string"}],"name":"setText","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"bytes4","name":"interfaceID","type":"bytes4"}],"name":"supportsInterface","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"pure","type":"function"},{"constant":true,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"string","name":"key","type":"string"}],"name":"text","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"}]');
;// CONCATENATED MODULE: ./src/config/abi/ens-registrar.json
const ens_registrar_namespaceObject = JSON.parse('[{"inputs":[{"internalType":"contract ENS","name":"_old","type":"address"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"operator","type":"address"},{"indexed":false,"internalType":"bool","name":"approved","type":"bool"}],"name":"ApprovalForAll","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"node","type":"bytes32"},{"indexed":true,"internalType":"bytes32","name":"label","type":"bytes32"},{"indexed":false,"internalType":"address","name":"owner","type":"address"}],"name":"NewOwner","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"node","type":"bytes32"},{"indexed":false,"internalType":"address","name":"resolver","type":"address"}],"name":"NewResolver","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"node","type":"bytes32"},{"indexed":false,"internalType":"uint64","name":"ttl","type":"uint64"}],"name":"NewTTL","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"node","type":"bytes32"},{"indexed":false,"internalType":"address","name":"owner","type":"address"}],"name":"Transfer","type":"event"},{"constant":true,"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"operator","type":"address"}],"name":"isApprovedForAll","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"old","outputs":[{"internalType":"contract ENS","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"}],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"}],"name":"recordExists","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"}],"name":"resolver","outputs":[{"internalType":"address","name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"operator","type":"address"},{"internalType":"bool","name":"approved","type":"bool"}],"name":"setApprovalForAll","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"address","name":"owner","type":"address"}],"name":"setOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"resolver","type":"address"},{"internalType":"uint64","name":"ttl","type":"uint64"}],"name":"setRecord","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"address","name":"resolver","type":"address"}],"name":"setResolver","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"bytes32","name":"label","type":"bytes32"},{"internalType":"address","name":"owner","type":"address"}],"name":"setSubnodeOwner","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"bytes32","name":"label","type":"bytes32"},{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"resolver","type":"address"},{"internalType":"uint64","name":"ttl","type":"uint64"}],"name":"setSubnodeRecord","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"},{"internalType":"uint64","name":"ttl","type":"uint64"}],"name":"setTTL","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"bytes32","name":"node","type":"bytes32"}],"name":"ttl","outputs":[{"internalType":"uint64","name":"","type":"uint64"}],"payable":false,"stateMutability":"view","type":"function"}]');
// EXTERNAL MODULE: ./src/config/abi/erc20.ts + 1 modules
var erc20 = __webpack_require__(5169);
// EXTERNAL MODULE: ./src/config/abi/erc20.json
var abi_erc20 = __webpack_require__(3324);
// EXTERNAL MODULE: ./src/config/abi/weth.json
var weth = __webpack_require__(9253);
// EXTERNAL MODULE: ./src/config/abi/Multicall.json
var Multicall = __webpack_require__(3373);
// EXTERNAL MODULE: ./src/utils/index.ts + 1 modules
var utils = __webpack_require__(8328);
;// CONCATENATED MODULE: ./src/hooks/useContract.ts














/**
 * Helper hooks to get specific contracts (by ABI)
 */ const useIfoV1Contract = (address)=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getIfoV1Contract(address, library.getSigner())
    , [
        address,
        library
    ]);
};
const useIfoV2Contract = (address)=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getIfoV2Contract(address, library.getSigner())
    , [
        address,
        library
    ]);
};
const useERC20 = (address, withSignerIfPossible = true)=>{
    const { library , account  } = useActiveWeb3React();
    return useMemo(()=>getBep20Contract(address, withSignerIfPossible ? getProviderOrSigner(library, account) : null)
    , [
        account,
        address,
        library,
        withSignerIfPossible
    ]);
};
/**
 * @see https://docs.openzeppelin.com/contracts/3.x/api/token/erc721
 */ const useERC721 = (address)=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getErc721Contract(address, library.getSigner())
    , [
        address,
        library
    ]);
};
const useCake = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getCakeContract(library.getSigner())
    , [
        library
    ]);
};
const useBunnyFactory = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getBunnyFactoryContract(library.getSigner())
    , [
        library
    ]);
};
const usePancakeRabbits = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getPancakeRabbitContract(library.getSigner())
    , [
        library
    ]);
};
const useProfileContract = (withSignerIfPossible = true)=>{
    const { library , account  } = useActiveWeb3React();
    return useMemo(()=>getProfileContract(withSignerIfPossible ? getProviderOrSigner(library, account) : null)
    , [
        withSignerIfPossible,
        account,
        library
    ]);
};
const useLotteryV2Contract = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getLotteryV2Contract(library.getSigner())
    , [
        library
    ]);
};
const useMasterchef = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getMasterchefContract(library.getSigner())
    , [
        library
    ]);
};
const useSousChef = (id)=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getSouschefContract(id, library.getSigner())
    , [
        id,
        library
    ]);
};
const useSousChefV2 = (id)=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getSouschefV2Contract(id, library.getSigner())
    , [
        id,
        library
    ]);
};
const usePointCenterIfoContract = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getPointCenterIfoContract(library.getSigner())
    , [
        library
    ]);
};
const useBunnySpecialContract = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getBunnySpecialContract(library.getSigner())
    , [
        library
    ]);
};
const useClaimRefundContract = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getClaimRefundContract(library.getSigner())
    , [
        library
    ]);
};
const useTradingCompetitionContract = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getTradingCompetitionContract(library.getSigner())
    , [
        library
    ]);
};
const useTradingCompetitionContractV2 = (withSignerIfPossible = true)=>{
    const { library , account  } = useActiveWeb3React();
    return useMemo(()=>getTradingCompetitionContractV2(withSignerIfPossible ? getProviderOrSigner(library, account) : null)
    , [
        library,
        withSignerIfPossible,
        account
    ]);
};
const useEasterNftContract = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getEasterNftContract(library.getSigner())
    , [
        library
    ]);
};
const useVaultPoolContract = (vaultKey)=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>{
        return vaultKey === VaultKey.CakeVault ? getCakeVaultContract(library.getSigner()) : getIfoPoolContract(library.getSigner());
    }, [
        library,
        vaultKey
    ]);
};
const useCakeVaultContract = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getCakeVaultContract(library.getSigner())
    , [
        library
    ]);
};
const useIfoPoolContract = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getIfoPoolContract(library.getSigner())
    , [
        library
    ]);
};
const usePredictionsContract = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getPredictionsContract(library.getSigner())
    , [
        library
    ]);
};
const useChainlinkOracleContract = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getChainlinkOracleContract(library.getSigner())
    , [
        library
    ]);
};
const useSpecialBunnyCakeVaultContract = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getBunnySpecialCakeVaultContract(library.getSigner())
    , [
        library
    ]);
};
const useSpecialBunnyPredictionContract = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getBunnySpecialPredictionContract(library.getSigner())
    , [
        library
    ]);
};
const useBunnySpecialLotteryContract = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getBunnySpecialLotteryContract(library.getSigner())
    , [
        library
    ]);
};
const useBunnySpecialXmasContract = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getBunnySpecialXmasContract(library.getSigner())
    , [
        library
    ]);
};
const useAnniversaryAchievementContract = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getAnniversaryAchievementContract(library.getSigner())
    , [
        library
    ]);
};
const useNftSaleContract = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getNftSaleContract(library.getSigner())
    , [
        library
    ]);
};
const usePancakeSquadContract = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getPancakeSquadContract(library.getSigner())
    , [
        library
    ]);
};
const useFarmAuctionContract = (withSignerIfPossible = true)=>{
    const { account , library  } = useActiveWeb3React();
    return useMemo(()=>getFarmAuctionContract(withSignerIfPossible ? getProviderOrSigner(library, account) : null)
    , [
        library,
        account,
        withSignerIfPossible
    ]);
};
const useNftMarketContract = ()=>{
    const { library  } = useActiveWeb3React();
    return useMemo(()=>getNftMarketContract(library.getSigner())
    , [
        library
    ]);
};
const useErc721CollectionContract = (collectionAddress, withSignerIfPossible = true)=>{
    const { library , account  } = useActiveWeb3React();
    return useMemo(()=>{
        return getErc721CollectionContract(withSignerIfPossible ? getProviderOrSigner(library, account) : null, collectionAddress);
    }, [
        account,
        library,
        collectionAddress,
        withSignerIfPossible
    ]);
};
// Code below migrated from Exchange useContract.ts
// returns null on errors
function useContract(address, ABI, withSignerIfPossible = true) {
    const { library , account  } = (0,hooks_useActiveWeb3React/* default */.Z)();
    return (0,external_react_.useMemo)(()=>{
        if (!address || !ABI || !library) return null;
        try {
            return (0,utils/* getContract */.uN)(address, ABI, withSignerIfPossible ? (0,utils/* getProviderOrSigner */.TY)(library, account) : null);
        } catch (error) {
            console.error('Failed to get contract', error);
            return null;
        }
    }, [
        address,
        ABI,
        library,
        withSignerIfPossible,
        account
    ]);
}
function useTokenContract(tokenAddress, withSignerIfPossible) {
    return useContract(tokenAddress, abi_erc20, withSignerIfPossible);
}
function useWETHContract(withSignerIfPossible) {
    const { chainId  } = (0,hooks_useActiveWeb3React/* default */.Z)();
    return useContract(chainId ? sdk_.WETH[chainId].address : undefined, weth, withSignerIfPossible);
}
function useENSRegistrarContract(withSignerIfPossible) {
    const { chainId  } = (0,hooks_useActiveWeb3React/* default */.Z)();
    let address;
    if (chainId) {
        // eslint-disable-next-line default-case
        switch(chainId){
            case sdk_.ChainId.MAINNET:
            case sdk_.ChainId.TESTNET:
                address = '0x00000000000C2E074eC69A0dFb2997BA6C7d2e1e';
                break;
        }
    }
    return useContract(address, ens_registrar_namespaceObject, withSignerIfPossible);
}
function useENSResolverContract(address, withSignerIfPossible) {
    return useContract(address, ens_public_resolver_namespaceObject, withSignerIfPossible);
}
function useBytes32TokenContract(tokenAddress, withSignerIfPossible) {
    return useContract(tokenAddress, erc20/* ERC20_BYTES32_ABI */.Jz, withSignerIfPossible);
}
function usePairContract(pairAddress, withSignerIfPossible) {
    return useContract(pairAddress, IPancakePairABI, withSignerIfPossible);
}
function useMulticallContract() {
    return useContract((0,addressHelpers/* getMulticallAddress */.I8)(), Multicall, false);
}


/***/ }),

/***/ 5999:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ useDebounce)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

// modified from https://usehooks.com/useDebounce/
function useDebounce(value, delay) {
    const { 0: debouncedValue , 1: setDebouncedValue  } = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(value);
    (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(()=>{
        // Update debounced value after delay
        const handler = setTimeout(()=>{
            setDebouncedValue(value);
        }, delay);
        // Cancel the timeout if value changes (also on delay change or unmount)
        // This is how we prevent debounced value from updating if value is changed ...
        // .. within the delay period. Timeout gets cleared and restarted.
        return ()=>{
            clearTimeout(handler);
        };
    }, [
        value,
        delay
    ]);
    return debouncedValue;
};


/***/ }),

/***/ 3917:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var state_user_hooks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8605);



const useTheme = ()=>{
    const [isDark, toggleTheme] = (0,state_user_hooks__WEBPACK_IMPORTED_MODULE_2__/* .useThemeManager */ .HY)();
    const theme = (0,react__WEBPACK_IMPORTED_MODULE_0__.useContext)(styled_components__WEBPACK_IMPORTED_MODULE_1__.ThemeContext);
    return {
        isDark,
        theme,
        toggleTheme
    };
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (useTheme);


/***/ }),

/***/ 7952:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "DT": () => (/* binding */ WrappedTokenInfo),
  "v0": () => (/* binding */ useActiveListUrls),
  "R0": () => (/* binding */ useAllLists),
  "z0": () => (/* binding */ useCombinedActiveList),
  "qB": () => (/* binding */ useCombinedInactiveList),
  "EF": () => (/* binding */ useIsListActive),
  "Rx": () => (/* binding */ useUnsupportedTokenList)
});

// UNUSED EXPORTS: listToTokenMap, useDefaultTokenList, useInactiveListUrls

// EXTERNAL MODULE: external "@pancakeswap/sdk"
var sdk_ = __webpack_require__(543);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(6022);
// EXTERNAL MODULE: ./src/config/constants/lists.ts
var constants_lists = __webpack_require__(428);
;// CONCATENATED MODULE: ./src/config/constants/tokenLists/pancake-default.tokenlist.json
const pancake_default_tokenlist_namespaceObject = JSON.parse('{"name":"PancakeSwap Default List","timestamp":"2021-05-06T00:00:00Z","version":{"major":3,"minor":0,"patch":0},"tags":{},"logoURI":"https://pancakeswap.finance/logo.png","keywords":["pancake","default"],"tokens":[{"name":"WBNB Token","symbol":"WBNB","address":"0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c","chainId":56,"decimals":18,"logoURI":"https://pancakeswap.finance/images/tokens/0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c.png"},{"name":"FHO Token","symbol":"$FHO","address":"0x84da2d4024ac44307ea67fe10077dbbdc10c5654","chainId":56,"decimals":9,"logoURI":"https://lh3.googleusercontent.com/j27DpW5xn_sQAzELtwakAvFTXZAXdfnQn8nsnAuQRB0RbEgfEwQgnjMbf4AK3SvqwCPz_B0QYvF_ylMGPpIAqdO-I2wRlirCHaTiuGUF3aK4nZ8zTstc4V10YQJgKumbmE7gZnbm8stRxF7mcsnBdCyFl8_an19jVvQuoCcUh46qJZWoddYNTwmYnIhzzlatG3dci73ihimT5ziL2auXFc-VEg2ezVKEWuWw66UIyorLI33MG1ZU_G8QmArgNHc7qRnb6L2rICbm3RH5iA7aj9v2PQ7uFZmmmM_lT3C5mYgadND_7kznjeXkCIvYHp5KqPhTRnaP1294cmPQYEqVM6P0o39F4fpirAy7Oi8_F5gOmhNsHCQqjBT-zmPAcS_KK3QNGHHIpC6Sd2CSAD0p3zw467mr0iFzDOqWWdXXYBXK1hnhVz7sT_Pljo-OMfiUnQGjNTvmq-gypwfYHt9XiKytkWxhbiRZri6Ea_Ov9LFKfKoPCdR8_m8Phw0kDOV3EOSvnTFqBhV-EHkAHocICuL-36duZ2CY-513-i9-mJkcr_re347ZoK_Hp08-0fUhSLFlKAQUIwRXkTtOIo8CfZ8DitVw41Z1t44SWJPOypi4T_6J7J2NxCbLsTCthf9D8bSwT3e67LtDOMpnt5BympFdScKWiRtA_dzOR8z5lM0wrIGBmQxJAi2rXliLPQ3EF_yol2Iql1hLrZ12vNeCCrR8=w1632-h1446"},{"name":"BUSD Token","symbol":"BUSD","address":"0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56","chainId":56,"decimals":18,"logoURI":"https://pancakeswap.finance/images/tokens/0xe9e7cea3dedca5984780bafc599bd69add087d56.png"},{"name":"Ethereum Token","symbol":"ETH","address":"0x2170Ed0880ac9A755fd29B2688956BD959F933F8","chainId":56,"decimals":18,"logoURI":"https://pancakeswap.finance/images/tokens/0x2170ed0880ac9a755fd29b2688956bd959f933f8.png"},{"name":"BTCB Token","symbol":"BTCB","address":"0x7130d2A12B9BCbFAe4f2634d864A1Ee1Ce3Ead9c","chainId":56,"decimals":18,"logoURI":"https://pancakeswap.finance/images/tokens/0x7130d2a12b9bcbfae4f2634d864a1ee1ce3ead9c.png"},{"name":"Tether USD","symbol":"USDT","address":"0x55d398326f99059fF775485246999027B3197955","chainId":56,"decimals":18,"logoURI":"https://pancakeswap.finance/images/tokens/0x55d398326f99059ff775485246999027b3197955.png"},{"name":"PancakeSwap Token","symbol":"CAKE","address":"0x0E09FaBB73Bd3Ade0a17ECC321fD13a19e81cE82","chainId":56,"decimals":18,"logoURI":"https://pancakeswap.finance/images/tokens/0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82.png"},{"name":"Venus","symbol":"XVS","address":"0xcF6BB5389c92Bdda8a3747Ddb454cB7a64626C63","chainId":56,"decimals":18,"logoURI":"https://pancakeswap.finance/images/tokens/0xcf6bb5389c92bdda8a3747ddb454cb7a64626c63.png"},{"name":"VAI Stablecoin","symbol":"VAI","address":"0x4BD17003473389A42DAF6a0a729f6Fdb328BbBd7","chainId":56,"decimals":18,"logoURI":"https://pancakeswap.finance/images/tokens/0x4bd17003473389a42daf6a0a729f6fdb328bbbd7.png"},{"name":"Pancake Bunny","symbol":"BUNNY","address":"0xC9849E6fdB743d08fAeE3E34dd2D1bc69EA11a51","chainId":56,"decimals":18,"logoURI":"https://pancakeswap.finance/images/tokens/0xc9849e6fdb743d08faee3e34dd2d1bc69ea11a51.png"},{"name":"SafeMoon","symbol":"SAFEMOON","address":"0x8076C74C5e3F5852037F31Ff0093Eeb8c8ADd8D3","chainId":56,"decimals":9,"logoURI":"https://pancakeswap.finance/images/tokens/0x8076c74c5e3f5852037f31ff0093eeb8c8add8d3.png"},{"name":"Alpaca","symbol":"ALPACA","address":"0x8F0528cE5eF7B51152A59745bEfDD91D97091d2F","chainId":56,"decimals":18,"logoURI":"https://pancakeswap.finance/images/tokens/0x8f0528ce5ef7b51152a59745befdd91d97091d2f.png"},{"name":"Belt","symbol":"BELT","address":"0xE0e514c71282b6f4e823703a39374Cf58dc3eA4f","chainId":56,"decimals":18,"logoURI":"https://pancakeswap.finance/images/tokens/0xE0e514c71282b6f4e823703a39374Cf58dc3eA4f.png"},{"name":"TokoCrypto","symbol":"TKO","address":"0x9f589e3eabe42ebC94A44727b3f3531C0c877809","chainId":56,"decimals":18,"logoURI":"https://pancakeswap.finance/images/tokens/0x9f589e3eabe42ebc94a44727b3f3531c0c877809.png"},{"name":"Nerve Finance","symbol":"NRV","address":"0x42F6f551ae042cBe50C739158b4f0CAC0Edb9096","chainId":56,"decimals":18,"logoURI":"https://pancakeswap.finance/images/tokens/0x42f6f551ae042cbe50c739158b4f0cac0edb9096.png"},{"name":"Ellipsis","symbol":"EPS","address":"0xA7f552078dcC247C2684336020c03648500C6d9F","chainId":56,"decimals":18,"logoURI":"https://pancakeswap.finance/images/tokens/0xa7f552078dcc247c2684336020c03648500c6d9f.png"}]}');
;// CONCATENATED MODULE: ./src/config/constants/tokenLists/pancake-unsupported.tokenlist.json
const pancake_unsupported_tokenlist_namespaceObject = JSON.parse('{"name":"Pancake Unsupported List","timestamp":"2021-01-05T20:47:02.923Z","version":{"major":1,"minor":0,"patch":0},"tags":{},"logoURI":"ipfs://QmNa8mQkrNKp1WEEeGjFezDmDeodkWRevGFN8JCV7b4Xir","keywords":["pancake","unsupported"],"tokens":[]}');
;// CONCATENATED MODULE: ./src/state/lists/hooks.ts







// use ordering of default list of lists to assign priority
function sortByListPriority(urlA, urlB) {
    const first = constants_lists/* DEFAULT_LIST_OF_LISTS.includes */.Lx.includes(urlA) ? constants_lists/* DEFAULT_LIST_OF_LISTS.indexOf */.Lx.indexOf(urlA) : Number.MAX_SAFE_INTEGER;
    const second = constants_lists/* DEFAULT_LIST_OF_LISTS.includes */.Lx.includes(urlB) ? constants_lists/* DEFAULT_LIST_OF_LISTS.indexOf */.Lx.indexOf(urlB) : Number.MAX_SAFE_INTEGER;
    // need reverse order to make sure mapping includes top priority last
    if (first < second) return 1;
    if (first > second) return -1;
    return 0;
}
/**
 * Token instances created from token info.
 */ class WrappedTokenInfo extends sdk_.Token {
    constructor(tokenInfo, tags){
        super(tokenInfo.chainId, tokenInfo.address, tokenInfo.decimals, tokenInfo.symbol, tokenInfo.name);
        this.tokenInfo = tokenInfo;
        this.tags = tags;
    }
    get logoURI() {
        return this.tokenInfo.logoURI;
    }
}
/**
 * An empty result, useful as a default.
 */ const EMPTY_LIST = {
    [sdk_.ChainId.MAINNET]: {},
    [sdk_.ChainId.TESTNET]: {}
};
const listCache = typeof WeakMap !== 'undefined' ? new WeakMap() : null;
function listToTokenMap(list) {
    const result = listCache?.get(list);
    if (result) return result;
    const map = list.tokens.reduce((tokenMap, tokenInfo)=>{
        const tags = tokenInfo.tags?.map((tagId)=>{
            if (!list.tags?.[tagId]) return undefined;
            return {
                ...list.tags[tagId],
                id: tagId
            };
        })?.filter((x)=>Boolean(x)
        ) ?? [];
        const token = new WrappedTokenInfo(tokenInfo, tags);
        if (tokenMap[token.chainId][token.address] !== undefined) throw Error('Duplicate tokens.');
        return {
            ...tokenMap,
            [token.chainId]: {
                ...tokenMap[token.chainId],
                [token.address]: {
                    token,
                    list
                }
            }
        };
    }, {
        ...EMPTY_LIST
    });
    listCache?.set(list, map);
    return map;
}
function useAllLists() {
    return (0,external_react_redux_.useSelector)((state)=>state.lists.byUrl
    );
}
function combineMaps(map1, map2) {
    return {
        [sdk_.ChainId.MAINNET]: {
            ...map1[sdk_.ChainId.MAINNET],
            ...map2[sdk_.ChainId.MAINNET]
        },
        [sdk_.ChainId.TESTNET]: {
            ...map1[sdk_.ChainId.TESTNET],
            ...map2[sdk_.ChainId.TESTNET]
        }
    };
}
// merge tokens contained within lists from urls
function useCombinedTokenMapFromUrls(urls) {
    const lists = useAllLists();
    return (0,external_react_.useMemo)(()=>{
        if (!urls) return EMPTY_LIST;
        return urls.slice()// sort by priority so top priority goes last
        .sort(sortByListPriority).reduce((allTokens, currentUrl)=>{
            const current = lists[currentUrl]?.current;
            if (!current) return allTokens;
            try {
                const newTokens = Object.assign(listToTokenMap(current));
                return combineMaps(allTokens, newTokens);
            } catch (error) {
                console.error('Could not show token list due to error', error);
                return allTokens;
            }
        }, EMPTY_LIST);
    }, [
        lists,
        urls
    ]);
}
// filter out unsupported lists
function useActiveListUrls() {
    return (0,external_react_redux_.useSelector)((state)=>state.lists.activeListUrls
    )?.filter((url)=>!constants_lists/* UNSUPPORTED_LIST_URLS.includes */.US.includes(url)
    );
}
function useInactiveListUrls() {
    const lists = useAllLists();
    const allActiveListUrls = useActiveListUrls();
    return Object.keys(lists).filter((url)=>!allActiveListUrls?.includes(url) && !constants_lists/* UNSUPPORTED_LIST_URLS.includes */.US.includes(url)
    );
}
// get all the tokens from active lists, combine with local default tokens
function useCombinedActiveList() {
    const activeListUrls = useActiveListUrls();
    const activeTokens = useCombinedTokenMapFromUrls(activeListUrls);
    const defaultTokenMap = listToTokenMap(pancake_default_tokenlist_namespaceObject);
    return combineMaps(activeTokens, defaultTokenMap);
}
// all tokens from inactive lists
function useCombinedInactiveList() {
    const allInactiveListUrls = useInactiveListUrls();
    return useCombinedTokenMapFromUrls(allInactiveListUrls);
}
// used to hide warnings on import for default tokens
function useDefaultTokenList() {
    return listToTokenMap(DEFAULT_TOKEN_LIST);
}
// list of tokens not supported on interface, used to show warnings and prevent swaps and adds
function useUnsupportedTokenList() {
    // get hard coded unsupported tokens
    const localUnsupportedListMap = listToTokenMap(pancake_unsupported_tokenlist_namespaceObject);
    // get any loaded unsupported tokens
    const loadedUnsupportedListMap = useCombinedTokenMapFromUrls(constants_lists/* UNSUPPORTED_LIST_URLS */.US);
    // format into one token address map
    return combineMaps(localUnsupportedListMap, loadedUnsupportedListMap);
}
function useIsListActive(url) {
    const activeListUrls = useActiveListUrls();
    return Boolean(activeListUrls?.includes(url));
}


/***/ }),

/***/ 8605:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TO": () => (/* binding */ useAudioModeManager),
/* harmony export */   "FT": () => (/* binding */ usePhishingBannerManager),
/* harmony export */   "vD": () => (/* binding */ useExchangeChartManager),
/* harmony export */   "OE": () => (/* binding */ useExchangeChartViewManager),
/* harmony export */   "YF": () => (/* binding */ useSubgraphHealthIndicatorManager),
/* harmony export */   "DG": () => (/* binding */ useExpertModeManager),
/* harmony export */   "HY": () => (/* binding */ useThemeManager),
/* harmony export */   "RO": () => (/* binding */ useUserSingleHopOnly),
/* harmony export */   "$2": () => (/* binding */ useUserSlippageTolerance),
/* harmony export */   "wX": () => (/* binding */ useUserExpertModeAcknowledgementShow),
/* harmony export */   "A6": () => (/* binding */ useUserTransactionTTL),
/* harmony export */   "_E": () => (/* binding */ useAddUserToken),
/* harmony export */   "QG": () => (/* binding */ useRemoveUserAddedToken),
/* harmony export */   "Fh": () => (/* binding */ useGasPrice),
/* harmony export */   "nF": () => (/* binding */ useGasPriceManager),
/* harmony export */   "z6": () => (/* binding */ useWatchlistTokens),
/* harmony export */   "Hx": () => (/* binding */ useWatchlistPools)
/* harmony export */ });
/* unused harmony exports useIsExpertMode, useUserFarmStakedOnly, useUserPoolStakedOnly, useUserPoolsViewMode, useUserFarmsViewMode, useUserPredictionAcceptedRisk, useUserPredictionChartDisclaimerShow, useUserUsernameVisibility, usePairAdder, toV2LiquidityToken, useTrackedTokenPairs */
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(543);
/* harmony import */ var _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4146);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(date_fns__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var lodash_flatMap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8190);
/* harmony import */ var lodash_flatMap__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash_flatMap__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(6022);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var config_constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3862);
/* harmony import */ var hooks_useActiveWeb3React__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(4011);
/* harmony import */ var hooks_Tokens__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(6435);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(6245);
/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(787);










function useAudioModeManager() {
    const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useDispatch)();
    const audioPlay = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useSelector)((state)=>state.user.audioPlay
    );
    const toggleSetAudioMode = (0,react__WEBPACK_IMPORTED_MODULE_3__.useCallback)(()=>{
        if (audioPlay) {
            dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_8__/* .muteAudio */ .B8)());
        } else {
            dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_8__/* .unmuteAudio */ .u7)());
        }
    }, [
        audioPlay,
        dispatch
    ]);
    return [
        audioPlay,
        toggleSetAudioMode
    ];
}
function usePhishingBannerManager() {
    const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useDispatch)();
    const hideTimestampPhishingWarningBanner = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useSelector)((state)=>state.user.hideTimestampPhishingWarningBanner
    );
    const now = Date.now();
    const showPhishingWarningBanner = hideTimestampPhishingWarningBanner ? (0,date_fns__WEBPACK_IMPORTED_MODULE_1__.differenceInDays)(now, hideTimestampPhishingWarningBanner) >= 1 : true;
    const hideBanner = (0,react__WEBPACK_IMPORTED_MODULE_3__.useCallback)(()=>{
        dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_8__/* .hidePhishingWarningBanner */ .l5)());
    }, [
        dispatch
    ]);
    return [
        showPhishingWarningBanner,
        hideBanner
    ];
}
// Get user preference for exchange price chart
// For mobile layout chart is hidden by default
function useExchangeChartManager(isMobile) {
    const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useDispatch)();
    const isChartDisplayed = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useSelector)((state)=>state.user.isExchangeChartDisplayed
    );
    const setUserChartPreference = (0,react__WEBPACK_IMPORTED_MODULE_3__.useCallback)((isDisplayed)=>{
        dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_8__/* .setIsExchangeChartDisplayed */ .hN)(isDisplayed));
    }, [
        dispatch
    ]);
    return [
        isMobile ? false : isChartDisplayed,
        setUserChartPreference
    ];
}
function useExchangeChartViewManager() {
    const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useDispatch)();
    const chartViewMode = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useSelector)((state)=>state.user.userChartViewMode
    );
    const setUserChartViewPreference = (0,react__WEBPACK_IMPORTED_MODULE_3__.useCallback)((view)=>{
        dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_8__/* .setChartViewMode */ .p9)(view));
    }, [
        dispatch
    ]);
    return [
        chartViewMode,
        setUserChartViewPreference
    ];
}
function useSubgraphHealthIndicatorManager() {
    const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useDispatch)();
    const isSubgraphHealthIndicatorDisplayed = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useSelector)((state)=>state.user.isSubgraphHealthIndicatorDisplayed
    );
    const setSubgraphHealthIndicatorDisplayedPreference = (0,react__WEBPACK_IMPORTED_MODULE_3__.useCallback)((newIsDisplayed)=>{
        dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_8__/* .setSubgraphHealthIndicatorDisplayed */ .Hr)(newIsDisplayed));
    }, [
        dispatch
    ]);
    return [
        isSubgraphHealthIndicatorDisplayed,
        setSubgraphHealthIndicatorDisplayedPreference
    ];
}
function useIsExpertMode() {
    return (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useSelector)((state)=>state.user.userExpertMode
    );
}
function useExpertModeManager() {
    const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useDispatch)();
    const expertMode = useIsExpertMode();
    const toggleSetExpertMode = (0,react__WEBPACK_IMPORTED_MODULE_3__.useCallback)(()=>{
        dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_8__/* .updateUserExpertMode */ .zv)({
            userExpertMode: !expertMode
        }));
    }, [
        expertMode,
        dispatch
    ]);
    return [
        expertMode,
        toggleSetExpertMode
    ];
}
function useThemeManager() {
    const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useDispatch)();
    const isDark = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useSelector)((state)=>state.user.isDark
    );
    const toggleTheme = (0,react__WEBPACK_IMPORTED_MODULE_3__.useCallback)(()=>{
        dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_8__/* .toggleTheme */ .X8)());
    }, [
        dispatch
    ]);
    return [
        isDark,
        toggleTheme
    ];
}
function useUserSingleHopOnly() {
    const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useDispatch)();
    const singleHopOnly = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useSelector)((state)=>state.user.userSingleHopOnly
    );
    const setSingleHopOnly = (0,react__WEBPACK_IMPORTED_MODULE_3__.useCallback)((newSingleHopOnly)=>{
        dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_8__/* .updateUserSingleHopOnly */ .fO)({
            userSingleHopOnly: newSingleHopOnly
        }));
    }, [
        dispatch
    ]);
    return [
        singleHopOnly,
        setSingleHopOnly
    ];
}
function useUserSlippageTolerance() {
    const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useDispatch)();
    const userSlippageTolerance = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useSelector)((state)=>{
        return state.user.userSlippageTolerance;
    });
    const setUserSlippageTolerance = (0,react__WEBPACK_IMPORTED_MODULE_3__.useCallback)((slippage)=>{
        dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_8__/* .updateUserSlippageTolerance */ .rQ)({
            userSlippageTolerance: slippage
        }));
    }, [
        dispatch
    ]);
    return [
        userSlippageTolerance,
        setUserSlippageTolerance
    ];
}
function useUserFarmStakedOnly(isActive) {
    const dispatch = useDispatch();
    const userFarmStakedOnly = useSelector((state)=>{
        return state.user.userFarmStakedOnly;
    });
    const setUserFarmStakedOnly = useCallback((stakedOnly)=>{
        const farmStakedOnly = stakedOnly ? FarmStakedOnly.TRUE : FarmStakedOnly.FALSE;
        dispatch(updateUserFarmStakedOnly({
            userFarmStakedOnly: farmStakedOnly
        }));
    }, [
        dispatch
    ]);
    return [
        userFarmStakedOnly === FarmStakedOnly.ON_FINISHED ? !isActive : userFarmStakedOnly === FarmStakedOnly.TRUE,
        setUserFarmStakedOnly, 
    ];
}
function useUserPoolStakedOnly() {
    const dispatch = useDispatch();
    const userPoolStakedOnly = useSelector((state)=>{
        return state.user.userPoolStakedOnly;
    });
    const setUserPoolStakedOnly = useCallback((stakedOnly)=>{
        dispatch(updateUserPoolStakedOnly({
            userPoolStakedOnly: stakedOnly
        }));
    }, [
        dispatch
    ]);
    return [
        userPoolStakedOnly,
        setUserPoolStakedOnly
    ];
}
function useUserPoolsViewMode() {
    const dispatch = useDispatch();
    const userPoolsViewMode = useSelector((state)=>{
        return state.user.userPoolsViewMode;
    });
    const setUserPoolsViewMode = useCallback((viewMode)=>{
        dispatch(updateUserPoolsViewMode({
            userPoolsViewMode: viewMode
        }));
    }, [
        dispatch
    ]);
    return [
        userPoolsViewMode,
        setUserPoolsViewMode
    ];
}
function useUserFarmsViewMode() {
    const dispatch = useDispatch();
    const userFarmsViewMode = useSelector((state)=>{
        return state.user.userFarmsViewMode;
    });
    const setUserFarmsViewMode = useCallback((viewMode)=>{
        dispatch(updateUserFarmsViewMode({
            userFarmsViewMode: viewMode
        }));
    }, [
        dispatch
    ]);
    return [
        userFarmsViewMode,
        setUserFarmsViewMode
    ];
}
function useUserPredictionAcceptedRisk() {
    const dispatch = useDispatch();
    const userPredictionAcceptedRisk = useSelector((state)=>{
        return state.user.userPredictionAcceptedRisk;
    });
    const setUserPredictionAcceptedRisk = useCallback((acceptedRisk)=>{
        dispatch(updateUserPredictionAcceptedRisk({
            userAcceptedRisk: acceptedRisk
        }));
    }, [
        dispatch
    ]);
    return [
        userPredictionAcceptedRisk,
        setUserPredictionAcceptedRisk
    ];
}
function useUserPredictionChartDisclaimerShow() {
    const dispatch = useDispatch();
    const userPredictionChartDisclaimerShow = useSelector((state)=>{
        return state.user.userPredictionChartDisclaimerShow;
    });
    const setPredictionUserChartDisclaimerShow = useCallback((showDisclaimer)=>{
        dispatch(updateUserPredictionChartDisclaimerShow({
            userShowDisclaimer: showDisclaimer
        }));
    }, [
        dispatch
    ]);
    return [
        userPredictionChartDisclaimerShow,
        setPredictionUserChartDisclaimerShow
    ];
}
function useUserExpertModeAcknowledgementShow() {
    const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useDispatch)();
    const userExpertModeAcknowledgementShow = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useSelector)((state)=>{
        return state.user.userExpertModeAcknowledgementShow;
    });
    const setUserExpertModeAcknowledgementShow = (0,react__WEBPACK_IMPORTED_MODULE_3__.useCallback)((showAcknowledgement)=>{
        dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_8__/* .updateUserExpertModeAcknowledgementShow */ ._C)({
            userExpertModeAcknowledgementShow: showAcknowledgement
        }));
    }, [
        dispatch
    ]);
    return [
        userExpertModeAcknowledgementShow,
        setUserExpertModeAcknowledgementShow
    ];
}
function useUserUsernameVisibility() {
    const dispatch = useDispatch();
    const userUsernameVisibility = useSelector((state)=>{
        return state.user.userUsernameVisibility;
    });
    const setUserUsernameVisibility = useCallback((usernameVisibility)=>{
        dispatch(updateUserUsernameVisibility({
            userUsernameVisibility: usernameVisibility
        }));
    }, [
        dispatch
    ]);
    return [
        userUsernameVisibility,
        setUserUsernameVisibility
    ];
}
function useUserTransactionTTL() {
    const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useDispatch)();
    const userDeadline = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useSelector)((state)=>{
        return state.user.userDeadline;
    });
    const setUserDeadline = (0,react__WEBPACK_IMPORTED_MODULE_3__.useCallback)((deadline)=>{
        dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_8__/* .updateUserDeadline */ .gw)({
            userDeadline: deadline
        }));
    }, [
        dispatch
    ]);
    return [
        userDeadline,
        setUserDeadline
    ];
}
function useAddUserToken() {
    const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useDispatch)();
    return (0,react__WEBPACK_IMPORTED_MODULE_3__.useCallback)((token)=>{
        dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_8__/* .addSerializedToken */ .eg)({
            serializedToken: (0,_helpers__WEBPACK_IMPORTED_MODULE_9__/* .serializeToken */ .Mq)(token)
        }));
    }, [
        dispatch
    ]);
}
function useRemoveUserAddedToken() {
    const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useDispatch)();
    return (0,react__WEBPACK_IMPORTED_MODULE_3__.useCallback)((chainId, address)=>{
        dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_8__/* .removeSerializedToken */ .zQ)({
            chainId,
            address
        }));
    }, [
        dispatch
    ]);
}
function useGasPrice() {
    const chainId = "56";
    const userGas = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useSelector)((state)=>state.user.gasPrice
    );
    return chainId === _pancakeswap_sdk__WEBPACK_IMPORTED_MODULE_0__.ChainId.MAINNET.toString() ? userGas : _helpers__WEBPACK_IMPORTED_MODULE_9__/* .GAS_PRICE_GWEI.testnet */ .j4.testnet;
}
function useGasPriceManager() {
    const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useDispatch)();
    const userGasPrice = useGasPrice();
    const setGasPrice = (0,react__WEBPACK_IMPORTED_MODULE_3__.useCallback)((gasPrice)=>{
        dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_8__/* .updateGasPrice */ .dy)({
            gasPrice
        }));
    }, [
        dispatch
    ]);
    return [
        userGasPrice,
        setGasPrice
    ];
}
function serializePair(pair) {
    return {
        token0: serializeToken(pair.token0),
        token1: serializeToken(pair.token1)
    };
}
function usePairAdder() {
    const dispatch = useDispatch();
    return useCallback((pair)=>{
        dispatch(addSerializedPair({
            serializedPair: serializePair(pair)
        }));
    }, [
        dispatch
    ]);
}
/**
 * Given two tokens return the liquidity token that represents its liquidity shares
 * @param tokenA one of the two tokens
 * @param tokenB the other token
 */ function toV2LiquidityToken([tokenA, tokenB]) {
    return new Token(tokenA.chainId, Pair.getAddress(tokenA, tokenB), 18, 'Cake-LP', 'Pancake LPs');
}
/**
 * Returns all the pairs of tokens that are tracked by the user for the current chain ID.
 */ function useTrackedTokenPairs() {
    const { chainId  } = useActiveWeb3React();
    const tokens = useAllTokens();
    // pinned pairs
    const pinnedPairs = useMemo(()=>chainId ? PINNED_PAIRS[chainId] ?? [] : []
    , [
        chainId
    ]);
    // pairs for every token against every base
    const generatedPairs = useMemo(()=>chainId ? flatMap(Object.keys(tokens), (tokenAddress)=>{
            const token = tokens[tokenAddress];
            // for each token on the current chain,
            return(// loop though all bases on the current chain
            (BASES_TO_TRACK_LIQUIDITY_FOR[chainId] ?? [])// to construct pairs of the given token with each base
            .map((base)=>{
                if (base.address === token.address) {
                    return null;
                }
                return [
                    base,
                    token
                ];
            }).filter((p)=>p !== null
            ));
        }) : []
    , [
        tokens,
        chainId
    ]);
    // pairs saved by users
    const savedSerializedPairs = useSelector(({ user: { pairs  }  })=>pairs
    );
    const userPairs = useMemo(()=>{
        if (!chainId || !savedSerializedPairs) return [];
        const forChain = savedSerializedPairs[chainId];
        if (!forChain) return [];
        return Object.keys(forChain).map((pairId)=>{
            return [
                deserializeToken(forChain[pairId].token0),
                deserializeToken(forChain[pairId].token1)
            ];
        });
    }, [
        savedSerializedPairs,
        chainId
    ]);
    const combinedList = useMemo(()=>userPairs.concat(generatedPairs).concat(pinnedPairs)
    , [
        generatedPairs,
        pinnedPairs,
        userPairs
    ]);
    return useMemo(()=>{
        // dedupes pairs of tokens in the combined list
        const keyed = combinedList.reduce((memo, [tokenA, tokenB])=>{
            const sorted = tokenA.sortsBefore(tokenB);
            const key = sorted ? `${tokenA.address}:${tokenB.address}` : `${tokenB.address}:${tokenA.address}`;
            if (memo[key]) return memo;
            memo[key] = sorted ? [
                tokenA,
                tokenB
            ] : [
                tokenB,
                tokenA
            ];
            return memo;
        }, {});
        return Object.keys(keyed).map((key)=>keyed[key]
        );
    }, [
        combinedList
    ]);
}
const useWatchlistTokens = ()=>{
    const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useDispatch)();
    const savedTokens = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useSelector)((state)=>state.user.watchlistTokens
    ) ?? [];
    const updatedSavedTokens = (0,react__WEBPACK_IMPORTED_MODULE_3__.useCallback)((address)=>{
        dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_8__/* .addWatchlistToken */ .zS)({
            address
        }));
    }, [
        dispatch
    ]);
    return [
        savedTokens,
        updatedSavedTokens
    ];
};
const useWatchlistPools = ()=>{
    const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useDispatch)();
    const savedPools = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useSelector)((state)=>state.user.watchlistPools
    ) ?? [];
    const updateSavedPools = (0,react__WEBPACK_IMPORTED_MODULE_3__.useCallback)((address)=>{
        dispatch((0,_actions__WEBPACK_IMPORTED_MODULE_8__/* .addWatchlistPool */ .Dn)({
            address
        }));
    }, [
        dispatch
    ]);
    return [
        savedPools,
        updateSavedPools
    ];
};


/***/ }),

/***/ 4797:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ useUserAddedTokens)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6022);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var hooks_useActiveWeb3React__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4011);
/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(787);




function useUserAddedTokens() {
    const { chainId  } = (0,hooks_useActiveWeb3React__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z)();
    const serializedTokensMap = (0,react_redux__WEBPACK_IMPORTED_MODULE_1__.useSelector)(({ user: { tokens  }  })=>tokens
    );
    return (0,react__WEBPACK_IMPORTED_MODULE_0__.useMemo)(()=>{
        if (!chainId) return [];
        return Object.values(serializedTokensMap?.[chainId] ?? {}).map(_helpers__WEBPACK_IMPORTED_MODULE_3__/* .deserializeToken */ .iG);
    }, [
        serializedTokensMap,
        chainId
    ]);
};


/***/ }),

/***/ 5509:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "v": () => (/* binding */ getGQLHeaders),
/* harmony export */   "d": () => (/* binding */ infoClient)
/* harmony export */ });
/* harmony import */ var config_constants_endpoints__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5906);
/* harmony import */ var graphql_request__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5805);
/* harmony import */ var graphql_request__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(graphql_request__WEBPACK_IMPORTED_MODULE_1__);


// Extra headers
// Mostly for dev environment
// No production env check since production preview might also need them
const getGQLHeaders = (endpoint)=>{
    if (endpoint === config_constants_endpoints__WEBPACK_IMPORTED_MODULE_0__/* .INFO_CLIENT */ .JY) {
        return {
            'X-Sf': process.env.NEXT_PUBLIC_SF_HEADER
        };
    }
    return undefined;
};
const infoClient = new graphql_request__WEBPACK_IMPORTED_MODULE_1__.GraphQLClient(config_constants_endpoints__WEBPACK_IMPORTED_MODULE_0__/* .INFO_CLIENT */ .JY, {
    headers: getGQLHeaders(config_constants_endpoints__WEBPACK_IMPORTED_MODULE_0__/* .INFO_CLIENT */ .JY)
});


/***/ }),

/***/ 261:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
const requestWithTimeout = (graphQLClient, request, variables, timeout = 30000)=>{
    return Promise.race([
        variables ? graphQLClient.request(request, variables) : graphQLClient.request(request),
        new Promise((_, reject)=>{
            setTimeout(()=>{
                reject(new Error(`Request timed out after ${timeout} milliseconds`));
            }, timeout);
        }), 
    ]);
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (requestWithTimeout);


/***/ }),

/***/ 9566:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "z": () => (/* binding */ getBlocksFromTimestamps),
/* harmony export */   "Z": () => (/* binding */ useBlocksFromTimestamps)
/* harmony export */ });
/* harmony import */ var graphql_request__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5805);
/* harmony import */ var graphql_request__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(graphql_request__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var views_Info_utils_infoQueryHelpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5126);
/* harmony import */ var config_constants_endpoints__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5906);




const getBlockSubqueries = (timestamps)=>timestamps.map((timestamp)=>{
        return `t${timestamp}:blocks(first: 1, orderBy: timestamp, orderDirection: desc, where: { timestamp_gt: ${timestamp}, timestamp_lt: ${timestamp + 600} }) {
      number
    }`;
    })
;
const blocksQueryConstructor = (subqueries)=>{
    return graphql_request__WEBPACK_IMPORTED_MODULE_0__.gql`query blocks {
    ${subqueries}
  }`;
};
/**
 * @notice Fetches block objects for an array of timestamps.
 * @param {Array} timestamps
 */ const getBlocksFromTimestamps = async (timestamps, sortDirection = 'desc', skipCount = 500)=>{
    if (timestamps?.length === 0) {
        return [];
    }
    const fetchedData = await (0,views_Info_utils_infoQueryHelpers__WEBPACK_IMPORTED_MODULE_2__/* .multiQuery */ .L)(blocksQueryConstructor, getBlockSubqueries(timestamps), config_constants_endpoints__WEBPACK_IMPORTED_MODULE_3__/* .BLOCKS_CLIENT */ .I0, skipCount);
    const sortingFunction = sortDirection === 'desc' ? (a, b)=>b.number - a.number
     : (a, b)=>a.number - b.number
    ;
    const blocks = [];
    if (fetchedData) {
        // eslint-disable-next-line no-restricted-syntax
        for (const key of Object.keys(fetchedData)){
            if (fetchedData[key].length > 0) {
                blocks.push({
                    timestamp: key.split('t')[1],
                    number: parseInt(fetchedData[key][0].number, 10)
                });
            }
        }
        // graphql-request does not guarantee same ordering of batched requests subqueries, hence manual sorting
        blocks.sort(sortingFunction);
    }
    return blocks;
};
/**
 * for a given array of timestamps, returns block entities
 * @param timestamps
 * @param sortDirection
 * @param skipCount
 */ const useBlocksFromTimestamps = (timestamps, sortDirection = 'desc', skipCount = 1000)=>{
    const { 0: blocks , 1: setBlocks  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)();
    const { 0: error , 1: setError  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const timestampsString = JSON.stringify(timestamps);
    const blocksString = blocks ? JSON.stringify(blocks) : undefined;
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        const fetchData = async ()=>{
            const timestampsArray = JSON.parse(timestampsString);
            const result = await getBlocksFromTimestamps(timestampsArray, sortDirection, skipCount);
            if (result.length === 0) {
                setError(true);
            } else {
                setBlocks(result);
            }
        };
        const blocksArray = blocksString ? JSON.parse(blocksString) : undefined;
        if (!blocksArray && !error) {
            fetchData();
        }
    }, [
        blocksString,
        error,
        skipCount,
        sortDirection,
        timestampsString
    ]);
    return {
        blocks,
        error
    };
};


/***/ }),

/***/ 5126:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "L": () => (/* binding */ multiQuery),
/* harmony export */   "z": () => (/* binding */ getDeltaTimestamps)
/* harmony export */ });
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4146);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(date_fns__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var graphql_request__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5805);
/* harmony import */ var graphql_request__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(graphql_request__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var utils_graphql__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5509);
/* harmony import */ var utils_requestWithTimeout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(261);




/**
 * Helper function to get large amount GraphQL subqueries
 * @param queryConstructor constructor function that combines subqueries
 * @param subqueries individual queries
 * @param endpoint GraphQL endpoint
 * @param skipCount how many subqueries to fire at a time
 * @returns
 */ const multiQuery = async (queryConstructor, subqueries, endpoint, skipCount = 1000)=>{
    let fetchedData = {};
    let allFound = false;
    let skip = 0;
    const client = new graphql_request__WEBPACK_IMPORTED_MODULE_1__.GraphQLClient(endpoint, {
        headers: (0,utils_graphql__WEBPACK_IMPORTED_MODULE_2__/* .getGQLHeaders */ .v)(endpoint)
    });
    try {
        while(!allFound){
            let end = subqueries.length;
            if (skip + skipCount < subqueries.length) {
                end = skip + skipCount;
            }
            const subqueriesSlice = subqueries.slice(skip, end);
            // eslint-disable-next-line no-await-in-loop
            const result = await (0,utils_requestWithTimeout__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z)(client, queryConstructor(subqueriesSlice));
            fetchedData = {
                ...fetchedData,
                ...result
            };
            allFound = Object.keys(result).length < skipCount || skip + skipCount > subqueries.length;
            skip += skipCount;
        }
        return fetchedData;
    } catch (error) {
        console.error('Failed to fetch info data', error);
        return null;
    }
};
/**
 * Returns UTC timestamps for 24h ago, 48h ago, 7d ago and 14d ago relative to current date and time
 */ const getDeltaTimestamps = ()=>{
    const utcCurrentTime = (0,date_fns__WEBPACK_IMPORTED_MODULE_0__.getUnixTime)(new Date()) * 1000;
    const t24h = (0,date_fns__WEBPACK_IMPORTED_MODULE_0__.getUnixTime)((0,date_fns__WEBPACK_IMPORTED_MODULE_0__.startOfMinute)((0,date_fns__WEBPACK_IMPORTED_MODULE_0__.subDays)(utcCurrentTime, 1)));
    const t48h = (0,date_fns__WEBPACK_IMPORTED_MODULE_0__.getUnixTime)((0,date_fns__WEBPACK_IMPORTED_MODULE_0__.startOfMinute)((0,date_fns__WEBPACK_IMPORTED_MODULE_0__.subDays)(utcCurrentTime, 2)));
    const t7d = (0,date_fns__WEBPACK_IMPORTED_MODULE_0__.getUnixTime)((0,date_fns__WEBPACK_IMPORTED_MODULE_0__.startOfMinute)((0,date_fns__WEBPACK_IMPORTED_MODULE_0__.subWeeks)(utcCurrentTime, 1)));
    const t14d = (0,date_fns__WEBPACK_IMPORTED_MODULE_0__.getUnixTime)((0,date_fns__WEBPACK_IMPORTED_MODULE_0__.startOfMinute)((0,date_fns__WEBPACK_IMPORTED_MODULE_0__.subWeeks)(utcCurrentTime, 2)));
    return [
        t24h,
        t48h,
        t7d,
        t14d
    ];
};


/***/ })

};
;
//# sourceMappingURL=54.js.map