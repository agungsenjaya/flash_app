"use strict";
exports.id = 337;
exports.ids = [337];
exports.modules = {

/***/ 3467:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/**
 * Truncate a transaction or address hash
 */ const truncateHash = (address, startLength = 4, endLength = 4)=>{
    return `${address.substring(0, startLength)}...${address.substring(address.length - endLength)}`;
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (truncateHash);


/***/ }),

/***/ 6631:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "M4": () => (/* binding */ BarChartLoader),
  "fn": () => (/* binding */ LineChartLoader)
});

// UNUSED EXPORTS: CandleChartLoader

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: external "@pancakeswap/uikit"
var uikit_ = __webpack_require__(2829);
// EXTERNAL MODULE: ./src/contexts/Localization/index.tsx + 3 modules
var Localization = __webpack_require__(9150);
;// CONCATENATED MODULE: ./src/views/Info/components/ChartLoaders/LineChartLoaderSVG.tsx



const LineChartLoaderSVG = (props)=>{
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Svg, {
        width: "100%",
        height: "100%",
        preserveAspectRatio: "none",
        viewBox: "0 0 100 50",
        ...props,
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx("path", {
                d: "M 0 49 C 1 49 1 45 4 47 C 7 49 7 35 11 37 C 13 38 14 32 16 34 C 18 35.6667 20 40 22 39 C 24 38 24 34 26 34 C 27 34 29 39 32 36 C 33 35 34 32 35 32 C 37 32 37 35 39 34 C 40 33 39 29 43 31 C 46 32 45 28 47 30 C 50 32 49 22 51 24 Q 53 26 55 24 C 56 23 56 25 57 26 C 58 27 59 28 60 28 C 63 28 66 17 67 16 C 68 15 69 17 70 16 C 71 15 71 13 74 13 C 76 13 76 14 77 15 C 79 17 80 18 82 18 C 83 18 83 17 84 17 C 87 17 89 24 91 24 C 93 24 95 20 96 17 C 97.6667 13.3333 98 9 101 6",
                stroke: "#7645D9",
                strokeWidth: "0.2",
                strokeDasharray: "156",
                strokeDashoffset: "156",
                fill: "transparent",
                opacity: "0.5",
                filter: "url(#glow)",
                children: /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                    id: "firstline",
                    attributeName: "stroke-dashoffset",
                    dur: "2s",
                    from: "156",
                    to: "-156",
                    begin: "0s;firstline.end+0.5s"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("path", {
                d: "M 0 49 C 1 49 1 45 4 47 C 7 49 7 35 11 37 C 13 38 14 32 16 34 C 18 35.6667 20 40 22 39 C 24 38 24 34 26 34 C 27 34 29 39 32 36 C 33 35 34 32 35 32 C 37 32 37 35 39 34 C 40 33 39 29 43 31 C 46 32 45 28 47 30 C 50 32 49 22 51 24 Q 53 26 55 24 C 56 23 56 25 57 26 C 58 27 59 28 60 28 C 63 28 66 17 67 16 C 68 15 69 17 70 16 C 71 15 71 13 74 13 C 76 13 76 14 77 15 C 79 17 80 18 82 18 C 83 18 83 17 84 17 C 87 17 89 24 91 24 C 93 24 95 20 96 17 C 97.6667 13.3333 98 9 101 6",
                stroke: "#7645D9",
                strokeWidth: "0.2",
                strokeDasharray: "156",
                strokeDashoffset: "156",
                fill: "transparent",
                opacity: "0.5",
                filter: "url(#glow)",
                children: /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                    id: "secondline",
                    attributeName: "stroke-dashoffset",
                    dur: "2s",
                    from: "156",
                    to: "-156",
                    begin: "1.3s;secondline.end+0.5s"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("defs", {
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("filter", {
                    id: "glow",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("feGaussianBlur", {
                            className: "blur",
                            result: "coloredBlur",
                            stdDeviation: "4"
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("feMerge", {
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("feMergeNode", {
                                    in: "coloredBlur"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("feMergeNode", {
                                    in: "coloredBlur"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("feMergeNode", {
                                    in: "coloredBlur"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("feMergeNode", {
                                    in: "SourceGraphic"
                                })
                            ]
                        })
                    ]
                })
            })
        ]
    }));
};
/* harmony default export */ const ChartLoaders_LineChartLoaderSVG = (LineChartLoaderSVG);

;// CONCATENATED MODULE: ./src/views/Info/components/ChartLoaders/BarChartLoaderSVG.tsx



const BarChartLoaderSVG = (props)=>{
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(uikit_.Svg, {
        width: "100%",
        height: "100%",
        viewBox: "0 0 50 25",
        preserveAspectRatio: "none",
        opacity: "0.1",
        ...props,
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("rect", {
                width: "8%",
                fill: "#1FC7D4",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "height",
                        dur: "0.9s",
                        values: "15%; 90%; 15%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite",
                        begin: "-0.9s"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "y",
                        dur: "0.9s",
                        values: "85%; 10%; 85%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite",
                        begin: "-0.9s"
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("rect", {
                x: "10.222%",
                width: "8%",
                fill: "#1FC7D4",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "height",
                        dur: "0.9s",
                        values: "15%; 90%; 15%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite",
                        begin: "-0.8s"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "y",
                        dur: "0.9s",
                        values: "85%; 10%; 85%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite",
                        begin: "-0.8s"
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("rect", {
                x: "20.444%",
                width: "8%",
                fill: "#1FC7D4",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "height",
                        dur: "0.9s",
                        values: "15%; 90%; 15%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite",
                        begin: "-0.7s"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "y",
                        dur: "0.9s",
                        values: "85%; 10%; 85%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite",
                        begin: "-0.7s"
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("rect", {
                x: "30.666%",
                width: "8%",
                fill: "#1FC7D4",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "height",
                        dur: "0.9s",
                        values: "15%; 90%; 15%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite",
                        begin: "-0.6s"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "y",
                        dur: "0.9s",
                        values: "85%; 10%; 85%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite",
                        begin: "-0.6s"
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("rect", {
                x: "40.888%",
                width: "8%",
                fill: "#1FC7D4",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "height",
                        dur: "0.9s",
                        values: "15%; 90%; 15%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite",
                        begin: "-0.5s"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "y",
                        dur: "0.9s",
                        values: "85%; 10%; 85%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite",
                        begin: "-0.5s"
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("rect", {
                x: "51.11%",
                width: "8%",
                fill: "#1FC7D4",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "height",
                        dur: "0.9s",
                        values: "15%; 90%; 15%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite",
                        begin: "-0.4s"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "y",
                        dur: "0.9s",
                        values: "85%; 10%; 85%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite",
                        begin: "-0.4s"
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("rect", {
                x: "61.332%",
                width: "8%",
                fill: "#1FC7D4",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "height",
                        dur: "0.9s",
                        values: "15%; 90%; 15%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite",
                        begin: "-0.3s"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "y",
                        dur: "0.9s",
                        values: "85%; 10%; 85%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite",
                        begin: "-0.3s"
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("rect", {
                x: "71.554%",
                width: "8%",
                fill: "#1FC7D4",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "height",
                        dur: "0.9s",
                        values: "15%; 90%; 15%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite",
                        begin: "-0.2s"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "y",
                        dur: "0.9s",
                        values: "85%; 10%; 85%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite",
                        begin: "-0.2s"
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("rect", {
                x: "81.776%",
                width: "8%",
                fill: "#1FC7D4",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "height",
                        dur: "0.9s",
                        values: "15%; 90%; 15%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite",
                        begin: "-0.1s"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "y",
                        dur: "0.9s",
                        values: "85%; 10%; 85%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite",
                        begin: "-0.1s"
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("rect", {
                x: "91.998%",
                width: "8%",
                fill: "#1FC7D4",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "height",
                        dur: "0.9s",
                        values: "15%; 90%; 15%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("animate", {
                        attributeName: "y",
                        dur: "0.9s",
                        values: "85%; 10%; 85%",
                        keyTimes: "0; 0.55; 1",
                        repeatCount: "indefinite"
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const ChartLoaders_BarChartLoaderSVG = (BarChartLoaderSVG);

;// CONCATENATED MODULE: ./src/views/Info/components/ChartLoaders/CandleChartLoaderSVG.tsx



const CandleChartLoaderSVG_CandleChartLoaderSVG = (props)=>{
    return(/*#__PURE__*/ _jsxs(Svg, {
        width: "100%",
        height: "100%",
        viewBox: "0 0 100 50",
        opacity: "0.1",
        ...props,
        children: [
            /*#__PURE__*/ _jsxs("rect", {
                width: "5%",
                fill: "#31D0AA",
                children: [
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "height",
                        dur: "2s",
                        values: "0%; 40%; 40%; 10%; 10%",
                        keyTimes: "0; 0.125; 0.5; 0.625; 1",
                        repeatCount: "indefinite"
                    }),
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "y",
                        dur: "2s",
                        from: "50%",
                        to: "30%",
                        values: "30%; 10%; 10%; 25%; 25%",
                        keyTimes: "0; 0.125; 0.5; 0.625; 1",
                        repeatCount: "indefinite"
                    }),
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "rx",
                        dur: "2s",
                        values: "0%; 0%; 100%; 100%;",
                        keyTimes: "0; 0.6; 0.625; 1",
                        repeatCount: "indefinite"
                    }),
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "x",
                        dur: "2s",
                        values: "32.5%; 32.5%; 47.5%; 47.5%;",
                        keyTimes: "0; 0.7; 0.8; 1",
                        repeatCount: "indefinite"
                    }),
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "opacity",
                        dur: "2s",
                        values: "1; 1; 0; 0;",
                        keyTimes: "0; 0.75; 0.9; 1",
                        repeatCount: "indefinite"
                    })
                ]
            }),
            /*#__PURE__*/ _jsxs("rect", {
                width: "5%",
                fill: "#31D0AA",
                children: [
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "height",
                        dur: "2s",
                        values: "0%; 0%; 20%; 20%; 10%; 10%",
                        keyTimes: "0; 0.125; 0.25; 0.5; 0.625; 1",
                        repeatCount: "indefinite"
                    }),
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "y",
                        dur: "2s",
                        values: "15%; 15%; 5%; 5%; 25%; 25%",
                        keyTimes: "0; 0.125; 0.25; 0.5; 0.625; 1",
                        repeatCount: "indefinite"
                    }),
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "rx",
                        dur: "2s",
                        values: "0%; 0%; 100%; 100%;",
                        keyTimes: "0; 0.6; 0.625; 1",
                        repeatCount: "indefinite"
                    }),
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "x",
                        dur: "2s",
                        values: "42.5%; 42.5%; 47.5%; 47.5%;",
                        keyTimes: "0; 0.7; 0.8; 1",
                        repeatCount: "indefinite"
                    }),
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "opacity",
                        dur: "2s",
                        values: "1; 1; 0; 0;",
                        keyTimes: "0; 0.75; 0.9; 1",
                        repeatCount: "indefinite"
                    })
                ]
            }),
            /*#__PURE__*/ _jsxs("rect", {
                width: "5%",
                fill: "#ED4B9E",
                children: [
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "height",
                        dur: "2s",
                        values: "0%; 0%; 35%; 35%; 10%; 10%",
                        keyTimes: "0; 0.25; 0.375; 0.5; 0.625; 1",
                        repeatCount: "indefinite"
                    }),
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "y",
                        dur: "2s",
                        values: "25%; 25%; 10%; 10%; 25%; 25%",
                        keyTimes: "0; 0.25; 0.375; 0.5; 0.625; 1",
                        repeatCount: "indefinite"
                    }),
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "rx",
                        dur: "2s",
                        values: "0%; 0%; 100%; 100%;",
                        keyTimes: "0; 0.6; 0.625; 1",
                        repeatCount: "indefinite"
                    }),
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "x",
                        dur: "2s",
                        values: "52.5%; 52.5%; 47.5%; 47.5%;",
                        keyTimes: "0; 0.7; 0.8; 1",
                        repeatCount: "indefinite"
                    }),
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "opacity",
                        dur: "2s",
                        values: "1; 1; 0; 0;",
                        keyTimes: "0; 0.75; 0.9; 1",
                        repeatCount: "indefinite"
                    })
                ]
            }),
            /*#__PURE__*/ _jsxs("rect", {
                width: "5%",
                fill: "#31D0AA",
                children: [
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "height",
                        dur: "2s",
                        values: "0%; 0%; 35%; 35%; 10%; 10%",
                        keyTimes: "0; 0.375; 0.5; 0.5; 0.625; 1",
                        repeatCount: "indefinite"
                    }),
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "y",
                        dur: "2s",
                        values: "15%; 15%; 0%; 0%; 25%; 25%",
                        keyTimes: "0; 0.375; 0.5; 0.5; 0.625; 1",
                        repeatCount: "indefinite"
                    }),
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "rx",
                        dur: "2s",
                        values: "0%; 0%; 100%; 100%;",
                        keyTimes: "0; 0.6; 0.625; 1",
                        repeatCount: "indefinite"
                    }),
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "x",
                        dur: "2s",
                        values: "62.5%; 62.5%; 47.5%; 47.5%;",
                        keyTimes: "0; 0.7; 0.8; 1",
                        repeatCount: "indefinite"
                    }),
                    /*#__PURE__*/ _jsx("animate", {
                        attributeName: "opacity",
                        dur: "2s",
                        values: "1; 1; 0; 0;",
                        keyTimes: "0; 0.75; 0.9; 1",
                        repeatCount: "indefinite"
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const ChartLoaders_CandleChartLoaderSVG = ((/* unused pure expression or super */ null && (CandleChartLoaderSVG_CandleChartLoaderSVG)));

;// CONCATENATED MODULE: ./src/views/Info/components/ChartLoaders/index.tsx








const LoadingText = external_styled_components_default()(uikit_.Box).withConfig({
    componentId: "sc-220ec09a-0"
})`
  position: absolute;
  margin-left: auto;
  margin-right: auto;
  top: 50%;
  left: 0;
  right: 0;
  text-align: center;
`;
const LoadingIndicator = external_styled_components_default()(uikit_.Box).withConfig({
    componentId: "sc-220ec09a-1"
})`
  height: 100%;
  position: relative;
`;
const BarChartLoader = ()=>{
    const { t  } = (0,Localization/* useTranslation */.$G)();
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(LoadingIndicator, {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(ChartLoaders_BarChartLoaderSVG, {}),
            /*#__PURE__*/ jsx_runtime_.jsx(LoadingText, {
                children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                    color: "textSubtle",
                    fontSize: "20px",
                    children: t('Loading chart data...')
                })
            })
        ]
    }));
};
const LineChartLoader = ()=>{
    const { t  } = (0,Localization/* useTranslation */.$G)();
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(LoadingIndicator, {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(ChartLoaders_LineChartLoaderSVG, {}),
            /*#__PURE__*/ jsx_runtime_.jsx(LoadingText, {
                children: /*#__PURE__*/ jsx_runtime_.jsx(uikit_.Text, {
                    color: "textSubtle",
                    fontSize: "20px",
                    children: t('Loading chart data...')
                })
            })
        ]
    }));
};
const CandleChartLoader = ()=>{
    const { t  } = useTranslation();
    return(/*#__PURE__*/ _jsxs(LoadingIndicator, {
        children: [
            /*#__PURE__*/ _jsx(CandleChartLoaderSVG, {}),
            /*#__PURE__*/ _jsx(LoadingText, {
                children: /*#__PURE__*/ _jsx(Text, {
                    color: "textSubtle",
                    fontSize: "20px",
                    children: t('Loading chart data...')
                })
            })
        ]
    }));
};


/***/ })

};
;
//# sourceMappingURL=337.js.map