"use strict";
exports.id = 838;
exports.ids = [838];
exports.modules = {

/***/ 5224:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var recharts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3655);
/* harmony import */ var recharts__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(recharts__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var hooks_useTheme__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3917);
/* harmony import */ var views_Info_utils_formatInfoNumbers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3589);
/* harmony import */ var views_Info_components_ChartLoaders__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(6631);
/* harmony import */ var contexts_Localization__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(9150);







const CustomBar = ({ x , y , width , height , fill  })=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("g", {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("rect", {
            x: x,
            y: y,
            fill: fill,
            width: width,
            height: height,
            rx: "2"
        })
    }));
};
// Calls setHoverValue and setHoverDate when part of chart is hovered
// Note: this NEEDs to be wrapped inside component and useEffect, if you plug it as is it will create big render problems (try and see console)
const HoverUpdater = ({ locale , payload , setHoverValue , setHoverDate  })=>{
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        setHoverValue(payload.value);
        setHoverDate(payload.time.toLocaleString(locale, {
            year: 'numeric',
            day: 'numeric',
            month: 'short'
        }));
    }, [
        locale,
        payload.value,
        payload.time,
        setHoverValue,
        setHoverDate
    ]);
    return null;
};
const Chart = ({ data , setHoverValue , setHoverDate  })=>{
    const { currentLanguage: { locale  } ,  } = (0,contexts_Localization__WEBPACK_IMPORTED_MODULE_6__/* .useTranslation */ .$G)();
    const { theme  } = (0,hooks_useTheme__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z)();
    if (!data || data.length === 0) {
        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(views_Info_components_ChartLoaders__WEBPACK_IMPORTED_MODULE_5__/* .BarChartLoader */ .M4, {}));
    }
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(recharts__WEBPACK_IMPORTED_MODULE_2__.ResponsiveContainer, {
        width: "100%",
        height: "100%",
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(recharts__WEBPACK_IMPORTED_MODULE_2__.BarChart, {
            data: data,
            margin: {
                top: 5,
                right: 15,
                left: 0,
                bottom: 5
            },
            onMouseLeave: ()=>{
                setHoverDate(undefined);
                setHoverValue(undefined);
            },
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(recharts__WEBPACK_IMPORTED_MODULE_2__.XAxis, {
                    dataKey: "time",
                    axisLine: false,
                    tickLine: false,
                    tickFormatter: (time)=>time.toLocaleDateString(undefined, {
                            day: '2-digit'
                        })
                    ,
                    minTickGap: 10
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(recharts__WEBPACK_IMPORTED_MODULE_2__.YAxis, {
                    dataKey: "value",
                    tickCount: 6,
                    scale: "linear",
                    axisLine: false,
                    tickLine: false,
                    color: theme.colors.textSubtle,
                    fontSize: "12px",
                    tickFormatter: (val)=>`$${(0,views_Info_utils_formatInfoNumbers__WEBPACK_IMPORTED_MODULE_4__/* .formatAmount */ .d)(val)}`
                    ,
                    orientation: "right",
                    tick: {
                        dx: 10,
                        fill: theme.colors.textSubtle
                    }
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(recharts__WEBPACK_IMPORTED_MODULE_2__.Tooltip, {
                    cursor: {
                        fill: theme.colors.backgroundDisabled
                    },
                    contentStyle: {
                        display: 'none'
                    },
                    formatter: (tooltipValue, name, props)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(HoverUpdater, {
                            locale: locale,
                            payload: props.payload,
                            setHoverValue: setHoverValue,
                            setHoverDate: setHoverDate
                        })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(recharts__WEBPACK_IMPORTED_MODULE_2__.Bar, {
                    dataKey: "value",
                    fill: theme.colors.primary,
                    shape: (props)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(CustomBar, {
                            height: props.height,
                            width: props.width,
                            x: props.x,
                            y: props.y,
                            fill: theme.colors.primary
                        })
                })
            ]
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Chart);


/***/ }),

/***/ 5091:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var recharts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3655);
/* harmony import */ var recharts__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(recharts__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var hooks_useTheme__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3917);
/* harmony import */ var views_Info_utils_formatInfoNumbers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3589);
/* harmony import */ var views_Info_components_ChartLoaders__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(6631);
/* harmony import */ var contexts_Localization__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(9150);







// Calls setHoverValue and setHoverDate when part of chart is hovered
// Note: this NEEDs to be wrapped inside component and useEffect, if you plug it as is it will create big render problems (try and see console)
const HoverUpdater = ({ locale , payload , setHoverValue , setHoverDate  })=>{
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        setHoverValue(payload.value);
        setHoverDate(payload.time.toLocaleString(locale, {
            year: 'numeric',
            day: 'numeric',
            month: 'short'
        }));
    }, [
        locale,
        payload.value,
        payload.time,
        setHoverValue,
        setHoverDate
    ]);
    return null;
};
/**
 * Note: remember that it needs to be mounted inside the container with fixed height
 */ const LineChart = ({ data , setHoverValue , setHoverDate  })=>{
    const { currentLanguage: { locale  } ,  } = (0,contexts_Localization__WEBPACK_IMPORTED_MODULE_6__/* .useTranslation */ .$G)();
    const { theme  } = (0,hooks_useTheme__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z)();
    if (!data || data.length === 0) {
        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(views_Info_components_ChartLoaders__WEBPACK_IMPORTED_MODULE_5__/* .LineChartLoader */ .fn, {}));
    }
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(recharts__WEBPACK_IMPORTED_MODULE_2__.ResponsiveContainer, {
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(recharts__WEBPACK_IMPORTED_MODULE_2__.AreaChart, {
            data: data,
            width: 300,
            height: 308,
            margin: {
                top: 5,
                right: 15,
                left: 0,
                bottom: 5
            },
            onMouseLeave: ()=>{
                if (setHoverDate) setHoverDate(undefined);
                if (setHoverValue) setHoverValue(undefined);
            },
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("defs", {
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("linearGradient", {
                        id: "gradient",
                        x1: "0",
                        y1: "0",
                        x2: "0",
                        y2: "1",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("stop", {
                                offset: "5%",
                                stopColor: theme.colors.inputSecondary,
                                stopOpacity: 0.5
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("stop", {
                                offset: "100%",
                                stopColor: theme.colors.secondary,
                                stopOpacity: 0
                            })
                        ]
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(recharts__WEBPACK_IMPORTED_MODULE_2__.XAxis, {
                    dataKey: "time",
                    axisLine: false,
                    tickLine: false,
                    tickFormatter: (time)=>time.toLocaleDateString(undefined, {
                            day: '2-digit'
                        })
                    ,
                    minTickGap: 10
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(recharts__WEBPACK_IMPORTED_MODULE_2__.YAxis, {
                    dataKey: "value",
                    tickCount: 6,
                    scale: "linear",
                    axisLine: false,
                    tickLine: false,
                    fontSize: "12px",
                    tickFormatter: (val)=>`$${(0,views_Info_utils_formatInfoNumbers__WEBPACK_IMPORTED_MODULE_4__/* .formatAmount */ .d)(val)}`
                    ,
                    orientation: "right",
                    tick: {
                        dx: 10,
                        fill: theme.colors.textSubtle
                    }
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(recharts__WEBPACK_IMPORTED_MODULE_2__.Tooltip, {
                    cursor: {
                        stroke: theme.colors.secondary
                    },
                    contentStyle: {
                        display: 'none'
                    },
                    formatter: (tooltipValue, name, props)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(HoverUpdater, {
                            locale: locale,
                            payload: props.payload,
                            setHoverValue: setHoverValue,
                            setHoverDate: setHoverDate
                        })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(recharts__WEBPACK_IMPORTED_MODULE_2__.Area, {
                    dataKey: "value",
                    type: "monotone",
                    stroke: theme.colors.secondary,
                    fill: "url(#gradient)",
                    strokeWidth: 2
                })
            ]
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (LineChart);


/***/ }),

/***/ 8543:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4146);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(date_fns__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2829);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var views_Info_utils_formatInfoNumbers__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3589);
/* harmony import */ var utils__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(8328);
/* harmony import */ var utils_truncateHash__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(3467);
/* harmony import */ var state_info_types__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(2157);
/* harmony import */ var config_constants_info__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(4003);
/* harmony import */ var contexts_Localization__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(9150);
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(8910);

// TODO PCS refactor ternaries
/* eslint-disable no-nested-ternary */ 










const Wrapper = styled_components__WEBPACK_IMPORTED_MODULE_2___default().div.withConfig({
    componentId: "sc-20b335cd-0"
})`
  width: 100%;
`;
const ResponsiveGrid = styled_components__WEBPACK_IMPORTED_MODULE_2___default().div.withConfig({
    componentId: "sc-20b335cd-1"
})`
  display: grid;
  grid-gap: 1em;
  align-items: center;
  grid-template-columns: 2fr 0.8fr repeat(4, 1fr);
  padding: 0 24px;
  @media screen and (max-width: 940px) {
    grid-template-columns: 2fr repeat(4, 1fr);
    & > *:nth-child(5) {
      display: none;
    }
  }
  @media screen and (max-width: 800px) {
    grid-template-columns: 2fr repeat(2, 1fr);
    & > *:nth-child(5) {
      display: none;
    }
    & > *:nth-child(3) {
      display: none;
    }
    & > *:nth-child(4) {
      display: none;
    }
  }
  @media screen and (max-width: 500px) {
    grid-template-columns: 2fr 1fr;
    & > *:nth-child(5) {
      display: none;
    }
    & > *:nth-child(3) {
      display: none;
    }
    & > *:nth-child(4) {
      display: none;
    }
    & > *:nth-child(2) {
      display: none;
    }
  }
`;
const RadioGroup = styled_components__WEBPACK_IMPORTED_MODULE_2___default()(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Flex).withConfig({
    componentId: "sc-20b335cd-2"
})`
  align-items: center;
  margin-right: 16px;
  margin-top: 8px;
  cursor: pointer;
`;
const SORT_FIELD = {
    amountUSD: 'amountUSD',
    timestamp: 'timestamp',
    sender: 'sender',
    amountToken0: 'amountToken0',
    amountToken1: 'amountToken1'
};
const TableLoader = ()=>{
    const loadingRow = /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(ResponsiveGrid, {
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Skeleton, {}),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Skeleton, {}),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Skeleton, {}),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Skeleton, {}),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Skeleton, {}),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Skeleton, {})
        ]
    });
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [
            loadingRow,
            loadingRow,
            loadingRow
        ]
    }));
};
const DataRow = ({ transaction  })=>{
    const { t  } = (0,contexts_Localization__WEBPACK_IMPORTED_MODULE_8__/* .useTranslation */ .$G)();
    const abs0 = Math.abs(transaction.amountToken0);
    const abs1 = Math.abs(transaction.amountToken1);
    const outputTokenSymbol = transaction.amountToken0 < 0 ? transaction.token0Symbol : transaction.token1Symbol;
    const inputTokenSymbol = transaction.amountToken1 < 0 ? transaction.token0Symbol : transaction.token1Symbol;
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(ResponsiveGrid, {
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.LinkExternal, {
                href: (0,utils__WEBPACK_IMPORTED_MODULE_6__/* .getBscScanLink */ .s6)(transaction.hash, 'transaction'),
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                    children: transaction.type === state_info_types__WEBPACK_IMPORTED_MODULE_7__/* .TransactionType.MINT */ .i.MINT ? t('Add %token0% and %token1%', {
                        token0: transaction.token0Symbol,
                        token1: transaction.token1Symbol
                    }) : transaction.type === state_info_types__WEBPACK_IMPORTED_MODULE_7__/* .TransactionType.SWAP */ .i.SWAP ? t('Swap %token0% for %token1%', {
                        token0: inputTokenSymbol,
                        token1: outputTokenSymbol
                    }) : t('Remove %token0% and %token1%', {
                        token0: transaction.token0Symbol,
                        token1: transaction.token1Symbol
                    })
                })
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                children: [
                    "$",
                    (0,views_Info_utils_formatInfoNumbers__WEBPACK_IMPORTED_MODULE_5__/* .formatAmount */ .d)(transaction.amountUSD)
                ]
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                    children: `${(0,views_Info_utils_formatInfoNumbers__WEBPACK_IMPORTED_MODULE_5__/* .formatAmount */ .d)(abs0)} ${transaction.token0Symbol}`
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                    children: `${(0,views_Info_utils_formatInfoNumbers__WEBPACK_IMPORTED_MODULE_5__/* .formatAmount */ .d)(abs1)} ${transaction.token1Symbol}`
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.LinkExternal, {
                href: (0,utils__WEBPACK_IMPORTED_MODULE_6__/* .getBscScanLink */ .s6)(transaction.sender, 'address'),
                children: (0,utils_truncateHash__WEBPACK_IMPORTED_MODULE_10__/* ["default"] */ .Z)(transaction.sender)
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                children: (0,date_fns__WEBPACK_IMPORTED_MODULE_3__.formatDistanceToNowStrict)(parseInt(transaction.timestamp, 10) * 1000)
            })
        ]
    }));
};
const TransactionTable = ({ transactions  })=>{
    const { 0: sortField , 1: setSortField  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(SORT_FIELD.timestamp);
    const { 0: sortDirection , 1: setSortDirection  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(true);
    const { t  } = (0,contexts_Localization__WEBPACK_IMPORTED_MODULE_8__/* .useTranslation */ .$G)();
    const { 0: page , 1: setPage  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(1);
    const { 0: maxPage , 1: setMaxPage  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(1);
    const { 0: txFilter , 1: setTxFilter  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(undefined);
    const sortedTransactions = (0,react__WEBPACK_IMPORTED_MODULE_1__.useMemo)(()=>{
        const toBeAbsList = [
            SORT_FIELD.amountToken0,
            SORT_FIELD.amountToken1
        ];
        return transactions ? transactions.slice().sort((a, b)=>{
            if (a && b) {
                const firstField = a[sortField];
                const secondField = b[sortField];
                const [first, second] = toBeAbsList.includes(sortField) ? [
                    Math.abs(firstField),
                    Math.abs(secondField)
                ] : [
                    firstField,
                    secondField
                ];
                return first > second ? (sortDirection ? -1 : 1) * 1 : (sortDirection ? -1 : 1) * -1;
            }
            return -1;
        }).filter((x)=>{
            return txFilter === undefined || x.type === txFilter;
        }).slice(config_constants_info__WEBPACK_IMPORTED_MODULE_11__/* .ITEMS_PER_INFO_TABLE_PAGE */ .si * (page - 1), page * config_constants_info__WEBPACK_IMPORTED_MODULE_11__/* .ITEMS_PER_INFO_TABLE_PAGE */ .si) : [];
    }, [
        transactions,
        page,
        sortField,
        sortDirection,
        txFilter
    ]);
    // Update maxPage based on amount of items & applied filtering
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        if (transactions) {
            const filteredTransactions = transactions.filter((tx)=>{
                return txFilter === undefined || tx.type === txFilter;
            });
            if (filteredTransactions.length % config_constants_info__WEBPACK_IMPORTED_MODULE_11__/* .ITEMS_PER_INFO_TABLE_PAGE */ .si === 0) {
                setMaxPage(Math.floor(filteredTransactions.length / config_constants_info__WEBPACK_IMPORTED_MODULE_11__/* .ITEMS_PER_INFO_TABLE_PAGE */ .si));
            } else {
                setMaxPage(Math.floor(filteredTransactions.length / config_constants_info__WEBPACK_IMPORTED_MODULE_11__/* .ITEMS_PER_INFO_TABLE_PAGE */ .si) + 1);
            }
        }
    }, [
        transactions,
        txFilter
    ]);
    const handleFilter = (0,react__WEBPACK_IMPORTED_MODULE_1__.useCallback)((newFilter)=>{
        if (newFilter !== txFilter) {
            setTxFilter(newFilter);
            setPage(1);
        }
    }, [
        txFilter
    ]);
    const handleSort = (0,react__WEBPACK_IMPORTED_MODULE_1__.useCallback)((newField)=>{
        setSortField(newField);
        setSortDirection(sortField !== newField ? true : !sortDirection);
    }, [
        sortDirection,
        sortField
    ]);
    const arrow = (0,react__WEBPACK_IMPORTED_MODULE_1__.useCallback)((field)=>{
        const directionArrow = !sortDirection ? '↑' : '↓';
        return sortField === field ? directionArrow : '';
    }, [
        sortDirection,
        sortField
    ]);
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(Wrapper, {
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Flex, {
                mb: "16px",
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Flex, {
                        flexDirection: [
                            'column',
                            'row'
                        ],
                        children: [
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(RadioGroup, {
                                onClick: ()=>handleFilter(undefined)
                                ,
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Radio, {
                                        onChange: ()=>null
                                        ,
                                        scale: "sm",
                                        checked: txFilter === undefined
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                                        ml: "8px",
                                        children: t('All')
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(RadioGroup, {
                                onClick: ()=>handleFilter(state_info_types__WEBPACK_IMPORTED_MODULE_7__/* .TransactionType.SWAP */ .i.SWAP)
                                ,
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Radio, {
                                        onChange: ()=>null
                                        ,
                                        scale: "sm",
                                        checked: txFilter === state_info_types__WEBPACK_IMPORTED_MODULE_7__/* .TransactionType.SWAP */ .i.SWAP
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                                        ml: "8px",
                                        children: t('Swaps')
                                    })
                                ]
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Flex, {
                        flexDirection: [
                            'column',
                            'row'
                        ],
                        children: [
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(RadioGroup, {
                                onClick: ()=>handleFilter(state_info_types__WEBPACK_IMPORTED_MODULE_7__/* .TransactionType.MINT */ .i.MINT)
                                ,
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Radio, {
                                        onChange: ()=>null
                                        ,
                                        scale: "sm",
                                        checked: txFilter === state_info_types__WEBPACK_IMPORTED_MODULE_7__/* .TransactionType.MINT */ .i.MINT
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                                        ml: "8px",
                                        children: t('Adds')
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(RadioGroup, {
                                onClick: ()=>handleFilter(state_info_types__WEBPACK_IMPORTED_MODULE_7__/* .TransactionType.BURN */ .i.BURN)
                                ,
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Radio, {
                                        onChange: ()=>null
                                        ,
                                        scale: "sm",
                                        checked: txFilter === state_info_types__WEBPACK_IMPORTED_MODULE_7__/* .TransactionType.BURN */ .i.BURN
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                                        ml: "8px",
                                        children: t('Removes')
                                    })
                                ]
                            })
                        ]
                    })
                ]
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_shared__WEBPACK_IMPORTED_MODULE_9__/* .TableWrapper */ .y6, {
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(ResponsiveGrid, {
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                                color: "secondary",
                                fontSize: "12px",
                                bold: true,
                                textTransform: "uppercase",
                                children: t('Action')
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_shared__WEBPACK_IMPORTED_MODULE_9__/* .ClickableColumnHeader */ ._J, {
                                color: "secondary",
                                fontSize: "12px",
                                bold: true,
                                onClick: ()=>handleSort(SORT_FIELD.amountUSD)
                                ,
                                textTransform: "uppercase",
                                children: [
                                    t('Total Value'),
                                    " ",
                                    arrow(SORT_FIELD.amountUSD)
                                ]
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_shared__WEBPACK_IMPORTED_MODULE_9__/* .ClickableColumnHeader */ ._J, {
                                color: "secondary",
                                fontSize: "12px",
                                bold: true,
                                onClick: ()=>handleSort(SORT_FIELD.amountToken0)
                                ,
                                textTransform: "uppercase",
                                children: [
                                    t('Token Amount'),
                                    " ",
                                    arrow(SORT_FIELD.amountToken0)
                                ]
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_shared__WEBPACK_IMPORTED_MODULE_9__/* .ClickableColumnHeader */ ._J, {
                                color: "secondary",
                                fontSize: "12px",
                                bold: true,
                                onClick: ()=>handleSort(SORT_FIELD.amountToken1)
                                ,
                                textTransform: "uppercase",
                                children: [
                                    t('Token Amount'),
                                    " ",
                                    arrow(SORT_FIELD.amountToken1)
                                ]
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_shared__WEBPACK_IMPORTED_MODULE_9__/* .ClickableColumnHeader */ ._J, {
                                color: "secondary",
                                fontSize: "12px",
                                bold: true,
                                onClick: ()=>handleSort(SORT_FIELD.sender)
                                ,
                                textTransform: "uppercase",
                                children: [
                                    t('Account'),
                                    " ",
                                    arrow(SORT_FIELD.sender)
                                ]
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_shared__WEBPACK_IMPORTED_MODULE_9__/* .ClickableColumnHeader */ ._J, {
                                color: "secondary",
                                fontSize: "12px",
                                bold: true,
                                onClick: ()=>handleSort(SORT_FIELD.timestamp)
                                ,
                                textTransform: "uppercase",
                                children: [
                                    t('Time'),
                                    " ",
                                    arrow(SORT_FIELD.timestamp)
                                ]
                            })
                        ]
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_shared__WEBPACK_IMPORTED_MODULE_9__/* .Break */ .SS, {}),
                    transactions ? /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                        children: [
                            sortedTransactions.map((transaction, index)=>{
                                if (transaction) {
                                    return(// eslint-disable-next-line react/no-array-index-key
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((react__WEBPACK_IMPORTED_MODULE_1___default().Fragment), {
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(DataRow, {
                                                transaction: transaction
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_shared__WEBPACK_IMPORTED_MODULE_9__/* .Break */ .SS, {})
                                        ]
                                    }, index));
                                }
                                return null;
                            }),
                            sortedTransactions.length === 0 ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Flex, {
                                justifyContent: "center",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                                    children: t('No Transactions')
                                })
                            }) : undefined,
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_shared__WEBPACK_IMPORTED_MODULE_9__/* .PageButtons */ .Ob, {
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_shared__WEBPACK_IMPORTED_MODULE_9__/* .Arrow */ .Eh, {
                                        onClick: ()=>{
                                            setPage(page === 1 ? page : page - 1);
                                        },
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.ArrowBackIcon, {
                                            color: page === 1 ? 'textDisabled' : 'primary'
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Text, {
                                        children: t('Page %page% of %maxPage%', {
                                            page,
                                            maxPage
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_shared__WEBPACK_IMPORTED_MODULE_9__/* .Arrow */ .Eh, {
                                        onClick: ()=>{
                                            setPage(page === maxPage ? page : page + 1);
                                        },
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.ArrowForwardIcon, {
                                            color: page === maxPage ? 'textDisabled' : 'primary'
                                        })
                                    })
                                ]
                            })
                        ]
                    }) : /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(TableLoader, {}),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_4__.Box, {})
                        ]
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TransactionTable);


/***/ }),

/***/ 7795:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2829);
/* harmony import */ var _pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_2__);



const Percent = ({ value , ...rest })=>{
    if (!value || Number.isNaN(value)) {
        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_2__.Text, {
            ...rest,
            children: "-"
        }));
    }
    const isNegative = value < 0;
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_pancakeswap_uikit__WEBPACK_IMPORTED_MODULE_2__.Text, {
        ...rest,
        color: isNegative ? 'failure' : 'success',
        children: [
            isNegative ? '↓' : '↑',
            Math.abs(value).toFixed(2),
            "%"
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Percent);


/***/ })

};
;
//# sourceMappingURL=838.js.map